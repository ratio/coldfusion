<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select feed_pic from feed
		  where feed_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.feed_pic#")>
			<cffile action = "delete" file = "#media_path#\#remove.feed_pic#">
		</cfif>

	</cfif>

	<cfif #feed_pic# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select feed_pic from feed
		  where feed_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.feed_pic# is not "">
         <cfif fileexists("#media_path#\#getfile.feed_pic#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.feed_pic#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "feed_pic"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update feed
	  set feed_title = '#feed_title#',
	      feed_desc = '#feed_desc#',
	      feed_url = '#feed_url#',

		  <cfif #feed_pic# is not "">
		   feed_pic = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   feed_pic = null,
		  </cfif>

		  feed_keywords = '#feed_keywords#',
		  feed_category_id = #feed_category_id#,
		  feed_updated = #now()#

		  where feed_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#

	  </cfquery>

      <cflocation URL="my_feeds.cfm?u=3" addtoken="no">

<cfelseif #button# is "Post">

	<cfquery name="user_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select usr_first_name, usr_last_name, usr_company_id from usr
	 where usr_id = #session.usr_id#
	</cfquery>

	<cfset feed_user_name = #user_info.usr_first_name# & " " & #user_info.usr_last_name#>

	<cfif user_info.usr_company_id is "">
	 <cfset feed_company_name = "">
	<cfelse>

	<cfquery name="comp_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select company_name from company
	 where company_id = #user_info.usr_company_id#
	</cfquery>

	</cfif>

   <cfset feed_company_name = "">

	<cfif #feed_pic# is not "">
		<cffile action = "upload"
		 fileField = "feed_pic"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="post" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert feed
	  (
       feed_title,
       feed_user_name,
       feed_company_name,
       feed_desc,
       feed_url,
       feed_pic,
       feed_keywords,
       feed_category_id,
       feed_created,
       feed_created_usr_id,
       feed_updated,
       feed_hub_id,
       feed_type_id
	  )
	  values
	  (
	  '#feed_title#',
	  '#feed_user_name#',
	  '#feed_company_name#',
	  '#feed_desc#',
	  '#feed_url#',
	  <cfif feed_pic is "">null<cfelse>'#cffile.serverfile#'</cfif>,
	  '#feed_keywords#',
	   #feed_category_id#,
	   #now()#,
	   #session.usr_id#,
	   #now()#,
	   #session.hub#,
	   5
	  )
	</cfquery>

</cfif>

<cflocation URL="/exchange/my_feeds.cfm?u=1" addtoken="no">
