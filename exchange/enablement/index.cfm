<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header">GET SUPPORT</td></tr>
       <tr><td><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td colspan=2><img src="/images/bah_enablement.png" width=175></td></tr>
		   <tr><td height=20></td></tr>
		   <tr><td class="feed_header" colspan=2>Important Links</td></tr>
		   <tr><td width=30 class="feed_sub_header"><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header"><a href="https://www.seedspot.org" target="_blank" rel="noopener" rel="noreferrer" style="padding-top: 0px;">SEED SPOT</td></tr>
		   <tr><td class="feed_sub_header"><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header"><a href="https://seedspot.org/programs" target="_blank" rel="noopener" rel="noreferrer" >Program Schedule</td></tr>
		   <tr><td class="feed_sub_header"><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header"><a href="https://seedspot.org/contact/" target="_blank" rel="noopener" rel="noreferrer" >Contact SEED SPOT</td></tr>
		   <tr><td class="feed_sub_header"><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header"><a href="https://seedspot.org/impact/" target="_blank" rel="noopener" rel="noreferrer" >Impact Report</td></tr>
		  </table>

      </td><td width=30>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Partnerning Together to Help You Succeed</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">

		   We're here to help you succeed.   SEED SPOT offers many Programs and Services to help you start,
		   run, manage, and run and expand your business.
		   sign up on our

		   </td></tr>

           <tr><td><hr></td></tr>

           <tr><td height=10></td></tr>
           <tr><td class="feed_header">Programs and Services</td></tr>
           <tr><td height=20></td></tr>

           <tr><td>

		   <!--- Programs and Services --->

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		      <tr>

		      <td valign=top width=125><img src="/images/ss-pitch.png" width=90 border=0 vspace=5></td>
		      <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 10px;"><u>PITCH EVENTS</u></td></tr>

                <tr>
                    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-right: 20px;">
				     These events allow you to do a,b and c.
		            </td>
		        </tr>

			   </table>

              </td></tr>

		      <tr><td height=15></td></tr>
		      <tr><td colspan=3><hr></td></tr>
		      <tr><td height=15></td></tr>

           	</table>


		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		      <tr>

		      <td valign=top width=125><img src="/images/ss-launch.png" width=90 border=0 vspace=5></td>
		      <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 10px;"><u>LAUNCH CAMPS</u></td></tr>

                <tr>
                    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-right: 20px;">
				     Launch Camps are designed to help you ...
		            </td>
		        </tr>

			   </table>

              </td></tr>

		      <tr><td height=15></td></tr>
		      <tr><td colspan=3><hr></td></tr>
		      <tr><td height=15></td></tr>

           	</table>




		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		      <tr>

		      <td valign=top width=125><img src="/images/ss-demoday.png" width=90 border=0 vspace=5></td>
		      <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 10px;"><u>DEMO DAYS</u></td></tr>

                <tr>
                    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-right: 20px;">
				     Get your product or service in front of ...
		            </td>
		        </tr>

			   </table>

              </td></tr>

		      <tr><td height=15></td></tr>
		      <tr><td colspan=3><hr></td></tr>
		      <tr><td height=15></td></tr>

           	</table>

 		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		      <tr>

 		      <td valign=top width=125><img src="/images/ss-impact.png" width=90 border=0 vspace=5></td>
 		      <td valign=top>

 			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 10px;"><u>IMPACT ENTREPRENUER WORKSHOPS</u></td></tr>

                 <tr>
                     <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-right: 20px;">
 				     Get engaged with ...
 		            </td>
 		        </tr>

 			   </table>

               </td></tr>

 		      <tr><td height=15></td></tr>
 		      <tr><td colspan=3><hr></td></tr>
 		      <tr><td height=15></td></tr>

           	</table>

 		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		      <tr>

 		      <td valign=top width=125><img src="/images/ss-founder.png" width=90 border=0 vspace=5></td>
 		      <td valign=top>

 			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 10px;"><u>FOUNDER SCRUM</u></td></tr>

                 <tr>
                     <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-right: 20px;">
 				     Talk to Founders to learn...
 		            </td>
 		        </tr>

 			   </table>

               </td></tr>

 		      <tr><td height=15></td></tr>
 		      <tr><td colspan=3><hr></td></tr>
 		      <tr><td height=15></td></tr>

           	</table>


		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		      <tr>

		      <td valign=top width=125><img src="/images/ss-webinar.png" width=90 border=0 vspace=5></td>
		      <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 10px;"><u>WEBINARS</u></td></tr>

                <tr>
                    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-right: 20px;">
				     Engage with an audience of ...
		            </td>
		        </tr>

			   </table>

              </td></tr>

		      <tr><td height=15></td></tr>
		      <tr><td colspan=3><hr></td></tr>
		      <tr><td height=15></td></tr>

           	</table>








           </td></tr>

          </table>

	     </td></tr>

      </table>

      </div>

      </td><td valign=top>

	  <cfinclude template="/exchange/network.cfm">
      <!---
      <cfinclude template="/exchange/components/my_communities/index.cfm">      --->

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

