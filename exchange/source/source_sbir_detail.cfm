<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=middle>Federal SBIR/STTR Awards</td><td class="feed_sub_header" align=right valign=middle><a href="source_sbir.cfm">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>

          </table>

		  <cfquery name="vendor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select top(1) company from sbir
	       where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
	       and duns = '#duns#'
		  </cfquery>

		  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select id, contract, department, abstract, award_start_date, award_title, research_keywords, award_amount, agency, phase from sbir
	       where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
	       and duns = '#duns#'
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_sub_header"><cfoutput><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer">#ucase(vendor.company)#</a></cfoutput></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr height=50>
               <td class="feed_option"><b>Award Date</b></td>
               <td class="feed_option"><b>Award Number</b></td>
               <td class="feed_option"><b>Agency</b></td>
               <td class="feed_option"><b>Award Title</b></td>
               <td class="feed_option"><b>Abstract & Keywords</b></td>
               <td class="feed_option" align=center><b>Program</b></td>
               <td class="feed_option" align=center><b>Phase</b></td>
               <td class="feed_option" align=right><b>Award Value</b></td>
            </tr>

           <cfset counter = 0>
           <cfoutput query="agencies">
            <cfif counter is 0>
             <tr bgcolor="ffffff" height=30>
            <cfelse>
             <tr bgcolor="e0e0e0" height=30>
            </cfif>
               <td class="text_xsmall" valign=top width=75>#dateformat(award_start_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=150><a href="/exchange/sbir/detail.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#contract#</b></a></td>
               <td class="text_xsmall" valign=top width=150>#department#<br>#agency#</td>
               <td class="text_xsmall" valign=top width=200>#award_title#</td>
               <td class="text_xsmall" valign=top>#ucase(replaceNoCase(agencies.abstract,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all"))#
               <br><br>
               <b>Research Keywords - </b>
               #ucase(replaceNoCase(research_keywords,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all"))#
               <br><br>
               </td>
               <td class="text_xsmall" valign=top width=75 align=center>Program</td>
               <td class="text_xsmall" valign=top width=75 align=center>#phase#</td>
               <td class="text_xsmall" valign=top width=100 align=right>#numberformat(award_amount,'$999,999,999')#</td>
            </tr>
            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>
          </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

