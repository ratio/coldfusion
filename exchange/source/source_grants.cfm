<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=middle>Companies awarded Federal Grants for <i>"<cfoutput>#session.source_keywords#</cfoutput></i>"</td><td class="feed_sub_header" valign=middle align=right><a href="/exchange/source/results.cfm">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, sum(federal_action_obligation) as amount, award_description, recipient_name, recipient_duns, recipient_city_name, recipient_state_code from grants
	       where contains((cfda_title, award_description),'"#session.source_keywords#"')
	       group by recipient_duns, recipient_name, award_description, recipient_city_name, recipient_state_code

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by recipient_name ASC
		    <cfelseif #sv# is 10>
		     order by recipient_name DESC
		    <cfelseif #sv# is 2>
		     order by total DESC
		    <cfelseif #sv# is 20>
		     order by total ASC
		    <cfelseif #sv# is 3>
		     order by amount DESC
		    <cfelseif #sv# is 30>
		     order by amount ASC
		    <cfelseif #sv# is 4>
		     order by recipient_city_name ASC
		    <cfelseif #sv# is 40>
		     order by recipient_city_name DESC
		    <cfelseif #sv# is 5>
		     order by recipient_state_code ASC
		    <cfelseif #sv# is 50>
		     order by recipient_state_code DESC

		    </cfif>

		   <cfelse>
		     order by amount DESC
		   </cfif>

		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr height=50>
               <td class="feed_sub_header"><a href="source_grants.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a></td>
               <td class="feed_sub_header"><a href="source_grants.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>City</b></a></td>
               <td class="feed_sub_header"><a href="source_grants.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>State</b></a></td>
               <td class="feed_sub_header" align=center><a href="source_grants.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Awards</b></a></td>
               <td class="feed_sub_header" align=right><a href="source_grants.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Amount</b></a></td>
            </tr>

           <cfset counter = 0>
           <cfoutput query="agencies">
            <cfif counter is 0>
             <tr bgcolor="ffffff" height=30>
            <cfelse>
             <tr bgcolor="e0e0e0" height=30>
            </cfif>
               <td class="feed_sub_header"><a href="source_grant_detail.cfm?duns=#recipient_duns#"><b>#ucase(recipient_name)#</b></a></td>
               <td class="feed_sub_header" style="font-weight: normal;">#ucase(recipient_city_name)#</td>
               <td class="feed_sub_header" style="font-weight: normal;">#recipient_state_code#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(total,'999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" width=100 align=right>#numberformat(amount,'$999,999,999')#</td>
            </tr>
            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>
          </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

