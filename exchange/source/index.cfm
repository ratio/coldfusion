<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"award_search_name")>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id#
 order by award_view_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="source_search.cfm">
		</div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle>Search Trends</td></tr>
         <tr><td><hr></td></tr>
        </table>

        <!--- My Searches --->

		<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="20">
		 select top(10) keyword_search_keyword, count(keyword_search_keyword) as total from keyword_search
		 where keyword_search_area = 1 and
		       keyword_search_usr_id = #session.usr_id#
		 group by keyword_search_keyword
		 order by total DESC
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td valign=top width=30%>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" valign=middle>My Recent Searches (Top 20)</td></tr>

         <cfif search.recordcount is 0>

          <tr><td class="feed_sub_header" style="font-weight: normal;">You do not have any recent searches.</td></tr>

         <cfelse>

          <cfoutput query="search">
           <tr><td class="feed_sub_header" height=30><img src="/images/dot_blue2.png" width=8 hspace=10>&nbsp;&nbsp;&nbsp;&nbsp;<a href="set_source.cfm?keyword=#keyword_search_keyword#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><b>#ucase(keyword_search_keyword)# (#total#)</b></a></td></tr>
          </cfoutput>

         </cfif>

        </table>

        </td><td valign=top>

		<cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="20">
		 select top(10) keyword_search_keyword, count(keyword_search_keyword) as total from keyword_search
		 where keyword_search_area = 1
		 group by keyword_search_keyword
		 order by total DESC
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" valign=middle>Recent Searches from Everyone (Top 20)</td></tr>
         <cfif market.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No market searches were found.</td></tr>
         <cfelse>
         <tr><td>
			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript">

			google.charts.load('current', {packages: ['corechart', 'bar']});
			google.charts.setOnLoadCallback(drawBasic);

			function drawBasic() {

				  var data = google.visualization.arrayToDataTable([
					['Keyword', 'Total'],
						<cfoutput query="market">
					['#keyword_search_keyword#', #total#],

					</cfoutput>
				  ]);

				  var options = {
					title: '',
					chartArea: {width: '85%'},
					legend: 'none',
					height: 200,
					hAxis: {
					  textStyle: {color: 'black', fontSize: 10},
					  format: 'currency',
					  title: '',
					  minValue: 0
					},
					vAxis: {
					  title: '# of Searches',
					}
				  };

				  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_1'));

				  chart.draw(data, options);
				}

				</script>

			  <div id="chart_div_1" style="width: 100%"></div>

           </td></tr>
           </cfif>

        </table>



        </td></tr>

        </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>