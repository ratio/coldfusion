<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Search Results</td><td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>

         </table>

          <!--- Get Results --->

		  <cfquery name="awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(id) as total, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from award_data
	       where contains((award_description),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="award_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from award_data
	       where contains((award_description),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year]
		  </cfquery>

		  <cfquery name="grants" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(id) as total, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from grants
	       where contains((award_description),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="grant_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from grants
	       where contains((award_description),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year]
		  </cfquery>

		  <cfquery name="sbir" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(id) as total, count(distinct(duns)) as vendors, count(distinct(department)) as agencies, sum(award_amount) as amount from sbir
	       where contains((award_title, abstract),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="sbir_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select count(id) as total, award_year, count(distinct(duns)) as vendors, count(distinct(department)) as agencies, sum(award_amount) as amount from sbir
	        where contains((award_title, abstract),'"#session.source_keywords#"')
			group by award_year
			order by award_year
		  </cfquery>

		  <cfset total_value = #evaluate(sbir.vendors + grants.vendors + awards.vendors)#>

		  <cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(company_id) as total from company
 	       where contains((company_about, company_history, company_keywords, company_homepage_text),'"#session.source_keywords#"')
		  </cfquery>

		  <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td height=15></td></tr>

		      <tr><td class="feed_sub_header" colspan=4><a href="source_exchange.cfm"><cfoutput>#company.total#</cfoutput> commercial companies</a> were found that deliver <i>"#session.source_keywords#"</i></td></tr>
              <tr><td height=10></td></tr>
		      <tr><td class="feed_sub_header" colspan=4><cfoutput>#total_value#</cfoutput> companies have been awarded Federal contracts, grants, and SBIR/STTRs for <i>"#session.source_keywords#"</i>.  Select below for additional information.</td></tr>
              <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" width=100></td>
			       <td class="feed_sub_header" width=100 align=center>Awards</td>
			       <td class="feed_sub_header" width=100 align=center>Agencies</td>
			       <td class="feed_sub_header" width=100 align=center>Companies</td>
			       <td class="feed_sub_header" width=150 align=right>Amount Awarded</td>
			       </tr>

			   <tr bgcolor="e0e0e0">
                   <td class="feed_option"><b><a href="source_awards.cfm">Federal Contract Awards</a></b></td>
                   <td class="feed_option" align=center><b>#numberformat(awards.total,'999,999')#</b></td>
                   <td class="feed_option" align=center><b>#numberformat(awards.agencies,'999,999')#</b></td>
                   <td class="feed_option" align=center><b>#numberformat(awards.vendors,'999,999')#</b></td>
                   <td class="feed_option" align=right><b>#numberformat(awards.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfoutput query="award_detail">

			   <tr bgcolor="e0e0e0">
                   <td class="feed_option">#award_detail.year#</td>
                   <td class="feed_option" align=center>#numberformat(award_detail.total,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(award_detail.agencies,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(award_detail.vendors,'999,999')#</td>
                   <td class="feed_option" align=right>#numberformat(award_detail.amount,'$999,999,999')#</td>
                   </tr>

           </cfoutput>

           <cfoutput>


               <tr><td class="feed_option"><a href="source_grants.cfm"><b>Federal Grants</b></a></td>
                   <td class="feed_option" align=center><b>#numberformat(grants.total,'999,999')#</b></td>
                   <td class="feed_option" align=center><b>#numberformat(grants.agencies,'999,999')#</b></td>
                   <td class="feed_option" align=center><b>#numberformat(grants.vendors,'999,999')#</b></td>
                   <td class="feed_option" align=right><b>#numberformat(grants.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfoutput query="grant_detail">

			   <tr>
                   <td class="feed_option">#grant_detail.year#</td>
                   <td class="feed_option" align=center>#numberformat(grant_detail.total,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(grant_detail.agencies,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(grant_detail.vendors,'999,999')#</td>
                   <td class="feed_option" align=right>#numberformat(grant_detail.amount,'$999,999,999')#</td>
                   </tr>

           </cfoutput>

           <cfoutput>


               <tr bgcolor="e0e0e0"><td class="feed_option"><a href="source_sbir.cfm"><b>Federal SBIR/STTR's</b></a></td>
                   <td class="feed_option" align=center><b>#numberformat(sbir.total,'999,999')#</b></td>
                   <td class="feed_option" align=center><b>#numberformat(sbir.agencies,'999,999')#</b></td>
                   <td class="feed_option" align=center><b>#numberformat(sbir.vendors,'999,999')#</b></td>
                   <td class="feed_option" align=right><b>#numberformat(sbir.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfoutput query="sbir_detail">

			   <tr bgcolor="e0e0e0">
                   <td class="feed_option">#sbir_detail.award_year#</td>
                   <td class="feed_option" align=center>#numberformat(sbir_detail.total,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(sbir_detail.agencies,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(sbir_detail.vendors,'999,999')#</td>
                   <td class="feed_option" align=right>#numberformat(sbir_detail.amount,'$999,999,999')#</td>
                   </tr>

           </cfoutput>


			  </table>


	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

