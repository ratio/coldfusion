<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Search Results</td><td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td class="feed_option">

           <cfoutput>
            <b>Keywords: </b>#session.source_keywords#
           </cfoutput>

           </td></tr>

           <tr><td colspan=2><hr></td></tr>

           <cfif isdefined("u")>
            <cfif u is 1>
             <tr><td><font size=2 color="red"><b></b></font></td></tr>
            <cfelseif u is 2>
             <tr><td><font size=2 color="green"><b></b></font></td></tr>
            </cfif>
            <tr><td>&nbsp;<td></tr>
           </cfif>

          </table>

		  <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from webcontent
		   left join ccomp on ccomp_website = webcontent_url
	       where contains((webcontent_content),'"#session.source_keywords#"')
		  </cfquery>

          <!--- Get Results --->

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <tr><td height=10></td></tr>
	              <tr><td class="feed_sub_header"><a href="source.cfm"><b>Federal Sourcing</b></a>&nbsp;|&nbsp;<a href="source_exchange.cfm">Exchange Companies</a>&nbsp;|&nbsp;<a href="source_market.cfm">In the Market</a></td></tr>
				  <tr><td height=10></td></tr>
		      </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <cfif market.recordcount is 0>
               <tr><td class="feed_option">No companies were found.</td></tr>
              <cfelse>

              <tr>
                  <td class="feed_sub_header"></td>
                  <td class="feed_sub_header"><b>Description</b></td>
                  <td width=30>&nbsp;</td>
                  <td class="feed_sub_header"><b>Location</b></td>
                  <td class="feed_sub_header"><b>Tags</b></td>
              </tr>

              <cfoutput query="market">
              <tr>
                  <td class="feed_option" valign=top><b>#ccomp_name#</b><br>
                  <span class="text_xsmall"><img src="//logo.clearbit.com/#ccomp_website#" width=100><br><a href="#ccomp_website#" target="_blank" rel="noopener" rel="noreferrer">#ccomp_website#</a></span></td>
                  <td class="feed_option" valign=top valign=top width=700>#replace(ccomp_description,"\n\n"," ","all")#</td>
                  <td width=30>&nbsp;</td>
                  <td class="feed_option" valign=top valign=top width=150>#ccomp_city#, #ccomp_state#</td>
                  <td class="feed_option" valign=top valign=top>#ccomp_market_1#<br>#ccomp_market_2#<br>#ccomp_market_3#</td>
               </tr>
               <tr><td colspan=5><hr></td></tr>
              </cfoutput>

              </cfif>

              </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

