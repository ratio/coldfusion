<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"award_search_name")>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id#
 order by award_view_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/source/source_search.cfm">
		</div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle>Search Results - "<i><cfoutput>#session.source_keywords#</cfoutput></i>"</td>
             <td align=right class="feed_sub_header"><a href="index.cfm" valign=middle>Return</td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

        <cfset grapharray = ArrayNew(2)>

		<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from award_data
	       where contains((award_description),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="award_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="10">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from award_data
		   where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year] DESC
		  </cfquery>




		  <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from grants
	       where contains((award_description),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="grant_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="10">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from grants
	       where contains((award_description),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year] DESC
		  </cfquery>

		  <cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, count(distinct(duns)) as vendors, count(distinct(department)) as agencies, sum(award_amount) as amount from sbir
		   where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="sbir_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="10">
			select count(id) as total, award_year, count(distinct(duns)) as vendors, count(distinct(department)) as agencies, sum(award_amount) as amount from sbir
		    where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
			group by award_year
			order by award_year DESC
		  </cfquery>

		   <cfquery name="patents_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		    select count(patent.id) as total, count(distinct(assignee_id)) as holders from patent
		    left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
	        where contains((title, abstract),'"#session.source_keywords#"')
		   </cfquery>

		   <cfquery name="patents_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="10">
            select count(patent.id) as total, datepart(yyyy, [date]) as [year], count(distinct(assignee_id)) as holders from patent
		    left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
		    where contains((title, abstract),'"#session.source_keywords#"') and
		    patent.patent_id is not null
            group by datepart(yyyy, [date])
            order by [year] DESC
		   </cfquery>
		  <cfset total_value = #evaluate(sbir.vendors + grants.vendors + awards.vendors)#>

<!---

       <cfset count = 1>
       <cfset year_array = ArrayNew(1)>
       <cfloop index="yr" from="2020" to="2010" step="-1">
        <cfset year_array[#count#] = #yr#>
        <cfset count = count + 1>
       </cfloop>


       <cfset count = 1>
       <cfset award_array = ArrayNew(1)>
       <cfloop query="award_detail">
        <cfset award_array[#count#] = #year#>
        <cfset count = count + 1>
       </cfloop>


		<cfscript>
			   myQuery=QueryNew("Values","varchar");
			  QueryAddColumn(myQuery,"Year","varchar",year_array);
			  QueryAddColumn(myQuery,"Awards","varchar",award_array);
			   WriteDump(myQuery);
		</cfscript>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Contracts', 'Grants', 'SBIR/STTRs'],

          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',0,0]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
    <div id="curve_chart" style="width: 100%; height: 500px"></div>




          <cfset year = #evaluate(dateformat(now(),'yyyy')-10)#> --->


		  <cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(company_id) as total from company
 	       where contains((company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#session.source_keywords#"')
		  </cfquery>

		  <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		      <tr><td class="feed_sub_header">#total_value# companies were found who were awarded Federal contracts, grants, and SBIR/STTRs.<td></tr>
              <tr><td><hr></td></tr>
              </table>
          </cfoutput>

          <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr height=50><td class="feed_sub_header" width=100>

               Federal Awards

			   </td>
			       <td class="feed_sub_header" width=100 align=center>## of Awards</td>
			       <td class="feed_sub_header" width=100 align=center>Agencies</td>
			       <td class="feed_sub_header" width=100 align=center>Companies</td>
			       <td class="feed_sub_header" width=150 align=right>Amount Awarded</td>
			       </tr>

               <tr><td colspan=5><hr></td></tr>

			   <tr height=30>
                   <td class="feed_sub_header"><a href="source_awards.cfm"><u>Contracts</u></a></td>
                   <td class="feed_sub_header" align=center>#numberformat(awards.total,'999,999')#</td>
                   <td class="feed_sub_header" align=center>#numberformat(awards.agencies,'999,999')#</td>
                   <td class="feed_sub_header" align=center>#numberformat(awards.vendors,'999,999')#</td>
                   <td class="feed_sub_header" align=right>#numberformat(awards.amount,'$999,999,999')#</td>
                   </tr>

           </cfoutput>

           <cfset counter = 0>

           <cfoutput query="award_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_sub_header" style="font-weight: normal;">#award_detail.year#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(award_detail.total,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(award_detail.agencies,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(award_detail.vendors,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(award_detail.amount,'$999,999,999')#</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>

           <tr><td colspan=5><hr></td></tr>

           <cfoutput>

               <tr><td class="feed_sub_header" height=30><a href="source_grants.cfm"><u>Grants</u></a></td>
                   <td class="feed_sub_header" align=center>#numberformat(grants.total,'999,999')#</td>
                   <td class="feed_sub_header" align=center>#numberformat(grants.agencies,'999,999')#</td>
                   <td class="feed_sub_header" align=center>#numberformat(grants.vendors,'999,999')#</td>
                   <td class="feed_sub_header" align=right>#numberformat(grants.amount,'$999,999,999')#</td>
                   </tr>

           </cfoutput>

           <cfset counter = 0>

           <cfoutput query="grant_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_sub_header" style="font-weight: normal;" height=30>#grant_detail.year#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(grant_detail.total,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(grant_detail.agencies,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(grant_detail.vendors,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(grant_detail.amount,'$999,999,999')#</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>

           <tr><td colspan=5><hr></td></tr>

           <cfoutput>

               <tr><td class="feed_sub_header" height=30><a href="source_sbir.cfm"><u>SBIR/STTR's</u></a></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(sbir.total,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(sbir.agencies,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(sbir.vendors,'999,999')#</b></td>
                   <td class="feed_sub_header" align=right><b>#numberformat(sbir.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfset counter = 0>

           <cfoutput query="sbir_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_sub_header" style="font-weight: normal;" height=30>#sbir_detail.award_year#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(sbir_detail.total,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(sbir_detail.agencies,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(sbir_detail.vendors,'999,999')#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(sbir_detail.amount,'$999,999,999')#</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>

           <tr><td colspan=5><hr></td></tr>

           <cfoutput>

			   <tr height=50><td class="feed_sub_header" width=100><a href="source_patents.cfm"><u>Patents</u></a></td>
			       <td class="feed_sub_header" width=100 align=center>## of Patents (#trim(numberformat(patents_total.total,'99,999'))#)</td>
			       <td class="feed_sub_header" width=100 align=center>Companies (#trim(numberformat(patents_total.holders,'99,999'))#)</td>
		       </tr>

		   </cfoutput>

           <cfoutput query="patents_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_sub_header" style="font-weight: normal;" height=30>#patents_detail.year#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(patents_detail.holders)#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(patents_detail.total)#</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>&nbsp;</td>
                   <td class="feed_sub_header" style="font-weight: normal;" align=center>&nbsp;</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>






               <tr><td colspan=5><hr></td></tr>









			  </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>