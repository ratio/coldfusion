<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

		<cfquery name="awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select distinct(recipient_duns), corp_url, elec_bus_poc_fname, elec_bus_poc_lnmae, elec_bus_poc_title, elec_bus_poc_us_phone, elec_bus_poc_email, pp_poc_fname, pp_poc_lname, pp_poc_title, pp_poc_phone, pp_poc_email, alt_poc_fname, alt_poc_lname, alt_poc_title, akt_poc_phone, alt_poc_email, poc_us_phone, poc_email, poc_title, poc_lname, poc_fnme, count(id) as total_awards, recipient_name, sum(federal_action_obligation) as total_value from award_data
	      left join sams on duns = recipient_duns
	      where contains((award_description),'"#session.source_keywords#"')
	      group by recipient_duns, corp_url, recipient_name, elec_bus_poc_fname, elec_bus_poc_lnmae, elec_bus_poc_title, elec_bus_poc_us_phone, elec_bus_poc_email, pp_poc_fname, pp_poc_lname, pp_poc_title, pp_poc_phone, pp_poc_email, alt_poc_lname, alt_poc_fname, alt_poc_title, akt_poc_phone, alt_poc_email, poc_us_phone, poc_email, poc_title, poc_fnme, poc_lname
	      order by total_value DESC
	    </cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle>SEARCH RESULTS (<cfoutput><i>#session.source_keywords#</i></cfoutput>)</td>
             <td align=right><a href="index.cfm" valign=middle><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

	    <cfquery name="grants" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select distinct(recipient_duns), corp_url, elec_bus_poc_fname, elec_bus_poc_lnmae, elec_bus_poc_title, elec_bus_poc_us_phone, elec_bus_poc_email, pp_poc_fname, pp_poc_lname, pp_poc_title, pp_poc_phone, pp_poc_email, alt_poc_fname, alt_poc_lname, alt_poc_title, akt_poc_phone, alt_poc_email, poc_us_phone, poc_email, poc_title, poc_lname, poc_fnme, count(id) as total_awards, recipient_name, sum(federal_action_obligation) as total_value from grants
	     left join sams on duns = recipient_duns
	     where contains((award_description),'"#session.source_keywords#"')
	     group by recipient_duns, corp_url, recipient_name, elec_bus_poc_fname, elec_bus_poc_lnmae, elec_bus_poc_title, elec_bus_poc_us_phone, elec_bus_poc_email, pp_poc_fname, pp_poc_lname, pp_poc_title, pp_poc_phone, pp_poc_email, alt_poc_lname, alt_poc_fname, alt_poc_title, akt_poc_phone, alt_poc_email, poc_us_phone, poc_email, poc_title, poc_fnme, poc_lname
	     order by total_value DESC
		</cfquery>

	    <cfquery name="sbir" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select distinct(sbir.duns), corp_url, pi_name, pi_title, pi_phone, pi_email, contact_name, contact_title, contact_phone, contact_email, elec_bus_poc_fname, elec_bus_poc_lnmae, elec_bus_poc_title, elec_bus_poc_us_phone, elec_bus_poc_email, pp_poc_fname, pp_poc_lname, pp_poc_title, pp_poc_phone, pp_poc_email, alt_poc_fname, alt_poc_lname, alt_poc_title, akt_poc_phone, alt_poc_email, poc_us_phone, poc_email, poc_title, poc_lname, poc_fnme, count(id) as total_awards, company, sum(award_amount) as total_value from sbir
	     left join sams on sams.duns = sbir.duns
         where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
	     group by sbir.duns, corp_url, company, pi_name, pi_title, pi_phone, pi_email, contact_name, contact_title, contact_phone, contact_email, elec_bus_poc_fname, elec_bus_poc_lnmae, elec_bus_poc_title, elec_bus_poc_us_phone, elec_bus_poc_email, pp_poc_fname, pp_poc_lname, pp_poc_title, pp_poc_phone, pp_poc_email, alt_poc_lname, alt_poc_fname, alt_poc_title, akt_poc_phone, alt_poc_email, poc_us_phone, poc_email, poc_title, poc_fnme, poc_lname
	     order by total_value DESC
		</cfquery>

	    <cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select distinct(company_name), rank, cast(short_description as varchar(max)) as comp_desc, company_keywords, email, phone, homepage_url from company
		 left join cb_organizations on uuid = company_cb_id
	     where contains((company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#session.source_keywords#"')
	     order by rank ASC
		</cfquery>

		<cfquery name="patents" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
         select * from patent
         join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
		 where contains((title, abstract),'"#session.source_keywords#"')
        </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr height=50>
               <td class="feed_sub_header" width=200><b>Vendor / POC</td>
               <td class="feed_sub_header"><b>Name</td>
               <td class="feed_sub_header"><b>Title</td>
               <td class="feed_sub_header"><b>Phone</td>
               <td class="feed_sub_header"><b>Email</td>
               <td class="feed_sub_header" align=center><b>Awards</b></td>
               <td class="feed_sub_header" align=right><b>Value</b></td>
            </tr>

           <tr><td height=10></td></tr>
           <tr><td class="feed_header" colspan=2>FEDERAL AWARDS</td></tr>
           <tr><td height=10></td></tr>

           <cfloop query="awards">

			   <cfquery name="award_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				select action_date, id, awarding_agency_name, awarding_sub_agency_name, federal_action_obligation, award_description from award_data
				where contains((award_description),'"#session.source_keywords#"') and
				federal_action_obligation > 0 and
				recipient_duns = '#awards.recipient_duns#'
				order by action_date DESC
			   </cfquery>

			   <cfoutput>
			   <tr bgcolor="e0e0e0">
				   <td class="feed_sub_header" width=200 valign=top colspan=5><a href="#awards.corp_url#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(awards.recipient_name)#</b></a></td>
				   <td class="feed_sub_header" align=center valign=top width=75><b>#awards.total_awards#</b></td>
				   <td class="feed_sub_header" align=right valign=top width=100><b>#numberformat(awards.total_value,'$999,999,999')#</b></td>
				   </tr>

			   <cfif awards.poc_fnme is "" and awards.poc_lname is "">
			   <cfelse>

			   <tr>
				   <td class="feed_option" valign=top>Primary</td>
				   <td class="feed_option" valign=top>#awards.poc_fnme# #awards.poc_lname#</td>
				   <td class="feed_option" valign=top>#awards.poc_title#</td>
				   <td class="feed_option" valign=top width=200>#awards.poc_us_phone#</td>
				   <td class="feed_option" valign=top>#awards.poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>

			   <cfif awards.alt_poc_fname is "" and awards.alt_poc_lname is "">
			   <cfelse>
			   <tr>
				   <td class="feed_option" valign=top>Alternative</td>
				   <td class="feed_option" valign=top>#awards.alt_poc_fname# #awards.alt_poc_lname#</td>
				   <td class="feed_option" valign=top>#awards.alt_poc_title#</td>
				   <td class="feed_option" valign=top>#awards.akt_poc_phone#</td>
				   <td class="feed_option" valign=top>#awards.alt_poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>

			   <cfif awards.pp_poc_fname is "" and awards.pp_poc_lname is "">
			   <cfelse>
			   <tr>
				   <td class="feed_option" valign=top>Past Performance</td>
				   <td class="feed_option" valign=top>#awards.pp_poc_fname# #awards.pp_poc_lname#</td>
				   <td class="feed_option" valign=top>#awards.pp_poc_title#</td>
				   <td class="feed_option" valign=top>#awards.pp_poc_phone#</td>
				   <td class="feed_option" valign=top colspan=3>#awards.pp_poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>

			   <cfif awards.elec_bus_poc_fname is "" and awards.elec_bus_poc_lnmae is "">
			   <cfelse>

			   <tr>
				   <td class="feed_option" valign=top>Electronic Business</td>
				   <td class="feed_option" valign=top>#awards.elec_bus_poc_fname# #awards.elec_bus_poc_lnmae#</td>
				   <td class="feed_option" valign=top>#awards.elec_bus_poc_title#</td>
				   <td class="feed_option" valign=top>#awards.elec_bus_poc_us_phone#</td>
				   <td class="feed_option" valign=top>#awards.elec_bus_poc_email#</td>
				</tr>
				</cfif>

			   </cfoutput>

			   <tr><td height=10 colspan=7><hr></td></tr>

			   <cfoutput query="award_detail">

			    <tr>
			       <td colspan=7 class="feed_option"><b>#ucase(award_detail.awarding_agency_name)# / #ucase(award_detail.awarding_sub_agency_name)#<br>Awarded - #dateformat(award_detail.action_date,'mm/dd/yyyy')# for #numberformat(award_detail.federal_action_obligation,'$999,999,999')#</b></td>
			    </tr>

			    <tr>
			       <td colspan=7 class="feed_option"><b>PRODUCT OR SERVICES</b><br>#award_detail.award_description#</td>
			    </tr>

			    <tr><td colspan=7><hr></td></tr>

			   </cfoutput>

			   <tr><td height=10></td></tr>

           </cfloop>


           <tr><td height=10></td></tr>
           <tr><td class="feed_header" colspan=2>FEDERAL GRANTS</td></tr>
           <tr><td height=10></td></tr>

           <cfloop query="grants">

			   <cfquery name="grant_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				select action_date, id, recipient_duns, awarding_agency_name, awarding_sub_agency_name, federal_action_obligation, award_description from grants
				where contains((award_description),'"#session.source_keywords#"') and
				federal_action_obligation > 0 and
				recipient_duns = '#grants.recipient_duns#'
				order by action_date DESC
			   </cfquery>


			   <cfoutput>
			   <tr bgcolor="e0e0e0">
				   <td class="feed_sub_header" width=200 valign=top colspan=5><b><a href="#grants.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#ucase(grants.recipient_name)#</a></b></td>
				   <td class="feed_sub_header" align=center valign=top width=75><b>#grants.total_awards#</b></td>
				   <td class="feed_sub_header" align=right valign=top width=100><b>#numberformat(grants.total_value,'$999,999,999')#</b></td>
				   </tr>
			   <tr>
				   <td class="feed_option" valign=top>Primary</td>
				   <td class="feed_option" valign=top>#grants.poc_fnme# #grants.poc_lname#</td>
				   <td class="feed_option" valign=top width=200>#grants.poc_title#</td>
				   <td class="feed_option" valign=top>#grants.poc_us_phone#</td>
				   <td class="feed_option" valign=top>#grants.poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>

			   <cfif grants.alt_poc_fname is "" and grants.alt_poc_lname is "">
			   <cfelse>

			   <tr>
				   <td class="feed_option" valign=top>Alternative</td>
				   <td class="feed_option" valign=top>#grants.alt_poc_fname# #grants.alt_poc_lname#</td>
				   <td class="feed_option" valign=top>#grants.alt_poc_title#</td>
				   <td class="feed_option" valign=top>#grants.akt_poc_phone#</td>
				   <td class="feed_option" valign=top>#grants.alt_poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>


			   <cfif grants.pp_poc_fname is "" and grants.pp_poc_lname is "">
			   <cfelse>

			   <tr>
				   <td class="feed_option" valign=top>Past Performance</td>
				   <td class="feed_option" valign=top>#grants.pp_poc_fname# #grants.pp_poc_lname#</td>
				   <td class="feed_option" valign=top>#grants.pp_poc_title#</td>
				   <td class="feed_option" valign=top>#grants.pp_poc_phone#</td>
				   <td class="feed_option" valign=top>#grants.pp_poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>

			   <cfif grants.elec_bus_poc_fname is "" and grants.elec_bus_poc_lnmae is "">
			   <cfelse>

			   <tr>
				   <td class="feed_option" valign=top>Electronic Business</td>
				   <td class="feed_option" valign=top>#grants.elec_bus_poc_fname# #grants.elec_bus_poc_lnmae#</td>
				   <td class="feed_option" valign=top>#grants.elec_bus_poc_title#</td>
				   <td class="feed_option" valign=top>#grants.elec_bus_poc_us_phone#</td>
				   <td class="feed_option" valign=top>#grants.elec_bus_poc_email#</td>
				</tr>
			   </cfif>

			   </cfoutput>

			   <tr><td height=10 colspan=7><hr></td></tr>

			   <cfoutput query="grant_detail">

			    <tr>
			       <td colspan=7 class="feed_option"><b>#ucase(grant_detail.awarding_agency_name)# / #ucase(grant_detail.awarding_sub_agency_name)#<br>Awarded - #dateformat(grant_detail.action_date,'mm/dd/yyyy')# for #numberformat(grant_detail.federal_action_obligation,'$999,999,999')#</b></td>
			    </tr>

			    <tr>
			       <td colspan=7 class="feed_option"><b>PRODUCT OR SERVICES</b><br>#grant_detail.award_description#</td>
			    </tr>

			    <tr><td colspan=7><hr></td></tr>

			   </cfoutput>

			   <tr><td height=10></td></tr>

           </cfloop>

           <tr><td height=10></td></tr>
           <tr><td class="feed_header" colspan=3>FEDERAL SBIR/STTR AWARDS</td></tr>
           <tr><td height=10></td></tr>

           <cfloop query="sbir">

				<cfquery name="sbir_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select award_title, department, award_amount, award_year, abstract as sbir_desc, research_keywords from sbir
				 where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"') and
				 duns = '#sbir.duns#'
				 order by award_start_date DESC
				</cfquery>

			   <cfoutput>
			   <tr bgcolor="e0e0e0">
				   <td class="feed_sub_header" width=200 valign=top colspan=5><b><a href="#sbir.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#ucase(sbir.company)#</a></b></td>
				   <td class="feed_sub_header" align=center valign=top width=75><b>#sbir.total_awards#</b></td>
				   <td class="feed_sub_header" align=right valign=top width=100><b>#numberformat(sbir.total_value,'$999,999,999')#</b></td>
				   </tr>

			   <tr>
				   <td class="feed_option" valign=top>Primary</td>
				   <td class="feed_option" valign=top>#sbir.contact_name#</td>
				   <td class="feed_option" valign=top>#sbir.contact_title#</td>
				   <td class="feed_option" valign=top>#sbir.contact_phone#</td>
				   <td class="feed_option" valign=top>#sbir.contact_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>


               <cfif sbir.pi_name is not "">
			   <tr>
				   <td class="feed_option" valign=top>PI Contact</td>
				   <td class="feed_option" valign=top>#sbir.pi_name#</td>
				   <td class="feed_option" valign=top>#sbir.pi_title#</td>
				   <td class="feed_option" valign=top>#sbir.pi_phone#</td>
				   <td class="feed_option" valign=top>#sbir.pi_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>

               <cfif sbir.poc_fnme is "" and sbir.poc_lname is "">
               <cfelse>

			   <tr>
				   <td class="feed_option" valign=top>Secondary</td>
				   <td class="feed_option" valign=top>#sbir.poc_fnme# #sbir.poc_lname#</td>
				   <td class="feed_option" valign=top>#sbir.poc_title#</td>
				   <td class="feed_option" valign=top>#sbir.poc_us_phone#</td>
				   <td class="feed_option" valign=top>#sbir.poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>
			   </cfif>


			   <tr>
				   <td class="feed_option" valign=top>Alternative</td>
				   <td class="feed_option" valign=top>#sbir.alt_poc_fname# #sbir.alt_poc_lname#</td>
				   <td class="feed_option" valign=top>#sbir.alt_poc_title#</td>
				   <td class="feed_option" valign=top>#sbir.akt_poc_phone#</td>
				   <td class="feed_option" valign=top>#sbir.alt_poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>


			   <tr>
				   <td class="feed_option" valign=top>Past Performance</td>
				   <td class="feed_option" valign=top>#sbir.pp_poc_fname# #sbir.pp_poc_lname#</td>
				   <td class="feed_option" valign=top>#sbir.pp_poc_title#</td>
				   <td class="feed_option" valign=top>#sbir.pp_poc_phone#</td>
				   <td class="feed_option" valign=top>#sbir.pp_poc_email#</td>
			   </tr>
			   <tr><td colspan=7><hr></td></tr>


			   <tr>
				   <td class="feed_option" valign=top>Electronic Business</td>
				   <td class="feed_option" valign=top>#sbir.elec_bus_poc_fname# #sbir.elec_bus_poc_lnmae#</td>
				   <td class="feed_option" valign=top>#sbir.elec_bus_poc_title#</td>
				   <td class="feed_option" valign=top>#sbir.elec_bus_poc_us_phone#</td>
				   <td class="feed_option" valign=top>#sbir.elec_bus_poc_email#</td>
				</tr>
			   </cfoutput>

			   <tr><td height=10></td></tr>

			   <tr><td height=10 colspan=7><hr></td></tr>

			   <cfoutput query="sbir_detail">

			    <tr>
			       <td colspan=7 class="feed_option"><b>#ucase(award_title)#</b></td>
			    </tr>

			    <tr>
			       <td colspan=7 class="feed_option"><b>ABSTRACT</b><br>#sbir_desc#</td>
			    </tr>

			    <tr>
			       <td colspan=7 class="feed_option"><b>#ucase(department)#&nbsp;|&nbsp;#award_year#&nbsp;|&nbsp;#numberformat(award_amount,'$999,999,999')#</td>
			    </tr>

			    <tr><td colspan=7><hr></td></tr>

			   </cfoutput>



           </cfloop>

           <tr><td height=10></td></tr>
           <tr><td colspan=7><hr></td></tr>
           <tr><td class="feed_header" colspan=6>COMPANIES</td>
               <td class="feed_header" align=right>RANK</td></tr>
           <tr><td height=10></td></tr>

           <cfloop query="comp">

			   <cfoutput>
			   <tr bgcolor="e0e0e0">
				   <td class="feed_sub_header" width=200 valign=top colspan=6><b><a href="#comp.homepage_url#" target="_blank" rel="noopener" rel="noreferrer">#ucase(comp.company_name)#</a></b></td>
				   <td class="feed_sub_header" width=200 valign=top align=right><b>#ucase(numberformat(comp.rank,'999,999'))#</b></td>
				   </tr>

               <tr><td class="feed_option" colspan=6><b>#ucase(comp.comp_desc)#</b></td></tr>

			   <tr>
				   <td class="feed_option" valign=top>Primary</td>
				   <td class="feed_option" valign=top>#comp.email#</td>
				   <td class="feed_option" valign=top>#comp.phone#</td>
			   </tr>

			   </cfoutput>

			   <tr><td height=10></td></tr>

           </cfloop>

           <tr><td height=10></td></tr>
           <tr><td class="feed_header" colspan=7>PATENTS</td></tr>
           <tr><td height=10></td></tr>

           <cfloop query="patents">

			   <cfoutput>
			   <tr bgcolor="e0e0e0">
				   <td class="feed_sub_header" width=200 valign=top colspan=7><b>#ucase(patents.organization)#</b></td>
				   </tr>

			   <tr><td height=5></td></tr>

			   <tr>
				   <td class="feed_option" valign=top colspan=7><b>#ucase(patents.title)#</b></td>
			   </tr>

			   <tr>
				   <td class="feed_option" valign=top colspan=7>#patents.abstract#</td>
			   </tr>

			   </cfoutput>

			   <tr><td height=10></td></tr>

           </cfloop>

   		  </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>