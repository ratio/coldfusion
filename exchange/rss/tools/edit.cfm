<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from rss
 where rss_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit RSS Newsfeed</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfoutput>

		   <tr><td class="feed_sub_header">Feed Provider</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="rss_name" value="#edit.rss_name#" required></td></tr>

		   <tr><td class="feed_sub_header">Provider Website</td>
		       <td><input class="input_text" style="width: 400px;" type="url" name="rss_name_url" value="#edit.rss_name_url#"></td></tr>

		   <tr><td class="feed_sub_header">Feed Category</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="rss_category" value="#edit.rss_category#"></td></tr>

		   <tr><td class="feed_sub_header">Feed URL</td>
		       <td><input class="input_text" style="width: 900px;" type="url" name="rss_url" value="#edit.rss_url#" required></td></tr>

			<tr><td class="feed_sub_header" valign=top>Feed Image</td>
				<td class="feed_sub_header" style="font-weight: normal;">

				<cfif #edit.rss_image# is "">
					<input type="file" id="image" onchange="validate_img()" name="rss_image">
				<cfelse>
				  <img src="#media_virtual#/#edit.rss_image#" width=100><br><br>
					<input type="file" id="image" onchange="validate_img()" name="rss_image"><br><br>
					<input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
				 </cfif>

				 </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td></td><td>

		   <input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>

		   <input type="hidden" name="i" value=#i#>

		   </cfoutput>

           </td></tr>

		  </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

