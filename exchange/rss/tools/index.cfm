<cfinclude template="/exchange/security/check.cfm">

<cfquery name="rss" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from rss
  where rss_hub_id = #session.hub#
  order by rss_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">RSS Newsfeeds</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/challenges/tools/">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">RSS Newsfeed has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">RSS Newsfeed has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">RSS Newsfeed has been successfully deleted.</td>
        <cfelseif u is 4>
         <td class="feed_sub_header" style="color: green;">RSS Newsfeed has been successfully ran.</td>
        <cfelseif u is 5>
         <td class="feed_sub_header" style="color: green;">All RSS Newsfeeds have been successfully ran.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add RSS Newsfeed</a>

       <a href="run_all_feeds.cfm" onclick="return confirm('Run Feeds?\r\nAre you sure you want to run all Feeds?');"><img src="/images/icon_play.png" width=25 hspace=10></a><a href="run_all_feeds.cfm" onclick="return confirm('Run Feeds?\r\nAre you sure you want to run all Feeds?');">Run All Feeds</a></td></tr>

      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #rss.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No RSS Newsfeed Channels have been created.</td></tr>

        <cfelse>

           <tr>
			<td class="feed_sub_header">Logo</b></td>
			<td class="feed_sub_header">Name</b></td>
			<td class="feed_sub_header" style="padding-left: 20px;">Category</b></td>
			<td class="feed_sub_header">URL</b></td>
			<td></td>
		   </tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="rss">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" width=75><a href="edit.cfm?i=#encrypt(rss_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

              <cfif rss_image is "">
               No Image
              <cfelse>
              <img src="#media_virtual#/#rss_image#" width=50>
              </cfif>

              </a>

              </td>

              <td class="feed_sub_header"><a href="edit.cfm?i=#encrypt(rss_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#rss_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal; padding-left: 20px;">#rss_category#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#rss_url#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right width=50><a href="run_feed.cfm?i=#encrypt(rss_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="return confirm('Run Feed?\r\nAre you sure you want to run this Feed?');"><img src="/images/icon_play.png" width=30 alt="Run Feed" title="Run Feed"></a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

