<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #rss_image# is not "">
		<cffile action = "upload"
		 fileField = "rss_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into rss
	 (
	   rss_name,
	   rss_name_url,
	   rss_category,
	   rss_url,
	   rss_hub_id,
	   rss_usr_id,
	   rss_image,
	   rss_updated
	  )
	 values
	 (
	 '#rss_name#',
	 '#rss_name_url#',
	 '#rss_category#',
	 '#rss_url#',
	  #session.hub#,
	  #session.usr_id#,
	  <cfif #rss_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	  #now()#
	  )
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select rss_image from rss
		  where rss_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.rss_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.rss_image#">
		</cfif>

	</cfif>

	<cfif rss_image is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select rss_image from rss
		  where rss_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.rss_image# is not "">
         <cfif fileexists("#media_path#\#getfile.rss_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.rss_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "rss_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update rss
	 set rss_name = '#rss_name#',
	     rss_name_url = '#rss_name_url#',

		  <cfif #rss_image# is not "">
		   rss_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   rss_image = null,
		  </cfif>

	     rss_category = '#rss_category#',
	     rss_url = '#rss_url#',
	     rss_updated = #now()#,
	     rss_usr_id = #session.usr_id#
	 where rss_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select rss_image from rss
		  where rss_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif remove.rss_image is not "">
         <cfif fileexists("#media_path#\#remove.rss_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.rss_image#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete rss
	 where rss_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

