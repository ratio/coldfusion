<cfinclude template="/exchange/security/check.cfm">

<cfquery name="view_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_xref_usr_id from hub_xref
 where hub_xref_hub_id = #session.hub# and
       hub_xref_active = 1 and
	   hub_xref_usr_id <> #session.usr_id#
</cfquery>

<cfif view_list.recordcount is 0>
 <cfset list = 0>
<cfelse>
 <cfset list = valuelist(view_list.hub_xref_usr_id)>
</cfif>

<cfquery name="view_list_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(usr_view_by_usr_id) from usr_view
 where usr_view_usr_id = #session.usr_id# and
       usr_view_by_usr_id in (#list#) and
       usr_view_hub_id = #session.hub#
</cfquery>

<cfif view_list_2.recordcount is 0>
 <cfset list2 = 0>
<cfelse>
 <cfset list2 = valuelist(view_list_2.usr_view_by_usr_id)>
</cfif>

<cfquery name="profile_views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id in (#list2#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 180px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
	  <td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td>

      <td valign=top width=100%>

	  <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_header">Profile Views</td>
		    <td align=right></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=20></td></tr>
	   </table>

       <cfif profile_views.recordcount is 0>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_sub_header" style="font-weight: normal;">No users have viewed your profile.</td></tr>
	   </table>

       </cfif>

	        <cfloop query="profile_views">

				<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select * from usr_follow
				 where usr_follow_usr_id = #session.usr_id# and
				       usr_follow_usr_follow_id = #profile_views.usr_id# and
				       usr_follow_hub_id = #session.hub#
				</cfquery>

            <cfset i = #encrypt(profile_views.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

			<cfquery name="count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select count(usr_view_by_usr_id) as total from usr_view
			 where usr_view_by_usr_id = #profile_views.usr_id# and
			       usr_view_hub_id = #session.hub# and
			       usr_view_usr_id = #session.usr_id#
			</cfquery>

            <cfoutput>
			<div class="profile_badge">

            <cfif #profile_views.usr_profile_display# is 2>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td height=10></td></tr>
	          <tr><td align=center><img src="/images/headshot.png" height=120></td></tr>
	          <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
	          <tr><td class="feed_sub_header" align=center>#count.total# Views</td></tr>
	         </table>

            <cfelse>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr><td valign=top width=140>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif #profile_views.usr_photo# is "">
					  <tr><td align=center valign=top width=130><a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img src="/images/headshot.png" height=125 width=125 border=0 vspace=15></td><td width=20>&nbsp;</td></tr>
					 <cfelse>
					  <tr><td align=center valign=top width=130><a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img style="border-radius: 180px;" vspace=15 src="#media_virtual#/#profile_views.usr_photo#" height=125 width=125 border=0><td width=20>&nbsp;</td></tr>
					 </cfif>

					 </table>

				 <td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=15></td></tr>
				 <tr><td class="feed_sub_header" style="padding-top: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#i#">#profile_views.usr_first_name# #profile_views.usr_last_name#</a>
				     <td class="feed_sub_header" style="padding-top: 0px;">

				     <cfif count.total is 0>
				     <cfelseif #count.total# is 1>
				      1 View
				     <cfelse>
				     #count.total# Views
				     </cfif>

				     </td></td>
				     <td align=right valign=top>

				     <cfif #follow.recordcount# is 1>
				     	<a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img src="/images/small_follow.png" width=20 alt="You are following this person" title="You are following this person" border=0></a>
				     </cfif>

				     </td></tr>
 				 <tr><td class="feed_option" style="padding-top: 0px; padding-bottom: 0px;" colspan=2><b>#ucase(profile_views.usr_company_name)#</b></td></tr>
				 <cfif #profile_views.usr_title# is not "">
				 	<tr><td class="feed_option" style="padding-top: 5px; padding-bottom: 8px;" colspan=2>#profile_views.usr_title#</td></tr>
				 </cfif>
			     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=20 valign=middle>&nbsp;&nbsp;#tostring(tobinary(profile_views.usr_email))#</td></tr>
<!---
			     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-phone_2.png" alt="Work Phone" title="Work Phone" width=20 valign=middle>&nbsp;&nbsp;<cfif #profile_views.usr_phone# is "">Not Provided<cfelse>#profile_views.usr_phone#</cfif></td></tr>
			     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon_cell_phone.png" alt="Cell Phone" title="Cell Phone" width=20 valign=middle>&nbsp;&nbsp;<cfif #profile_views.usr_cell_phone# is "">Not Provided<cfelse>#profile_views.usr_cell_phone#</cfif></td></tr> --->
				<tr><td height=5></td></tr>
				<tr><td colspan=3 class="text_xsmall"><font color="gray" colspan=2><i>Last updated on #dateformat(profile_views.usr_updated,'mmm dd, yyyy')#</i></font></td></tr>
				</table>

				</td></tr>

				</table>

				</cfif>

			</div>

	   	   </cfoutput>

	   	   </cfloop>

     </div>

     </td>

     </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

