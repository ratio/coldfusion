<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from lens
   where lens_hub_id = #session.hub#
   order by lens_order
  </cfquery>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="column_left.cfm">

      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/">Welcome</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_news_feed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/news.cfm">Newswire</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/my_feeds.cfm">Posts</a></span>
          </div>

		  <div class="main_box_2">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">Create Post</td><td align=right class="feed_option"><a href="/exchange/my_feeds.cfm"><img src="/images/delete.png" border=0 width=20></a></td></tr>
				   <tr><td colspan=2><hr></td></tr>
				   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3>Please complete the below form to publish your Post.</td></tr>
          </table>

          <form action="post_db.cfm" method="post" enctype="multipart/form-data" >

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_sub_header"><b>Title</b></td></tr>
				   <tr><td colspan=2><input type="text" class="input_text" name="feed_title" style="width: 750px;" required maxlength="100"></td></tr>
				   <tr><td class="feed_sub_header"><b>Message</b></td></tr>
				   <tr><td colspan=2><textarea name="feed_desc" class="input_textarea" style="width: 750px;" rows=5></textarea></td></tr>
				   <tr><td class="feed_sub_header"><b>URL Reference</b></td></tr>
				   <tr><td colspan=2><input type="url" name="feed_URL" class="input_text" style="width: 750px;" maxlength="200"></td></tr>
                   <tr><td class="feed_sub_header" valign=top>Picture / Image</td></tr>
                   <tr><td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="feed_pic"></td></tr>

                   <tr><td class="link_small_gray">Image will be auto scaled to 900px (width) by 250px (height)</td></tr>


  			       <tr><td class="feed_sub_header">Post Category</td>
  			           <td class="feed_sub_header">Tags or Keywords</td></tr>
			       <tr><td width=400>
					  <select class="input_select" name="feed_category_id" style="width: 350px;" required>
					   <cfoutput query="lens">
						<option value=#lens_id#>#lens_name#
					   </cfoutput>
					  </select></td><td><input type="text" class="input_text" name="feed_keywords" style="width: 350px;" required maxlength="100"></td>

					  </tr>

                   <tr><td height=20></td></tr>
				   <tr><td colspan=2><input type="submit" name="button" value="Post" class="button_blue_large"></td></tr>

			  </table>

		  </td></tr>

          </table>

 		  </form>



	      </div>

      </td><td valign=top width=185>

      <cfinclude template="column_right.cfm">

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>