<cfsetting RequestTimeout = "9000000">

<cfquery name="cb" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cb_people
 left join cb_people_descriptions on cb_people_descriptions.uuid = cb_people.uuid
</cfquery>

<cfset count = 0>

<cfloop query="cb">

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into person
	 (
	 person_company,
	 person_cb_company_id,
	 person_first_name,
	 person_last_name,
	 person_full_name,
	 person_title,
	 person_email,
	 person_phone,
	 person_cell,
	 person_twitter,
	 person_facebook,
	 person_linkedin,
	 person_photo,
	 person_desc,
	 person_gender,
	 person_country,
	 person_state,
	 person_city,
	 person_cb_id,
	 person_sams_duns,
	 person_source,
	 person_created,
	 person_updated,
	 person_hub_id
	 )
	 values
	 (
     '#cb.featured_job_organization_name#',
     '#cb.featured_job_organization_uuid#',
     '#cb.first_name#',
     '#cb.last_name#',
     '#cb.name#',
     '#cb.featured_job_title#',
     null,
     null,
     null,
     '#cb.twitter_url#',
     '#cb.facebook_url#',
     '#cb.linkedin_url#',
     '#cb.logo_url#',
     '#cb.description#',
     '#cb.gender#',
     '#cb.country_code#',
     '#cb.state_code#',
     '#cb.city#',
     '#cb.uuid#',
     null,
     'Crunchbase',
     #now()#,
     #now()#,
     1
     )
	</cfquery>

	<cfset count = count + 1>

</cfloop>

<cfoutput>
	users = #count#
</cfoutput>