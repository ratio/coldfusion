<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into person_rel
	 (
	  person_rel_person_id,
	  person_rel_usr_id,
	  person_rel_hub_id,
	  person_rel_type_id,
	  person_rel_org,
	  person_rel_comments,
	  person_rel_started,
	  person_rel_ended,
	  person_rel_created,
	  person_rel_updated
	 )
	 values
	 (
	 #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	 #session.usr_id#,
	 #session.hub#,
	 #person_rel_type_id#,
	'#person_rel_org#',
	'#person_rel_comments#',
	 <cfif person_rel_started is not "">'#person_rel_started#'<cfelse>null</cfif>,
	 <cfif person_rel_ended is not "">'#person_rel_ended#'<cfelse>null</cfif>,
	 #now()#,
	 #now()#
	 )
	 </cfquery>

</cftransaction>

<cflocation URL="relationships.cfm?i=#i#&u=1" addtoken="no">