<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete person_status
	 where person_status_person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		   person_status_hub_id = #session.hub#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into person_status
	 (
	  person_status_company,
	  person_status_alumni,
	  person_status_hub_id,
	  person_status_person_id
	 )
	 values
	 (
	 <cfif isdefined("person_status_company")>1<cfelse>null</cfif>,
	 <cfif isdefined("person_status_alumni")>1<cfelse>null</cfif>,
	 #session.hub#,
	 #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 )
	 </cfquery>

</cftransaction>

<cflocation URL="profile.cfm?i=#i#&u=6" addtoken="no">