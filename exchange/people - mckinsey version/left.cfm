<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfquery name="relcount" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(person_rel_id) as total from person_rel
 where person_rel_person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

  <cfoutput>

    <tr><td valign=top>

    <table cellspacing=0 cellpadding=0 width=0 width=100%>
     <tr><td class="text_xsmall" align=center>

             <cfif profile.person_photo is "">
             <img src="/images/private.png" width=225 height=225 onerror="this.onerror=null; this.src='/images/private.png'">
             <cfelse>

             <cfif findnocase("placeholder",profile.person_photo,1)>
             <img src="/images/private.png" width=225 height=225>
             <cfelse>
             <img src="#profile.person_photo#" width=225 height=225 onerror="this.onerror=null; this.src='/images/private.png'">
             </cfif>

             </cfif>

     </td></tr>

     <tr><td height=20></td></tr>

	 <tr><td align=center>
	 <cfif #profile.person_linkedin# is not "">
	  <a href="#profile.person_linkedin#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" alt="LinkedIn" title="LinkedIn" width=30 hspace=5 align=absmiddle></a>
	 </cfif>
	 <cfif #profile.person_facebook# is not "">
	  <a href="#profile.person_facebook#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" alt="Facebook" title="Facebook" width=30 hspace=5 align=absmiddle></a>
	 </cfif>
	 <cfif #profile.person_twitter# is not "">
	  <a href="#profile.person_twitter#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" alt="Twitter" title="Twitter" width=30 hspace=5 align=absmiddle></a>
	 </cfif>
	 </td></tr>

      <tr><td height=15></td></tr>

      <tr><td><hr></td></tr>
      <tr><td class="feed_sub_header">Company Relationships</td></tr>

      <tr><td class="feed_sub_header" style="font-weight: normal;">


      <a href="relationships.cfm?i=#i#" style="font-weight: normal;">
      <cfif relcount.total is 0>
       No relationships have been identifed.
      <cfelse>

      <cfif relcount.total is 1>
       Relationship Identified
      <cfelse>
       #relcount.total# Relationships Identified
      </cfif>

      </cfif>
      </a>

      </td></tr>


      <tr><td><hr></td></tr>
      <tr><td class="feed_sub_header">Contact Information</td></tr>
      <tr><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.person_email# is "">Not Provided<cfelse>#profile.person_email#</cfif></td></tr>
      <tr><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon-phone_2.png" alt="Work Phone" title="Work Phone" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.person_phone# is "">Not Provided<cfelse>#profile.person_phone#</cfif></td></tr>
	  <tr><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon_cell_phone.png" alt="Cell Phone" title="Cell Phone" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.person_cell# is "">Not Provided<cfelse>#profile.person_cell#</cfif></td></tr>

      <tr><td><hr></td></tr>
      <tr><td class="feed_sub_header">Location</td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">
	   <cfif profile.person_city is "" and profile.person_state is "">
	    Not Specified
	   <cfelse>
	   <cfif profile.person_city is "" and profile.person_state is 0>
	    Not Specified
	   <cfelse>
	   #profile.person_city# <cfif #profile.person_state# is not 0>#profile.person_state#</cfif>
	   </cfif>
	   </cfif>
	   </td></tr>

     </table>

    <table cellspacing=0 cellpadding=0 border=0 width=88%>


      <tr><td colspan=2><hr></td></tr>

      <tr><td class="feed_sub_header">Company Status</td></tr>

	   <tr><td class="feed_sub_header" style="font-weight: normal;">Currently at Company</td>
	       <td align=right>

	       <cfif status.person_status_company is 1>
	        <img src="/images/box_checked.png" width=20 style="margin-right: 10px;">
	       <cfelse>
	        <img src="/images/box_unchecked.png" width=20 style="margin-right: 10px;">
	       </cfif>

	   </td></tr>

	   <tr><td class="feed_sub_header" style="font-weight: normal;">Company Alumni</td>
	       <td align=right>

	       <cfif status.person_status_alumni is 1>
	        <img src="/images/box_checked.png" width=20 style="margin-right: 10px;">
	       <cfelse>
	        <img src="/images/box_unchecked.png" width=20 style="margin-right: 10px;">
	       </cfif>
	   </td></tr>



    </table>

  </cfoutput>
</table>