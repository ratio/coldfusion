<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Clear">

	<cfset StructDelete(Session,"people_search_keyword")>
	<cfset StructDelete(Session,"people_search_type")>
    <cflocation URL="index.cfm" addtoken="no">

<cfelse>

	<cfset search_string = #replace(people_search_keyword,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
	<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	<cfset search_string = #replace(search_string,'"(','("',"all")#>
	<cfset search_string = #replace(search_string,')"','")',"all")#>

	<cfset session.people_search_keyword = #search_string#>
	<cfset session.people_search_type = #people_search_type#>

	<cflocation URL="results.cfm" addtoken="no">

</cfif>

