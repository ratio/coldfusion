<cfinclude template="/exchange/security/check.cfm">

<cfset location = 2>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfset perpage = 100>

 <cfif session.people_search_type is 1>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

     select * from exchange.dbo.person
     left join seedspot.dbo.person_rel on seedspot.dbo.person_rel.person_rel_person_id = exchange.dbo.person.person_id
     left join seedspot.dbo.person_status on seedspot.dbo.person_status.person_status_person_id = exchange.dbo.person.person_id
     left join exchange.dbo.company on exchange.dbo.company.company_duns = exchange.dbo.person.person_sams_duns
	 where contains((person_first_name, person_last_name, person_full_name),'#session.people_search_keyword#')

     <cfif isdefined("session.filter_list")>
       and seedspot.dbo.person_rel.person_rel_type_id in (#session.filter_list#)
     </cfif>

     <cfif isdefined("session.alumni")>
       and seedspot.dbo.person_status.person_status_alumni = 1
     </cfif>

     <cfif sv is 1>
      order by person_last_name ASC
     <cfelseif sv is 10>
      order by person_last_name DESC
     <cfelseif sv is 2>
      order by person_first_name ASC
     <cfelseif sv is 20>
      order by person_first_name DESC
     <cfelseif sv is 3>
      order by person_title ASC
     <cfelseif sv is 30>
      order by person_title DESC
     <cfelseif sv is 4>
      order by person_company ASC
     <cfelseif sv is 40>
      order by person_company DESC
     </cfif>

    </cfquery>

 <cfelseif session.people_search_type is 2>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     select * from exchange.dbo.person
     left join seedspot.dbo.person_rel on seedspot.dbo.person_rel.person_rel_person_id = exchange.dbo.person.person_id
     left join seedspot.dbo.person_status on seedspot.dbo.person_status.person_status_person_id = exchange.dbo.person.person_id
     left join exchange.dbo.company on exchange.dbo.company.company_duns = exchange.dbo.person.person_sams_duns
	 where contains((person_title),'#session.people_search_keyword#')

     <cfif isdefined("session.filter_list")>
       and seedspot.dbo.person_rel.person_rel_type_id in (#session.filter_list#)
     </cfif>

     <cfif isdefined("session.alumni")>
       and seedspot.dbo.person_status.person_status_alumni = 1
     </cfif>

     <cfif sv is 1>
      order by person_last_name ASC
     <cfelseif sv is 10>
      order by person_last_name DESC
     <cfelseif sv is 2>
      order by person_first_name ASC
     <cfelseif sv is 20>
      order by person_first_name DESC
     <cfelseif sv is 3>
      order by person_title ASC
     <cfelseif sv is 30>
      order by person_title DESC
     <cfelseif sv is 4>
      order by person_company ASC
     <cfelseif sv is 40>
      order by person_company DESC
     </cfif>

    </cfquery>

 <cfelseif session.people_search_type is 3>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     select * from exchange.dbo.person
     join exchange.dbo.cb_organizations on exchange.dbo.cb_organizations.uuid = exchange.dbo.person.person_cb_company_id
     left join seedspot.dbo.person_rel on seedspot.dbo.person_rel.person_rel_person_id = exchange.dbo.person.person_id
     left join seedspot.dbo.person_status on seedspot.dbo.person_status.person_status_person_id = exchange.dbo.person.person_id
     left join exchange.dbo.company on exchange.dbo.company.company_duns = exchange.dbo.person.person_sams_duns
	 where contains((category_list, category_groups_list),'#session.people_search_keyword#')

     <cfif isdefined("session.filter_list")>
       and seedspot.dbo.person_rel.person_rel_type_id in (#session.filter_list#)
     </cfif>

     <cfif isdefined("session.alumni")>
       and seedspot.dbo.person_status.person_status_alumni = 1
     </cfif>

     <cfif sv is 1>
      order by person_last_name ASC
     <cfelseif sv is 10>
      order by person_last_name DESC
     <cfelseif sv is 2>
      order by person_first_name ASC
     <cfelseif sv is 20>
      order by person_first_name DESC
     <cfelseif sv is 3>
      order by person_title ASC
     <cfelseif sv is 30>
      order by person_title DESC
     <cfelseif sv is 4>
      order by person_company ASC
     <cfelseif sv is 40>
      order by person_company DESC
     </cfif>

    </cfquery>

 <cfelseif session.people_search_type is 4>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     select * from exchange.dbo.person
     left join seedspot.dbo.person_rel on seedspot.dbo.person_rel.person_rel_person_id = exchange.dbo.person.person_id
     left join seedspot.dbo.person_status on seedspot.dbo.person_status.person_status_person_id = exchange.dbo.person.person_id
     left join exchange.dbo.company on exchange.dbo.company.company_duns = exchange.dbo.person.person_sams_duns
	 where contains((person_company),'#session.people_search_keyword#')

     <cfif isdefined("session.filter_list")>
       and seedspot.dbo.person_rel.person_rel_type_id in (#session.filter_list#)
     </cfif>

     <cfif isdefined("session.alumni")>
       and seedspot.dbo.person_status.person_status_alumni = 1
     </cfif>

     <cfif sv is 1>
      order by person_last_name ASC
     <cfelseif sv is 10>
      order by person_last_name DESC
     <cfelseif sv is 2>
      order by person_first_name ASC
     <cfelseif sv is 20>
      order by person_first_name DESC
     <cfelseif sv is 3>
      order by person_title ASC
     <cfelseif sv is 30>
      order by person_title DESC
     <cfelseif sv is 4>
      order by person_company ASC
     <cfelseif sv is 40>
      order by person_company DESC
     </cfif>

    </cfquery>


 </cfif>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/people/filter.cfm">

       </td><td valign=top width=100%>

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">People</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right>
              </td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <cfinclude template="people_search.cfm">

          </div>

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Search Results -

	          <cfif agencies.recordcount is 0>
                No People Found
              <cfelseif agencies.recordcount is 1>
                1 Person Found
              <cfelse>
                <cfoutput>#numberformat(agencies.recordcount,'999,999')# People Found</cfoutput>
	          </cfif>

	          </td>
	          <td class="feed_sub_header" style="font-weight: normal;" align=right>

	          <cfoutput>

	           	    <cfif agencies.recordcount GT #perpage#>
						<b>Page #numberformat(thisPage,'99,999')# of #numberformat(totalPages,'99,999')#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

			    </cfoutput>

	          </td></tr>

	        <tr><td colspan=2><hr></td></tr>
          </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=10></td></tr>

          <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Last Name</b></a><cfif isdefined("sv") and sv is 1>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>First Name</b></a><cfif isdefined("sv") and sv is 2>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Title</b></a><cfif isdefined("sv") and sv is 3>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Company</b></a><cfif isdefined("sv") and sv is 4>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right>Social Links</td>
            <td></td>
         </tr>

         <cfset counter = 0>

		 <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=45>
         <cfelse>
          <tr bgcolor="e0e0e0" height=45>
         </cfif>

             <td class="feed_option" style="font-weight: normal;" width=60><a href="profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

             <cfif person_photo is "">
             <img src="/images/private.png" width=40>
             <cfelse>

             <cfif findnocase("placeholder",person_photo,1)>
             <img src="/images/private.png" width=40>
             <cfelse>
             <img src="#person_photo#" width=40>
             </cfif>

             </cfif>


             </a></td>

             <td class="feed_sub_header" style="font-weight: normal;" width=150><a href="profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#person_last_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=150><a href="profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#person_first_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=250>
             <cfif person_title is "">
              Unknown
             <cfelse>
             #person_title#
             </cfif>

             </td>
             <td class="feed_sub_header" style="font-weight: normal;" width=250>

             <cfif person_sams_duns is "">
             	<a href="get_comp.cfm?id=#person_cb_company_id#" target="_blank" rel="noopener" rel="noreferrer">#person_company#</a>
             <cfelse>
             	<a href="/exchange/include/federal_profile.cfm?duns=#company_duns#" target="_blank" rel="noopener" rel="noreferrer">#company_name#</a>
             </cfif>

             </td>

             <td class="feed_sub_header" style="font-weight: normal;" width=125 align=right>

             <cfif person_facebook is not ""><a href="#person_facebook#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=25 hspace=5></a></cfif>
             <cfif person_twitter is not ""><a href="#person_twitter#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=25 hspace=5></a></cfif>
             <cfif person_linkedin is not ""><a href="#person_linkedin#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=25 hspace=5></a></cfif>

             </td>

			<td width=50 align=right>
			<div class="dropdown">
			  <img src="/images/3dots2.png" style="cursor: pointer; margin-right: 10px;" height=6>
			  <div class="dropdown-content" style="width: 250px; text-align: left;">
				<a href="/exchange/include/save_person.cfm" onclick="window.open('/exchange/include/save_person.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=200, scrollbars=yes,resizable=yes,width=900,height=375'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
			  </div>
			</div>
			</td>


         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

        </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>