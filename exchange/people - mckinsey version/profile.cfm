<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="status" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from person_status
 where person_status_person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_person_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,#session.usr_id#, #session.company_id#,#session.hub#,#now()#)
</cfquery>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 20px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	display: inline-block;
	margin-left: -4px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	padding-right: 20px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}

.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/pportfolio/recent.cfm">

      </td><td valign=top>

          <cfoutput>

          <div class="tab_active">
           <span class="feed_header" valign=absmiddle><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<a href="profile.cfm?i=#i#">Profile</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<a href="relationships.cfm?i=#i#">Relationships</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><i class="fa fa-commenting" aria-hidden="true"></i></i>&nbsp;&nbsp;&nbsp;<a href="comments.cfm?i=#i#">Intel</a></span>
          </div>

          </cfoutput>

	  <div class="main_box_2">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=20></td></tr>

		   <tr><td valign=top width=20%>

		   <cfinclude template="left.cfm">

           </td><td width=40>&nbsp;</td><td valign=top width=100%>

           <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_header" style="font-size: 40px; padding-bottom: 0px;">#profile.person_first_name# #profile.person_last_name#</td>
			      <td class="feed_header" align=right>

			<td width=50 align=right>
			<div class="dropdown">
			  <img src="/images/3dots2.png" style="cursor: pointer;" height=8>
			  <div class="dropdown-content" style="width: 250px; text-align: left;">
				<a href="/exchange/include/save_person.cfm" onclick="window.open('/exchange/include/save_person.cfm?i=#encrypt(profile.person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=200, scrollbars=yes,resizable=yes,width=900,height=375'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
			  </div>
			</div>
			</td>
			      </td></tr>

           </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_sub_header" colspan=2 style="font-size: 24px;"><a href="get_comp.cfm?id=#profile.person_cb_company_id#" style="font-size: 24px;" target="_blank">#profile.person_company#</a></td></tr>
				<tr><td class="feed_sub_header">#profile.person_title#</td>
			    <td align=right>

					<div class="dropdown">
					<img src="/images/icon_edit.png" width=20 hspace=10><span class="feed_sub_header">Update Profile</span>
					  <div class="dropdown-content" style="height: 230px; width: 300px; top: 14; text-align: left; right: 10;">
						  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                          <form action="update.cfm" method="post">
                           <tr><td class="feed_sub_header" colspan=2>Update Profile</td></tr>
                           <tr><td colspan=2><hr></td></tr>

								   <tr>
								      <td class="feed_sub_header" width=200>Currently at Company</td>
								      <td><input type="checkbox" name="person_status_company" style="width: 20px; height: 20px; margin-right: 10px;" <cfif status.person_status_company is 1>checked</cfif>></td>
								   </tr>

								   <tr>
								      <td class="feed_sub_header">Current Alumni</td>
								      <td><input type="checkbox" name="person_status_alumni" style="width: 20px; height: 20px; margin-right: 10px;" <cfif status.person_status_alumni is 1>checked</cfif>></td>
								   </tr>

								   <cfoutput>
								   <input type="hidden" name="i" value=#i#>
								   </cfoutput>

                           <tr><td colspan=2><hr></td></tr>
                           <tr><td height=10></td></tr>
                           <tr><td colspan=2><input type="submit" name="button" class="button_blue" value="Update"></td></tr>
						   </form>
						  </table>

					  </div>
					</div>

				    </td>


				</tr>
				<tr><td height=10></td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><td height=10></td></tr>

                <cfif isdefined("u")>
                 <cfif u is 6>
                  <tr><td class="feed_sub_header" style="color: green;">Profile has been successfully updated.</td></tr>
                  <tr><td height=10></td></tr>
                 </cfif>
                </cfif>

				<tr><td class="feed_header">Snapshot</td></tr>
				<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#profile.person_desc#</td>



				</tr>
			  </table>

		   </cfoutput>

			  <cfquery name="jobs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select * from cb_jobs
			   left join cb_organizations on cb_organizations.uuid = org_uuid
			   where person_uuid = '#profile.person_cb_id#'
			   order by started_on DESC
			  </cfquery>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td height=10></td></tr>
				<tr><td class="feed_header">History</td></tr>
				<tr><td height=10></td></tr>

				<cfif jobs.recordcount is 0>
					<tr><td class="feed_sub_header" style="font-weight: normal;">No history found.</td></tr>
			    <cfelse>

			      <cfset count = 1>

                  <cfoutput query="jobs">
                   <tr>
                   <td width=70 valign=top>

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_sub_header"><a href="get_comp.cfm?id=#jobs.org_uuid#"><img src="#logo_url#" width=50 onerror="this.onerror=null; this.src='/images/stock_company.png'"></a></td></tr>
				      </table>

                   </td><td valign=top>

                   <!---

                   <cfif jobs.ended_on is "">
                   <cfset diff = #datediff('m',dateformat(jobs.started_on,'mm/dd/yyyy'),dateformat(now(),'mm/dd/yyyy'))#>
                   <cfelse>
                   <cfset diff = #datediff('m',dateformat(jobs.started_on,'mm/dd/yyyy'),dateformat(jobs.ended_on,'mm/dd/yyyy'))#>
                   </cfif> --->

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_sub_header" style="padding-bottom: 5px;"><a href="get_comp.cfm?id=#jobs.org_uuid#" style="font-size: 22px;" target="_blank">#jobs.org_name#</a> <cfif jobs.is_current is 'true'>( Current )</cfif></td>
                            <td class="feed_sub_header" style="padding-bottom: 5px;" align=right>#dateformat(jobs.started_on,'mmm dd, yyyy')# - <cfif jobs.ended_on is "">Current<cfelse>#dateformat(jobs.ended_on,'mmm dd, yyyy')#</cfif></td></tr>
                        <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#jobs.title#</td>
                            <td class="link_small_gray" style="font-weight: normal; padding-top: 0px;" align=right>

  <!---                          <cfif diff is 12>
                            1 Year
                            <cfelseif diff LT 12>
                             #diff# Months
                            <cfelse>
                             #numberformat(evaluate(diff/12),'99.9')# Years
                            </cfif> --->

                            </td></tr>

                        <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;">#jobs.short_description#</td></tr>
                        <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;">#jobs.category_groups_list#</td></tr>
                        <tr><td height=10></td></tr>


                        </tr>
                      </table>

                      </td></tr>

                  <cfif count LT jobs.recordcount>
                  <tr><td colspan=2><hr></td></tr>
                  </cfif>

                  <cfset count = count + 1>

                  </cfoutput>

			    </cfif>
			  </table>

            </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

