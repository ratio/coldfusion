<cfinclude template="/exchange/security/check.cfm">

<cfquery name="suppor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from support
 where support_cat_id = #support_cat_id#
 order by support_order
</cfquery>

<cfquery name="cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from support_cat
 where support_cat_id = #support_cat_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		   <tr><td class="feed_header">CUSTOMER SUPPORT</td></tr>
 		   <tr><td><hr></td></tr>
 		  </table>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td valign=top width="225">

		   <cfinclude template="/exchange/support/support_menu.cfm">

  		   </td><td valign=top width=60>&nbsp;</td><td valign=top>

 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		     <tr><td>&nbsp;</td></tr>

 		     <cfoutput>
				 <tr><td class="feed_header">#cat.support_cat_name#</td></tr>
					 <cfif #cat.support_cat_name# is not "">
						 <tr><td class="feed_sub_header" style="font-weight: normal;">#cat.support_cat_desc#</td></tr>
					 </cfif>
			     <tr><td><hr></td></tr>
             </cfoutput>

             <cfif #suppor.recordcount# is 0>
             <tr><td class="feed_option">No support information is available.</td></tr>
             <cfelse>
				 <cfoutput query="suppor">
				   <tr><td class="feed_sub_header">#support_name#</td></tr>
				   <tr><td class="feed_sub_header" style="font-weight: normal;">#replace(support_desc,"#chr(10)#","<br>","all")#</td></tr>
				   <tr><td><hr></td></tr>
				 </cfoutput>
             </cfif>

            </table>

  		   </td></tr>

 		  </table>

 		  <!--- End of the Workbench --->

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

