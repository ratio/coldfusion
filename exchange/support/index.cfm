<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		   <tr><td class="feed_header">Help & Support</td></tr>
 		   <tr><td><hr></td></tr>
 		  </table>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td valign=top width="225">

		   <cfinclude template="/exchange/support/support_menu.cfm">

  		   </td><td valign=top width=60>&nbsp;</td><td valign=top>

 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		     <tr><td>&nbsp;</td></tr>

             <cfif isdefined("u")>
              <tr><td class="feed_sub_header" style="color: green;"><font color="green"><b>Thank you for contacting Customer Support.  We have received your request and appreciate you taking the time to submit it.  We will respond within 24 hours.</font></td></tr>
              <tr><td height=10></td></tr>
             </cfif>

             <tr><td class="feed_header">Welcome to Customer Support</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;">We're here to help. If you have any questions please don't hestiate to contact us below.  If you can't find assistance here please access the Exchange Knowledge Center for more support topics.</td></tr>
             <tr><td height=10></td></tr>
            </table>

 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfquery name="support" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select * from hub
				 where hub_id = #session.hub#
				</cfquery>

				<tr><td valign=top>

	 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <cfoutput>
	             <tr><td class="feed_sub_header">Contact Us</td></tr>
	             <tr><td class="feed_sub_header"><img src="/images/icon-email_2.png" width=25>&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:#support.hub_support_email#"><u>#support.hub_support_email#</u></a></td></tr>
	             <tr><td class="feed_sub_header"><img src="/images/icon-email_2.png" width=25>&nbsp;&nbsp;&nbsp;&nbsp;#support.hub_support_phone#</td></tr>
                </cfoutput>

                </table>

                </td><td valign=top>

	 		    <cfif support.hub_library_id is 2>

					<table cellspacing=0 cellpadding=0 border=0 width=100%>

					<cfoutput>
					 <tr><td class="feed_sub_header" colspan=2>&nbsp;</td></tr>
					 <tr><td class="feed_sub_header"><a href="https://exchangeanswers.zendesk.com/hc/en-us" target="_blank" rel="noopener"><img src="/images/icon_knowledgebase.png" width=85 border=0 title="Knowledge Center" alt="Knowledge Center"></a></td>
						 <td class="feed_sub_header" style="font-weight: normal;" width=88%><a href="https://exchangeanswers.zendesk.com/hc/en-us" target="_blank" rel="noopener"><b>Exchange Knowledge Center</b></a><br>
						 <br>Ask questions, search for help, get insights and access our knowledge base for<br>more information about how to use and capitalize upon the Exchange.</td></tr>
					</cfoutput>

					</table>

                </cfif>

                </td></tr>


            </table>

            <form action="submit.cfm" method="post">

 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		     <tr><td>&nbsp;</td></tr>
             <tr><td class="feed_sub_header" colspan=2>Submit Feedback</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>We look forward to hearing from you.  If you have a question, comment, or suggestion, please fill out the information below and we'll get back to you as soon as possible.</td></tr>
             <tr><td height=10></td></tr>
             <tr><td class="feed_sub_header" width=135>Topic</td>
                 <td><select name="request_type" class="input_select">
                     <option value="General">General Question
                     <option value="Problem">Problem or Error
                     <option value="Request">Feature Request
                     <option value="Feedback" selected>Feedback
                     <option value="Other">Other
                     </td></tr>

             <tr><td height=10></td></tr>
             <tr><td class="feed_sub_header" valign=top>Message</td>
                 <td><textarea name="request_description" class="input_textarea" cols=100 rows=6 required></textarea></td></tr>
             <tr><td height=10></td></tr>
		     <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Submit"></td></tr>
             <tr><td>&nbsp;</td></tr>
            </table>

            </form>

  		   </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

