<cfif isdefined("session.hub")>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from support_cat
	 where support_cat_hub_id = #session.hub#
	 order by support_cat_order
	</cfquery>

<cfelse>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from support_cat
	 where support_cat_hub_id is null
	 order by support_cat_order
	</cfquery>

</cfif>

<cfif isdefined("support_cat_id")>
 <cfset #selected_cat# = #support_cat_id#>
</cfif>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td>&nbsp;</td></tr>
 <tr><td class="feed_header" colspan=2>Support Categories</td></tr>
 <tr><td height=20></td></tr>

     <tr><td valign=top width=20><cfif not isdefined("selected_cat")><img src="/images/icon_selected.png" height=20 width=5><cfelse>&nbsp;</cfif></td><td width=50 valign=top class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;"><a href="/exchange/support/"><a href="index.cfm">Home</a></td></tr>
     <tr><td height=5></td></tr>
     <tr><td></td><td><hr></td></tr>
     <tr><td height=5></td></tr>

     <cfset count = 1>

	 <cfloop query="cats">

		<cfquery name="support" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(support_id) as total from support
		 where support_cat_id = #cats.support_cat_id#
		</cfquery>

	  <cfoutput>

	  <tr>
	      <td valign=top width=20><cfif isdefined("selected_cat") and #selected_cat# is #cats.support_cat_id#><img src="/images/icon_selected.png" height=20 width=5><cfelse>&nbsp;</cfif></td>
	      <td class="feed_sub_header" valign=top style="padding-top: 0px; padding-bottom: 0px;"><a href="support.cfm?support_cat_id=#cats.support_cat_id#"><b>#cats.support_cat_name#</b></a></td></tr>

	  <cfif count is not cats.recordcount>
       <tr><td height=5></td></tr>
       <tr><td></td><td><hr></td></tr>
       <tr><td height=5></td></tr>
	  </cfif>

	  <cfset count = count + 1>

	 </cfoutput>


 </cfloop>

</table>