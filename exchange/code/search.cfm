<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from naics
 where naics_code_description like '%#keyword#%'
 order by naics_code_description
</cfquery>

<cfquery name="psc" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from psc
 where psc_description like '%#keyword#%'
 order by psc_description
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Add Federal Codes</td><td class="feed_option" align=right></td></tr>
         <tr><td height=10></td></tr>
        </table>

         <form action="search.cfm" method="post">
			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_option"><b>Search for Code</b>&nbsp;&nbsp;
				 <input type="text" name="keyword" size=30 required value="<cfoutput>#keyword#</cfoutput>">&nbsp;&nbsp;
				 <input class="button_blue" style="font-size: 11px; height: 22px; width: 65px;" type="submit" name="button" value="Search">
			 </td></tr>
             </table>
         </form>

      <!--- Display Results --->

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
      <tr><td>&nbsp;</td></tr>

        <!--- NAICS Results --->

        <tr><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" colspan=2>NAICS Code Description</td></tr>
         <tr><td height=10></td></tr>

         <cfif naics.recordcount is 0>
          <tr><td class="feed_option">No matching NAICS descriptions were found.</td></tr>
         <cfelse>
         <cfoutput query="naics">
          <tr>
             <td class="feed_option" valign=top width=40><input type="checkbox" name="value"></td>
             <td class="feed_option" valign=top>#replaceNoCase(ucase(naics_code_description),keyword,"<span style='background:yellow'>#ucase(keyword)#</span>","all")#</td>
          </tr>
         </cfoutput>
         </cfif>

         </table>

         </td><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" colspan=2>PSC Code Description</td></tr>
         <tr><td height=10></td></tr>

         <cfif psc.recordcount is 0>
          <tr><td class="feed_option">No matching PSC descriptions were found.</td></tr>
         <cfelse>
         <cfoutput query="psc">
          <tr>
             <td class="feed_option" valign=top width=40><input type="checkbox" name="value"></td>
             <td class="feed_option" valign=top>#replaceNoCase(psc_description,keyword,"<span style='background:yellow'>#ucase(keyword)#</span>","all")#</td>
          </tr>
         </cfoutput>
         </cfif>

         </table>

         </td>
       </tr>


      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>