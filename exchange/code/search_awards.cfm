<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select naics_code, naics_description, count(id) as awards, sum(federal_action_obligation) as total from award_data
 where contains((award_description),'"#keyword#"') and
 federal_action_obligation > 0
 group by naics_code, naics_description
 order by total DESC
</cfquery>

<cfquery name="psc" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(product_or_service_code), product_or_service_code_description, count(id) as awards, sum(federal_action_obligation) as total from award_data
 where contains((award_description),'"#keyword#"') and
 federal_action_obligation > 0
 group by product_or_service_code, product_or_service_code_description
 order by total DESC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Search Results</td><td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
         <tr><td height=5></td></tr>
         <tr><td class="feed_option">Displayed below are the NAICS and PSC codes and buying volume that matches the keyword you provided.
         To save this profile, please enter the profile name and select the appropriate codes below.</td></tr>
         <tr><td height=10></td></tr>
        </table>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr>
             <form action="search_awards.cfm" method="post">
			     <td class="feed_option"><b>Capability</b>&nbsp;&nbsp;
			     &nbsp;&nbsp;<input type="text" name="keyword" size=25 required value="<cfoutput>#keyword#</cfoutput>">
			     &nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 22px; width: 65px;" type="submit" name="button" value="Refresh">
			     </td>
			 </form>

             <form action="save_profile.cfm" method="post">
			     <td class="feed_option" align=right><b>Save profile as</b>&nbsp;&nbsp;
			     &nbsp;&nbsp;<input type="text" name="opp_profile_name" size=35 required>
			     &nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 22px; width: 65px;" type="submit" name="button" value="Save">
			     </td>
             <input type="hidden" name="opp_profile_keyword" value="<cfoutput>#keyword#</cfoutput>">

			 </tr>
             <tr><td height=10></td></tr>
			 <tr><td colspan=2><hr></td></tr>
             </table>

      <!--- Display Results --->

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
      <tr><td>&nbsp;</td></tr>

        <!--- NAICS Results --->

        <tr><td valign=top width=48%>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
             <td class="feed_sub_header" colspan=2>NAICS Code Description</td>
             <td class="feed_sub_header" align=center>Awards</td>
             <td class="feed_sub_header" align=right>Award $</td>

             </tr>
         <tr><td height=10></td></tr>

         <cfif naics.recordcount is 0>
          <tr><td class="feed_option">No matching NAICS descriptions were found.</td></tr>
         <cfelse>
         <cfoutput query="naics">
          <tr>
             <td class="feed_option" valign=top width=40><input type="checkbox" name="opp_profile_naics_list" style="width: 16px; height: 16px;" value="#naics_code#"></td>
             <td class="feed_option" valign=top>#replaceNoCase(ucase(naics_description),keyword,"<span style='background:yellow'>#ucase(keyword)#</span>","all")#</td>
             <td class="feed_option" valign=top align=center width=50>#numberformat(awards,'999,999')#</td>
             <td class="feed_option" valign=top align=right width=100>#numberformat(total,'$999,999,999')#</td>
          </tr>
         </cfoutput>
         </cfif>

         </table>

         </td><td valign=top><td width=40>&nbsp;</td><td valign=top width=48%>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" colspan=2>Product or Service Code Description</td>
             <td class="feed_sub_header" align=center>Awards</td>
             <td class="feed_sub_header" align=right>Award $</td>
         </tr>
         <tr><td height=10></td></tr>

         <cfif psc.recordcount is 0>
          <tr><td class="feed_option">No matching PSC descriptions were found.</td></tr>
         <cfelse>
         <cfoutput query="psc">
          <tr>
             <td class="feed_option" valign=top width=40><input type="checkbox" name="opp_profile_psc_list" style="width: 16px; height: 16px;" value="#product_or_service_code#"></td>
             <td class="feed_option" valign=top>#replaceNoCase(product_or_service_code_description,keyword,"<span style='background:yellow'>#ucase(keyword)#</span>","all")#</td>
             <td class="feed_option" valign=top align=center width=50>#numberformat(awards,'999,999')#</td>
             <td class="feed_option" valign=top align=right width=100>#numberformat(total,'$999,999,999')#</td>
          </tr>
         </cfoutput>
         </cfif>

         </table>

         </td>
       </tr>

       </form>


      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>