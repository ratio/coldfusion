<cfinclude template="/exchange/security/check.cfm">

<cfquery name="save" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into opp_profile
 (opp_profile_name,
  opp_profile_usr_id,
  opp_profile_company_id,
  opp_profile_hub_id,
  opp_profile_naics_list,
  opp_profile_psc_list)
 values
 ('#opp_profile_name#',
   #session.usr_id#,
   #session.company_id#,
   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  '#opp_profile_naics_list#',
  '#opp_profile_psc_list#'
 )
</cfquery>

<cflocation URL="index.cfm" addtoken="no">