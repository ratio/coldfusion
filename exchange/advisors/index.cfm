<cfinclude template="/exchange/security/check.cfm">

<cfquery name="advisors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from advisor
 join usr on usr_id = advisor_advisor_id
 where advisor_hub_id = #session.hub# and
       advisor_usr_id = #session.usr_id#
 order by usr_last_name
</cfquery>

<style>
.mentor_badge {
    width: 22%;
    display: inline-block;
    height: 275px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    background-color: #ffffff;
}
</style>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">My Advisors</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

		<cfoutput query="advisors">

		<div class="mentor_badge">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td height=5></td></tr>

		 <cfif #advisors.usr_photo# is "">
		  <tr><td align=center><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" width=200 height=200 border=0 title="Update Profile" alt="Update Profile"></a></td></tr>
		 <cfelse>
		  <tr><td align=center><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 180px;" src="#media_virtual#/#advisors.usr_photo#" width=200 height=200 border=0 title="Update Profile" alt="Update Profile"></a></td></tr>
		 </cfif>

	      <tr><td align=center class="feed_header" style="padding-top: 0px; padding-bottom: 0px;" valign=top><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#usr_first_name# #usr_last_name#</a></td></tr>
	      <tr><td height=5></td></tr>
	      <tr><td align=center class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; font-weight: normal;">#usr_company_name#</td></tr>
	      <tr><td height=5></td></tr>
	      <tr><td align=center class="link_small_gray" style="padding-top: 0px; padding-bottom: 0px;">#usr_title#</td></tr>

		</table>

		</div>

		</cfoutput>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>