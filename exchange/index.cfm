<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/images/exchange_ico.ico">

</head><div class="center">
<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 20px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	display: inline-block;
	margin-left: -4px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	padding-right: 20px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}

.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfset location = 'left'>
	  <cfinclude template="column_left.cfm">

      </td><td valign=top>

          <cfset sponsor = 0>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_home2.png" width=20 align=absmiddle>&nbsp;&nbsp;<a href="/exchange/">Welcome</a></span>
          </div>

		  <cfquery name="hasnews" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select hub_app_id from hub_app
           join app on app_id = hub_app_app_id
           where hub_app_hub_id = #session.hub# and
                 app_security_id = 'NEWSWIRE01'
          </cfquery>

          <cfif hasnews.recordcount GT 0>

          	<div class="tab_not_active">
          	 <span class="feed_sub_header"><img src="/images/icon_news_feed.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="/exchange/news.cfm">Newswire</a></span>
          	</div>

          </cfif>


          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="/exchange/my_feeds.cfm">Posts</a></span>
          </div>

		  <cfset location = 'center'>
		  <cfquery name="hp_center" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from home_section
			left join component on component_id = home_section_component_id
			where home_section_profile_id = #usr.hub_xref_role_id# and
			      home_section_hub_id = #session.hub# and
			      home_section_location = 'Center' and
			      home_section_active = 1
			  	  order by home_section_order
		  </cfquery>

          <cfif hp_center.recordcount is 0>

	      <div class="main_box_2">

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr>
               <td class="feed_sub_header" style="font-weight: normal;">No home page sections or content exists.</td>
            </tr>

           </table>
          </div>

          <cfelse>

          <cfset hp_center_counter = 1>

          <cfoutput query="hp_center">

          <cfif hp_center_counter is 1>
	          <div class="main_box_2">
          <cfelse>
	          <div class="main_box">
          </cfif>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #hp_center.home_section_display# is 1>

				<tr>
				   <td class="feed_header">#hp_center.home_section_name#</td>
				</tr>

				<cfif hp_center.home_section_desc is not "">

					<tr>
					   <td class="feed_sub_header" style="font-weight: normal;">#hp_center.home_section_desc#</td>
					</tr>

				</cfif>

				<tr><td><hr></td></tr>
				<tr><td height=10></td></tr>

            </cfif>

            <cfif hp_center.home_section_component_id is not 0>

            <tr>

               <td>
                  <cfinclude template="#hp_center.component_path#">
               </td>

            </tr>

            <cfelse>

            <tr>
               <td class="feed_sub_header" style="font-weight: normal;">#hp_center.home_section_content#</td>
            </tr>

            </cfif>

           <cfset hp_center_counter = hp_center_counter + 1>

           </table>

           </div>

          </cfoutput>

          </cfif>

      </td><td valign=top width=185>

      <cfset location = 'right'>

      <cfinclude template="column_right.cfm">

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>