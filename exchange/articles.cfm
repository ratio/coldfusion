<cfif not isdefined("session.news_time")>
 <cfset #session.news_time# = 0>
</cfif>

<cfif not isdefined("session.news_source")>
 <cfset #session.news_source# = 0>
</cfif>

<cfquery name="sources" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(newsfeed_source) from newsfeed
 where newsfeed_hub_id = #session.hub#
 order by newsfeed_source
</cfquery>

<cfquery name="news" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from newsfeed
 where newsfeed_hub_id = #session.hub#

 <cfif isdefined("session.news_keyword")>
  <cfif trim(session.news_keyword) is not "">
	 and contains((newsfeed_title, newsfeed_content, newsfeed_category),'#session.news_keyword#	')
  </cfif>
 </cfif>

 <cfif session.news_source is not 0>
  and newsfeed_source = '#session.news_source#'
 </cfif>

 <cfif #session.news_time# is not 0>

	   <cfif #session.news_time# is 1>

	   <cfset start_date = dateformat(now(),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
	   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

	   <cfelseif #session.news_time# is 2>

		   <cfset start_date = dateformat(dateadd('d','-7',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

		  <cfelseif #session.news_time# is 3>
		   <cfset start_date = dateformat(dateadd('d','-30',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

		  <cfelseif #session.news_time# is 4>
		   <cfset start_date = dateformat(dateadd('d','-90',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

	 </cfif>

	  and (newsfeed_import_date >= '#start_date#' and newsfeed_date <= '#end_date#')

 </cfif>

 order by newsfeed_date DESC

</cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt news.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(news.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <form action="/exchange/news_search.cfm" method="post">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><b>Newswire</b> keeps you updated about what's going on in the market.</td></tr>

       <tr><td height=10></td></tr>
       <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

			<input style="width: 300px;" class="input_text" type="text" name="news_keyword" placeholder="search news..." <cfif isdefined("session.news_keyword")>value="<cfoutput>#replace(session.news_keyword,'"','','all')#</cfoutput>"</cfif>>&nbsp;&nbsp;

			<select name="news_source" class="input_select" style="width:250px">
				<option value=0>All Sources
				<cfoutput query="sources">
				 <option value="#newsfeed_source#" <cfif session.news_source is #newsfeed_source#>selected</cfif>>#newsfeed_source#
				</cfoutput>
			</select>

            &nbsp;<b>Posted</b>&nbsp;&nbsp;
			<select name="news_time" class="input_select">
				<option value=0 <cfif session.news_time is 0>selected</cfif>>Show All
				<option value=1 <cfif session.news_time is 1>selected</cfif>>Today
				<option value=2 <cfif session.news_time is 2>selected</cfif>>This Week
				<option value=3 <cfif session.news_time is 3>selected</cfif>>This Month
				<option value=4 <cfif session.news_time is 4>selected</cfif>>This Quarter
			</select>

			<input class="button_blue" type="submit" name="button" value="Filter">&nbsp;
			<input class="button_blue" type="submit" name="button" value="Reset">&nbsp;
		   </td>

		   <td align=right class="feed_sub_header">

		   </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=3><hr></td></tr>

           <tr><td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">
           <cfif news.recordcount is 0>
             No articles were found.
           <cfelseif news.recordcount is 1>
            1 article was found.
           <cfelse>
            <cfoutput>#numberformat(news.recordcount,'99,999')# articles were found.</cfoutput>
           </cfif>
           </td>

           <td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;" align=right>
			<cfoutput>
				<cfif news.recordcount GT #perpage#>
					<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt news.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>
			</cfoutput>

           </td>
           </tr>
       </table>

       </form>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td>

<!--- Start Post Updates --->

<cfinclude template="updates.cfm">

<cfif news.recordcount is 0>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_sub_header" style="font-weight: normal;">To start receiving posts please ensure you have subscribed to one or more <a href="/exchange/feed/categories.cfm"><b>Categories</b>.</a></td></tr>
  </table>

<cfelse>

<cfoutput query="news" startrow="#url.start#" maxrows="#perpage#">

<div class="feed_box">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=10></td></tr>

   <tr><td valign=top width=75>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td style="padding-top: 5px;">

			<cfif news.newsfeed_source_logo is "">
				<a href="#news.newsfeed_url#" target="_blank"><img src="#image_virtual#/icon_rss.png" width=50 border=0></a>
			<cfelse>
				<a href="#news.newsfeed_url#" target="_blank"><img src="#media_virtual#/#news.newsfeed_source_logo#" width=50 border=0></a>
			</cfif>


			</td></tr>
	   </table>

   </td><td valign=top>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;"><a href="#news.newsfeed_url#" target="_blank">#news.newsfeed_title#</a></td>
             <td class="link_small_gray" style="font-weight: normal;" align=right>#dateformat(news.newsfeed_date,'mmm dd')#, #timeformat(news.newsfeed_date)#</td>
         </tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#news.newsfeed_content#</td></tr>

         <tr>
             <td class="link_small_gray" style="font-weight: normal;"><a href="#news.newsfeed_url#" target="_blank"><u>#news.newsfeed_url#</u></a></td>
             <td class="link_small_gray" style="font-weight: normal;" align=right>#news.newsfeed_category#</td>
         </tr>

	   </table>

   </td></tr>

  <tr><td height=10></td></tr>
  </table>

</div>

</cfoutput>

</cfif>

</td></tr>

</table>