<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfif isdefined("session.usr_id")>

  <cfquery name="error_name" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from usr
   where usr_id = #session.usr_id#
  </cfquery>

  <cfset usr_first_name = #error_name.usr_first_name#>
  <cfset usr_last_name = #error_name.usr_last_name#>
  <cfset usr_id = #error_name.usr_id#>

  <cfelse>

   <cfset usr_first_name = "Unknown">
   <cfset usr_last_nname = "User">
   <cfset usr_id = "Unknown">

  </cfif>

  <cfif isdefined("session.hub")>

  <cfquery name="hubinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select hub_name from hub
    where hub_id = #session.hub#
  </cfquery>

  <cfset exchange_name = #hubinfo.hub_name#>

  <cfelse>

   <cfset exchange_name = "Exchange">

  </cfif>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=30></td></tr>
           <tr>
               <td class="feed_header"></td>
               <td valign=top width=40>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                 <tr><td class="feed_header" style="font-size: 45px;">Unfortunately, an error has occured.</td></tr>
                 <tr><td height=10></td></tr>
                 <tr><td class="feed_sub_header" style="font-weight: normal;">We apologize for the error and have notified our Product Manager with the error details.   You should be hearing from us shortly with a timeline on when we will get this fixed.</td></tr>
	             <tr><td class="feed_sub_header" style="font-weight: normal;">If you'd like to speak to someone now, please contact us at one of the options below.   Again, we're sorry.</td></tr>
                 <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Customer Support</b><br>support@ratio.exchange<br>Phone - 1-540-407-9911</td></tr>

               </table>

               </td>
               </tr>

			<cfmail from="<noreply@ratio.exchange>"
					  to="jim@ratioim.com, jerry.mcquoid@ratioim.com, jeff.rose@ratioim.com"
			  username="noreply@ratio.exchange"
			  password="Gofus107!"
				  port="25"
				useSSL="false"
				type="html"
				server="mail.ratio.exchange"
			   subject="System Error">
			<html>
			<head>
			<title>System Message</title>
			</head><div class="center">
			<body class="body">
			<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">

			<tr><td class="feed_option">

			  <cfoutput>
				  <tr><td class="feed_option">
				  User Name: #usr_first_name# #usr_last_name#<br>
				  User: #usr_id#
				  Exchange / Network: #exchange_name#
				  <br>
				  <br>
                  Page: #error.template# <br>
				  #error.diagnostics#<br>
				  Date and time: #error.DateTime# <br>
				  Remote Address: #error.remoteAddress# <br>
				  HTTP Referer: #error.HTTPReferer#<br>
				  Browser: #error.browser#<br>
				  Content: #error.generatedcontent#<br>
				  Query String: #error.querystring#<br>
				  </td></tr>
				  <tr><td>&nbsp;<br></td></tr>
			  </cfoutput>

			</td></tr>
			</table>

			</body>
			</html>
			</cfmail>

          </table>

	  </div>

	  </td></tr>

  </table>

</body>
</html>