<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from service
 where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_header">Edit Service</td>
	       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>

           <form action="db.cfm" method="post" enctype="multipart/form-data">

           <cfoutput>

                <tr><td class="feed_sub_header" width=150>Sponsor Name</td>
                    <td><input type="text" class="input_text" name="service_name" value="#edit.service_name#" size=50 required></td></tr>

                <tr><td class="feed_sub_header" valign=top>Short Description</td>
                    <td><textarea name="service_short_desc" class="input_textarea" style="width: 800px; height: 100px;">#edit.service_short_desc#</textarea></td></tr>

                <tr><td class="feed_sub_header" valign=top>Long Description</td>
                    <td><textarea name="service_long_desc" class="input_textarea" style="width: 800px; height: 300px;">#edit.service_long_desc#</textarea></td></tr>

                <tr><td class="feed_sub_header" valign=top>Thank You Messasge</td>
                    <td><textarea name="service_thank_you" class="input_textarea" style="width: 800px; height: 200px;">#edit.service_thank_you#</textarea></td></tr>

                <tr><td></td>
                    <td class="link_small_gray">Will be displayed after a user emails you about the Service.</td>
                </tr>

                <tr><td class="feed_sub_header">Email</td>
                    <td><input type="email" class="input_text" name="service_email" style="width: 300px;" value="#edit.service_email#">

                    <span class="link_small_gray">Email address for questions about this Service.</span>

                    </td></tr>

				<tr><td class="feed_sub_header">URL</td>
                    <td><input name="service_url" type="url" class="input_text" style="width: 300px;" value="#edit.service_url#">

                    <span class="link_small_gray">External URL for more information about this Service.</span>

                    </td></tr>

				<tr><td class="feed_sub_header">Image</td>

					<td class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.service_image# is "">
					  <input type="file" name="service_image">
					<cfelse>
					  View Current Logo: <a href="#media_virtual#/#edit.service_image#" target="_blank" rel="noopener" rel="noreferrer">#edit.service_image#</a><br><br>
					  <input type="file" name="service_image"><br><br>
					  <input type="checkbox" name="remove_attachment">&nbsp;or, check to remove logo
					 </cfif>


					 </td></tr>

				<tr><td class="feed_sub_header">Order</td>
			        <td><input type="number" name="service_order" class="input_text" required style="width: 100px;" value=#edit.service_order#>

			        <span class="link_small_gray">Determines the order the Service will be displayed when viewing all Services.</span>

			        </td></tr>

				<tr><td class="feed_sub_header">Active</td>
			        <td>
			        <select name="service_active" class="input_select">
			         <option value=1 <cfif edit.service_active is 1>selected</cfif>>Yes
			         <option value=0>No
			        </select>

			        <span class="link_small_gray">Determines whether Service will be displayed.</span>

			        </td></tr>

  			    <tr><td height=5></td></tr>
  			    <tr><td colspan=2><hr></td></tr>
  			    <tr><td height=5></td></tr>

   			    <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
              			         <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Sponsor.\r\nAre you sure you want to delete this sponsor?');">&nbsp;&nbsp;
   			            </td></tr>



			   <input type="hidden" name="i" value=#i#>

               </cfoutput>

			   </form>

	  </table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>