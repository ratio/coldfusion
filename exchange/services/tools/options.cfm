<cfquery name="services" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from service
 where service_hub_id = #session.hub#
 order by service_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header">Services Configuration</td>
      <td class="feed_sub_header" align=right><a href="/exchange/admin/apps/">Apps</a></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td height=10></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr>

    <td class="feed_sub_header">

     <cfif isdefined("u")>
      <cfif u is 1>
       <span style="color: green;">Services has been successfully added.</span>
      <cfelseif u is 2>
       <span style="color: green;">Service has been successfully updated.</span>
      <cfelseif u is 3>
       <span style="color: red;">Service has been successfully deleted.</span>
      </cfif>
     </cfif>

    </td>
    <td class="feed_sub_header" align=right><a href="add.cfm"><img src="/images/plus3.png" width=15 hspace=10></a><a href="add.cfm">Add Service</a></td></tr>

</table>

<cfif services.recordcount is 0>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <tr><td class="feed_sub_header" style="font-weight: normal;">No Services have been added.</td></tr>
	</table>

<cfelse>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr>
		 <td></td>
		 <td class="feed_sub_header">Service</td>
		 <td class="feed_sub_header">Short Description</td>
		 <td class="feed_sub_header" align=center>Order</td>
		 <td class="feed_sub_header" align=right>Active</td>
	 </tr>

    <cfset counter = 0>
	<cfloop query="services">

		<cfoutput>

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=60>
         <cfelse>
          <tr bgcolor="e0e0e0" height=60>
         </cfif>

			 <td class="feed_sub_header" width=70><a href="edit.cfm?i=#encrypt(service_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif services.service_image is ""><img src="#image_virtual#/icon_services.png" height=40 width=40><cfelse><img src="#media_virtual#/#services.service_image#" width=40 height=40 hspace=5></cfif></td>
			 <td class="feed_sub_header" width=300><a href="edit.cfm?i=#encrypt(service_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#services.service_name#</a></td>
			 <td class="feed_sub_header" style="font-weight: normal;">#services.service_short_desc#</td>
			 <td class="feed_sub_header" style="font-weight: normal;" align=center>#services.service_order#</td>
			 <td class="feed_sub_header" style="font-weight: normal;" align=right width=75><cfif #services.service_active# is 1>Yes<cfelse>No</cfif></td>
		 </tr>

		 <cfif counter is 0>
		  <cfset counter = 1>
		 <cfelse>
		  <cfset counter = 0>
		 </cfif>

		</cfoutput>

	</cfloop>

</cfif>

</table>

