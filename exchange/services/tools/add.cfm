<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_header">Add Service</td>
	       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>

           <form action="db.cfm" method="post" enctype="multipart/form-data">

                <tr><td class="feed_sub_header" width=150>Sponsor Name</td>
                    <td><input type="text" class="input_text" name="service_name" size=50 required></td></tr>

                <tr><td class="feed_sub_header" valign=top>Short Description</td>
                    <td><textarea name="service_short_desc" class="input_textarea" style="width: 800px; height: 100px;"></textarea></td></tr>

                <tr><td class="feed_sub_header" valign=top>Long Description</td>
                    <td><textarea name="service_long_desc" class="input_textarea" style="width: 800px; height: 300px;"></textarea></td></tr>

                <tr><td class="feed_sub_header" valign=top>Thank You Messasge</td>
                    <td><textarea name="service_thank_you" class="input_textarea" style="width: 800px; height: 200px;"></textarea></td></tr>

                <tr><td></td>
                    <td class="link_small_gray">Will be displayed after a user emails you about the Service.</td>
                </tr>

                <tr><td class="feed_sub_header">Email</td>
                    <td><input type="email" class="input_text" name="service_email" style="width: 300px;">

                    <span class="link_small_gray">Email address for questions about this Service.</span>

                    </td></tr>

				<tr><td class="feed_sub_header">URL</td>
                    <td><input name="service_url" type="url" class="input_text" style="width: 300px;">

                    <span class="link_small_gray">External URL for more information about this Service.</span>

                    </td></tr>

				<tr><td class="feed_sub_header">Image</td>
			        <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="service_image">



			        </td></tr>

				<tr><td class="feed_sub_header">Order</td>
			        <td><input type="number" name="service_order" class="input_text" required style="width: 100px;">

			        <span class="link_small_gray">Determines the order the Service will be displayed when viewing all Services.</span>

			        </td></tr>

				<tr><td class="feed_sub_header">Active</td>
			        <td>
			        <select name="service_active" class="input_select">
			         <option value=1>Yes
			         <option value=0>No
			        </select>

			        <span class="link_small_gray">Determines whether Service will be displayed.</span>

			        </td></tr>

  			    <tr><td height=5></td></tr>
  			    <tr><td colspan=2><hr></td></tr>
  			    <tr><td height=5></td></tr>
   			    <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add" vspace=10>
   			            </td></tr>

			   </form>

	  </table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>