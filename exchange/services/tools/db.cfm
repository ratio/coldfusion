<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

<cfif #service_image# is not "">
 <cffile action = "upload"
   fileField = "service_image"
   destination = "#media_path#"
   nameConflict = "MakeUnique">
</cfif>

<cfquery name="sponsor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into service
 (
 service_active,
 service_name,
 service_email,
 service_order,
 service_hub_id,
 service_image,
 service_short_desc,
 service_long_desc,
 service_thank_you,
 service_url,
 service_created,
 service_updated,
 service_usr_id
 )
 values
 (
 #service_active#,
 '#service_name#',
 '#service_email#',
  #service_order#,
  #session.hub#,
  <cfif #service_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
 '#service_short_desc#',
 '#service_long_desc#',
 '#service_thank_you#',
 '#service_url#',
 #now()#,
 #now()#,
 #session.usr_id#
 )
</cfquery>

<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select service_image from service
		  where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		        service_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.service_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.service_image#">
        </cfif>

	</cfif>

	<cfif #service_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select service_image from service
		  where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.service_image# is not "">
	        <cfif fileexists("#media_path#\#getfile.service_image#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.service_image#">
            </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "service_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update service
	  set service_name = '#service_name#',

		  <cfif #service_image# is not "">
		   service_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   service_image = null,
		  </cfif>

         service_short_desc = '#service_short_desc#',
         service_long_desc = '#service_long_desc#',
         service_thank_you = '#service_thank_you#',
         service_active = #service_active#,
         service_email = '#service_email#',
         service_order = #service_order#,
         service_url = '#service_url#'
       where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select service_image from service
	  where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cfif remove.service_image is not "">
        <cfif fileexists("#media_path#\#remove.service_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.service_image#">
	    </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete service
	  where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>