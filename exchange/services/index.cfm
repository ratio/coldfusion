<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

<cfset app_security_id = "Service123">

<cfinclude template = "/exchange/include/app_info.cfm">

<cfquery name="services" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from service
 where service_hub_id = #session.hub# and
       service_active = 1
 order by service_order
</cfquery>

<style>
.service_badge {
    width: 47%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 225px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  	<tr><td class="feed_header">#app_info.menu_name#</td>
		  	    <td class="feed_sub_header">&nbsp;</td></tr>
		  	<tr><td colspan=2><hr></td></tr>
		  	<tr><td height=10></td></tr>

		  	<cfif app_info.menu_desc is not "">
		  	 <tr><td class="feed_sub_header" style="font-weight: normal;">#app_info.menu_desc#</td></tr>
		  	</cfif>

		  </table>

		  </cfoutput>

			<cfif services.recordcount is 0>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <tr><td class="feed_sub_header" style="font-weight: normal;">No Services have been added.</td></tr>
				</table>

			<cfelse>
			<br>

			    <cfloop query="services">

			    <div class="service_badge">
			     <cfinclude template="badge.cfm">
			    </div>

			    </cfloop>

			</cfif>

		</table>

		</div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>