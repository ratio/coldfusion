<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

<cfset app_security_id = "Service123">

<cfinclude template = "/exchange/include/app_info.cfm">

<cfquery name="service" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from service
 where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  	<tr><td class="feed_header">#app_info.menu_name#</td>
		  	    <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		  	<tr><td colspan=2><hr></td></tr>
		  	<tr><td height=10></td></tr>

		  	<cfif app_info.menu_desc is not "">
		  	 <tr><td class="feed_sub_header" style="font-weight: normal;">#app_info.menu_desc#</td></tr>
		  	</cfif>

		  </table>

		  </cfoutput>

		  <cfoutput query="service">

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td valign=top width=130>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <cfif #service.service_image# is "">
				  <tr><td align=center valign=top width=110 style="padding-right: 20px;"><img src="#image_virtual#/icon_services.png" height=110 width=110 border=0 vspace=15></td></tr>
				 <cfelse>
				  <tr><td align=center valign=top width=110 style="padding-right: 20px;"><img style="border-radius: 180px;" vspace=15 src="#media_virtual#/#service.service_image#" height=110 width=110 border=0></td></tr>
				 </cfif>

				 <tr><tr>

				 </table>

			<td valign=top>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td height=15></td></tr>
			 <tr><td class="feed_header" style="padding-top: 0px; padding-bottom: 0px;">#service.service_name#</a></td>
				 <td align=right valign=top></td></tr>

			 <tr><td height=10></td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#service.service_short_desc#</td></tr>
             <tr><td class="feed_sub_header">Description of Service</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#replace(service.service_long_desc,"#chr(10)#","<br>","all")#</td></tr>

             <cfif service.service_url is not "">
              <tr><td class="feed_sub_header">More Information</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><a style="font-weight: normal;" href="#service.service_url#"><u>#service.service_url#</u></a></td></tr>
             </cfif>

             <tr><td>

			 <form action="submit.cfm" method="post">
			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td><hr></td></tr>
			  <tr><td class="feed_sub_header">Contact Us</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">If you have any questions or are interested in this Service please contact us below.</td></tr>
			  <tr><td><textarea name="service_message" style="width: 800px; height: 100px;" class="input_textarea" required></textarea></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td><input type="submit" name="button" class="button_blue_large" value="Submit"></td></tr>
			 </table>
			 <cfoutput>
			  <input type="hidden" name="i" value=#i#>
			 </cfoutput>

			 </form>

             </td></tr>


			</table>

			</td></tr>

			</table>

		  </cfoutput>

		</table>

		</div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>