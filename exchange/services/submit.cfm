<cfinclude template="/exchange/security/check.cfm">

<cfquery name="from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="service" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from service
 where service_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<!--- Email Service Owner --->

<cfmail from="#h_info.hub_name# <noreply@ratio.exchange>"
		  to="#service.service_email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="Service Interest">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>You have received the following message about a Service on your Exchange.</b></td></tr>
 <tr><td height=20></td></tr>
 <tr><td><b>Date</b> - #dateformat(now(),'mmm dd, yyyy')# at #timeformat(now())#</td></tr>
 <tr><td><b>User</b> - #from.usr_first_name# #from.usr_last_name#</td></tr>
 <tr><td><b>Email</b> - #tostring(tobinary(from.usr_email))#</td></tr>
 <tr><td><b>Phone</b> - #from.usr_phone#</td></tr>
 <tr><td height=10></td></tr>
 <tr><td><b>Service</b></td></tr>
 <tr><td>#service.service_name#</td></tr>
 <tr><td height=10></td></tr>
 <tr><td><b>Message</b></td></tr>
 <tr><td>#service_message#</td></tr>

</table>

</cfoutput>

</body>
</html>

</cfmail>

<!--- Email User --->

<cfmail from="#h_info.hub_name# <noreply@ratio.exchange>"
		  to="#tostring(tobinary(from.usr_email))#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="Service Question Received">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>Hi #from.usr_first_name# #from.usr_last_name#</b></td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">Thank you for sending your question about our #service.service_name# Service.  We have received your question and will contact you as soon as possible to discuss.</td></tr>
 <tr><td height=30></td></tr>
 <tr><td><b>Thank You</b></td></tr>
 <tr><td height=30></td></tr>
 <tr><td>#h_info.hub_support_name#</td></tr>
 <tr><td>#h_info.hub_support_phone#</td></tr>
 <tr><td>#h_info.hub_support_email#</td></tr>

</table>

</cfoutput>

</body>
</html>

</cfmail>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

<cfset app_security_id = "Service123">

<cfinclude template = "/exchange/include/app_info.cfm">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  	<tr><td class="feed_header">#app_info.menu_name#</td>
		  	    <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		  	<tr><td colspan=2><hr></td></tr>
		  	<tr><td height=10></td></tr>

		  	<cfif app_info.menu_desc is not "">
		  	 <tr><td class="feed_sub_header" style="font-weight: normal;">#app_info.menu_desc#</td></tr>
		  	</cfif>

		  </table>

		  </cfoutput>

		  <cfoutput query="service">

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td valign=top width=130>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <cfif #service.service_image# is "">
				  <tr><td align=center valign=top width=110 style="padding-right: 20px;"><img src="#image_virtual#/icon_services.png" height=110 width=110 border=0 vspace=15></td></tr>
				 <cfelse>
				  <tr><td align=center valign=top width=110 style="padding-right: 20px;"><img style="border-radius: 180px;" vspace=15 src="#media_virtual#/#service.service_image#" height=110 width=110 border=0></td></tr>
				 </cfif>

				 <tr><tr>

				 </table>

			<td valign=top>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td height=15></td></tr>
			 <tr><td class="feed_header" style="padding-top: 0px; padding-bottom: 0px;">#service.service_name#</a></td>
				 <td align=right valign=top></td></tr>

			 <tr><td height=10></td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#service.service_short_desc#</td></tr>
             <tr><td class="feed_sub_header">Description of Service</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#service.service_long_desc#</td></tr>

             <cfif service.service_url is not "">
              <tr><td class="feed_sub_header">More Information</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><a style="font-weight: normal;" href="#service.service_url#"><u>#service.service_url#</u></a></td></tr>
             </cfif>

             <tr><td>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td><hr></td></tr>
			  <tr><td class="feed_sub_header">Thank You</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">
			  <cfoutput>
			   #replace(service.service_thank_you,"#chr(10)#","<br>","all")#
			  </cfoutput></td></tr>
			 </table>

             </td></tr>


			</table>

			</td></tr>

			</table>

		  </cfoutput>

		</table>

		</div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>