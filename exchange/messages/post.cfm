<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfif #message_file_name# is not "">
		<cffile action = "upload"
		 fileField = "message_file_name"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="update_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update message_read
	 set message_read_date = #now()#
	 where message_read_usr_id = #session.usr_id# and
	       message_read_message_id = #decrypt(thread,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="update_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update message_read
	 set message_read_date = null
	 where message_read_usr_id <> #session.usr_id# and
	       message_read_message_id = #decrypt(thread,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="update_3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update message
	 set message_updated = #now()#
	 where message_id = #decrypt(thread,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into message
	 (
	 message_message,
	 message_file_type,
	 message_file_name,
	 message_from_id,
	 message_date,
	 message_reply_to_id
	 )
	 values
	 (
	 '#message_message#',

	 <cfif isdefined("message_file_name")>'#message_file_type#'</cfif>,

  	 <cfif #message_file_name# is not "">
      '#cffile.serverfile#',
     <cfelse>
       null,
      </cfif>

	  #session.usr_id#,
	  #now()#,
	  #decrypt(thread,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 )
	</cfquery>

	<cfquery name="subject" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from message
	 where message_id = #decrypt(thread,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfquery name="from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="to" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from message_read
	 join usr on usr_id = message_read_usr_id
	 where message_read_message_id = #decrypt(thread,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	 message_read_usr_id <> #session.usr_id#
	</cfquery>

	<cfif isdefined("test_email")>
	 <cfset to_email = #test_email#>
	<cfelse>
	 <cfset to_email = #decrypt(to.usr_email,session.key, "AES/CBC/PKCS5Padding", "HEX")#>
	</cfif>

	<cfmail from="BPN Message <noreply@ratio.exchange>"
			  to="#to_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#to.usr_first_name#, you received a message from #from.usr_first_name# #from.usr_last_name#">
	<html>
	<head><cfoutput>
	<title>Booz Allen Partner Network</title>
	</head></cfoutput>
	<body class="body">

	<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>Hi #to.usr_first_name#,</b></td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">You received a message from #from.usr_first_name# #from.usr_last_name#<cfif from.usr_company_name is not "">, from #from.usr_company_name#,</cfif> through the #h_info.hub_name#.  To view or respond to this message, please login.</td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>Subject</b></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#subject.message_subject#</td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#message_message#</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td><a href="#h_info.hub_login_page#"><img src="#image_virtual#/email_login.png" height=30></a></td></tr>
	</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>


</cftransaction>

<cflocation URL="index.cfm?thread=#thread#&u=2" addtoken="no">
