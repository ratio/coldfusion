<cfinclude template="/exchange/security/check.cfm">

<html>
<head><cfoutput>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head></cfoutput><div class="center">
<body class="body">

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 250px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}

.message {
    width: auto;
    height: auto;
    z-index: 100;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 0px;
    padding-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    background-color: #ffffff;
}

.thread_box {
    width: auto;
    height: auto;
    z-index: 100;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 10px;
    padding-bottom: 10px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    border-color: #b0b0b0;
    border-width: thin;
    border-left: 0px;
    border-top: 0px;
    border-right: 0px;
    border-bottom: 0px;
	border-style: solid;
    background-color: #ffffff;
}

.message_box {
    width: auto;
    height: auto;
    z-index: 100;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 0px;
    padding-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    border-radius: 0px;
    border-width: thin;
    border-top: 0px;
    border-right: 0px;
    border-bottom: 0px;
    border-left: 1px;
	border-color: gray;
	border-style: solid;
    background-color: #FFFFFF;
}
.message_text {
    font-family: calibri, arial;
    font-size: 16px;
    border: 0px solid gray;
    border-radius: 2px;
    width: 100%;
    height: 200px;
    margin-top: 5px;
    margin-bottom: 10px;
    padding-left: 15px;
    padding-right: 15px;
    outline: none !important;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
    curson: pointer;
    color: #000000;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
	  <td valign=top width=185>
      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfif usr.hub_xref_role_id is 1>
	      <cfinclude template="/exchange/portfolio/recent.cfm">
      <cfelse>
	      <cfinclude template="/exchange/profile_company.cfm">
      </cfif>
      </td>

      <td valign=top width=100%>

	  <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_header">New Message</td>
		    <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	    <tr><td colspan=2><hr></td></tr>
	    <tr><td height=10></td></tr>
	   </table>

		<cfquery name="hub_children" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from hub
		 where hub_parent_hub_id = #session.hub#
		</cfquery>

		<cfif hub_children.recordcount is 0>
		 <cfset hub_list = 0>
		<cfelse>
		 <cfset hub_list = valuelist(hub_children.hub_id)>
		</cfif>

 	   <cfquery name="network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select distinct(usr_id), usr_first_name, usr_last_name from usr_follow
	     join usr on usr_id = usr_follow_usr_follow_id
	     join hub_xref on hub_xref_usr_id = usr_id
	     where usr_follow_usr_id = #session.usr_id# and
        (usr_follow_hub_id = #session.hub# or usr_follow_hub_id in (#hub_list#))
	     order by usr_last_name
	   </cfquery>

       <form action="new_post.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif network.recordcount is 0>

	        <tr><td class="feed_sub_header" style="font-weight: normal;">You are not connected with anyone.  To create a message you must be connected to one or more people within the <a href="/exchange/marketplace/people"><b>Network</b></a>.</td></tr>

        <cfelse>

			<tr><td class="feed_sub_header" style="font-weight: normal;" width=25><b>To</b></td>

			<td>

			<select name="message_to_id" class="input_select">
			<cfoutput query="network">
			 <option value=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#usr_last_name#, #usr_first_name#
			</cfoutput>

			</td></tr>
			<tr><td height=10></td></tr>

		   </table>

		   <div class="message">
				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td colspan=2><textarea class="message_text" name="message_message" required placeholder="Write a Message..."></textarea></td></tr>
				</table>
			</div>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=10></td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><b>Attach</b>&nbsp;&nbsp;

		   <input type="file" name="message_file_name">

		   <b>Type (if attached):</b> &nbsp;&nbsp;&nbsp;

		   <input type="radio" name="message_file_type" value="Image" checked>&nbsp;&nbsp;Image&nbsp;&nbsp;
		   <input type="radio" name="message_file_type" value="Document">&nbsp;&nbsp;Document&nbsp;&nbsp;

		   </td></tr>
		   <tr><td><hr></td></tr>
		   <tr><td style="padding-top: 5px; padding-bottom: 10px;"><input type="submit" name="button" value="Post" class="button_blue_large"></td></tr>

       </cfif>

       </table>

       </form>

     </td>

     <td valign=top>

      <cfinclude template="/exchange/network.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
     </td>

     </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

