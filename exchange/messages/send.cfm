<cfquery name="message" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from message
 join usr on usr_id = message_from_id
 where message_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfif user.usr_company_id is 0 or user.usr_company_id is "">
 <cfset comp = "No Company">
<cfelse>
 <cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company
  where company_id = #user.usr_company_id#
 </cfquery>
 <cfset comp = #comp.company_name#>
</cfif>

<cftransaction>

	<cfquery name="send" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into message
	 (
	  message_from_id,
	  message_to_id,
	  message_date,
	  message_to_email,
	  message_subject,
	  message_message,
	  message_reply_to_id
	  )
	  values
	  (#session.usr_id#,
	   #message.message_from_id#,
	   #now()#,
	  '#message.usr_email#',
	  '#message_subject#',
	  '#message_message#',
	   #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	  )
	</cfquery>

</cftransaction>


<cfmail from="Exchange <noreply@ratio.exchange>"
		  to="#message.usr_email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="#message.usr_first_name#, you received a message from #user.usr_first_name# #user.usr_last_name#">
<html>
<head><cfoutput>
<title>#session.network_name#</title>
</head></cfoutput>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>Hi #message.usr_first_name#,</b></td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">You received a message from #user.usr_first_name# #user.usr_last_name#<cfif comp is not "No Company">, from #comp#,</cfif> through the Exchange.  To view or respond to this message, please login to the Exchange.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>Subject</b></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#message_subject#</td></tr>
 <tr><td>&nbsp;</td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<cflocation URL="index.cfm?u=1" addtoken="no">