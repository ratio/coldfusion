<cfinclude template="/exchange/security/check.cfm">

<cfquery name="message" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_profile_display, message_id, company_id, usr_photo, usr_first_name, usr_last_name, usr_id, company_name, message_date, message_subject, message_message from message
 join usr on usr_id = message_from_id
 left join company on company_id = usr_company_id
 where message_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="read" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update message
 set message_read = #now()#
 where message_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<html>
<head><cfoutput>
	<title>#session.network_name#</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head></cfoutput><div class="center">
<body class="body" onload='setFocusToTextBox()'>

<script>
function setFocusToTextBox(){
document.getElementById('message_box').focus();
}
</script>

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 250px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
	  <td valign=top width=185>
      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      </td>

      <td valign=top width=100%>

	  <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_header">MESSAGE</td>
		    <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
		<tr><td colspan=2><hr></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td height=20></td></tr>

         <form action="send.cfm" method="post">
		 <cfoutput>

		 <tr><td valign=top width=100>

	     <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td>

			 <cfif message.usr_profile_display is 2>
				  <img style="border-radius: 150px;" src="/images/headshot.png" width=80 height=80 border=0 alt="Private Profile" vspace=5  title="Private Profile">
			 <cfelse>
				 <cfif #message.usr_photo# is "">
				  <img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=80 height=80 border=0 alt="#message.usr_first_name# #message.usr_last_name#" title="#message.usr_first_name# #message.usr_last_name#">
				 <cfelse>
				  <img style="border-radius: 150px;" vspace=5  src="#media_virtual#/#message.usr_photo#" width=80 height=80 border=0 alt="#message.usr_first_name# #message.usr_last_name#" title="#message.usr_first_name# #message.usr_last_name#">
				 </cfif>
			 </cfif>
		  </td></tr>

	     </table>

	     </td><td valign=top>
	     <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td class="feed_sub_header" style="padding-bottom: 0px; padding-top: 10px;">#message.usr_first_name# #message.usr_last_name#</a></td>
		      <td class="feed_sub_header" align=right></td></tr>
		  <tr><td class="feed_sub_header" style="padding-bottom: 0px; padding-top: 0px; font-weight: normal; font-weight: normal;" colspan=2><a style="font-weight: normal;" href="/exchange/include/company_profile.cfm?id=#message.company_id#" target="_blank" rel="noopener" rel="noreferrer">#message.company_name#</a></td></tr>
		  <tr><td class="feed_sub_header"><input type="text" style="font-weight: normal; width: 1000px;" name="message_subject" class="input_text" value= "re:  #message.message_subject#"></td>
		      <td class="feed_sub_header" align=right>
		      </td></tr>
	     </table>
	     </td></tr>
         <tr><td colspan=2><hr></td></tr>
		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><textarea id="message_box" style="width: 1100; height: 300px;" name="message_message" class="input_textarea">#chr(10)# #chr(10)##chr(10)# #chr(10)#Original Message (#dateformat(message.message_date,'mmm dd, yyyy')# at #timeformat(message.message_date)#) #chr(10)# #chr(10)# #message.message_message#</textarea></td></tr>
		 <tr><td colspan=2><input type="submit" name="button" value="Send" class="button_blue_large">&nbsp;&nbsp;
		         <input type="submit" name="button" value="Cancel" class="button_blue_large">
		 </td></tr>
		 <input type="hidden" name="i" value=#i#>
		 </cfoutput>
		 </form>

	   </table>

     </div>


     </td>

     </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

