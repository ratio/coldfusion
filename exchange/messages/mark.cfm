<cfinclude template="/exchange/security/check.cfm">

<cfif a is 1>

	<cfquery name="mark" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update message_read
	 set message_read_date = #now()#
	 where message_read_message_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       message_read_usr_id = #session.usr_id#
	</cfquery>

<cfelse>

	<cfquery name="mark" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update message_read
	 set message_read_date = null
	 where message_read_message_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       message_read_usr_id = #session.usr_id#
	</cfquery>

</cfif>

<cfif a is 1>
	<cflocation URL="index.cfm?u=5" addtoken="no">
<cfelse>
	<cflocation URL="index.cfm?u=6" addtoken="no">
</cfif>