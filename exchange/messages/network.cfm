<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr_follow
  join usr on usr_id = usr_follow_usr_follow_id
  left join company on company_id = usr_company_id
  where usr_follow_usr_id = #session.usr_id#
 </cfquery>

<div class="right_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_header" colspan=2><a href="/exchange/marketplace/people/set.cfm?v=1">MY NETWORK</a></td></tr>
            <tr><td colspan=3><hr></td></tr>
            <cfoutput>
            <tr><td class="feed_option" colspan=2><b>

				 <cfif #network.recordcount# is 0>
				  	No Connections
				 <cfelseif #network.recordcount# is 1>
				  	1 Connection
				 <cfelseif #network.recordcount# GT 1>
				    <cfoutput>
				 	#network.recordcount# Connections
				 	</cfoutput>
				 </cfif>

				 </b></td></tr>

			</cfoutput>

			<tr><td height=10></td></tr>

			 <cfset counter = 1>

			 <cfoutput query="network">

			 <tr height=50>
             <td width=55 valign=absmiddle>

			     <cfif usr_profile_display is 2>
					  <img style="border-radius: 150px;" src="/images/headshot.png" width=37 height=37 border=0 alt="Private Profile" vspace=5  title="Private Profile">
			     <cfelse>
					 <cfif #usr_photo# is "">
					  <a href="/exchange/marketplace/people/profile.cfm?member=#usr_id#"><img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 <cfelse>
					  <a href="/exchange/marketplace/people/profile.cfm?member=#usr_id#"><img style="border-radius: 150px;" vspace=5  src="#media_virtual#/#usr_photo#" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 </cfif>
				 </cfif>
			  </td>

			  <td><span class="feed_sub_header"><a href="/exchange/marketplace/people/profile.cfm?member=#usr_id#">#usr_first_name# #usr_last_name#</a></span><br><span class="feed_option">#company_name#</span></td>
             </tr>

             <cfif counter is #network.recordcount#>
             <cfelse>
             <tr><td colspan=2><hr></td></tr>
             <cfset counter = counter + 1>
             </cfif>

			 </cfoutput>

		</table>

    </td></tr>

</table>

</div>