<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.image {
	width: 600px;
}
.message_scroll_box {
    height: 797px;
    padding-right: 0px;
    background-color: ffffff;
    overflow:auto;
}
.message_scroll_box_2 {
    height: 522px;
    padding-right: 10px;
    background-color: ffffff;
    overflow:auto;
}
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    messages. inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 250px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}

.message {
    width: auto;
    height: auto;
    z-index: 100;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 0px;
    padding-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    background-color: #ffffff;
}
.thread_box {
    width: auto;
    height: 800px;
    z-index: 100;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 00px;
    padding-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    border-width: thin;
    border-left: 1px;
    border-top: 1px;
    border-right: 1px;
    border-bottom: 1px;
	border-style: solid;
    background-color: #ffffff;
    border-color: gray;
}
.message_box {
    width: auto;
    height: 800px;
    z-index: 100;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 0px;
    padding-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    border-radius: 0px;
    border-width: thin;
    border-top: 1px;
    border-right: 1px;
    border-bottom: 1px;
    border-left: 1px;
	border-style: solid;
    background-color: #FFFFFF;
    border-color: gray;
}
.message_text {
    font-family: calibri, arial;
    font-size: 16px;
    border: 0px solid gray;
    border-radius: 2px;
    width: 100%;
    height: 125px;
    margin-top: 0px;
    margin-bottom: 10px;
    padding-left: 20px;
    padding-right: 20px;
    outline: none !important;
    padding-top: 5px;
    padding-bottom: 10px;
    background-color: #ffffff;
    curson: pointer;
    color: #000000;
}

/* Style The Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  right: 10;
  font-size: 13px;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 100px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
</style>

<script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
$(document).ready(function() {
    $('#chat-scroll').animate({
        scrollTop: $('#message_scroll_box_2').get(0).scrollHeight
    }, 2000);
});
</script>

<cfif not isdefined("session.message_filter")>
 <cfset session.message_filter = 1>
</cfif>

<cfquery name="user_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select distinct(message_from_id) as id from message
where message_display_id = #session.usr_id# and
message_from_id <> #session.usr_id# and
message_hub_id = #session.hub#
union
select distinct(message_to_id) as id from message
where message_display_id = #session.usr_id# and
message_to_id <> #session.usr_id# and
message_hub_id = #session.hub#
</cfquery>

<cfquery name="message_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select message_display_id, max(message_id) as message_id, max(message_date) from message
  join usr a on a.usr_id = message_to_id
  join usr b on b.usr_id = message_from_id
  where (message_from_id = #session.usr_id# or message_to_id = #session.usr_id#) and
        message_view_id = #session.usr_id# and
        message_hub_id = #session.hub#
  <cfif isdefined("session.message_keyword")>
   and (message_message like '%#session.message_keyword#%' or
       a.usr_full_name like '%#session.message_keyword#%' or
       b.usr_full_name like '%#session.message_keyword#%')
  </cfif>

  group by message_display_id
  order by message_id DESC
</cfquery>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
	  <td valign=top width=185>
      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td>

      <td valign=top width=100%>

	  <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_header">Messaging</td></tr>
		<tr><td colspan=2><hr></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header">

             <cfif isdefined("u")>
              <cfif u is 1>
               <span style="color: green;">Message has been successfully saved.</span>
              <cfelseif u is 4>
               <span style="color: red;">Message(s) have been successfully deleted.</span>
              <cfelseif u is 5>
               <span style="color: green;">Message marked as read.</span>
              <cfelseif u is 6>
               <span style="color: green;">Message marked as unread.</span>
              <cfelseif u is 7>
               <span style="color: green;">Message has been removed.</span>
              </cfif>
             </cfif>

              <td align=right class="feed_sub_header"></td></tr>

         <cfif message_list.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">You have not posted or received any messages.</td></tr>
          <tr><td height=10></td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">
          <input type="submit" name="button" value="Create Message" class="button_blue_large" onclick="window.location = 'new.cfm'"></td></tr>
         <cfelse>
          <tr><td height=10></td></tr>
		 </cfif>

	   </table>

       <cfif message_list.recordcount GT 0>

       <cfset counter = 1>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>

         <td width=32% valign=top>

         <div class="thread_box">

         <div class="message_scroll_box">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr>
           <form action="set_keyword.cfm" method="post">
			   <td colspan=3 valign=middle>
			   &nbsp;&nbsp;<img src="/images/icon_search.png" width=20 vspace=10>
				&nbsp;&nbsp;<input type="text" name="message_keyword" class="message_text" style="margin-left: 0px; padding-left: 0px; margin-top: 0px; margin-bottom: 0px; padding-bottom: 0px; font-size: 18px; padding-top: 0px; height: 40px; width: 175px;" placeholder="Search messages">
			   </td>
               </form>
               <td align=right class="feed_sub_header" style="padding-right: 10px;">
                <cfif isdefined("session.message_keyword")><a href="clear.cfm" style="padding-right: 10px; color: gray;">X</a></cfif>
				<input type="submit" name="button" value="New" class="button_blue" onclick="window.location='new.cfm'">
               </td>
           </tr>
           </table>

		   <cfloop query="message_list">

		    <cfquery name="messages" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		      select * from message
		      join usr on usr_id = message_display_id
		      where message_id = #message_list.message_id#
            </cfquery>

           <cfif not isdefined("i")>
            <cfset i = #encrypt(messages.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>
           </cfif>

           <cfif #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# is #messages.usr_id#>
            <cfset bgcolor = "e0e0e0">
            <cfset bar = 1>
           <cfelse>
            <cfset bgcolor = "ffffff">
            <cfset bar = 0>
           </cfif>

		   <cfoutput>

		   <table cellspacing=0 cellpadding=0 width=100% style="border-bottom: 1px solid gray; background-color: #bgcolor#; border-top: 1px solid gray; background-color: #bgcolor#;">

		   <tr><td <cfif bar is 1>bgcolor="435177"</cfif> style="padding-right: 3px; margin-left: 0px;"></td><td width=30>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
  		    <tr><td style="padding-left: 15px; padding-right: 15px;">

			 <a href="index.cfm?i=#encrypt(messages.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
			 <cfif messages.usr_profile_display is 2>
				  <img style="border-radius: 150px;" src="/images/headshot.png" width=60 height=60 border=0 alt="Private Profile" vspace=5  title="Private Profile">
			 <cfelse>
				 <cfif #messages.usr_photo# is "">
				  <img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=60 height=60 border=0 alt="#messages.usr_first_name# #messages.usr_last_name#" title="#messages.usr_first_name# #messages.usr_last_name#">
				 <cfelse>
				  <img style="border-radius: 150px;" vspace=5  src="#media_virtual#/#messages.usr_photo#" width=60 height=60 border=0 alt="#messages.usr_first_name# #messages.usr_last_name#" title="#messages.usr_first_name# #messages.usr_last_name#">
				 </cfif>
			 </cfif>
			 </a>
		  </td></tr>

	     </table>

	     </td><td valign=top>

	     <table cellspacing=0 cellpadding=0 border=0 width=100% style="padding-right: 15px;">

			  <tr><td height=15></td></tr>
			  <tr>
			      <td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;">

			      <cfif #messages.message_read# is 1>
			      	<a href="index.cfm?i=#encrypt(messages.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" style="font-weight: normal;">#messages.usr_first_name# #messages.usr_last_name#</a>
			      <cfelse>
			      	<a href="index.cfm?i=#encrypt(messages.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#messages.usr_first_name# #messages.usr_last_name#</a>
			      </cfif>

				  </td>
			      <cfif #messages.message_read# is 1>
			      	<td align=right class="link_small_gray" style="font-size: 16px;">#dateformat(messages.message_date,'mmm dd')#</td>
			      <cfelse>
			      	<td align=right class="link_small_gray" style="font-size: 16px;"><b>#dateformat(messages.message_date,'mmm dd')#</b></td>
			      </cfif>
			  </tr>
 		      <tr>
			      <cfif #messages.message_read# is 1>
	 		          <td colspan=2 class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px; font-weight: normal;">
	 		      <cfelse>
	 		          <td colspan=2 class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;">
	 		      </cfif>
	 		          <cfif #messages.message_from_id# is #session.usr_id#>You:<cfelse>#messages.message_from_first_name#:</cfif>
	 		          <cfif len(messages.message_message) GT 45>#left(messages.message_message,45)#...<cfelse>#messages.message_message#</cfif>
 		          </td>
 		      </tr>
			  <tr><td height=15></td></tr>

	     </table>
	     </td></tr>

		 </cfoutput>

		 </cfloop>

    	 </table>

   		 <table cellspacing=0 cellpadding=0 width=100% border=0>
   		  <tr><td class="feed_sub_header" style="font-weight: normal; padding-left: 20px;">There are no more messsages.</td></tr>
   		 </table>


		</div>

         </td><td width=68% valign=top>

         <div class="message_box">

         <!--- Profile --->

		<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
         select * from usr
         where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
        </cfquery>

           <cfoutput>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td width=100>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
	        <tr><td height=20></td></tr>
  		    <tr><td style="padding-left: 15px; padding-right: 15px;">

			 <cfif usr.usr_profile_display is 2>
				  <img style="border-radius: 150px;" src="/images/headshot.png" width=65 height=65 border=0 alt="Private Profile" vspace=5  title="Private Profile">
			 <cfelse>
				 <cfif #usr.usr_photo# is "">
				  <img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=65 height=65 border=0>
				 <cfelse>
				  <img style="border-radius: 150px;" vspace=5 src="#media_virtual#/#usr.usr_photo#" width=65 height=65 border=0>
				 </cfif>
		     </cfif>

		     </td></tr>

		    </table>

		    </td><td>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td height=5></td></tr>
			  <tr>
			      <td class="feed_sub_header" style="padding-left: 0px; padding-top: 0px; padding-bottom: 5px;">#usr.usr_first_name# #usr.usr_last_name#</td>
                  <td align=right>
					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer; padding-right: 15px;" height=8>
					  <div class="dropdown-content" style="width: 175px; text-align: left;">
						<a href="delete_thread.cfm?i=#i#" onclick="return confirm('Delete Thread?\r\nDeleting the thread will remove all messages and cannot be undone.  Are you sure you want to continue?');"><i class="fa fa-fw fa-trash"></i>&nbsp;&nbsp;Delete</a>
					  </div>
					</div>
				  </td>
			  </tr>

			  <tr>
			      <td class="link_small_gray" style="padding-left: 0px; padding-top: 0px; padding-bottom: 10px;">#usr.usr_title#, #usr.usr_company_name#</td>
			  </tr>

			</table>

			</td></tr>

	     <tr><td colspan=2><hr></td></tr>

    	 </table>

		 </cfoutput>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		       <form action="new_post.cfm" method="post" enctype="multipart/form-data" >
				   <tr><td colspan=3><textarea class="message_text" style="padding-top: 10px; width: auto; height: 75px;" required name="message_message" placeholder="Write a message to <cfoutput>#usr.usr_first_name#</cfoutput>..."></textarea></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td></td>
                       <td class="link_small_gray" valign=top><span style="padding-left: 15px; padding-right: 10px;">Attach</span>

					   <input type="file" name="message_file_name">

                       Type (if attached): &nbsp;&nbsp;&nbsp;

					   <input type="radio" name="message_file_type" value="Image" checked>&nbsp;&nbsp;Image&nbsp;&nbsp;
					   <input type="radio" name="message_file_type" value="Document">&nbsp;&nbsp;Document&nbsp;&nbsp;

						</td>

				       <td align=right style="padding-top: 0px; padding-right: 10px; padding-bottom: 0px;"><input type="submit" name="button" value="Post" class="button_blue"></td></tr>

                   <cfoutput>
                   <input type="hidden" name="message_to_id" value=#i#>
                   </cfoutput>

                   </form>
				   <tr><td colspan=3><hr></td></tr>

       <tr><td colspan=3>

       <!--- Messages --->

		<cftransaction>

			<cfquery name="mess" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from message
			 join usr on usr_id = message_from_id
			 where (message_to_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# or
					message_from_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#)
			 and message_view_id = #session.usr_id#
			 and message_hub_id = #session.hub#
			 order by message_date DESC
			</cfquery>

			<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update message
			 set message_read = 1
			 where (message_to_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# or
					message_from_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#)
			 and message_view_id = #session.usr_id#
			 and message_hub_id = #session.hub#
			</cfquery>

        </cftransaction>

	         <div class="message_scroll_box_2">

             <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfoutput query="mess">

             <tr><td width=70 valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td height=10></td></tr>
				<tr><td style="padding-left: 25px; padding-right: 15px;">

				 <cfif mess.usr_profile_display is 2>
					  <img style="border-radius: 150px;" src="/images/headshot.png" width=50 height=50 border=0 alt="Private Profile" vspace=5 title="Private Profile">
				 <cfelse>
					 <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(mess.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
					 <cfif #mess.usr_photo# is "">
					  <img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=50 height=50 border=0 alt="#mess.usr_first_name# #mess.usr_last_name#" title="#mess.usr_first_name# #mess.usr_last_name#">
					 <cfelse>
					  <img style="border-radius: 150px;" vspace=5 src="#media_virtual#/#mess.usr_photo#" width=50 height=50 border=0 alt="#mess.usr_first_name# #mess.usr_last_name#" title="#mess.usr_first_name# #mess.usr_last_name#">
					 </cfif>
					 </a>
				 </cfif>

				 </td>
				 </tr>

				</table>

		    </td><td>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				  <tr><td height=5></td></tr>
				  <tr>
					  <td class="feed_sub_header" style="padding-left: 0px; padding-top: 10px; padding-bottom: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(mess.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#mess.usr_first_name# #mess.usr_last_name#</a></td>
					  <td align=right class="link_small_gray" style="font-size: 15px; padding-right: 10px;">#dateformat(message_date,'mmm dd')#, #timeformat(message_date)#</td>
				  </tr>
				  <tr>
				     <td class="link_small_gray" style="padding-top: 5px;">
				     <cfif #mess.usr_title# is not "">
				      #mess.usr_title#
				      </cfif>
				      <cfif #mess.usr_company_name# is not "">
				      , #mess.usr_company_name#
				      </cfif></td></tr>

				  <cfif mess.message_file_type is "">
					  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#replace(mess.message_message,"#chr(10)#","<br>","all")#</td></tr>
				  <cfelseif message_file_type is "Image">
					  <cfif mess.message_file_name is not "">
						  <tr><td height=20></td></tr>
						  <tr><td colspan=2><img src="#media_virtual#/#mess.message_file_name#" class="image"></td></tr>
						  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#replace(mess.message_message,"#chr(10)#","<br>","all")#</td></tr>
					  <cfelse>
						  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#replace(mess.message_message,"#chr(10)#","<br>","all")#</td></tr>
					  </cfif>
				  <cfelseif mess.message_file_type is "Document">
					  <cfif mess.message_file_name is not "">
						  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#replace(mess.message_message,"#chr(10)#","<br>","all")#</td></tr>
						  <tr><td colspan=2 class="link_small_gray" style="font-weight: normal; padding-right: 20px;">Download Attachment: <a href="#media_virtual#/#mess.message_file_name#" target="_blank" rel="noopener" rel="noreferrer"><u>#message_file_name#</u></a></td></tr>
					  <cfelse>
						  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#replace(mess.message_message,"#chr(10)#","<br>","all")#</td></tr>
					  </cfif>
				  </cfif>

                  <!---
				  <tr>
					  <td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 5px;">#replace(message_message,"#chr(10)#","<br>","all")#</td>
				  </tr> --->

				</table>

		    </td></tr>
	        <tr><td height=10></td></tr>
	        <tr><td colspan=2><hr></td></tr>

		    </cfoutput>

		   </table>

         </div>

          </td></tr>

        </table>



       </div>

       </td></tr>

       </table>



       </cfif>

     </div>

     </div>

     </td>

     <td valign=top>

      <cfinclude template="/exchange/components/my_network/index.cfm">

     </td>

     </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

