<cfquery name="get_tour" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_role
 where hub_role_id = #usr.hub_xref_role_id#
</cfquery>

<cfif get_tour.hub_role_tour_id is not "">

    <cfset tour_id = #get_tour.hub_role_tour_id#>

    <cfoutput>

		<script src="//app.helphero.co/embed/#tour_id#"></script>

		<script>
		  HelpHero.anonymous();
		</script>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td height=5></td></tr>
		 <tr><td align=right class="feed_sub_header" style="padding-bottom: 7px;"><a href="##" onclick="HelpHero.startTour('#tour_id#')">Take a Tour</a><a href="##" onclick="HelpHero.startTour('#tour_id#')"><img src="/images/icon_tour_site.png" width=20 hspace=5></a></td></tr>
		</table>

	</cfoutput>

<cfelse>
	<table>
	 <tr><td height=40>&nbsp;</td></tr>
	</table>
</cfif>

  <cfquery name="hp_right" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from home_section
	left join component on component_id = home_section_component_id
	where home_section_profile_id = #usr.hub_xref_role_id# and
		  home_section_hub_id = #session.hub# and
		  home_section_location = 'Right' and
		  home_section_active = 1
		  order by home_section_order
  </cfquery>

  <cfif hp_right.recordcount is 0>

  <div class="right_box">

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <tr><td class="feed_sub_header" style="font-weight: normal;">No home page right sections exist.</td></tr>
	</table>

  </div>

  <cfelse>

  <cfoutput query="hp_right">

	<cfif hp_right.home_section_component_id is not 0>
	   <cfinclude template="#hp_right.component_path#">
	<cfelse>

  <div class="right_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif #hp_right.home_section_display# is 1>

			<tr>
			   <td class="feed_header">#hp_right.home_section_name#</td>
			</tr>

			<tr><td><hr></td></tr>

			<cfif hp_right.home_section_desc is not "">

				<tr>
				   <td class="feed_sub_header" style="font-weight: normal;">#hp_right.home_section_desc#</td>
				</tr>

			</cfif>

		</cfif>

		<tr>
		   <td class="feed_sub_header" style="font-weight: normal;">#hp_right.home_section_content#</td>
		</tr>

	   </table>

	   </cfif>

	   </div>

	  </cfoutput>

  </cfif>