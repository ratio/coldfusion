<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset enddate = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>

<cfif not isdefined("sv")>
 <cfset sv=2>
</cfif>

<cfquery name="qual" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from qual
 where qual_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="contract" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select top(1) * from award_data
 where award_id_piid = '#qual.qual_contract#'
</cfquery>

<cfquery name="related" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select awarding_sub_agency_code, awarding_sub_agency_name, count(distinct(award_id_piid)) as contracts, sum(federal_action_obligation) as total from award_data
 where (period_of_performance_current_end_date > '#enddate#' and
       naics_code = '#contract.naics_code#' and
       product_or_service_code = '#contract.product_or_service_code#' and
       award_id_piid <> '#qual.qual_contract#')

    <cfif trim(qual.qual_keywords) is not "">
	 and (contains((*),'#trim(qual.qual_keywords)#'))
	</cfif>

 group by awarding_sub_agency_code, awarding_sub_agency_name

 <cfif sv is 1>
  order by awarding_sub_agency_name ASC
 <cfelseif sv is 10>
  order by awarding_sub_agency_name DESC
 <cfelseif sv is 2>
  order by total DESC
 <cfelseif sv is 20>
  order by total ASC
  <cfelseif sv is 3>
   order by contracts DESC
  <cfelseif sv is 30>
  order by contracts ASC
 </cfif>

</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

	  </td><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Cross Selling Opportunities</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">The below results represent current contracts that are similar to the Contract number you entered.  To fine tune the results, modify your keywords.</td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif related.recordcount is 0>

            <tr><td class="feed_sub_header" style="font-weight: normal;">No similar contracts were found.</td></tr>

            <cfelse>


            <tr><td colspan=3>

            <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr>
                  <td class="feed_sub_header" style="font-weight: normal;">
                  <b>Source Contract:</b>&nbsp;#qual.qual_contract#,&nbsp;&nbsp;
                  <b>NAICS Code:</b>&nbsp;#contract.naics_code#,&nbsp;&nbsp;
                  <b>PSC Code:</b>&nbsp;#contract.product_or_service_code#
                  </td>

                  <td class="feed_sub_header" style="font-weight: normal;" align=right>
                  <form action="refresh.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
                  <b>Keywords:</b>&nbsp;

                  <input type="text" class="input_text" name="qual_keywords" style="width: 250px;" value="#replace(qual.qual_keywords,'"','','all')#">

                  &nbsp;

                  <input type="submit" name="button" value="Refresh" class="button_blue">
                  <input type="hidden" name="i" value=#i#>
                  </form>
                  </td>

                  </tr>

			  </table>

			</cfoutput>



            </td></tr>


            <cfset counter = 0>

            <cfoutput>
            <tr>
               <td class="feed_sub_header"><a href="run_qual.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Agency</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
               <td class="feed_sub_header" align=center><a href="run_qual.cfm?i=#i#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Contracts</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
               <td class="feed_sub_header" align=right><a href="run_qual.cfm?i=#i#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Amount</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            </tr>
            </cfoutput>

           <cfoutput query="related">

           <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">
           </cfif>

               <td class="feed_sub_header"><a href="qual_contracts.cfm?i=#i#&code=#related.awarding_sub_agency_code#">#related.awarding_sub_agency_name#</a></td>
               <td class="feed_sub_header" align=center>#contracts#</td>
               <td class="feed_sub_header" align=right width=200>#numberformat(related.total,'$999,999,999')#</td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

           </cfif>

          </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

