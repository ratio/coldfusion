<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(qual_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update qual
 set qual_keywords = '#search_string#'
 where qual_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cflocation URL="run_qual.cfm?i=#i#" addtoken="no">
