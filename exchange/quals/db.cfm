<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(qual_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into qual (qual_contract, qual_keywords, qual_desc, qual_usr_id, qual_name, qual_hub_id)
	 values ('#qual_contract#','#search_string#','#qual_desc#',#session.usr_id#,'#qual_name#',#session.hub#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update qual
	 set qual_name = '#qual_name#',
	     qual_keywords = '#search_string#',
	     qual_desc = '#qual_desc#',
         qual_contract = '#qual_contract#'
	 where qual_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       qual_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete qual
	 where qual_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       qual_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

