<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="quals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from qual
  where qual_usr_id = #session.usr_id# and
        qual_hub_id = #session.hub#
  order by qual_name
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Cross Selling Reports</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';">Add Report</a></td></tr>
	          <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Cross Selling allows you to enter in a contract number and look for similar contracts across the Federal Government.</td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif quals.recordcount is 0>

            <tr><td class="feed_sub_header" style="font-weight: normal;">No Quals have been created.</td></tr>

           <cfelse>

           <tr>
              <td class="feed_sub_header">Report Name</td>
              <td class="feed_sub_header">Contract</td>
              <td class="feed_sub_header">Keywords</td>
              <td class="feed_sub_header">Description</td>
           </tr>

           <cfset counter = 0>

            <cfoutput query="quals">

            	<cfif counter is 0>
            	 <tr bgcolor="ffffff">
            	<cfelse>
            	 <tr bgcolor="e0e0e0">
            	</cfif>

            		<td class="feed_sub_header" width=200><a href="run_qual.cfm?i=#encrypt(qual_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';">#qual_name#</a></td>
            		<td class="feed_sub_header" style="font-weight: normal;">#qual_contract#</td>
            		<td class="feed_sub_header" style="font-weight: normal;"><cfif qual_keywords is "">No Keywords Used<cfelse>#replace(qual_keywords,'"','','all')#</cfif></td>
            		<td class="feed_sub_header" style="font-weight: normal;"><cfif qual_desc is "">No Description Provided<cfelse>#qual_desc#</cfif></td>
            		<td class="feed_sub_header" align=right><a href="edit.cfm?i=#encrypt(qual_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/icon_edit.png" width=20 hspace=10 border=0 alt="Edit" title="Edit"></a><a href="edit.cfm?i=#encrypt(qual_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Edit</a></td></tr>

            	<cfif counter is 0>
            	 <cfset counter = 1>
            	<cfelse>
            	 <cfset counter = 0>
            	</cfif>

            </cfoutput>

           </cfif>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>