<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="app" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from app
 join app_category on app_category.app_category_id = app.app_category_id
 where app_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="installed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select hub_app_app_id, hub_app_status_id from hub_app
  where hub_app_hub_id = #session.hub# and
        hub_app_app_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <cfoutput>
		   <tr><td class="feed_header">App Details</td>
		       <td class="feed_sub_header" align=right><a href="apps.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

           <cfif isdefined("u")>
            <cfif u is 1>
             <tr><td class="feed_sub_header" style="color: green;">You have successfully installed this App.  You can now use it.</td></tr>
             <tr><td height=20></td></tr>
            </cfif>
           </cfif>


	   </cfoutput>

	   </table>

	  <cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=140>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td align=center>

      			 <cfif #app.app_image# is "">
	  			  <img src="#image_virtual#/app.png" height=110 width=110 border=0>
	  			 <cfelse>
	  			  <img style="border-radius: 2px;" vspace=15 src="#image_virtual#/#app.app_image#" height=110 width=110 border=0>
			     </cfif>

			     </td>

			     </tr>

			     <tr><td height=20></td></tr>


                 <cfif app.app_provisionable is 1>

					 <cfif installed.recordcount is 0>

					 <tr><td align=center class="feed_sub_header">

						<a href="app_add.cfm?i=#encrypt(app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=1" onclick="return confirm('Install App?\r\nAre you sure you want to start this App?  By doing so you agree with the Pricing plan listed below or your current plan or agreement with the Exchange.');"><img src="/images/install.png" height=35></a>

						</td></tr>

					 <cfelse>

					 <tr><td align=center><img src="/images/installed.png" height=35></td></tr>

					 <tr><td height=10></td></tr>
					 <tr><td><hr></td></tr>

					 <tr><td align=center class="feed_sub_header">App Status</td></tr>

					 <tr><td align=center>

					  <cfif installed.hub_app_status_id is 1>
						<a href="update_status.cfm?i=#encrypt(app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=0" onclick="return confirm('Stop App?\r\nAre you sure you want to stop this App?  By doing so you will no longer have access to it and will not be able to use its functions.');"><img src="/images/app_on.png" alt="Stop App" title="Stop App" height=35></a>
					  <cfelse>
						<a href="update_status.cfm?i=#encrypt(app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=1" onclick="return confirm('Start App?\r\nAre you sure you want to start this App?  By doing so you agree with the Pricing plan listed below or your current plan or agreement with the Exchange.');"><img src="/images/app_off.png" alt="Start App" title="Start App" height=35></a>
					  </cfif>

					 </td></tr>

					 <tr><td height=10></td></tr>

					 <tr><td align=center class="link_small_gray">Click above to toggle the App on or off</td></tr>

					 <tr><td height=10></td></tr>
					 <tr><td><hr></td></tr>
					 <tr><td height=10></td></tr>

					 <tr><td align=center class="feed_sub_header">

						<a href="app_add.cfm?i=#encrypt(app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=0" onclick="return confirm('Uninstall App?\r\nUninstalling this App will remove it from your installed Apps.  Are you sure you want to continue?');"><img src="/images/uninstall.png" height=35></a>

						</td></tr>

					 <tr><td height=10></td></tr>

					 <tr><td align=center class="link_small_gray">Uninstalling this App will remove it from your installed Apps.</td></tr>

						<!---
						<cfif installed.recordcount is 1>
						  <input type="submit" name="button" value="Remove" class="button_blue" style="background-color: red;" onclick="window.location = 'app_add.cfm?i=#encrypt(app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=0'">
						<cfelse>
						  <input type="submit" name="button" value="Install" class="button_blue" style="background-color: green;" onclick="window.location = 'app_add.cfm?i=#encrypt(app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=1'">
						</cfif> --->

					 </td></tr>

					 </cfif>

                 </cfif>

			  </table>

           </td><td width=30>&nbsp;</td><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <tr><td class="feed_header">#app.app_name#</td>

				  <td align=right>

				  <cfif app.app_new is 1>
				    <img src="#image_virtual#/app_new.png" height=30>
			      </cfif>

			      <cfif app.app_status is 1>
				    <img src="#image_virtual#/app_planned.png" hspace=10 height=30>
				  <cfelseif app.app_status is 2>
				    <img src="#image_virtual#/app_alpha.png" hspace=10 height=30>
				  <cfelseif app.app_status is 3>
				    <img src="#image_virtual#/app_beta.png" hspace=10 height=30>
				  <cfelseif app.app_status is 4>
				    <img src="#image_virtual#/app_released.png" hspace=10 height=30>
                  </cfif>

			     </td></tr>
			  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				  <tr><td class="feed_sub_header" style="color: gray;" colspan=2>#app.app_category_name#</td></tr>
				  <cfif trim(app.app_desc_long) is "">
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">No Description Provided</td></tr>
                  <cfelse>
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">#replace(app.app_desc_long,"#chr(10)#","<br>","all")#</td></tr>
				  </cfif>
                  <tr><td><hr></td></tr>

				  <tr><td class="feed_sub_header">App Features & Capabilites</td></tr>

				  <cfif trim(app.app_desc_details) is "">
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">No Features or Capabilites Provided</td></tr>
                  <cfelse>
					<tr><td class="feed_sub_header" style="font-weight: normal;">#replace(app.app_desc_details,"#chr(10)#","<br>","all")#</td></tr>
				  </cfif>

                  <tr><td><hr></td></tr>

				  <tr><td class="feed_sub_header">Exchange Fee</td></tr>

				  <cfif trim(app.app_pricing) is not "">
				    <tr><td class="feed_sub_header" style="font-weight: normal;">#replace(app.app_pricing,"#chr(10)#","<br>","all")#</td></tr>
				    <tr><td height=10></td></tr>
				  </cfif>

				  <tr><td class="feed_sub_header" style="font-weight: normal;">


				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				    <tr><td class="feed_sub_header" style="font-weight: normal;" width=20%>Monthly Cost (per/user):</td>
				        <td class="feed_sub_header" style="font-weight: normal;"><cfif app.app_cost_broker_monthly is 0>Free<cfelse>#numberformat(app.app_cost_broker_monthly,'$999.99')#</cfif></td></tr>

				    <tr><td class="feed_sub_header" style="font-weight: normal;">Annual Cost (per/user):</td>
				        <td class="feed_sub_header" style="font-weight: normal;"><cfif app.app_cost_broker_annual is 0>Free<cfelse>#numberformat(app.app_cost_broker_annual,'$999.99')#</cfif></td></tr>


				  </table>

                  </td></tr>



			  </table>



           </td></tr>

      </table>

      </cfoutput>


	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>