<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.app_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 210px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 12px;
    background-color: #ffffff;
}
</style>

<cfif not isdefined("session.app_filter")>
 <cfset session.app_filter = 1>
</cfif>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">App Library</td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">Welcome to the Exchange App Library.   The App Library contains dozens of pre-built applications that can be snapped into your Exchange.</td></tr>
	   <tr><td colspan=2><hr></td></tr>

	   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
	    <form action = "app_refresh.cfm" method="post">
	    <b>Filter Apps</b>
	    &nbsp;
	    <select name="app_filter" class="input_select" onchange="form.submit();">
	     <option value=1 <cfif session.app_filter is 1>selected</cfif>>All Apps
	     <option value=2 <cfif session.app_filter is 2>selected</cfif>>Installed Apps
	    </select>

	    </form>

	    </td>
	   </tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">App has been successfully installed.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: red;">App has been successfully removed.</td></tr>
        </cfif>
        <tr><td height=10></td></tr>
       </cfif>

	  </table>

	  <cfquery name="installed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select hub_app_app_id from hub_app
	   where hub_app_hub_id = #session.hub#
	  </cfquery>

	  <cfif installed.recordcount is 0>
	   <cfset install_list = 0>
	  <cfelse>
	   <cfset install_list = valuelist(installed.hub_app_app_id)>
	  </cfif>

	  <cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from app
	   join app_category on app_category.app_category_id = app.app_category_id
	   where app_active = 1

	   <cfif session.app_filter is 2>
	    and app_id in (#install_list#)
	   </cfif>

	   order by app_name
	  </cfquery>

      <cfoutput query="apps">

	  <div class="app_badge">
	   <cfinclude template="app_badge.cfm">
	  </div>

	  </cfoutput>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>