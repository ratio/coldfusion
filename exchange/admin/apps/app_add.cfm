<cfinclude template="/exchange/security/check.cfm">

<cfif a is 1>

<cfquery name="add" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into hub_app
 (
  hub_app_app_id,
  hub_app_status_id,
  hub_app_hub_id,
  hub_app_installed_by_usr_id,
  hub_app_installed
  )
  values
  (
  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
  1,
  #session.hub#,
  #session.usr_id#,
  #now()#
  )
</cfquery>

<cflocation URL="app_info.cfm?i=#i#&u=1" addtoken="no">

<cfelse>

<cfquery name="remove_from_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 delete hub_app
 where hub_app_app_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_app_hub_id = #session.hub#
</cfquery>

<cfquery name="remove_from_menus" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 delete menu
 where menu_app_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       menu_hub_id = #session.hub#
</cfquery>

<cflocation URL="apps.cfm?u=2" addtoken="no">

	<cfquery name="transaction" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into app_status
	 (
	  app_status_hub_id,
	  app_status_usr_id,
	  app_status_app_id,

	  <cfif a is 1>
	   app_status_started
	  <cfelse>
	   app_status_stopped
	  </cfif>
	  )
	  values
	  (
	  #session.hub#,
	  #session.usr_id#,
	  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #now()#
	  )
	</cfquery>

</cfif>

