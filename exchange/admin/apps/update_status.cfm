<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub_app
	 set hub_app_status_id = #a#
	 where hub_app_hub_id = #session.hub# and
		   hub_app_app_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="transaction" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into app_status
	 (
	  app_status_hub_id,
	  app_status_usr_id,
	  app_status_app_id,

	  <cfif a is 1>
	   app_status_started
	  <cfelse>
	   app_status_stopped
	  </cfif>
	  )
	  values
	  (
	  #session.hub#,
	  #session.usr_id#,
	  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #now()#
	  )
	</cfquery>


</cftransaction>

<cflocation URL="app_info.cfm?i=#i#" addtoken="no">
