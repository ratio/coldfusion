<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">
  <cfinclude template="/exchange/hubs/admin/hub_profile.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

      <form action="support_db.cfm" method="post">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td class="feed_header">Support Team</td></tr>
       <tr><td>&nbsp;</td></tr>

       	   <cfoutput>

			   <tr>
					  <td class="feed_option" width=15%><b>Support Team Name:</b></td>
					  <td class="feed_option"><input style="font-size: 12px; width: 200px;" type="text" maxlength=100 name="hub_support_name" value="#hub.hub_support_name#"></td>
			   </tr>

			   <tr>
					  <td class="feed_option" width=15%><b>Support Email Address:</b></td>
					  <td class="feed_option"><input style="font-size: 12px; width: 200px;" type="text" maxlength=100 name="hub_support_email" value="#hub.hub_support_email#"></td>
			   </tr>

			   <tr>
					  <td class="feed_option" width=15%><b>Support Phone Number:</b></td>
					  <td class="feed_option"><input style="font-size: 12px; width: 120px;" type="text" maxlength=20 name="hub_support_phone" value="#hub.hub_support_phone#"></td>
			   </tr>

			   <tr><td height=10></td></tr>

			   <tr><td></td><td><input class="button_blue" type="submit" name="button" value="Update" vspace=10></td></tr>
           </cfoutput>

	   </table>

	   </form>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>