<div class="left_box">

<cfquery name="hub_apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_app
 join app on app_id = hub_app_app_id
 where hub_app_hub_id = #session.hub# and
       (app_tools_path is not null or app_tools_path <> '')
 order by app_name
</cfquery>


<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header">Common Tasks</td>
  <tr><td><hr></td></tr>
  <tr><td height=10></td></tr>
  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="/exchange/admin/">Settings Home</a></td></tr>
  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="/exchange/admin/license/">License Management</a></td></tr>
  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="/exchange/admin/users/">Manage Users</a></td></tr>
  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="/exchange/admin/profiles/">Role Management</a></td></tr>
  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="/exchange/admin/comp/">Companies</a></td></tr>
  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="/exchange/admin/apps/apps.cfm">App Library</a></td></tr>
  <tr><td height=20></td></tr>
  <tr><td class="feed_header">Configure Apps</td>
  <tr><td><hr></td></tr>

  <cfif hub_apps.recordcount is 0>

    <tr><td class="feed_sub_header" style="font-weight: normal;">No apps have been installed that need to be configured.</td></tr>

  <cfelse>

	  <cfoutput query="hub_apps">

	  <cfif trim(hub_apps.app_tools_path) is not "">
		  <tr><td class="feed_sub_header" style="padding-top: 2px; padding-bottom: 2px;"><img src="/images/icon_bluebox.png" height=10 width=10>&nbsp;&nbsp;&nbsp;<a href="#app_tools_path#"><cfif len(app_name) GT 20>#left(app_name,20)#...<cfelse>#app_name#</cfif></a></td></tr>
	  </cfif>

	  </cfoutput>

  </cfif>

  <tr><td height=10></td></tr>
</table>

</div>
