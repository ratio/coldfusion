<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

	<cfif #hub_role_image# is not "">
		<cffile action = "upload"
		 fileField = "hub_role_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into hub_role
     (
     hub_role_name,
     hub_role_limit_deals,
     hub_role_limit_boards,
     hub_role_limit_portfolios,
     hub_role_limit_reports,
     hub_role_image,
     hub_role_order,
     hub_role_tour_id,
     hub_role_public,
     hub_role_hub_id,
     hub_role_desc,
     hub_role_monthly,
     hub_role_annually,
     hub_role_trial,
     hub_role_trial_duration
     )
     values
     (
     '#hub_role_name#',
      <cfif hub_role_limit_deals is "">null<cfelse>#hub_role_limit_deals#</cfif>,
      <cfif hub_role_limit_boards is "">null<cfelse>#hub_role_limit_boards#</cfif>,
      <cfif hub_role_limit_portfolios is "">null<cfelse>#hub_role_limit_portfolios#</cfif>,
      <cfif hub_role_limit_reports is "">null<cfelse>#hub_role_limit_reports#</cfif>,
      <cfif #hub_role_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
      #hub_role_order#,
     <cfif hub_role_tour_id is "">null<cfelse>'#hub_role_tour_id#'</cfif>,
      #hub_role_public#,
      #session.hub#,
     '#hub_role_desc#',
      #hub_role_monthly#,
      #hub_role_annually#,
      #hub_role_trial#,
      <cfif hub_role_trial_duration is "">null<cfelse>#hub_role_trial_duration#</cfif>
     )
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_role_image from hub_role
		  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.hub_role_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.hub_role_image#">
		</cfif>

	</cfif>

	<cfif #hub_role_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_role_image from hub_role
		  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.hub_role_image# is not "">
         <cfif fileexists("#media_path#\#getfile.hub_role_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.hub_role_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "hub_role_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub_role
	 set hub_role_name = '#hub_role_name#',
	     hub_role_limit_deals =  <cfif hub_role_limit_deals is "">null<cfelse>#hub_role_limit_deals#</cfif>,
	     hub_role_limit_boards =  <cfif hub_role_limit_boards is "">null<cfelse>#hub_role_limit_boards#</cfif>,
	     hub_role_limit_portfolios =  <cfif hub_role_limit_portfolios is "">null<cfelse>#hub_role_limit_portfolios#</cfif>,
	     hub_role_limit_reports =  <cfif hub_role_limit_reports is "">null<cfelse>#hub_role_limit_reports#</cfif>,

		  <cfif #hub_role_image# is not "">
		   hub_role_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   hub_role_image = null,
		  </cfif>

	     hub_role_order = #hub_role_order#,
	     hub_role_tour_id = <cfif hub_role_tour_id is "">null<cfelse>'#hub_role_tour_id#'</cfif>,
	     hub_role_public = #hub_role_public#,
         hub_role_monthly = #hub_role_monthly#,
	     hub_role_annually = #hub_role_annually#,
	     hub_role_trial = #hub_role_trial#,
	     hub_role_trial_duration = #hub_role_trial_duration#,
	     hub_role_desc = '#hub_role_desc#'
	 where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_role_image from hub_role
		  where  hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#)
		</cfquery>

		<cfif remove.hub_role_image is not "">
         <cfif fileexists("#media_path#\#remove.hub_role_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.hub_role_image#">
         </cfif>
		</cfif>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete hub_role
	 where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete menu
	 where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

</cftransaction>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

