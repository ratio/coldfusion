<cfinclude template="/exchange/security/check.cfm">

<cfquery name="menu" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from menu
  left join app on app_id = menu_app_id
  where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        (menu_parent_id is null or menu_parent_id = 0) and
        menu_hub_id = #session.hub#
        order by menu_order
</cfquery>

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">MENU EDITOR - #ucase(profile.hub_role_name)#</td>
	       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>
	   </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Menu Item has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Menu Item has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Menu Item has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <cfoutput>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="menu_add.cfm?i=#i#">Add Menu Item</a></td></tr>
       </cfoutput>

       <tr><td height=10></td></tr>

      </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
	      <tr>
	         <td><cfinclude template="preview.cfm"></td>
	      </tr>
	      <tr><td height=10></td></tr>
	   </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #menu.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Menu Items exist.</td></tr>

        <cfelse>
			<tr>
			<td class="feed_sub_header" width=300>Menu Name</td>
			<td class="feed_sub_header">Type</td>
			<td class="feed_sub_header">Value</td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfloop query="menu">

			<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from menu
			  left join app on app_id = menu_app_id
			  where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
					menu_parent_id = #menu.menu_id# and
					menu_hub_id = #session.hub#
					order by menu_order
			</cfquery>

          <cfoutput>
          <tr><td height=8></td></tr>

          <tr bgcolor="e0e0e0">

              <td class="feed_sub_header" valign=top><a href="menu_edit.cfm?m=#encrypt(menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">#menu_order# - #menu_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">

              <cfif menu_type_id is 1>
               Grouping
              <cfelseif menu_type_id is 2>
               URL
              <cfelseif menu_type_id is 3>
               App
              <cfelseif menu_type_id is 4>
               Embedded URL
              <cfelseif menu_type_id is 5>
               Custom Path
              <cfelseif menu_type_id is 6>
               Custom Code
              <cfelseif menu_type_id is 7>
               iFrame
              </cfif>

              </td>

               <td class="feed_sub_header" valign=top style="font-weight: normal;">

               <cfif menu_type_id is 1>

               <cfelseif menu_type_id is 2>
                #menu_url#
               <cfelseif menu_type_id is 3>
                #app_name#
               <cfelseif menu_type_id is 4>
                #menu_embed#
               <cfelseif menu_type_id is 5>
                #menu_custom_path#
               <cfelseif menu_type_id is 6>
                Code
               <cfelseif menu_type_id is 7>
                #menu_iframe_url#
               </cfif>

              </td>

              <td class="feed_sub_header" valign=top align=right><a href="menu_add.cfm?m=#encrypt(menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a> <a href="menu_add.cfm?m=#encrypt(menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Add Item</a>&nbsp;&nbsp;</td>

          </tr>

          <cfloop query="child">

			<cfquery name="grandchild" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from menu
			  left join app on app_id = menu_app_id
			  where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
					menu_parent_id = #child.menu_id# and
					menu_hub_id = #session.hub#
					order by menu_order
			</cfquery>

          <cfoutput>

           <tr bgcolor="ffffff">

              <td class="feed_sub_header" valign=top style="padding-bottom: 0px;"><img src="/images/sub.png" width=30 style="margin-left: 20px;">&nbsp;<a href="menu_edit.cfm?m=#encrypt(child.menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">#child.menu_order# - #child.menu_name#</a></td>

              <td class="feed_sub_header" valign=top style="font-weight: normal;">

              <cfif child.menu_type_id is 1>
               Grouping
              <cfelseif child.menu_type_id is 2>
               URL
              <cfelseif child.menu_type_id is 3>
               App
              <cfelseif child.menu_type_id is 4>
               Embedded URL
              <cfelseif child.menu_type_id is 5>
               Custom Path
              <cfelseif child.menu_type_id is 6>
               Custom Code
              <cfelseif child.menu_type_id is 7>
               iFrame
              </cfif>

              </td>

               <td class="feed_sub_header" valign=top style="font-weight: normal;">


               <cfif child.menu_type_id is 1>

               <cfelseif child.menu_type_id is 2>
                #menu_url#
               <cfelseif child.menu_type_id is 3>
                #app_name#
               <cfelseif child.menu_type_id is 4>
                #menu_embed#
               <cfelseif child.menu_type_id is 5>
                #menu_custom_path#
               <cfelseif child.menu_type_id is 6>
                Code
               <cfelseif child.menu_type_id is 7>
                #menu_iframe_url#
               </cfif>


              </td>


              <td class="feed_sub_header" valign=top style="padding-bottom: 0px;" align=right><a href="menu_add.cfm?m=#encrypt(menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a><a href="menu_add.cfm?m=#encrypt(menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Add Item</a>&nbsp;&nbsp;</td>

          </tr>

          </cfoutput>

          <cfloop query="grandchild">

          <cfoutput>

           <tr bgcolor="ffffff">
              <td class="feed_sub_header" valign=top style="padding-bottom: 0px;"><img src="/images/sub.png" width=30 style="margin-left: 60px;">&nbsp;<a href="menu_edit.cfm?m=#encrypt(grandchild.menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">#grandchild.menu_order# - #grandchild.menu_name#</a></td>

              <td class="feed_sub_header" valign=top style="font-weight: normal;">

              <cfif grandchild.menu_type_id is 1>
               Grouping
              <cfelseif grandchild.menu_type_id is 2>
               URL
              <cfelseif grandchild.menu_type_id is 3>
               App
              <cfelseif grandchild.menu_type_id is 4>
               Embedded URL
              <cfelseif grandchild.menu_type_id is 5>
               Custom Path
              <cfelseif grandchild.menu_type_id is 6>
               Custom Code
              <cfelseif grandchild.menu_type_id is 7>
               iFrame
              </cfif>



             </td>

               <td class="feed_sub_header" valign=top style="font-weight: normal;">

               <cfif grandchild.menu_type_id is 1>

               <cfelseif grandchild.menu_type_id is 2>
                #menu_url#
               <cfelseif grandchild.menu_type_id is 3>
                #app_name#
               <cfelseif grandchild.menu_type_id is 4>
                #menu_embed#
               <cfelseif grandchild.menu_type_id is 5>
                #menu_custom_path#
               <cfelseif grandchild.menu_type_id is 6>
                Code
               <cfelseif grandchild.menu_type_id is 7>
                #menu_iframe_url#
               </cfif>


              </td>


              <td class="feed_sub_header" valign=top style="padding-bottom: 0px;" align=right>&nbsp;</td>
          </tr>

          </cfoutput>

          </cfloop>

          </cfloop>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

         </cfloop>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

