<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from menu
  where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        menu_id = #decrypt(m,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        menu_hub_id = #session.hub#
</cfquery>

<cfquery name="parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from menu
  where menu_parent_id = 0 and
        menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        menu_hub_id = #session.hub#
</cfquery>

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from app_category
  order by app_category_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <form action="menu_db.cfm" method="post">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">#ucase(profile.hub_role_name)# - Edit Menu Item</td>
	       <td class="feed_sub_header" align=right><a href="menu.cfm?i=#i#">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>
	   </cfoutput>

	   <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header" width=150>Menu Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="menu_name" value="#edit.menu_name#" maxlength="50" required></td></tr>

		   <tr><td class="feed_sub_header" width=150 valign=top>Description</td>
		       <td><textarea name="menu_desc" class="input_textarea" style="width: 900px; height: 100px;">#edit.menu_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header" width=150>Menu Icon</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="menu_icon" value='#edit.menu_icon#' maxlength="200" placeholder="entering nothing will use the default app icon"></td></tr>

		   <tr><td></td>
		       <td class="link_small_gray"><a href="https://fontawesome.com/v4.7.0/icons/" target="_blank" rel="noopener" rel="noreferrer"><b><u>Search for icons</u></b></a>, click on selection, and paste the icon code above i.e., #htmleditformat("<i class=fa fa-question-circle aria-hidden=true></i>")#</td></tr>
           <tr><td height=10></td></tr>

       </cfoutput>

		   <tr><td class="feed_sub_header" width=150>Parent</td>
		       <td>
		       <select name="menu_parent_id" class="input_select" style="input_select">
		       <option value=0>No Parent Menu

		       <cfloop query="parent">

				<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select * from menu
				  where menu_parent_id = #parent.menu_id# and
						menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
						menu_hub_id = #session.hub#
				</cfquery>

		        <cfoutput>
		         <option value=#parent.menu_id# <cfif #edit.menu_parent_id# is #parent.menu_id#>selected</cfif>>#parent.menu_name#
                </cfoutput>

                <cfloop query="child">

		        <cfoutput>
		         <option value=#child.menu_id# <cfif #edit.menu_parent_id# is #child.menu_id#>selected</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;#child.menu_name#
                </cfoutput>

                </cfloop>

		       </cfloop>
		       </select>
		       </td></tr>

		   <cfoutput>

		   <tr><td class="feed_sub_header" width=150>Order</td>
		       <td><input class="input_text" style="width: 75px;" type="number" value=#edit.menu_order# name="menu_order" step=".1" required></td></tr>

		   <tr><td class="feed_sub_header" width=150>Active</td>
		       <td>
		           <select name="menu_active" class="input_select">
		           <option value=0>No
		           <option value=1 <cfif #edit.menu_active# is 1>selected</cfif>>Yes
		           </select>
		       </td></tr>

		   </cfoutput>

           <tr><td colspan=2><hr></td></tr>

		   <tr><td class="feed_sub_header" valign=top width=150><br>Type of<br>Action</td>
		       <td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

		         <tr>
		            <td width=40><input type="radio" name="menu_type_id" value=1 style="width: 22px; height: 22px;" <cfif edit.menu_type_id is 1>checked</cfif>></td>
		            <td class="feed_sub_header" style="font-weight: normal;">Grouping</td>
		            <td class="feed_sub_header" style="font-weight: normal;">Do nothing when clicked</td>
		         </tr>

		         <tr>
		            <td width=40><input type="radio" name="menu_type_id" value=3 style="width: 22px; height: 22px;" <cfif edit.menu_type_id is 3>checked</cfif>></td>
		            <td class="feed_sub_header" style="font-weight: normal;">Run App:</td>
		            <td>

		            <select name="menu_app_id" class="input_select" style="width: 300px;">
		            <option value=0>Select App
		            <cfloop query="cats">

					<cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					  select * from app
					  join hub_app on hub_app_app_id = app_id
					  where app_category_id = #cats.app_category_id# and
					  hub_app_hub_id = #session.hub# and
					  app_active = 1
					  order by app_name
					</cfquery>

					<cfif apps.recordcount GT 0>

		            <cfoutput>
		             <optgroup label="#app_category_name#">
		            </cfoutput>

                    <cfoutput query="apps">
                      <option value=#app_id# <cfif #edit.menu_app_id# is #app_id#>selected</cfif>>&nbsp;&nbsp;#app_name#
                    </cfoutput>

		            </cfif>

		            </cfloop>
		            </select>

		            </td>
		         </tr>

		         <cfoutput>

		         <tr>
		            <td width=40><input type="radio" name="menu_type_id" value=2 style="width: 22px; height: 22px;" <cfif edit.menu_type_id is 2>checked</cfif>></td>
		            <td class="feed_sub_header" style="font-weight: normal;">Go To URL:</td>
		            <td><input type="url" class="input_text" name="menu_url" style="width: 800px;" value="#edit.menu_url#"></td>
		         </tr>


		         <tr>
		            <td width=40><input type="radio" name="menu_type_id" value=5 style="width: 22px; height: 22px;" <cfif edit.menu_type_id is 5>checked</cfif>></td>
		            <td class="feed_sub_header" style="font-weight: normal;">Custom Path:</td>
		            <td><input type="text" class="input_text" name="menu_custom_path" value="#edit.menu_custom_path#" style="width: 800px;"></td>
		         </tr>

		         <tr>
		            <td width=40 valign=top><input type="radio" name="menu_type_id" value=4 style="width: 22px; height: 22px; margin-top: 10px;" <cfif edit.menu_type_id is 4>checked</cfif>></td>
		            <td valign=top class="feed_sub_header" style="font-weight: normal;">Embed URL:</td>
		            <td><input type="url" class="input_text" name="menu_embed" value="#edit.menu_embed#" style="width: 800px;"></td>
		         </tr>

		         <tr>
		            <td width=40 valign=top><input type="radio" name="menu_type_id" value=7 style="width: 22px; height: 22px; margin-top: 10px;" <cfif edit.menu_type_id is 7>checked</cfif>></td>
		            <td valign=top class="feed_sub_header" style="font-weight: normal;">iFrame URL:</td>
		            <td><input type="url" class="input_text" name="menu_iframe_url" value="#edit.menu_iframe_url#" style="width: 800px;"></td>
                 </tr>

		         <tr>
		            <td width=40 valign=top><input type="radio" name="menu_type_id" value=6 style="width: 22px; height: 22px; margin-top: 10px;" <cfif edit.menu_type_id is 6>checked</cfif>></td>
		            <td valign=top class="feed_sub_header" style="font-weight: normal;">Custom Code:</td>
		            <td><textarea name="menu_custom_code" class="input_textarea" style="width: 800px; height: 400px;">#edit.menu_custom_code#</textarea></td></tr>

                <input type="hidden" name="m" value=#m#>
                <input type="hidden" name="i" value=#i#>

                </cfoutput>
				</table>

		       </td></tr>

           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>
		  </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

