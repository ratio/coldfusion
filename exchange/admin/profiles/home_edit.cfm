<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from component
  order by component_name
</cfquery>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from home_section
  where home_section_profile_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        home_section_hub_id = #session.hub# and
        home_section_id = #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <form action="home_db.cfm" method="post">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">#profile.hub_role_name# - Edit Homepage Section</td>
	       <td class="feed_sub_header" align=right><a href="home.cfm?i=#i#">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header" width=150>Section Name</td>
		       <td><input class="input_text" style="width: 500px;" type="text" name="home_section_name" maxlength="300" value="#edit.home_section_name#"></td></tr>

		   <tr><td class="feed_sub_header" width=150 valign=top>Description</td>
		       <td><textarea name="home_section_desc" class="input_textarea" style="width: 900px; height: 100px;">#edit.home_section_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header" width=150>Order</td>
		       <td><input class="input_text" style="width: 75px;" type="number" name="home_section_order" step=".1" value=#edit.home_section_order# required></td></tr>

		   <tr><td class="feed_sub_header" width=150>Active</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

		       <select name="home_section_active" class="input_select" style="width: 75px;">
		       	<option value=0>No
		       	<option value=1 <cfif #edit.home_section_active# is 1>selected</cfif>>Yes
		       </select>

		       </td></tr>

		   <tr><td class="feed_sub_header" width=150>Display</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

		       <select name="home_section_display" class="input_select" style="width: 75px;">
		       	<option value=0 <cfif #edit.home_section_display# is 0>selected</cfif>>No
		       	<option value=1 <cfif #edit.home_section_display# is 1>selected</cfif>>Yes
		       </select>

		       <span class="link_small_gray">Display Section Name and Description with App / Component?</span>

		       </td></tr>

		   <tr><td class="feed_sub_header" width=150>Location</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

		       <select name="home_section_location" class="input_select" style="width: 200px;">
		       	<option value="Left" <cfif edit.home_section_location is "Left">selected</cfif>>Left
		       	<option value="Center" <cfif edit.home_section_location is "Center">selected</cfif>>Center
		       	<option value="Right" <cfif edit.home_section_location is "Right">selected</cfif>>Right
		       </select

		       </td></tr>

       </cfoutput>
		   <tr><td class="feed_sub_header" width=150>Component</td>
		       <td class="feed_sub_header" style="font-weight: normal;">
		       <select name="home_section_component_id" class="input_select">
		       <option value=0>No Component
		       <cfoutput query="comp">
		       <option value=#component_id# <cfif #edit.home_section_component_id# is #component_id#>selected</cfif>>#component_name#
		       </cfoutput>
		       </select>

		       &nbsp;&nbsp;<b>Or, include content below</b>

		       </td>
		   </tr>

		   <cfoutput>

		   <tr><td class="feed_sub_header" width=150 valign=top>Content</td>
		       <td><textarea name="home_section_content" class="input_textarea" style="width: 900px; height: 900px;">#edit.home_section_content#</textarea></td></tr>

           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

           <input type="hidden" name="h" value=#h#>
           <input type="hidden" name="i" value=#i#>
           </cfoutput>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>
		  </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

