<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into home_section
     (
      home_section_name,
      home_section_active,
      home_section_display,
      home_section_desc,
      home_section_location,
      home_section_order,
      home_section_profile_id,
      home_section_hub_id,
      home_section_content,
      home_section_component_id
      )
      values
      (
      '#home_section_name#',
       #home_section_active#,
       #home_section_display#,
      '#home_section_desc#',
      '#home_section_location#',
       #home_section_order#,
       #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
       #session.hub#,
      '#home_section_content#',
       #home_section_component_id#
       )
	</cfquery>

    <cflocation URL="home.cfm?i=#i#&u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       update home_section
       set home_section_name = '#home_section_name#',
           home_section_active = #home_section_active#,
           home_section_display = #home_section_display#,
           home_section_desc = '#home_section_desc#',
           home_section_location = '#home_section_location#',
           home_section_component_id = #home_section_component_id#,
           home_section_content = '#home_section_content#',
           home_section_order = #home_section_order#
       where home_section_id = #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
             home_section_profile_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
             home_section_hub_id = #session.hub#
    </cfquery>

    <cflocation URL="home.cfm?i=#i#&u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete home_section
	 where home_section_id = #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="home.cfm?i=#i#&u=3" addtoken="no">

</cfif>

