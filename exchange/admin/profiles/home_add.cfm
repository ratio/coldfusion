<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select max(home_section_order) as total from home_section
  where home_section_hub_id = #session.hub#

  <cfif l is "Left">
   and home_section_location = 'Left'
  <cfelseif l is "Center">
   and home_section_location = 'Center'
  <cfelseif l is "Right">
   and home_section_location = 'Right'
  </cfif>

</cfquery>

<cfif max.total is "">
 <cfset mtotal = 0>
<cfelse>
 <cfset mtotal = #max.total#>
</cfif>

<cfset next = #evaluate(round(mtotal)+1)#>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from component
  order by component_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <form action="home_db.cfm" method="post">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">#profile.hub_role_name# - Add Homepage Section</td>
	       <td class="feed_sub_header" align=right><a href="home.cfm?i=#i#">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>
	   </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header" width=150>Section Name</td>
		       <td><input class="input_text" style="width: 500px;" type="text" name="home_section_name" maxlength="300"></td></tr>

		   <tr><td class="feed_sub_header" width=150 valign=top>Description</td>
		       <td><textarea name="home_section_desc" class="input_textarea" style="width: 900px; height: 100px;"></textarea></td></tr>

		   <tr><td class="feed_sub_header" width=150>Order</td>
		       <td><input class="input_text" style="width: 75px;" type="number" name="home_section_order" step=".1" required value=<cfoutput>#next#</cfoutput>></td></tr>

		   <tr><td class="feed_sub_header" width=150>Active</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

		       <select name="home_section_active" class="input_select" style="width: 75px;">
		       	<option value=0>No
		       	<option value=1 selected>Yes
		       </select>

		       </td></tr>

		   <tr><td class="feed_sub_header" width=150>Display</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

		       <select name="home_section_display" class="input_select" style="width: 75px;">
		       	<option value=0>No
		       	<option value=1 selected>Yes
		       </select>

		       <span class="link_small_gray">Display Section Name and Description with App / Component?</span>

		       </td></tr>


		   <tr><td class="feed_sub_header" width=150>Location</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

		       <select name="home_section_location" class="input_select" style="width: 200px;">
		       	<option value="Left" <cfif l is "l">selected</cfif>>Left
		       	<option value="Center" <cfif l is "c">selected</cfif>>Center
		       	<option value="Right" <cfif l is "r">selected</cfif>>Right
		       </select>

		       </td></tr>

		   <tr><td class="feed_sub_header" width=150>Component</td>
		       <td class="feed_sub_header" style="font-weight: normal;">
		       <select name="home_section_component_id" class="input_select">
		       <option value=0>No Component
		       <cfoutput query="comp">
		       <option value=#component_id#>#component_name#
		       </cfoutput>
		       </select>

		       &nbsp;&nbsp;<b>Or, include content below</b>

		       </td>
		   </tr>

		   <tr><td class="feed_sub_header" width=150 valign=top>Content</td>
		       <td><textarea name="home_section_content" class="input_textarea" style="width: 900px; height: 900px;"></textarea></td></tr>

           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

           <cfoutput>
           <input type="hidden" name="i" value=#i#>
           </cfoutput>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

