<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into hub_role
     (
     hub_role_name,
     hub_role_hub_id,
     hub_role_desc
     )
     values
     (
     '#profile_name#',
      #session.hub#,
     'Cloned Profile'
     )
</cfquery>

<cfquery name="max_hub_role" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select max(hub_role_id) as id from hub_role
</cfquery>

<!--- Clone Homepage --->

  <cfquery name="home" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from home_section
   where home_section_hub_id = #session.hub# and
		 home_section_profile_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfloop query="home">

	  <cfquery name="home_insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   insert into home_section
	   (
		home_section_profile_id,
		home_section_hub_id,
		home_section_order,
		home_section_title,
		home_section_desc,
		home_section_type_id,
		home_section_embed,
		home_section_name,
		home_section_content,
		home_section_component_id,
		home_section_location,
		home_section_display,
		home_section_active
	   )
	   values
	   (
	   #max_hub_role.id#,
	   #session.hub#,
	   #home.home_section_order#,
	  '#home.home_section_title#',
	  '#home.home_section_desc#',
	   <cfif #home.home_section_type_id# is "">null<cfelse>#home.home_section_type_id#</cfif>,
	  '#home.home_section_embed#',
	  '#home.home_section_name#',
	  '#home.home_section_content#',
	   #home.home_section_component_id#,
	  '#home.home_section_location#',
	   #home.home_section_display#,
	   #home.home_section_active#
	   )
	  </cfquery>

  </cfloop>

<cfquery name="parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from menu
 where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       menu_parent_id = 0 and
       menu_hub_id = #session.hub#
</cfquery>

<cfloop query="parent">

	<cfquery name="insert_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into menu
     (
      menu_name,
      menu_active,
      menu_order,
      menu_role_id,
      menu_hub_id,
      menu_parent_id,
      menu_icon,
      menu_type_id,
      menu_url,
      menu_app_id,
      menu_embed,
      menu_custom_path
      )
      values
      (
      '#parent.menu_name#',
       #parent.menu_active#,
       #parent.menu_order#,
       #max_hub_role.id#,
       #parent.menu_hub_id#,
       0,
      '#parent.menu_icon#',
       #parent.menu_type_id#,
      '#parent.menu_url#',
       #parent.menu_app_id#,
      '#parent.menu_embed#',
      '#parent.menu_custom_path#'
       )
	</cfquery>

	<cfquery name="max_menu_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(menu_id) as id from menu
	</cfquery>

	<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from menu
	 where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		   menu_parent_id = #parent.menu_id# and
		   menu_hub_id = #session.hub#
	</cfquery>

	<cfloop query="child">

		<cfquery name="insert_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into menu
		 (
		  menu_name,
		  menu_active,
		  menu_order,
		  menu_role_id,
		  menu_hub_id,
		  menu_parent_id,
		  menu_icon,
		  menu_type_id,
		  menu_url,
		  menu_app_id,
		  menu_embed,
		  menu_custom_path
		  )
		  values
		  (
		  '#child.menu_name#',
		   #child.menu_active#,
		   #child.menu_order#,
		   #max_hub_role.id#,
		   #child.menu_hub_id#,
		   #max_menu_parent.id#,
		  '#child.menu_icon#',
		   #child.menu_type_id#,
		  '#child.menu_url#',
		   #child.menu_app_id#,
		  '#child.menu_embed#',
		  '#child.menu_custom_path#'
		   )
		</cfquery>

			<cfquery name="max_menu_grandchild" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select max(menu_id) as id from menu
			</cfquery>

			<cfquery name="grandchild" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from menu
			 where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   menu_parent_id = #child.menu_id# and
				   menu_hub_id = #session.hub#
			</cfquery>

			<cfloop query="grandchild">

				<cfquery name="insert_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert into menu
				 (
				  menu_name,
				  menu_active,
				  menu_order,
				  menu_role_id,
				  menu_hub_id,
				  menu_parent_id,
				  menu_icon,
				  menu_type_id,
				  menu_url,
				  menu_app_id,
				  menu_embed,
				  menu_custom_path
				  )
				  values
				  (
				  '#grandchild.menu_name#',
				   #grandchild.menu_active#,
				   #grandchild.menu_order#,
				   #max_hub_role.id#,
				   #grandchild.menu_hub_id#,
				   #max_menu_grandchild.id#,
				  '#grandchild.menu_icon#',
				   #grandchild.menu_type_id#,
				  '#grandchild.menu_url#',
				   #grandchild.menu_app_id#,
				  '#grandchild.menu_embed#',
				  '#grandchild.menu_custom_path#'
				   )
				</cfquery>

			</cfloop>

	</cfloop>

</cfloop>

</cftransaction>

<cflocation URL="index.cfm?u=4" addtoken="no">
