<cfinclude template="/exchange/security/check.cfm">

<cfif a is 0>

	<cfquery name="toggle" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     update home_section
     set home_section_active = 0
     where home_section_id = #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

<cfelse>

	<cfquery name="toggle" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     update home_section
     set home_section_active = 1
     where home_section_id = #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

</cfif>

<cflocation URL="home.cfm?i=#i#&u=9" addtoken="no">