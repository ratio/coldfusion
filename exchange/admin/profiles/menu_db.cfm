<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

    <!--- Types

    1 - Nothing
    2 - URL
    3 - App
    4 - Embed --->

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into menu
     (
      menu_name,
      menu_desc,
      menu_active,
      menu_order,
      menu_role_id,
      menu_hub_id,
      menu_parent_id,
      menu_icon,
      menu_type_id,
      menu_url,
      menu_app_id,
      menu_embed,
      menu_custom_path,
      menu_custom_code,
      menu_iframe_url
      )
      values
      (
      '#menu_name#',
      '#menu_desc#',
       #menu_active#,
       #menu_order#,
       #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
       #session.hub#,
       #menu_parent_id#,
      '#menu_icon#',
       #menu_type_id#,
      '#menu_url#',
       #menu_app_id#,
      '#menu_embed#',
      '#menu_custom_path#',
      '#menu_custom_code#',
      '#menu_iframe_url#'
       )
	</cfquery>

    <cflocation URL="menu.cfm?i=#i#&u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       update menu
       set menu_name = '#menu_name#',
           menu_desc = '#menu_desc#',
           menu_order = #menu_order#,
           menu_parent_id = #menu_parent_id#,
           menu_icon = '#menu_icon#',
           menu_type_id = #menu_type_id#,
           menu_active = #menu_active#,
           menu_url = '#menu_url#',
           menu_app_id = #menu_app_id#,
           menu_embed = '#menu_embed#',
           menu_iframe_url = '#menu_iframe_url#',
           menu_custom_path = '#menu_custom_path#',
           menu_custom_code = '#menu_custom_code#'
       where menu_id = #decrypt(m,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

    <cflocation URL="menu.cfm?i=#i#&u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cftransaction>

		<cfquery name="delete_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete menu
		 where menu_id = #decrypt(m,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update menu
		 set menu_parent_id = null
		 where menu_parent_id = #decrypt(m,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

	</cftransaction>

    <cflocation URL="menu.cfm?i=#i#&u=3" addtoken="no">

</cfif>

