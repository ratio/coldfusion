<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub#
  order by hub_role_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Role Management</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Role has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Role has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Role has been successfully deleted.</td>
        <cfelseif u is 4>
         <td class="feed_sub_header" style="color: green;">New Role has been created from the cloned menu.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Role</a></td></tr>

      </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #profiles.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Roles exist.</td></tr>

        <cfelse>

        <tr>
				<td class="feed_sub_header" colspan=6></td>
				<td class="feed_sub_header" colspan=2 align=center>Price Per User</b></td>
				<td class="feed_sub_header" colspan=2 align=center>Exchange Fee</b></td>
				<td></td>
        </tr>

        <tr>
				<td class="feed_sub_header" width=300>Role Name</b></td>
				<td class="feed_sub_header">Description</b></td>
				<td class="feed_sub_header" align=center>Active Users</b></td>
				<td class="feed_sub_header" align=center>Apps</b></td>
				<td class="feed_sub_header" align=center>Allow Trial?</b></td>
				<td class="feed_sub_header" align=center>Duration</b></td>
				<td class="feed_sub_header" align=center>Monthly</b></td>
				<td class="feed_sub_header" align=center>Annually</b></td>
				<td class="feed_sub_header" align=center>Monthly</b></td>
				<td class="feed_sub_header" align=center>Annual</b></td>
				<td class="feed_sub_header" align=right></b></td>

			</tr>

        </cfif>

        <cfset counter = 0>

         <cfset app_cost_monthly = 0>
         <cfset app_cost_annual = 0>
         <cfset app_fee_monthly = 0>
         <cfset app_fee_annual = 0>

         <cfloop query="profiles">

		<cfquery name="menu" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select app_id from menu
          join app on app_id = menu_app_id
		  where menu_role_id = #profiles.hub_role_id#
		</cfquery>

		<cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select count(distinct(menu_app_id)) as total from menu
		  where menu_role_id = #profiles.hub_role_id# and
		        menu_hub_id = #session.hub#
		</cfquery>

		<cfquery name="cost" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select sum(app_cost_broker_monthly) as monthly, sum(app_cost_broker_annual) as annual from menu
          join app on app_id = menu_app_id
		  where menu_role_id = #profiles.hub_role_id#
		</cfquery>

		<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select count(hub_xref_id) as total from hub_xref
		  where hub_xref_role_id = #profiles.hub_role_id# and
		        hub_xref_active = 1 and
		        hub_xref_hub_id = #session.hub#
		</cfquery>

         <cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top width=200><a href="edit.cfm?i=#encrypt(profiles.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#profiles.hub_role_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" width=250><cfif profiles.hub_role_desc is "">No description provided<cfelse>#profiles.hub_role_desc#</cfif></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#users.total#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#apps.total#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center><cfif profiles.hub_role_trial is 1>Yes<cfelse>No</cfif></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center><cfif profiles.hub_role_trial_duration GT 0>#profiles.hub_role_trial_duration# Days<cfelse>N/A</cfif></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(profiles.hub_role_monthly,'$999.99')#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(profiles.hub_role_annually,'$999.99')#</td>

              <cfif users.total is "">
               <cfset total_users = 0>
              <cfelse>
               <cfset total_users = users.total>
              </cfif>

              <cfif cost.monthly is "">
               <cfset monthly_cost = 0>
              <cfelse>
               <cfset monthly_cost = cost.monthly>
              </cfif>

              <cfif cost.annual is "">
               <cfset annual_cost = 0>
              <cfelse>
               <cfset annual_cost = cost.annual>
              </cfif>

              <cfset mc = evaluate(monthly_cost*total_users)>
              <cfset ac = evaluate(annual_cost*total_users)>

              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(mc,'$99,999.99')#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(ac,'$99,999.99')#</td>
              <td align=right valign=top style="padding-top: 15px;">

					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=8>
					  <div class="dropdown-content" style="width: 225px; text-align: left;">
						<a href="edit.cfm?i=#encrypt(profiles.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Edit Role</a>
						<a href="home.cfm?i=#encrypt(profiles.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;Home Page Editor</a>
						<a href="menu.cfm?i=#encrypt(profiles.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;Menu Editor</a>
						<a href="menu_clone.cfm?i=#encrypt(profiles.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-clone" aria-hidden="true"></i>&nbsp;&nbsp;Clone Role</a>
						<cfif isdefined("update_master")>
						<a href="menu_master.cfm?i=#encrypt(profiles.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-clone" aria-hidden="true"></i>&nbsp;&nbsp;Send to Master</a>
						</cfif>
					  </div>
					</div>

               </td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          <cfif users.total is "">
           <cfset users_total = 0>
          <cfelse>
           <cfset users_total = users.total>
          </cfif>

          <cfif profiles.hub_role_monthly is "">
           <cfset hrm = 0>
          <cfelse>
           <cfset hrm = profiles.hub_role_monthly>
          </cfif>

          <cfif profiles.hub_role_annually is "">
           <cfset hra = 0>
          <cfelse>
           <cfset hra = profiles.hub_role_annually>
          </cfif>

          <cfif cost.monthly is "">
           <cfset cm = 0>
          <cfelse>
           <cfset cm = cost.monthly>
          </cfif>

          <cfif cost.annual is "">
           <cfset ca = 0>
          <cfelse>
           <cfset ca = cost.annual>
          </cfif>

          <cfset app_cost_monthly = app_cost_monthly + evaluate(users_total * hrm)>
          <cfset app_cost_annual = app_cost_annual + evaluate(users_total * hra)>
          <cfset app_fee_monthly = app_fee_monthly + evaluate(users_total * cm)>
          <cfset app_fee_annual = app_fee_annual + evaluate(users_total * ca)>
         </cfoutput>

         </cfloop>

         <cfoutput>

         <tr>
            <td class="feed_sub_header" colspan=6>Total</td>
            <td class="feed_sub_header" align=center>#numberformat(app_cost_monthly,'$999,999.99')#</td>
            <td class="feed_sub_header" align=center>#numberformat(app_cost_annual,'$999,999.99')#</td>
            <td class="feed_sub_header" align=center>#numberformat(app_fee_monthly,'$999,999.99')#</td>
            <td class="feed_sub_header" align=center>#numberformat(app_fee_annual,'$999,999.99')#</td>
         </tr>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

