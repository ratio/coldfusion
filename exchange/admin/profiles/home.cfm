<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Home Page Editor - #profile.hub_role_name#</td>
	       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>
	   </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Homepage Section has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Homepage Section has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Homepage Section has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right></td>

       </tr>
      </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfoutput>
		<tr>
		   <td class="feed_header">Left Section</td>
		   <td align=right><a href="home_add.cfm?i=#i#&l=l"><img src="#image_virtual#/plus3.png" width=20 border=0></a></td>
		   <td></td>
		   <td class="feed_header">Center Section</td>
		   <td align=right><a href="home_add.cfm?i=#i#&l=c"><img src="#image_virtual#/plus3.png" width=20 border=0></a></td>
		   <td></td>
		   <td class="feed_header">Right Section</td>
		   <td align=right><a href="home_add.cfm?i=#i#&l=r"><img src="#image_virtual#/plus3.png" width=20 border=0></a></td>
		</tr>
		</cfoutput>

		<tr>
		   <td class="feed_header" colspan=2><hr></td>
		   <td></td>
		   <td class="feed_header" colspan=2><hr></td>
		   <td></td>
		   <td class="feed_header" colspan=2><hr></td>
		</tr>

         <tr><td valign=top colspan=2>

         <!--- Left --->

		 <cfquery name="home_left" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from home_section
		   where home_section_hub_id = #session.hub# and
		 		home_section_profile_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		 		home_section_location = 'Left'
		   order by home_section_order
		 </cfquery>

	        <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfif #home_left.recordcount# is 0>

				<tr><td class="feed_sub_header" style="font-weight: normal;">No Left Sections exist.</td></tr>

				<cfelse>

					<tr>
					 <td class="feed_sub_header">&nbsp;&nbsp;Order</td>
					 <td class="feed_sub_header" width=300>Section Name</td>
					 <td class="feed_sub_header"></td>
					</tr>

				<cfset counter = 0>

				 <cfloop query="home_left">

				  <cfoutput>
				  <tr><td height=8></td></tr>

                  <cfif counter is 0>
                   <tr bgcolor="ffffff">
                  <cfelse>
                   <tr bgcolor="e0e0e0">
                  </cfif>

					  <td width=75 class="feed_sub_header" valign=top style="font-weight: normal;">&nbsp;&nbsp;&nbsp;&nbsp;#home_left.home_section_order#</td>
					  <td class="feed_sub_header" valign=top><a href="home_edit.cfm?h=#encrypt(home_left.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#"><cfif #home_left.home_section_name# is "">No Name<cfelse>#home_left.home_section_name#</cfif></a></td>
                      <td width=120 align=right>
                      <cfif home_left.home_section_active is 0>
	                      <a href="toggle.cfm?h=#encrypt(home_left.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#&a=1"><img style="padding-right: 10px;" src="#image_virtual#/icon_slider_off.png" height=25></a>
                      <cfelse>
	                      <a href="toggle.cfm?h=#encrypt(home_left.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#&a=0"><img style="padding-right: 10px;" src="#image_virtual#/icon_slider_on.png" height=25></a>
                      </cfif>
                      </td>

                      </tr>

				  </cfoutput>

				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				 </cfloop>

                 </cfif>

	        </table>

         <!--- Center --->

         </td><td width=30>&nbsp;</td><td valign=top colspan=2>

		 <cfquery name="home_center" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from home_section
		   left join component on component_id = home_section_component_id
		   where home_section_hub_id = #session.hub# and
		 		home_section_profile_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		 		home_section_location = 'Center'
		   order by home_section_order
		 </cfquery>

	        <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfif #home_left.recordcount# is 0>

				<tr><td class="feed_sub_header" style="font-weight: normal;">No Center Sections exist.</td></tr>

				<cfelse>
					<tr>
					 <td class="feed_sub_header">&nbsp;&nbsp;Order</td>
					 <td class="feed_sub_header" width=300>Section Name</td>
					 <td class="feed_sub_header"></td>
					</tr>

				<cfset counter = 0>

				 <cfloop query="home_center">

				  <cfoutput>
				  <tr><td height=8></td></tr>

                  <cfif counter is 0>
                   <tr bgcolor="ffffff">
                  <cfelse>
                   <tr bgcolor="e0e0e0">
                  </cfif>

					  <td width=75 class="feed_sub_header" valign=top style="font-weight: normal;">&nbsp;&nbsp;&nbsp;&nbsp;#home_center.home_section_order#</td>
					  <td class="feed_sub_header" valign=top><a href="home_edit.cfm?h=#encrypt(home_center.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#"><cfif #home_center.home_section_name# is "">No Name<cfelse>#home_center.home_section_name#</cfif></a></td>
                      <td align=right width=120>

                      <cfif #trim(home_center.component_setup_path)# is not "">
	                      <a href="#home_center.component_setup_path#?component_id=#component_id#&i=#i#&h=#encrypt(home_center.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/icon_config.png" width=20 border=0 hspace=10 alt="Setup" title="Setup"></a>
                      </cfif>

                      <cfif home_center.home_section_active is 0>
	                      <a href="toggle.cfm?h=#encrypt(home_center.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#&a=1"><img style="padding-right: 10px;" src="#image_virtual#/icon_slider_off.png" height=25></a>
                      <cfelse>
	                      <a href="toggle.cfm?h=#encrypt(home_center.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#&a=0"><img style="padding-right: 10px;" src="#image_virtual#/icon_slider_on.png" height=25></a>
                      </cfif>

                      </td>
				  </tr>

				  </cfoutput>

				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				 </cfloop>

                 </cfif>

	        </table>


         <!--- Right --->

         </td><td width=30>&nbsp;</td><td valign=top colspan=2>

		 <cfquery name="home_right" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from home_section
		   left join component on component_id = home_section_component_id
		   where home_section_hub_id = #session.hub# and
		 		home_section_profile_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		 		home_section_location = 'Right'
		   order by home_section_order
		 </cfquery>


	        <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfif #home_right.recordcount# is 0>

				<tr><td class="feed_sub_header" style="font-weight: normal;">No Right Sections exist.</td></tr>

				<cfelse>
					<tr>
					 <td class="feed_sub_header">&nbsp;&nbsp;Order</td>
					 <td class="feed_sub_header" width=300>Section Name</td>
					 <td class="feed_sub_header"></td>
					</tr>

				<cfset counter = 0>

				 <cfloop query="home_right">

				  <cfoutput>
				  <tr><td height=8></td></tr>

                  <cfif counter is 0>
                   <tr bgcolor="ffffff">
                  <cfelse>
                   <tr bgcolor="e0e0e0">
                  </cfif>

					  <td width=75 class="feed_sub_header" valign=top style="font-weight: normal;">&nbsp;&nbsp;&nbsp;&nbsp;#home_right.home_section_order#</td>
					  <td class="feed_sub_header" valign=top><a href="home_edit.cfm?h=#encrypt(home_right.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#"><cfif #home_right.home_section_name# is "">No Name<cfelse>#home_right.home_section_name#</cfif></a></td>
                      <td align=right width=120>

                      <cfif #trim(home_right.component_setup_path)# is not "">
	                      <a href="#home_right.component_setup_path#?component_id=#component_id#&i=#i#&h=#encrypt(home_right.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/icon_config.png" width=20 border=0 hspace=10 alt="Setup" title="Setup"></a>
                      </cfif>

                      <cfif home_right.home_section_active is 0>
	                      <a href="toggle.cfm?h=#encrypt(home_right.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#&a=1"><img style="padding-right: 10px;" src="#image_virtual#/icon_slider_off.png" height=25></a>
                      <cfelse>
	                      <a href="toggle.cfm?h=#encrypt(home_right.home_section_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#&a=0"><img style="padding-right: 10px;" src="#image_virtual#/icon_slider_on.png" height=25></a>
                      </cfif>
                      </td>
				  </tr>

				  </cfoutput>

				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				 </cfloop>

                 </cfif>

	        </table>








         </td></tr>

       </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

