<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_role
 where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_role_hub_id = #session.hub#
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Role</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header" width=150>Role Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="hub_role_name" value="#edit.hub_role_name#" maxlength="50" required></td></tr>

		   <tr><td class="feed_sub_header" width=150 valign=top>Description</td>
		       <td><textarea name="hub_role_desc" class="input_textarea" style="width: 500px; height: 100px;">#edit.hub_role_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header" width=150>Order</td>
		       <td><input class="input_text" style="width: 75px;" type="number" step=.1 name="hub_role_order" value="#edit.hub_role_order#" required></td></tr>

		   <tr><td class="feed_sub_header" width=175>Display Subscriptions</td>
		       <td>
		           <select name="hub_role_public" class="input_select">
		            <option value=0>Do Not Display
		            <option value=1 <cfif #edit.hub_role_public# is 1>selected</cfif>>Display
		           </select>
		           <span class="link_small_gray">Determines whether user will see this profile in their subscriptions page.</span>
		        </td>
		   </tr>

			<tr><td class="feed_sub_header" valign=top>Role Image</td>
				<td class="feed_sub_header" style="font-weight: normal;">

				<cfif #edit.hub_role_image# is "">
				<input type="file" id="image" onchange="validate_img()" name="hub_role_image">
				<cfelse>
				  <img src="#media_virtual#/#edit.hub_role_image#" width=150><br><br>
				  <input type="file" id="image" onchange="validate_img()" name="hub_role_image"><br><br>
				  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
				 </cfif>

				 </td></tr>


		   <tr><td class="feed_sub_header" width=150>Tour ID</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="hub_role_tour_id" maxlength="100" value="#edit.hub_role_tour_id#">

		       <span class="link_small_gray">Leave blank if no <a href="http://www.helphero.co" target="_blank"><u>Helphero Tour ID</u></a>.</span>

		       </td></tr>

		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr>
		       <td class="feed_sub_header" width=165>Limits</td>
		       <td class="feed_sub_header" width=10%>Opp Boards</td>
		       <td class="feed_sub_header" width=10%>Deals</td>
		       <td class="feed_sub_header" width=10%>Portfolios</td>
		       <td class="feed_sub_header" width=100>Market Reports</td>

           </tr>

           <tr><td width=13.2%>&nbsp;</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_limit_boards" step = 1 value=#edit.hub_role_limit_boards#></td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_limit_deals" step = 1 value=#edit.hub_role_limit_deals#></td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_limit_portfolios" step = 1 value=#edit.hub_role_limit_portfolios#></td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_limit_reports" step = 1 value=#edit.hub_role_limit_reports#></td>

		       </tr>

            <tr><td></td>
                <td class="link_small_gray" colspan=3>Leave blank for unlimited.</td>
            </tr>

		   </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr>
		       <td class="feed_sub_header" width=13.2%>License Cost</td>
		       <td class="feed_sub_header" width=10%>Monthly</td>
		       <td class="feed_sub_header" width=10%>Annually</td>
		       <td class="feed_sub_header" width=10%>Allow Trial?</td>
		       <td class="feed_sub_header">Trial Duration</td>

           </tr>

           <tr><td>&nbsp;</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_monthly" value=#edit.hub_role_monthly# step="1" required></td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_annually" value=#edit.hub_role_annually# step="1" required></td>

		       <td><select name="hub_role_trial" class="input_select" style="width: 100px;">
		           <option value=0>No
		           <option value=1 <cfif edit.hub_role_trial is 1>selected</cfif>>Yes
		       </td>

		       <td><input class="input_text" style="width: 100px;" type="number" name="hub_role_trial_duration" value=#edit.hub_role_trial_duration# step="1" min="0" max="30" required>

		       <span class="link_small_gray">Days (Max 30 Days)</span>

		       </td></tr>

		   </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

		   <tr><td width=175>&nbsp;</td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>

		   <input type="hidden" name="i" value=#i#>

		   </cfoutput>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

