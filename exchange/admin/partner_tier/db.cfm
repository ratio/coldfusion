<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into partner_tier (partner_tier_name, partner_tier_desc, partner_tier_hub_id, partner_tier_order)
	 values ('#partner_tier_name#','#partner_tier_desc#',#session.hub#,#partner_tier_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update partner_tier
	 set partner_tier_name = '#partner_tier_name#',
	     partner_tier_desc = '#partner_tier_desc#',
	     partner_tier_order = #partner_tier_order#
	 where partner_tier_id = #partner_tier_id# and
	       partner_tier_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete partner_tier
	 where partner_tier_id = #partner_tier_id# and
	       partner_tier_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

