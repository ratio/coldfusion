<cfinclude template="/exchange/security/check.cfm">

<cfquery name="partner_tier" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from partner_tier
  where partner_tier_hub_id = #session.hub#
  order by partner_tier_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Company Tiers</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Company Tier has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Company Tier has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Company Tier has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Company Tier</a></td></tr>

      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #partner_tier.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Company Tiers exist.</td></tr>

        <cfelse>
			<tr>
				<td class="feed_sub_header">Order</td>
				<td class="feed_sub_header">Tier Name</td>
				<td class="feed_sub_header">Description</td>
			</tr>
        </cfif>

        <cfset counter = 0>

         <cfoutput query="partner_tier">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top width=100><a href="edit.cfm?partner_tier_id=#partner_tier_id#">#partner_tier_order#</a></td>
              <td class="feed_sub_header" valign=top width=300><a href="edit.cfm?partner_tier_id=#partner_tier_id#">#partner_tier_name#</a></td>
              <td class="feed_sub_header" valign=top><a href="edit.cfm?partner_tier_id=#partner_tier_id#">#partner_tier_desc#</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

