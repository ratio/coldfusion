<cfinclude template="/exchange/security/check.cfm">

<cfquery name="ac" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from ac
  left join hub_role on hub_role_id = ac_role_id
  where ac_hub_id = #session.hub#
  order by ac_code
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Invitation Codes</td>
	       <td class="feed_sub_header" align=right></td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">Invitation Codes allow you to associate and auto provision roles for users during the new user sign-up process.</td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Invitation Code has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Invitation Code has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Invitation Code has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Invitation Code</a></td></tr>

      </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #ac.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Invitation Codes exist.</td></tr>

        <cfelse>
				<td class="feed_sub_header">Code</b></td>
				<td class="feed_sub_header">Name / Campaign</b></td>
				<td class="feed_sub_header">Role</b></td>
				<td class="feed_sub_header" align=center>Start</b></td>
				<td class="feed_sub_header" align=center>End</b></td>

			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="ac">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header"><a href="edit.cfm?ac_id=#ac_id#" width=100>#ac_code#</a></td>
              <td class="feed_sub_header"><a href="edit.cfm?ac_id=#ac_id#" width=700>#ac_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">#hub_role_name#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#dateformat(ac_start_date,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#dateformat(ac_end_date,'mm/dd/yyyy')#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>


	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

