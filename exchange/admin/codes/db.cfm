<cfinclude template="/exchange/security/check.cfm">


<cfif #button# is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into ac
	 (
	  ac_name,
	  ac_code,
	  ac_role_id,
	  ac_start_date,
	  ac_end_date,
	  ac_hub_id
	 )
	 values
	 (
	 '#ac_name#',
	 '#ac_code#',
	  #ac_role_id#,
	 '#ac_start_date#',
	 '#ac_end_date#',
	  #session.hub#
	 )
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update ac
	 set ac_name = '#ac_name#',
	     ac_code = '#ac_code#',
	     ac_role_id = #ac_role_id#,
	     ac_start_date = '#ac_start_date#',
	     ac_end_date = '#ac_end_date#'
	 where ac_id = #ac_id# and
	       ac_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete ac
	 where ac_id = #ac_id# and
	       ac_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>


