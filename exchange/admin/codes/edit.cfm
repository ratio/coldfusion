<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from ac
 where ac_id = #ac_id# and
       ac_hub_id = #session.hub#
</cfquery>

<cfquery name="roles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub#
  order by hub_role_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <form action="db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Invitation Code</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

		   <tr><td class="feed_sub_header" width=150>Code</td>
		       <td><input class="input_text" style="width: 200px;" type="text" name="ac_code" maxlength="100" value="#edit.ac_code#" required></td></tr>

		   <tr><td class="feed_sub_header" width=175>Name / Campaign</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="ac_name" maxlength="300" value="#edit.ac_name#" required></td></tr>

           </cfoutput>

		   <tr><td class="feed_sub_header" width=150>Role</td>
		       <td>
		       <select name="ac_role_id" class="input_select">
		       <cfoutput query="roles">
		        <option value=#hub_role_id# <cfif hub_role_id is edit.ac_role_id>selected</cfif>>#hub_role_name#
		       </cfoutput>
		       </select>

           </td></tr>

           <cfoutput>

		   <tr><td class="feed_sub_header" width=150>Start</td>
		       <td><input class="input_text" type="date" name="ac_start_date" value="#edit.ac_start_date#" required></td></tr>

		   <tr><td class="feed_sub_header" width=150>End</td>
		       <td><input class="input_text" type="date" name="ac_end_date" value="#edit.ac_end_date#" required></td></tr>

		   </cfoutput>

           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

           <cfoutput>
           <input type="hidden" name="ac_id" value=#ac_id#>
           </cfoutput>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>
		  </table>

      </td></tr>
     </table>


	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

