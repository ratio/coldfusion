<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">
  <cfinclude template="/exchange/hubs/admin/hub_profile.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

		<cfquery name="invite_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from usr
		 where usr_email = '#email#'
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">Invite Member</td>
		     <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
		 <tr><td height=10></td></tr>
         <tr><td class="feed_option"><b>Name:</b>&nbsp;<cfoutput>#invite_info.usr_first_name# #invite_info.usr_last_name#</cfoutput></td></tr>
         <tr><td class="feed_option"><b>Email Address:</b>&nbsp;<cfoutput>#email#</cfoutput></td></tr>
         <tr><td height=10></td></tr>

            <form action="exchange_invite.cfm" method="post">

  		    <tr><td class="feed_option"><b>Subject</b></td></tr>
  		    <tr><td class="feed_option"><input type="text" name="subject" size=80 required></td></tr>
  		    <tr><td class="feed_option"><b>Invitation Message</b></td></tr>
  		    <tr><td class="feed_option"><textarea name="message" cols=90 rows=5 required></textarea></td></tr>
  		    <tr><td class="feed_option"><b>Invitation Code</b></td></tr>
  		    <tr><td class="feed_option"><input type="text" name="invite_code" size=30 required></td></tr>
            <tr><td height=10></td></tr>
			<tr><td><input class="button_blue" style="font-size: 11px; height: 24px; width: 100px;" type="submit" name="button" value="Send Invitation"></td></tr>

  		    <cfoutput>
  		     <input type="hidden" name="email" value="#email#">
  		    </cfoutput>

       </table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>