<cfinclude template="/exchange/security/check.cfm">

<cfquery name="types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_tab
  where hub_tab_hub_id = #session.hub# and
        hub_tab_user_type_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  order by hub_tab_order
</cfquery>

<cfquery name="types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr_type
  where usr_type_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        usr_type_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header_2.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">TABS</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Admin Tools</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" colspan=2><a href="add.cfm">Add Tab</a></td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #types.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Tabs Exist.</td></tr>

        <cfelse>

			<tr><td class="feed_sub_header" width=5%>Order</td>
				<td class="feed_sub_header" width=300>Tab</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="types">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=middle><a href="edit.cfm?i=#encrypt(hub_tab_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#hub_tab_order#</a></td>
              <td class="feed_sub_header" valign=middle><a href="edit.cfm?i=#encrypt(hub_tab_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#hub_tab_name#</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

