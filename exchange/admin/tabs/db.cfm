<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into hub_tab (hub_tab_name, hub_tab_order, hub_tab_hub_id)
	 values ('#hub_tab_name#',#hub_tab_order#,#session.hub#)
	</cfquery>

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete hub_tab
	 where hub_tab_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub_tab
	 set hub_tab_name = '#hub_tab_name#',
	     usr_tab_order = #hub_tab_order#
	 where hub_tab_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">