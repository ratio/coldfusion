<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  update hub
  set hub_support_name = '#hub_support_name#',
	  hub_support_email = '#hub_support_email#',
	  hub_support_phone = '#hub_support_phone#'
  where hub_id = #session.hub#
</cfquery>

<cflocation URL="/exchange/hubs/admin/" addtoken="no">
