<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Tab">

	   <cfquery name="insert_tab" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into tab
		  (tab_link, tab_link_location, tab_updated, tab_hub_id, tab_name, tab_title, tab_short_desc, tab_content, tab_order)
		  values
		  ('#tab_link#',#tab_link_location#, #now()#, #session.hub#, '#tab_name#','#tab_title#','#tab_short_desc#','#tab_content#',#tab_order#)
		</cfquery>

<cflocation URL="tabs.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	   <cfquery name="update_tab" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    update tab
	    set tab_name = '#tab_name#',
	        tab_updated = #now()#,
	        tab_title = '#tab_title#',
	        tab_link ='#tab_link#',
	        tab_link_location = #tab_link_location#,
	        tab_short_desc = '#tab_short_desc#',
	        tab_content = '#tab_content#',
	        tab_order = #tab_order#
	    where tab_id = #tab_id#
	   </cfquery>

<cflocation URL="tabs.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	   <cfquery name="delete_tab" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete tab
		  where tab_id = #tab_id#
		</cfquery>

<cflocation URL="tabs.cfm?u=3" addtoken="no">


</cfif>

<cflocation URL="tabs.cfm" addtoken="no">