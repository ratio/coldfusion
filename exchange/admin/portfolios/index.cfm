<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from portfolio
  join usr on usr_id = portfolio_usr_id
  where portfolio_hub_id = #session.hub#

  <cfif sv is 1>
   order by portfolio_name ASC
  <cfelseif sv is 10>
   order by portfolio_name DESC
  <cfelseif sv is 2>
   order by usr_last_name ASC, usr_first_name ASC
  <cfelseif sv is 20>
   order by usr_last_name DESC, usr_first_name ASC
  <cfelseif sv is 3>
   order by portfolio_company_access ASC
  <cfelseif sv is 30>
   order by portfolio_company_access DESC
  </cfif>

</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">PORTFOLIOS</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/index.cfm">Admin Tools</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <cfif isdefined("u")>

        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">Company Access has been granted.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">Company Access has been revoked.</td></tr>
        </cfif>
       </cfif>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif portfolios.recordcount is 0>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No Portfolios have been created.</td></tr>

        <cfelse>

			<tr>
                <td class="feed_sub_header"><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Portfoio Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
			    <td class="feed_sub_header">Description</td>
                <td class="feed_sub_header"><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>User</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
                <td class="feed_sub_header" align=right><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Company Access</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
			</tr>

			<tr><td height=10></td></tr>

        <cfset counter = 0>

        <cfloop query="portfolios">

         <cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top width=300>#portfolio_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;" valign=top width=500>#portfolio_desc#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;" valign=top>#usr_last_name# #usr_first_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>

			  <cfif portfolios.portfolio_company_access is 1>
				  <a href="company_grant.cfm?i=#encrypt(portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&l=0"><img src="/images/icon_slider_on.png" width=60 border=0 hspace=5 title="Revoke Company View Access" alt="Revoke Company View Access"></a>
			  <cfelse>
				  <a href="company_grant.cfm?i=#encrypt(portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&l=1"><img src="/images/icon_slider_off.png" width=60 border=0 hspace=5 title="Grant Company View Access" alt="Grant Company View Access"></a>
			  </cfif>

              </td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfloop>

        </cfif>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

