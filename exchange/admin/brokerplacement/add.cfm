<cfinclude template="/exchange/security/check.cfm">

<cfquery name="area" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from area
  where area_id = #area_id#
</cfquery>

<cfquery name="brokerlist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company
  join broker on broker_company_id = company_id
  where broker_status_id = 1
  <cfif isdefined("session.hub")>
   and broker_hub_id = #session.hub#
  </cfif>
  order by company_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">
	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Add Broker Placement to Page</td></tr>
	   <tr><td><hr></td></tr>
	   <tr><td>&nbsp;</td></tr>

        <tr><td>

        <form action="db.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfoutput>

         <tr><td class="feed_sub_header" width=200>Callout Page</td>
             <td class="feed_sub_header" style="font-weight: normal;">#area.area_name#</td></tr>

         <input type="hidden" name="broker_callout_area_id" value=#area_id#>

        </cfoutput>

         <tr><td class="feed_sub_header" width=200>Broker</td>
             <td>

             <select name="broker_callout_company_id" class="input_select">
             <cfoutput query="brokerlist">
               <option value=#company_id#>#company_name#
             </cfoutput>
             </select>


             </td></tr>


         <tr><td class="feed_sub_header" width=200>Order</td>
             <td><input type="number" name="broker_callout_order" step="1" class="input_text" style="width: 100px;"></td></tr>

         <tr><td></td>
             <td><input type="submit" name="button" class="button_blue_large" value="Add"></td></tr>

	    </table>

	    </form>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

