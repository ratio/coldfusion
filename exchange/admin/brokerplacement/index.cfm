<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="area" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from area
	  order by area_name
	</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Broker Page Placement</td></tr>
	   <tr><td><hr></td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>
			    <td class="feed_sub_header">PAGE CALLOUTS</td>
			    <td class="feed_option" align=right></td>
			</tr>

			<tr><td height=10></td></tr>

        <cfloop query="area">

         <cfoutput>
         <tr><td class="feed_option" bgcolor="e0e0e0"><b>#area_name#</b></td>
             <td class="feed_option" bgcolor="e0e0e0" align=right><a href="add.cfm?area_id=#area_id#"><b>Add Broker</b></a></tr>
         </cfoutput>

		 <cfquery name="brokers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from broker_callout
		   join company on company_id = broker_callout_company_id
		   where broker_callout_area_id = #area.area_id#

		   <cfif isdefined("session.hub")>
		    and broker_callout_hub_id = #session.hub#
		   <cfelse>
		    and broker_callout_hub_id = null
		   </cfif>
		   order by broker_callout_order
		 </cfquery>

         <cfset counter = 0>

         <cfif brokers.recordcount is 0>
          <tr><td class="feed_option" colspan=3>No Brokers exist.</td></tr>
         <cfelse>

         <cfoutput query="brokers">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>
              <td class="feed_option" valign=top colspan=2><a href="edit.cfm?area_id=#area.area_id#&id=#brokers.broker_callout_id#">#brokers.broker_callout_order#.  #brokers.company_name#</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>

         <tr><td>&nbsp;</td></tr>

        </cfloop>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

