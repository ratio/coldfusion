<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into broker_callout
	 (
	  broker_callout_area_id,
	  broker_callout_company_id,
	  broker_callout_order,
	  broker_callout_hub_id
	  )
	  values
	  (
	  #broker_callout_area_id#,
	  #broker_callout_company_id#,
	  #broker_callout_order#,
	  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>
	  )
	</cfquery>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Save">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update broker_callout
	 set broker_callout_order = #broker_callout_order#,
	 broker_callout_company_id = #broker_callout_company_id#
	 where broker_callout_id = #broker_callout_id#
	</cfquery>

	<cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete broker_callout
	 where broker_callout_id = #broker_callout_id#
	</cfquery>

	<cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>
