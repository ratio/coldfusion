<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

        <cfif isdefined("remove_photo")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select hub_logo from hub
			  where hub_id = #session.hub#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.hub_logo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.hub_logo#">
        	</cfif>

        </cfif>

		<cfif #hub_logo# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select hub_logo from hub
			  where hub_id = #session.hub#
			</cfquery>

			<cfif #getfile.hub_logo# is not "">
             <cfif FileExists("#media_path#\#getfile.hub_logo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.hub_logo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "hub_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>


	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set hub_name = '#hub_name#',
	     hub_show_app_library = <cfif isdefined("hub_show_app_library")>1<cfelse>null</cfif>,
	     hub_name_short = '#hub_name_short#',
	     hub_newaccount_go = #hub_newaccount_go#,
	     hub_signup = #hub_signup#,
	     hub_desc = '#hub_desc#',
	     hub_website = '#hub_website#',
	     hub_logout_page = '#hub_logout_page#',
	     hub_login_page = '#hub_login_page#',
	     hub_subdomain_name = '#hub_subdomain_name#',
	     hub_forgot_password_page = '#hub_forgot_password_page#',
	     hub_city = '#hub_city#',
	     hub_zip = '#hub_zip#',
	     hub_login_page_title = '#hub_login_page_title#',
	     hub_login_page_desc = '#hub_login_page_desc#',
	     hub_state = '#hub_state#',
	     hub_password_policy = #hub_password_policy#,
	     hub_tags = '#hub_tags#',
         hub_join = #hub_join#,
         hub_default_role_id = #hub_default_role_id#,
	     hub_sso_path = '#hub_sso_path#',
	     hub_tag_line = '#hub_tag_line#',
	     hub_header_bgcolor = '#hub_header_bgcolor#',

		  <cfif #hub_logo# is not "">
		   hub_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_photo")>
		   hub_logo = null,
		  </cfif>

	     hub_updated = #now()#,
	     hub_support_name = '#hub_support_name#',
	     hub_support_email = '#hub_support_email#',
	     hub_support_phone = '#hub_support_phone#',
         hub_type_id = #hub_type_id#
	 where hub_id = #session.hub#
	</cfquery>

    <cflocation URL="/exchange/admin/index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Cancel">

    <cflocation URL="/exchange/admin/index.cfm" addtoken="no">

</cfif>

