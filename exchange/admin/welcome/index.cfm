<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Welcome Message</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/index.cfm">Settings</td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="db.cfm" method="post" enctype="multipart/form-data" >

           <cfoutput>

           <cfif isdefined("u")>
            <tr><td class="feed_sub_header" style="color: green;" colspan=2>Welcome message has been successfully updated.</td></tr>
           </cfif>

           <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>

           This message will be the first thing a new user sees when logging in for the first time.

           </td></tr>
		   <tr><td><textarea name="hub_welcome_message" name="hub_welcome_message" class="input_textarea" style="width: 1000px; height: 400px;">#edit.hub_welcome_message#</textarea>	   </td></tr>

           <tr><td class="link_small_gray" colspan=2>The Welcome Message can include HTML tags such as bold, underline or links to external URLs, images or graphics.</td></tr>




           <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Active</b>&nbsp;&nbsp;
                  <select name="hub_welcome_message_active" class="input_select">
                   <option value=0>No
                   <option value=1 <cfif edit.hub_welcome_message_active is 1>selected</cfif>>Yes
                  </select>

                  <span class="link_small_gray">Yes indicates the messasge will be displayed to new users.</span>

                  </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10>

           </cfoutput>
           </form>

	    </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

