<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Agreement">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into agreement
	 (
	  agreement_name,
	  agreement_desc,
	  agreement_order,
	  agreement_hub_id
	  )
	  values
	  (
	  '#agreement_name#',
	  '#agreement_desc#',
	   #agreement_order#,
	   #session.hub#
	   )
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete agreement
	 where agreement_id = #agreement_id# and
	       agreement_hub_id = #session.hub#
	</cfquery>

   <cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Save">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update agreement
	 set agreement_name = '#agreement_name#',
	     agreement_desc = '#agreement_desc#',
	     agreement_order = #agreement_order#
	 where agreement_id = #agreement_id# and
	       agreement_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

</cfif>

