<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Cancel">

     <cflocation URL="/exchange/company/" addtoken="no">

<cfelseif #button# is "Add Product or Service">

	<cfif #product_attachment# is not "">
		<cffile action = "upload"
		 fileField = "product_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert product
	  (product_order, product_public, product_attachment, product_keywords, product_url, product_patent_details, product_name, product_desc, product_pitch, product_diff, product_updated, product_company_id, product_pricing, product_type, product_topic)
	  values
	  (#product_order#, <cfif isdefined("product_public")>1<cfelse>null</cfif>,<cfif #product_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,'#product_keywords#','#product_url#','#product_patent_details#', '#product_name#','#product_desc#','#product_pitch#','#product_diff#',#now()#,#session.company_id#,'#product_pricing#','#product_type#',<cfif #isdefined("product_topic")#>'#product_topic#'<cfelse>null</cfif>)
	</cfquery>

	<cflocation URL="/exchange/company/products/index.cfm?l=3&u=3" addtoken="no">

<cfelseif #button# is "Delete">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select product_attachment from product
		  where (product_id = #product_id#) and
				(product_company_id = #session.company_id#)
		</cfquery>

		<cfif remove.product_attachment is not "">
         <cfif fileexists("#media_path#\#remove.product_attachment#")>
			 <cffile action = "delete" file = "#media_path#\#remove.product_attachment#">
         </cfif>
		</cfif>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete product
		  where (product_id = #product_id#) and
				(product_company_id = #session.company_id#)
		</cfquery>

	</cftransaction>

	<cflocation URL="/exchange/company/products/index.cfm?l=3&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select product_attachment from product
		  where product_id = #product_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.product_attachment#")>
			<cffile action = "delete" file = "#media_path#\#remove.product_attachment#">
		</cfif>

	</cfif>

	<cfif #product_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select product_attachment from product
		  where product_id = #product_id#
		</cfquery>

		<cfif #getfile.product_attachment# is not "">
         <cfif fileexists("#media_path#\#getfile.product_attachment#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.product_attachment#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "product_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update product
	  set product_name = '#product_name#',

		  <cfif #product_attachment# is not "">
		   product_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   product_attachment = null,
		  </cfif>

	      product_keywords = '#product_keywords#',
	      product_desc = '#product_desc#',
	      product_public = <cfif isdefined("product_public")>1<cfelse>null</cfif>,
	      product_patent_details = '#product_patent_details#',
	      product_pitch = '#product_pitch#',
	      product_order = #product_order#,
	      product_diff = '#product_diff#',
	      product_url = '#product_url#',
	      product_updated = #now()#,
	      product_pricing = '#product_pricing#',
	      <cfif isdefined("product_topic_id")>
	      	product_topic = '#product_topic_id#',
	      <cfelse>
	        product_topic = null,
	      </cfif>
	      product_type = '#product_type#'
	  where (product_id = #product_id# ) and
	        (product_company_id = #session.company_id#)
	</cfquery>

</cfif>

<cflocation URL="/exchange/company/products/index.cfm?l=3&u=1" addtoken="no">