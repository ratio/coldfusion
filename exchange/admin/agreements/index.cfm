<cfinclude template="/exchange/security/check.cfm">

<cfquery name="agreements" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from agreement
  where agreement_hub_id = #session.hub#
  order by agreement_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Agreements</td>
	       <td class="feed_sub_header" align=right>
	       <a href="add.cfm">Add Agreement</a>&nbsp;|&nbsp;
	       <a href="/exchange/admin/index.cfm">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <cfif isdefined("u")>

        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">Agreement has been successfully added.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">Agreement has been successfully updated.</td></tr>
        <cfelseif u is 3>
         <tr><td class="feed_sub_header" style="color: red;">Agreement has been successfully deleted.</td></tr>
        </cfif>
       </cfif>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif agreements.recordcount is 0>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No Agreements have been created.</td></tr>

        <cfelse>

			<tr>
			    <td class="feed_sub_header" width=100>Order</td>
			    <td class="feed_sub_header" width=400>Agreement</td>
			    <td class="feed_sub_header">Description</td>
			</tr>

			<tr><td height=10></td></tr>

        <cfset counter = 0>

        <cfloop query="agreements">

         <cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top width=100><a href="edit.cfm?agreement_id=#agreement_id#">#agreement_order#</a></td>
              <td class="feed_sub_header" valign=top width=300><a href="edit.cfm?agreement_id=#agreement_id#">#agreement_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;">#agreement_desc#</a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfloop>

        </cfif>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

