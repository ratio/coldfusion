<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from agreement
 where agreement_id = #agreement_id# and
       agreement_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Agreement</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

		   <tr><td class="feed_sub_header">Agreement Name</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="agreement_name" value="#edit.agreement_name#" required></td></tr>
		   <tr><td valign=top class="feed_sub_header">Description</td>
		       <td><textarea class="input_textarea" style="width: 600px; height: 200px;" name="agreement_desc">#edit.agreement_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header" width=175>Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" value=#edit.agreement_order# name="agreement_order" required></td></tr>


		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td></td><td>

		   <input class="button_blue_large" type="submit" name="button" value="Save" vspace=10>
           &nbsp;&nbsp;
           <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Agreement\r\nAre you sure you want to delete this Agreement?');">

		   </td></tr>

		   <input type="hidden" name="agreement_id" value=#agreement_id#>

		   </cfoutput>

		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

