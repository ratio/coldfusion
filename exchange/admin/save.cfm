<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

        <cfif isdefined("remove_photo")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select hub_logo from hub
			  where hub_id = #session.hub#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.hub_logo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.hub_logo#">
        	</cfif>

        </cfif>

		<cfif #hub_logo# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select hub_logo from hub
			  where hub_id = #session.hub#
			</cfquery>

			<cfif #getfile.hub_logo# is not "">
             <cfif FileExists("#media_path#\#getfile.hub_logo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.hub_logo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "hub_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set hub_name = '#hub_name#',
	     hub_desc = '#hub_desc#',
	     hub_advisor_id = #hub_advisor_id#,
	     hub_logout_page = '#hub_logout_page#',
	     hub_login_page = '#hub_login_page#',
	     hub_subdomain_name = '#hub_subdomain_name#',
	     hub_forgot_password_page = '#hub_forgot_password_page#',
	     hub_city = '#hub_city#',
	     hub_home_about = <cfif isdefined("hub_home_about")>1<cfelse>0</cfif>,
	     hub_home_sponsors = <cfif isdefined("hub_home_sponsors")>1<cfelse>0</cfif>,
	     hub_home_child_hubs = <cfif isdefined("hub_home_child_hubs")>1<cfelse>0</cfif>,
	     hub_state = '#hub_state#',
	     hub_tags = '#hub_tags#',
	     hub_tag_line = '#hub_tag_line#',
	     hub_header_bgcolor = '#hub_header_bgcolor#',
	     hub_long_desc = '#hub_long_desc#',

		  <cfif #hub_logo# is not "">
		   hub_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_photo")>
		   hub_logo = null,
		  </cfif>

	     hub_updated = #now()#,
         hub_owner_id = #hub_owner_id#,
	     hub_support_name = '#hub_support_name#',
	     hub_support_email = '#hub_support_email#',
	     hub_support_phone = '#hub_support_phone#',
         hub_type_id = #hub_type_id#
	 where hub_id = #session.hub#
	</cfquery>

    <cflocation URL="/exchange/hubs/admin/index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Cancel">

    <cflocation URL="/exchange/hubs/admin/" addtoken="no">

</cfif>

