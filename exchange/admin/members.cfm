<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	   <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select * from hub_xref
	    left join usr on usr.usr_id = hub_xref.hub_xref_usr_id
	    where hub_xref.hub_xref_hub_id = #session.hub#
	    order by usr.usr_last_name
	   </cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Members</td>

      <td class="feed_option" align=right>
      <form action="invite.cfm" method="post">

      <b>Invite Member (email address)</b>&nbsp;&nbsp;<input type="email" name="email" size=30 required>
		 &nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Find">&nbsp;
      </form>

      </td></tr>

	  <tr><td height=5></td></tr>
	  <cfif isdefined("u")>
	   <cfif u is 1>
		<tr><td class="feed_option"><font color="green"><b>HUB profile has been successfully updated.</b></font></td></tr>
	   <cfelseif u is 2>
		<tr><td class="feed_option"><font color="green"><b>Member invitation has been sent.</b></font></td></tr>
	   <cfelseif u is 3>
		<tr><td class="feed_option"><font color="green"><b>Member information has been successfully updated.</b></font></td></tr>
	   <cfelseif u is 4>
		<tr><td class="feed_option"><font color="green"><b>Member has been removed from the HUB.</b></font></td></tr>
	   </cfif>
	  </cfif>

      <tr><td>&nbsp;</td></tr>
       </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr>
		       <td class="feed_option"><b>Name</b></td>
		       <td class="feed_option"><b>Email</b></td>
		       <td class="feed_option"><b>Phone</b></td>
		       <td class="feed_option"><b>Invitation Code</b></td>
		       <td class="feed_option"><b>Invitation Sent</b></td>
		       <td class="feed_option"><b>Invitation Accepted</b></td>
		       <td class="feed_option"><b>Joined</b></td>
		       <td class="feed_option" align=center><b>Default</b></td>
		       <td class="feed_option"><b>Role</b></td>
		       <td class="feed_option" align=right><b>Status</b></td>
		       <td class="feed_option" align=right></td>
		   </tr>

	    <cfset counter = 0>

	    <cfoutput query="members">

	       <cfif counter is 0>
	        <tr bgcolor="ffffff">
	       <cfelse>
	        <tr bgcolor="e0e0e0">
	       </cfif>

	          <td class="feed_option"><a href="edit_member.cfm?i=#usr_id#">#usr_last_name#, #usr_first_name#</a></td>
	          <td class="feed_option">#usr_email#</td>
	          <td class="feed_option">#usr_phone#</td>
	          <td class="feed_option">#hub_xref_invitation_code#</td>
	          <td class="feed_option">#dateformat(hub_xref_invite_sent,'mm/dd/yyyy')#</td>
	          <td class="feed_option">#dateformat(hub_xref_invite_accepted,'mm/dd/yyyy')#</td>
	          <td class="feed_option">#dateformat(hub_xref_joined,'mm/dd/yyyy')#</td>
	          <td class="feed_option" align=center><cfif #hub_xref_default# is 1>Yes<cfelse>No</cfif></td>
	          <td class="feed_option"><cfif #hub_xref_usr_role# is 1>Admin<cfelseif #hub_xref_usr_role# is 2>User</cfif></td>
	          <td class="feed_option" align=right><cfif #hub_xref_active# is 1>Active<cfelse>Not Active</cfif></td>
	          <td class="feed_option" align=right><a href="remove.cfm?id=#hub_xref_id#" onclick="return confirm('Remove Member.\r\nRemoving the member will disassociate them with your HUB and require them to rejoin. Are you sure you want to proceed?');">Remove</a></td>
	       </tr>

	    <cfif counter is 0>
	     <cfset counter = 1>
	    <cfelse>
	     <cfset counter = 0>
	    </cfif>

	    </cfoutput>

       </table>




	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>