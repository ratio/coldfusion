
<cfinclude template="/exchange/security/check.cfm">

<cfquery name="dupes" datasource="#datasource#" username="#username#" password="#password#">
SELECT
    company_name, company_website, company_city, company_duns, cast(company_long_desc as varchar(max)) as long, company_keywords, company_state, COUNT(*) as count
FROM
    company
<cfif isdefined("keyword")>
where company_name like '#keyword#%'
</cfif>
GROUP BY
    company_name, company_website, company_city, company_duns, cast(company_long_desc as varchar(max)), company_keywords, company_state
HAVING
    COUNT(*) > 1
order by count DESC
</cfquery>

<html>
<head>
	<title>Exchange</title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset perpage = 100>
<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt dupes.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(dupes.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>
  <!---
  <cfinclude template="/exchange/include/header.cfm"> --->

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Duplicate Companies</td>
	       <td class="feed_option" align=right></td></tr>

       <tr><td height=20></td></tr>
       <tr>

           		    <td class="feed_sub_header" valign=top>
           		    <cfoutput>

					<cfif dupes.recordcount GT #perpage#>
						<b>#numberformat(thisPage,'9,999')# of #numberformat(totalPages,'9,999')#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt dupes.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>
					</cfoutput>

				</td>

         <form action="index.cfm" method="post">
          <td class="feed_sub_header" align=right>
           Search:&nbsp;&nbsp;
           <input type="text" class="input_text" name="keyword" style="width: 300px;">
           &nbsp;&nbsp;
           <input type="submit" name="button" value="Search" class="button_blue">

          </td>
         </form>


         </tr>

         <cfif isdefined("keyword")>
         <cfoutput>
         <tr><td class="feed_option" colspan=2><b>You searched for <i>#keyword#</i></b></td></tr>
         </cfoutput>

         </cfif>

	  </table>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td>&nbsp;</td></tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td class="feed_option"><font color="green"><b>HUB has been successfully deleted.</b></font></td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_option"><font color="green"><b>HUB information has been udpated successfully.</b></font></td></tr>
        </cfif>
         <tr><td>&nbsp;</td></tr>
       </cfif>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #dupes.recordcount# is 0>

        <tr><td class="feed_option"><b>No duplicates exist.</b></td></tr>

        <cfelse>

			<tr>
			    <td class="feed_option"><b>Company Name</b></td>
			    <td class="feed_option"><b>Duns</b></td>
				<td class="feed_option"><b>Website</b></td>
				<td class="feed_option"><b>Description</b></td>
				<td class="feed_option"><b>Keywords</b></td>
				<td class="feed_option"><b>City</b></td>
				<td class="feed_option"><b>State</b></td>
				<td class="feed_option" align=right><b>Count</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

				<cfoutput query="dupes" startrow="#url.start#" maxrows="#perpage#">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_option" valign=top width=350><a href="open.cfm?company_name=#dupes.company_name#"><b>#ucase(dupes.company_name)#</b></a></td>
              <td class="feed_option" valign=top>#dupes.company_duns#</td>
              <td class="feed_option" valign=top>#dupes.company_website#</td>
              <td class="feed_option" valign=top>#dupes.long#</td>
              <td class="feed_option" valign=top>#dupes.company_keywords#</td>
              <td class="feed_option" valign=top>#dupes.company_city#</td>
              <td class="feed_option" valign=top>#dupes.company_state#</td>
              <td class="feed_option" valign=top align=right>#dupes.count#</td>

           </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<!---
<cfinclude template="/exchange/include/footer.cfm"> --->

</body>
</html>

