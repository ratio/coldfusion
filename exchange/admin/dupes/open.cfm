
<cfinclude template="/exchange/security/check.cfm">

<cfquery name="open" datasource="#datasource#" username="#username#" password="#password#">
SELECT * from company
where company_name = '#company_name#'
</cfquery>

<html>
<head>
	<title>Exchange</title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("s")>
 <cfset s = 0>
</cfif>

  <!---
  <cfinclude template="/exchange/include/header.cfm"> --->

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Duplicate Companies</td>
	       <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>

       <tr><td height=10></td></tr>

       <cfoutput>
       <tr><td class="feed_option"><b>Total Potential Duplicates: #open.recordcount#</b></td></tr>
       </cfoutput>

       <tr><td height=10></td></tr>

	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td>&nbsp;</td></tr>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #open.recordcount# is 0>

        <tr><td class="feed_option"><b>No duplicates exist.</b></td></tr>

        <cfelse>

        <tr>
        <td colspan=10 class="feed_option"><b>
        <cfoutput>
        <a href="open.cfm?s=1&company_name=#company_name#">Select All</a>&nbsp;|&nbsp;<a href="open.cfm?s=0&company_name=#company_name#">Deselect All</a></b>
        </cfoutput>

        </td></tr>
        <tr><td height=10></td></tr>

			<tr>
			    <td class="feed_option"><b>Delete</b></td>
				<td class="feed_option"><b>Edit</b></td>
			    <td class="feed_option"><b>Company Name</b></td>
			    <td class="feed_option" align=center><b>Duns</b></td>
			    <td class="feed_option"><b>CB ID</b></td>
				<td class="feed_option" align=center><b>Products</b></td>
				<td class="feed_option"><b>Website</b></td>
				<td class="feed_option"><b>Short Description</b></td>
				<td class="feed_option"><b>Long Description</b></td>
				<td class="feed_option"><b>Keywords</b></td>
				<td class="feed_option"><b>City</b></td>
				<td class="feed_option"><b>State</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

        <form action="delete.cfm" method="post">

		<cfloop query="open">

		<cfquery name="products" datasource="#datasource#" username="#username#" password="#password#">
		select count(product_id) as total from product
		where product_company_id = #open.company_id#
		</cfquery>


		<cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_option" width=60 valign=top><input type="checkbox" name="company_id" value=#open.company_id# style="width: 18px; height: 18px;" <cfif s is 1>checked</cfif>></td>
              <td class="feed_option" width=60 valign=top><b><a href="/exchange/admin/companies/edit.cfm?company_id=#open.company_id#" target="_blank" rel="noopener" rel="noreferrer">Edit</a></b></td>
              <td class="feed_option" valign=top width=250><a href="/exchange/include/company_profile.cfm?id=#open.company_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(open.company_name)#</b></a></td>
              <td class="feed_option" valign=top align=center>#open.company_duns#</td>
              <td class="feed_option" valign=top>#open.company_cb_id#</td>
              <td class="feed_option" valign=top align=center>#products.total#</td>
              <td class="feed_option" valign=top>#open.company_website#</td>
              <td class="feed_option" valign=top width=300>#open.company_about#</td>
              <td class="feed_option" valign=top width=300>#open.company_long_desc#</td>
              <td class="feed_option" valign=top>#open.company_keywords#</td>
              <td class="feed_option" valign=top>#open.company_city#</td>
              <td class="feed_option" valign=top>#open.company_state#</td>
           </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

         </cfloop>

		 <tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete these record(s)?');"></td></tr>

         <cfoutput>
         <input type="hidden" name="company_name" value="#company_name#">
         </cfoutput>

         </form>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<!---
<cfinclude template="/exchange/include/footer.cfm"> --->

</body>
</html>

