<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Next >">

    <cfset session.new_email = '#usr_email#'>

    <!--- Check to see if the user is in this Exchange already --->

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 join hub_xref on hub_xref_usr_id = usr_id
	 where usr_email = '#tobase64(usr_email)#' and
	       hub_xref_hub_id = #session.hub#
	</cfquery>

	<cfif check.recordcount GT 0>
	 <cflocation URL="add.cfm?u=1" addtoken="no">
	<cfelse>
	 <cflocation URL="permissions.cfm" addtoken="no">
	</cfif>

<cfelseif #button# is "Next >>">

    <cfif isdefined("user_type_id")>
      <cfset session.usr_type_id = #usr_type_id#>
    <cfelse>
      <cfset session.usr_type_id = 0>
    </cfif>

    <cfset session.role_id = #hub_xref_role_id#>
    <cfset session.usr_role = #hub_xref_usr_role#>
    <cfset session.add_companies = #hub_xref_company_add#>
    <cfset session.usr_first_name = #usr_first_name#>
    <cfset session.usr_last_name = #usr_last_name#>
    <cfset session.usr_full_name = #usr_first_name# & " " & #usr_last_name#>

    <cflocation URL="invite.cfm" addtoken="no">

<cfelseif #button# is "Provision Instantly">

  <cftransaction>

    <!--- Add or Associate the New User --->

    <cfif session.new_user is "Yes">

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into usr
		  (usr_full_name, usr_first_name, usr_last_name, usr_email, usr_active, usr_created, usr_updated, usr_password)
		  values
		  ('#session.usr_full_name#','#session.usr_first_name#','#session.usr_last_name#','#tobase64(session.new_email)#',1,#now()#,#now()#,'#tobase64("Password123!")#')
		</cfquery>

		<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(usr_id) as id from usr
		</cfquery>

		<cfset session.new_user_id = #max.id#>

    <cfelse>

		<cfquery name="get_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select usr_id from usr
		 where usr_email = '#tobase64(session.new_email)#'
		</cfquery>

		<cfset session.new_user_id = #get_user.usr_id#>

    </cfif>

    <!--- Add to the Exchange or Network --->

		<cfif session.usr_type_id is not 0>
		 <cfloop index="i" list="#session.usr_type_id#">
		   <cfquery name="insert_type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into usr_type_xref
			  (
			  usr_type_xref_hub_id,
			  usr_type_xref_usr_type_id,
			  usr_type_xref_usr_id
			  )
			  values
			  (
			  #session.hub#,
			  #i#,
			  #session.new_user_id#
			  )
           </cfquery>
		 </cfloop>
		</cfif>


		<!--- Update Categories --->

		<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from lens
		  where lens_hub_id = #session.hub#
		</cfquery>

		<cfloop query="lens">

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into align
			 (align_company_id, align_type_id, align_type_value, align_usr_id, align_hub_id)
			 values
			 (null, 1, #lens.lens_id#, #session.new_user_id#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref
		 (hub_xref_usr_id,
		  hub_xref_company_add,
		  hub_xref_hub_id,
		  hub_xref_active,
		  hub_xref_joined,
		  hub_xref_usr_role,
		  hub_xref_role_id
		 )
		 values
		 (#session.new_user_id#,
		  #session.add_companies#,
		  #session.hub#,
		  1,
		  #now()#,
		  #session.usr_role#,
		  #session.role_id#
		 )
		</cfquery>

	</cftransaction>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Send Invitation">

  <cftransaction>

    <!--- Add or Associate the New User --->

    <cfif session.new_user is "Yes">

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into usr
		  (usr_full_name, usr_first_name, usr_last_name, usr_email, usr_active, usr_created, usr_updated)
		  values
		  ('#session.usr_full_name#','#session.usr_first_name#','#session.usr_last_name#','#tobase64(session.new_email)#',0,#now()#,#now()#)
		</cfquery>

		<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(usr_id) as id from usr
		</cfquery>

		<cfset session.new_user_id = #max.id#>

    <cfelse>

		<cfquery name="get_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select usr_id from usr
		 where usr_email = '#tobase64(session.new_email)#'
		</cfquery>

		<cfset session.new_user_id = #get_user.usr_id#>

    </cfif>

    <!--- Add to the Exchange or Network --->

		<cfif session.usr_type_id is not 0>
		 <cfloop index="i" list="#session.usr_type_id#">
		   <cfquery name="insert_type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into usr_type_xref
			  (
			  usr_type_xref_hub_id,
			  usr_type_xref_usr_type_id,
			  usr_type_xref_usr_id
			  )
			  values
			  (
			  #session.hub#,
			  #i#,
			  #session.new_user_id#
			  )
           </cfquery>
		 </cfloop>
		</cfif>


		<!--- Update Categories --->

		<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from lens
		  where lens_hub_id = #session.hub#
		</cfquery>

		<cfloop query="lens">

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into align
			 (align_company_id, align_type_id, align_type_value, align_usr_id, align_hub_id)
			 values
			 (null, 1, #lens.lens_id#, #session.new_user_id#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref
		 (hub_xref_usr_id,
		  hub_xref_company_add,
		  hub_xref_hub_id,
		  hub_xref_active,
		  hub_xref_joined,
		  hub_xref_usr_role,
		  hub_xref_role_id,
		  hub_xref_invite_sent,
		  hub_xref_invite_message,
		  hub_xref_new_user
		 )
		 values
		 (#session.new_user_id#,
		  #session.add_companies#,
		  #session.hub#,
		  0,
		  #now()#,
		  #session.usr_role#,
		  #session.role_id#,
		  #now()#,
		 '#hub_xref_invite_messsage#',
		 <cfif session.new_user is "Yes">1<cfelse>null</cfif>
		 )
		</cfquery>

		<cfquery name="getid" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(hub_xref_id) as id from hub_xref
		 where hub_xref_hub_id = #session.hub#
	    </cfquery>

	    <cfset invite_code = #createuuid()#>

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update hub_xref
		 set hub_xref_invite_code = '#invite_code#'
		 where hub_xref_id = #getid.id#
		</cfquery>

	</cftransaction>

    <!--- Send Invite --->

<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfmail from="#h_info.hub_name# <noreply@ratio.exchange>"
		  to="#session.new_email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="#h_info.hub_name# - Invitation to Join">

<cfoutput>
<html>
<head>
<title>#h_info.hub_name#</title>
</head>
<body class="body">

<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
 <tr><td class="feed_header">Welcome to the #h_info.hub_name#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td class="feed_sub_header" style="font-weight: normal;">#hub_xref_invite_messsage#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td class="feed_sub_header" style="font-weight: normal;">To accept this invitation and activate your account, please click here <a href="#session.site_url#?i=#h_info.hub_encryption_key#&invite=#invite_code#">#session.site_url#?i=#h_info.hub_encryption_key#&invite=#invite_code#</a> or paste the URL in your brower.</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td class="feed_sub_header">#h_info.hub_support_name#</td></tr>
 <tr><td class="feed_sub_header" style="font-weight: normal;">#h_info.hub_support_phone#</td></tr>
 <tr><td class="feed_sub_header" style="font-weight: normal;">#h_info.hub_support_email#</td></tr>

</table>

</body>
</html>
</cfoutput>

</cfmail>

<cflocation URL="index.cfm?u=1" addtoken="no">

</cfif>