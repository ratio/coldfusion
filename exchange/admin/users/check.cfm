<cfinclude template="/exchange/security/check.cfm">

<cfquery name="check_exchange" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_email = '#email#'
</cfquery>

<cfset session.new_user_email = #email#>

<cfif check_exchange.recordcount is 0>

 <cflocation URL="new_user.cfm" addtoken="no">

<cfelse>

	<cfquery name="check_in_this_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 join hub_xref on hub_xref_usr_id = usr_id
	 where hub_xref_hub_id = #session.hub# and
	       usr_email = '#email#'
	</cfquery>

	<cfif check_in_this_hub.recordcount is 1>
	 <cflocation URL="index.cfm?u=2" addtoken="no">
	<cfelse>
	 <cflocation URL="invite_user.cfm" addtoken="no">
	</cfif>

</cfif>