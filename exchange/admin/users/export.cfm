<cfinclude template="/exchange/security/check.cfm">

<cfheader name="Content-Disposition" value="inline; filename=users.xls">
<cfcontent type="application/vnd.msexcel">

<cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select hub_role_name, hub_xref_invite_code, hub_role_id, usr_id, usr_first_name, hub_xref_active, usr_last_name, usr_email, usr_company_name, usr_phone, usr_photo, hub_xref_default as default_hub, hub_xref_usr_role, hub_xref_role_id, hub_xref_active as active from hub_xref
	    join usr on usr.usr_id = hub_xref.hub_xref_usr_id
	    left join hub_role on hub_role_id = hub_xref.hub_xref_role_id
	    where hub_xref.hub_xref_hub_id = #session.hub#

	    <cfif isdefined("session.user_keyword")>
	      and (usr_first_name like '%#session.user_keyword#%' or
	           usr_last_name like '%#session.user_keyword#%' or
	           usr_full_name like '%#session.user_keyword#%')
	    </cfif>

	    order by usr.usr_last_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr>
	   <td class="feed_sub_header">Name</td>
	   <td class="feed_sub_header">Email</td>
	   <td class="feed_sub_header">Company</td>
	   <td class="feed_sub_header">Role</td>
	   <td class="feed_sub_header">Access Level</td>
	   <td class="feed_sub_header" align=center>Active</td>
	   <td class="feed_sub_header">Invite Code</td>
   </tr>

<cfset counter = 0>

<cfoutput query="agencies">
	<tr>
	  <td class="feed_sub_header">#usr_last_name#, #usr_first_name#</td>
	  <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>
	  <td class="feed_sub_header" style="font-weight: normal;">#usr_company_name#</td>
	  <td class="feed_sub_header" style="font-weight: normal;">#hub_role_name#</td>
	  <td class="feed_sub_header" style="font-weight: normal;"><cfif #hub_xref_usr_role# is 0>Normal User<cfelse>Administrator</cfif></td>
	  <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #hub_xref_active# is 1>Yes<cfelse>No</cfif></td>
	  <td class="feed_sub_header" style="font-weight: normal;">#hub_xref_invite_code#</td>
   </tr>
</cfoutput>

</table>