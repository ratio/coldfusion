<cfinclude template="/exchange/security/check.cfm">

<cfif t is "Portfolio">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio
	 set portfolio_usr_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where portfolio_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       portfolio_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="manage.cfm?u=1&i=#i#" addtoken="no">

<cfelseif t is "Opportunity">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update pipeline
	 set pipeline_usr_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where pipeline_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       pipeline_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="manage.cfm?u=2&i=#i#" addtoken="no">

<cfelseif t is "Market">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update market_report
	 set market_report_created_by_usr_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where market_report_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       market_report_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="manage.cfm?u=3&i=#i#" addtoken="no">

<cfelseif t is "Challenge">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update challenge
	 set challenge_created_by_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where challenge_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       challenge_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="manage.cfm?u=4&i=#i#" addtoken="no">

<cfelseif t is "Need">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update need
	 set need_owner_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where need_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       need_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="manage.cfm?u=5&i=#i#" addtoken="no">

</cfif>