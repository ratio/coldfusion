<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Add User</td>
		   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="green" class="feed_sub_header" style="color: ffffff;" align=center width=200>Email</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Role & Permissions</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Send Invite</td>
	   </tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td height=20></td></tr>

		 <form action="next.cfm" method="post">

               <cfif isdefined("u")>
               <cfoutput>
                <tr><td class="feed_sub_header" style="color: red;" colspan=2>A user with the email address "<i>#session.new_email#</i>" has already been registered with this Exchange.</td></tr>
               </cfoutput>
               </cfif>

			   <tr><td class="feed_sub_header" width=150><b>Email Address</b></td>
				   <td class="feed_option"><input type="email" class="input_text" style="width: 300px;" name="usr_email" required></td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Next >" vspace=10></td></tr>

	  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

