<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Reset Password">

    <cftransaction>

        <!-- Generate Random Password --->

  		<cfset result1="">
  		<cfset i=0>

  		<cfloop index="i" from="1" to="10">
  		 <cfset result1=result1&Chr(RandRange(65, 90))>
  		</cfloop>

  	   <!--- Update User Password --->

	   <cfquery name="email" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select usr_email from usr
		  where usr_id = #usr_id#
       </cfquery>

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update usr
		  set usr_password = '#result1#',
		      usr_updated = #now()#
		  where usr_id = #usr_id#
       </cfquery>

     </cftransaction>

<!-- Notify User --->

	<cfmail from="The Exchange <noreply@ratio.exchange>"
			  to="#email.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Password Reset">
	<html>
	<head>
	<title>RATIO Exchange</title>
	</head><div class="center">
	<body class="body">

	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your Exchange password has been reset.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Email: </b><cfoutput>#email.usr_email#</cfoutput></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Temporary Password: </b><cfoutput>#result1#</cfoutput></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Please be sure to change your temporary password after you <a href="https://go.ratio.exchange/signin/">login</a>.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Exchange Support Team</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
	 <tr><td>&nbsp;</td></tr>
	</table>
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="223a60">
	 <tr><td height=75 valign=middle>&nbsp;<img src="#image_virtual#/exchange_logo.png" width=160 hspace=10></td></tr>
	</table>
	</body>
	</html>

	</cfmail>

</cfif>

<cflocation URL="index.cfm?u=4" addtoken="no">

