<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr_type
	where usr_type_hub_id = #session.hub#
	order by usr_type_order
</cfquery>

<cfquery name="types_xref" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr_type_xref
	where usr_type_xref_hub_id = #session.hub# and
	      usr_type_xref_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfset #type_list# = #valuelist(types_xref.usr_type_xref_usr_type_id)#>

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_role
 where hub_role_hub_id = #session.hub#
 order by hub_role_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Update User</td>
              <td class="feed_sub_header" align=right>
              <cfoutput>
              <a href="open.cfm?i=#i#">Return</a>
              </cfoutput></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

		<form name="thisForm" action="edit_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfoutput>
	   <tr><td class="feed_sub_header" width=175>User</td>
	       <td class="feed_sub_header">#edit.usr_full_name#</td></tr>
       </cfoutput>


	   <tr><td class="feed_sub_header" valign=top>User Type(s)</td>
	       <td valign=top class="feed_sub_header" style="font-weight: normal;">

	       <cfif types.recordcount is 0>
	        No User Types Exist.
	       <cfelse>

			<table cellspacing=0 cellpadding=0 border=0>
             <cfoutput query="types">
              <tr>
                 <td width=30><input type="checkbox" style="width: 22px; height: 22px;" name="usr_type_id" value=#types.usr_type_id# <cfif listfind(type_list,types.usr_type_id)>checked</cfif>></td>
                 <td class="feed_sub_header" style="font-weight: normal;">#types.usr_type_name#</td>
              </tr>
             </cfoutput>
            </table>

            </cfif>

           </td>
       </tr>


	   <tr><td class="feed_sub_header">Role</td>
	       <td>
	       <select name="hub_xref_role_id" class="input_select" style="width: 200px;">
	        <option value=0>Select Profile
	        <cfoutput query="profiles">
	         <option value=#hub_role_id# <cfif edit.hub_xref_role_id is #hub_role_id#>selected</cfif>>#hub_role_name#
	        </cfoutput>
	       </select></td></tr>

       <cfoutput>

	   <tr><td class="feed_sub_header">Access</td>
	       <td>
	       <select name="hub_xref_usr_role" class="input_select" style="width: 200px;">
	         <option value=0>Normal User
	         <option value=1 <cfif edit.hub_xref_usr_role is 1>selected</cfif>>Administrator
	       </select></td></tr>

		<tr><td class="feed_sub_header" width=75>Add Companies</td>
		    <td>

			<select name="hub_xref_company_add" class="input_select">
			 <option value=0>No
			 <option value=1 <cfif edit.hub_xref_company_add is 1>selected</cfif>>Yes
			</select>

			<span class="link_small_gray">Allows user to identify Companies as "In Network"</span>

			</td></tr>

		<tr><td class="feed_sub_header" width=75>Active</td>
		    <td>

			<select name="hub_xref_active" class="input_select">
			 <option value=0 <cfif #edit.hub_xref_active# is 0>selected</cfif>>No
			 <option value=1 <cfif #edit.hub_xref_active# is 1>selected</cfif>>Yes
			</select>

			<span class="link_small_gray">Determines if user can login and is active in this Exchange</span>

			</td></tr>

       <tr><td height=10></td></tr>

	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td></td><td>

	   <input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;

	   </td></tr>

	   </table>

        <input type="hidden" name="i" value="#i#">
       </cfoutput>

		</form>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



