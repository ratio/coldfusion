<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio
	 set portfolio_usr_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where portfolio_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       portfolio_hub_id = #session.hub#
	</cfquery>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update pipeline
	 set pipeline_usr_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where pipeline_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       pipeline_hub_id = #session.hub#
	</cfquery>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update market_report
	 set market_report_created_by_usr_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where market_report_created_by_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       market_report_hub_id = #session.hub#
	</cfquery>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update challenge
	 set challenge_created_by_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where challenge_created_by_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       challenge_hub_id = #session.hub#
	</cfquery>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update need
	 set need_owner_id = #decrypt(new_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 where need_owner_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       need_hub_id = #session.hub#
	</cfquery>

</cftransaction>

<cflocation URL="manage.cfm?u=4&i=#i#" addtoken="no">


