<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

	 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into advisor
	  (
	   advisor_usr_id,
	   advisor_advisor_id,
	   advisor_role,
	   advisor_hub_id
	   )
	   values
	   (
	    #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	    #decrypt(advisor_advisor_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	   '#advisor_role#',
	    #session.hub#
	   )
	 </cfquery>

	<cflocation URL="open.cfm?u=4&i=#i#" addtoken="no">

<cfelseif button is "Save">

	 <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update advisor
      set advisor_advisor_id = #decrypt(advisor_advisor_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
          advisor_role = '#advisor_role#'
      where advisor_id = #advisor_id# and
            advisor_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
            advisor_hub_id = #session.hub#
     </cfquery>

	<cflocation URL="open.cfm?u=5&i=#i#" addtoken="no">

<cfelseif button is "Delete">

	 <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      delete advisor
      where advisor_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
            advisor_hub_id = #session.hub# and
            advisor_id = #advisor_id#
     </cfquery>

	<cflocation URL="open.cfm?u=6&i=#i#" addtoken="no">

</cfif>

