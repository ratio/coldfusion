<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

    <cftransaction>

	<cfquery name="usr_updte" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update usr
	 set usr_active = #usr_active#,
	     usr_exchange_admin = #usr_exchange_admin#,
	     usr_advisor = #usr_advisor#,
	     usr_company_id = <cfif #company_id# is 0>null<cfelse>#company_id#</cfif>,
	     usr_email = '#usr_email#',
	     usr_advisor_id = #usr_advisor_id#,
	     usr_password = '#usr_password#',
	     usr_updated = #now()#
	 where usr_id = #usr_id#
	</cfquery>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete premium
	 where premium_usr_id = #usr_id#
	</cfquery>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into premium
	 (premium_usr_id, premium_demand, premium_opps, premium_hubs, premium_sourcing, premium_vehicle, premium_awards, premium_forecast, premium_competitor, premium_sbir, premium_grants, premium_updated)
	 values
	 (
	  #usr_id#,
	  <cfif isdefined("premium_demand")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_opps")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_hubs")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_sourcing")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_vehicle")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_awards")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_forecast")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_competitor")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_sbir")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_grants")>1<cfelse>null</cfif>,
	  #now()#
	 )
	</cfquery>

	</cftransaction>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Cancel">

    <cflocation URL="index.cfm" addtoken="no">

</cfif>

<cflocation URL="edit.cfm?usr_id=#usr_id#" addtoken="no">