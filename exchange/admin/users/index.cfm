<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

	   <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select hub_xref_invite_code, hub_role_name, hub_role_id, usr_id, usr_first_name, hub_xref_active, usr_last_name, usr_email, usr_company_name, usr_phone, usr_photo, hub_xref_default as default_hub, hub_xref_usr_role, hub_xref_role_id, hub_xref_active as active from hub_xref
	    join usr on usr.usr_id = hub_xref.hub_xref_usr_id
	    left join hub_role on hub_role_id = hub_xref.hub_xref_role_id
	    where hub_xref.hub_xref_hub_id = #session.hub#

	    <cfif isdefined("session.user_keyword")>
	      and (usr_first_name like '%#session.user_keyword#%' or
	           usr_last_name like '%#session.user_keyword#%' or
	           usr_full_name like '%#session.user_keyword#%')
	    </cfif>

	    order by usr.usr_last_name
	   </cfquery>

	   <cfif isdefined("export")>
		 <cfinclude template="/exchange/include/export_to_excel.cfm">
		</cfif>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Manage Users</td>
		       <td class="feed_sub_header" align=right><a href="/exchange/admin/index.cfm">Settings</td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		</table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>
      <form action="set.cfm" method="post">
      <td class="feed_sub_header" style="font-weight: normal;"><b>Search</b>
      &nbsp;&nbsp;
      <input type="text" class="input_text" name="user_keyword" placeholder="user name" style="width: 200px;">
      &nbsp;&nbsp;
      <input type="submit" name="button" class="button_blue" value="Go">
      &nbsp;&nbsp;<input type="submit" name="button" class="button_blue" value="All">
      </form>
      </td>
	  <td align=right class="feed_sub_header"><a href="new_user.cfm">
	  <img src="/images/plus3.png" width=15 border=0 hspace=10></a><a href="add.cfm">Add User</a>

	  <img src="/images/icon_export_excel.png" width=20 border=0 hspace=10></a><a href="export.cfm">Export to Excel</a>

	  </td></tr>

       <tr>

	  <cfif isdefined("u")>
	   <cfif u is 1>
		<td class="feed_sub_header" style="color: green;">User has been successfully added.</td>
	   <cfelseif u is 2>
		<td class="feed_sub_header" style="color: green;">User has been successfully updated.</td>
	   <cfelseif u is 3>
		<td class="feed_sub_header" style="color: red;">Email address already exists in this Exchange.</td>
	   <cfelseif u is 4>
		<td class="feed_sub_header" style="color: red;">User has been successfully deleted.</td>
	   <cfelseif u is 5>
		<td class="feed_sub_header" style="color: green;">Users password has been successfully reset.</td>
	   <cfelseif u is 86>
		<td class="feed_sub_header" style="color: green;">The user has been successfully removed.</td>
	   </cfif>
	  </cfif>

      </tr>

       </table>

       <cfif agencies.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" style="font-weight: normal;">There are no users in this Exchange.</td></tr>

        </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr>
		       <td></td>
		       <td class="feed_sub_header">Name</td>
		       <td class="feed_sub_header">Email</td>
		       <td class="feed_sub_header">Role</td>
		       <td class="feed_sub_header">Invite Code</td>
		       <td class="feed_sub_header">Access Level</td>
		       <td class="feed_sub_header" align=center>Active</td>
		       <td class="feed_sub_header" align=right>Records</td>
		   </tr>

	    <cfset counter = 0>

	    <cfoutput query="agencies">

	       <cfif counter is 0>
	        <tr bgcolor="ffffff">
	       <cfelse>
	        <tr bgcolor="e0e0e0">
	       </cfif>

			 <cfif #usr_photo# is "">
			  <td width=75><a href="open.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 border=0></td>
			 <cfelse>
			  <td width=75><a href="open.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" src="#media_virtual#/#usr_photo#" height=40 width=40 border=0></td>
			 </cfif>

              <td class="feed_sub_header"><a href="open.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#usr_last_name#, #usr_first_name#</a></td>
	          <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>
	          <td class="feed_sub_header" style="font-weight: normal;"><cfif #hub_role_name# is "">Unspecified<cfelse>#hub_role_name#</cfif></td>
	          <td class="feed_sub_header" style="font-weight: normal;">#hub_xref_invite_code#</td>
	          <td class="feed_sub_header" style="font-weight: normal;"><cfif #hub_xref_usr_role# is 0>Normal User<cfelse>Administrator</cfif></td>
	          <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #hub_xref_active# is 1><b><font color="green">Yes</font><cfelse><font color="red">No</font></cfif></b></td>
              <td class="feed_sub_header" align=right><a href="manage.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Manage</a></td>

	       </tr>

	    <cfif counter is 0>
	     <cfset counter = 1>
	    <cfelse>
	     <cfset counter = 0>
	    </cfif>

	    </cfoutput>

       </table>

       </cfif>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfset StructDelete(Session,"user_keyword")>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>