<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add User">

    <!--- Check for duplicates --->

	 <cfquery name="find" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from usr
	  join hub_xref on hub_xref_usr_id = usr_id
	  where hub_xref_hub_id = #session.hub# and
	  usr_email = '#tobase64(usr_email)#'
	 </cfquery>

	 <cfif find.recordcount GT 0>
	   <cflocation URL="new_user.cfm?u=1" addtoken="no">
	 </cfif>

	<cftransaction>

    <cfset usr_full_name = #usr_first_name# & " " & #usr_last_name#>

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into usr
		  (usr_phone, usr_cell_phone, usr_company_name, usr_password_date, usr_full_name, usr_profile_display, usr_company_id, usr_updated, usr_title, usr_first_name, usr_last_name, usr_email, usr_password, usr_created, usr_validation, usr_setting_notification)
		  values
		  ('#usr_phone#','#usr_cell_phone#','#usr_company_name#',#now()#,'#usr_full_name#', 1, <cfif usr_company_id is 0>null<cfelse>#usr_company_id#</cfif>, #now()#, '#usr_title#', '#usr_first_name#','#usr_last_name#','#tobase64(usr_email)#','#tobase64(usr_password)#',#now()#,1,1)
		</cfquery>

		<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(usr_id) as id from usr
		</cfquery>

		<!--- Update User Types --->

		<cfif isdefined("usr_type_id")>
		 <cfloop index="i" list="#usr_type_id#">
		   <cfquery name="insert_type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into usr_type_xref
			  (
			  usr_type_xref_hub_id,
			  usr_type_xref_usr_type_id,
			  usr_type_xref_usr_id
			  )
			  values
			  (
			  #session.hub#,
			  #i#,
			  #max.id#
			  )
           </cfquery>
		 </cfloop>
		</cfif>

		<!--- Update Categories --->

		<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from lens
		  where lens_hub_id = #session.hub#
		</cfquery>

		<cfloop query="lens">

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into align
			 (align_company_id, align_type_id, align_type_value, align_usr_id, align_hub_id)
			 values
			 (<cfif isdefined("default_company_id")>#default_company_id#<cfelse>null</cfif>, 1, #lens.lens_id#, #max.id#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref
		 (hub_xref_usr_id,
		  hub_xref_profile_update,
		  hub_xref_company_add,
		  hub_xref_hub_id,
		  hub_xref_active,
		  hub_xref_joined,
		  hub_xref_usr_role,
		  hub_xref_role_id
		 )
		 values
		 (#max.id#,
		  #hub_xref_profile_update#,
		  #hub_xref_company_add#,
		  #session.hub#,
		  #hub_xref_active#,
		  #now()#,
		  #hub_xref_usr_role#,
		  #hub_xref_role_id#
		 )
		</cfquery>

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (2,3,4)) and
	       (align_usr_id = #max.id#)
	        and (align_hub_id = #session.hub#)
	</cfquery>

	<cfif isdefined("market_id")>

		<cfloop index="m_element" list=#market_id#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#max.id#, 2, #m_element#, #session.hub#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sector_id")>

		<cfloop index="s_element" list=#sector_id#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#max.id#, 3, #s_element#, #session.hub#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topic_id")>

		<cfloop index="t_element" list=#topic_id#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#max.id#, 4, #t_element#, #session.hub#)
			</cfquery>

		</cfloop>

	</cfif>

	</cftransaction>

	<cflocation URL="index.cfm?u=1" addtoken="no">

</cfif>