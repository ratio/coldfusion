<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: ffffff">

<script type="text/javascript">
    // return the value to the parent window
    function returnYourChoice(choice){
        opener.setSearchResult(targetField, choice);
        window.close();
    }
</script>

<cfif value is "">
		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td height=20></td></tr>
		  <tr><td colspan=3 class="feed_header">You didn't search for anything.  Please try again.</td></tr>
		</table>
<cfelse>

	<cfquery name="lookup" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from company
	 where company_name like '#value#%'
	 order by company_duns
	</cfquery>

    <form name="thisform">
		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td height=10></td></tr>
		  <tr><td colspan=3 class="feed_header">SEARCH RESULTS</td></tr>
		  <tr><td height=10></td></tr>
		  <tr>
			 <td class="feed_sub_header">ID</td>
			 <td class="feed_sub_header">COMPANY NAME</td>
			 <td class="feed_sub_header">DUNS</td>
			 <td class="feed_sub_header">LOCATION</td>
		  </tr>

		  <cfset counter = 0>
		  <cfoutput query="lookup">

		  <cfif counter is 0>
		   <tr bgcolor="ffffff">
		  <cfelse>
		   <tr bgcolor="e0e0e0">
		  </cfif>

			   <td class="feed_sub_header"><span style="feed_sub_header" onclick="returnYourChoice('#lookup.company_id#')">#lookup.company_id#</span></td>
			   <td class="feed_sub_header" style="font-weight: normal;">#lookup.company_name#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#lookup.company_duns#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#lookup.company_city#, #lookup.company_state#</td>
		   </tr>

		   <cfif counter is 0>
		    <cfset counter = 1>
		   <cfelse>
		    <cfset counter = 0>
		   </cfif>

		  </cfoutput>
		</table>
    </form>
 </cfif>

</body>
</html>