<cfinclude template="/exchange/security/check.cfm">

<cfquery name="current" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select advisor_advisor_id from advisor
 where advisor_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       advisor_hub_id = #session.hub#
</cfquery>

<cfif current.recordcount is 0>
 <cfset current_list = 0>
<cfelse>
 <cfset current_list = valuelist(current.advisor_advisor_id)>
</cfif>

<cfquery name="advisors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(usr_id), usr_first_name, usr_last_name, usr_photo from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where hub_xref_active = 1 and
       hub_xref_hub_id = #session.hub# and
       usr_id not in (#current_list#)
 order by usr_last_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">ADD ADVISOR</td>
              <td class="feed_sub_header" align=right>
              <cfoutput>
              <a href="open.cfm?i=#i#">Return</a>
              </cfoutput></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

		<form action="advisor_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" width=100>User</td>
	       <td>
	           <select name="advisor_advisor_id" class="input_select">
	           <cfoutput query="advisors">
	             <option value=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#usr_last_name#, #usr_first_name#
	           </cfoutput>
	           </select>
	       </td>
	   </tr>

	   <tr><td class="feed_sub_header">Role</td>
	       <td><input class="input_text" type="text" name="advisor_role" style="width: 400px;" maxlength=50 required></td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td></td><td>

	   <input class="button_blue_large" type="submit" name="button" value="Add" vspace=10>&nbsp;&nbsp;

	   </td></tr>

	   </table>

	   <cfoutput>
        <input type="hidden" name="i" value="#i#">
       </cfoutput>

		</form>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



