<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_xref_hub_id = #session.hub#
</cfquery>

<cfquery name="new" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_first_name, usr_last_name, usr_id, usr_email from hub_xref
 join usr on usr.usr_id = hub_xref.hub_xref_usr_id
 left join hub_role on hub_role_id = hub_xref.hub_xref_role_id
 where hub_xref.hub_xref_hub_id = #session.hub# and
       hub_xref.hub_xref_active = 1
 order by usr_last_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
          <tr><td class="feed_header">CHANGE OWNERSHIP</td>
              <td class="feed_sub_header" align=right><a href="manage.cfm?i=#i#">Return</a></td></tr>
          </cfoutput>
          <tr><td colspan=2><hr></td></tr>
        </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>

          <cfif t is "Portfolio">

			<cfquery name="portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from portfolio
			 where portfolio_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       portfolio_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   portfolio_hub_id = #session.hub#
			</cfquery>

           <cfoutput>
           	<tr><td colspan=2 class="feed_header">Selected Portfolio - #portfolio.portfolio_name#</td></tr>
           </cfoutput>

          <cfelseif t is "Challenge">

			<cfquery name="challenge" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from challenge
			 where challenge_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       challenge_created_by_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   challenge_hub_id = #session.hub#
			</cfquery>

           <cfoutput>
           	<tr><td colspan=2 class="feed_header">Selected Challenge - #challenge.challenge_name#</td></tr>
           </cfoutput>

          <cfelseif t is "Need">

			<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from need
			 where need_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       need_owner_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   need_hub_id = #session.hub#
			</cfquery>

           <cfoutput>
           	<tr><td colspan=2 class="feed_header">Selected Need - #need.need_name#</td></tr>
           </cfoutput>

          <cfelseif t is "Opportunity">

			<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from pipeline
			 where pipeline_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       pipeline_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   pipeline_hub_id = #session.hub#
			</cfquery>

           <cfoutput>
           	<tr><td colspan=2 class="feed_header">Selected Opportunity - #pipeline.pipeline_name#</td></tr>
           </cfoutput>

          <cfelseif t is "Market">

			<cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from market_report
			 where market_report_id = #decrypt(id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       market_report_created_by_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   market_report_hub_id = #session.hub#
			</cfquery>

           <cfoutput>
           	<tr><td colspan=2 class="feed_header">Selected Market Report - #market.market_report_name#</td></tr>
           </cfoutput>

          </cfif>

		  <tr><td height=10></td></tr>

          <form action="change_db.cfm" method="post">
          <cfoutput>
          <tr>
             <td class="feed_sub_header" width=100>From</td>
             <td class="feed_sub_header" style="font-weight: normal;">#edit.usr_first_name# #edit.usr_last_name#<td>
          </tr>
          </cfoutput>

             <td class="feed_sub_header">To</td>
             <td>

             <select name="new_id" class="input_select">
              <cfoutput query="new">
              	<option value=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#usr_last_name#, #usr_first_name# (#tostring(tobinary(usr_email))#)
              </cfoutput>
             </select>

             <td>

          <tr>

          <tr><td colspan=2><hr></td></tr>

          <tr><td height=10></td></tr>

          <tr><td></td><td><input type="submit" name="button" value="Change" class="button_blue_large" onclick="return confirm('Transfer Ownership?\r\nAre you sure you want to transfer ownership to the new user?');"></td></tr>

          <cfoutput>
           <input type="hidden" name="i" value=#i#>
           <input type="hidden" name="t" value=#t#>
           <input type="hidden" name="id" value=#id#>
          </cfoutput>

          </form>

        </table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



