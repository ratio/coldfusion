<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_role
 where hub_role_hub_id = #session.hub#
 order by hub_role_name
</cfquery>

<cfquery name="in_the_exchange" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_email = '#tobase64(session.new_email)#'
</cfquery>

<cfquery name="types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr_type
	where usr_type_hub_id = #session.hub#
	order by usr_type_order
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Add User</td>
		   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Email</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header"  style="color: ffffff;" align=center width=200>Role & Permissions</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Send Invite</td>
	   </tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td height=20></td></tr>

	   <form action="next.cfm" method="post">

       <cfoutput>

       <tr><td class="feed_sub_header" width=150>Email Address</td>
           <td class="feed_sub_header">#session.new_email#</td></tr>


       <cfif in_the_exchange.recordcount is 0>

       <cfset #session.new_user# = "Yes">

	   <tr><td class="feed_sub_header" width=150><b>Name</b></td>
		   <td class="feed_option">

		   <input type="text" class="input_text" style="width: 200px;" name="usr_first_name" placeholder="First Name" required>
		   &nbsp;
		   <input type="text" class="input_text" style="width: 300px;" name="usr_last_name" placeholder="Last Name" required>

		   </td></tr>

       <cfelse>

       <cfset #session.new_user# = "No">

       <input type="hidden" name="usr_first_name" value="#in_the_exchange.usr_first_name#">
       <input type="hidden" name="usr_last_name" value="#in_the_exchange.usr_last_name#">

       <tr><td class="feed_sub_header" width=150>Name</td>
           <td class="feed_sub_header">#in_the_exchange.usr_full_name#  (User is already registered in another Exchange)</td></tr>

       </cfif>

       </cfoutput>

	   <tr><td class="feed_sub_header">Role</td>
	       <td>
	       <select name="hub_xref_role_id" class="input_select" style="width: 200px;">
	        <cfoutput query="profiles">
	         <option value=#hub_role_id#>#hub_role_name#
	        </cfoutput>
	       </select></td></tr>

	   <tr><td class="feed_sub_header">Access Rights</td>
	       <td>
	       <select name="hub_xref_usr_role" class="input_select" style="width: 200px;">
	         <option value=0>Normal User
	         <option value=1>Administrator
	       </select></td></tr>

	   <tr><td class="feed_sub_header" valign=top>User Type(s)</td>
	       <td valign=top>

			<table cellspacing=0 cellpadding=0 border=0>
             <cfif types.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No User Types Defined</td></tr>
             <cfelse>
             <cfoutput query="types">
              <tr>
                 <td width=30><input type="checkbox" style="width: 22px; height: 22px;" name="usr_type_id" value=#types.usr_type_id#></td>
                 <td class="feed_sub_header" style="font-weight: normal;">#types.usr_type_name#</td>
              </tr>
             </cfoutput>
             </cfif>
            </table>

           </td>
       </tr>

		<tr><td class="feed_sub_header" width=75>Add Companies</td>
		    <td>

			<select name="hub_xref_company_add" class="input_select">
			 <option value=0>No
			 <option value=1>Yes
			</select>

			<span class="link_small_gray">Allows user to identify Companies as "In Network"</span>

			</td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Next >>" vspace=10></td></tr>

	  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>