<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

   <cftransaction>

		<cfquery name="tracker" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into hub_usr_tracker
		 (
		  hub_usr_tracker_usr_id,
		  hub_usr_tracker_hub_id,
		  hub_usr_tracker_active,
		  hub_usr_tracker_date,
		  hub_usr_tracker_role_id,
		  hub_usr_tracker_change_usr_id,
		  hub_usr_tracker_action
		 )
		 values
		 (
		  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		  #session.hub#,
		  #hub_xref_active#,
		  #now()#,
		  #hub_xref_role_id#,
		  #session.usr_id#,
		 'Updated User'
		 )
		</cfquery>

		<cfquery name="up2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update hub_xref
		 set hub_xref_active = #hub_xref_active#,
			 hub_xref_usr_role = #hub_xref_usr_role#,
			 hub_xref_role_id = #hub_xref_role_id#,
			 hub_xref_company_add = #hub_xref_company_add#
			 where (hub_xref_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   hub_xref_hub_id = #session.hub#)
		</cfquery>

		<!--- Update User Types --->

		<cfquery name="delete_types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete usr_type_xref
		 where usr_type_xref_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		       usr_type_xref_hub_id = #session.hub#
		</cfquery>

		<cfif isdefined("usr_type_id")>
		 <cfloop index="t" list="#usr_type_id#">
		   <cfquery name="insert_type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into usr_type_xref
			  (
			  usr_type_xref_hub_id,
			  usr_type_xref_usr_type_id,
			  usr_type_xref_usr_id
			  )
			  values
			  (
			  #session.hub#,
			  #t#,
			  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
			  )
           </cfquery>
		 </cfloop>
		</cfif>

    </cftransaction>

    <cflocation URL="open.cfm?i=#i#&u=2" addtoken="no">

<cfelseif #button# is "Reset Password">

    <cftransaction>

        <!-- Generate Random Password --->

  		<cfset result1="">
  		<cfset r=0>

  		<cfloop index="r" from="1" to="10">
	  		 <cfset result1=result1&Chr(RandRange(65, 90))>
  		</cfloop>

  	   <!--- Update User Password --->

	   <cfquery name="email" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select usr_email from usr
		  where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
       </cfquery>

	   <cfquery name="update_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update usr
		  set usr_password = '#encrypt(result1,session.key, "AES/CBC/PKCS5Padding", "HEX")#',
		      usr_updated = #now()#,
		      usr_password_reset = 1
		  where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
       </cfquery>

     </cftransaction>

<!-- Notify User --->

	<cfmail from="BPN Message <noreply@ratio.exchange>"
			  to="#decrypt(email.usr_email,session.key, "AES/CBC/PKCS5Padding", "HEX")#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Password Reset">
	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head><div class="center">
	<body class="body">

	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your password to the #session.network_name# has been reset. </td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Email: </b><cfoutput>#email.usr_email#</cfoutput></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Temporary Password: </b><cfoutput>#result1#</cfoutput></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">You will be required to change your password at your next login.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	</table>
	</body>
	</html>

	</cfmail>

<cflocation URL="index.cfm?u=5" addtoken="no">

</cfif>