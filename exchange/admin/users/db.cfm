<cfinclude template="/exchange/security/check.cfm">

<!--- Get HUB Information --->

<cfquery name="hub_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_role
 where hub_role_hub_id = #session.hub#
 order by hub_role_name
</cfquery>

<cfif #button# is "Search">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_email = '#tobase64(usr_email)#'
	</cfquery>

	<cfif check.recordcount GT 0>

	 <cflocation URL="associate.cfm?i=#encrypt(check.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" addtoken="no">

	<cfelse>

	 <cfset session.new_email = '#usr_email#'>
	 <cflocation URL="new_user.cfm" addtoken="no">

	</cfif>


<cfelseif #button# is "Reset Password">

    <cftransaction>

        <!-- Generate Random Password --->

  		<cfset result1="">
  		<cfset i=0>

  		<cfloop index="i" from="1" to="10">
  		 <cfset result1=result1&Chr(RandRange(65, 90))>
  		</cfloop>

  	   <!--- Update User Password --->

	   <cfquery name="email" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select usr_email from usr
		  where usr_id = #usr_id#
       </cfquery>

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update usr
		  set usr_password = '#result1#',
		      usr_updated = #now()#
		  where usr_id = #usr_id#
       </cfquery>

     </cftransaction>

<!-- Notify User --->

	<cfmail from="#hub_info.hub_name# <noreply@ratio.exchange>"
			  to="#email.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Password Reset">

	<cfoutput>

	<html>
	<head>
	<title>#hub_info.hub_name#</title>
	</head><div class="center">
	<body class="body">

	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your Exchange password has been reset.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Email: </b><cfoutput>#email.usr_email#</cfoutput></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Temporary Password: </b><cfoutput>#result1#</cfoutput></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Please be sure to change your temporary password after you <a href="#hub_info.hub_login_page#">login</a>.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>#hub_info.hub_support_name#</b></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_email#</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_phone#</td></tr>
	 <tr><td>&nbsp;</td></tr>
	</table>

	</body>
	</html>

	</cfoutput>

	</cfmail>

<cfelseif #button# is "Add">

<cftransaction>

        <cfif usr_password is "" and isdefined("pass")>

        <!-- Generate Random Password --->

  		<cfset result1="">
  		<cfset i=0>

  		<cfloop index="i" from="1" to="10">
  		 <cfset result1=result1&Chr(RandRange(65, 90))>
  		</cfloop>

			<cfset pw = #result1#>

  		<cfelse>

  		    <cfset pw = "#usr_password#">

  		</cfif>

  	   <!--- Insert User --->

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into usr
		  (usr_updated, usr_advisor_id, usr_phone, usr_title, usr_first_name, usr_last_name, usr_email, usr_password, usr_created, usr_validation, usr_setting_notification,usr_active)
		  values
		  (#now()#, #usr_advisor_id#, '#usr_phone#', '#usr_title#','#usr_first_name#','#usr_last_name#','#usr_email#','#pw#',#now()#,1,1,1)
		</cfquery>

	    <cfquery name="usr_max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select max(usr_id) as id from usr
	    </cfquery>

       <!--- Insert Company --->

	    <cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref(hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_default, hub_xref_joined, hub_xref_usr_role)
		 values(#usr_max.id#,#session.hub#,#hub_xref_active#,1,#now()#,#hub_xref_usr_role#)
		</cfquery>

       <cfif choose_comp is 3>

       <cfelseif choose_comp is 1>

       <!--- Create New Company --->

	    <cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into company(company_hub_id, company_marketplace, company_duns, company_registered, company_updated, company_admin_id, company_name, company_website)
		 values(#session.hub#, 1, '#duns#', #now()#, #now()#, #usr_max.id#,'#company_name#','#company_website#')
		</cfquery>

	    <cfquery name="company_max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(company_id) as id from company
		</cfquery>

	    <cfquery name="update_usr_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update usr
		 set usr_company_id = #company_max.id#
		 where usr_id = #usr_max.id#
		</cfquery>

		<!--- Insert Company Alignments --->

		<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete align
		 where (align_type_id in (1,2,3,4)) and
			   (align_hub_id = #session.hub#) and
			   (align_company_id = #company_max.id#)
		</cfquery>

		<cfloop index="l_element" list=#lenses#>
			<cfquery name="insert_lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 1, #l_element#)
			</cfquery>
		</cfloop>

		<cfloop index="cm_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#company_max.id#, 2, #cm_element#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfloop index="cs_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#company_max.id#, 3, #cs_element#,#session.hub#)
			</cfquery>

		</cfloop>

		<cfloop index="ct_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#company_max.id#, 4, #ct_element#,#session.hub#)
			</cfquery>

		</cfloop>

	   <cfelseif choose_comp is 2>

	   <!--- Associate user to existing company --->

		<cfquery name="associate_use" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
         update usr
         set usr_company_id = #textbox1#
         where usr_id = #usr_max.id#
        </cfquery>

	   </cfif>

    <!--- Insert User Alignments --->

		<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete align
		 where (align_type_id in (1,2,3,4)) and
			   (align_hub_id = #session.hub#) and
			   (align_usr_id = #usr_max.id#)
		</cfquery>

		<cfloop index="l_element" list=#lenses#>
			<cfquery name="insert_lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 1, #l_element#)
			</cfquery>
		</cfloop>

		<cfloop index="m_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#usr_max.id#, 2, #m_element#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfloop index="s_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#usr_max.id#, 3, #s_element#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfloop index="t_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#usr_max.id#, 4, #t_element#, #session.hub#)
			</cfquery>

		</cfloop>

</cftransaction>

<cfif isdefined("welcome")>

	<cfquery name="welcome" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from welcome
	 where welcome_hub_id = #session.hub# and
		   welcome_active = 1
	 order by welcome_order DESC
	</cfquery>

	<cfloop query="welcome">

		<cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into feed
		 (feed_status,
		  feed_posted_by_hub_id,
		  feed_title,
		  feed_desc,
		  feed_updated,
		  feed_created,
		  feed_type_id
		  )
		 values
		 (1,
		   #session.hub#,
		  '#welcome.welcome_title#',
		  '#welcome.welcome_message#',
		  #now()#,
		  #now()#,
		   2
		   )
		</cfquery>

		<cfquery name="feedmax" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(feed_id) as id from feed
		</cfquery>

		<cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into feed_usr
		 (feed_usr_feed_id, feed_usr_usr_id, feed_usr_created)
		 values
		 (#feedmax.id#,#usr_max.id#,#now()#)
		</cfquery>

	</cfloop>

</cfif>

<!-- Notify User --->

<cfif isdefined("pass")>

	<cfmail from="#hub_info.hub_name# <noreply@ratio.exchange>"
			  to="#usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#hub_info.hub_name# - Invitation to Join">
	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head><div class="center">
	<body class="body">

	<cfoutput>

		<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 20px; font-weight: bold; padding-top: 20px; padding-bottom: 10px;">Congratulations, you've been invited to join the #hub_info.hub_name# Hub.</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your account is now active and ready for use.</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To login, please visit <a href="#hub_info.hub_login_page#">#hub_info.hub_login_page#</a></td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Login Email: </b>#usr_email#</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Temporary Password: </b>#result1#</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>#hub_info.hub_support_name#</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_email#</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_phone#</td></tr>
		</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>

</cfif>

<cfelseif #button# is "Remove User">

	    <cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete hub_xref
		 where hub_xref_hub_id = #session.hub# and
		       hub_xref_usr_id = #usr_id#
		</cfquery>

<cflocation URL="index.cfm?u=4" addtoken="no">

<cfelseif #button# is "Save Updates">

<cftransaction>

	    <cfquery name="update1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update hub_xref
		 set hub_xref_active = #hub_xref_active#,
		     hub_xref_usr_role = #hub_xref_usr_role#
		 where hub_xref_hub_id = #session.hub# and
		       hub_xref_usr_id = #usr_id#
		</cfquery>

	    <cfquery name="update2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update usr
		 set usr_email = '#usr_email#'
		 where usr_id = #usr_id#
		</cfquery>

</cftransaction>

<cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Invite">

<cftransaction>

	    <cfquery name="get_usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select usr_id, usr_email from usr
		 where usr_email = '#session.new_user_email#'
		</cfquery>

		<cfif #get_usr.recordcount# is 1>

	    <cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref(hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_default, hub_xref_joined, hub_xref_usr_role)
		 values(#get_usr.usr_id#,#session.hub#,1,0,#now()#,2)
		</cfquery>

		 <!--- Insert User Alignments --->

		<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete align
		 where (align_type_id in (1,2,3,4)) and
			   (align_hub_id = #session.hub#) and
			   (align_usr_id = #get_usr.usr_id#)
		</cfquery>

		<cfloop index="m_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#get_usr.usr_id#, 2, #m_element#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfloop index="s_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#get_usr.usr_id#, 3, #s_element#, #session.hub#)
			</cfquery>

		</cfloop>

		<cfloop index="t_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#get_usr.usr_id#, 4, #t_element#, #session.hub#)
			</cfquery>

		</cfloop>

<cfif isdefined("welcome")>

	<cfquery name="welcome" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from welcome
	 where welcome_hub_id = #session.hub# and
		   welcome_active = 1
	 order by welcome_order DESC
	</cfquery>

	<cfloop query="welcome">

		<cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into feed
		 (feed_status,
		  feed_posted_by_hub_id,
		  feed_title,
		  feed_desc,
		  feed_updated,
		  feed_created,
		  feed_type_id
		  )
		 values
		 (1,
		   #session.hub#,
		  '#welcome.welcome_title#',
		  '#welcome.welcome_message#',
		  #now()#,
		  #now()#,
		   2
		   )
		</cfquery>

		<cfquery name="feedmax" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(feed_id) as id from feed
		</cfquery>

		<cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into feed_usr
		 (feed_usr_feed_id, feed_usr_usr_id, feed_usr_created)
		 values
		 (#feedmax.id#,#get_usr.usr_id#,#now()#)
		</cfquery>

	</cfloop>

</cfif>

	<cfmail from="#hub_info.hub_name# <noreply@ratio.exchange>"
			  to="#get_usr.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#hub_info.hub_name# - #email_subject#">
	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head><div class="center">
	<body class="body">

	<cfoutput>

		<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 20px; font-weight: bold; padding-top: 20px; padding-bottom: 10px;">Congratulations, you've been invited to join the #hub_info.hub_name# EXCHANGE Hub.</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your account is now active and ready for use.</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#email_message#</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To login, please visit <a href="#hub_info.hub_login_page#">#hub_info.hub_login_page#</a></td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>#hub_info.hub_support_name#</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_email#</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_phone#</td></tr>

		</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>

		<cfelse>

		Sorry, an unexpected problem occured.  Please contact the EXCHANGE to get this resolved.

		</cfif>


</cftransaction>

</cfif>

<cflocation URL="index.cfm" addtoken="no">