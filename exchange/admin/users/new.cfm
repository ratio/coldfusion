<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <div class="main_box">

		   <cfquery name="state" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from state
			order by state_name
		   </cfquery>

		   <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from market
		   </cfquery>

		   <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from sector
			order by sector_name
		   </cfquery>

		   <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from topic
			order by topic_name
		   </cfquery>

		   <cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from lens
			order by lens_name
		   </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add User</td></tr>
		   <tr><td>&nbsp;</td></tr>

		   <cfif isdefined("ec")>
		    <cfif ec is 1>
			    <tr><td class="feed_option" colspan=2><font color="red"><b>Email address already exists.</b></td></tr>
		    </cfif>
		    <tr><td>&nbsp;</td></tr>
		   </cfif>

             <form action="db.cfm" method="post">

 				   <tr><td class="feed_option"><b>First Name</b></td></tr>
                   <tr><td><input type="text" name="usr_first_name" size=25> *</td></tr>
  			       <tr><td class="feed_option"><b>Last Name</b></td></tr>
                   <tr><td><input type="text" name="usr_last_name" size=25> *</td></tr>
				   <tr><td class="feed_option"><b>Email</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_email" size=25><font size=2> *</td></tr>
				   <tr><td class="feed_option"><b>Title</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_title" size=25></td></tr>
			       <tr><td class="feed_option"><b>Address</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_address_line_1" size=50></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_address_line_2" size=50></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_city" size=30>&nbsp;&nbsp;

                   <select name="usr_state" style="height: 21px;">
                   <option value=0>
                   <cfoutput query="state">
                   <option value="#state_abbr#">#state_name#
                   </cfoutput>
                   </select>
                   &nbsp;&nbsp;

                   <input type="text" name="usr_zip" size=10>

                   </td></tr>

			       <tr><td class="feed_option"><b>Phone</b></td></tr>
                   <tr><td><input type="tel" name="usr_phone" size=15></td></tr>
				   <tr><td class="feed_option"><b>Company Name</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="company_name" size=45></td></tr>
				   <tr><td class="feed_option"><b>Website</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="company_website" size=45></td></tr>
				   <tr><td class="feed_option"><b>DUNS Number</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="duns" size=25>&nbsp;&nbsp;<input type="checkbox" name="sync">&nbsp;Sync from SAM?</td></tr>

           <tr><td colspan=2>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>
  			    <td class="feed_option" style="padding-left: 0px; padding-bottom: 10px;"><b>Lens</b></td>
  			    <td class="feed_option" style="padding-left: 0px; padding-bottom: 10px;"><b>Markets</b></td>
  				<td class="feed_option" style="padding-left: 3px; padding-bottom: 10px;"><b>Sectors</b></td>
  				<td class="feed_option" style="padding-left: 3px; padding-bottom: 10px;"><b>Capabilities</b></td>
  				</tr>
  			<tr>

  				 <td width=210>
  					  <select style="font-size: 12px; width: 190px; height: 160px; padding-left: 10px; padding-top: 5px;" name="lenses" multiple>
   					   <cfoutput query="lens">
  						<option value=#lens_id# selected>#lens_name#
  					   </cfoutput>
  					  </select>

  				</td>

  				 <td width=140>
  					  <select style="font-size: 12px; width: 120px; height: 160px; padding-left: 10px; padding-top: 5px;" name="markets" multiple>
  					   <cfoutput query="market">
  						<option value=#market_id#>#market_name#
  					   </cfoutput>
  					  </select>

  				</td>

  				 <td width=160>
  					  <select style="font-size: 12px; width: 150x; height: 160px; padding-left: 10px; padding-top: 5px;" name="sectors" multiple>
  					   <cfoutput query="sector">
  						<option value=#sector_id#>#sector_name#
  					   </cfoutput>
  					  </select>

  				</td>

  				<td>
  					  <select style="font-size: 12px; width: 180px; height: 160px; padding-left: 10px; padding-top: 5px;" name="topics" multiple>
  					   <cfoutput query="topic">
  						<option value=#topic_id#>#topic_name#
  					   </cfoutput>
  					  </select>

  				</td></tr>

           </table>



           </td></tr>


           <tr><td class="feed_option"><b>Premium Services</b></td></tr>

           <tr><td class="feed_option">
           <input type="checkbox" name="premium_awards">&nbsp;&nbsp;Awards Research
           &nbsp;&nbsp;<input type="checkbox" name="premium_forecast">&nbsp;&nbsp;Forecasts
           &nbsp;&nbsp;<input type="checkbox" name="premium_competitor">&nbsp;&nbsp;Competitor Engine
           &nbsp;&nbsp;<input type="checkbox" name="premium_sbir">&nbsp;&nbsp;SBIR/STTR Engine
           &nbsp;&nbsp;<input type="checkbox" name="premium_grants">&nbsp;&nbsp;Grants</td></tr>

		   <tr><td height=5></td></tr>
 		   <tr><td colspan=2>

		   <tr><td class="feed_option"><b>User Status</b>&nbsp;
		   <select name="usr_active">
			<option value=1>Active
			<option value=0>Not Active
		   </select>
		   </td></tr>

		   <tr><td>&nbsp;</td></tr>

		   <tr><td colspan=2>

		   <input class="button_blue" type="submit" name="button" value="Add" vspace=10>&nbsp;&nbsp;
		   <input class="button_blue" type="submit" name="button" value="Cancel" vspace=10></td></tr>

           </td></tr>

		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

