<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_xref_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">EDIT USER</td>
              <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

        <cfoutput>

		<form name="thisForm" action="edit_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif isdefined("u")>
         <cfif u is 1>
          <tr><td colspan=2 class="feed_sub_header" style="color: red;">Email address already exists and must be unique for each user.</td></tr>
          <tr><td height=10></td></tr>
         </cfif>
        </cfif>

	   <tr><td class="feed_sub_header" width=15%>First Name</td>
	       <td><input class="input_text" type="text" name="usr_first_name" value="#edit.usr_first_name#" style="width: 200px;" maxlength=50 required></td></tr>

	   <tr><td class="feed_sub_header">Last Name</td>
	       <td><input class="input_text" type="text" name="usr_last_name" value="#edit.usr_last_name#" style="width: 200px;" maxlength=50 required></td></tr>

	   <tr><td class="feed_sub_header">Company</td>
	       <td><input class="input_text" type="text" name="usr_company_name" value="#edit.usr_company_name#" style="width: 300px;" maxlength=50 required></td></tr>

	   <tr><td class="feed_sub_header">Email</td>
	       <td><input class="input_text" type="email" name="usr_email" value="#decrypt(edit.usr_email,session.key, "AES/CBC/PKCS5Padding", "HEX")#" style="width: 300px;" maxlength=50 required></td></tr>

	   <tr><td class="feed_sub_header">Title</td>
	       <td><input class="input_text" style="width: 300px;" type="text" value="#edit.usr_title#" name="usr_title" maxlength=50></td></tr>

		<tr><td class="feed_sub_header" width=75>Role</td>
		    <td>

			<select name="hub_xref_role_id" class="input_select">
			 <option value=1 <cfif #edit.hub_xref_role_id# is 1>selected</cfif>>Booz Allen Hamilton or Affiliate
			 <option value=2 <cfif #edit.hub_xref_role_id# is 2>selected</cfif>>Partner
			</select>

			</td></tr>


		<tr><td class="feed_sub_header" width=75>Active</td>
		    <td>

			<select name="hub_xref_active" class="input_select">
			 <option value=0 <cfif #edit.hub_xref_active# is 0>selected</cfif>>No
			 <option value=1 <cfif #edit.hub_xref_active# is 1>selected</cfif>>Yes
			</select>

			<span class="link_small_gray">Determines whether user can login</span>

			</td></tr>

		<tr><td class="feed_sub_header" width=75>Add Partners</td>
		    <td>

			<select name="hub_xref_company_add" class="input_select">
			 <option value=0 <cfif #edit.hub_xref_company_add# is 0>selected</cfif>>No
			 <option value=1 <cfif #edit.hub_xref_company_add# is 1>selected</cfif>>Yes
			</select>

			<span class="link_small_gray">Allows user to add a Company to In Network Partners</span>


			</td></tr>

		<tr><td class="feed_sub_header" width=75>Access Rights</td>
		    <td>

			<select name="hub_xref_usr_role" class="input_select">
			 <option value=0 <cfif #edit.hub_xref_usr_role# is 0>selected</cfif>>Normal User
			 <option value=1 <cfif #edit.hub_xref_usr_role# is 1>selected</cfif>>Administrator
			</select>

			</td></tr>

	   <tr><td height=10></td></tr>
       <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td></td><td>

	   <input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
	   <input class="button_blue_large" type="submit" name="button" value="Reset Password" vspace=10 onclick="return confirm('Reset Password?\r\nAre you sure you want to reset this users password?');">&nbsp;&nbsp;

	   </td></tr>

        <input type="hidden" name="i" value="#i#">

	   </table>

		</form>

		</cfoutput>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



