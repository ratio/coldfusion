<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_xref_hub_id = #session.hub#
</cfquery>

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       portfolio_hub_id = #session.hub#
 order by portfolio_name
</cfquery>

<cfquery name="pipelines" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pipeline
 where pipeline_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       pipeline_hub_id = #session.hub#
 order by pipeline_name
</cfquery>

<cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_created_by_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       market_report_hub_id = #session.hub#
 order by market_report_name
</cfquery>

<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_owner_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       need_hub_id = #session.hub#
 order by need_name
</cfquery>

<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_created_by_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       challenge_hub_id = #session.hub#
 order by challenge_name
</cfquery>


<cfquery name="new" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_first_name, usr_last_name, usr_id, usr_email from hub_xref
 join usr on usr.usr_id = hub_xref.hub_xref_usr_id
 left join hub_role on hub_role_id = hub_xref.hub_xref_role_id
 where hub_xref.hub_xref_hub_id = #session.hub# and
       hub_xref.hub_xref_active = 1
 order by usr_last_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">User Records</td>
              <td class="feed_sub_header" align=right colspan=2><a href="index.cfm">Return</a></td></tr>
          <tr><td colspan=3><hr></td></tr>

          <tr><td height=10></td></tr>
           <tr>
               <cfoutput>
               <td colspan=2 class="feed_header">#edit.usr_first_name# #edit.usr_last_name# Records</td>
               </cfoutput>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>

               <form action="change_all.cfm" method="post">

               <b>Change ownership of all records to</b>&nbsp;&nbsp;
               <select name="new_id" class="input_select">
               <cfoutput query="new">
              	<option value=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#usr_last_name#, #usr_first_name# (#tostring(tobinary(usr_email))#)
               </cfoutput>
               </select>

               <input type="submit" name="button" value="Change" class="button_blue"  onclick="return confirm('Transfer Ownership?\r\nAre you sure you want to transfer ownership for all records to the new user?');">

               <cfoutput>
               <input type="hidden" name="i" value=#i#>
               </cfoutput>

               </form>

               </td></tr>

          <cfif isdefined("u")>
           <cfif u is 1>
            <tr><td class="feed_sub_header" style="color: green;">Portfolio ownership has been successfully changed.</td></tr>
           <cfelseif u is 2>
            <tr><td class="feed_sub_header" style="color: green;">Opportunity ownership has been successfully changed.</td></tr>
           <cfelseif u is 3>
            <tr><td class="feed_sub_header" style="color: green;">Market Report ownership has been successfully changed.</td></tr>
           <cfelseif u is 4>
            <tr><td class="feed_sub_header" style="color: green;">All records have been successfully changed.</td></tr>
           <cfelseif u is 5>
            <tr><td class="feed_sub_header" style="color: green;">Challenge has been successfully changed.</td></tr>
           <cfelseif u is 6>
            <tr><td class="feed_sub_header" style="color: green;">Need has been successfully changed.</td></tr>
           </cfif>
          </cfif>

        </table>

        <!--- Portfolios --->

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" colspan=3>Portfolios Owned</td></tr>
          <tr><td colspan=3><hr></td></tr>

          <cfif portfolios.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Portfolios were found.</td></tr>
          <cfelse>
           <cfset counter = 0>
           <cfoutput query="portfolios">
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

                <td width=2%><img src="/images/icon_bluebox.png" width=10></td>
                <td class="feed_sub_header">#portfolios.portfolio_name#</td>
                <td class="feed_sub_header" align=right><a href="change.cfm?t=portfolio&id=#encrypt(portfolios.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Change Owner</a></td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

          </cfif>

        </table>

        <!--- Pipelines --->

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=20></td></tr>
          <tr><td class="feed_sub_header" colspan=3>Opportunities Boards Owned</td></tr>
          <tr><td colspan=3><hr></td></tr>

          <cfif pipelines.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunity Boards were found.</td></tr>
          <cfelse>
           <cfset counter = 0>
           <cfoutput query="pipelines">
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

                <td width=2%><img src="/images/icon_bluebox.png" width=10></td>
                <td class="feed_sub_header">#pipelines.pipeline_name#</td>
                <td class="feed_sub_header" align=right><a href="change.cfm?t=opportunity&id=#encrypt(pipelines.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Change Owner</a></td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

          </cfif>

        </table>


        <!--- Market Reports --->

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=20></td></tr>
          <tr><td class="feed_sub_header" colspan=3>Market Reports Owned</td></tr>
          <tr><td colspan=3><hr></td></tr>

          <cfif market.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Market Reports were found.</td></tr>
          <cfelse>
           <cfset counter = 0>
           <cfoutput query="market">
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

                <td width=2%><img src="/images/icon_bluebox.png" width=10></td>
                <td class="feed_sub_header">#market.market_report_name#</td>
                <td class="feed_sub_header" align=right><a href="change.cfm?t=market&id=#encrypt(market.market_report_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Change Owner</a></td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

          </cfif>

        </table>

        <!--- Challenges --->

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=20></td></tr>
          <tr><td class="feed_sub_header" colspan=3>Challenges Owned</td></tr>
          <tr><td colspan=3><hr></td></tr>

          <cfif challenges.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Challenges were found.</td></tr>
          <cfelse>
           <cfset counter = 0>
           <cfoutput query="challenges">
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

                <td width=2%><img src="/images/icon_bluebox.png" width=10></td>
                <td width=400 class="feed_sub_header">#challenge_name#</td>
                <td class="feed_sub_header" align=right><a href="change.cfm?t=challenge&id=#encrypt(challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Change Owner</a></td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

          </cfif>

        </table>

        <!--- Needs --->

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=20></td></tr>
          <tr><td class="feed_sub_header" colspan=3>Needs Owned</td></tr>
          <tr><td colspan=3><hr></td></tr>

          <cfif needs.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Needs were found.</td></tr>
          <cfelse>
           <cfset counter = 0>
           <cfoutput query="needs">
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

                <td width=2%><img src="/images/icon_bluebox.png" width=10></td>
                <td	 class="feed_sub_header">#need_name#</td>
                <td class="feed_sub_header" align=right><a href="change.cfm?t=need&id=#encrypt(need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&i=#i#">Change Owner</a></td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

          </cfif>

        </table>





	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



