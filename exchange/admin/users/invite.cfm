<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Add User</td>
		   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Email</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Role & Permissions</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header" style="color: ffffff;" align=center width=200>Send Invite</td>
	   </tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td height=20></td></tr>

	   <form action="next.cfm" method="post">

       <cfoutput>
       <tr><td class="feed_sub_header" width=150>User</td>
           <td class="feed_sub_header">#session.usr_full_name#</td></tr>

       <tr><td class="feed_sub_header" width=150>Email Address</td>
           <td class="feed_sub_header">#session.new_email#</td></tr>
       </cfoutput>

	   <tr><td class="feed_sub_header">Invitation Message</td></tr>

	   <tr><td class="feed_sub_header" colspan=2><textarea name="hub_xref_invite_messsage" class="input_textarea" style="width: 800px; height: 200px;"></textarea></td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
	   <input class="button_blue_large" type="submit" name="button" value="Send Invitation" vspace=10>
        &nbsp;&nbsp;or&nbsp;&nbsp;
	   <input class="button_blue_large" type="submit" name="button" value="Provision Instantly" vspace=10>
	   &nbsp;&nbsp;

	   <span class="link_small_gray"><u>Provision Instantly</u> actives the account immediately and does not send an invitation email.  For new users, password will be set to Password123!</span>

	   </td></tr>

	  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

