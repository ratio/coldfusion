<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_role
 where hub_role_hub_id = #session.hub#
 order by hub_role_name
</cfquery>

<cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from market
	where market_hub_id = #session.hub#
    order by market_name
</cfquery>

<cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from sector
    where sector_hub_id = #session.hub#
	order by sector_name
</cfquery>

<cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from topic
	where topic_hub_id = #session.hub#
    order by topic_name
</cfquery>

<cfquery name="types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr_type
	where usr_type_hub_id = #session.hub#
	order by usr_type_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Add User</td>
              <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

		<form name="thisForm" action="new_user_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif isdefined("u")>
         <cfif u is 1>
          <tr><td colspan=2 class="feed_sub_header" style="color: red;">Email address already exists and must be unique for each user.</td></tr>
          <tr><td height=10></td></tr>
         </cfif>
        </cfif>

		<cfquery name="comp_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select hub_comp_company_id from hub_comp
		 where hub_comp_hub_id = #session.hub#
		</cfquery>

		<cfif comp_list.recordcount is 0>
		 <cfset #clist# = 0>
		<cfelse>
		 <cfset #clist# = #valuelist(comp_list.hub_comp_company_id)#>
		</cfif>

		<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from company
		 where company_id in (#clist#)
		 order by company_name
		</cfquery>

       <cfoutput>

	   <tr><td class="feed_sub_header" width=15%>First Name</td>
	       <td><input class="input_text" type="text" name="usr_first_name" style="width: 200px;" maxlength=50 required></td></tr>

	   <tr><td class="feed_sub_header">Last Name</td>
	       <td><input class="input_text" type="text" name="usr_last_name" style="width: 200px;" maxlength=50 required></td></tr>

	   <tr><td class="feed_sub_header">Title</td>
	       <td><input class="input_text" style="width: 300px;" type="text" name="usr_title" maxlength=50></td></tr>

	   <tr><td class="feed_sub_header">Email</td>
	       <td><input class="input_text" type="email" name="usr_email" style="width: 300px;" maxlength=50 required>

	       <span class="link_small_gray">Must be unique to this Exchange</span

	       </td></tr>

	   <tr><td class="feed_sub_header">Phone Number</td>
	       <td><input class="input_text" type="text" name="usr_phone" style="width: 300px;" maxlength=50></td></tr>

	   <tr><td class="feed_sub_header">Cell Phone</td>
	       <td><input class="input_text" type="text" name="usr_cell_phone" style="width: 300px;" maxlength=50></td></tr>


	   <tr><td class="feed_sub_header">Company Name</td>
	       <td><input class="input_text" type="text" name="usr_company_name" style="width: 300px;" maxlength=50 required></td></tr>

       </cfoutput>

	   <tr><td class="feed_sub_header">Company Association</td>
	       <td>
	       <select name="usr_company_id" class="input_select" style="width: 300px;">
	        <option value=0>Select Company
	        <cfoutput query="companies">
	         <option value=#company_id#>#company_name#
	        </cfoutput>
	       </select></td></tr>


               <tr><td class="feed_sub_header" valign=top>Alignments</td>
                    <td valign=top>

			<table cellspacing=0 cellpadding=0 border=0>

				<tr><td class="feed_sub_header"><b>Markets</b></td>
					<td class="feed_sub_header"><b>Accounts / Sectors</b></td>
					<td class="feed_sub_header"><b>Capabilities and Services</b></td>
				</tr>

			<tr>

				 <td width=200>
					  <select class="input_select" name="market_id" style="width: 150px; height: 200px;" multiple>
					   <cfoutput query="market">
						<option value=#market_id#>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=400>
					  <select class="input_select" style="width: 350px; height: 200px;" name="sector_id" multiple>
					   <cfoutput query="sector">
						<option value=#sector_id#>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select class="input_select" style="width: 350px; height: 200px;" name="topic_id" multiple>
					   <cfoutput query="topic">
						<option value=#topic_id#>#topic_name#
					   </cfoutput>
					  </select>

				</td></tr>

				<tr><td height=10></td></tr>
                <tr><td class="link_small_gray" colspan=2>To select multiple items please use the SHIFT or CTRL key.</td></tr>
				<tr><td height=10></td></tr>

			  </table>

                    </td></tr>

	   <tr><td class="feed_sub_header" valign=top>User Type(s)</td>
	       <td valign=top>

			<table cellspacing=0 cellpadding=0 border=0>
             <cfoutput query="types">
              <tr>
                 <td width=30><input type="checkbox" style="width: 22px; height: 22px;" name="usr_type_id" value=#types.usr_type_id#></td>
                 <td class="feed_sub_header" style="font-weight: normal;">#types.usr_type_name#</td>
              </tr>
             </cfoutput>
            </table>

           </td>
       </tr>

	   <tr><td class="feed_sub_header">Role</td>
	       <td>
	       <select name="hub_xref_role_id" class="input_select" style="width: 200px;">
	        <cfoutput query="profiles">
	         <option value=#hub_role_id#>#hub_role_name#
	        </cfoutput>
	       </select></td></tr>

	   <tr><td class="feed_sub_header">Access Rights</td>
	       <td>
	       <select name="hub_xref_usr_role" class="input_select" style="width: 200px;">
	         <option value=0>Normal User
	         <option value=1>Administrator
	       </select></td></tr>

		<tr><td class="feed_sub_header" width=75>Add Companies</td>
		    <td>

			<select name="hub_xref_company_add" class="input_select">
			 <option value=0>No
			 <option value=1>Yes
			</select>

			<span class="link_small_gray">Allows user to identify Companies as "In Network"</span>

			</td></tr>

	   <tr><td class="feed_sub_header" width=75>Company Updates</td>
		   <td>

			<select name="hub_xref_profile_update" class="input_select">
			 <option value=0>No
			 <option value=1>Yes
			</select>

			<span class="link_small_gray">Allow user to update information within their Company profile</span>

			</td></tr>

		<tr><td class="feed_sub_header" width=75>Active</td>
		    <td>

			<select name="hub_xref_active" class="input_select">
			 <option value=0>No
			 <option value=1 selected>Yes
			</select>

			<span class="link_small_gray">Determines if user can login and is active in this Exchange</span>

			</td></tr>

	   <tr><td class="feed_sub_header"><b>Password</b></td>
           <td><input type="password" class="input_text" name="usr_password" style="width: 200px;" maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,}" title="Passwords must contain at least one number, one uppercase and lowercase letter, a special character, and at least 8 characters" required></td></tr>

       <tr><td></td><td class="link_small_gray">Passwords must contain at least one number, one uppercase and lowercase letter, a special character, and at least 8 characters.</td></tr>

       <tr><td height=10></td></tr>

	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td></td><td>

	   <input class="button_blue_large" type="submit" name="button" value="Add User" vspace=10>

	   </td></tr>

	   </table>

		</form>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



