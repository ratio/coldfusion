<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 join hub_role on hub_role_id = hub_xref_role_id
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_xref_hub_id = #session.hub#
</cfquery>

<cfquery name="advisors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from advisor
 join usr on usr_id = advisor_advisor_id
 where advisor_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       advisor_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/admin/admin_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">User Profile</td>
              <td class="feed_sub_header" align=right>

              <a href="index.cfm">Return</a></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif isdefined("u")>
         <cfif u is 2>
          <tr><td colspan=2 class="feed_sub_header" style="color: green;">User information has been successfully updated.</td></tr>
          <tr><td height=10></td></tr>
         </cfif>
        </cfif>

	    </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("u")>
           <cfif u is 4>
            <tr><td colspan=3 class="feed_sub_header" style="color: green;">Advisor has been successfully added.</td></tr>
           <cfelseif u is 5>
            <tr><td colspan=3 class="feed_sub_header" style="color: green;">Advisor has been successfully updated.</td></tr>
           <cfelseif u is 6>
            <tr><td colspan=3 class="feed_sub_header" style="color: red;">Advisor has been successfully deleted.</td></tr>
           </cfif>
          </cfif>

          <tr><td class="feed_header">Summary</td>
              <td class="feed_sub_header" align=right>

              <cfoutput>
              <a href="edit.cfm?i=#i#"><img src="/images/icon_edit.png" width=20 alt="Update" title="Update" hspace=5></a>
              <a href="edit.cfm?i=#i#">Update</a>
              &nbsp;|&nbsp;
              <a href="remove.cfm?i=#i#" onclick="return confirm('Remove User?\r\nAre you sure you want to remove this user?');"><img src="/images/delete.png" width=20 alt="Update" title="Update" hspace=5></a>
              <a href="remove.cfm?i=#i#" onclick="return confirm('Remove User?\r\nAre you sure you want to remove this user?');">Remove User</a>
              </cfoutput>

              </td></tr>

        </table>

        <cfoutput>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td valign=top width=33%>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header" width=150>Name</td>
              <td class="feed_sub_header" style="font-weight: normal;">#profile.usr_first_name# #profile.usr_last_name#</td></tr>
          <tr><td class="feed_sub_header">Email Address</td>
              <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(profile.usr_email))#</td></tr>
          <tr><td class="feed_sub_header">Title</td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif profile.usr_title is "">Not Provided<cfelse>#profile.usr_title#</cfif></td></tr>

          <tr><td class="feed_sub_header">Location</td>
              <td class="feed_sub_header" style="font-weight: normal;">

              <cfif profile.usr_city is "" and #profile.usr_state# is "">
               Not Provided
              <cfelse>
              #profile.usr_city# #profile.usr_state#
              </cfif>

              </td></tr>
          <tr><td class="feed_sub_header" width=150>Phone</td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_phone# is "">Not Provided<cfelse>#profile.usr_phone#</cfif></td></tr>
          <tr><td class="feed_sub_header">Cell</td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_cell_phone# is "">Not Provided<cfelse>#profile.usr_cell_phone#</cfif></td></tr>

         </table>

        <td><td valign=top width=33%>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header" width=100>Active</td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.hub_xref_active# is 1>Yes<cfelse>No</cfif></td></tr>
          <tr><td class="feed_sub_header">Role</td>
              <td class="feed_sub_header" style="font-weight: normal;">#profile.hub_role_name#</td></tr>
          <tr><td class="feed_sub_header">Access Level</td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif profile.hub_xref_usr_role is 1>Administrator<cfelse>Normal User</cfif></td></tr>
          <tr><td class="feed_sub_header">Add Companies</td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif profile.hub_xref_company_add is 1>Yes<cfelse>No</cfif></td></tr>

        </table>


        </td>

        <td><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header">Invitation Sent</td>
              <td class="feed_sub_header" style="font-weight: normal;">

              <cfif profile.hub_xref_invite_sent is "">
               No Sent
              <cfelse>
              #dateformat(profile.hub_xref_invite_sent,'mmm dd, yyyy')# at #timeformat(profile.hub_xref_invite_sent)#
              </cfif>

              </td></tr>
          <tr><td class="feed_sub_header">Invitation Accepted</td>
              <td class="feed_sub_header" style="font-weight: normal;">

              <cfif profile.hub_xref_invite_accepted is "">
               Not Accepted
              <cfelse>
              #dateformat(profile.hub_xref_invite_accepted,'mmm dd, yyyy')# at #timeformat(profile.hub_xref_invite_accepted)#
              </cfif></td></tr>

        </table>

        </tr>

        </table>

        </cfoutput>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td colspan=2><hr></td></tr>
          <tr><td class="feed_header">Advisors</td>
              <td class="feed_sub_header" align=right>

              <cfoutput>
              <a href="advisor_add.cfm?i=#i#"><img src="/images/plus3.png" width=15 alt="Add Advisor" title="Add Advisor" hspace=5></a>
              <a href="advisor_add.cfm?i=#i#">Add Advisor</a>
              </cfoutput>

              </td></tr>

        </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfif advisors.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Advisors have been selected for this user.</td></tr>
         <cfelse>

         <cfoutput query="advisors">

            <tr>
               <td width=60>

				 <cfif #advisors.usr_photo# is "">
				  <a href="advisor_edit.cfm?i=#i#&advisor_id=#advisor_id#"><img src="/images/headshot.png" height=40 width=40 vspace=5 border=0></a>
				 <cfelse>
				  <a href="advisor_edit.cfm?i=#i#&advisor_id=#advisor_id#"><img src="#media_virtual#/#advisors.usr_photo#" height=40 width=40 vspace=5 border=0></a>
				 </cfif>

               </td>

               <td class="feed_sub_header" width=200><a href="advisor_edit.cfm?i=#i#&advisor_id=#advisor_id#">#usr_first_name# #usr_last_name#</a></td>
               <td class="feed_sub_header" style="font-weight: normal;">#advisor_role#</td>

            </tr>

         </cfoutput>

         </cfif>
        </table>

	 </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>



