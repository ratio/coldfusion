<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into sector (sector_name, sector_hub_id)
	 values ('#sector_name#',#session.hub#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update sector
	 set sector_name = '#sector_name#'
	 where sector_id = #sector_id#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete sector
	 where sector_id = #sector_id#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

