<cfinclude template="/exchange/security/check.cfm">

<cfquery name="sectors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from sector
  where sector_hub_id = #session.hub#
  order by sector_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Sectors</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>



	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Sector has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Sector has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Sector has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Sector</a></td></tr>

      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #sectors.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Sectors exist.</td></tr>

        <cfelse>
				<td class="feed_sub_header" width=300>Sector Name</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="sectors">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=middle><a href="edit.cfm?sector_id=#sector_id#">#sector_name#</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

