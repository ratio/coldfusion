<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="email_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_email = '#email#'
	</cfquery>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete hub_xref
	 where hub_xref_hub_id = #session.hub# and
	       hub_xref_usr_id = #email_info.usr_id#
	</cfquery>

	<cfquery name="add_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert hub_xref(hub_xref_usr_id, hub_xref_hub_id, hub_xref_invite_sent, hub_xref_active, hub_xref_invitation_code, hub_xref_default, hub_xref_usr_role)
	 values(#email_info.usr_id#,#session.hub#,#now()#,0,'#invite_code#',0,2)
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(hub_xref_id) as id from hub_xref
	</cfquery>

	<cfquery name="hub_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #hub_info.hub_owner_id#
	</cfquery>

	<cfmail from="The Exchange <noreply@ratio.exchange>"
			  to="#email_info.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#subject#">

	<html>
	<head>
	<title>RATIO Exchange</title>
	</head><div class="center">
	<body class="body">
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <cfoutput>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(message,"#chr(10)#","<br>","all")#</td></tr>
		 <tr><td>&nbsp;</td></tr>

		 <tr><td class="feed_option"><b>#hub_info.hub_name#</b></td></tr>
		 <tr><td class="feed_option">#hub_info.hub_desc#</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td class="feed_option"><b>Invitation sent from: </b> #hub_admin.usr_first_name# #hub_admin.usr_last_name#</td></tr>
		 <tr><td class="feed_option"><b>Invitation code: </b> #invite_code#</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b><a href="https://go.ratio.exchange/letsgo/invitation.cfm?i=#max.id#">Click here</a></b> to accept the invitation and join the HUB.</td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Exchange Support Team</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
		 <tr><td>&nbsp;</td></tr>
	 </cfoutput>
	</table>
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
	 <tr><td>&nbsp;</td></tr>
	 <tr><td height=75 valign=middle><img src="#image_virtual#/exchange_logo.png" width=160 hspace=5></td></tr>
	</table>
	</body>
	</html>
	</cfmail>

</cftransaction>

<cflocation URL="index.cfm?u=2" addtoken="no">