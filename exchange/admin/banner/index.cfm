<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Change Banner</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/index.cfm">Settings</td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="db.cfm" method="post" enctype="multipart/form-data" >

           <cfoutput>

           <tr><td class="feed_sub_header" valign=top><b>Banner</b></td></tr>
		   <tr><td class="feed_sub_header">

				<cfif #edit.hub_banner# is "">
				  <input type="file" name="hub_banner">
				<cfelse>
				  <img src="#media_virtual#/#edit.hub_banner#" width=800>
				  <br><br>
				  <b>Change Banner</b><br><br>
				  <input type="file" name="hub_banner"><br><br>
				  <input type="checkbox" name="remove_banner_attachment">&nbsp;or, check to remove banner
				 </cfif>

		   </td></tr>

		   <tr><td height=10></td></tr>

		   <tr><td class="link_small_gray">Preferred size is 1,200 pixels width, 300 pixcels height.</td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td colspan=3><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10>

           </cfoutput>
           </form>

	    </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

