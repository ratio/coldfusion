<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

<cfif isdefined("remove_banner_attachment")>

		<cfquery name="remove2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_banner from hub
		  where hub_id = #session.hub#
		</cfquery>

   	    <cfif fileexists("#media_path#\#remove2.hub_banner#")>
		<cffile action = "delete" file = "#media_path#\#remove2.hub_banner#">
		</cfif>

</cfif>

<cfif hub_banner is not "">

	<cfquery name="getfile2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select hub_banner from hub
	  where hub_id = #session.hub#
	</cfquery>

	<cfif #getfile2.hub_banner# is not "">
	 <cfif fileexists("#media_path#\#getfile2.hub_banner#")>
	 	<cffile action = "delete" file = "#media_path#\#getfile2.hub_banner#">
	 </cfif>
	</cfif>

	<cffile action = "upload"
	 fileField = "hub_banner"
	 destination = "#media_path#"
	 nameConflict = "MakeUnique">

</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set  <cfif #hub_banner# is not "">
		   hub_banner = '#cffile.serverfile#'
		  </cfif>
		  <cfif isdefined("remove_banner_attachment")>
		   hub_banner = null
		  </cfif>
	 where hub_id = #session.hub#

	</cfquery>

    <cflocation URL="index.cfm" addtoken="no">

</cfif>