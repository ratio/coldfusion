<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

   <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from comm
	where comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	      comm_hub_id = #session.hub#
   </cfquery>

   <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from state
	order by state_name
   </cfquery>

   <cfquery name="manager" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr
	join hub_xref on hub_xref_usr_id = usr_id
	where hub_xref_hub_id = #session.hub# and
		  hub_xref_active = 1
	order by usr_last_name, usr_first_name
   </cfquery>

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

      <form action="save.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

	  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Community</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header">Community Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="comm_name" value="#edit.comm_name#" size=70 required></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea class="input_text" style="width: 600px; height: 100px;" name="comm_desc">#edit.comm_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header">Tag Line</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="comm_tag_line" value="#edit.comm_tag_line#" maxlength="200"></td></tr>

		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="comm_tags" value="#edit.comm_tags#" maxlength="200"></td></tr>

		   <tr><td></td><td class="link_small_gray">seperate tags with a comma</td></tr>

			   <tr>
				<td class="feed_sub_header" valign=top>Modules</td>
                <td valign=top>

			    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <tr><td width=35><input type="checkbox" name="comm_calendar" style="width: 22px; height: 22px;" <cfif edit.comm_calendar is 1>checked</cfif>></td>
			  	    <td class="feed_sub_header" style="font-weight: normal;">Calendar</td></tr>

			    <tr><td width=35><input type="checkbox" name="comm_documents" style="width: 22px; height: 22px;" <cfif edit.comm_documents is 1>checked</cfif>></td>
				    <td class="feed_sub_header" style="font-weight: normal;">Documents</td></tr>

			    <tr><td width=35><input type="checkbox" name="comm_opportunity" style="width: 22px; height: 22px;" <cfif edit.comm_opportunity is 1>checked</cfif>></td>
				    <td class="feed_sub_header" style="font-weight: normal;">Opportunity Board</td></tr>

			    </table>

			   </td></tr>

		   <tr><td class="feed_sub_header" valign=top>Image / Logo</td>
		       <td class="feed_sub_header" style="font-weight: normal;">

				<cfif #edit.comm_logo# is "">
				  <input type="file" name="comm_logo">
				<cfelse>
				  <img style="border-radius: 0px;" src="#media_virtual#/#edit.comm_logo#" width=100>
				  <br><br>
				  <input type="file" name="comm_logo"><br><br>
				  <input type="checkbox" name="remove_attachment">&nbsp;or, check to remove logo
				 </cfif>

		   </td></tr>

		   <tr>
		        <td class="feed_sub_header">City, State, Zip</td>
		        <td><input class="input_text" style="width: 200px;" type="text" name="comm_city" value="#edit.comm_city#" size=20 maxlength="50">&nbsp;&nbsp;

           </cfoutput>

		   <select name="comm_state" class="input_select">
  	        <option value=0>Select State
		    <cfoutput query="states">
		     <option value="#state_abbr#" <cfif #edit.comm_state# is #state_abbr#>selected</cfif>>#state_name#
		    </cfoutput>
		   </select>&nbsp;&nbsp;

           <cfoutput>

		   <input class="input_text" style="width: 100px;" type="text" name="comm_zip" value="#edit.comm_zip#" maxlength="10">

		   </td></tr>

		   </cfoutput>

	       <tr><td class="feed_sub_header">Community Manager</td>
		       <td>
				   <select name="comm_owner_id" class="input_select">
				   <cfoutput query="manager">
					<option value=#usr_id# <cfif #usr_id# is #edit.comm_owner_id#>selected</cfif>>#usr_last_name#, #usr_first_name#
				   </cfoutput>
				   </select></td></tr>

		   <cfoutput>

		   <tr><td></td><td class="link_small_gray">User must exist within BPX to be selected as a Community Manager.</td></tr>
           <tr><td height=10></td></tr>

		   <tr><td class="feed_sub_header">Community Type</td>
		   <td>

		   <select name="comm_type_id" class="input_select">
		    <option value=3 <cfif #edit.comm_type_id# is 3>selected</cfif>>Open
		    <option value=1 <cfif #edit.comm_type_id# is 1>selected</cfif>>Standard
		    <option value=2 <cfif #edit.comm_type_id# is 2>selected</cfif>>Private
		   </select>

		   </td></tr>

		   <tr><td class="feed_sub_header">Status</td>
		       <td>

		   <select name="comm_status" class="input_select">
		    <option value=0 <cfif #edit.comm_status# is 0>selected</cfif>>Not Approved
		    <option value=1 <cfif #edit.comm_status# is 1>selected</cfif>>Approved
		   </select>

		   </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>


		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
		   <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Community?\r\nAre you sure you want to delete this Community?');">&nbsp;&nbsp;

            <input type="hidden" name="old_admin_id" value=#edit.comm_owner_id#>
            <input type="hidden" name="cid" value=#cid#>
           </cfoutput>

		   </td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

