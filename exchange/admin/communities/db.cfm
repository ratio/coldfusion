<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into hub (hub_name, hub_desc, hub_created, hub_admin_id, hub_home_page_logo, hub_home_page_image, hub_home_page_header, hub_home_page_text, hub_header_bgcolor, hub_advisor_id, hub_url)
	 values ('#hub_name#','#hub_desc#',#now()#,#session.usr_id#,'#hub_home_page_logo#','#hub_home_page_image#','#hub_home_page_header#','#hub_home_page_text#','#hub_header_bgcolor#',#session.usr_id#,'#hub_url#')
	</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set hub_name = '#hub_name#',
	     hub_desc = '#hub_desc#',
	     hub_created = #now()#,
	     hub_admin_id = #session.usr_id#,
	     hub_home_page_logo = '#hub_home_page_logo#',
	     hub_home_page_image = '#hub_home_page_image#',
	     hub_home_page_header = '#hub_home_page_header#',
	     hub_home_page_text = '#hub_home_page_text#',
	     hub_header_bgcolor = '#hub_header_bgcolor#',
	     hub_advisor_id = #session.usr_id#,
	     hub_url = '#hub_url#'
	 where hub_id = #hub_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">