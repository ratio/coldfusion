<cfinclude template="/exchange/security/check.cfm">

<cfquery name="comm" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_hub_id = #session.hub#
  order by comm_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Communities</td>
	       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Community</a>&nbsp;|&nbsp;<a href="/exchange/admin/index.cfm">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">Community has been successfully added.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">Community has been successfully updated.</td></tr>
        <cfelseif u is 3>
         <tr><td class="feed_sub_header" style="color: red;">Community has been deleted.</td></tr>
        </cfif>
       </cfif>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #comm.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Communities exist.</td></tr>

        <cfelse>

			<tr>
			    <td class="feed_sub_header">Community Name</td>
			    <td class="feed_sub_header">Description</td>
				<td class="feed_sub_header" align=center>Members</td>
				<td class="feed_sub_header" align=center>Created</td>
				<td class="feed_sub_header" align=center>Updated</td>
				<td class="feed_sub_header" align=right>Status</td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfloop query="comm">

			<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select count(comm_xref_id) as total from comm_xref
			  where comm_xref_comm_id = #comm.comm_id#
			</cfquery>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

          <cfoutput>

              <td class="feed_sub_header" width=250><a href="edit.cfm?cid=#encrypt(comm.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm.comm_name#</a></td>
              <td class="feed_sub_header" width=500 style="font-weight: normal;">#comm.comm_desc#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#members.total#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#dateformat(comm.comm_created,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#dateformat(comm.comm_updated,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right>
              <cfif #comm.comm_status# is 0>
               Not Approved
              <cfelseif #comm.comm_status# is 1>
               Approved
              <cfelseif #comm.comm_status# is 2>
               Suspended
               </cfif></td>

          </cfoutput>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfloop>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

