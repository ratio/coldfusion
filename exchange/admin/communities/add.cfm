<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

   <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from state
	order by state_name
   </cfquery>

   <cfquery name="manager" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr
	join hub_xref on hub_xref_usr_id = usr_id
	where hub_xref_hub_id = #session.hub# and
		  hub_xref_active = 1
	order by usr_last_name, usr_first_name
   </cfquery>

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

      <form action="save.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

	  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Community</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header">Community Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="comm_name"  required></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea class="input_text" style="width: 600px; height: 100px;" name="comm_desc"></textarea></td></tr>

		   <tr><td class="feed_sub_header">Tag Line</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="comm_tag_line" maxlength="200"></td></tr>

		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="comm_tags" maxlength="200"></td></tr>

		   <tr><td></td><td class="link_small_gray">seperate tags with a comma</td></tr>


			   <tr>
				<td class="feed_sub_header" valign=top>Modules</td>
                <td valign=top>

			    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <tr><td width=35><input type="checkbox" name="comm_calendar" style="width: 22px; height: 22px;"></td>
			  	    <td class="feed_sub_header" style="font-weight: normal;">Calendar</td></tr>

			    <tr><td width=35><input type="checkbox" name="comm_documents" style="width: 22px; height: 22px;"></td>
				    <td class="feed_sub_header" style="font-weight: normal;">Documents</td></tr>

			    <tr><td width=35><input type="checkbox" name="comm_opportunity" style="width: 22px; height: 22px;"></td>
				    <td class="feed_sub_header" style="font-weight: normal;">Opportunity Board</td></tr>

			    </table>

			   </td></tr>


		   <tr><td class="feed_sub_header" valign=top>Image / Logo</td>
		       <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="comm_logo"></td></tr>

		   <tr>
		        <td class="feed_sub_header">City, State, Zip</td>
		        <td><input class="input_text" style="width: 200px;" type="text" name="comm_city" maxlength="50">&nbsp;&nbsp;

           </cfoutput>

		   <select name="comm_state" class="input_select">
  	        <option value=0>Select State
		    <cfoutput query="states">
		     <option value="#state_abbr#">#state_name#
		    </cfoutput>
		   </select>&nbsp;&nbsp;

           <cfoutput>

		   <input class="input_text" style="width: 100px;" type="text" name="comm_zip" maxlength="10">

		   </td></tr>

		   </cfoutput>

	       <tr><td class="feed_sub_header">Community Manager</td>
		       <td>
				   <select name="comm_owner_id" class="input_select">
				   <cfoutput query="manager">
					<option value=#usr_id#>#usr_last_name#, #usr_first_name# (#tostring(tobinary(usr_email))#)
				   </cfoutput>
				   </select></td></tr>

		   <cfoutput>

		   <tr><td></td><td class="link_small_gray">User must exist already to be selected as a Community Manager.</td></tr>
           <tr><td height=10></td></tr>

		   <tr><td class="feed_sub_header">Community Type</td>
		   <td>

		   <select name="comm_type_id" class="input_select">
		    <option value=3>Open
		    <option value=1>Standard
		    <option value=2>Private
		   </select>

		   </td></tr>

		   <tr><td class="feed_sub_header">Status</td>
		       <td>

		   <select name="comm_status" class="input_select">
		    <option value=0>Not Approved
		    <option value=1 selected>Approved
		   </select>

		   </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10>&nbsp;&nbsp;
		   <input class="button_blue_large" type="submit" name="button" value="Cancel" vspace=10>

           </cfoutput>

		   </td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

