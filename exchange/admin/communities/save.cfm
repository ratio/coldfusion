<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

<cftransaction>

	<cfif #comm_logo# is not "">
		<cffile action = "upload"
		 fileField = "comm_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="add" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into comm
	  (comm_calendar, comm_documents, comm_opportunity, comm_zip, comm_state, comm_city, comm_tag_line, comm_tags, comm_type_id, comm_owner_id, comm_created, comm_hub_id, comm_name, comm_desc, comm_logo, comm_updated, comm_status)
	  values
	  (<cfif isdefined("comm_calendar")>1<cfelse>null</cfif>, <cfif isdefined("comm_documents")>1<cfelse>null</cfif>,<cfif isdefined("comm_opportunity")>1<cfelse>null</cfif>,'#comm_zip#','#comm_state#','#comm_city#','#comm_tag_line#','#comm_tags#',#comm_type_id#, #comm_owner_id#, #now()#, #session.hub#, '#comm_name#','#comm_desc#',<cfif #comm_logo# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,#now()#,#comm_status#)
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(comm_id) as id from comm
	</cfquery>

    <cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into comm_xref
	 (comm_xref_usr_id, comm_xref_comm_id, comm_xref_active, comm_xref_usr_role, comm_xref_joined)
	 values(#comm_owner_id#, #max.id#, 1, 1,#now()#)
    </cfquery>

</cftransaction>

<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select comm_logo from comm
	  where comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cfif remove.comm_logo is not "">
	 <cfif fileexists("#media_path#\#remove.comm_logo#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.comm_logo#">
	 </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete comm
	  where comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	        comm_hub_id = #session.hub#
	</cfquery>

<cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Update">

<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_logo from comm
		  where comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

   	    <cfif fileexists("#media_path#\#remove.comm_logo#")>
			<cffile action = "delete" file = "#media_path#\#remove.comm_logo#">
		</cfif>

</cfif>

<cfif comm_logo is not "">

	<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select comm_logo from comm
	  where comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfif #getfile.comm_logo# is not "">
	 <cfif fileexists("#media_path#\#getfile.comm_logo#")>
	 	<cffile action = "delete" file = "#media_path#\#getfile.comm_logo#">
	 </cfif>
	</cfif>

	<cffile action = "upload"
	 fileField = "comm_logo"
	 destination = "#media_path#"
	 nameConflict = "MakeUnique">

</cfif>

    <cfif comm_owner_id is old_admin_id>
    <cfelse>

		<!--- Update Old Owners Access --->

		<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update comm_xref
		 set comm_xref_usr_role = 0
		 where comm_xref_comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			   comm_xref_usr_id = #old_admin_id#
		</cfquery>

		<!--- Check to see if new owner is a member --->

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select comm_xref_id from comm_xref
		 where comm_xref_usr_id = #comm_owner_id# and
			   comm_xref_comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif check.recordcount is 1>

			<!--- If user exists, update record --->

			<cfquery name="update_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update comm_xref
			 set comm_xref_usr_role = 1
			 where comm_xref_comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				   comm_xref_usr_id = #comm_owner_id#
			</cfquery>

		<cfelse>

			<!--- User does not exist, create record --->

			<cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into comm_xref
			 (comm_xref_usr_id, comm_xref_comm_id, comm_xref_active, comm_xref_usr_role, comm_xref_joined)
			 values(#comm_owner_id#, #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")#, 1, 1,#now()#)
			</cfquery>

		</cfif>

    </cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update comm
	 set comm_name = '#comm_name#',
	     comm_desc = '#comm_desc#',
	     comm_calendar = <cfif isdefined("comm_calendar")>1<cfelse>null</cfif>,
	     comm_documents = <cfif isdefined("comm_documents")>1<cfelse>null</cfif>,
	     comm_opportunity = <cfif isdefined("comm_opportunity")>1<cfelse>null</cfif>,
	     comm_tags = '#comm_tags#',
	     comm_tag_line = '#comm_tag_line#',
	     comm_type_id = #comm_type_id#,
	     comm_city = '#comm_city#',
	     comm_state = '#comm_state#',
         comm_zip = '#comm_zip#',

		  <cfif #comm_logo# is not "">
		   comm_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   comm_logo = null,
		  </cfif>

	     comm_updated = #now()#,
	     comm_status = #comm_status#,
	     comm_owner_id = #comm_owner_id#
	 where comm_id = #decrypt(cid,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       comm_hub_id = #session.hub#

	</cfquery>

<cflocation URL="index.cfm?u=2" addtoken="no">

</cfif>

