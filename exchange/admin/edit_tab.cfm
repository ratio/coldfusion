<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <script src="/include/nicedit.js" type="text/javascript"></script>
  <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <cfquery name="tab" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from tab
	   where tab_id = #tab_id#
	  </cfquery>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Tab</td>
               <td class="feed_option" align=right><a href="tabs.cfm">Return</td></tr>
           <tr><td>&nbsp;</td></tr>
        </table>

        <cfoutput>

        <form action="tab_db.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_option"><b>Tab Name</b></td></tr>
          <tr><td><input type="text" name="tab_name" value="#tab.tab_name#" style="font-family: arial; font-size: 12px; width: 200px" maxlength="30" required></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Tab Title</b></td></tr>
          <tr><td><input type="text" name="tab_title" value="#tab.tab_title#" style="font-family: arial; font-size: 12px; width: 500px" maxlength="200"></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Tab Order</b></td></tr>
          <tr><td><input type="number" name="tab_order" value="#tab.tab_order#" size=4 required></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Tab Link</b> - if content is provided the system will display this URL before displaying content below.</td></tr>
          <tr><td><input type="text" name="tab_link" style="width: 500px;" maxlength="499" value="#tab.tab_link#"></td></tr>
          <tr><td class="text_xsmall">The content of this link will appears in the tab when user clicks on it.</td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Tab Link Location</b></td></tr>
          <tr><td class="feed_option"><input type="radio" name="tab_link_location" value=1 <cfif #tab.tab_link_location# is 1 or #tab.tab_link_location# is "">checked</cfif>>&nbsp;&nbsp;Within the EXCHANGE&nbsp;&nbsp;<input type="radio" name="tab_link_location" value=0 <cfif #tab.tab_link_location# is 0>checked</cfif>>&nbsp;&nbsp;New Window</td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_header">Or, create your own content</td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Tab Short Description</b></td></tr>
          <tr><td><textarea name="tab_short_desc" style="font-family: arial; font-size: 12px; width: 785px; height: 100px;">#tab.tab_short_desc#</textarea></td></tr>
          <tr><td>&nbsp;</td></tr>
  	 	  <tr><td class="feed_option"><b>Tab Page Content</b></td></tr>
          <tr><td><textarea name="tab_content" style="font-family: arial; font-size: 12px; width: 785px; height: 1500px;">#tab.tab_content#</textarea></td></tr>
          <tr><td>
          <input class="button_blue" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
          <input class="button_blue" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Tab?\r\nAre you sure you want to delete this tab?');">
          </td></tr>

        </table>

        <input type="hidden" name="tab_id" value=#tab_id#>

        </form>

        </cfoutput>

      </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>