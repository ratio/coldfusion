<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <form action="cat_db.cfm" method="post">

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Resource Category</td>
		       <td class="feed_option"><a href="index.cfm">Return</a></td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <tr><td class="feed_option"><b>Name</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 420px;" type="text" name="resource_category_name"></td></tr>

		   <tr><td class="feed_option" valign=top><b>Description</b></td></tr>
		   <tr><td><textarea name="resource_category_desc" cols=140 rows=3></textarea></td></tr>

		   <tr><td class="feed_option"><b>Order</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 120px;" type="text" name="resource_category_order"></td></tr>

		   <tr><td><input class="button_blue" type="submit" name="button" value="Save" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

