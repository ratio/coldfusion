<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">


		<cfif #resource_file# is not "">

			<cffile action = "upload"
			 fileField = "resource_file"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into resource (resource_hub_id, resource_file,resource_cat_id, resource_name, resource_desc,resource_order,resource_url, resource_created,resource_created_usr_id)
	 values (#session.hub#, <cfif #resource_file# is not "">'#cffile.serverfile#'<cfelse>null</cfif>, #resource_cat_id#,'#resource_name#','#resource_desc#',<cfif #resource_order# is "">null<cfelse>#resource_order#</cfif>,'#resource_url#',#now()#,#session.usr_id#)
	</cfquery>

<cfelseif #button# is "Delete">

<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 delete resource
 where resource_id = #resource_id#
</cfquery>

<cfelseif #button# is "Update">

        <cfif isdefined("remove_file")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select resource_file from resource
			  where resource_id = #resource_id#
			</cfquery>

        	<cffile action = "delete" file = "#media_path#\#remove.resource_file#">

        </cfif>

		<cfif #resource_file# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select resource_file from resource
			  where resource_id = #resource_id#
			</cfquery>

			<cfif #getfile.resource_file# is not "">
			 <cffile action = "delete" file = "#media_path#\#getfile.resource_file#">
			</cfif>

			<cffile action = "upload"
			 fileField = "resource_file"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update resource
	 set resource_name = '#resource_name#',
	     resource_desc = '#resource_desc#',
	     resource_hub_id = #session.hub#,
	     resource_cat_id = #resource_cat_id#,

		  <cfif #resource_file# is not "">
		   resource_file = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_file")>
		   resource_file = null,
		  </cfif>

	     resource_url = '#resource_url#',
	     resource_order = <cfif #resource_order# is "">null<cfelse>#resource_order#</cfif>,
	     resource_created = #now()#
	 where resource_id = #resource_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">