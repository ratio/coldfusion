<cfinclude template="/exchange/security/check.cfm">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from resource_category
 where resource_category_id = #resource_category_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <form action="cat_db.cfm" method="post">

	  <div class="main_box">

	  <cfoutput query="info">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Resource Category</td>
		       <td class="feed_option"><a href="index.cfm">Return</a></td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <tr><td class="feed_option"><b>Name</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 420px;" type="text" value="#resource_category_name#" name="resource_category_name"></td></tr>
		   <tr><td class="feed_option"><b>Description</b></td></tr>
		   <tr><td><textarea name="resource_category_desc" cols=140 rows=3>#resource_category_desc#</textarea></td></tr>
		   <tr><td class="feed_option"><b>Order</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 120px;" type="text" name="resource_category_order"  value=#resource_category_order#></td></tr>
		   <tr><td><input class="button_blue" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Resource Category?\r\nAre you sure you want to delete this resource category?  By deleting this category you will also remove any resources that are associated with it.');"></td></tr>
		  </table>

		  <input type="hidden" name="resource_category_id" value=#resource_category_id#>

	   </cfoutput>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

