<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into resource_category (resource_category_hub_id, resource_category_name, resource_category_desc,resource_category_order,resource_category_created,resource_category_created_usr_id)
	 values (#session.hub#,'#resource_category_name#','#resource_category_desc#',<cfif #resource_category_order# is "">null<cfelse>#resource_category_order#</cfif>,#now()#,#session.usr_id#)
	</cfquery>

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete resource_category
	 where resource_category_id = #resource_category_id#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete resource
	 where resource_cat_id = #resource_category_id#
	</cfquery>

</cftransaction>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update resource_category
	 set resource_category_name = '#resource_category_name#',
	     resource_category_desc = '#resource_category_desc#',
	     resource_category_order = <cfif #resource_category_order# is "">null<cfelse>#resource_category_order#</cfif>
	 where resource_category_id = #resource_category_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">