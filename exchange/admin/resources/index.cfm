<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from resource_category
	  where resource_category_hub_id = #session.hub#
	  order by resource_category_order
	</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Resources</td></tr>
	   <tr><td>&nbsp;</td></tr>
	   <tr><td class="feed_option">
	   <b>
		  <a href="add.cfm">Add Resource</a>&nbsp;|&nbsp;<a href="cat_add.cfm">Add Resource Category</a>
	   </b>
	   </td></tr>
	   <tr><td>&nbsp;</td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=1 width=100%>


			<tr>
			    <td class="feed_option"><b>Order</b></td>
			    <td class="feed_option" width=300><b>Resource Name</b></td>
			    <td class="feed_option"><b>Description</b></td>
			</tr>

        <cfloop query="cats">

         <cfoutput>
         <tr><td colspan=6 class="feed_option" bgcolor="d0d0d0"><b><a href="cat_edit.cfm?resource_category_id=#resource_category_id#">#resource_category_name#</a></b></td></tr>
         </cfoutput>

		 <cfquery name="resources" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from resource
		   where resource_cat_id = #cats.resource_category_id#
		   order by resource_order
		 </cfquery>

         <cfset counter = 0>

         <cfif resources.recordcount is 0>
          <tr><td class="feed_option" colspan=3>No resources exist.</td></tr>
         <cfelse>

         <cfoutput query="resources">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_option" valign=top>#resource_order#</a></td>
              <td class="feed_option" valign=top width=15%><a href="edit.cfm?resource_id=#resource_id#">#resource_name#</a></td>
              <td class="feed_option">#resource_desc#</a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>
        </cfloop>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

