<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from resource_category
 where resource_category_hub_id = #session.hub#
 order by resource_category_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script src="/include/nicedit.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Resource</td>
		       <td class="feed_option"><a href="index.cfm">Return</a></td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <tr><td class="feed_option"><b>Title</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 520px;" type="text" name="resource_name"></td></tr>
		   <tr><td class="feed_option"><b>Description</b></td></tr>
		   <tr><td><textarea name="resource_desc" cols=140 rows=10></textarea></td></tr>
		   <tr><td class="feed_option"><b>Resource Category</b></td></tr>
		   <tr><td>
		   <select name="resource_cat_id">
		   <cfoutput query="cats">
		    <option value=#resource_category_id#>#resource_category_name#
		   </cfoutput>
		   </select></td></tr>

		   <tr><td class="feed_option"><b>Attachment / File</b></td></tr>
		   <tr><td class="feed_option"><input type="file" name="resource_file"></td></tr>
		   <tr><td class="feed_option"><b>URL</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 250px;" type="url" name="resource_url"></td></tr>
		   <tr><td class="feed_option"><b>Order</b></td></tr>
		   <tr><td><input style="font-size: 12px; width: 120px;" type="text" name="resource_order"></td></tr>
		   <tr><td><input class="button_blue" type="submit" name="button" value="Save" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

