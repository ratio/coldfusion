<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into support (support_hub_id, support_cat_id, support_name, support_desc,support_order, support_created,support_created_usr_id)
	 values (#session.hub#, #support_cat_id#,'#support_name#','#support_desc#',<cfif #support_order# is "">null<cfelse>#support_order#</cfif>,#now()#,#session.usr_id#)
	</cfquery>

<cfelseif #button# is "Delete">

<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 delete support
 where support_id = #support_id#
</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update support
	 set support_name = '#support_name#',
	     support_desc = '#support_desc#',
	     support_cat_id = #support_cat_id#,
	     support_order = <cfif #support_order# is "">null<cfelse>#support_order#</cfif>,
	     support_created = #now()#
	 where support_id = #support_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">