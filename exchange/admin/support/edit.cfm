<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.hub")>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from support_cat
	 where support_cat_hub_id = #session.hub#
	 order by support_cat_order
	</cfquery>

<cfelse>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from support_cat
	 where support_cat_hub_id is null
	 order by support_cat_order
	</cfquery>

</cfif>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from support
 where support_id = #support_id#
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

	  	   <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Support Topic</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header">Topic Name</td>
		       <td><input class="input_text" style="width: 500px;" type="text" name="support_name" value="#edit.support_name#"></td></tr>
		   <tr><td valign=top class="feed_sub_header">Description</td>
		       <td><textarea class="input_textarea" style="width: 600px; height: 200px;" name="support_desc">#edit.support_desc#</textarea></td></tr>
		   <tr><td class="feed_sub_header">Category</td>

		   </cfoutput>

		   <td>
		   <select name="support_cat_id" class="input_select">
		   <cfoutput query="cats">
		    <option value=#support_cat_id# <cfif #support_cat_id# is #edit.support_cat_id#>selected</cfif>>#support_cat_name#
		   </cfoutput>
		   </select></td></tr>

           <cfoutput>

		   <tr><td class="feed_sub_header">Display Order</td>
		       <td><input class="input_text" style="width: 100px;" required type="number" value="#edit.support_order#" name="support_order"></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Topic?\r\nAre you sure you want to delete this Customer Support topic?');"></td></tr>

		   <input type="hidden" name="support_id" value=#support_id#>
		  </table>

		   </cfoutput>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

