<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.hub")>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from support_cat
	  where support_cat_hub_id = #session.hub#
	  order by support_cat_order
	</cfquery>

<cfelse>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from support_cat
	  where support_cat_hub_id is null
	  order by support_cat_order
	</cfquery>

</cfif>

<cfquery name="library" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_library_id from hub
 where hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Help & Support</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/index.cfm">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <cfif isdefined("u") and u is 100>
	   <tr><td class="feed_sub_header" style="color: green;">Support Library has been successfully changed.</td></tr>
	  </cfif>

       <form action="change_library.cfm" method="post">
	   <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Support Library</b></a>
	       &nbsp;&nbsp;
	       <select name="hub_library_id" class="input_select">
	        <option value=0>Local
	        <option value=1 <cfif #library.hub_library_id# is 1>selected</cfif>>Exchange Knowledge Center
	        <option value=2 <cfif #library.hub_library_id# is 2>selected</cfif>>Both
	       </select>

	       <input type="submit" name="button" class="button_blue" value="Submit">
	   </form>
	       </td></tr>

	   <tr><td class="feed_sub_header" style="font-weight: normal;">This setting allows you to choose which library will be displayed to the user when clicking on Help & Support.</td></tr>

	   <tr><td><hr></td></tr>

	   <tr><td class="feed_sub_header"><a href="cat_add.cfm">Add Local Support Category</a>&nbsp;|&nbsp;<a href="add.cfm">Add Local Support Topic</a></td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif cats.recordcount is 0>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No Customer Support local categories exist.  Please add a Category before adding Customer Support Topics.</td></tr>

        <cfelse>

			<tr>
			    <td class="feed_sub_header" width=100>Order</td>
			    <td class="feed_sub_header" width=400>Title</td>
			    <td class="feed_sub_header">Description</td>
			</tr>

			<tr><td height=10></td></tr>

        <cfloop query="cats">

         <cfoutput>
         <tr><td colspan=6 class="feed_sub_header" bgcolor="d0d0d0"><a href="cat_edit.cfm?support_cat_id=#support_cat_id#"><b>#support_cat_name#</b></a></td></tr>
         </cfoutput>

		 <cfquery name="support" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from support
		   where support_cat_id = #cats.support_cat_id#
		   order by support_order
		 </cfquery>

         <cfset counter = 0>

         <cfif support.recordcount is 0>
          <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;">No Customer Support Topics have been created for this Category.</td></tr>
         <cfelse>

         <cfoutput query="support">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" width=60 valign=top><a href="edit.cfm?support_id=#support_id#">#support_order#</a></td>
              <td class="feed_sub_header" valign=top width=300><a href="edit.cfm?support_id=#support_id#">#support_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;">#support_desc#</a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>

         <tr><td>&nbsp;</td></tr>

        </cfloop>

        </cfif>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

