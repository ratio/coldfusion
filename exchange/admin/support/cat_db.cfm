<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into support_cat (support_cat_hub_id, support_cat_name, support_cat_desc,support_cat_order,support_cat_created,support_cat_created_usr_id)
	 values (#session.hub#,'#support_cat_name#','#support_cat_desc#',<cfif #support_cat_order# is "">null<cfelse>#support_cat_order#</cfif>,#now()#,#session.usr_id#)
	</cfquery>

<cfelseif #button# is "Cancel">

  <cflocation URL="index.cfm" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete support_cat
	 where support_cat_id = #support_cat_id#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete support
	 where support_cat_id = #support_cat_id#
	</cfquery>

</cftransaction>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update support_cat
	 set support_cat_name = '#support_cat_name#',
	     support_cat_desc = '#support_cat_desc#',
	     support_cat_order = <cfif #support_cat_order# is "">null<cfelse>#support_cat_order#</cfif>
	 where support_cat_id = #support_cat_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">