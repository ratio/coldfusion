<cfinclude template="/exchange/security/check.cfm">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from featured
 where featured_id = #featured_id#
</cfquery>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select company_name from company
 where company_id = #info.featured_company_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <form action="db.cfm" method="post">

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header" colspan=2>Edit Featured Company</td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <cfoutput>

			   <tr><td class="feed_option" width=150><b>Company Name</b></td>
			       <td class="feed_option">#comp.company_name#</td></tr>

			   <tr><td class="feed_option"><b>Order</b></td>
			       <td><input type="number" name="featured_order" step=".1" value="#info.featured_order#" required></td></tr>

			   <tr><td class="feed_option"><b>Featured Title</b></td>
			       <td><input type="text" name="featured_title" size=80 value="#info.featured_title#"></td></tr>

			   <tr><td class="feed_option" valign=top><b>Featured Text</b></td>
			       <td valign=top><textarea name="featured_comments" rows=5 cols=100>#info.featured_comments#</textarea></td></tr>

			   <tr><td class="feed_option"><b>Active</b></td>
			       <td><select name="featured_active">
			        <option value=0 <cfif #info.featured_active# is "" or #info.featured_active# is 0>selected</cfif>>No, not displayed on the Featured Page
			        <option value=1 <cfif #info.featured_active# is 1>selected</cfif>>Yes, displayed on the Featured Page
			        </select></td></tr>



			   <tr><td height=20></td></tr>
			   <tr><td colspan=2><input class="button_blue" type="submit" name="button" value="Update">&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Remove" vspace=10 onclick="return confirm('Remove Featured Company?\r\nAre you sure you want to remove this company from Featured Page if active?');"></td></tr>
		       <input type="hidden" name="featured_id" value=#featured_id#>
		   </cfoutput>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

