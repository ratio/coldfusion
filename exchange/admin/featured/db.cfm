<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update featured
	 set featured_order = #featured_order#,
	     featured_title = '#featured_title#',
	     featured_hub_id = <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	     featured_comments = '#featured_comments#',
	     featured_updated = #now()#,
	     featured_active = #featured_active#
	 where featured_id = #featured_id#
	</cfquery>

<cfelseif #button# is "Remove">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      delete featured
      where featured_id = #featured_id#
    </cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">