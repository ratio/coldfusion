<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from featured
	  join company on company_id = featured_company_id

	  <cfif isdefined("session.hub")>
	    where featured_hub_id = #session.hub#
	  <cfelse>
	    where featured_hub_id is null
	  </cfif>

	  order by featured_order
	</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Featured Companies</td></tr>
	   <tr><td>&nbsp;</td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #info.recordcount# is 0>

        <tr><td class="feed_option"><b>No featured companies have been added.</b></td></tr>

        <cfelse>

			<tr><td class="feed_option" width=50><b>Order</b></td>
				<td class="feed_option"><b>Company</b></td>
				<td class="feed_option"><b>Featured Content</b></td>
				<td class="feed_option" align=right><b>Active</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="info">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_option" valign=top>#featured_order#</td>
              <td class="feed_option" valign=top><a href="edit.cfm?featured_id=#featured_id#"><b>#company_name#</b></a></td>
              <td class="feed_option" valign=top>#featured_comments#</td>
              <td class="feed_option" valign=top align=right><cfif featured_active is 0 or featured_active is "">No<cfelse><b>Yes</b></cfif></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

