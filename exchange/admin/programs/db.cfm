<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into program
	 (program_hub_id,
	  program_location,
	  program_cat_id,
	  program_name,
	  program_desc,
	  program_url,
	  program_keywords,
	  program_date,
	  program_time,
	  program_updated,
	  program_usr_id
	  )
	 values
	 (#session.hub#,
	 '#program_location#',
	  #program_cat_id#,
	 '#program_name#',
	 '#program_desc#',
	 '#program_url#',
	 '#program_keywords#',
	 '#program_date#',
	 '#program_time#',
	  #now()#,
	  #session.usr_id#
	  )
	</cfquery>

<cflocation URL="index.cfm?u=4" addtoken="no">

<cfelseif #button# is "Delete">

<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 delete program
 where program_id = #program_id# and
       program_hub_id = #session.hub#
</cfquery>

<cflocation URL="index.cfm?u=6" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update program
	 set program_name = '#program_name#',
	     program_location = '#program_location#',
	     program_desc = '#program_desc#',
	     program_keywords = '#program_keywords#',
	     program_url = '#program_url#',
	     program_date = '#program_date#',
	     program_time = '#program_time#',
	     program_cat_id = #program_cat_id#,
	     program_updated = #now()#,
	     program_usr_id = #session.usr_id#
	 where program_id = #program_id# and
	       program_hub_id = #session.hub#
	</cfquery>

<cflocation URL="index.cfm?u=5" addtoken="no">

</cfif>

