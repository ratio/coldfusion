<cfquery name="rights" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header"><cfif session.network is 0>Exchange<cfelse>Network</cfif> Settings</td>
      <td class="feed_sub_header" align=right></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td height=20></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top width=350>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_header">User Management</td></tr>
			<tr><td height=10></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/users/">Manage Users</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/user_type/">User Types</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/profiles/">Role Management</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/certs/">User Certifications</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/logs/">Usage Log</a></td></tr>

			<tr><td height=20></td></tr>

			<tr><td class="feed_header">Alignments</td></tr>
			<tr><td height=10></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/markets/">Markets</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/sectors/">Sectors</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/capabilities/">Capabilities</a></td></tr>

		</table>

       </td><td valign=top width=350>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_header">Company Management</td></tr>
			<tr><td height=10></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/comp/">Companies</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/fields/">Custom Fields</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/partner_tier/">Company Tiers</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/partner_program/">Company Programs</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/agreements/">Company Agreements</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/ratings/">Comments Ratings</a></td></tr>

			<tr><td height=20></td></tr>

			<tr><td class="feed_header">Module Management</td></tr>
			<tr><td height=10></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/portfolios/">Portfolios</a></td></tr>

		</table>

       </td><td valign=top>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_header"><cfif #session.network# is 0>Exchange<cfelse>Network</cfif> Management</td></tr>
			<tr><td height=10></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/profile.cfm"><cfif session.network is 0>Exchange<cfelse>Network</cfif> Profile</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/license/">License Management</a><img src="/images/new.png" height=22 hspace=10 align=middle></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/apps/apps.cfm">App Library</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/codes/">Invitation Codes</a><img src="/images/new.png" height=22 hspace=10 align=middle></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/banner/">Banner</a></td></tr>

			<cfif session.network is 0>
				<cfif rights.hub_restrict_child_hub is 1>
					<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
						<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
						<a href="/exchange/admin/networks/">Networks</a></td></tr>
				</cfif>
			</cfif>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/communities/">Communities</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/support/">Help & Support</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/restrict/">Restrict IP Addresses</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/feeds/">Manage Posts</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/pulse/">Newswire Categories</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/updates/">News & Updates</a></td></tr>

			<tr><td class="feed_sub_header" style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
			<img src="/images/icon_bluebox.png" height=10 width=10 style="margin-right: 15px;">
			<a href="/exchange/admin/welcome/">Welcome Message</a></td></tr>


		</table>

       </td>
    </tr>
</table>



