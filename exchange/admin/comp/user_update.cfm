<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into usr_comp
	 (
	 usr_comp_usr_id,
	 usr_comp_company_id,
	 usr_comp_hub_id,
	 usr_comp_access_rights,
	 usr_comp_updated
	 )
	 values
	 (
     #decrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
     #company_id#,
     #session.hub#,
     #usr_comp_access_rights#,
     #now()#
	 )
	</cfquery>

	<cflocation URL="detail.cfm?id=#company_id#&u=41" addtoken="no">

<cfelseif button is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update usr_comp
      set usr_comp_access_rights = #usr_comp_access_rights#,
          usr_comp_updated = #now()#
      where usr_comp_usr_id = #decrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
            usr_comp_company_id = #company_id#
    </cfquery>

	<cflocation URL="detail.cfm?id=#company_id#&u=42" addtoken="no">

<cfelseif button is "Remove">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete usr_comp
      where usr_comp_usr_id = #decrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
            usr_comp_company_id = #company_id#
	</cfquery>

	<cflocation URL="detail.cfm?id=#company_id#&u=43" addtoken="no">

</cfif>



