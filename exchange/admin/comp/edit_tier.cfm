<cfinclude template="/exchange/security/check.cfm">

<cfquery name="tiers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from partner_tier
  where partner_tier_hub_id = #session.hub#
  order by partner_tier_order
</cfquery>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select company_extend_tier_id from company_extend
  where company_extend_hub_id = #session.hub# and
        company_extend_company_id = #id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Company Tier</td>
			   <td class="feed_sub_header" align=right><a href="detail.cfm?id=#id#">Return</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>


        <form action="edit_tier_db.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

			<tr><td class="feed_sub_header" width=100>Select Tier</td>
			    <td>

			    <select name="company_extend_tier_id" class="input_select">
			    <option value=0>Select Tier
			    <cfoutput query="tiers">
			     <option value=#partner_tier_id# <cfif partner_tier_id is #edit.company_extend_tier_id#>selected</cfif>>#partner_tier_name#
			    </cfoutput>
			    </select>

			    &nbsp;&nbsp;<input type="submit" name="button" value="Update" class="button_blue_large"></td>
			</tr>

        </table>

        <cfoutput>

        <input type="hidden" name="id" value="#id#">

        </form>

        </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

