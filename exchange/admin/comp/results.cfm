<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

<cfset session.check = 0>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_name like '%#session.discover_name#%'

       <cfif session.discover_website is not "">
       	or company_website like '%#session.discover_website#%'
       </cfif>

	<cfif #sv# is 1>
	 order by company_name DESC
	<cfelseif #sv# is 10>
	 order by company_name ASC
	<cfelseif #sv# is 2>
	 order by company_city ASC
	<cfelseif #sv# is 20>
	 order by company_city DESC
	<cfelseif #sv# is 3>
	 order by company_state ASC
	<cfelseif #sv# is 30>
	 order by company_state DESC
	<cfelseif #sv# is 4>
	 order by company_zip ASC
	<cfelseif #sv# is 40>
	 order by company_zip DESC
	<cfelseif #sv# is 6>
	 order by company_duns ASC
	<cfelseif #sv# is 60>
	 order by company_duns DESC
	<cfelseif #sv# is 7>
	 order by total DESC
	<cfelseif #sv# is 70>
	 order by total ASC
	<cfelseif #sv# is 8>
	 order by total DESC
	<cfelseif #sv# is 80>
	 order by total ASC
	</cfif>

</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Add New Company - Search Results</td>
		   <td class="feed_sub_header" align=right><a href="discover.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

        <cfif companies.recordcount is 0>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Companies were found that match your search criteria.</td></tr>
        <tr><td height=10></td></tr>

        <form action="redirect.cfm" method="post">
		<tr><td><input class="button_blue_large" type="submit" name="button" value="Continue >>" vspace=10></td></tr>
        </form>

        <cfelse>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">The following Companies were found that may be a match for your search criteria.</td></tr>
        <tr><td height=10></td></tr>

        <tr><td>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr>
			<td width=80></td>
			<td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header">Website</td>
			<td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zipcode</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
		</tr>

        <tr><td height=10></td></tr>

                <cfset count = 1>
				<cfoutput query="companies">

				<tr>

					<td class="feed_option">

                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40>
					</cfif>

					</td>
					<td class="feed_sub_header" width=500><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option" width=100><cfif companies.company_duns is "">Unknown<cfelse>#ucase(companies.company_duns)#</cfif></a></td>
					<td class="feed_option"><a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer"><cfif len(companies.company_website) GT 40>#ucase(left(companies.company_website,40))#...<cfelse>#ucase(companies.company_website)#</cfif></a></td>
					<td class="feed_option" width=100>#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
					<td class="feed_option" align=right width=100>#companies.company_zip#</a></td>
				</tr>

				   <tr><td colspan=8><hr></td></tr>

				</cfoutput>
        </table>

        </td></tr>

        </table>

        <form action="redirect.cfm" method="post">

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
	        <tr><td height=10></td></tr>
	        <tr><td width=35><input type="checkbox" name="certify" style="width: 22px; height: 22px;" required></td>
	            <td class="feed_sub_header" style="font-weight: normal;">I certify that none of the Companies above match the new Company I am adding.</td></tr>

	        <tr><td></td><td class="feed_sub_header" style="font-weight: normal;">Note - If the Company does exist you can add the Company by viewing their profile and adding them to this Exchange.</td></tr>
	        <tr><td height=10></td></tr>
	        <tr><td colspan=2><hr></td></tr>
	        <tr><td height=10></td></tr>

			<tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Continue >>" vspace=10></td></tr>
        </table>

        </form>

        </cfif>

        </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

