<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_contact
  where company_contact_id = #company_contact_id# and
        company_contact_company_id = #id# and
        company_contact_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Company Contact</td>
			   <td class="feed_sub_header" align=right>
			   <a href="detail.cfm?id=#id#">Return</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <cfoutput>

        <form action="contact_save.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" width=100>Name</td>
			    <td><input type="text" class="input_text" name="company_contact_name" value="#edit.company_contact_name#" style="width: 400px;" maxlength="100"></td>
			</tr>

			<tr><td class="feed_sub_header" width=100>Title</td>
			    <td><input type="text" class="input_text" name="company_contact_title" value="#edit.company_contact_title#" style="width: 400px;" maxlength="100"></td>
			</tr>

			<tr><td class="feed_sub_header" width=100>Email</td>
			    <td><input type="email" class="input_text" name="company_contact_email" value="#edit.company_contact_email#" style="width: 400px;" maxlength="200"></td>
			</tr>

			<tr><td class="feed_sub_header" width=100>Phone</td>
			    <td><input type="text" class="input_text" name="company_contact_phone" value="#edit.company_contact_phone#" style="width: 200px;" maxlength="30"></td>
			</tr>

			<tr><td class="feed_sub_header" width=100>Cell</td>
			    <td><input type="text" class="input_text" name="company_contact_cell" value="#edit.company_contact_cell#" style="width: 200px;" maxlength="30"></td>
			</tr>

			<tr><td height=10></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td height=10></td></tr>

			<tr><td></td>
			    <td>
			    <input type="submit" name="button" value="Update" class="button_blue_large">
    	        &nbsp;&nbsp;
    	        <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Company Contact?\r\nAre you sure you want to delete this Company Contact?');">


			    </td>
			</tr>

        </table>

        <input type="hidden" name="id" value="#id#">
        <input type="hidden" name="company_contact_id" value=#company_contact_id#>

        </form>

        </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

