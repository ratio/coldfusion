<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add New Company</td>
			   <td class="feed_sub_header" align=right>
			   <a href="index.cfm">Settings</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

      <form action="set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

            The first step in adding a new Company is to check whether it already exists in the Exchange Marketplace.  Please
            enter the Company Name and/or Website below.


        </td></tr>
        <tr><td height=10></td></tr>

		<tr><td class="feed_sub_header" width=150>Company Name</td>
			<td><input class="input_text" style="width: 320px;" type="text" name="company_name" maxlength="100" required></td></tr>
		<tr><td class="feed_sub_header">Website</td>
			<td><input class="input_text" style="width: 320px;" type="text" name="company_website" placeholder="www.company.com"></td></tr>

            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>
			<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Next >>" vspace=10></td></tr>

            </form>

          </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

