<cfinclude template="/exchange/security/check.cfm">

<cfif session.edit_comp_company_id is 0>
 <cflocation URL="details.cfm" addtoken="no">
</cfif>

<cfquery name="edit" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.edit_comp_company_id#
</cfquery>

<cfquery name="size" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from size
</cfquery>

<cfquery name="entity" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from entity
</cfquery>

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Edit Company</td>
		   <td class="feed_sub_header" align=right><a href="detail.cfm?id=#session.edit_comp_company_id#">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td>

      <form action="db.cfm" enctype="multipart/form-data" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

			<cfoutput>
			<tr><td class="feed_sub_header" width=20%>Company Name</td>
				<td><input class="input_text" style="width: 320px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_name" maxlength="100" value="#edit.company_name#" required></td></tr>
			<tr><td class="feed_sub_header">Address (Line 1)</td>
				<td><input class="input_text" style="width: 320px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_address_l1" value="#edit.company_address_l1#"></td></tr>
			<tr><td class="feed_sub_header">Address (Line 2)</td>
				<td><input class="input_text" style="width: 320px;" maxlength="100" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" value="#edit.company_address_l2#" name="company_address_l2"></td></tr>
			<tr><td class="feed_sub_header">City/State/Zip</td>
				<td><input class="input_text" style="width: 200px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_city" value="#edit.company_city#">&nbsp;&nbsp;
            </cfoutput>
				<select name="company_state" class="input_select">
				<option value=0>Select State
				<cfoutput query="state">
				 <option value="#state_abbr#" <cfif #edit.company_state# is #state_abbr#>selected</cfif>>#state_name#
				</cfoutput>
				</select>

		    <cfoutput>

				<input class="input_text" style="width: 150px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_zip" value="#edit.company_zip#" maxlength="25">

				</td></tr>

			<tr><td class="feed_sub_header">Website</td>
			    <td><input name="company_website" maxlength="200"class="input_text" style="width: 320px;" value="#edit.company_website#" type="url" vspace=5></td></tr>
			<tr><td class="feed_sub_header">Domain</td>
			    <td><input name="company_domain" maxlength="200"class="input_text" style="width: 320px;" type="text" value="#edit.company_domain#" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" placeholder="company.com" vspace=5></td></tr>
			<tr><td class="feed_sub_header">POC First Name</td><td><input class="input_text" style="width: 200px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_poc_first_name" value="#edit.company_poc_first_name#" maxlength="100"></td></tr>
			<tr><td class="feed_sub_header">Last Name</td><td><input class="input_text" style="width: 200px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_poc_last_name" value="#edit.company_poc_last_name#" maxlength="100"></td></tr>
			<tr><td class="feed_sub_header">Title</td><td><input class="input_text" style="width: 200px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_poc_title" value="#edit.company_poc_title#" maxlength="100"></td></tr>
			<tr><td class="feed_sub_header">Email</td><td><input class="input_text" style="width: 200px;" type="email" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_poc_email" value="#edit.company_poc_email#" maxlength="100"></td></tr>
			<tr><td class="feed_sub_header">Phone</td><td><input class="input_text" style="width: 200px;" type="text" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_poc_phone" value="#edit.company_poc_phone#" maxlength="100"></td></tr>

                <tr><td class="feed_sub_header" valign=top>Company Logo</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.company_logo# is "">
					  <input type="file" name="company_logo" id="image" onchange="validate_img()">
					<cfelse>
					  <img src="#media_virtual#/#edit.company_logo#" width=150><br><br>
					  <input type="file" name="company_logo" id="image" onchange="validate_img()"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo
					 </cfif>

					 </td></tr>

            <tr><td class="feed_sub_header">Year Founded</td><td><input class="input_text" style="width: 100px;" type="text" maxlength="4" name="company_founded" value="#edit.company_founded#"></td></tr>
            <tr><td class="feed_sub_header">DUNS Number</td><td><input class="input_text" style="width: 100px;" maxlength="12" type="text" name="company_duns" value="#edit.company_duns#"></td></tr>
            <tr><td class="feed_sub_header">Employees</td><td><input class="input_text" style="width: 100px;" maxlength="12" type="number" name="company_employees" value="#edit.company_employees#"></td></tr>
 		    </cfoutput>
 		    <tr><td class="feed_sub_header">Company Size</td>
 		        <td><select name="company_size_id" class="input_select">

					<option value=0>Select
					<cfoutput query="size">
					 <option value=#size_id# <cfif #edit.company_size_id# is #size_id#>selected</cfif>>#size_name#
					</cfoutput>

				</select>
				</td></tr>

 		    <tr><td class="feed_sub_header">Business Entity</td>
			    <td><select name="company_entity_id" class="input_select">

					<option value=0>Select
					<cfoutput query="entity">
					 <option value=#entity_id# <cfif #edit.company_entity_id# is #entity_id#>selected</cfif>>#entity_name#
					</cfoutput>

				</select>
				</td></tr>

            <cfoutput>

            <tr><td class="feed_sub_header">Revenue (2018)</td><td><input class="input_text" style="width: 200px;" type="number" name="company_fy18_revenue" value=#edit.company_fy18_revenue#></td></tr>
            <tr><td class="feed_sub_header">Revenue (2017)</td><td><input class="input_text" style="width: 200px;" type="number" name="company_fy17_revenue" value=#edit.company_fy17_revenue#></td></tr>
            <tr><td class="feed_sub_header">Revenue (2016) </td><td><input class="input_text" style="width: 200px;" type="number" name="company_fy16_revenue" value=#edit.company_fy16_revenue#></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Company Tagline</td></tr>
			<tr><td colspan=2><input type="text" name="company_tagline" class="input_text" value="#edit.company_tagline#" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" style="width: 1000px;"></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Company Description</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 150px;" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_about">#edit.company_about#</textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Long Description</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 150px;" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_long_desc">#edit.company_long_desc#</textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Summary of Products & Services</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 100px;" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_product_summary">#edit.company_product_summary#</textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Company History</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 100px;" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_history">#edit.company_history#</textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Leadership Team</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 100px;" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" name="company_leadership">#edit.company_leadership#</textarea></td></tr>
			<tr><td class="feed_sub_header" colspan=2>Company Keywords</td></tr>
			<tr><td colspan=2><input name="company_keywords" maxlength="300" class="input_text" style="width: 800px;" value="#edit.company_keywords#" onkeypress="isAlphaNum(event);" onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');" type="text"></td></tr>
            <tr><td height=10></td></tr>
            </table>

            </cfoutput>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>
			<tr><td colspan=2>

			<input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>
			&nbsp;&nbsp;
			<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Company?\r\nAre you sure you want to delete this Company?');">

			</td></tr>

            </form>

          </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

