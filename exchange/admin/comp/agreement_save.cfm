<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Agreement">

    <!---
	<cfif #company_agreement_filename# is not "">
		<cffile action = "upload"
		 fileField = "company_agreement_filename"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif> --->

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert company_agreement
	  (
	   company_agreement_agreement_id,
	   company_agreement_filename,
	   company_agreement_hub_id,
	   company_agreement_company_id,
	   company_agreement_start_date,
	   company_agreement_end_date,
	   company_agreement_comments,
	   company_agreement_updated,
	   company_agreement_updated_by_usr_id
	  )
	  values
	  (
	   #company_agreement_agreement_id#,
	   <!---
	   <cfif #company_agreement_filename# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	   --->
	   null,
	   #session.hub#,
	   #id#,
	   <cfif #company_agreement_start_date# is "">null<cfelse>'#company_agreement_start_date#'</cfif>,
	   <cfif #company_agreement_end_date# is "">null<cfelse>'#company_agreement_end_date#'</cfif>,
	  '#company_agreement_comments#',
	   #now()#,
	   #session.usr_id#
	  )
	</cfquery>

	<cflocation URL="detail.cfm?id=#id#&u=31" addtoken="no">

<cfelseif #button# is "Delete">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select company_agreement_filename from company_agreement
		  where company_agreement_id = #company_agreement_id# and
		        company_agreement_company_id = #id# and
		        company_agreement_hub_id = #session.hub#
		</cfquery>

		<cfif remove.company_agreement_filename is not "">
         <cfif fileexists("#media_path#\#remove.company_agreement_filename#")>
			 <cffile action = "delete" file = "#media_path#\#remove.company_agreement_filename#">
         </cfif>
		</cfif>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete company_agreement
		  where company_agreement_id = #company_agreement_id# and
		        company_agreement_company_id = #id# and
		        company_agreement_hub_id = #session.hub#
		</cfquery>

	</cftransaction>

<cflocation URL="detail.cfm?id=#id#&u=33" addtoken="no">

<cfelseif #button# is "Save">

    <!---

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select company_agreement_filename from company_agreement
		  where company_agreement_id = #company_agreement_id# and
		        company_agreement_company_id = #id# and
		        company_agreement_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.company_agreement_filename#")>
			<cffile action = "delete" file = "#media_path#\#remove.company_agreement_filename#">
		</cfif>

	</cfif>

	<cfif #company_agreement_filename# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select company_agreement_filename from company_agreement
		  where company_agreement_id = #company_agreement_id# and
		        company_agreement_company_id = #id# and
		        company_agreement_hub_id = #session.hub#
		</cfquery>

		<cfif #getfile.company_agreement_filename# is not "">
         <cfif fileexists("#media_path#\#getfile.company_agreement_filename#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.company_agreement_filename#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "company_agreement_filename"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif> --->

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update company_agreement
	  set company_agreement_comments = '#company_agreement_comments#',

          <!---
		  <cfif #company_agreement_filename# is not "">
		   company_agreement_filename = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   company_agreement_filename = null,
		  </cfif> --->

	      company_agreement_start_date = <cfif #company_agreement_start_date# is "">null<cfelse>'#company_agreement_start_date#'</cfif>,
	      company_agreement_end_date = <cfif #company_agreement_end_date# is "">null<cfelse>'#company_agreement_end_date#'</cfif>,
	      company_agreement_updated = #now()#
		  where company_agreement_id = #company_agreement_id# and
		        company_agreement_company_id = #id# and
		        company_agreement_hub_id = #session.hub#
	</cfquery>

</cfif>

<cflocation URL="detail.cfm?id=#id#&u=32" addtoken="no">