<cfinclude template="/exchange/security/check.cfm">

<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_program
 where partner_program_hub_id = #session.hub#
       order by partner_program_order
</cfquery>

<cfquery name="selected" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select partner_program_xref_program_id from partner_program_xref
 where partner_program_xref_partner_id = #id# and
       partner_program_xref_hub_id = #session.hub#
</cfquery>

<cfif selected.recordcount is 0>
 <cfset select_list = 0>
<cfelse>
 <cfset select_list = valuelist(selected.partner_program_xref_program_id)>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <form action="edit_program_db.cfm" method="post">

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Company Programs</td>
		       <td class="feed_sub_header" align=right><a href="detail.cfm?id=<cfoutput>#id#</cfoutput>">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
               <td width=75 class="feed_sub_header">Select</td>
               <td class="feed_sub_header">Program Name</td>
            </tr>
            <tr><td height=5></td></tr>

           <cfoutput query="programs">

            <tr>
               <td width=75>&nbsp;&nbsp;<input type="checkbox" name="partner_program_id" value=#partner_program_id# style="width: 22px; height: 22px;" <cfif listfind(select_list,partner_program_id)>checked</cfif>></td>
               <td class="feed_sub_header" style="font-weight: normal;">#partner_program_name#</td>
            </tr>

           </cfoutput>

		  <cfoutput>

		   <tr><td colspan=2><hr></td></tr>

		   <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>

		   <input type="hidden" name="id" value=#id#>

		   </cfoutput>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

