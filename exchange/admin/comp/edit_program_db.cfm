<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete partner_program_xref
	 where partner_program_xref_hub_id = #session.hub# and
	       partner_program_xref_partner_id = #id#
	</cfquery>

	<cfif isdefined("partner_program_id")>

		<cfloop index="p" list=#partner_program_id#>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into partner_program_xref
			 (
			  partner_program_xref_partner_id,
			  partner_program_xref_program_id,
			  partner_program_xref_hub_id
			  )
			  values
			  (
			  #id#,
			  #p#,
			  #session.hub#
			  )
			</cfquery>

		</cfloop>

	</cfif>

</cftransaction>

<cflocation URL="detail.cfm?u=9&id=#id#" addtoken="no">