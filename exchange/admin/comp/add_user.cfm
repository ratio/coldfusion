<cfinclude template="/exchange/security/check.cfm">

<cfquery name="ulist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select usr_comp_usr_id from usr_comp
  where usr_comp_company_id = #id#
</cfquery>

<cfif ulist.recordcount is 0>
 <cfset usr_list = 0>
<cfelse>
 <cfset usr_list = valuelist(ulist.usr_comp_usr_id)>
</cfif>


<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  join hub_xref on hub_xref_usr_id = usr_id
  where hub_xref_hub_id = #session.hub# and
        hub_xref_active = 1 and
        hub_xref_usr_id not in (#usr_list#)
  order by usr_last_name, usr_first_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Company User</td>
			   <td class="feed_sub_header" align=right>
			   <a href="detail.cfm?id=#id#">Return</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <form action="user_update.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" width=100>User</td>
			    <td class="feed_sub_header" style="font-weight: normal;">

			    <select name="usr_id" class="input_select">
			     <cfoutput query="users">
			      <option value=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#usr_last_name#, #usr_first_name# (#tostring(tobinary(usr_email))#)
			     </cfoutput>
			    </select>

			</tr>

			<tr><td class="feed_sub_header" width=100>Role</td>
			    <td><select name="usr_comp_access_rights" class="input_select">
			        <option value=0>Regular
			        <option value=1>Administrator
			        </select>

			    </td>
			</tr>

			<tr><td height=5></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td height=10></td></tr>

			<tr><td></td>
			    <td><input type="submit" name="button" value="Add" class="button_blue_large"></td>
			</tr>

			<cfoutput>
			 <input type="hidden" name="company_id" value=#id#>
			</cfoutput>

        </table>

       </form>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

