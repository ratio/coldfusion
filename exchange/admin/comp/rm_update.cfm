<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into company_rm
	 (
	 company_rm_title,
	 company_rm_comments,
	 company_rm_company_id,
	 company_rm_usr_id,
	 company_rm_hub_id,
	 company_rm_updated,
	 company_rm_order
	 )
	 values
	 (
	 '#company_rm_title#',
	 '#company_rm_comments#',
	  #company_rm_company_id#,
	  #company_rm_usr_id#,
	  #session.hub#,
	  #now()#,
	  #company_rm_order#
	 )
	</cfquery>

	<cflocation URL="detail.cfm?id=#company_rm_company_id#&u=4" addtoken="no">

<cfelseif button is "Save">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update company_rm
      set company_rm_title = '#company_rm_title#',
          company_rm_usr_id = #company_rm_usr_id#,
          company_rm_updated = #now()#,
          company_rm_order = #company_rm_order#,
          company_rm_comments = '#company_rm_comments#'
      where company_rm_id = #company_rm_id# and
            company_rm_company_id = #company_rm_company_id# and
            company_rm_hub_id = #session.hub#
    </cfquery>

	<cflocation URL="detail.cfm?id=#company_rm_company_id#&u=5" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete company_rm
      where company_rm_id = #company_rm_id# and
            company_rm_company_id = #company_rm_company_id# and
            company_rm_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="detail.cfm?id=#company_rm_company_id#&u=6" addtoken="no">

</cfif>



