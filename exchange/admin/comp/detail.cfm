<cfinclude template="/exchange/security/check.cfm">

<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from company
  where company_id = #id#
</cfquery>

<cfif comp.company_registered_by_hub_id is #session.hub#>
 <cfset session.edit_comp = 1>
 <cfset session.edit_comp_company_id = #comp.company_id#>
<cfelse>
 <cfset session.edit_comp = 0>
 <cfset session.edit_comp_company_id = 0>
</cfif>

<cfset session.edit_comp_id = #comp.company_id#>

<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  join usr_comp on usr_comp_usr_id = usr_id and
       usr_comp_company_id = #comp.company_id#
  order by usr_last_name, usr_first_name
</cfquery>

<cfquery name="contacts" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_contact
  where company_contact_company_id = #id# and
        company_contact_hub_id = #session.hub#
  order by company_contact_name
</cfquery>

<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from partner_program
  join partner_program_xref on partner_program_xref_program_id = partner_program_id
  where partner_program_xref_partner_id = #id# and
        partner_program_xref_hub_id = #session.hub#
  order by partner_program_order
</cfquery>

<cfquery name="agreements" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_agreement
  join agreement on agreement_id = company_agreement_agreement_id
  left join usr on usr_id = company_agreement_updated_by_usr_id
  where company_agreement_company_id = #id# and
        company_agreement_hub_id = #session.hub#
  order by agreement_order
</cfquery>

<cfset query = 0>

<cfquery name="extend" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_extend
  join partner_tier on partner_tier_id = company_extend_tier_id
  where company_extend_hub_id = #session.hub# and
        company_extend_company_id = #id#
</cfquery>

<cfquery name="rm" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_rm
  join usr on usr_id = company_rm_usr_id
  where company_rm_hub_id = #session.hub# and
        company_rm_company_id = #id#
  order by company_rm_order
</cfquery>

<cfif extend.recordcount is 0>
    <cfset query=1>
	<cfquery name="add" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into company_extend
	  (company_extend_hub_id, company_extend_company_id)
	  values
	  (#session.hub#, #id#)
	 </cfquery>
</cfif>

<cfif query is 1>
	<cfquery name="extend" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from company_extend
	  where company_extend_hub_id = #session.hub# and
			company_extend_company_id = #id#
	</cfquery>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Companies (In Network)</td>
	       <td class="feed_sub_header" align=right>

	       <cfif session.edit_comp is 1>
	       <img src="/images/icon_edit.png" width=15 hspace=10><a href="edit.cfm">Edit Company</a>
	       &nbsp;|&nbsp;
	       </cfif>

	       <a href="index.cfm">Return</a>
	       </td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;"></td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">Relationship Manager has been updated.</td></tr>
        <cfelseif u is 3>
        <tr><td class="feed_sub_header" style="color: green;"></td></tr>
        <cfelseif u is 4>
        <tr><td class="feed_sub_header" style="color: green;">Relationship Manager has been added.</td></tr>
        <cfelseif u is 5>
        <tr><td class="feed_sub_header" style="color: green;">Relationship Manager has been updated.</td></tr>
        <cfelseif u is 6>
        <tr><td class="feed_sub_header" style="color: red;">Relationship Manager has been deleted.</td></tr>
        <cfelseif u is 8>
        <tr><td class="feed_sub_header" style="color: green;">Company Tier have been successfully updated.</td></tr>
        <cfelseif u is 9>
        <tr><td class="feed_sub_header" style="color: green;">Company Programs have been successfully updated.</td></tr>
        <cfelseif u is 11>
        <tr><td class="feed_sub_header" style="color: green;">Company Contact has been added.</td></tr>
        <cfelseif u is 12>
        <tr><td class="feed_sub_header" style="color: green;">Company Contact has been updated.</td></tr>
        <cfelseif u is 13>
        <tr><td class="feed_sub_header" style="color: red;">Company Contact has been deleted.</td></tr>
        <cfelseif u is 31>
        <tr><td class="feed_sub_header" style="color: green;">Company Agreement has been added.</td></tr>
        <cfelseif u is 32>
        <tr><td class="feed_sub_header" style="color: green;">Company Agreement has been updated.</td></tr>
        <cfelseif u is 33>
        <tr><td class="feed_sub_header" style="color: red;">Company Agreement has been deleted.</td></tr>

        </cfif>
       </cfif>

       <tr><td>

        <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td height=10></td></tr>
			<tr><td class="feed_header">#comp.company_name#</td></tr>
			<tr><td class="feed_sub_header" style="font-weight: normal;">#comp.company_long_desc#</td>
			<tr><td class="feed_sub_header" valign=top style="font-weight: normal;">Website - <a href="#comp.company_website#" target="_blank" rel="noopener" rel="noreferrer"><u>#comp.company_website#</u></a></td>
			<tr><td><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr>
			    <td class="feed_sub_header">Company Tier</td>
			    <td class="feed_sub_header" align=right><a href="edit_tier.cfm?id=#id#"><img src="/images/icon_edit.png" width=20 hspace=10 alt="Edit" title="Edit" border=0></a><a href="edit_tier.cfm?id=#id#">Edit</a></td></tr>
         <cfif extend.company_extend_tier_id is "">
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Company Tier has been selected.</td></tr>
         <cfelse>
          <tr><td class="feed_sub_header" style="font-weight: normal;">#extend.partner_tier_name#</td></tr>
         </cfif>

        </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td colspan=2><hr></td></tr>
			<tr>
			    <td class="feed_sub_header">Company Programs</td>
			    <td class="feed_sub_header" align=right><a href="edit_program.cfm?id=#id#"><img src="/images/icon_edit.png" width=20 hspace=10 alt="Edit" title="Edit" border=0></a><a href="edit_program.cfm?id=#id#">Edit</a></td></tr>
        </table>

        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfif programs.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Company Programs have been selected.</td></tr>
         <cfelse>
         	<cfoutput query="programs">
         	 <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><li>#programs.partner_program_name#</li></td></tr>
         	</cfoutput>
         	<tr><td height=10></td></tr>
         </cfif>
        </table>

        <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td colspan=2><hr></td></tr>
			<tr><td class="feed_sub_header">Company Users</td>
			    <td class="feed_sub_header" align=right><a href="add_user.cfm?id=#id#"><img src="/images/plus3.png" width=15 hspace=10 alt="Add" title="Add" border=0></a><a href="add_user.cfm?id=#id#">Add</a></td></tr>
        </table>

        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif users.recordcount is 0>
         <tr><td class="feed_sub_header" style="font-weight: normal;">No Company Users have been identified.</td></tr>
        <cfelse>
          <tr>
             <td></td>
             <td class="feed_sub_header" valign=top>Name</td>
             <td class="feed_sub_header" valign=top>Title</td>
             <td class="feed_sub_header" valign=top>Email</td>
             <td class="feed_sub_header" valign=top>Role</td>
          </tr>
         <cfset counter = 0>

         <cfoutput query="users">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="e0e0e0">
         </cfif>

             <td width=70>
             <a href="edit_user.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&id=#id#">
             <cfif #usr_photo# is "">
			  <img src="/images/headshot.png" height=40 width=40 border=0>
			 <cfelse>
			  <img style="border-radius: 2px;" src="#media_virtual#/#usr_photo#" height=40 width=40>
			 </cfif>
			 </a>
			 </td>

             <td class="feed_sub_header" width=200><a href="edit_user.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&id=#id#">#usr_first_name# #usr_last_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;"><cfif #usr_title# is "">Not Provided<cfelse>#usr_title#</cfif></td>
             <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>
             <td class="feed_sub_header" style="font-weight: normal;">

             <cfif usr_comp_access_rights is 0>
              Regular
             <cfelse>
              Admin
             </cfif>

             </td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

			<tr><td height=10></td></tr>
        </table>



        <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td colspan=2><hr></td></tr>
			<tr><td class="feed_sub_header">Relationship Managers</td>
			    <td class="feed_sub_header" align=right><a href="add_rm.cfm?id=#id#"><img src="/images/plus3.png" width=15 hspace=10 alt="Add" title="Add" border=0></a><a href="add_rm.cfm?id=#id#">Add</a></td></tr>
        </table>

        </cfoutput>




        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif rm.recordcount is 0>
         <tr><td class="feed_sub_header" style="font-weight: normal;">No Relationship Managers have been identified.</td></tr>
        <cfelse>
          <tr>
             <td></td>
             <td class="feed_sub_header" valign=top>Name</td>
             <td class="feed_sub_header" valign=top>Title</td>
             <td class="feed_sub_header" valign=top>Relationship Title</td>
             <td class="feed_sub_header" valign=top>Email</td>
             <td class="feed_sub_header" valign=top align=right>Order</td>
          </tr>
         <cfset counter = 0>
         <cfoutput query="rm">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="e0e0e0">
         </cfif>

             <td width=70>
             <a href="edit_rm.cfm?company_rm_id=#company_rm_id#&company_rm_company_id=#id#">
             <cfif #usr_photo# is "">
			  <img src="/images/headshot.png" height=40 width=40 border=0>
			 <cfelse>
			  <img style="border-radius: 2px;" src="#media_virtual#/#usr_photo#" height=40 width=40>
			 </cfif>
			 </a>
			 </td>

             <td class="feed_sub_header" width=200><a href="edit_rm.cfm?company_rm_id=#company_rm_id#&company_rm_company_id=#id#">#usr_first_name# #usr_last_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;">#usr_title#</td>

             <td class="feed_sub_header" width=300 style="font-weight: normal;">#company_rm_title#</td>
             <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>
             <td class="feed_sub_header" align=right style="font-weight: normal;">#company_rm_order#</td>
         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

			<tr><td colspan=6><hr></td></tr>
			<tr><td height=10></td></tr>
        </table>

        <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header">Company Contacts</td>
			    <td class="feed_sub_header" align=right><a href="add_contact.cfm?id=#id#"><img src="/images/plus3.png" width=15 hspace=10 alt="Add Contact" title="Add Contact" border=0></a><a href="add_contact.cfm?id=#id#">Add Contact</a></td></tr>
        </table>

        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif contacts.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Company Contacts have been created.</td></tr>
         <cfelse>

         <cfset counter = 0>

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_sub_header">Name</td>
             <td class="feed_sub_header">Title</td>
             <td class="feed_sub_header">Email</td>
             <td class="feed_sub_header">Phone</td>
             <td class="feed_sub_header">Cell</td>
          </tr>

          <cfoutput query="contacts">
           <tr>
              <td class="feed_sub_header" style="font-weight: normal;"><a href="edit_contact.cfm?id=#id#&company_contact_id=#company_contact_id#"><b>#company_contact_name#</b></td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_title#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_email#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_phone#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_cell#</td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>

         </cfif>

        <tr><td colspan=5><hr></td></tr>

        </table>

        <cfset counter = 0>

	    </table>

<!--- Company Agreements --->

       <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header">Company Agreements</td>
			    <td class="feed_sub_header" align=right><a href="add_agreement.cfm?id=#id#"><img src="/images/plus3.png" width=15 hspace=10 alt="Add Agreement" title="Add Agreement" border=0></a><a href="add_agreement.cfm?id=#id#">Add Agreement</a></td></tr>
        </table>

        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif agreements.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Company Agreements have been created / uploaded.</td></tr>
         <cfelse>

         <cfset counter = 0>

          <tr>
             <td class="feed_sub_header">Agreement</td>
             <td class="feed_sub_header">Summary</td>
             <td class="feed_sub_header" align=center>Start Date</td>
             <td class="feed_sub_header" align=center>End Date</td>
             <td class="feed_sub_header">Updated By</td>
             <td class="feed_sub_header" align=right>Updated On</td>
          </tr>

          <cfoutput query="agreements">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>
          <td class="feed_sub_header" style="font-weight: normal;"><a href="edit_agreement.cfm?id=#id#&company_agreement_id=#company_agreement_id#"><b>#agreement_name#</b></td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_agreement_comments#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#dateformat(company_agreement_start_date,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#dateformat(company_agreement_end_date,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#usr_first_name# #usr_last_name#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#dateformat(company_agreement_updated,'mm/dd/yyyy')#</td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>

         </cfif>

        </table>

        <cfset counter = 0>

	    </table>









       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

