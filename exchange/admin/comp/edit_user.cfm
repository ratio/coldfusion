<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr_comp
  join usr on usr_id = usr_comp_usr_id
  where usr_comp_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
            usr_comp_company_id = #id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Company User</td>
			   <td class="feed_sub_header" align=right>
			   <a href="detail.cfm?id=#id#">Return</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <form action="user_update.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" width=100>User</td>
			    <td class="feed_sub_header" style="font-weight: normal;">

			    <cfoutput>
			    #edit.usr_last_name#, #edit.usr_first_name# (#tostring(tobinary(edit.usr_email))#)
			    </cfoutput>

			</tr>

			<tr><td class="feed_sub_header" width=100>Role</td>
			    <td><select name="usr_comp_access_rights" class="input_select">
			        <option value=0>Regular
			        <option value=1 <cfif #edit.usr_comp_access_rights# is 1>selected</cfif>>Administrator
			        </select>
			    </td>
			</tr>

			<tr><td height=5></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td height=10></td></tr>

			<tr><td></td>
			    <td>

			    <input type="submit" name="button" value="Update" class="button_blue_large">
			    &nbsp;&nbsp;
			    <input type="submit" name="button" value="Remove" class="button_blue_large" onclick="return confirm('Remove User?\r\nAre you sure you want to remove this user from the Company?');">


			    </td>
			</tr>

			<cfoutput>
			 <input type="hidden" name="usr_id" value=#i#>
			 <input type="hidden" name="company_id" value=#id#>
			</cfoutput>

        </table>

       </form>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

