<cfinclude template="/exchange/security/check.cfm">

<cfquery name="agreements" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from agreement
  where agreement_hub_id = #session.hub#
  order by agreement_order
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Company Agreement</td>
			   <td class="feed_sub_header" align=right>
			   <a href="detail.cfm?id=#id#">Return</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <form action="agreement_save.cfm" method="post" enctype="multipart/form-data">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td height=10></td></tr>
			<tr><td class="feed_sub_header" width=100>Agreement</td>
			    <td>

			    <select name="company_agreement_agreement_id" class="input_select" style="width: 300px;">
			    <cfoutput query="agreements">
			     <option value=#agreement_id#>#agreement_name#
			    </cfoutput>

			</tr>

        <cfoutput>

			<tr><td class="feed_sub_header" width=15% valign=top>Summary</td>
			    <td>
			    <textarea name="company_agreement_comments" class="input_textarea" style="width: 600px; height: 100px;"></textarea><td>
			</tr>

			<tr><td class="feed_sub_header">Start Date</td>
			    <td><input type="date" class="input_date" name="company_agreement_start_date" style="width: 175px;"></td>
			</tr>

			<tr><td class="feed_sub_header">End Date</td>
			    <td><input type="date" class="input_date" name="company_agreement_end_date" style="width: 175px;"></td>
			</tr>

			<tr><td height=10></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td height=10></td></tr>

			<!---

			<tr><td class="feed_sub_header" valign=top>Upload Agreement</td>
				<td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="company_agreement_filename"></td></tr>

			<tr><td height=20></td></tr> --->

			<tr><td></td>
			    <td><input type="submit" name="button" value="Add Agreement" class="button_blue_large"></td>
			</tr>

        </table>

        <input type="hidden" name="id" value="#id#">

        </form>

        </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

