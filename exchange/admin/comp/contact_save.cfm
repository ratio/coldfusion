<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add Contact">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert company_contact
	  (
	   company_contact_name,
	   company_contact_company_id,
	   company_contact_title,
	   company_contact_email,
	   company_contact_phone,
	   company_contact_cell,
	   company_contact_hub_id,
	   company_contact_updated,
	   company_contact_updated_by_usr_id
	   )
	   values
	   (
	   '#company_contact_name#',
	    #id#,
	   '#company_contact_title#',
	   '#company_contact_email#',
	   '#company_contact_phone#',
	   '#company_contact_cell#',
	    #session.hub#,
	    #now()#,
	    #session.usr_id#
	    )
	</cfquery>

	<cflocation URL="detail.cfm?id=#id#&u=11" addtoken="no">

<cfelseif button is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update company_contact
	  set company_contact_name = '#company_contact_name#',
		  company_contact_title = '#company_contact_title#',
		  company_contact_email = '#company_contact_email#',
		  company_contact_phone = '#company_contact_phone#',
		  company_contact_cell = '#company_contact_cell#'
	  where company_contact_hub_id = #session.hub# and
			company_contact_id = #company_contact_id# and
			company_contact_company_id = #id#
	</cfquery>

	<cflocation URL="detail.cfm?id=#id#&u=12" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete company_contact
	  where company_contact_id = #company_contact_id# and
	        company_contact_hub_id = #session.hub# and
	        company_contact_company_id = #id#
	</cfquery>

	<cflocation URL="detail.cfm?id=#id#&u=13" addtoken="no">

</cfif>

