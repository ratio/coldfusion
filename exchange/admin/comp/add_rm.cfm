<cfinclude template="/exchange/security/check.cfm">

<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  join hub_xref on hub_xref_usr_id = usr_id
  where hub_xref_hub_id = #session.hub# and
        hub_xref_active = 1
  order by usr_last_name, usr_first_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Relationship Manager</td>
			   <td class="feed_sub_header" align=right>
			   <a href="detail.cfm?id=#id#">Return</a>
			   </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <form action="rm_update.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" width=100>Name</td>
			    <td class="feed_sub_header" style="font-weight: normal;">

			    <select name="company_rm_usr_id" class="input_select">
			     <cfoutput query="users">
			      <option value=#usr_id#>#usr_last_name#, #usr_first_name#
			     </cfoutput>
			    </select>

			</tr>

			<tr><td class="feed_sub_header" width=200>Relationship Title</td>
			    <td><input type="text" class="input_text" name="company_rm_title" style="width: 500px;" maxlength="300"></td>
			</tr>

			<tr><td class="feed_sub_header" valign=top width=100>Comments</td>
			    <td><textarea class="input_textarea" name="company_rm_comments" style="width: 800px; height: 100px;"></textarea></td>
			</tr>

			<tr><td class="feed_sub_header" width=200>Display Order</td>
			    <td><input type="number" class="input_text" name="company_rm_order" style="width: 75px;" step="1" required></td>
			</tr>

			<tr><td height=5></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td height=10></td></tr>

			<tr><td></td>
			    <td><input type="submit" name="button" value="Add" class="button_blue_large"></td>
			</tr>

			<cfoutput>
			 <input type="hidden" name="company_rm_company_id" value=#id#>
			</cfoutput>

        </table>

       </form>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

