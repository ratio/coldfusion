<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Update">

        <cfif isdefined("remove_logo")>

			<cfquery name="remove" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select company_logo from company
			  where company_id = #session.edit_comp_company_id#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.company_logo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.company_logo#">
        	</cfif>

        </cfif>

		<cfif #company_logo# is not "">

			<cfquery name="getfile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select company_logo from company
			  where company_id = #session.edit_comp_company_id#
			</cfquery>

			<cfif #getfile.company_logo# is not "">
             <cfif FileExists("#media_path#\#getfile.company_logo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.company_logo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

		<cftransaction>

			<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  update company
			  set company_name = '#company_name#',
				  company_about = '#company_about#',

				  <cfif #company_logo# is not "">
				   company_logo = '#cffile.serverfile#',
				  </cfif>
				  <cfif isdefined("remove_logo")>
				   company_logo = null,
				  </cfif>

				  company_website = '#company_website#',
				  company_poc_first_name = '#company_poc_first_name#',
				  company_poc_last_name = '#company_poc_last_name#',
				  company_poc_title = '#company_poc_title#',
				  company_poc_email = '#company_poc_email#',
				  company_poc_phone = '#company_poc_phone#',
				  company_size_id = <cfif #company_size_id# is 0>null<cfelse>#company_size_id#</cfif>,
				  company_entity_id = <cfif #company_entity_id# is 0>null<cfelse>#company_entity_id#</cfif>,
				  company_address_l1 = '#company_address_l1#',
				  company_address_l2 = '#company_address_l2#',
				  company_city = '#company_city#',
				  company_state = <cfif #company_state# is 0>null<cfelse>'#company_state#'</cfif>,
				  company_zip = '#company_zip#',
				  company_duns = '#company_duns#',
				  company_history = '#company_history#',
				  company_long_desc = '#company_long_desc#',
				  company_tagline = '#company_tagline#',
				  company_leadership = '#company_leadership#',
				  company_founded = <cfif #company_founded# is "">null<cfelse>#company_founded#</cfif>,
				  company_employees = <cfif #company_employees# is "">null<cfelse>#company_employees#</cfif>,
				  company_keywords = '#company_keywords#',
				  company_fy16_revenue = <cfif #company_fy16_revenue# is "">null<cfelse>#company_fy16_revenue#</cfif>,
				  company_fy17_revenue = <cfif #company_fy17_revenue# is "">null<cfelse>#company_fy17_revenue#</cfif>,
				  company_fy18_revenue = <cfif #company_fy18_revenue# is "">null<cfelse>#company_fy18_revenue#</cfif>,
				  company_updated = #now()#
				  where company_id = #session.edit_comp_company_id#
			</cfquery>

			</cftransaction>

	<cflocation URL="detail.cfm?u=42&id=#session.edit_comp_company_id#" addtoken="no">

<cfelseif button is "Delete">

<cftransaction>

	<cfquery name="deletepartner" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  delete partner
	  where partner_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete1" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  delete company
	  where company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete_cert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  delete cert
	  where cert_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="updateusrs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  update usr
	  set usr_company_id = null
	  where usr_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete2" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  delete product
	  where product_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete3" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  delete media
	  where media_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete4" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  delete customer
	  where customer_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete5" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   delete biz_code_xref
	   where biz_code_xref_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete6" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		delete align
		where align_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete7" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 delete doc
		 where doc_company_id = #session.edit_comp_company_id#
	</cfquery>

	<cfquery name="delete8" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  delete naics_xref
		  where naics_xref_company_id = #session.edit_comp_company_id#
	</cfquery>

 </cftransaction>

			<cflocation URL="index.cfm?u=43" addtoken="no">

<cfelseif button is "Add">

		<cfif #company_logo# is not "">

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

			<cftransaction>

			<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  insert into company
			   (
				company_size_id,
				company_private_hub_id,
				company_product_summary,
				company_tagline,
				company_long_desc,
				company_entity_id,
				company_registered,
				company_registered_by_usr_id,
				company_registered_by_hub_id,
				company_name,
				company_about,
				company_logo,
				company_website,
				company_poc_first_name,
				company_poc_last_name,
				company_poc_title,
				company_poc_email,
				company_poc_phone,
				company_address_l1,
				company_address_l2,
				company_city,
				company_state,
				company_zip,
				company_duns,
				company_history,
				company_leadership,
				company_founded,
				company_employees,
				company_keywords,
				company_fy16_revenue,
				company_fy17_revenue,
				company_fy18_revenue,
				company_updated
				)
			  values
			  (
				 <cfif #company_size_id# is 0>null<cfelse>#company_size_id#</cfif>,
				 #session.hub#,
				'#company_product_summary#',
				'#company_tagline#',
				'#company_long_desc#',
				 <cfif #company_entity_id# is 0>null<cfelse>#company_entity_id#</cfif>,
				 #now()#,
				 #session.usr_id#,
				 #session.hub#,
				'#company_name#',
				'#company_about#',

				  <cfif #company_logo# is not "">
				   '#cffile.serverfile#',
				  <cfelse>
				   null,
				  </cfif>

				'#company_website#',
				'#company_poc_first_name#',
				'#company_poc_last_name#',
				'#company_poc_title#',
				'#company_poc_email#',
				'#company_poc_phone#',
				'#company_address_l1#',
				'#company_address_l2#',
				'#company_city#',
				<cfif #company_state# is 0>null<cfelse>'#company_state#'</cfif>,
				'#company_zip#',
				'#company_duns#',
				'#company_history#',
				'#company_leadership#',
				 <cfif #company_founded# is "">null<cfelse>#company_founded#</cfif>,
				 <cfif #company_employees# is "">null<cfelse>#company_employees#</cfif>,
				'#company_keywords#',
				 <cfif #company_fy16_revenue# is "">null<cfelse>#company_fy16_revenue#</cfif>,
				 <cfif #company_fy17_revenue# is "">null<cfelse>#company_fy17_revenue#</cfif>,
				 <cfif #company_fy18_revenue# is "">null<cfelse>#company_fy18_revenue#</cfif>,
				 #now()#
			   )
			</cfquery>

			<cfquery name="max" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select max(company_id) as id from company
			</cfquery>

			</cftransaction>

			<cfquery name="add_to_network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert hub_comp
			 (
			 hub_comp_hub_id,
			 hub_comp_company_id,
			 hub_comp_added_by_usr_id,
			 hub_comp_added
			 )
			 values
			 (
			  #session.hub#,
			  #max.id#,
			  #session.usr_id#,
			  #now()#
			 )
			</cfquery>

<cflocation URL="index.cfm?u=1" addtoken="no">

</cfif>