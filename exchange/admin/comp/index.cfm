<cfinclude template="/exchange/security/check.cfm">

<cfquery name="complist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_comp_company_id from hub_comp
 where hub_comp_hub_id = #session.hub#
</cfquery>

<cfset perpage = 100>

<cfif complist.recordcount is 0>
 <cfset clist = 0>
<cfelse>
 <cfset clist = valuelist(complist.hub_comp_company_id)>
</cfif>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select company_logo, company_id, company_name, company_long_desc, company_website, company_address_l1, company_city, company_state, company_poc_first_name, company_poc_last_name, company_poc_title, company_poc_phone, company_poc_email from company
  where company_id in (#clist#)

  <cfif isdefined("name") and button is not "Clear">
   and company_name like '%#name#%'
  </cfif>

  order by company_name
</cfquery>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>


<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <form action="index.cfm" method="post">

	   <tr>

	   <td class="feed_header">Companies</td>

           <td class="feed_sub_header" style="font-weight: normal;">

             <input type="text" class="input_text" name="name" style="width: 300px;" <cfif isdefined("name") and button is not "Clear">value="<cfoutput>#name#</cfoutput>"</cfif> placeholder="search for company name">

             &nbsp;&nbsp;&nbsp;

             <input type="submit" name="button" class="button_blue" value="Search">
             &nbsp;

             <input type="submit" name="button" class="button_blue" value="Clear">

           </td>


           <td align=center class="feed_sub_header">

              <cfoutput>
					<cfif agencies.recordcount GT #perpage#>
						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>
            </cfoutput>


           </td>

	       <td class="feed_sub_header" align=right>

	       <a href="discover.cfm"><img src="/images/plus3.png" border=0 width=15 hspace=5 alt="Add New Company" title="Add New Company"></a>
	       <a href="discover.cfm">Add New Company</a>&nbsp;|&nbsp;


	       <a href="index.cfm?export=1">Export to Excel</a>&nbsp;|&nbsp;
	       <a href="/exchange/admin/index.cfm">Settings</a>

	       </td>

	       </tr>
        </form>
	   <tr><td colspan=4><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <cfif isdefined("u")>
        <cfif u is 9>
         <tr><td class="feed_sub_header" style="color: green;">Company Programs have been successfully updated.</td></tr>
        <cfelseif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">New Company has been successfully added.</td></tr>
        <cfelseif u is 3>
        <tr><td class="feed_sub_header" style="color: green;"></td></tr>
        </cfif>
         <tr><td>&nbsp;</td></tr>
       </cfif>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #agencies.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this <cfif #session.network# is 0>Exchange<cfelse>Network</cfif>.</td></tr>

        <cfelse>

			<tr>
			    <td></td>
			    <td class="feed_sub_header">Company Name</td>
			    <td class="feed_sub_header">Description</td>
				<td class="feed_sub_header">Website</td>
				<td class="feed_sub_header">City</td>
				<td class="feed_sub_header" align=center>State</td>
                <td></td>
			</tr>

        </cfif>

        <cfset counter = 0>

		 <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

					<td class="feed_option" valign=top width=60>

                   <a href="detail.cfm?id=#company_id#">
                    <cfif agencies.company_logo is "">
					  <img src="//logo.clearbit.com/#agencies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#agencies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>


              <td class="feed_sub_header" width=350 valign=top><a href="detail.cfm?id=#company_id#">#agencies.company_name#</a></td>
              <td class="feed_sub_header" width=500 valign=top style="font-weight: normal;">
              <cfif #len(agencies.company_long_desc GT 300)#>
               #left(agencies.company_long_desc,'300')#...
              <cfelse>
               #agencies.company_long_desc#
              </cfif>

              </td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">#agencies.company_website#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">#ucase(agencies.company_city)#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#ucase(agencies.company_state)#</td>
              <td class="feed_sub_header" valign=top><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">Profile</td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

