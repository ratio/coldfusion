<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from lens
 where lens_id = #lens_id#
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <form action="db.cfm" method="post">

	  <div class="main_box">


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Category</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header">Category Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="lens_name" value="#edit.lens_name#" required></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea class="input_textarea" style="width: 600px; height: 150px;" name="lens_desc">#edit.lens_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header">Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="lens_order" value=#edit.lens_order# required></td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>

		   <input type="hidden" name="lens_id" value=#lens_id#>

		   </cfoutput>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

