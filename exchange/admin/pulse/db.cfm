<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into lens (lens_name, lens_order, lens_desc, lens_hub_id)
	 values ('#lens_name#',#lens_order#,'#lens_desc#',#session.hub#)
	</cfquery>

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete lens
	 where lens_id = #lens_id#
	</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update lens
	 set lens_name = '#lens_name#',
	     lens_order = #lens_order#,
	     lens_desc = '#lens_desc#'
	 where lens_id = #lens_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">