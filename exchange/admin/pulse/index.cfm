<cfinclude template="/exchange/security/check.cfm">

<cfquery name="categories" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from lens
  where lens_hub_id = #session.hub#
  order by lens_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Newswire Categories</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" align=right><a href="add.cfm"><img src="/images/plus3.png" width=15 border=0 hspace=10></a><a href="add.cfm">Add Category</a></td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #categories.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Newswire Categories Exist.</td></tr>

        <cfelse>

			<tr><td class="feed_sub_header" width=100>Order</td>
				<td class="feed_sub_header" width=300>Category Name</b></td>
				<td class="feed_sub_header">Description</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="categories">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=middle><a href="edit.cfm?lens_id=#lens_id#">#lens_order#</a></td>
              <td class="feed_sub_header" valign=middle><a href="edit.cfm?lens_id=#lens_id#">#lens_name#</a></td>
              <td class="feed_sub_header" valign=middle style="font-weight: normal;">#lens_desc#</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

