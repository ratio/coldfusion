<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Member</td>
               <td class="feed_option" align=right><a href="members.cfm">Return</a></td></tr>
           <tr><td>&nbsp;</td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfquery name="member_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from usr
		   left join hub_xref on hub_xref.hub_xref_usr_id = usr.usr_id
		   where usr.usr_id = #i# and
		         hub_xref.hub_xref_hub_id = #session.hub#
		 </cfquery>

         <cfif member_info.recordcount is 0>
          <tr><td class="feed_option">Member not found as part of this HUB.</td></tr>
         <cfelse>

         <form action="edit_member_db.cfm" method="post">

         <cfoutput>

          <tr>
             <td class="feed_option" width=150><b>Name:</b></td>
             <td class="feed_option">#member_info.usr_first_name# #member_info.usr_last_name#</td></tr>

          <tr>
             <td class="feed_option" width=150><b>Email Address:</b></td>
             <td class="feed_option">#member_info.usr_email#</td></tr>

          <tr>
             <td class="feed_option" width=150><b>Join Message:</b></td>
             <td class="feed_option">#member_info.hub_xref_join_message#</td></tr>

         <tr>
             <td class="feed_option" width=150><b>Status:</b></td>
             <td class="feed_option">
             <select name="status">
              <option value=0 <cfif #member_info.hub_xref_active# is 0>selected</cfif>>Not Approved
              <option value=1 <cfif #member_info.hub_xref_active# is 1>selected</cfif>>Approved
             <select></td></tr>

         <tr>
             <td class="feed_option" width=150><b>Role:</b></td>
             <td class="feed_option">
             <select name="role">
              <option value=1 <cfif #member_info.hub_xref_usr_role# is 1>selected</cfif>>HUB Administrator
              <option value=2 <cfif #member_info.hub_xref_usr_role# is 2>selected</cfif>>User
             <select></td></tr>

         <tr>
             <td></td>
             <td class="feed_option">
             <input type="checkbox" name="notify">&nbsp;Sent member an email indicating they were approved and can now access the HUB?
             </td></tr>

          <tr><td height=10></td></tr>
          <tr><td></td><td><input class="button_blue" style="font-size: 11px; height: 25px; width: 75px;" type="submit" name="button" value="Update"></td></tr>

          <input type="hidden" name="i" value=#i#>

         </cfoutput>

         </form>

         </cfif>

       </table>




	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>