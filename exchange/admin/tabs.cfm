<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">


	   <cfquery name="tabs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select * from tab
	    where tab_hub_id = #session.hub#
	    order by tab_order
	   </cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Tabs</td><td class="feed_option" align=right><a href="add_tab.cfm">Add Tab</td></tr>

	  <tr><td height=5></td></tr>
	  <cfif isdefined("u")>
	   <cfif u is 1>
		<tr><td class="feed_option"><font color="green"><b>Tab has been successfully added.</b></font></td></tr>
	   <cfelseif u is 2>
		<tr><td class="feed_option"><font color="green"><b>Tab has been successfully updated.</b></font></td></tr>
	   <cfelseif u is 3>
		<tr><td class="feed_option"><font color="red"><b>Tab has been deleted.</b></font></td></tr>
	   </cfif>
	  </cfif>

       </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #tabs.recordcount# is 0>

 		     <tr><td class="feed_option">No tabs have been created.</td></tr>

        <cfelse>

			 <tr>
				 <td class="feed_option" width=75><b>Order</b></td>
				 <td class="feed_option" width=200><b>Tab Name</b></td>
				 <td class="feed_option"><b>Tab Title</b></td>
			 </tr>

        <cfset counter = 0>

			<cfoutput query="tabs">

				<cfif counter is 0>
				 <tr bgcolor="ffffff">
				<cfelse>
				 <tr bgcolor="e0e0e0">
				</cfif>

				<td class="feed_option">#tab_order#</td>
				<td class="feed_option"><a href="edit_tab.cfm?tab_id=#tab_id#">#tab_name#</a></td>
				<td class="feed_option">#tab_title#</td>

				</tr>

			</cfoutput>

        </cfif>

        </table>


	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>