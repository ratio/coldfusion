<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into certification (certification_cat_id, certification_name, certification_desc)
	 values (#certification_cat_id#,'#certification_name#','#certification_desc#')
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete certification
	 where certification_id = #certification_id#
	</cfquery>

   <cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update certification
	 set certification_name = '#certification_name#',
	     certification_desc = '#certification_desc#',
	     certification_cat_id = #certification_cat_id#
	 where certification_id = #certification_id#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

</cfif>

