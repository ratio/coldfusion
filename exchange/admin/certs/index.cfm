<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from cert_cat
  where cert_cat_hub_id = #session.hub#
  order by cert_cat_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">User Certifications</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/index.cfm">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header"><a href="cat_add.cfm">Add Certification Category</a>&nbsp;|&nbsp;<a href="add.cfm">Add Certification</a></td></tr>

       <cfif isdefined("u")>

        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">Certification has been successfully added.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">Certification has been successfully updated.</td></tr>
        <cfelseif u is 3>
         <tr><td class="feed_sub_header" style="color: red;">Certification has been successfully deleted.</td></tr>

        <cfelseif u is 21>
         <tr><td class="feed_sub_header" style="color: green;">Certification Category has been successfully updated.</td></tr>
        <cfelseif u is 22>
         <tr><td class="feed_sub_header" style="color: green;">Certification Cateogry has been successfully updated.</td></tr>
        <cfelseif u is 23>
         <tr><td class="feed_sub_header" style="color: red;">Certification has been successfully deleted.</td></tr>

        </cfif>
       </cfif>


        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif cats.recordcount is 0>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No Certification categories exist.  Please add a Certification category before adding Certifications.</td></tr>

        <cfelse>

			<tr>
			    <td class="feed_sub_header" width=100>Category</td>
			    <td class="feed_sub_header" width=400>Certification</td>
			    <td class="feed_sub_header">Description</td>
			</tr>

			<tr><td height=10></td></tr>

        <cfloop query="cats">

         <cfoutput>
         <tr><td colspan=2 class="feed_sub_header" bgcolor="d0d0d0"><a href="cat_edit.cfm?cert_cat_id=#cert_cat_id#"><b>#cert_cat_name#</b></a></td>
             <td align=right bgcolor="d0d0d0"><a href="add.cfm?cat_id=#cert_cat_id#"><img src="/images/plus3.png" width=15 hspace=10 border=0 alt="Add Certification" title="Add Certification"></a></td></tr>
         </cfoutput>

		 <cfquery name="certs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from certification
		   where certification_cat_id = #cats.cert_cat_id#
		   order by certification_name
		 </cfquery>

         <cfif certs.recordcount is 0>
          <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;">No Certifications have been created for this category.</td></tr>
         <cfelse>

         <cfset counter = 0>

         <cfoutput query="certs">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td></td>
              <td class="feed_sub_header" valign=top width=300><a href="edit.cfm?certification_id=#certification_id#">#certification_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;">#certification_desc#</a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>

         <tr><td>&nbsp;</td></tr>

        </cfloop>

        </cfif>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

