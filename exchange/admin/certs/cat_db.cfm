<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into cert_cat (cert_cat_hub_id, cert_cat_name, cert_cat_desc,cert_cat_order)
	 values (#session.hub#,'#cert_cat_name#','#cert_cat_desc#',<cfif #cert_cat_order# is "">null<cfelse>#cert_cat_order#</cfif>)
	</cfquery>

    <cflocation URL="index.cfm?u=21" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete cert_cat
	 where cert_cat_id = #cert_cat_id#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete certification
	 where certification_cat_id = #cert_cat_id#
	</cfquery>

</cftransaction>

    <cflocation URL="index.cfm?u=23" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update cert_cat
	 set cert_cat_name = '#cert_cat_name#',
	     cert_cat_desc = '#cert_cat_desc#',
	     cert_cat_order = <cfif #cert_cat_order# is "">null<cfelse>#cert_cat_order#</cfif>
	 where cert_cat_id = #cert_cat_id#
	</cfquery>

    <cflocation URL="index.cfm?u=22" addtoken="no">

</cfif>

