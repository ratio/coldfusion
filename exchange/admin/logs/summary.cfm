<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Usage Log - Detailed Log</td>
               <td class="feed_sub_header" align=right>

			   <a href="index.cfm?export=1">Export to Excel</a>

			   &nbsp;|&nbsp;


               <a href="/exchange/admin/logs/">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>
          </table>

          <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			SELECT usr_first_name, usr_last_name, usr_email,  log_usr_id, CAST(log_date AS DATE) as date, count(log_usr_id) as total
			FROM log
			join usr on usr_id = log_usr_id
			where log_hub_id = #session.hub#
			GROUP BY usr_last_name, usr_first_name, usr_email, log_usr_id, CAST(log_date AS DATE)
			order by cast(log_date as date) DESC, usr_last_name
	      </cfquery>

		  <cfif isdefined("export")>
		      <cfinclude template="/exchange/include/export_to_excel.cfm">
	      </cfif>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr>
               <td class="feed_sub_header">Name</td>
               <td class="feed_sub_header">Email</td>
               <td class="feed_sub_header">Date</td>
               <td class="feed_sub_header" align=right>Usage</td>
           </tr>

           <cfset counter = 0>

           <cfoutput query="agencies">
            <cfset var = #date#>
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

               <td class="feed_sub_header"><a href="detail.cfm?i=#encrypt(log_usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&date=#date#">#usr_last_name#, #usr_first_name#</a></td>
               <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>
               <td class="feed_sub_header" style="font-weight: normal;">#dateformat(date,'mm/dd/yyyy')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#total#</td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>
          </table>

	  </div>

	  </td></tr>

 </table>

<br>
<br>
<br>
<br>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>

