<cfinclude template="/exchange/security/check.cfm">

  <cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr
	where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

		  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Usage Log - #user.usr_first_name# #user.usr_last_name# - #dateformat(date,'mm/dd/yyyy')#</td>
               <td class="feed_sub_header" align=right>

                            <a href="detail.cfm?export=1&i=#i#&date=#date#">Export to Excel</a>

               &nbsp;|&nbsp;

               <a href="summary.cfm">Return</td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>
          </table>
          </cfoutput>

          <cfset start = '#dateformat(date,'yyyymmdd')#' & ' 00:00:00.000'>
          <cfset end = '#dateformat(date,'yyyymmdd')#' & ' 23:59:59.000'>

          <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			SELECT usr_first_name, usr_last_name, usr_email, log_page, log_query_string, log_ip, log_date, CONVERT(VARCHAR(8),log_date,108) as time from log
			join usr on usr_id = log_usr_id
			where log_hub_id = #session.hub# and
			      log_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			      (log_date >= '#start#' and log_date <= '#end#')
			order by log_date DESC
	      </cfquery>

		  <cfif isdefined("export")>
		    <cfinclude template="/exchange/include/export_to_excel.cfm">
	      </cfif>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr>
               <td class="feed_sub_header">Page</td>
               <td class="feed_sub_header">IP Address</td>
               <td class="feed_sub_header" align=right>Date / Time</td>
           </tr>

           <cfset counter = 0>

           <cfoutput query="agencies">
            <cfif counter is 0>
             <tr bgcolor="ffffff">
            <cfelse>
             <tr bgcolor="e0e0e0">
            </cfif>

               <td class="feed_sub_header" style="font-weight: normal;">#log_page#</td>
               <td class="feed_sub_header" style="font-weight: normal;">#log_ip#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#dateformat(log_date,'mm/dd/yyyy')# at #timeformat(log_date)#</td>

            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>
          </table>

	  </div>

	  </td></tr>

 </table>

<br>
<br>
<br>
<br>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>

