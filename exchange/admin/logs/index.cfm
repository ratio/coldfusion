<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset i = #session.hub#>

<cfquery name="hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #i#
</cfquery>

<cfif not isdefined("session.log_date_from")>
 <cfset #session.log_date_from# = #dateformat(dateadd('D',-30,now()),'mm/dd/yyyy')#>
</cfif>

<cfif not isdefined("session.log_date_to")>
 <cfset #session.log_date_to# = #dateformat(now(),'mm/dd/yyyy')#>
</cfif>

<!--- Log data --->

<cfquery name="usage" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select cast(log_date AS date) [date], Count(1)  [log_count] from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#' and
       log_hub_id = #i#
 group by cast(log_date AS date)
 order by 1
</cfquery>

<cfquery name="portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(log_id) as total from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#'
 and log_hub_id = #i#
 and contains((*),'/portfolio/')
</cfquery>

<cfquery name="network_out" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(log_id) as total from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#'
 and log_hub_id = #i#
 and contains((*),'network_out.cfm')
</cfquery>

<cfquery name="network_in" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(log_id) as total from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#'
 and log_hub_id = #i#
 and contains((*),'network_in.cfm')
</cfquery>

<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(log_id) as total from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#'
 and log_hub_id = #i#
 and contains((*),'/pipeline/')
</cfquery>

<cfquery name="marketreport" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(log_id) as total from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#'
 and log_hub_id = #i#
 and contains((*),'/exchange/market/')
</cfquery>

<cfquery name="research" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(log_id) as total from log
 where log_date between '#session.log_date_from#' and '#session.log_date_to#'
 and log_hub_id = #i#
 and (contains((*),'/exchange/awards/') or
 	  contains((*),'/exchange/research/'))
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Usage Log</td>
               <td class="feed_sub_header" align=right>


			   <a href="summary.cfm">Detailed Log</a>

			   &nbsp;|&nbsp;


               <a href="/exchange/admin/">Settings</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>
          </table>

	  <cfoutput>
 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_sub_header" style="font-weight: normal;">
        <form action="set_date.cfm" method="post">
        <b>Select Dates:</b>&nbsp;&nbsp;
        <input type="date" name="log_from" class="input_text" style="width: 175px;" value="#dateformat(session.log_date_from,'yyyy-mm-dd')#">
        &nbsp;<b>To</b>&nbsp;
        <input type="date" name="log_to" class="input_text" style="width: 175px;" value="#dateformat(session.log_date_to,'yyyy-mm-dd')#">
        <input type="submit" name="button" value="Refresh" class="button_blue">
        </form>
        </td></tr>
       </table>
      </cfoutput>

 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_sub_header" align=center>Daily Usage (Page Hits)</td></tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {
			var data = google.visualization.arrayToDataTable([
			  ['Date', 'Usage'],

			  <cfoutput query="usage">
			  <cfif log_count GT 10>
			  ['#dateformat(date,'mmm d')#', #log_count#],
			  </cfif>
			  </cfoutput>
			]);

			var options = {
			  title: '',
			  selectionMode: 'multiple',
			  hAxis: { title: '', textStyle: {color: 'black', fontSize: 12}},
			  vAxis: { title: '', textStyle: {color: 'black', fontSize: 12}},
			  chartArea:{left:70,top:20,width:'90%',height:'75%'},
			  legend: { position: 'bottom', textStyle: {fontSize: 12}}
			};

			var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

			chart.draw(data, options);
		  }
		</script>
		<center><div id="curve_chart" style="width: 100%; height: 285px"></div></center>

		</td></tr>
	   </table>




 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
 	   <tr><td height=10></td></tr>
       <tr><td class="feed_sub_header" align=center>Functional Usage</td></tr>
       </table>

 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = new google.visualization.DataTable();
			var chart = new google.visualization.PieChart(document.getElementById('vendor_graph'));

			data.addColumn('string','Cat');
			data.addColumn('number','Value');
			data.addColumn('string','Link');

			data.addRows([

			  <cfoutput>
			  ['Research',#research.total#,''],
			  ['Company Portfolios',#portfolio.total#,''],
			  ['In Network Companies',#network_in.total#,''],
			  ['Out of Network Companies',#network_out.total#,''],
			  ['Opportunity Modeling',#pipeline.total#,''],
			  ['Market Reports',#marketreport.total#,''],
			  </cfoutput>

			]);

			var options = {
			legend: 'labeled',
			title: '',
			chartArea:{right: 30, left:50,top:30,width:'83%',height:'75%'},
			pieHole: 0.4,
			height: 325,
			fontSize: 12,
			};

	 google.visualization.events.addListener(chart, 'select', function () {
		var selection = chart.getSelection();
		if (selection.length > 0) {
		  window.open(data.getValue(selection[0].row, 2), '_self');
		  console.log(data.getValue(selection[0].row, 2));
		}
	  });

	  function drawChart1() {
		chart.draw(data, options);
	  }
	  drawChart1();

		  }

	 </script>

	 <div id="vendor_graph" style="width: 100%"></div>

	 </td></tr>
	 </table>

	  </div>

	  </td><td width=30>&nbsp;</td></tr>

  </table>

















	  </div>

	  </td></tr>

 </table>

<br>
<br>
<br>
<br>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>

