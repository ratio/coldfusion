<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="roles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub#
  order by hub_role_name
</cfquery>

<cfquery name="roles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub#
  order by hub_role_name
</cfquery>

<cfquery name="license" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_order
 left join license on license_id = hub_order_license_id
 where hub_order_hub_id = #session.hub# and
 hub_order_active = 1
</cfquery>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

      <form action="profile_db.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header"><cfif session.network is 0>Exchange<cfelse>Network</cfif> Profile</td>
			   <td class="feed_sub_header" align=right><a href="index.cfm">Settings</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       	   <cfoutput>

			   <tr>
					  <td class="feed_sub_header" width=15%>License</td>
					  <td class="feed_sub_header">

					  <cfif license.license_name is "">
					   No License Assigned
					  <cfelse>
					  <a href="/exchange/admin/license/">#license.license_name#</a>&nbsp;&nbsp;&nbsp;( <a href="/exchange/admin/license/"><u>Manage</u></a> )
					  </cfif>


					  </td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Short Name / Abbr</td>
					  <td><input class="input_text" style="width: 200px;" type="text" maxlength=20 required name="hub_name_short" value="#hub.hub_name_short#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Full Name</td>
					  <td><input class="input_text" style="width: 500px;" type="text" maxlength=200 required name="hub_name" value="#hub.hub_name#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Tag Line</td>
					  <td><input class="input_text" style="width: 500px;" type="text" maxlength=100 name="hub_tag_line" value="#hub.hub_tag_line#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top>Description</td>
					  <td><textarea class="input_textarea" style="width: 800px; height: 150px;" name="hub_desc" required>#hub.hub_desc#</textarea></td>
			   </tr>

		       <tr><td class="feed_sub_header">Keywords</td>
		           <td><input class="input_text" style="width: 600px;" name="hub_tags" value="#hub.hub_tags#" maxlength="200"></td></tr>

		       <tr><td></td><td class="link_small_gray">seperate tags with a comma</td></tr>


               <cfif session.network is 0>

				   <tr>
						  <td class="feed_sub_header" valign=top>Type</td>
						  <td class="feed_sub_header" style="font-weight: normal;">
						  Open Exchange&nbsp;<input type="radio" name="hub_type_id" value=1 <cfif #hub.hub_type_id# is 1>checked</cfif>>&nbsp;&nbsp;
						  Standard Exchange&nbsp;<input type="radio" name="hub_type_id" value=2 <cfif #hub.hub_type_id# is 2>checked</cfif>>&nbsp;&nbsp;
						  Private Exchange&nbsp;<input type="radio" name="hub_type_id" value=3 <cfif #hub.hub_type_id# is 3>checked</cfif>>&nbsp;&nbsp;
				   </tr>

				   <tr><td></td>
					   <td class="link_small_gray">- <b>Open Exchanges</b> are visible in the Marketplace and do not require approval to join.<br>
												   - <b>Standard Exchanges</b> are visible in the Marketplace and require approval to join.<br>
												   - <b>Private Exchanges</b> are hidden from the Marketplace and are invitation only.</td></tr>

               <cfelse>


				   <tr>
						  <td class="feed_sub_header" valign=top>Type</td>
						  <td class="feed_sub_header" style="font-weight: normal;">
						  Open Network&nbsp;<input type="radio" name="hub_type_id" value=1 <cfif #hub.hub_type_id# is 1>checked</cfif>>&nbsp;&nbsp;
						  Standard Network&nbsp;<input type="radio" name="hub_type_id" value=2 <cfif #hub.hub_type_id# is 2>checked</cfif>>&nbsp;&nbsp;
						  Private Network&nbsp;<input type="radio" name="hub_type_id" value=3 <cfif #hub.hub_type_id# is 3>checked</cfif>>&nbsp;&nbsp;
				   </tr>

				   <tr><td></td>
					   <td class="link_small_gray">- <b>Open Networks</b> are visible in the Marketplace and do not require approval to join.<br>
												   - <b>Standard Networks</b> are visible in the Marketplace and require approval to join.<br>
												   - <b>Private Networks</b> are hidden from the Marketplace and are invitation only.</td></tr>

               </cfif>

			   <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" valign=top>Image / Logo</td>
			   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					<cfif #hub.hub_logo# is "">
					  <input type="file" name="hub_logo">
					<cfelse>
					  <img style="border-radius: 0px;" src="#media_virtual#/#hub.hub_logo#" width=100>
					  <br><br>
					  <input type="file" name="hub_logo"><br><br>
					  <font size=2><input type="checkbox" name="remove_photo">&nbsp;or, check to remove image /logo
					 </cfif>

		       </td></tr>

			   <tr><td class="feed_sub_header">Location</td>
			   <td><input class="input_text" type="text" name="hub_city" placeholder="City" style="width: 200px;" value="#hub.hub_city#" maxlength="50">&nbsp;&nbsp;

			   </cfoutput>

			   <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from state
				order by state_name
			   </cfquery>

			   <select name="hub_state" class="input_select">
				<cfoutput query="states">
				 <option value="#state_abbr#" <cfif #hub.hub_state# is #state_abbr#>selected</cfif>>#state_name#
				</cfoutput>
			   </select>&nbsp;&nbsp;

			   <cfoutput>

			   <input class="input_text" type="text" name="hub_zip" placeholder="Zipcode" style="width: 100px;" maxlength=10 value="#hub.hub_zip#" maxlength="50">

			   </td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Header Background Color</b></td>
					  <td class="feed_sub_header"><input style="font-size: 12px; width: 100px;" type="color" maxlength=10 name="hub_header_bgcolor" <cfif #hub.hub_header_bgcolor# is "">value="##022447"<cfelse>value="#hub.hub_header_bgcolor#"</cfif>></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Password Change Policy</b></td>
					  <td>

					   <select name="hub_password_policy" class="input_select">
				         <option value=0 <cfif #hub.hub_password_policy# is 0>selected</cfif>>Never
				         <option value=30 <cfif #hub.hub_password_policy# is 30>selected</cfif>>30 Days
				         <option value=60 <cfif #hub.hub_password_policy# is 60>selected</cfif>>60 Days
				         <option value=90 <cfif #hub.hub_password_policy# is 90>selected</cfif>>90 Days
				         <option value=180 <cfif #hub.hub_password_policy# is 180>selected</cfif>>180 Days
				       </select>

                       </td>

			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Show App Library?</b></td>
					  <td>
					     <input type="checkbox" name="hub_show_app_library" <cfif #hub.hub_show_app_library# is 1>checked</cfif> style="width: 22px; height: 22px;">
					  </td>
			   </tr>

			   <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Support Team Name</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" required maxlength=100 name="hub_support_name" value="#hub.hub_support_name#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Support Email Address</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" required maxlength=100 name="hub_support_email" value="#hub.hub_support_email#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Support Phone Number</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" required maxlength=20 name="hub_support_phone" value="#hub.hub_support_phone#"></td>
			   </tr>

			   <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Login Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=300 name="hub_login_page" value="#hub.hub_login_page#"></td>
			   </tr>


			   <tr>
					  <td class="feed_sub_header" width=15%><b>Login Page Title</b></td>
					  <td><input class="input_text" style="width: 800px;" type="text" maxlength=300 name="hub_login_page_title" value="#hub.hub_login_page_title#" maxlength=300></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top>Login Page Text</td>
					  <td><textarea class="input_textarea" style="width: 800px; height: 150px;" name="hub_login_page_desc" required>#hub.hub_login_page_desc#</textarea></td>
			   </tr>


			   <tr>
					  <td class="feed_sub_header" width=15%><b>Logout Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=300 name="hub_logout_page" value="#hub.hub_logout_page#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Forgot Password Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=300 name="hub_forgot_password_page" value="#hub.hub_forgot_password_page#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Allow Signup from Login Page?</b></td>
					  <td>
					     <select name="hub_signup" class="input_select">
					      <option value=0>No
					      <option value=1 <cfif hub.hub_signup is 1>selected</cfif>>Yes
					     </select>
					     <span class="link_small_gray">If yes, Sign Up link will appear on the log in page.</span>
					  </td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>New User Fast Access</b></td>
					  <td>
					     <select name="hub_newaccount_go" class="input_select">
					      <option value=0>No
					      <option value=1 <cfif #hub.hub_newaccount_go# is 1>selected</cfif>>Yes
					     </select>
					     <span class="link_small_gray">Allow immediate access to Exchange after new account registration?</span>
					  </td>
			   </tr>

               </cfoutput>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Default Role</b></td>
					  <td>
					     <select name="hub_default_role_id" class="input_select">
					     <cfoutput query="roles">
					      <option value=#roles.hub_role_id# <cfif #roles.hub_role_id# is #hub.hub_default_role_id#>selected</cfif>>#roles.hub_role_name#
					     </cfoutput>
					     </select>
					     <span class="link_small_gray">Used when a user signs up or joins your Exchange from the Marketplace.</span>
					  </td>
			   </tr>


			   <tr>
					  <td class="feed_sub_header" width=15%><b>Allow Join from the Marketplace?</b></td>
					  <td>
					     <select name="hub_join" class="input_select">
					      <option value=0>No
					      <option value=1 <cfif hub.hub_join is 1>selected</cfif>>Yes
					     </select>
					     <span class="link_small_gray">Allows users to join your Exchange from the Marketplace.</span>

					  </td>
			   </tr>

			   <cfoutput>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Allow User Invite?</b></td>
					  <td>
					     <select name="hub_allow_user_invite" class="input_select">
					      <option value=0>No
					      <option value=1 <cfif hub.hub_allow_user_invite is 1>selected</cfif>>Yes
					     </select>
					     <span class="link_small_gray">Allows users to invite other users into the Exchange or Network.</span>
					  </td>
			   </tr>

			   <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Main Website</b></td>
					  <td><input class="input_text" style="width: 800px;" type="url" maxlength=1000 name="hub_website" value="#hub.hub_website#"></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Subdomain</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=150 name="hub_subdomain_name" value="#hub.hub_subdomain_name#"><span class="link_small_gray">Must be provided by the Exchange Support Team</span></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Single Sign-on URL</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=150 name="hub_sso_path" value="#hub.hub_sso_path#"><span class="link_small_gray">Must be provided by the Exchange Support Team</span></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Unique URL</b></td>
					  <td class="feed_sub_header" style="font-weight: normal;">#session.site_url#?i=#hub.hub_encryption_key#</td></tr>

               <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
               <tr><td height=10></td></tr>

			   <tr><td></td><td>
			   <input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
			   <input class="button_blue_large" type="submit" name="button" value="Cancel" vspace=10>

			   </td></tr>
           </cfoutput>

	   </table>

	   </form>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>