<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script src="/include/nicedit.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

  <cfinclude template = "/exchange/include/header.cfm">

<!-- Get Lookups -->

   <cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from usr
    where usr_id = #session.usr_id#
   </cfquery>

   <cfquery name="feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from feed
    where feed_id = #feed_id# and
    feed_posted_by_hub_id = #session.hub#
   </cfquery>

   <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from market
    where market_hub_id = #session.hub#
   </cfquery>

   <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from sector
    where sector_hub_id = #session.hub#
    order by sector_name
   </cfquery>

   <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from topic
    where topic_hub_id = #session.hub#
    order by topic_name
   </cfquery>

   <cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from lens
    order by lens_name
   </cfquery>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Feed</td><td class="feed_option" align=right><a href="index.cfm">Return</td></tr>
		</table>

       <form action="save_edit.cfm" method="post" enctype="multipart/form-data" >

        <table cellspacing=0 cellpadding=0 border=0>

  		<tr><td>&nbsp;</td></tr>

          <tr><td class="feed_option"><b>Organization</b></td></tr>

          <cfoutput>

          <tr><td><input type="text" name="feed_organization" style="font-family: arial; font-size: 12px; width: 285px" value="#feed.feed_organization#"></td></tr>

          <tr><td>&nbsp;</td></tr>

          <tr><td class="feed_option"><b>Title</b></td></tr>
          <tr><td><input type="text" name="feed_title" style="font-family: arial; font-size: 12px; width: 785px" maxlength="300" value="#feed.feed_title#" required></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Summary (will show in the Feed)</b></td></tr>

          <tr><td><textarea name="feed_desc" style="font-family: arial; font-size: 12px; width: 785px; height: 100px;" required>#feed.feed_desc#</textarea></td></tr>
          <tr><td>&nbsp;</td></tr>

        		<tr><td height=10>&nbsp;</td></tr>
         		<tr><td colspan=5 class="feed_option"><b>Full Description (will show in the detail page)</b></td></tr>
          		<tr><td colspan=5><textarea name="feed_desc_long" style="font-family: arial; font-size: 12px; width: 785px; height: 200px;">#feed.feed_desc_long#</textarea></td></tr>
        		<tr><td height=10>&nbsp;</td></tr>
   				<tr><td class="feed_option"><b>URL</b></td></tr>
         		<tr><td colspan=5><input type="url" name="feed_URL" style="font-family: arial; font-size: 12px; width: 785px" maxlength="200" value="#feed.feed_url#"></td></tr>
       			<tr><td height=10>&nbsp;</td></tr>

          <input type="hidden" name="feed_id" value=#feed.feed_id#>

          <tr><td colspan=2>

          </cfoutput>

  			<table cellspacing=0 cellpadding=0 border=0>

          <tr><td>&nbsp;</td></tr>

            <tr>
  			    <td class="feed_option" style="padding-left: 0px; padding-bottom: 10px;"><b>Feed Categories</b></td>
  			    <td class="feed_option" style="padding-left: 0px; padding-bottom: 10px;"><b>Markets</b></td>
  				<td class="feed_option" style="padding-left: 3px; padding-bottom: 10px;"><b>Sectors</b></td>
  				<td class="feed_option" style="padding-left: 3px; padding-bottom: 10px;"><b>Capabilities & Services</b></td>
  				</tr>

				 <td width=210>
					  <select style="font-size: 12px; width: 190px; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_lens_id" multiple required>
 					   <cfoutput query="lens">
						<option value=#lens_id# <cfif listfind(feed.feed_lens_id,lens_id)>selected</cfif>>#lens_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=140>
					  <select style="font-size: 12px; width: 120px; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_market_id" multiple>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(feed.feed_market_id,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				 </td>

				 <td width=160>
					  <select style="font-size: 12px; width: 150x; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_sector_id" multiple>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(feed.feed_sector_id,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select style="font-size: 12px; width: 180px; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_topic_id" multiple>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(feed.feed_topic_id,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

                <tr><td>&nbsp;</td></tr>

				<cfif #feed.feed_attachment# is "">
				  <tr><td class="feed_option"><b>Attachment</b></td></tr>
				  <tr><td height=5></td></tr>
				  <tr><td><input type="file" name="feed_attachment"></td></tr>
				<cfelse>
				  <tr><td class="feed_option"><b>Attachment</b></td></tr>
				  <tr><td height=5></td></tr>
				  <tr><td class="feed_option">View Current Attachment: <cfoutput><a href="#media_virtual#/#feed.feed_attachment#" target="_blank" rel="noopener" rel="noreferrer">#feed.feed_attachment#</a></cfoutput></td></tr>
				  <tr><td height=10></td></tr>
				  <tr><td><input type="file" name="feed_attachment"></td></tr>
				  <tr><td height=10></td></tr>
				  <tr><td class="feed_option"><input type="checkbox" name="remove_attachment">&nbsp;or, check to remove attachment</td></tr>
				 </cfif>

				<tr><td height=10></td></tr>

				<tr><td colspan=2 class="feed_option"><b>Status</b>&nbsp;&nbsp;
				<select name="feed_status">
				<option value=0 <cfif #feed.feed_status# is 0>selected</cfif>>Pending
				<option value=1 <cfif #feed.feed_status# is 1>selected</cfif>>Live
				</select>

				</td></tr>

                <tr><td height=10></td></tr>
   		   		<tr><td colspan=5>
   		   		<input class="button_blue" type="submit" name="button" value="Save" vspace=10>&nbsp;&nbsp;
   		   		<input class="button_blue" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Feed?\r\nAre you sure you want to delete this feed?');">
   		   		</td></tr>

                </form>

             </table>

             </td></tr>

</table>

</td></tr>
</table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>