<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Feed</td><td class="feed_option" align=right><a href="index.cfm">Return</td></tr>
		</table>

<!-- Get Lookups -->

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

   <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from market
    where market_hub_id = #session.hub#
   </cfquery>

   <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from sector
    where sector_hub_id = #session.hub#
    order by sector_name
   </cfquery>

   <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from topic
    where topic_hub_id = #session.hub#
    order by topic_name
   </cfquery>

   <cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from lens
    order by lens_name
   </cfquery>

   <cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from usr
    where usr_id = #session.usr_id#
   </cfquery>

        <form action="save.cfm" method="post" enctype="multipart/form-data" >

        <table cellspacing=0 cellpadding=0 border=0>

  		<tr><td>&nbsp;</td></tr>

          <tr><td class="feed_option"><b>Organization</b></td></tr>
          <tr><td><input type="text" name="feed_organization" style="font-family: arial; font-size: 12px; width: 285px" maxlength="250"></td></tr>

          <tr><td>&nbsp;</td></tr>

          <tr><td class="feed_option"><b>Title</b></td></tr>
          <tr><td><input type="text" name="feed_title" style="font-family: arial; font-size: 12px; width: 785px" maxlength="250" required></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td class="feed_option"><b>Summary (will show in the Feed)</b></td></tr>
          <tr><td><textarea name="feed_desc" style="font-family: arial; font-size: 12px; width: 785px; height: 100px;" required></textarea></td></tr>
          <tr><td>&nbsp;</td></tr>

        			<tr><td height=10>&nbsp;</td></tr>
         			<tr><td colspan=5 class="feed_option"><b>Full Description (will show in the detail page)</b></td></tr>
          		<tr><td colspan=5><textarea name="feed_desc_long" style="font-family: arial; font-size: 12px; width: 785px; height: 200px;"></textarea></td></tr>
        			<tr><td height=10>&nbsp;</td></tr>
         			<tr><td colspan=5 class="feed_option"><b>Recommendation (will show in the detail page)</b></td></tr>
           		<tr><td colspan=5><textarea name="feed_value_1" style="font-family: arial; font-size: 12px; width: 785px; height: 100px;"></textarea></td></tr>
       			<tr><td height=10>&nbsp;</td></tr>
   				<tr><td class="feed_option"><b>URL</b></td></tr>
         			<tr><td colspan=5><input type="url" name="feed_url" style="font-family: arial; font-size: 12px; width: 785px" maxlength="200"></td></tr>
       			<tr><td height=10>&nbsp;</td></tr>

          <tr><td colspan=2>

  			<table cellspacing=0 cellpadding=0 border=0>

            <tr>
  			    <td class="feed_option" style="padding-left: 0px; padding-bottom: 10px;"><b>Feed Categories</b></td>
  			    <td class="feed_option" style="padding-left: 0px; padding-bottom: 10px;"><b>Markets</b></td>
  				<td class="feed_option" style="padding-left: 3px; padding-bottom: 10px;"><b>Sectors</b></td>
  				<td class="feed_option" style="padding-left: 3px; padding-bottom: 10px;"><b>Capabilities & Services</b></td>
  				</tr>
  			<tr>

  				 <td width=210>
  					  <select style="font-size: 12px; width: 190px; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_lens_id" multiple required>
   					   <cfoutput query="lens">
  						<option value=#lens_id# <cfif #lens_id# is 1>selected</cfif>>#lens_name#
  					   </cfoutput>
  					  </select>

  				</td>

			 <td width=140>
  					  <select style="font-size: 12px; width: 120px; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_market_id" multiple>
  					   <cfoutput query="market">
  						<option value=#market_id#>#market_name#
  					   </cfoutput>
  					  </select>

  				</td>

  				 <td width=160>
  					  <select style="font-size: 12px; width: 150x; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_sector_id" multiple>
  					   <cfoutput query="sector">
  						<option value=#sector_id#>#sector_name#
  					   </cfoutput>
  					  </select>

  				</td>

  				<td>
  					  <select style="font-size: 12px; width: 180px; height: 160px; padding-left: 10px; padding-top: 5px;" name="feed_topic_id" multiple>
  					   <cfoutput query="topic">
  						<option value=#topic_id#>#topic_name#
  					   </cfoutput>
  					  </select>

  				</td>

  				</tr>

				<tr><td>&nbsp;</td></tr>
				<tr><td class="feed_option"><b>Attachment</b></td></tr>
				<tr><td height=10></td></tr>
			    <tr><td><input type="file" name="feed_attachment"></td></tr>
                <tr><td>&nbsp;</td></tr>
				<tr><td colspan=2 class="feed_option"><b>Status</b>&nbsp;&nbsp;
				<select name="feed_status">
				<option value=0 selected>Pending
				<option value=1>Live
				</select>

				</td></tr>

   		   		<tr><td colspan=5>
   		   		<input class="button_blue" type="submit" name="button" value="Save" vspace=10>
   		   		</td></tr>

                </form>
</table>

</td></tr>
</table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>