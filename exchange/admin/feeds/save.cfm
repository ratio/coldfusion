<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

		<!--- Find Matching Users --->

		<cfquery name="matching" datasource="#client_datasource#" username="#client_username#" password="#client_password#">

		 select align_usr_id from align
		 where align_hub_id is null and
		       align_type_id = 1 and align_type_value in (#feed_lens_id#)
         union
		 select align_usr_id from align
		 where align_hub_id is null and
		       align_type_id = 2 and align_type_value in (#feed_market_id#)
         union
		 select align_usr_id from align
		 where align_hub_id is null and
		       align_type_id = 3 and align_type_value in (#feed_sector_id#)
         union
		 select align_usr_id from align
		 where align_hub_id is null and
		       align_type_id = 4 and align_type_value in (#feed_topic_id#)

		</cfquery>

		<cfset #user_list# = #valuelist(matching.align_usr_id)#>

	<cfif #feed_attachment# is not "">
		<cffile action = "upload"
		 fileField = "feed_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into feed
	 (feed_lens_id, feed_market_id, feed_sector_id, feed_topic_id, feed_value_1, feed_type_id, feed_posted_by_usr_id, feed_posted_by_company_id, feed_posted_by_hub_id, feed_created, feed_updated, feed_title, feed_desc, feed_desc_long, feed_organization, feed_status, feed_url, feed_attachment)
	 values
	 ('#feed_lens_id#','#feed_market_id#','#feed_sector_id#','#feed_topic_id#','#feed_value_1#', 2, #session.usr_id#,#session.company_id#, #session.hub#,#now()#,#now()#,'#feed_title#','#feed_desc#','#feed_desc_long#','#feed_organization#',#feed_status#,'#feed_url#',<cfif #feed_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>)
	</cfquery>

	<cfquery name="feedmax" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(feed_id) as id from feed
	</cfquery>

	<cfloop index="element" list=#user_list#>
	        <cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
             insert into feed_usr
             (feed_usr_feed_id, feed_usr_usr_id, feed_usr_created)
             values
             (#feedmax.id#,#element#,#now()#)
            </cfquery>
	</cfloop>

<cflocation URL="/exchange/hubs/admin/feeds/index.cfm?u=1" addtoken="no">

</cfif>