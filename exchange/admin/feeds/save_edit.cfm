<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Cancel">

 <cflocation URL="/exchange/hubs/admin/feeds/" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete feed
	 where feed_id = #feed_id# and
	 feed_posted_by_hub_id = #session.hub#
	</cfquery>

	<cfquery name="delete_feed_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete feed_usr
	 where feed_usr_feed_id = #feed_id#
	</cfquery>

	<cflocation URL="/exchange/hubs/admin/feeds/index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Save">

<cftransaction>

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select feed_attachment from feed
		  where feed_id = #feed_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.feed_attachment#">

	</cfif>

	<cfif #feed_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select feed_attachment from feed
		  where feed_id = #feed_id#
		</cfquery>

		<cfif #getfile.feed_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.feed_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "feed_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update feed
	 set feed_organization = '#feed_organization#',
		 feed_title = '#feed_title#',
		 feed_desc = '#feed_desc#',
		  <cfif #feed_attachment# is not "">
		   feed_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   feed_attachment = null,
		  </cfif>

		 feed_updated = #now()#,
		 feed_desc_long = '#feed_desc_long#',
		 feed_url = '#feed_url#',
		 feed_lens_id = '#feed_lens_id#',
		 feed_market_id = '#feed_market_id#',
		 feed_sector_id = '#feed_sector_id#',
		 feed_topic_id = '#feed_topic_id#',
		 feed_status = #feed_status#
	 where feed_id = #feed_id#
	</cfquery>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete feed_usr
	 where feed_usr_feed_id = #feed_id#
	</cfquery>

	<!--- Find Matching Users --->

	<cfquery name="matching" datasource="#client_datasource#" username="#client_username#" password="#client_password#">

	 select align_usr_id from align
	 where align_hub_id is null and
		   align_type_id = 1 and align_type_value in (#feed_lens_id#)
	 union
	 select align_usr_id from align
	 where align_hub_id is null and
		   align_type_id = 2 and align_type_value in (#feed_market_id#)
	 union
	 select align_usr_id from align
	 where align_hub_id is null and
		   align_type_id = 3 and align_type_value in (#feed_sector_id#)
	 union
	 select align_usr_id from align
	 where align_hub_id is null and
		   align_type_id = 4 and align_type_value in (#feed_topic_id#)

	</cfquery>

	<cfset #user_list# = #valuelist(matching.align_usr_id)#>

	<cfloop index="element" list=#user_list#>
			<cfquery name="insert_feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into feed_usr
			 (feed_usr_feed_id, feed_usr_usr_id, feed_usr_created)
			 values
			 (#feed_id#,#element#,#now()#)
			</cfquery>
	</cfloop>

<cftransaction>

	<cflocation URL="/exchange/hubs/admin/feeds/index.cfm?u=2" addtoken="no">

</cfif>