<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comments_rating
 where comments_rating_id = #comments_rating_id# and
       comments_rating_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Comment Rating</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header" width=100>Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="comments_rating_value" value=#edit.comments_rating_value# step=1 required></td></tr>

		   <tr><td class="feed_sub_header" width=100>Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="comments_rating_name" value="#edit.comments_rating_name#" maxlength="50" required></td></tr>

			<tr><td class="feed_sub_header" valign=top>Image</td>
				<td class="feed_sub_header" style="font-weight: normal;">

				<cfif #edit.comments_rating_image# is "">
				  <input type="file" name="comments_rating_image" id="image" onchange="validate_img()">
				<cfelse>
				  <img src="#media_virtual#/#edit.comments_rating_image#" width=100><br><br>
				  <input type="file" name="comments_rating_image" id="image" onchange="validate_img()"><br><br>
				  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
				 </cfif>

				 </td></tr>

           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>

		   <input type="hidden" name="comments_rating_id" value=#comments_rating_id#>

		   </cfoutput>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

