<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #comments_rating_image# is not "">
		<cffile action = "upload"
		 fileField = "comments_rating_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into comments_rating (comments_rating_name, comments_rating_hub_id, comments_rating_value, comments_rating_image)
	 values ('#comments_rating_name#',#session.hub#,#comments_rating_value#,<cfif #comments_rating_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comments_rating_image from comments_rating
		  where comments_rating_id = #comments_rating_id# and
		        comments_rating_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.comments_rating_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.comments_rating_image#">
		</cfif>

	</cfif>

	<cfif comments_rating_image is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comments_rating_image from comments_rating
		  where comments_rating_id = #comments_rating_id# and
		        comments_rating_hub_id = #session.hub#
		</cfquery>

		<cfif #getfile.comments_rating_image# is not "">
         <cfif fileexists("#media_path#\#getfile.comments_rating_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.comments_rating_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "comments_rating_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update comments_rating
	 set comments_rating_name = '#comments_rating_name#',

		  <cfif #comments_rating_image# is not "">
		   comments_rating_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   comments_rating_image = null,
		  </cfif>

	     comments_rating_value = #comments_rating_value#

	 where comments_rating_id = #comments_rating_id# and
	       comments_rating_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comments_rating_image from comments_rating
		  where comments_rating_id = #comments_rating_id# and
		        comments_rating_hub_id = #session.hub#
		</cfquery>

		<cfif remove.comments_rating_image is not "">
         <cfif fileexists("#media_path#\#remove.comments_rating_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.comments_rating_image#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete comments_rating
	 where comments_rating_id = #comments_rating_id# and
	       comments_rating_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

