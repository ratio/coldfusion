<cfinclude template="/exchange/security/check.cfm">

<cfquery name="comments_rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comments_rating
  where comments_rating_hub_id = #session.hub#
  order by comments_rating_value DESC
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Comment Ratings</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Admin Tools</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Comment Rating has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Comment Rating has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Comment Rating has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Comment Rating</a></td></tr>

      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #comments_rating.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Comment Ratings exist.</td></tr>

        <cfelse>
        <tr>
				<td class="feed_sub_header" width=100>Value</b></td>
				<td class="feed_sub_header">Name</b></td>
				<td class="feed_sub_header" width=300 align=right>Image</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="comments_rating">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=middle width=5%><a href="edit.cfm?comments_rating_id=#comments_rating_id#">#comments_rating_value#</a></td>
              <td class="feed_sub_header" valign=middle><a href="edit.cfm?comments_rating_id=#comments_rating_id#">#comments_rating_name#</a></td>
              <td class="feed_sub_header" align=right valign=middle>

              <cfif comments_rating_image is "">
               No Image Selected
              <cfelse>
              <img src="#media_virtual#/#comments_rating_image#" height=20>
              </cfif>



              </td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

