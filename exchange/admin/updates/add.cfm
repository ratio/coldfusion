<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header" colspan=2>Add Update</td></tr>
		   <tr><td>&nbsp;</td></tr>

			   <tr><td class="feed_option" width=15%><b>Name</b></td></tr>
			   <tr><td class="feed_option"><input type="text" name="exchange_update_name" size=50></td></tr>
			   <tr><td class="feed_option"><b>Short Description</b></td></tr>
			   <tr><td class="feed_option"><textarea name="exchange_update_desc_short" rows=3 cols=120></textarea></td></tr>
			   <tr><td class="feed_option"><b>Long Description</b></td></tr>
			   <tr><td class="feed_option"><textarea name="exchange_update_desc_long" rows=15 cols=150></textarea></td></tr>
			   <tr><td class="feed_option" width=15%><b>More Information URL</b></td></tr>
			   <tr><td class="feed_option"><input type="url" name="exchange_update_url" size=100></td></tr>
               <tr><td height=10></td></tr>
               <tr><td class="feed_option" valign=top><b>Attachment</b></td></tr>
               <tr><td class="feed_option" style="font-weight: normal;"><input type="file" name="exchange_update_attachment"></td></tr>
               <tr><td height=10></td></tr>
			   <tr><td class="feed_option" width=15%><b>Order</b></td></tr>
			   <tr><td class="feed_option"><input type="number" name="exchange_update_order" step=".1" required></td></tr>
			   <tr><td class="feed_option"><b>Active</b>&nbsp;(will be displayed on home page)</td></tr>
			   <tr><td class="feed_option">
			   <select name="exchange_update_active">
			    <option value=0>No
			    <option value=1>Yes
			   </select></td></tr>


			   <tr><td height=5></td></tr>

			   <tr><td><input class="button_blue" type="submit" name="button" value="Save" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>


      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

