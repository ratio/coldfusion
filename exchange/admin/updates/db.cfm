<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

    <cfif #exchange_update_attachment# is not "">
		<cffile action = "upload"
		 fileField = "exchange_update_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      insert into exchange_update
      (exchange_update_attachment, exchange_update_hub_id, exchange_update_url, exchange_update_order, exchange_update_name, exchange_update_desc_short, exchange_update_desc_long, exchange_update_date, exchange_update_active)
      values
      (<cfif #exchange_update_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,'#exchange_update_url#', #exchange_update_order#,'#exchange_update_name#','#exchange_update_desc_short#','#exchange_update_desc_long#',#now()#,#exchange_update_active#)
	</cfquery>

<cfelseif #button# is "Update">


	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select exchange_update_attachment from exchange_update
		  where exchange_update_id = #exchange_update_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.exchange_update_attachment#">

	</cfif>

	<cfif #exchange_update_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select exchange_update_attachment from exchange_update
		  where exchange_update_id = #exchange_update_id#
		</cfquery>

		<cfif #getfile.exchange_update_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.exchange_update_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "exchange_update_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update exchange_update
	 set exchange_update_name = '#exchange_update_name#',
		  <cfif #exchange_update_attachment# is not "">
		   exchange_update_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   exchange_update_attachment = null,
		  </cfif>
	     exchange_update_url = '#exchange_update_url#',
	     exchange_update_order = #exchange_update_order#,
	     exchange_update_hub_id = <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	     exchange_update_desc_short = '#exchange_update_desc_short#',
	     exchange_update_desc_long = '#exchange_update_desc_long#',
	     exchange_update_date = #now()#,
	     exchange_update_active = #exchange_update_active#
	 where exchange_update_id = #exchange_update_id#
    </cfquery>

<cfelseif #button# is "Delete">

  <cftransaction>

	 <cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select exchange_update_attachment from exchange_update
	   where exchange_update_id = #exchange_update_id#
	 </cfquery>

	 <cfif remove.exchange_update_attachment is not "">
	  <cffile action = "delete" file = "#media_path#\#remove.exchange_update_attachment#">
	 </cfif>

	 <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete exchange_update
	  where exchange_update_id = #exchange_update_id#
	 </cfquery>

  </cftransaction>

</cfif>

<cflocation URL="index.cfm" addtoken="no">