<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into topic (topic_name, topic_hub_id)
	 values ('#topic_name#',#session.hub#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update topic
	 set topic_name = '#topic_name#'
	 where topic_id = #topic_id#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete topic
	 where topic_id = #topic_id#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

