<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template = "/exchange/include/header.cfm">
  <cfinclude template="/exchange/hubs/admin/hub_profile.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	    <!--- check to see if the user has registered with the Exchange --->

		<cfquery name="check_exchange" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from usr
		 where usr_email = '#email#'
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">Invite Member</td>
		     <td class="feed_option" align=right><a href="members.cfm">Return</a></td></tr>
		 <tr><td height=10></td></tr>

			<cfif isdefined("ec")>
			 <tr><td class="feed_option"><font color="red"><b>You must first select a company before migrating information to the Exchange.</b></td></tr>
		     <tr><td height=10></td></tr>
        	</cfif>


         <tr><td class="feed_option"><b>Email Address:</b>&nbsp;<cfoutput>#email#</cfoutput></td></tr>
         <tr><td height=10></td></tr>

         <cfif #check_exchange.recordcount# is 1>

            <!--- check to see if the user is a current member of the Hub --->

			<cfquery name="hub_exchange" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from hub_xref
			 where hub_xref_usr_id = #check_exchange.usr_id# and
			       hub_xref_hub_id = #session.hub#
			</cfquery>

			<cfoutput>

				<cfif #hub_exchange.recordcount# is 1>
				 <tr><td class="feed_option">The email address you entered is already a registered member of this HUB.</td></tr>
				<cfelse>
				 <tr><td class="feed_option">The email address you entered is already registered with the Exchange.  <a href="exchange.cfm?email=#email#"><b>Click here</b></a> to invite them to this HUB.</td></tr>
				</cfif>

			</cfoutput>

         <cfelse>

			<cfquery name="hub_sam" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from sams
			 where (poc_email = '#email#' or alt_poc_email = '#email#' or pp_poc_email = '#email#' or elec_bus_poc_email = '#email#')
			</cfquery>

            <cfif hub_sam.recordcount is 0>
             <tr><td class="feed_option">The email address you entered was not found in the Exchange or any other sources of information we have.  <a href="new_user.cfm">Click here</a> to add and invite the user to this HUB.</td></tr>
            <cfelse>
             <tr><td class="feed_option">The email address you entered was not found in the Exchange.  However, we did find the email address associated with the below company(ies) in <a href="http://www.sam.gov" target="_blank" rel="noopener" rel="noreferrer">SAM.Gov</a>.</td></tr>
             <tr><td>

			 <form action="sam.cfm" method="post">

             <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=10></td></tr>
				 <tr>
					 <td width=30>&nbsp;</td>
					 <td class="feed_option"><b>Company Name</b></td>
					 <td class="feed_option"><b>Location</b></td>
					 <td class="feed_option"><b>DUNS</b></td>
					 <td class="feed_option"><b>Point of Contact</b></td>
					 <td class="feed_option"><b>Location</b></td>
					 <td width=30%>&nbsp;</td>
				 </tr>
				 <cfoutput>

				 <tr><td><input type="radio" name="company"></td>
					 <td class="feed_option"><a href="/exchange/include/company_profile.cfm?id=0&duns=#hub_sam.duns#" target="_blank" rel="noopener" rel="noreferrer">#hub_sam.legal_business_name#</a></td>
					 <td class="feed_option">#hub_sam.city_2#, #hub_sam.state_2#</td>
					 <td class="feed_option">#hub_sam.duns#</td>
					 <td class="feed_option">#hub_sam.poc_fnme# #hub_sam.poc_lname# (#hub_sam.poc_title#)</td>
					 <td class="feed_option">#hub_sam.poc_city#, #hub_sam.state_3#</td>
				 </tr>

				 <tr><td height=10></td></tr>

	             <tr><td colspan=8 class="feed_option">By selecting a company we will migrate the companies information over to the Exchange so it will be available to them upon acceptance to your invitation.  If this isn't the company you wanted, click Create New Member.</td></tr>

                 <tr><td>&nbsp;</td></tr>

					 <tr>

						 <td colspan=6>

						 <input class="button_blue" style="font-size: 11px; height: 24px; width: 100px;" type="submit" name="button" value="Select Company">&nbsp;
						 <input class="button_blue" style="font-size: 11px; height: 24px; width: 125px;" type="submit" name="button" value="Create New Member">&nbsp;

						 </td>
						 </tr>

			     <input type="hidden" name="duns" value="#hub_sam.duns#">
			     <input type="hidden" name="email" value="#email#">

 				 </cfoutput>

                </form>

             </table>

             </td></tr>


            </cfif>

         </cfif>

       </table>



	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>