<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title>Exchange</title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template="/exchange/include/header.cfm">

	<cfquery name="info" datasource="#datasource#" username="#username#" password="#password#">
	  select * from exchange_update
	  where exchange_update_id = #exchange_update_id#
	</cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

	  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header" colspan=2>Edit Update</td></tr>
		   <tr><td>&nbsp;</td></tr>

			   <tr><td class="feed_option" width=15%><b>Name</b></td></tr>
			   <tr><td class="feed_option"><input type="text" name="exchange_update_name" size=50 value="#info.exchange_update_name#"></td></tr>
			   <tr><td class="feed_option"><b>Short Description</b></td></tr>
			   <tr><td class="feed_option"><textarea name="exchange_update_desc_short" rows=3 cols=120>#info.exchange_update_desc_short#</textarea></td></tr>
			   <tr><td class="feed_option"><b>Long Description</b></td></tr>
			   <tr><td class="feed_option"><textarea name="exchange_update_desc_long" rows=20 cols=150>#info.exchange_update_desc_long#</textarea></td></tr>
			   <tr><td class="feed_option" width=15%><b>More Information URL</b></td></tr>
			   <tr><td class="feed_option"><input type="url" name="exchange_update_url" size=100 value="#info.exchange_update_url#"></td></tr>

                <tr><td height=10></td></tr>

                <tr><td class="feed_option" valign=top><b>Attachment</b></td></tr>
                <tr><td class="feed_option">

					<cfif #info.exchange_update_attachment# is "">
					  <input type="file" name="exchange_update_attachment">
					<cfelse>
					  <b>Current Attachment:</b>&nbsp;&nbsp;&nbsp;<a href="#media_virtual#/#info.exchange_update_attachment#" target="_blank" rel="noopener" rel="noreferrer"><u>#info.exchange_update_attachment#</u></a><br><br>
					  <input type="file" name="exchange_update_attachment"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove attachment
					 </cfif>

                     </td></tr>

			   <tr><td class="feed_option" width=15%><b>Order</b></td></tr>
			   <tr><td class="feed_option"><input type="number" name="exchange_update_order" step=".1" value="#info.exchange_update_order#" required></td></tr>
			   <tr><td class="feed_option"><b>Active</b>&nbsp;(will be displayed on home page)</td></tr>
			   <tr><td class="feed_option">
			   <select name="exchange_update_active">
			    <option value=0 <cfif #info.exchange_update_active# is 0>selected</cfif>>No
			    <option value=1 <cfif #info.exchange_update_active# is 1>selected</cfif>>Yes
			   </select></td></tr>

			   <tr><td height=5></td></tr>

			   <tr><td>

			   <input class="button_blue" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
			   <input class="button_blue" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Update?\r\nAre you sure you want to delete this update?');">

		       <input type="hidden" name="exchange_update_id" value=#exchange_update_id#>

			   </td></tr>
		  </table>

		  </cfoutput>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

