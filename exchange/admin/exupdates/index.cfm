<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="info" datasource="#datasource#" username="#username#" password="#password#">
	  select * from exchange_update
	  where exchange_update_hub_id = #session.hub#
	  order by exchange_update_date DESC
	</cfquery>

<html>
<head>
	<title>Exchange</title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Updates</td></tr>
	   <tr><td>&nbsp;</td></tr>
	   <tr><td class="feed_option">
	   <b>
		  <a href="add.cfm">Add Update</a>
	   </b>
	   </td></tr>
	   <tr><td>&nbsp;</td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #info.recordcount# is 0>

        <tr><td class="feed_option"><b>No Updates exist.</b></td></tr>

        <cfelse>

			<tr>
			    <td class="feed_option" width=50><b>Order</b></td>
			    <td class="feed_option" width=300><b>Name</b></td>
			    <td class="feed_option"><b>Short Description</b></td>
				<td class="feed_option" align=right width=150><b>Updated</b></td>
				<td class="feed_option" align=right width=75><b>Active</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="info">

			  <cfif #counter# is 0>
			   <tr bgcolor="ffffff">
			  <cfelse>
			   <tr bgcolor="f0f0f0">
			  </cfif>

				  <td class="feed_option" valign=top>#exchange_update_order#</td>
				  <td class="feed_option" valign=top><a href="edit.cfm?exchange_update_id=#exchange_update_id#"><b>#exchange_update_name#</b></a></td>
				  <td class="feed_option" valign=top>#exchange_update_desc_short#</td>
				  <td class="feed_option" valign=top align=right>#dateformat(exchange_update_date,'mm/dd/yyyy')# at #timeformat(exchange_update_date)#</td>
				  <td class="feed_option" valign=top align=right><cfif #exchange_update_active# is 0>No<cfelse>Yes</cfif></td>
			  </tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

		 </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

