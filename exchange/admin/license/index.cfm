<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="license" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_order
 left join license on license_id = hub_order_license_id
 where hub_order_hub_id = #session.hub# and
 hub_order_active = 1
</cfquery>

<cfquery name="active" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(hub_xref_id) as total from hub_xref
 where hub_xref_hub_id = #session.hub# and
       hub_xref_active = 1
</cfquery>

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(portfolio_id) as total from portfolio
 where portfolio_hub_id = #session.hub#
</cfquery>

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(pipeline_id) as total from pipeline
 where pipeline_hub_id = #session.hub#
</cfquery>

<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(market_report_id) as total from market_report
 where market_report_hub_id = #session.hub#
</cfquery>

<cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(deal_id) as total from deal
 where deal_hub_id = #session.hub#
</cfquery>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">License Management</td>
			   <td class="feed_sub_header" align=right>

			   <a href="/exchange/admin/logs/">Usage Log</a>

			   &nbsp;|&nbsp;

			   <a href="/exchange/admin/">Settings</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>


               <tr><td class="feed_header">License Terms</td></tr>
               <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" width=150>Version</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#license.license_name#</td></tr>

			   <tr><td class="feed_sub_header" width=150>Term / Duration</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#dateformat(license.hub_order_start_date,'mm/dd/yyyy')# - #dateformat(license.hub_order_end_date,'mm/dd/yyyy')#</td></tr>

			   <tr><td></td>
			       <td class="feed_sub_header" style="padding-top: 0px;">

				   <b>#datediff('d',now(),license.hub_order_end_date)# Days Remaining</b>

				   </td></tr>

			   <tr><td class="feed_sub_header" width=150>User Limit</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#numberformat(license.hub_order_users,'99,999')#</td></tr>

			   <tr><td class="feed_sub_header" width=150 style="padding-top: 0px;">Active Users</td>
				   <td class="feed_sub_header" style="padding-top: 0px; font-weight: normal;">#numberformat(active.total,'99,999')#</td></tr>

			   <tr><td class="feed_sub_header" width=150 style="padding-top: 0px;">Available</td>
				   <td class="feed_sub_header" style="padding-top:0px; font-weight: normal;">#numberformat(evaluate(license.hub_order_users - active.total),'99,999')#</td></tr>

		   </table>

		   </td><td width=50>&nbsp;</td><td valign=top width=60%>


		   <table cellspacing=0 cellpadding=0 border=0 width=100%>


               <tr><td class="feed_header">Limits & Thresholds</td></tr>
               <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" width=30%>App</td>
				   <td class="feed_sub_header" width=20% align=center>Maximum</td>
				   <td class="feed_sub_header" width=20% align=center>Used</td>
				   <td class="feed_sub_header" width=20% align=center>Available</td>
				   <td class="feed_sub_header" width=10% align=right>Status</td>
				   </tr>

			   <tr><td class="feed_sub_header" width=150>Company Portfolios</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(license.hub_order_limit_portfolios,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(portfolios.total,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(evaluate(license.hub_order_limit_portfolios - portfolios.total),'999,999')#</td>

                   <td class="feed_sub_header" align=right>
				   <cfif #evaluate(license.hub_order_limit_portfolios - portfolios.total)# LT 0>
				   	<font color="red">Upgrade</font>
				   <cfelse>
				   	<font color="green">Good</font>
				   </cfif>

				   </td>



				   </tr>

			   <tr bgcolor="e0e0e0"><td class="feed_sub_header" width=150>Opportuity Boards</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(license.hub_order_limit_boards,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(boards.total,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(evaluate(license.hub_order_limit_boards - boards.total),'999,999')#</td>

                   <td class="feed_sub_header" align=right>
				   <cfif #evaluate(license.hub_order_limit_boards - boards.total)# LT 0>
				   	<font color="red">Upgrade</font>
				   <cfelse>
				   	<font color="green">Good</font>
				   </cfif>

				   </td>



				   </tr>

			   <tr><td class="feed_sub_header" width=150>Market Reports</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(license.hub_order_limit_reports,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(reports.total,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(evaluate(license.hub_order_limit_reports - reports.total),'999,999')#</td>
                   <td class="feed_sub_header" align=right>
				   <cfif #evaluate(license.hub_order_limit_reports - reports.total)# LT 0>
				   	<font color="red">Upgrade</font>
				   <cfelse>
				   	<font color="green">Good</font>
				   </cfif>

				   </td>

				   </tr>

			   <tr bgcolor="e0e0e0"><td class="feed_sub_header" width=150>Deals</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(license.hub_order_limit_deals,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(deals.total,'999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center><b>#numberformat(evaluate(license.hub_order_limit_deals - deals.total),'999,999')#</b></td>
                   <td class="feed_sub_header" align=right>
				   <cfif #evaluate(license.hub_order_limit_deals - deals.total)# LT 0>
				   	<font color="red">Upgrade</font>
				   <cfelse>
				   	<font color="green">Good</font>
				   </cfif>

				   </td></tr>

		      </table>

		   </td></tr>

		   </table>

       </cfoutput>

<!--- Profiles --->

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub#
  order by hub_role_name
</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=20></td></tr>
          <tr><td><hr></td></tr>
          <tr><td height=10></td></tr>
          <tr><td class="feed_header"><a href="/exchange/admin/profiles/">Roles Summary</a></td></tr>
        </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #profiles.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Roles exist.</td></tr>

        <cfelse>

        <tr>
				<td class="feed_sub_header" colspan=5></td>
				<td class="feed_sub_header" colspan=2 align=center>Price Per User</b></td>
				<td class="feed_sub_header" colspan=2 align=center>Exchange Fee</b></td>
				<td></td>
        </tr>

        <tr>
				<td class="feed_sub_header" width=300>Role Name</b></td>
				<td class="feed_sub_header" align=center>Active Users</b></td>
				<td class="feed_sub_header" align=center>Apps Used</b></td>
				<td class="feed_sub_header" align=center>Allow Trial?</b></td>
				<td class="feed_sub_header" align=center>Duration</b></td>
				<td class="feed_sub_header" align=center>Monthly</b></td>
				<td class="feed_sub_header" align=center>Annually</b></td>
				<td class="feed_sub_header" align=center>Monthly</b></td>
				<td class="feed_sub_header" align=center>Annual</b></td>
				<td class="feed_sub_header" align=right></b></td>

			</tr>

        </cfif>

        <cfset counter = 0>

         <cfset app_cost_monthly = 0>
         <cfset app_cost_annual = 0>
         <cfset app_fee_monthly = 0>
         <cfset app_fee_annual = 0>

         <cfloop query="profiles">

		<cfquery name="menu" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select app_id from menu
          join app on app_id = menu_app_id
		  where menu_role_id = #profiles.hub_role_id#
		</cfquery>

		<cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select count(app_id) as total from menu
          join app on app_id = menu_app_id
		  where menu_role_id = #profiles.hub_role_id#
		</cfquery>

		<cfquery name="cost" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select sum(app_cost_broker_monthly) as monthly, sum(app_cost_broker_annual) as annual from menu
          join app on app_id = menu_app_id
		  where menu_role_id = #profiles.hub_role_id#
		</cfquery>

		<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select count(hub_xref_id) as total from hub_xref
		  where hub_xref_role_id = #profiles.hub_role_id# and
		        hub_xref_active = 1 and
		        hub_xref_hub_id = #session.hub#
		</cfquery>

         <cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top>#profiles.hub_role_name#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#users.total#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#apps.total#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center><cfif profiles.hub_role_trial is 1>Yes<cfelse>No</cfif></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center><cfif profiles.hub_role_trial_duration GT 0>#profiles.hub_role_trial_duration# Days<cfelse>N/A</cfif></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(profiles.hub_role_monthly,'$999.99')#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(profiles.hub_role_annually,'$999.99')#</td>

              <cfif users.total is "">
               <cfset total_users = 0>
              <cfelse>
               <cfset total_users = users.total>
              </cfif>

              <cfif cost.monthly is "">
               <cfset monthly_cost = 0>
              <cfelse>
               <cfset monthly_cost = cost.monthly>
              </cfif>

              <cfif cost.annual is "">
               <cfset annual_cost = 0>
              <cfelse>
               <cfset annual_cost = cost.annual>
              </cfif>

              <cfset mc = evaluate(monthly_cost*total_users)>
              <cfset ac = evaluate(annual_cost*total_users)>

              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(mc,'$99,999.99')#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#numberformat(ac,'$99,999.99')#</td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          <cfif users.total is "">
           <cfset users_total = 0>
          <cfelse>
           <cfset users_total = users.total>
          </cfif>

          <cfif profiles.hub_role_monthly is "">
           <cfset hrm = 0>
          <cfelse>
           <cfset hrm = profiles.hub_role_monthly>
          </cfif>

          <cfif profiles.hub_role_annually is "">
           <cfset hra = 0>
          <cfelse>
           <cfset hra = profiles.hub_role_annually>
          </cfif>

          <cfif cost.monthly is "">
           <cfset cm = 0>
          <cfelse>
           <cfset cm = cost.monthly>
          </cfif>

          <cfif cost.annual is "">
           <cfset ca = 0>
          <cfelse>
           <cfset ca = cost.annual>
          </cfif>

          <cfset app_cost_monthly = app_cost_monthly + evaluate(users_total * hrm)>
          <cfset app_cost_annual = app_cost_annual + evaluate(users_total * hra)>
          <cfset app_fee_monthly = app_fee_monthly + evaluate(users_total * cm)>
          <cfset app_fee_annual = app_fee_annual + evaluate(users_total * ca)>


         </cfoutput>

         </cfloop>

         <cfoutput>

         <tr>
            <td class="feed_sub_header" colspan=5>Total</td>
            <td class="feed_sub_header" align=center>#numberformat(app_cost_monthly,'$999,999.99')#</td>
            <td class="feed_sub_header" align=center>#numberformat(app_cost_annual,'$999,999.99')#</td>
            <td class="feed_sub_header" align=center>#numberformat(app_fee_monthly,'$999,999.99')#</td>
            <td class="feed_sub_header" align=center>#numberformat(app_fee_annual,'$999,999.99')#</td>
         </tr>

         </cfoutput>

	    </table>


























	   </form>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>