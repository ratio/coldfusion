<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from company
	  left join usr on usr.usr_id = company.company_admin_id
	  where company_name like '#session.comp_name#%'
	  order by company_name
	</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Companies (<cfoutput>#numberformat(info.recordcount,'999,999')#</cfoutput>)</td>
	       <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr> <tr><td>&nbsp;</td></tr>


       <cfif isdefined("u")>
        <cfif u is 3>
         <tr><td class="feed_option"><font color="red"><b>Company has been deleted.</b></font></td></tr>
        <cfelseif u is 1>
         <tr><td class="feed_option"><font color="green"><b>Company has been added.</b></font></td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_option"><font color="green"><b>Company has been updated.</b></font></td></tr>
        </cfif>
       <tr><td>&nbsp;</td></tr>
       </cfif>






      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #info.recordcount# is 0>

        <tr><td class="feed_option"><b>No Companies exist.</b></td></tr>

        <cfelse>

            <tr>
               <td colspan=5>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>
			    <td class="feed_option"><b>Company Name</b></td>
				<td class="feed_option"><b>Website</b></td>
				<td class="feed_option"><b>Admin</b></td>
				<td class="feed_option"><b>Email</b></td>
				<td class="feed_option" align=right><b>Registered</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="info">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_option"><a href="edit.cfm?company_id=#company_id#">#company_name#</a></td>
              <td class="feed_option"><a href="#company_website#" target="_blank" rel="noopener" rel="noreferrer">#company_website#</a></td>
              <td class="feed_option">#usr_last_name#, #usr_first_name#</td>
              <td class="feed_option">#usr_email#</td>
              <td class="feed_option" width=125 align=right><cfif #company_registered# is "">Not Registered<cfelse>#dateformat(company_registered,'mm/dd/yyyy')#</cfif></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

     </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

