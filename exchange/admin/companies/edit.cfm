<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="size" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from size
</cfquery>

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 where company_id = #company_id#
</cfquery>

<cfquery name="entity" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from entity
</cfquery>

<cfquery name="broker_types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from broker_type
 order by broker_type_name
</cfquery>

<cfquery name="broker_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from broker
 where broker_company_id = #company_id#
 <cfif isdefined("session.hub")>
  and broker_hub_id = #session.hub#
 </cfif>
</cfquery>

<cfquery name="alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select xref_comp_broker_broker_type_id from xref_comp_broker
 where xref_comp_broker_company_id = #company_id#
 <cfif isdefined("session.hub")>
  and xref_comp_broker_hub_id = #session.hub#
 <cfelse>
  and xref_comp_broker_hub_id is null
 </cfif>
</cfquery>

<cfif alignments.recordcount is 0>
 <cfset broker_type_list = 0>
<cfelse>
 <cfset broker_type_list = valuelist(alignments.xref_comp_broker_broker_type_id)>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">
	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Company</td>
            <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
        <tr><td>&nbsp;</td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td valign=top>

      <form action="db.cfm" enctype="multipart/form-data" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfoutput>
				<tr><td class="feed_option"><b>Company Name</b></td><td><input style="font-size: 12px; width: 320px;" type="text" name="company_name" value="#company.company_name#" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Address (Line 1)</b></td><td><input style="font-size: 12px; width: 320px;" maxlength="100" type="text" value="#company.company_address_l1#" name="company_address_l1"></td></tr>
				<tr><td class="feed_option"><b>Address (Line 2)</b></td><td><input style="font-size: 12px; width: 320px;" maxlength="100" type="text" value="#company.company_address_l2#" name="company_address_l2"></td></tr>
				<tr><td class="feed_option"><b>City/State/Zip</b></td><td>
				<input style="font-size: 12px; width: 200px;" type="text" name="company_city" value="#company.company_city#">&nbsp;&nbsp;
			</cfoutput>

				<cfquery name="state" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select * from state
				 order by state_name
				</cfquery>

				<select name="company_state">
				<cfoutput query="state">
				 <option value="#state_abbr#" <cfif #company.company_state# is #state_abbr#>selected</cfif>>#state_name#
				</cfoutput>
				</select>

				<cfoutput>
				<input style="font-size: 12px; width: 100px;" type="text" name="company_zip" maxlength="25" value="#company.company_zip#">

				</td></tr>

				<tr><td class="feed_option"><b>Website</b></td><td><input name="company_website" maxlength="200" style="font-size: 12px; width: 400px;" type="text" value="#company.company_website#" vspace=5></td></tr>
				<tr><td class="feed_option"><b>POC First Name</b></td><td><input style="font-size: 12px; width: 220px;" type="text" value="#company.company_poc_first_name#" name="company_poc_first_name" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Last Name</b></td><td><input style="font-size: 12px; width: 220px;" type="text" value="#company.company_poc_last_name#" name="company_poc_last_name" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Title</b></td><td><input style="font-size: 12px; width: 220px;" type="text" value="#company.company_poc_title#" name="company_poc_title" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Email</b></td><td><input style="font-size: 12px; width: 220px;" type="email" value="#company.company_poc_email#" name="company_poc_email" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Phone</b></td><td><input style="font-size: 12px; width: 220px;" type="text" value="#company.company_poc_phone#" name="company_poc_phone" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Company Logo URL</b></td><td><input style="font-size: 12px; width: 220px;" type="url" value="#company.company_logo_url#" name="company_logo_url" maxlength="100"></td></tr>
 			    <tr><td class="feed_option"><b>Or, upload Company logo</b></td><td>

					<cfif #company.company_logo# is "">
					  <input type="file" name="company_logo"></td></tr>
					<cfelse>
					  <img style="border-radius: 8px;" src="#media_virtual#/#company.company_logo#" width=100>
					  <br>
					  <font size=2><b>Change Logo</b></font><br>
					  <input type="file" name="company_logo"><br>
					  <font size=2><input type="checkbox" name="remove_logo">&nbsp;or, check to remove logo
					 </cfif>

 			    </td></tr>

			</table>

            </td><td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td class="feed_option"><b>Year Founded</b></td><td><input style="font-size: 12px; width: 75px;" type="text" maxlength="4" value="#company.company_founded#" name="company_founded"></td></tr>
            <tr><td class="feed_option"><b>DUNS Number</b></td><td class="feed_option"><input style="font-size: 12px; width: 120px;" maxlength="12" type="text" value="#company.company_duns#" name="company_duns"></td></tr>
            <tr><td class="feed_option"><b>Employees</b></td><td class="feed_option"><input style="font-size: 12px; width: 120px;" maxlength="12" type="number" value="#company.company_employees#" name="company_employees"></td></tr>

            </cfoutput>

 		    <tr><td class="feed_option"><b>Company Size</b></td>
 		        <td><select name="company_size_id" style="font-size: 12px;">

					<option value=0>Select
					<cfoutput query="size">
					 <option value=#size_id# <cfif #company.company_size_id# is #size_id#>selected</cfif>>#size_name#
					</cfoutput>

				</select>
				</td></tr>

 		    <tr><td class="feed_option"><b>Business Entity</b></td>
			    <td><select name="company_entity_id" style="font-size: 12px;">

					<option value=0>Select
					<cfoutput query="entity">
					 <option value=#entity_id# <cfif #company.company_entity_id# is #entity_id#>selected</cfif>>#entity_name#
					</cfoutput>

				</select>
				</td></tr>

            <cfoutput>

            <tr><td class="feed_option"><b>Revenue (2018)</b></td><td><input style="font-size: 12px; width: 120px;" type="number" value="#company.company_fy18_revenue#" name="company_fy18_revenue"></td></tr>
            <tr><td class="feed_option"><b>Revenue (2017)</b></td><td><input style="font-size: 12px; width: 120px;" type="number" value="#company.company_fy17_revenue#" name="company_fy17_revenue"></td></tr>
            <tr><td class="feed_option"><b>Revenue (2016) </b></td><td><input style="font-size: 12px; width: 120px;" type="number" value="#company.company_fy16_revenue#" name="company_fy16_revenue"></td></tr>

            </table>

            </td></tr>

            <tr><td class="feed_option"><b>Company Tagline</b></td></tr>
			<tr><td colspan=2><input type="text" size=40 name="company_tagline" value="#company.company_tagline#"></td></tr>
			<tr><td>&nbsp;</td></tr>

            <tr><td class="feed_option"><b>Company Short Description</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 75px;" name="company_about">#company.company_about#</textarea></td></tr>
			<tr><td>&nbsp;</td></tr>

            <tr><td class="feed_option"><b>Company Long Description</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="company_long_desc">#company.company_long_desc#</textarea></td></tr>
			<tr><td>&nbsp;</td></tr>

            <tr><td class="feed_option"><b>Company History</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="company_history">#company.company_history#</textarea></td></tr>
			<tr><td>&nbsp;</td></tr>
            <tr><td class="feed_option"><b>Leadership Team</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="company_leadership">#company.company_leadership#</textarea></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td class="feed_option"><b>Company Keywords</b></td></tr>
			<tr><td><input name="company_keywords" maxlength="300" style="font-size: 12px; width: 600px;" value="#company.company_keywords#" type="text" vspace=5></td></tr>

            </cfoutput>

            <tr><td class="feed_option"><b>Company Admin</b></td></tr>
            <tr><td>

		    <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from hub_xref
			 left join usr on usr_id = hub_xref_usr_id
			 where hub_xref_hub_id = #session.hub#
			 order by usr_last_name, usr_first_name
			</cfquery>

				<select name="company_admin_id">
				<option value=0>Select Admin
				 <cfoutput query="members">
				  <option value=#usr_id# <cfif #company.company_admin_id# is #usr_id#>selected</cfif>>#usr_last_name#, #usr_first_name#
				 </cfoutput>
				<select>

             </td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=2><hr></td></tr>





               <tr><td class="feed_option"><b>Broker</b></td></tr>

               <tr><td>
   				<select name="broker_status_id">
   				<option value=0>No
   				<option value=1 <cfif broker_info.broker_status_id is 1>selected</cfif>>Yes
   				<select>
                </td></tr>

               <tr><td class="feed_option"><b>Broker Type</b></td></tr>

               <tr><td>
   				<select name="broker_type_id">
   				<option value=0>Not Selected
   				<option value=1 <cfif broker_info.broker_type_id is 1>selected</cfif>>Independent
   				<option value=2 <cfif broker_info.broker_type_id is 2>selected</cfif>>Brokerage
   				<select>
                </td></tr>

               <tr><td class="feed_option"><b>Broker Level</b></td></tr>

               <tr><td>
   				<select name="broker_level_id">
   				<option value=0>Not Selected
   				<option value=1 <cfif broker_info.broker_level_id is 1>selected</cfif>>Basic
   				<option value=2 <cfif broker_info.broker_level_id is 2>selected</cfif>>Silver
   				<option value=3 <cfif broker_info.broker_level_id is 3>selected</cfif>>Gold
   				<option value=4 <cfif broker_info.broker_level_id is 4>selected</cfif>>Platinum
   				<select>
                </td></tr>

               <tr><td class="feed_option"><b>Featured Broker</b></td></tr>

               <tr><td>
   				<select name="broker_featured">
   				<option value=0>No
   				<option value=1 <cfif broker_info.broker_featured is 1>selected</cfif>>Yes
   				<select>
                </td></tr>

                <cfoutput>

               <tr><td class="feed_option"><b>Broker Services</b></td></tr>
   			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="broker_services">#broker_info.broker_services#</textarea></td></tr>

   			<tr><td class="feed_option"><b>Broker Tagline</b></td></tr>
   			<tr><td><input name="broker_tagline" maxlength="300" style="font-size: 12px; width: 400px;" value="#broker_info.broker_tagline#" type="text" vspace=5></td></tr>

   			<tr><td class="feed_option"><b>Broker Keywords</b></td></tr>
   			<tr><td><input name="broker_services_keywords" maxlength="300" style="font-size: 12px; width: 400px;" value="#broker_info.broker_services_keywords#" type="text" vspace=5></td></tr>

               </cfoutput>

                     <tr><td class="feed_option" valign=top><b>Broker Alignments</b></td></tr>
                     <tr>
                         <td class="feed_option" style="font-weight: normal;">

   					  <select name="broker_service_type_id" style="width: 300px; height: 175px;" multiple>
   					   <cfoutput query="broker_types">
   						<option value=#broker_type_id# <cfif listfind(broker_type_list,broker_type_id)>selected</cfif>>#broker_type_name#
   					   </cfoutput>
   					  </select>

                         </td></tr>

   <cfoutput>

             <tr><td class="feed_option"><input type="checkbox" name="crawl">&nbsp;&nbsp;<b>Crawl company homepage?</b>

             &nbsp;&nbsp;&nbsp;Last crawl -
             <cfif #company.company_homepage_updated# is "">Never<cfelse>#dateformat(company.company_homepage_updated,'mm/dd/yyyy')# at #timeformat(company.company_homepage_updated)#</cfif></td></tr>

             </cfoutput>

             <tr><td height=10></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0>
			<cfoutput>
				<tr><td colspan=3>
				<input class="button_blue" type="submit" name="button" value="Save" vspace=10>&nbsp;&nbsp;
				<input class="button_blue" type="submit" name="button" value="Remove" vspace=10 onclick="return confirm('Remove Company.\r\nAre you sure you want to remove this company from the Hub?');">
				</td></tr>

				<input type="hidden" name="company_id" value=#company_id#>
            </cfoutput>
		    </table>

		    </td></tr>

		    </table>

            </form>

            </td></tr>

          </table>

          </td></tr>

          </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

