<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">


 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

		  <cfinclude template="/exchange/admin/admin_menu.cfm">
		  <cfinclude template="/exchange/marketplace/recent.cfm">
		  <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td colspan=2 class="feed_header"><a href="network_in.cfm">ADD NEW IN NETWORK PARTNER</a></td></tr>
			<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">New Partners must not currently be in the Exchange.  To find out, please search to find out if the Company exists already.</td></tr>
			<tr><td colspan=2><hr></td></tr>
	        <tr><td height=10></td></tr>
		</table>

        <form action="network_in_add_next.cfm" method="post">


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>
				<td class="feed_sub_header" width=15%>Company Name</td>
				<td><input type="text" name="company_name" class="input_text" style="width: 300px;" required></td>
		    </tr>

			<tr>
				<td class="feed_sub_header">Company Domain</td>
				<td><input type="text" name="company_domain" placeholder="i.e., domain.com" class="input_text" style="width: 300px;" required></td>
		    </tr>

		    <tr><td height=10></td></tr>
		    <tr><td></td><td><input type="submit" name="button" value="Search" class="button_blue_large"></td></tr>

        </table>

        </form>


	  </div>

      </td>

      </tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>