<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="size" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from size
</cfquery>

<cfquery name="entity" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from entity
</cfquery>

<cfquery name="broker_types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from broker_type
 order by broker_type_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Add Company</td>
            <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
        <tr><td>&nbsp;</td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td valign=top>

      <form action="db.cfm" enctype="multipart/form-data" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfoutput>
				<tr><td class="feed_option"><b>Company Name</b></td><td><input style="font-size: 12px; width: 320px;" type="text" name="company_name" maxlength="100" required></td></tr>
				<tr><td class="feed_option"><b>Address (Line 1)</b></td><td><input style="font-size: 12px; width: 320px;" maxlength="100" type="text" name="company_address_l1"></td></tr>
				<tr><td class="feed_option"><b>Address (Line 2)</b></td><td><input style="font-size: 12px; width: 320px;" maxlength="100" type="text" name="company_address_l2"></td></tr>
				<tr><td class="feed_option"><b>City/State/Zip</b></td><td>
				<input style="font-size: 12px; width: 200px;" type="text" name="company_city">&nbsp;&nbsp;
			</cfoutput>

				<cfquery name="state" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select * from state
				 order by state_name
				</cfquery>

				<select name="company_state">
				<cfoutput query="state">
				 <option value="#state_abbr#">#state_name#
				</cfoutput>
				</select>

				<cfoutput>
				<input style="font-size: 12px; width: 100px;" type="text" name="company_zip" maxlength="25">

				</td></tr>

				<tr><td class="feed_option"><b>Website</b></td><td><input name="company_website" maxlength="200" style="font-size: 12px; width: 400px;" type="url" vspace=5></td></tr>

				<tr><td class="feed_option"><b>POC First Name</b></td><td><input style="font-size: 12px; width: 220px;" type="text" name="company_poc_first_name" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Last Name</b></td><td><input style="font-size: 12px; width: 220px;" type="text" name="company_poc_last_name" maxlength="100"></td></tr>

				<tr><td class="feed_option"><b>Title</b></td><td><input style="font-size: 12px; width: 220px;" type="text" name="company_poc_title" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Email</b></td><td><input style="font-size: 12px; width: 220px;" type="email" name="company_poc_email" maxlength="100"></td></tr>
				<tr><td class="feed_option"><b>Phone</b></td><td><input style="font-size: 12px; width: 220px;" type="text" name="company_poc_phone" maxlength="100"></td></tr>


				<tr><td class="feed_option"><b>Company Logo URL</b></td><td><input style="font-size: 12px; width: 220px;" type="url" name="company_logo_url" maxlength="100"></td></tr>

 			    <tr><td class="feed_option"><b>Or, upload Company logo</b></td><td><input type="file" name="company_logo"></td></tr>

			</table>

            </td><td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td class="feed_option"><b>Year Founded</b></td><td><input style="font-size: 12px; width: 75px;" type="text" maxlength="4" name="company_founded"></td></tr>
            <tr><td class="feed_option"><b>DUNS Number</b></td><td class="feed_option"><input style="font-size: 12px; width: 120px;" maxlength="12" type="text" name="company_duns"></td></tr>
            <tr><td class="feed_option"><b>Employees</b></td><td class="feed_option"><input style="font-size: 12px; width: 120px;" maxlength="12" type="number" name="company_employees"></td></tr>

            </cfoutput>

 		    <tr><td class="feed_option"><b>Company Size</b></td>
 		        <td><select name="company_size_id" style="font-size: 12px;">

					<option value=0>Select
					<cfoutput query="size">
					 <option value=#size_id#>#size_name#
					</cfoutput>

				</select>
				</td></tr>

 		    <tr><td class="feed_option"><b>Business Entity</b></td>
			    <td><select name="company_entity_id" style="font-size: 12px;">

					<option value=0>Select
					<cfoutput query="entity">
					 <option value=#entity_id#>#entity_name#
					</cfoutput>

				</select>
				</td></tr>

            <cfoutput>

            <tr><td class="feed_option"><b>Revenue (2018)</b></td><td><input style="font-size: 12px; width: 120px;" type="number" name="company_fy18_revenue"></td></tr>
            <tr><td class="feed_option"><b>Revenue (2017)</b></td><td><input style="font-size: 12px; width: 120px;" type="number" name="company_fy17_revenue"></td></tr>
            <tr><td class="feed_option"><b>Revenue (2016) </b></td><td><input style="font-size: 12px; width: 120px;" type="number" name="company_fy16_revenue"></td></tr>
<!---
				<tr><td colspan=2 class="feed_option"><b>Company is discoverable on the Exchange?</b>&nbsp;&nbsp;

				<select name="company_discoverable">
				 <option value=1>Yes
				 <option value=0>No
				<select>

				</td></tr> --->

            </table>

            </td></tr>

            <tr><td class="feed_option"><b>Company Tagline</b></td></tr>
			<tr><td colspan=2><input type="text" size=40 name="company_tagline"></td></tr>
			<tr><td>&nbsp;</td></tr>

            <tr><td class="feed_option"><b>Company Short Description</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 75px;" name="company_about"></textarea></td></tr>
			<tr><td>&nbsp;</td></tr>

            <tr><td class="feed_option"><b>Company Long Description</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="company_long_desc"></textarea></td></tr>
			<tr><td>&nbsp;</td></tr>

            <tr><td class="feed_option"><b>Company History</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="company_history"></textarea></td></tr>
			<tr><td>&nbsp;</td></tr>
            <tr><td class="feed_option"><b>Leadership Team</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="company_leadership"></textarea></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td class="feed_option"><b>Company Keywords</b></td></tr>
			<tr><td><input name="company_keywords" maxlength="300" style="font-size: 12px; width: 400px;" type="text" vspace=5></td></tr>

            </cfoutput>

            <tr><td class="feed_option"><b>Company Admin</b></td></tr>
            <tr><td>

            <!---

		    <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from usr
			 order by usr_last_name, usr_first_name
			</cfquery>

			--->

		    <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from hub_xref
			 left join usr on usr_id = hub_xref_usr_id
			 where hub_xref_hub_id = #session.hub# and
			       hub_xref_usr_role = 1
			 order by usr_last_name, usr_first_name
			</cfquery>

				<select name="company_admin_id">
				 <option value=0>Select Admin
				 <cfoutput query="members">
				  <option value=#usr_id#>#usr_last_name#, #usr_first_name#
				 </cfoutput>
				<select>

             </td></tr>







            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>

            <tr><td class="feed_option"><b>Broker</b></td></tr>

            <tr><td>
				<select name="broker_status_id">
				<option value=0>No
				<option value=1>Yes
				<select>
             </td></tr>

            <tr><td class="feed_option"><b>Broker Type</b></td></tr>

            <tr><td>
				<select name="broker_type_id">
				<option value=0>Not Selected
				<option value=1>Independent
				<option value=2>Brokerage
				<select>
             </td></tr>

            <tr><td class="feed_option"><b>Broker Level</b></td></tr>

            <tr><td>
				<select name="broker_level_id">
				<option value=0>Not Selected
				<option value=1>Basic
				<option value=2>Silver
				<option value=3>Gold
				<option value=4>Platinum
				<select>
             </td></tr>

            <tr><td class="feed_option"><b>Featured Broker</b></td></tr>

            <tr><td>
				<select name="broker_featured">
				<option value=0>No
				<option value=1>Yes
				<select>
             </td></tr>

            <tr><td class="feed_option"><b>Broker Services</b></td></tr>
			<tr><td colspan=2><textarea style="font-family: arial; font-size: 12px; width: 785px; height: 150px;" name="broker_services"></textarea></td></tr>

			<tr><td class="feed_option"><b>Broker Tagline</b></td></tr>
			<tr><td><input name="broker_tagline" maxlength="300" style="font-size: 12px; width: 400px;" type="text" vspace=5></td></tr>

			<tr><td class="feed_option"><b>Broker Keywords</b></td></tr>
			<tr><td><input name="broker_services_keywords" maxlength="300" style="font-size: 12px; width: 400px;" type="text" vspace=5></td></tr>

                  <tr><td class="feed_option" valign=top><b>Broker Alignments</b></td></tr>
                  <tr>
                      <td class="feed_option" style="font-weight: normal;">

					  <select name="broker_service_type_id" style="width: 300px; height: 175px;" multiple>
					   <cfoutput query="broker_types">
						<option value=#broker_type_id#>#broker_type_name#
					   </cfoutput>
					  </select>

                      </td></tr>

             <tr><td height=10></td></tr>
             <tr><td class="feed_option"><input type="checkbox" name="crawl">&nbsp;&nbsp;<b>Update Crawl Content?</b></td></tr>

          </table>

		    <table cellspacing=0 cellpadding=0 border=0>
				<cfoutput>
					<tr><td colspan=3><input class="button_blue" type="submit" name="button" value="Add" vspace=10></td></tr>
				</cfoutput>
		    </table>

		    </td></tr>

		    </table>



            </form>

            </td></tr>

          </table>

          </td></tr>

          </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

