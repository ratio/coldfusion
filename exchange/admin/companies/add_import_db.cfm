<cfif #company_filename# is not "">
	<cffile action = "upload"
	 fileField = "company_filename"
	 destination = "#temp_path#"
	 nameConflict = "MakeUnique">
</cfif>

<cfset theFile = "#temp_path#\#cffile.serverfile#">

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

			<cfloop query="data" startrow="2">

				<cfset val = "">

				<cfquery name="dupe" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select company_id from company
				 where company_name = '#company_name#'
				</cfquery>

				<cfif dupe.recordcount is 0>

				<cfif company_name is not "" or company_name is not "NULL">

				<cfif company_tag_1 is "" or company_tag_1 is "NULL">
					 <cfset val = "">
				<cfelse>
				     <cfif company_tag_2 is "" or company_tag_2 is "NULL">
				      <cfset val = "#company_tag_1#">
				     <cfelse>
				       <cfif company_tag_3 is "" or company_tag_3 is "NULL">
				         <cfset val = "#company_tag_1#" & "," & "#company_tag_2#">
				       <cfelse>
				         <cfset val = "#company_tag_1#" & "," & "#company_tag_2#" & "," & "#company_tag_3#">
				       </cfif>
				      </cfif>
				</cfif>

				<cfquery name="company_import" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into company
				   (
					company_name,
					company_about,
					company_website,
					company_city,
					company_state,
					company_source_website,
					company_updated,
					company_keywords
					)
				  values
				  (
					'#company_name#',
					'#company_description#',
					'#company_website#',
					<cfif #company_city# is "" or #company_city# is "NULL">null<cfelse>'#company_city#'</cfif>,
					<cfif #company_state# is "" or #company_state# is "NULL">null<cfelse>'#company_state#'</cfif>,
					'#company_source_name#',
					'10/10/2019',
					'#val#'
				   )
				</cfquery>

				<cfelse>

				 <cfoutput>Duplication found - #company_name#<br></cfoutput>

				</cfif>

               </cfif>

           </cfloop>

<cfelse>

	Could not find the file.

</cfif>