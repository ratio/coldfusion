<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Companies</td>
	       <td class="feed_option" align=right><a href="add.cfm">Add Company</a></td></tr>
	   <tr><td>&nbsp;</td></tr>

       <cfif isdefined("u")>
        <cfif u is 3>
         <tr><td class="feed_option"><font color="red"><b>Company has been deleted.</b></font></td></tr>
        <cfelseif u is 1>
         <tr><td class="feed_option"><font color="green"><b>Company has been added.</b></font></td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_option"><font color="green"><b>Company has been updated.</b></font></td></tr>
        </cfif>
       <tr><td>&nbsp;</td></tr>
       </cfif>

      </table>

	    <form action="set.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_option"><b>Company Name: &nbsp;&nbsp;</b><input type="text" name="company_name" required size=30>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" style="height: 24px; width: 75px;" value="Search" vspace=10></td></tr>
	    </table>

	    </form>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

