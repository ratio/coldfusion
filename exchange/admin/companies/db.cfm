<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Save">

        <cfif isdefined("remove_logo")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select company_logo from company
			  where company_id = #company_id#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.company_logo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.company_logo#">
        	</cfif>

        </cfif>

		<cfif #company_logo# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select company_logo from company
			  where company_id = #company_id#
			</cfquery>

			<cfif #getfile.company_logo# is not "">
             <cfif FileExists("#media_path#\#getfile.company_logo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.company_logo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  update company
			  set company_name = '#company_name#',
			      company_admin_id = <cfif #company_admin_id# is 0>null<cfelse>#company_admin_id#</cfif>,
				  company_about = '#company_about#',
				  company_tagline = '#company_tagline#',
				  company_long_desc = '#company_long_desc#',
				  company_hub_id = #session.hub#,
				  company_logo_url = '#company_logo_url#',

				  <cfif #company_logo# is not "">
				   company_logo = '#cffile.serverfile#',
				  </cfif>
				  <cfif isdefined("remove_logo")>
				   company_logo = null,
				  </cfif>

				  company_website = '#company_website#',
				  company_poc_first_name = '#company_poc_first_name#',
				  company_poc_last_name = '#company_poc_last_name#',
				  company_poc_title = '#company_poc_title#',
				  company_poc_email = '#company_poc_email#',
				  company_poc_phone = '#company_poc_phone#',
				  company_size_id = <cfif #company_size_id# is 0>null<cfelse>#company_size_id#</cfif>,
				  company_entity_id = <cfif #company_entity_id# is 0>null<cfelse>#company_entity_id#</cfif>,
				  company_address_l1 = '#company_address_l1#',
				  company_address_l2 = '#company_address_l2#',
				  company_city = '#company_city#',
				  company_state = '#company_state#',
				  company_zip = '#company_zip#',
				  company_duns = '#company_duns#',
				  company_history = '#company_history#',
				  company_leadership = '#company_leadership#',
				  company_founded = <cfif #company_founded# is "">null<cfelse>#company_founded#</cfif>,
				  company_employees = <cfif #company_employees# is "">null<cfelse>#company_employees#</cfif>,
				  company_keywords = '#company_keywords#',
				  company_fy16_revenue = <cfif #company_fy16_revenue# is "">null<cfelse>#company_fy16_revenue#</cfif>,
				  company_fy17_revenue = <cfif #company_fy17_revenue# is "">null<cfelse>#company_fy17_revenue#</cfif>,
				  company_fy18_revenue = <cfif #company_fy18_revenue# is "">null<cfelse>#company_fy18_revenue#</cfif>,
				  company_updated = #now()#
				  where company_id = #company_id#
			</cfquery>

			<!--- Update Broker Settings --->

			<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              select broker_company_id from broker
              where broker_company_id = #company_id#
              <cfif isdefined("session.hub")>
               and broker_hub_id = #session.hub#
              <cfelse>
               and broker_hub_id is null
              </cfif>
            </cfquery>

            <cfif check.recordcount is 0>
			 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into broker
			  (broker_company_id, broker_hub_id)
			  values
			  (#company_id#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			 </cfquery>
            </cfif>

			<cfquery name="update_broker" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update broker
			 set broker_company_id = #company_id#,
			     broker_hub_id = <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			     broker_featured = #broker_featured#,
			     broker_services = '#broker_services#',
			     broker_tagline = '#broker_tagline#',
			     broker_services_keywords = '#broker_services_keywords#',
			     broker_type_id = #broker_type_id#,
			     broker_level_id = #broker_level_id#,
			     broker_status_id = #broker_status_id#
			 where broker_company_id = #company_id# and
			       <cfif isdefined("session.hub")>
			        broker_hub_id = #session.hub#
			       <cfelse>
			        broker_hub_id is null
			       </cfif>
			</cfquery>

			<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 delete xref_comp_broker
			 where xref_comp_broker_company_id = #company_id#
			 <cfif isdefined("session.hub")>
			  and xref_comp_broker_hub_id = #session.hub#
			 <cfelse>
			  and xref_comp_broker_hub_id is null
			 </cfif>
			</cfquery>

			<cfif isdefined("broker_service_type_id")>

				<cfloop index="broker" list=#broker_service_type_id#>

					<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 Insert into xref_comp_broker
					 (xref_comp_broker_company_id, xref_comp_broker_broker_type_id, xref_comp_broker_hub_id)
					 Values
					 (#company_id#, #broker#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
					</cfquery>

				</cfloop>

			 </cfif>

			<cfif isdefined("crawl")>
			 <cfset #session.crawl_company_id# = #company_id#>
	         <cfinclude template="/exchange/include/crawl_website.cfm">
			</cfif>

	<cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif button is "Remove">

	<cftransaction>

		<cfquery name="update_crawl" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update company
		 set company_hub_id = null
		 where company_id = #company_id#
		</cfquery>

		<cfquery name="remove_from_network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete hub_comp
		 where hub_comp_hub_id = #session.hub# and
		       hub_comp_company_id = #company_id#
		</cfquery>

	</cftransaction>

	<cflocation URL="index.cfm?u=6" addtoken="no">

<cfelseif button is "Delete">

<cftransaction>

			<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              delete company
              where company_id = #company_id#
            </cfquery>

			<cfquery name="updateusrs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              update usr
              set usr_company_id = null
              where usr_company_id = #company_id#
            </cfquery>

			<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              delete product
              where product_company_id = #company_id#
            </cfquery>

			<cfquery name="delete3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              delete media
              where media_company_id = #company_id#
            </cfquery>

			<cfquery name="delete4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              delete customer
              where customer_company_id = #company_id#
            </cfquery>

 			<cfquery name="delete5" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
               delete biz_code_xref
               where biz_code_xref_company_id = #company_id#
            </cfquery>

  			<cfquery name="delete6" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                delete align
                where align_company_id = #company_id#
            </cfquery>

   			<cfquery name="delete7" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                 delete doc
                 where doc_company_id = #company_id#
            </cfquery>

    		<cfquery name="delete8" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                  delete naics_xref
                  where naics_xref_company_id = #company_id#
            </cfquery>

 </cftransaction>

			<cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif button is "Add">

<cftransaction>

		<cfif #company_logo# is not "">

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into company
			   (
				company_admin_id,
				company_size_id,
				company_entity_id,
				company_registered,
                company_hub_id,
				company_name,
				company_about,
				company_tagline,
				company_long_desc,
				company_logo_url,
				company_logo,
				company_website,
				company_poc_first_name,
				company_poc_last_name,
				company_poc_title,
				company_poc_email,
				company_poc_phone,
				company_address_l1,
				company_address_l2,
				company_city,
				company_state,
				company_zip,
				company_duns,
				company_history,
				company_leadership,
				company_founded,
				company_employees,
				company_keywords,
				company_fy16_revenue,
				company_fy17_revenue,
				company_fy18_revenue,
				company_updated,
				company_marketplace
				)
			  values
			  (
				 <cfif #company_admin_id# is 0>null<cfelse>#company_admin_id#</cfif>,
				 <cfif #company_size_id# is 0>null<cfelse>#company_size_id#</cfif>,
				 <cfif #company_entity_id# is 0>null<cfelse>#company_entity_id#</cfif>,
				 #now()#,
                 #session.hub#,
				'#company_name#',
				'#company_about#',
				'#company_tagline#',
				'#company_long_desc#',
				'#company_logo_url#',

				  <cfif #company_logo# is not "">
				   '#cffile.serverfile#',
				  <cfelse>
				   null,
				  </cfif>

				'#company_website#',
				'#company_poc_first_name#',
				'#company_poc_last_name#',
				'#company_poc_title#',
				'#company_poc_email#',
				'#company_poc_phone#',
				'#company_address_l1#',
				'#company_address_l2#',
				'#company_city#',
				'#company_state#',
				'#company_zip#',
				'#company_duns#',
				'#company_history#',
				'#company_leadership#',
				 <cfif #company_founded# is "">null<cfelse>#company_founded#</cfif>,
				 <cfif #company_employees# is "">null<cfelse>#company_employees#</cfif>,
				'#company_keywords#',
				 <cfif #company_fy16_revenue# is "">null<cfelse>#company_fy16_revenue#</cfif>,
				 <cfif #company_fy17_revenue# is "">null<cfelse>#company_fy17_revenue#</cfif>,
				 <cfif #company_fy18_revenue# is "">null<cfelse>#company_fy18_revenue#</cfif>,
				 #now()#,
				 1
			   )
			</cfquery>

			<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select max(company_id) as id from company
			</cfquery>

			 <cfquery name="add_to_network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
              insert into hub_comp
              (hub_comp_hub_id,
               hub_comp_company_id,
               hub_comp_added_by_usr_id,
               hub_comp_added_by_company_id,
               hub_comp_added
               )
               values
               (
                #session.hub#,
                #max.id#,
                #session.usr_id#,
                #session.company_id#,
                #now()#
               )
             </cfquery>

			 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into broker
			  (
			   broker_company_id,
			   broker_hub_id,
			   broker_services,
			   broker_tagline,
			   broker_services_keywords,
			   broker_type_id,
			   broker_level_id,
			   broker_status_id,
			   broker_featured
			   )
			   values
			   (#max.id#,
			     <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			     '#broker_services#',
			     '#broker_tagline#',
			     '#broker_services_keywords#',
			      #broker_type_id#,
			      #broker_level_id#,
			      #broker_status_id#,
                  #broker_featured#
                )
			</cfquery>

			<cfif isdefined("broker_service_type_id")>

				<cfloop index="broker" list=#broker_service_type_id#>

					<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 Insert into xref_comp_broker
					 (xref_comp_broker_company_id, xref_comp_broker_broker_type_id, xref_comp_broker_hub_id)
					 Values
					 (#max.id#, #broker#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
					</cfquery>

				</cfloop>

			 </cfif>





















</cftransaction>

			<cfif isdefined("crawl")>
			 <cfset #session.crawl_company_id# = #max_comp.id#>
	         <cfinclude template="/exchange/include/crawl_website.cfm">
			</cfif>

<cflocation URL="index.cfm?u=1" addtoken="no">

</cfif>