<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">

	      <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <div class="main_box">

     <form action="add_import_db.cfm" method="post" enctype="multipart/form-data" >

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Upload Companies</td></tr>
	   <tr><td>&nbsp;</td></tr>
	   <tr><td class="feed_option"><b>Filename: </b> &nbsp;&nbsp;<input type="file" name="company_filename" required size=40></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td><input type="submit" name="button" value="Import"></td></tr>

       <tr><td>&nbsp;</td></tr>
       <tr><td class="feed_option"><a href="company_import_template.xlsx" target="_blank" rel="noopener" rel="noreferrer">Download Template</a></td></tr>


	  </table>

	  </form>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

