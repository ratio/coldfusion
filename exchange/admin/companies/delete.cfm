<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete company
	 where company_id = #company_id#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where align_company_id = #company_id#
	</cfquery>

	<cfquery name="delete3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete vehicle_xref
	 where vehicle_xref_company_id = #company_id#
	</cfquery>

	<cfquery name="delete4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete naics_xref
	 where naics_xref_company_id = #company_id#
	</cfquery>

	<cfquery name="delete5" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete media
	 where media_company_id = #company_id#
	</cfquery>

	<cfquery name="delete6" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete customer
	 where customer_company_id = #company_id#
	</cfquery>

	<cfquery name="delete7" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete partner
	 where partner_company_id = #company_id#
	</cfquery>

	<cfquery name="delete8" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete product
	 where product_company_id = #company_id#
	</cfquery>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update usr
	 set usr_company_id = null
	 where usr_company_id = #company_id#
	</cfquery>

</cftransaction>

<cflocation URL="index.cfm?u=3" addtoken="no">
