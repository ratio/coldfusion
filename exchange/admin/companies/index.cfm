<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Companies</td>
	       <td class="feed_option" align=right><a href="add.cfm">Add Company</a></td></tr>
	   <tr><td height=10></td></tr>

       <cfif isdefined("u")>
        <cfif u is 3>
         <tr><td class="feed_option"><font color="red"><b>Company has been deleted.</b></font></td></tr>
        <cfelseif u is 1>
         <tr><td class="feed_option"><font color="green"><b>Company has been added.</b></font></td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_option"><font color="green"><b>Company has been updated.</b></font></td></tr>
        <cfelseif u is 5>
         <tr><td class="feed_option"><font color="green"><b>You are no longer masking as a different company.</b></font></td></tr>
        <cfelseif u is 4>
         <tr><td class="feed_option"><font color="green"><b>You are now masking yourself as "<cfoutput>#session.mask_company#</cfoutput>".</b></font></td></tr>
        <cfelseif u is 6>
         <tr><td class="feed_option"><font color="green"><b>Company has been removed from Hub.</b></font></td></tr>

        </cfif>
       <tr><td height=5></td></tr>
       </cfif>

      </table>

		<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from company
		  where company_hub_id = #session.hub#
		  order by company_name
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr>
		     <td class="feed_option"><b>Company Name</b></td>
		     <td class="feed_option"><b>Website</b></td>
		     <td class="feed_option"><b>City</b></td>
		     <td class="feed_option"><b>State</b></td>
		     <td class="feed_option" align=right><b>Mask</b></td>
		  </tr>

		  <cfset counter = 0>

		  <cfoutput query="info">

		   <cfif counter is 0>
		    <tr bgcolor="ffffff">
		   <cfelse>
		    <tr bgcolor="e0e0e0">
		   </cfif>

		   <td class="feed_option"><a href="edit.cfm?company_id=#info.company_id#"><cfif #info.company_name# is "">No company name<cfelse>#info.company_name#</cfif></a></td>
		   <td class="feed_option"><a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">#info.company_website#</a></td>
		   <td class="feed_option">#info.company_city#</td>
		   <td class="feed_option">#info.company_state#</td>
		   <td class="feed_option" align=right><a href="mask.cfm?company_id=#info.company_id#">Mask</a></td>

		   </tr>

		   <cfif counter is 0>
		    <cfset counter = 1>
		   <cfelse>
		    <cfset counter = 0>
		   </cfif>

		  </cfoutput>

		</table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

