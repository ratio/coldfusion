<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Company">

<--- Insert from SAM --->

   <cftransaction>

	   <cfquery name="hub_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select * from hub
	    where hub_id = #session.hub#
	   </cfquery>

        <!-- Generate Random Password --->

  		<cfset result1="">
  		<cfset i=0>

  		<cfloop index="i" from="1" to="10">
  		 <cfset result1=result1&Chr(RandRange(65, 90))>
  		</cfloop>

  	   <!--- Insert User --->

       <cfset usr_full_name = #usr_first_name# & " " & #usr_last_name#>

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into usr
		  (usr_full_name, usr_updated, usr_advisor_id, usr_phone, usr_address_line_1, usr_address_line_2, usr_city, usr_state, usr_zip, usr_title, usr_first_name, usr_last_name, usr_email, usr_password, usr_created, usr_validation, usr_validation_date, usr_setting_notification,usr_active)
		  values
		  ('#usr_full_name#', #now()#, #usr_advisor_id#, '#usr_phone#','#usr_address_line_1#','#usr_address_line_2#','#usr_city#','#usr_state#','#usr_zip#','#usr_title#','#usr_first_name#','#usr_last_name#','#usr_email#','#result1#',#now()#,1,#now()#,1,1)
		</cfquery>

	   <cfquery name="usr_max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select max(usr_id) as id from usr
	   </cfquery>

       <!--- Insert Company --->

	    <cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into company(company_duns, company_registered, company_updated, company_admin_id, company_name, company_website)
		 values('#duns#', #now()#, #now()#, #usr_max.id#,'#company_name#','#company_website#')
		</cfquery>

	    <cfquery name="company_max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(company_id) as id from company
		</cfquery>

	    <cfquery name="update_usr_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update usr
		 set usr_company_id = #company_max.id#
		 where usr_id = #usr_max.id#
		</cfquery>

	    <cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref(hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_default, hub_xref_joined, hub_xref_usr_role)
		 values(#usr_max.id#,#session.hub#,1,1,#now()#,2)
		</cfquery>

        <!--- Get SAM Information --->

		   <cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from sams
			where duns = '#duns#'
		   </cfquery>

			<!--- Sync SAM Information --->

			<cfquery name="update_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update company
			 set company_name = '#sams.legal_business_name#',
			     company_address_l1 = '#sams.phys_address_l1#',
				 company_address_l2 = '#sams.phys_address_line_2#',
				 company_city = '#sams.city_1#',
				 company_state = '#sams.state_1#',
				 company_registered = #now()#,
				 company_zip = '#sams.zip_1#',
				 company_founded = #left(sams.biz_start_date,4)#,
				 company_cage_code = '#sams.cage_code#',
				 company_updated = #now()#,
				 company_sam_pull_updated = #now()#
			 where company_id = #company_max.id#
			</cfquery>

			<cfquery name="delete_naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 delete naics_xref
			 where naics_xref_company_id = #company_max.id#
			</cfquery>

			<cfloop index="element" list="#listsort(sams.naisc_code_string,'Text','ASC','~')#" delimiters="~">

			<cfquery name="insert_naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into naics_xref
			 (
			  naics_xref_company_id,
			  naics_xref_code,
			  naics_xref_code_pri
			 )
			 values
			 (
			  #company_max.id#,
			 '#left(element,'6')#',
			 <cfif #left(element,'6')# is #sams.pri_naics#>1<cfelse>0</cfif>
			 )
			</cfquery>

		   </cfloop>

			<cfquery name="delete_bizcode" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 delete biz_code_xref
			 where biz_code_xref_company_id = #company_max.id#
			</cfquery>

		   <cfloop index="biz_element" list="#listsort(sams.biz_type_string,'Text','ASC','~')#" delimiters="~">

			<cfquery name="insert_biz_class" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into biz_code_xref
			 (
			  biz_code_xref_code,
			  biz_code_xref_company_id
			 )
			 values
			 ('#biz_element#',
			   #company_max.id#
			 )
			</cfquery>

		   </cfloop>

     <!--- Insert User Alignments --->

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (1,2,3,4)) and
	       (align_usr_id = #usr_max.id#)
	</cfquery>

	<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from lens
	</cfquery>

    <cfloop query="lens">
		<cfquery name="insert_lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 Insert into align
		 (align_usr_id, align_type_id, align_type_value)
		 Values
		 (#usr_max.id#, 1, #lens.lens_id#)
		</cfquery>
    </cfloop>

	<cfif isdefined("markets")>

		<cfloop index="m_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 2, #m_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sectors")>

		<cfloop index="s_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 3, #s_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topics")>

		<cfloop index="t_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 4, #t_element#)
			</cfquery>

		</cfloop>

	</cfif>

    <!--- Insert Company Alignments --->

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (1,2,3,4)) and
	       (align_company_id = #company_max.id#)
	</cfquery>

	<cfquery name="clens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from lens
	</cfquery>

    <cfloop query="clens">
		<cfquery name="insert_lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 Insert into align
		 (align_company_id, align_type_id, align_type_value)
		 Values
		 (#company_max.id#, 1, #clens.lens_id#)
		</cfquery>
    </cfloop>

	<cfif isdefined("markets")>

		<cfloop index="cm_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 2, #cm_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sectors")>

		<cfloop index="cs_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 3, #cs_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topics")>

		<cfloop index="ct_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 4, #ct_element#)
			</cfquery>

		</cfloop>

	</cfif>

	</cftransaction>

<!-- Notify User

<cfif isdefined("pass")>

	<cfmail from="The Exchange <noreply@ratio.exchange>"
			  to="#usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Welcome to the Exchange!">
	<html>
	<head>
	<title>RATIO Exchange</title>
	</head><div class="center">
	<body class="body">

	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 20px; font-weight: bold; padding-top: 20px; padding-bottom: 10px;">Congratulations on joining the Exchange!</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your Exchange account is now active and ready to use.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To login into the Exchange, please visit <a href="https://www.ratio.exchange/signin/">https://www.ratio.exchange</a></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Login Email: </b><cfoutput>#usr_email#</cfoutput></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Temporary Password: </b><cfoutput>#result1#</cfoutput></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Exchange Support Team</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
	 <tr><td>&nbsp;</td></tr>
	</table>
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="223a60">
	 <tr><td height=75 valign=middle>&nbsp;<img src="http://go.ratio.exchange/images/exchange_logo.png" width=160 hspace=10></td></tr>
	</table>
	</body>
	</html>

	</cfmail> --->

<cfelseif #button# is "Save New">

    <cftransaction>

       <!--- Get HUB Information --->

	   <cfquery name="hub_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	    select * from hub
	    where hub_id = #session.hub#
	   </cfquery>

        <!-- Generate Random Password --->

  		<cfset result1="">
  		<cfset i=0>

  		<cfloop index="i" from="1" to="10">
  		 <cfset result1=result1&Chr(RandRange(65, 90))>
  		</cfloop>

  	   <!--- Insert User --->

	   <cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into usr
		  (usr_updated, usr_advisor, usr_advisor_id, usr_exchange_admin, usr_phone, usr_address_line_1, usr_address_line_2, usr_city, usr_state, usr_zip, usr_title, usr_first_name, usr_last_name, usr_email, usr_password, usr_created, usr_validation, usr_validation_date, usr_setting_notification,usr_active)
		  values
		  (#now()#, 0, #usr_advisor_id#, 0, '#usr_phone#','#usr_address_line_1#','#usr_address_line_2#','#usr_city#','#usr_state#','#usr_zip#','#usr_title#','#usr_first_name#','#usr_last_name#','#usr_email#','#result1#',#now()#,1,#now()#,1,1)
		</cfquery>

	    <cfquery name="usr_max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select max(usr_id) as id from usr
	    </cfquery>

       <!--- Insert Company --->

	    <cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into company(company_duns, company_registered, company_updated, company_admin_id, company_name, company_website)
		 values('#duns#', #now()#, #now()#, #usr_max.id#,'#company_name#','#company_website#')
		</cfquery>

	    <cfquery name="company_max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(company_id) as id from company
		</cfquery>

	    <cfquery name="update_usr_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update usr
		 set usr_company_id = #company_max.id#
		 where usr_id = #usr_max.id#
		</cfquery>

	    <cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref(hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_default, hub_xref_joined, hub_xref_usr_role)
		 values(#usr_max.id#,#session.hub#,1,1,#now()#,2)
		</cfquery>

     <!--- Insert User Alignments --->

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (1,2,3,4)) and
	       (align_usr_id = #usr_max.id#)
	</cfquery>

	<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from lens
	</cfquery>

    <cfloop query="lens">
		<cfquery name="insert_lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 Insert into align
		 (align_usr_id, align_type_id, align_type_value)
		 Values
		 (#usr_max.id#, 1, #lens.lens_id#)
		</cfquery>
    </cfloop>

	<cfif isdefined("markets")>

		<cfloop index="m_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 2, #m_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sectors")>

		<cfloop index="s_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 3, #s_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topics")>

		<cfloop index="t_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#usr_max.id#, 4, #t_element#)
			</cfquery>

		</cfloop>

	</cfif>

    <!--- Insert Company Alignments --->

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (1,2,3,4)) and
	       (align_company_id = #company_max.id#)
	</cfquery>

	<cfquery name="clens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from lens
	</cfquery>

    <cfloop query="clens">
		<cfquery name="insert_lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 Insert into align
		 (align_company_id, align_type_id, align_type_value)
		 Values
		 (#company_max.id#, 1, #clens.lens_id#)
		</cfquery>
    </cfloop>

	<cfif isdefined("markets")>

		<cfloop index="cm_element" list=#markets#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 2, #cm_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sectors")>

		<cfloop index="cs_element" list=#sectors#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 3, #cs_element#)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topics")>

		<cfloop index="ct_element" list=#topics#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#company_max.id#, 4, #ct_element#)
			</cfquery>

		</cfloop>

	</cfif>

	</cftransaction>

<!-- Notify User --->

<cfif isdefined("pass")>

	<cfmail from="The Exchange <noreply@ratio.exchange>"
			  to="#usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Welcome to the #hub_info.hub_name# Exchange Hub">
	<html>
	<head>
	<title>RATIO Exchange</title>
	</head><div class="center">
	<body class="body">

	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 20px; font-weight: bold; padding-top: 20px; padding-bottom: 10px;">Congratulations, you've been added to the #hub_info.hub_name# Exchange Hub.</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Your Exchange account is now active and ready for use.</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To login into the Hub, please visit <a href="https://www.ratio.exchange/signin/">https://www.ratio.exchange</a></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Login Email: </b><cfoutput>#usr_email#</cfoutput></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Temporary Password: </b><cfoutput>#result1#</cfoutput></td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Exchange Support Team</td></tr>
	 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
	 <tr><td>&nbsp;</td></tr>
	</table>
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="223a60">
	 <tr><td height=75 valign=middle>&nbsp;<img src="http://go.ratio.exchange/images/exchange_logo.png" width=160 hspace=10></td></tr>
	</table>
	</body>
	</html>

	</cfmail>

</cfif>

</cfif>

<cflocation URL="members.cfm?u=2" addtoken="no">

</cfif>

<cflocation URL="members.cfm" addtoken="no">