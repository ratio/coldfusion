<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into ip_address (ip_address_number, ip_address_hub_id)
	 values ('#ip_address_number#',#session.hub#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update ip_address
	 set ip_address_number = '#ip_address_number#'
	 where ip_address_id = #i#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete ip_address
	 where ip_address_id = #i#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

