<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from hub where hub_id = #hub_id#
  </cfquery>

  <cfquery name="hub_owner" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from usr
   where usr_id = #info.hub_owner_id#
  </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=200>

	  <div class="exchange_admin_box2">

	      <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </div>

	  </td><td valign=top>

	  <form action="update.cfm" method="post">

	  <div class="main_box">

	  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Exchange HUB</td></tr>
		   <tr><td>&nbsp;</td></tr>

		   <tr><td class="feed_option"><b>Name</b></td></tr>
		   <tr><td class="feed_option">#info.hub_name#</td></tr>

		   <tr><td class="feed_option"><b>Short Description</b></td></tr>
		   <tr><td class="feed_option">#info.hub_desc#</td></tr>

		   <tr><td class="feed_option"><b>Long Description</b></td></tr>
		   <tr><td class="feed_option">#info.hub_long_desc#</td></tr>

		   <tr><td class="feed_option"><b>HUB Type</b></td></tr>
		   <tr><td class="feed_option"><cfif #info.hub_type_id# is 0>Open<cfelseif #info.hub_type_id# is 1>Standard<cfelseif #info.hub_type_id# is 2>Private</cfif></td></tr>

		   <tr><td class="feed_option"><b>HUB Rules</b></td></tr>
		   <tr><td class="feed_option">#info.hub_rules#</td></tr>

		   <tr><td class="feed_option"><b>HUB Owner</b></td></tr>
		   <tr><td class="feed_option">
		   #hub_owner.usr_first_name# #hub_owner.usr_last_name# (<a href="mailto:#hub_owner.usr_email#">#hub_owner.usr_email#</a>)
		   <br>
		   #hub_owner.usr_phone#</td></tr>

		   <tr><td height=5></td></tr>

           <tr><td class="feed_option"><b>HUB Premium Services</b></td></tr>

		   <cfquery name="subscription" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from premium
			where premium_hub_id = #hub_id#
		   </cfquery>

           <tr><td class="feed_option" colspan=2>
           <input type="checkbox" name="premium_hubs" <cfif #subscription.premium_hubs# is 1>checked</cfif>>&nbsp;&nbsp;Hubs&nbsp;&nbsp;
           <input type="checkbox" name="premium_opps" <cfif #subscription.premium_opps# is 1>checked</cfif>>&nbsp;&nbsp;Opportunities&nbsp;&nbsp;
           <input type="checkbox" name="premium_awards" <cfif #subscription.premium_awards# is 1>checked</cfif>>&nbsp;&nbsp;Awards&nbsp;&nbsp;
           <input type="checkbox" name="premium_marketplace" <cfif #subscription.premium_marketplace# is 1>checked</cfif>>&nbsp;&nbsp;Marketplace&nbsp;&nbsp;
           <input type="checkbox" name="premium_vehicle"  <cfif #subscription.premium_vehicle# is 1>checked</cfif>>&nbsp;&nbsp;Vehicles&nbsp;&nbsp;
           <input type="checkbox" name="premium_forecast"  <cfif #subscription.premium_forecast# is 1>checked</cfif>>&nbsp;&nbsp;Forecasts&nbsp;&nbsp;
           <input type="checkbox" name="premium_competitor"  <cfif #subscription.premium_competitor# is 1>checked</cfif>>&nbsp;&nbsp;Competitor Engine&nbsp;&nbsp;
           <input type="checkbox" name="premium_sbir" <cfif #subscription.premium_sbir# is 1>checked</cfif>>&nbsp;&nbsp;SBIR/STTR Engine&nbsp;&nbsp;
           <input type="checkbox" name="premium_grants" <cfif #subscription.premium_grants# is 1>checked</cfif>>&nbsp;&nbsp;Grants</td></tr>

		   <tr><td class="feed_option"><b>Status</b></td></tr>
		   <tr><td class="feed_option">

		   <select name="hub_status">
		    <option value=0 <cfif #info.hub_status# is 0>selected</cfif>>Not Approved
		    <option value=1 <cfif #info.hub_status# is 1>selected</cfif>>Approved
		    <option value=2 <cfif #info.hub_status# is 2>selected</cfif>>Suspended
		   </select>

		   </td></tr>


		   <tr><td colspan=2><input class="button_blue" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Cancel" vspace=10></td></tr>
		  </table>

		  <input type="hidden" name="hub_id" value=#hub_id#>
		  <input type="hidden" name="orig_status" value=#info.hub_status#>

         </cfoutput>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

