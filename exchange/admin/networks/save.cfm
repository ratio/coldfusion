<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

<cftransaction>

	<cfif #hub_logo# is not "">
		<cffile action = "upload"
		 fileField = "hub_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="add" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into hub
	  (hub_zip, hub_state, hub_city, hub_tag_line, hub_tags, hub_type_id, hub_owner_id, hub_created, hub_parent_hub_id, hub_name, hub_desc, hub_logo, hub_updated, hub_status)
	  values
	  ('#hub_zip#','#hub_state#','#hub_city#','#hub_tag_line#','#hub_tags#',#hub_type_id#, #hub_owner_id#, #now()#, #session.hub#, '#hub_name#','#hub_desc#',<cfif #hub_logo# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,#now()#,#hub_status#)
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(hub_id) as id from hub
	</cfquery>

    <cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into hub_xref
	 (hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_usr_role, hub_xref_joined)
	 values(#hub_owner_id#, #max.id#, 1, 1,#now()#)
    </cfquery>

</cftransaction>

<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select hub_logo from hub
	  where hub_id = #cid#
	</cfquery>

    <cfif remove.hub_logo is not "">
	 <cfif fileexists("#media_path#\#remove.hub_logo#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.hub_logo#">
	 </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete hub
	  where hub_id = #cid# and
	        hub_parent_hub_id = #session.hub#
	</cfquery>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete premium
	  where premium_hub_id = #cid#
	</cfquery>

<cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Update">

<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_logo from hub
		  where hub_id = #cid#
		</cfquery>

   	    <cfif fileexists("#media_path#\#remove.hub_logo#")>
			<cffile action = "delete" file = "#media_path#\#remove.hub_logo#">
		</cfif>

</cfif>

<cfif hub_logo is not "">

	<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select hub_logo from hub
	  where hub_id = #cid#
	</cfquery>

	<cfif #getfile.hub_logo# is not "">
	 <cfif fileexists("#media_path#\#getfile.hub_logo#")>
	 	<cffile action = "delete" file = "#media_path#\#getfile.hub_logo#">
	 </cfif>
	</cfif>

	<cffile action = "upload"
	 fileField = "hub_logo"
	 destination = "#media_path#"
	 nameConflict = "MakeUnique">

</cfif>


    <cfif hub_owner_id is old_admin_id>
    <cfelse>

		<!--- Update Old Owners Access --->

		<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update hub_xref
		 set hub_xref_usr_role = 0
		 where hub_xref_hub_id = #cid# and
			   hub_xref_usr_id = #old_admin_id#
		</cfquery>

		<!--- Check to see if new owner is a member --->

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select hub_xref_id from hub_xref
		 where hub_xref_usr_id = #hub_owner_id# and
			   hub_xref_hub_id = #cid#
		</cfquery>

		<cfif check.recordcount is 1>

			<!--- If user exists, update record --->

			<cfquery name="update_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update hub_xref
			 set hub_xref_usr_role = 1
			 where hub_xref_hub_id = #cid# and
				   hub_xref_usr_id = #hub_owner_id#
			</cfquery>

		<cfelse>

			<!--- User does not exist, create record --->

			<cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into hub_xref
			 (hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_usr_role, hub_xref_joined)
			 values(#hub_owner_id#, #cid#, 1, 1,#now()#)
			</cfquery>

		</cfif>

    </cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set hub_name = '#hub_name#',
	     hub_desc = '#hub_desc#',
	     hub_tags = '#hub_tags#',
	     hub_tag_line = '#hub_tag_line#',
	     hub_type_id = #hub_type_id#,
	     hub_city = '#hub_city#',
	     hub_state = '#hub_state#',
         hub_zip = '#hub_zip#',

		  <cfif #hub_logo# is not "">
		   hub_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   hub_logo = null,
		  </cfif>

	     hub_updated = #now()#,
	     hub_status = #hub_status#,
	     hub_owner_id = #hub_owner_id#
	 where hub_id = #cid# and
	       hub_parent_hub_id = #session.hub#

	</cfquery>

<cflocation URL="index.cfm?u=2" addtoken="no">

</cfif>

