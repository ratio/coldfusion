<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_parent_hub_id = #session.hub#
  order by hub_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Networks</td>
	       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Network</a>&nbsp;|&nbsp;<a href="/exchange/admin/index.cfm">Settings</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">Network has been successfully added.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">Network has been successfully updated.</td></tr>
        <cfelseif u is 3>
         <tr><td class="feed_sub_header" style="color: green;">Network has been deleted.</td></tr>
        </cfif>
         <tr><td>&nbsp;</td></tr>
       </cfif>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #edit.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Networks exist.</td></tr>

        <cfelse>

			<tr>
			    <td class="feed_sub_header">Network Name</td>
			    <td class="feed_sub_header">Description</td>
				<td class="feed_sub_header" align=center>Members</td>
				<td class="feed_sub_header" align=right>Status</td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfloop query="edit">

			<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select count(hub_xref_id) as total from hub_xref
			  where hub_xref_hub_id = #edit.hub_id#
			</cfquery>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

          <cfoutput>

              <td class="feed_sub_header" width=250><a href="edit.cfm?i=#encrypt(edit.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#edit.hub_name#</a></td>
              <td class="feed_sub_header" width=500 style="font-weight: normal;">#edit.hub_desc#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#members.total#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right>
              <cfif #edit.hub_status# is 0>
               Not Approved
              <cfelseif #edit.hub_status# is 1>
               Approved
              <cfelseif #edit.hub_status# is 2>
               Suspended
               </cfif></td>

          </cfoutput>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfloop>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

