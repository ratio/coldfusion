<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

   <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from hub
    where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
   </cfquery>

   <cfquery name="creator" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select * from usr
    where usr_id = #edit.hub_owner_id#
   </cfquery>

   <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from state
	order by state_name
   </cfquery>

   <cfquery name="manager" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr
	join hub_xref on hub_xref_usr_id = usr_id
	where hub_xref_hub_id = #session.hub# and
		  hub_xref_active = 1
	order by usr_last_name, usr_first_name
   </cfquery>

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Exchange</td>
			   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       	   <cfoutput>


			   <tr>
					  <td class="feed_sub_header" width=15%>Network Creator</td>
					  <td class="feed_sub_header">#creator.usr_last_name#, #creator.usr_first_name#</td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Network Name</td>
					  <td><input class="input_text" style="width: 500px;" type="text" maxlength=200 required name="hub_name" value="#edit.hub_name#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Tag Line</td>
					  <td><input class="input_text" style="width: 500px;" type="text" maxlength=100 name="hub_tag_line" value="#edit.hub_tag_line#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top>Description</td>
					  <td><textarea class="input_textarea" style="width: 800px; height: 150px;" name="hub_desc" required>#edit.hub_desc#</textarea></td>
			   </tr>

		       <tr><td class="feed_sub_header">Keywords</td>
		           <td><input class="input_text" style="width: 600px;" name="hub_tags" value="#edit.hub_tags#" maxlength="200"></td></tr>

		       <tr><td></td><td class="link_small_gray">seperate tags with a comma</td></tr>

				   <tr>
						  <td class="feed_sub_header" valign=top>Type</td>
						  <td class="feed_sub_header" style="font-weight: normal;">
						  Open Network&nbsp;<input type="radio" name="hub_type_id" value=1 <cfif #edit.hub_type_id# is 1>checked</cfif>>&nbsp;&nbsp;
						  Standard Network&nbsp;<input type="radio" name="hub_type_id" value=2 <cfif #edit.hub_type_id# is 2>checked</cfif>>&nbsp;&nbsp;
						  Private Network&nbsp;<input type="radio" name="hub_type_id" value=3 <cfif #edit.hub_type_id# is 3>checked</cfif>>&nbsp;&nbsp;
				   </tr>

				   <tr><td></td>
					   <td class="link_small_gray">- <b>Open Networks</b> are visible in the Marketplace and do not require approval to join.<br>
												   - <b>Standard Networks</b> are visible in the Marketplace and require approval to join.<br>
												   - <b>Private Networks</b> are hidden from the Marketplace and are invitation only.</td></tr>

			   <tr><td height=10></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Network Status</b></td>
					  <td>

					   <select name="hub_status" class="input_select">
				         <option value=0 <cfif #edit.hub_status# is "" or #edit.hub_status# is 0>selected</cfif>>Not Active
				         <option value=1 <cfif #edit.hub_status# is 1>selected</cfif>>Active
				       </select>

                       </td>

			   </tr>

			   <tr><td class="feed_sub_header" valign=top>Image / Logo</td>
			   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.hub_logo# is "">
					  <input type="file" name="hub_logo">
					<cfelse>
					  <img style="border-radius: 0px;" src="#media_virtual#/#edit.hub_logo#" width=100>
					  <br><br>
					  <input type="file" name="hub_logo"><br><br>
					  <font size=2><input type="checkbox" name="remove_photo">&nbsp;or, check to remove image /logo
					 </cfif>

		       </td></tr>

			   <tr><td class="feed_sub_header">Location</td>
			   <td><input class="input_text" type="text" name="hub_city" placeholder="City" style="width: 200px;" value="#edit.hub_city#" maxlength="50">&nbsp;&nbsp;

			   </cfoutput>

			   <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from state
				order by state_name
			   </cfquery>

			   <select name="hub_state" class="input_select">
				<cfoutput query="states">
				 <option value="#state_abbr#" <cfif #edit.hub_state# is #state_abbr#>selected</cfif>>#state_name#
				</cfoutput>
			   </select>&nbsp;&nbsp;

			   <cfoutput>

			   <input class="input_text" type="text" name="hub_zip" placeholder="Zipcode" style="width: 100px;" maxlength=10 value="#edit.hub_zip#" maxlength="50">

			   </td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Header Background Color</b></td>
					  <td class="feed_sub_header"><input style="font-size: 12px; width: 100px;" type="color" maxlength=10 name="hub_header_bgcolor" <cfif #edit.hub_header_bgcolor# is "">value="##022447"<cfelse>value="#edit.hub_header_bgcolor#"</cfif>></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Password Change Policy</b></td>
					  <td>

					   <select name="hub_password_policy" class="input_select">
				         <option value=0 <cfif #edit.hub_password_policy# is 0>selected</cfif>>Never
				         <option value=30 <cfif #edit.hub_password_policy# is 30>selected</cfif>>30 Days
				         <option value=60 <cfif #edit.hub_password_policy# is 60>selected</cfif>>60 Days
				         <option value=90 <cfif #edit.hub_password_policy# is 90>selected</cfif>>90 Days
				         <option value=180 <cfif #edit.hub_password_policy# is 180>selected</cfif>>180 Days
				       </select>

                       </td>

			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Home Page Sponsors</b></td>
					  <td>

					   <select name="hub_home_sponsors" class="input_select">
				         <option value=0>Do Not Display
				         <option value=1 <cfif #edit.hub_home_sponsors# is 1>selected</cfif>>Display
				       </select>

                       </td>

			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Support Team Name</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=100 name="hub_support_name" required value="#edit.hub_support_name#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Support Email Address</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=100 name="hub_support_email" required value="#edit.hub_support_email#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Support Phone Number</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=20 name="hub_support_phone" required value="#edit.hub_support_phone#"></td>
			   </tr>

               <tr><td height=10>&nbsp;</td></tr>
               <tr><td colspan=2><hr></td></tr>
               <tr><td height=10>&nbsp;</td></tr>
			   <tr>
					  <td class="feed_sub_header" width=15%><b>Login Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=300 name="hub_login_page" placeholder="leave blank to use the default page" value="#edit.hub_login_page#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Login Page Title</b></td>
					  <td><input class="input_text" style="width: 800px;" type="text" maxlength=300 value="#edit.hub_login_page_title#" name="hub_login_page_title" maxlength=300></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top>Login Page Text</td>
					  <td><textarea class="input_textarea" style="width: 800px; height: 150px;" name="hub_login_page_desc">#edit.hub_login_page_desc#</textarea></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Logout Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=300 placeholder="leave blank to use the default page" name="hub_logout_page" value="#edit.hub_logout_page#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Forgot Password Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=300 placeholder="leave blank to use the default page" name="hub_forgot_password_page" value="#edit.hub_forgot_password_page#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Allow Signup?</b></td>
					  <td>
					     <select name="hub_signup" class="input_select">
					      <option value=0>No
					      <option value=1 <cfif #edit.hub_signup# is 1>selected</cfif>>Yes
					     </select>
					  </td>
			   </tr>

               <tr><td height=10>&nbsp;</td></tr>
               <tr><td colspan=2><hr></td></tr>
               <tr><td height=10>&nbsp;</td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Main Website</b></td>
					  <td><input class="input_text" style="width: 600px;" type="url" maxlength=1000 value="#edit.hub_website#" name="hub_website"></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Unique URL</b></td>
					  <td class="feed_sub_header" style="font-weight: normal;">#session.site_url#?i=#edit.hub_encryption_key#</td></tr>

               <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
               <tr><td height=10></td></tr>

			   <tr><td></td><td>
			   <input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>
			   &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">



			   </td></tr>
           <input type="hidden" name="i" value=#i#>

           </cfoutput>

	   </table>

    </td></tr>
  </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

