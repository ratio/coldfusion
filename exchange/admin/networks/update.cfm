<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

<cftransaction>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set hub_status = #hub_status#
	 where hub_id = #hub_id#
	</cfquery>

	<cfquery name="delete_premium" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete premium
	 where premium_hub_id = #hub_id#
	</cfquery>

	<cfquery name="update_premium" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into premium
	 (premium_hub_id, premium_opps, premium_hubs, premium_marketplace, premium_vehicle, premium_awards, premium_forecast, premium_competitor, premium_sbir, premium_grants, premium_updated)
	 values
	 (
	  #hub_id#,
	  <cfif isdefined("premium_opps")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_hubs")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_marketplace")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_vehicle")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_awards")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_forecast")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_competitor")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_sbir")>1<cfelse>null</cfif>,
	  <cfif isdefined("premium_grants")>1<cfelse>null</cfif>,
	  #now()#
	 )
	</cfquery>

</cftransaction>

	<cfif orig_status is 0 and hub_status is 1>

	<cfquery name="hub_owner" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select hub_owner_id from hub
	 where hub_id = #hub_id#
	</cfquery>

	<cfquery name="user_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #hub_owner.hub_owner_id#
	</cfquery>

    <!--- Email User --->

	<cfmail from="The Exchange <noreply@ratio.exchange>"
			  to="#user_info.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Your new Exchange HUB has been approved!">

	<html>
	<head>
	<title>RATIO Exchange</title>
	</head><div class="center">
	<body class="body">
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <cfoutput>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Thank you for creating a new HUB on the Exchange.
		 <p>
		 Your HUB has been approved and it is now available on the Exchange.</td></tr>

         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Exchange Support Team</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
		 <tr><td>&nbsp;</td></tr>
	 </cfoutput>
	</table>
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
	 <tr><td>&nbsp;</td></tr>
	 <tr><td height=75 valign=middle><img src="#image_virtual#/exchange_logo.png" width=160 hspace=5></td></tr>
	</table>
	</body>
	</html>
	</cfmail>

  </cfif>

</cfif>

<cflocation URL="index.cfm?u=2" addtoken="no">