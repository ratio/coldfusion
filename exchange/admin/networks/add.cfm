<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="creator" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="products" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from product_pack
 where product_pack_public = 1
 order by product_pack_name
</cfquery>

<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where hub_xref_hub_id = #session.hub# and
 	   hub_xref_active = 1
 order by usr_last_name, usr_first_name
</cfquery>

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Network</td>
			   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Network Creator</b></td>
					  <td class="feed_sub_header"><cfoutput>#creator.usr_first_name# #creator.usr_last_name#</cfoutput>

                       </td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Network Name</td>
					  <td><input class="input_text" style="width: 500px;" type="text" maxlength=200 required name="hub_name"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%>Tag Line</td>
					  <td><input class="input_text" style="width: 500px;" type="text" maxlength=100 name="hub_tag_line"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top>Description</td>
					  <td><textarea class="input_textarea" style="width: 800px; height: 150px;" name="hub_desc" required></textarea></td>
			   </tr>

		       <tr><td class="feed_sub_header">Keywords</td>
		           <td><input class="input_text" style="width: 600px;" name="hub_tags" maxlength="200"></td></tr>

		       <tr><td></td><td class="link_small_gray">seperate tags with a comma</td></tr>


				   <tr>
						  <td class="feed_sub_header" valign=top>Type</td>
						  <td class="feed_sub_header" style="font-weight: normal;">
						  Open Network&nbsp;<input type="radio" name="hub_type_id" value=1>&nbsp;&nbsp;
						  Standard Network&nbsp;<input type="radio" name="hub_type_id" value=2 checked>&nbsp;&nbsp;
						  Private Network&nbsp;<input type="radio" name="hub_type_id" value=3>&nbsp;&nbsp;
				   </tr>

				   <tr><td></td>
					   <td class="link_small_gray">- <b>Open Networks</b> are visible in the Marketplace and do not require approval to join.<br>
												   - <b>Standard Networks</b> are visible in the Marketplace and require approval to join.<br>
												   - <b>Private Networks</b> are hidden from the Marketplace and are invitation only.</td></tr>

			   <tr><td height=10></td></tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Starting Template</b></td>
					  <td>

					   <select name="product_pack" class="input_select">
				         <option value=0>Custom
				         <cfoutput query="products">
				          <option value=#product_pack_id#>#product_pack_name#
				         </cfoutput>
				       </select>

				       <span class="link_small_gray">This is used to provision the starting profiles and menus.  It cannot be changed after creating.</span>

                       </td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Network Status</b></td>
					  <td>

					   <select name="hub_status" class="input_select">
				         <option value=0>Not Active
				         <option value=1 selected>Active
				       </select>

                       </td>

			   </tr>

			   <tr><td class="feed_sub_header" valign=top>Image / Logo</td>
			   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					  <input type="file" name="hub_logo">

		       </td></tr>

			   <tr><td class="feed_sub_header">Location</td>
			   <td><input class="input_text" type="text" name="hub_city" placeholder="City" style="width: 200px;" maxlength="50">&nbsp;&nbsp;

			   <select name="hub_state" class="input_select">
				<cfoutput query="states">
				 <option value="#state_abbr#">#state_name#
				</cfoutput>
			   </select>&nbsp;&nbsp;

			   <input class="input_text" type="text" name="hub_zip" placeholder="Zipcode" style="width: 100px;" maxlength=10 maxlength="50">

			   </td></tr>

			   <tr>
				  <td class="feed_sub_header" width=15%><b>Header Background Color</b></td>
				  <td class="feed_sub_header"><input style="font-size: 12px; width: 100px;" type="color" maxlength=10 name="hub_header_bgcolor"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Password Change Policy</b></td>
					  <td>

					   <select name="hub_password_policy" class="input_select">
				         <option value=0>Never
				         <option value=30>30 Days
				         <option value=60>60 Days
				         <option value=90>90 Days
				         <option value=180>180 Days
				       </select>

                       </td>

			   </tr>


			   <tr>
					  <td class="feed_sub_header" width=15%><b>Allow Signup?</b></td>
					  <td>
					     <select name="hub_signup" class="input_select">
					      <option value=0>No
					      <option value=1>Yes
					     </select>
					  </td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=20%><b>Support Team Name</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=100 name="hub_support_name" required></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Support Email Address</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=100 name="hub_support_email" required></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Support Phone Number</b></td>
					  <td><input class="input_text" style="width: 300px;" type="text" maxlength=20 name="hub_support_phone" required></td>
			   </tr>

               <tr><td height=10>&nbsp;</td></tr>
               <tr><td colspan=2><hr></td></tr>
               <tr><td height=10>&nbsp;</td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Login Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="url" placeholder="leave blank to use the default page" maxlength=300 name="hub_login_page"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Login Page Title</b></td>
					  <td><input class="input_text" style="width: 800px;" type="text" maxlength=300 name="hub_login_page_title" maxlength=300></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top>Login Page Text</td>
					  <td><textarea class="input_textarea" style="width: 800px; height: 150px;" name="hub_login_page_desc"></textarea></td>
			   </tr>


			   <tr>
					  <td class="feed_sub_header" width=15%><b>Logout Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="url" placeholder="leave blank to use the default page" maxlength=300 name="hub_logout_page"></td>
			   </tr>


			   <tr>
					  <td class="feed_sub_header" width=15%><b>Forgot Password Page</b></td>
					  <td><input class="input_text" style="width: 600px;" type="url" placeholder="leave blank to use the default page" maxlength=300 name="hub_forgot_password_page"></td>
			   </tr>

               <tr><td height=10>&nbsp;</td></tr>
               <tr><td colspan=2><hr></td></tr>
               <tr><td height=10>&nbsp;</td></tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Main Website</b></td>
					  <td><input class="input_text" style="width: 600px;" type="url" maxlength=1000 name="hub_website"></td></tr>

               <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
               <tr><td height=10></td></tr>

               <cfoutput>
               <input type="hidden" name="hub_owner_id" value=#session.usr_id#>
               </cfoutput>

			   <tr><td></td><td>
			   <input class="button_blue_large" type="submit" name="button" value="Add" vspace=10>

			   </td></tr>

	   </table>

</td></tr>
</table>

	   </form>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

