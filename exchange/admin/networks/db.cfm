<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

    <cftransaction>

    <cfset encryption_code_1 = #randrange(10,210000)#>
    <cfset encryption_code_2 = #randrange(1000000,2100000000)#>
    <cfset encryption_code_3 = #randrange(33240,98839238)#>
    <cfset encryption_key = #tobase64(encryption_code_2)#>
    <cfset e_code = #encryption_code_1#&#encryption_key#&#encryption_code_3#>

    <cfset loginout = #session.site_url# & "?i=" & #e_code#>

	<cfif #hub_logo# is not "">
		<cffile action = "upload"
		 fileField = "hub_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert hub
		  (
		  hub_name,
		  hub_type_id,
		  hub_parent_hub_id,
		  hub_owner_id,
		  hub_signup,
		  hub_website,
		  hub_login_page_title,
		  hub_login_page_desc,
		  hub_encryption_key,
		  hub_status,
		  hub_desc,
		  hub_logout_page,
		  hub_login_page,
		  hub_forgot_password_page,
		  hub_city,
		  hub_zip,
		  hub_state,
		  hub_password_policy,
		  hub_tags,
		  hub_tag_line,
		  hub_header_bgcolor,
		  hub_logo,
		  hub_updated,
		  hub_support_name,
		  hub_support_email,
		  hub_support_phone
		  )
		  values
		  (
		  '#hub_name#',
		   #hub_type_id#,
		   #session.hub#,
		   #hub_owner_id#,
		   #hub_signup#,
		  '#hub_website#',
		  '#hub_login_page_title#',
		  '#hub_login_page_desc#',
		  '#e_code#',
		   #hub_status#,
		  '#hub_desc#',
		  '#loginout#',
		  '#loginout#',
		  '#loginout#',
		  '#hub_city#',
		  '#hub_zip#',
		  '#hub_state#',
		   #hub_password_policy#,
		  '#hub_tags#',
		  '#hub_tag_line#',
		  '#hub_header_bgcolor#',

		  <cfif #hub_logo# is not "">
		   '#cffile.serverfile#',
		  <cfelse>
			null,
		  </cfif>
		   #now()#,
		  '#hub_support_name#',
		  '#hub_support_email#',
		  '#hub_support_phone#'
		   )
		</cfquery>

		<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(hub_id) as id from hub
		</cfquery>

		<cfquery name="lens_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into lens (lens_name, lens_order, lens_desc, lens_hub_id)
		 values ('Announcements',1,null,#max.id#)
		</cfquery>

		<cfquery name="lens_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into lens (lens_name, lens_order, lens_desc, lens_hub_id)
		 values ('Events',2,null,#max.id#)
		</cfquery>

		<cfquery name="lens_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into lens (lens_name, lens_order, lens_desc, lens_hub_id)
		 values ('Updates',3,null,#max.id#)
		</cfquery>

		<cfquery name="insert_role" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into hub_role
		 (
		  hub_role_name,
		  hub_role_hub_id
		  )
		  values
		  (
		  'Admin Users',
		   #max.id#
		  )
		</cfquery>

		<cfquery name="max_role" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(hub_role_id) as id from hub_role
		 where hub_role_hub_id = #max.id#
		</cfquery>

		<cfquery name="add_user_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref
		 (
		  hub_xref_usr_id,
		  hub_xref_hub_id,
		  hub_xref_role_id,
		  hub_xref_active,
		  hub_xref_company_add,
		  hub_xref_usr_role
		  )
		  values
		  (
		  #hub_owner_id#,
		  #max.id#,
		  #max_role.id#,
		  1,
		  1,
		  1
		  )
		</cfquery>

    <cfif product_pack is not 0>

    <!--- Get Product Pack Profiles Information --->

	<cfquery name="get_product_profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from product_profile
	 where product_profile_product_id = #product_pack#
	</cfquery>

	<cfloop query="get_product_profiles">

		<cfquery name="get_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from hub_role
		 where hub_role_id = #get_product_profiles.product_profile_profile_id#
		</cfquery>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into hub_role
			 (
			 hub_role_name,
			 hub_role_hub_id,
			 hub_role_desc
			 )
			 values
			 (
			 '#get_profile.hub_role_name#',
			  #max.id#,
			 '#get_profile.hub_role_desc#'
			 )
		</cfquery>

		<cfquery name="max_hub_role" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(hub_role_id) as id from hub_role
		</cfquery>

          <!--- Update Homepage --->

		  <cfquery name="home" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from home_section
		   where home_section_hub_id = 0 and
		         home_section_profile_id = #get_product_profiles.product_profile_profile_id#
		  </cfquery>

          <cfloop query="home">

			  <cfquery name="home_insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   insert into home_section
			   (
				home_section_profile_id,
				home_section_hub_id,
				home_section_order,
				home_section_title,
				home_section_desc,
				home_section_type_id,
				home_section_embed,
				home_section_name,
				home_section_content,
				home_section_component_id,
				home_section_location,
				home_section_display,
				home_section_active
			   )
			   values
			   (
			   #max_hub_role.id#,
			   #max.id#,
			   #home.home_section_order#,
			  '#home.home_section_title#',
			  '#home.home_section_desc#',
			   <cfif #home.home_section_type_id# is "">null<cfelse>#home.home_section_type_id#</cfif>,
			  '#home.home_section_embed#',
			  '#home.home_section_name#',
			  '#home.home_section_content#',
			   #home.home_section_component_id#,
			  '#home.home_section_location#',
			   #home.home_section_display#,
			   #home.home_section_active#
			   )
			  </cfquery>

          </cfloop>

		<cfquery name="parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from menu
		 where menu_role_id = #get_product_profiles.product_profile_profile_id# and
			   menu_parent_id = 0 and
			   menu_hub_id = 0
		</cfquery>

		<cfloop query="parent">

			<cfquery name="insert_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into menu
			 (
			  menu_name,
			  menu_active,
			  menu_order,
			  menu_role_id,
			  menu_hub_id,
			  menu_parent_id,
			  menu_icon,
			  menu_type_id,
			  menu_url,
			  menu_app_id,
			  menu_embed,
			  menu_custom_path
			  )
			  values
			  (
			  '#parent.menu_name#',
			   #parent.menu_active#,
			   #parent.menu_order#,
			   #max_hub_role.id#,
			   #max.id#,
			   0,
			  '#parent.menu_icon#',
			   #parent.menu_type_id#,
			  '#parent.menu_url#',
			   #parent.menu_app_id#,
			  '#parent.menu_embed#',
			  '#parent.menu_custom_path#'
			   )
			</cfquery>

			<cfquery name="max_menu_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select max(menu_id) as id from menu
			</cfquery>

			<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from menu
			 where menu_role_id = #get_product_profiles.product_profile_profile_id# and
				   menu_parent_id = #parent.menu_id# and
				   menu_hub_id = 0
			</cfquery>

			<cfloop query="child">

				<cfquery name="insert_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert into menu
				 (
				  menu_name,
				  menu_active,
				  menu_order,
				  menu_role_id,
				  menu_hub_id,
				  menu_parent_id,
				  menu_icon,
				  menu_type_id,
				  menu_url,
				  menu_app_id,
				  menu_embed,
				  menu_custom_path
				  )
				  values
				  (
				  '#child.menu_name#',
				   #child.menu_active#,
				   #child.menu_order#,
				   #max_hub_role.id#,
				   #max.id#,
				   #max_menu_parent.id#,
				  '#child.menu_icon#',
				   #child.menu_type_id#,
				  '#child.menu_url#',
				   #child.menu_app_id#,
				  '#child.menu_embed#',
				  '#child.menu_custom_path#'
				   )
				</cfquery>

					<cfquery name="max_menu_grandchild" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select max(menu_id) as id from menu
					</cfquery>

					<cfquery name="grandchild" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from menu
					 where menu_role_id = #get_product_profiles.product_profile_profile_id# and
						   menu_parent_id = #child.menu_id# and
						   menu_hub_id = 0
					</cfquery>

					<cfloop query="grandchild">

						<cfquery name="insert_parent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 insert into menu
						 (
						  menu_name,
						  menu_active,
						  menu_order,
						  menu_role_id,
						  menu_hub_id,
						  menu_parent_id,
						  menu_icon,
						  menu_type_id,
						  menu_url,
						  menu_app_id,
						  menu_embed,
						  menu_custom_path
						  )
						  values
						  (
						  '#grandchild.menu_name#',
						   #grandchild.menu_active#,
						   #grandchild.menu_order#,
						   #max_hub_role.id#,
						   #max.id#,
						   #max_menu_grandchild.id#,
						  '#grandchild.menu_icon#',
						   #grandchild.menu_type_id#,
						  '#grandchild.menu_url#',
						   #grandchild.menu_app_id#,
						  '#grandchild.menu_embed#',
						  '#grandchild.menu_custom_path#'
						   )
						</cfquery>

					</cfloop>

				</cfloop>

	        </cfloop>

          </cfloop>

	<cfquery name="getapps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select distinct(menu_app_id) as app_id from menu
	 where menu_hub_id = #max.id# and
	       (menu_app_id <> 0 or menu_app_id is not null)
	</cfquery>

	<cfloop query="getapps">
		<cfquery name="getapps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into hub_app(hub_app_app_id, hub_app_hub_id, hub_app_installed)
		  values(#app_id#,#max.id#,#now()#)
	    </cfquery>
	</cfloop>

	<!--- Insert Default Alignments --->

	<cfquery name="market_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into market(market_name, market_hub_id)
	  values('Federal',#max.id#)
    </cfquery>

	<cfquery name="market_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into market(market_name, market_hub_id)
	  values('Commercial',#max.id#)
    </cfquery>

	<cfquery name="market_3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into market(market_name, market_hub_id)
	  values('State & Local',#max.id#)
    </cfquery>

	<cfquery name="market_4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into market(market_name, market_hub_id)
	  values('International',#max.id#)
    </cfquery>

	<cfquery name="sector_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into sector(sector_name, sector_hub_id)
	  values('Financial',#max.id#)
    </cfquery>

	<cfquery name="sector_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into sector(sector_name, sector_hub_id)
	  values('Healthcare',#max.id#)
    </cfquery>

	<cfquery name="sector_3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into sector(sector_name, sector_hub_id)
	  values('Energy',#max.id#)
    </cfquery>

	<cfquery name="sector_4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into sector(sector_name, sector_hub_id)
	  values('Consumer Products',#max.id#)
    </cfquery>

	<cfquery name="topic_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into topic(topic_name, topic_hub_id)
	  values('Machine Learning (ML)',#max.id#)
    </cfquery>

	<cfquery name="topic_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into topic(topic_name, topic_hub_id)
	  values('Artificial Intelligence (AI)',#max.id#)
    </cfquery>

	<cfquery name="topic_3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into topic(topic_name, topic_hub_id)
	  values('Wireless / Mobility',#max.id#)
    </cfquery>

	<cfquery name="topic_4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into topic(topic_name, topic_hub_id)
	  values('Manufacturing',#max.id#)
    </cfquery>

    </cfif>

    </cftransaction>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	    <cfquery name="code" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select hub_encryption_key from hub
	     where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	    </cfquery>

        <cfif isdefined("remove_photo")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select hub_logo from hub
			  where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.hub_logo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.hub_logo#">
        	</cfif>

        </cfif>

		<cfif #hub_logo# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select hub_logo from hub
			  where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
			</cfquery>

			<cfif #getfile.hub_logo# is not "">
             <cfif FileExists("#media_path#\#getfile.hub_logo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.hub_logo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "hub_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub
	 set hub_name = '#hub_name#',
	     hub_website = '#hub_website#',
	     hub_signup = #hub_signup#,
	     hub_login_page_title = '#hub_login_page_title#',
	     hub_login_page_desc = '#hub_login_page_desc#',
	     hub_status = #hub_status#,
         hub_type_id = #hub_type_id#,
	     hub_desc = '#hub_desc#',
	     hub_logout_page = '#hub_logout_page#',
	     hub_login_page = '#hub_login_page#',
	     hub_forgot_password_page = '#hub_forgot_password_page#',
	     hub_city = '#hub_city#',
	     hub_zip = '#hub_zip#',
	     hub_state = '#hub_state#',
	     hub_password_policy = #hub_password_policy#,
	     hub_tags = '#hub_tags#',
	     hub_tag_line = '#hub_tag_line#',
	     hub_header_bgcolor = '#hub_header_bgcolor#',

		  <cfif #hub_logo# is not "">
		   hub_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_photo")>
		   hub_logo = null,
		  </cfif>

	     hub_updated = #now()#,
	     hub_support_name = '#hub_support_name#',
	     hub_support_email = '#hub_support_email#',
	     hub_support_phone = '#hub_support_phone#'
	 where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif button is "Delete">

<cftransaction>

	<cfquery name="delete_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete hub
  	 where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete hub_xref
  	 where hub_xref_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete hub_app
  	 where hub_app_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete hub_role
  	 where hub_role_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_5" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete home_section
  	 where home_section_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_6" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete market
  	 where market_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_7" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete sector
  	 where sector_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_8" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete topic
  	 where topic_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_9" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete state
  	 where state_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cfquery name="delete_10" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  	 delete menu
  	 where menu_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

</cftransaction>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

