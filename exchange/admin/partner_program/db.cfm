<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into partner_program (partner_program_name, partner_program_desc, partner_program_hub_id, partner_program_order)
	 values ('#partner_program_name#','#partner_program_desc#',#session.hub#,#partner_program_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update partner_program
	 set partner_program_name = '#partner_program_name#',
	     partner_program_desc = '#partner_program_desc#',
	     partner_program_order = #partner_program_order#
	 where partner_program_id = #partner_program_id# and
	       partner_program_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete partner_program
	 where partner_program_id = #partner_program_id# and
	       partner_program_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

