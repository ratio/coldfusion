<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from section_field
  where section_field_id = #section_field_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <form action="field_db.cfm" method="post">

	  <div class="main_box">

	  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">EDIT FIELD</td><td class="feed_sub_header" align=right><a href="fields.cfm?section_id=#section_id#">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header">Field Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="section_field_name" required value="#edit.section_field_name#"></td></tr>
		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea class="input_textarea" style="width: 600px; height: 100px;" name="section_field_desc">#edit.section_field_desc#</textarea></td></tr>
		   <tr><td class="feed_sub_header">Section Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="section_field_order" step=".1" value=#edit.section_field_order# required></td></tr>
		   <tr><td class="feed_sub_header">Field Type</td>
		   <td>
		   <select name="section_field_type" class="input_select">
		   <option value=1 <cfif #edit.section_field_type# is 1>selected</cfif>>Text Field
		   <option value=2 <cfif #edit.section_field_type# is 2>selected</cfif>>Memo Field
		   </select></td></tr>

		   <input type="hidden" name="section_field_section_id" value=#section_id#>
		   <input type="hidden" name="section_field_id" value=#section_field_id#>

		   </cfoutput>

		   <tr><td height=10></td></tr>
		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record.\r\nAre you sure you want to delete this record?');"></td></tr>


		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

