<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <form action="section_db.cfm" method="post">

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">ADD SECTION</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header">Section Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="section_name" required></td></tr>
		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea class="input_textarea" style="width: 600px; height: 100px;" name="section_desc"></textarea></td></tr>
		   <tr><td class="feed_sub_header">Section Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="section_order" step=".1" required></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

