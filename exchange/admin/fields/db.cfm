<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into topic (topic_name, topic_hub_id, topic_parent_id)
	 values ('#topic_name#',#session.hub#, #topic_parent_id#)
	</cfquery>

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete topic
	 where topic_id = #topic_id#
	</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update topic
	 set topic_name = '#topic_name#',
	     topic_parent_id = #topic_parent_id#
	 where topic_id = #topic_id#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">