<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from section
  where section_id = #section_id#
</cfquery>

<cfquery name="fields" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from section_field
  where section_field_section_id = #section_id#
  order by section_field_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">CUSTOM FIELDS - <cfoutput>#edit.section_name#</cfoutput></td>
	   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
       <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header"><a href="field_add.cfm?section_id=<cfoutput>#section_id#</cfoutput>">Add Field</a></td></tr>
	   <tr><td height=10></td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #fields.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No fields exist.</td></tr>

        <cfelse>

			<tr>
			<td class="feed_sub_header">Order</td>
			<td class="feed_sub_header">Field Name</td>
			<td class="feed_sub_header">Description</td>
			<td class="feed_sub_header">Field Type</td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="fields">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top width=75><a href="field_edit.cfm?section_field_id=#section_field_id#&section_id=#section_id#">#section_field_order#</a></td>
              <td class="feed_sub_header" valign=top width=200><a href="field_edit.cfm?section_field_id=#section_field_id#&section_id=#section_id#">#section_field_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">#section_field_desc#</td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">

              <cfif #section_field_type# is 1>
               Text Field
              <cfelseif #section_field_type# is 2>
               Memo Field
              </cfif>

              </td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

