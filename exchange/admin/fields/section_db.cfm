<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into section (section_name, section_desc, section_hub_id, section_order)
	 values ('#section_name#','#section_desc#', #session.hub#,#section_order#)
	</cfquery>

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete section
	 where section_id = #section_id#
	</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update section
	 set section_name = '#section_name#',
	     section_desc = '#section_desc#',
	     section_order = #section_order#
	 where section_id = #section_id# and
	       section_hub_id = #session.hub#
	</cfquery>

</cfif>

<cflocation URL="index.cfm" addtoken="no">