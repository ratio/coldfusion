<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

<cfset section_field_unique_id = "#session.hub##randrange(100,999999999)##section_field_section_id#">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into section_field (section_field_unique_id, section_field_section_id, section_field_name, section_field_desc, section_field_type, section_field_order)
	 values ('#section_field_unique_id#', #section_field_section_id#, '#section_field_name#','#section_field_desc#', #section_field_type#,#section_field_order#)
	</cfquery>

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete section_field
	 where section_field_id = #section_field_id#
	</cfquery>

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update section_field
	 set section_field_name = '#section_field_name#',
	     section_field_desc = '#section_field_desc#',
	     section_field_order = #section_field_order#,
	     section_field_type = #section_field_type#
	 where section_field_id = #section_field_id#
	</cfquery>

</cfif>

<cflocation URL="fields.cfm?section_id=#section_field_section_id#" addtoken="no">