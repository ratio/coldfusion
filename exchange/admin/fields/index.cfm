<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="sections" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from section
	  where section_hub_id = #session.hub#
	  order by section_order
	</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">


      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">CUSTOM COMPANY PROFILE FIELDS</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/">Admin Tools</td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_sub_header" align=right colspan=2><a href="section_add.cfm">Add Section</a></td></tr>
	   <tr><td height=10></td></tr>
      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #sections.recordcount# is 0>

        <tr><td class="feed_sub_header">No Sections exist.</td></tr>

        <cfelse>

			<tr>
			<td class="feed_sub_header">Order</td>
			<td class="feed_sub_header">Section Name</td>
			<td class="feed_sub_header">Description</td>
			<td>&nbsp;</td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="sections">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top width=75><a href="fields.cfm?section_id=#section_id#">#section_order#</a></td>
              <td class="feed_sub_header" valign=top width=300><a href="fields.cfm?section_id=#section_id#">#section_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #section_desc# is "">No description provided<cfelse>#section_desc#</cfif></td>
              <td class="feed_sub_header" valign=top align=right><a href="section_edit.cfm?section_id=#section_id#">Edit</a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

