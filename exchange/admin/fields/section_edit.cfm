<cfinclude template="/exchange/security/check.cfm">


 <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from section
  where section_id = #section_id#
 </cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <form action="section_db.cfm" method="post">

	  <div class="main_box">

	  <cfoutput>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">EDIT SECTION</td>
		       <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header">Section Name</td>
		       <td><input class="input_text" style="width: 400px;" type="text" name="section_name" value="#edit.section_name#" required></td></tr>
		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea class="input_textarea" style="width: 600px; height: 100px;" name="section_desc">#edit.section_desc#</textarea></td></tr>
		   <tr><td class="feed_sub_header">Section Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="section_order" step=".1" value=#edit.section_order# required></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td></td>
		       <td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record.\r\nAre you sure you want to delete this record?');"></td></tr>
		  </table>

		  <input type="hidden" name="section_id" value=#section_id#>

	   </cfoutput>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

