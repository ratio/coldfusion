<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from topic
 where topic_id = #topic_id#
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
       <cfinclude template="/exchange/hubs/admin/admin_menu.cfm">
	  </div>

      </td><td valign=top>

	  <form action="db.cfm" method="post">

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Capability</td>
		       <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <cfoutput>
			   <tr><td class="feed_option"><b>Capability Name</b></td></tr>
			   <tr><td><input style="font-size: 12px; width: 320px;" type="text" name="topic_name" value="#info.topic_name#" required></td></tr>
		       <input type="hidden" name="topic_id" value=#topic_id#>
		   </cfoutput>

			   <tr><td height=10></td></tr>
			   <tr><td class="feed_option"><b>Align to EXCHANGE Capability or Service</b></td></tr>
			   <tr><td><select name="topic_parent_id">
			            <cfoutput query="parent">
			             <option value=#topic_id# <cfif #topic_id# is #info.topic_parent_id#>selected</cfif>>#topic_name#
			            </cfoutput></td></tr>

			   <tr><td><input class="button_blue" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record.\r\nAre you sure you want to delete this record?');"></td></tr>

		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

