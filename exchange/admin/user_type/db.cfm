<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into usr_type (usr_type_name, usr_type_hub_id, usr_type_order)
	 values ('#usr_type_name#',#session.hub#,#usr_type_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update usr_type
	 set usr_type_name = '#usr_type_name#',
	     usr_type_order = #usr_type_order#
	 where usr_type_id = #usr_type_id# and
	       usr_type_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete usr_type
	 where usr_type_id = #usr_type_id# and
	       usr_type_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

