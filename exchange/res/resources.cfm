<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cat_name" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res_cat
 where res_cat_id = #session.resource_id#
</cfquery>

<cfquery name="resources" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res
 where res_hub_id = #session.hub# and
       res_cat_id = #session.resource_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">
        <cfinclude template="search_resources.cfm">
	  </div>


      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfoutput>
        <tr>
           <td class="feed_header">#cat_name.res_cat_name#</td>
           <td class="feed_sub_header" align=right><a href="index.cfm">All Resources</a></td>
        </tr>
        <tr><td colspan=2><hr></td></tr>
        </cfoutput>

      </table>

      <cfif resources.recordcount is 0>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No resources were found for this category.</td></tr>

        </table>


      <cfelse>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			<cfset count = 1>

			<cfoutput query="resources">

			   <tr>
				   <td width=70>

					 <a href="open.cfm?i=#resources.res_id#">

					 <cfif resources.res_image is "">
					  <img src="//logo.clearbit.com/#resources.res_domain#" width=40 height=40 border=0 onerror="this.src='/images/no_logo.png'">
					 <cfelse>
					  <img src="#media_virtual#/#resources.res_image#" width=40 height=0 border=0>
					 </cfif>

					 </a>

				  </td>

				  <td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" style="padding-bottom: 0px; padding-top: 0px;"><a href="open.cfm?i=#res_id#"><b>#res_name#</b></a></td>
						<td class="feed_sub_header" align=right>#dateformat(res_updated,'mmm dd')#</td></tr>
					<tr><td colspan=2 class="feed_sub_header" style="padding-top: 5px; font-weight: normal;">#res_desc_short#</td></tr>
				  </table>

				  </td></tr>

			   <cfif count LT resources.recordcount>
			   <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   </cfif>

			   <cfset count = count + 1>

			   </cfoutput>

			   <tr><td height=20></td></tr>

			   </table>


	  </td></tr>

 </table>

 </cfif>

 	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>