<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #res_cat_image# is not "">
		<cffile action = "upload"
		 fileField = "res_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into res_cat (res_cat_image, res_cat_hub_id, res_cat_name, res_cat_desc,res_cat_order,res_cat_updated, res_cat_usr_id)
	 values (<cfif #res_cat_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,#session.hub#,'#res_cat_name#','#res_cat_desc#',<cfif #res_cat_order# is "">null<cfelse>#res_cat_order#</cfif>,#now()#,#session.usr_id#)
	</cfquery>

  <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete res_cat
	 where res_cat_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete res
	 where res_cat_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

</cftransaction>

<cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Update">


	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select res_cat_image from res_cat
		  where res_cat_id = #res_cat_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.res_cat_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.res_cat_image#">
		</cfif>

	</cfif>

	<cfif #res_cat_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select res_cat_image from res_cat
		  where res_cat_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.res_cat_image# is not "">
         <cfif fileexists("#media_path#\#getfile.res_cat_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.res_cat_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "res_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update res_cat
	 set res_cat_name = '#res_cat_name#',

		  <cfif #res_cat_image# is not "">
		   res_cat_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   res_cat_image = null,
		  </cfif>

	     res_cat_desc = '#res_cat_desc#',
	     res_cat_updated = #now()#,
	     res_cat_usr_id = #session.usr_id#,
	     res_cat_order = <cfif #res_cat_order# is "">null<cfelse>#res_cat_order#</cfif>
	 where res_cat_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       res_cat_hub_id = #session.hub#
	</cfquery>

<cflocation URL="index.cfm?u=2" addtoken="no">

</cfif>

