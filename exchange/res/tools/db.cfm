<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #res_file# is not "">
		<cffile action = "upload"
		 fileField = "res_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into res
	 (res_hub_id,
	  res_file,
	  res_cat_id,
	  res_name,
	  res_desc_short,
	  res_desc_long,
	  res_domain,
	  res_link,
	  res_keywords,
	  res_order,
	  res_updated,
	  res_usr_id
	  )
	 values
	 (#session.hub#,
     <cfif #res_file# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	  #res_cat_id#,
	 '#res_name#',
	 '#res_desc_short#',
	 '#res_desc_long#',
	 '#res_domain#',
	 '#res_link#',
	 '#res_keywords#',
	 <cfif #res_order# is "">null<cfelse>#res_order#</cfif>,
	  #now()#,
	  #session.usr_id#
	  )
	</cfquery>

<cflocation URL="index.cfm?u=4" addtoken="no">

<cfelseif #button# is "Delete">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select res_file from res
		  where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif remove.res_file is not "">
         <cfif fileexists("#media_path#\#remove.res_file#")>
			 <cffile action = "delete" file = "#media_path#\#remove.res_file#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete res
	 where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       res_hub_id = #session.hub#
	</cfquery>

   </cftransaction>

<cflocation URL="index.cfm?u=6" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select res_file from res
		  where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.res_file#")>
			<cffile action = "delete" file = "#media_path#\#remove.res_file#">
		</cfif>

	</cfif>

	<cfif #res_file# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select res_file from res
		  where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.res_file# is not "">
         <cfif fileexists("#media_path#\#getfile.res_file#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.res_file#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "res_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update res
	 set res_name = '#res_name#',

		  <cfif #res_file# is not "">
		   res_file = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   res_file = null,
		  </cfif>

	     res_desc_short = '#res_desc_short#',
	     res_desc_long = '#res_desc_long#',
	     res_domain = '#res_domain#',
	     res_keywords = '#res_keywords#',
	     res_link = '#res_link#',
	     res_cat_id = #res_cat_id#,
	     res_order = <cfif #res_order# is "">null<cfelse>#res_order#</cfif>,
	     res_updated = #now()#,
	     res_usr_id = #session.usr_id#
	 where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       res_hub_id = #session.hub#
	</cfquery>

<cflocation URL="index.cfm?u=5" addtoken="no">

</cfif>

