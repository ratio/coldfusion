<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res_cat
 where res_cat_hub_id = #session.hub#
 order by res_cat_order
</cfquery>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res
 where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       res_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Resource</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header">Resource Name</td>
		       <td><input class="input_text" style="width: 600px;" type="text" value="#edit.res_name#" name="res_name"></td></tr>
		   <tr><td valign=top class="feed_sub_header">Short Description</td>
		       <td><textarea class="input_textarea" style="width: 800px; height: 100px;" name="res_desc_short">#edit.res_desc_short#</textarea></td></tr>
		   <tr><td valign=top class="feed_sub_header">Long Description</td>
		       <td><textarea class="input_textarea" style="width: 800px; height: 300px;" name="res_desc_long">#edit.res_desc_long#</textarea></td></tr>

		   <tr><td class="feed_sub_header">Domain</td>
		       <td><input class="input_text" style="width: 300px;" type="text" value="#edit.res_domain#" name="res_domain">&nbsp;<span class="link_small_gray">i.e., www.websitename.org (do not include http://)</span></td></tr>

		   <tr><td class="feed_sub_header">Full URL</td>
		       <td><input class="input_text" style="width: 800px;" type="url" value="#edit.res_link#" name="res_link"></td></tr>

		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" style="width: 800px;" type="text" value="#edit.res_keywords#" name="res_keywords"></td></tr>

           <tr><td class="feed_sub_header" valign=top>Upload Document</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.res_file# is "">
					  <input type="file" name="res_file">
					<cfelse>
					  <a href="#media_virtual#/#edit.res_file#" target="_blank" rel="noopener">Download Current Document</a><br><br>
					  <input type="file" name="res_file"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					 </cfif>

					 </td></tr>

           </cfoutput>

		   <tr><td class="feed_sub_header">Category</td>
		   <td>
		   <select name="res_cat_id" class="input_select">
		   <cfoutput query="cats">
		    <option value=#res_cat_id# <cfif #edit.res_cat_id# is #res_cat_id#>selected</cfif>>#res_cat_name#
		   </cfoutput>
		   </select></td></tr>

		   <cfoutput>

		   <tr><td class="feed_sub_header">Display Order</td>
		       <td><input class="input_text" style="width: 100px;" required type="number" value=#edit.res_order# name="res_order" step=.1></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>
		   &nbsp;
		   <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>
		     <input type="hidden" name="i" value=#i#>
		   </cfoutput>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>