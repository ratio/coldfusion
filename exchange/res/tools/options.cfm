<cfquery name="res_cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from res_cat
  where res_cat_hub_id = #session.hub#
  order by res_cat_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header">Resource Library Configuration</td>
      <td class="feed_sub_header" align=right><a href="/exchange/admin/apps/myapps.cfm">My Apps</a></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td height=10></td></tr>
</table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header"><a href="cat_add.cfm">Add Resource Category</a>

	   <cfif res_cat.recordcount GT 0>
	   &nbsp;|&nbsp;<a href="add.cfm">Add Resource</a>
	   </cfif>

	   </td></tr>

	   <tr><td>&nbsp;</td></tr>

        <tr><td>

        <cfif res_cat.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" style="font-weight: normal;">No Resource Categories exist.  Please add a Resource Category before adding a Resource.</td></tr>
        </table>

        <cfelse>

        <cfloop query="res_cat">

        <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=60>

         <a href="cat_edit.cfm?res_cat_id=#res_cat_id#">

         <cfif #res_cat.res_cat_image# is "">
	         <img src="#image_virtual#icon_book.png" width=40 height=40>
         <cfelse>
	         <img src="#media_virtual#/#res_cat_image#" width=40 height=40>
         </cfif>

         </a>

         </td>
             <td class="feed_sub_header"><a href="cat_edit.cfm?i=#encrypt(res_cat_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#res_cat_name#</a></td>
             <td align=right class="feed_sub_header">
		         <a href="cat_edit.cfm?i=#encrypt(res_cat_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/icon_edit.png" hspace=10 width=20 alt="Edit" title="Edit"></a>
		         <a href="add.cfm?i=#encrypt(res_cat_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/plus3.png" width=15 alt="Add Resource" title="Add Resource"></a>&nbsp;
	         </td>
         </tr>

         <tr><td colspan=3><hr></td></tr>

        </table>

        </cfoutput>

		 <cfquery name="res" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from res
		   where res_cat_id = #res_cat.res_cat_id#
		   order by res_order
		 </cfquery>

         <cfset counter = 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif res.recordcount is 0>
          <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;">No Resources have been added to this Category.</td></tr>
         <cfelse>

         <cfoutput query="res">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td width=50>&nbsp;</td>
              <td class="feed_sub_header" width=50><a href="edit.cfm?i=#encrypt(res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#res_order#</a></td>

              <td class="feed_sub_header" width=50><a href="edit.cfm?i=#encrypt(res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

				 <cfif res_image is "">
				  <img src="//logo.clearbit.com/#res_domain#" width=40 height=40 border=0 onerror="this.src='/images/no_logo.png'">
				 <cfelse>
				  <img src="#media_virtual#/#res_image#" width=40 height=0 border=0>
				 </cfif>

			  </td>

              <td class="feed_sub_header"><a href="edit.cfm?i=#encrypt(res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#res_name#</a></td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>

         <tr><td>&nbsp;</td></tr>

        </cfloop>

        </cfif>

	    </table>

       </td></tr>
     </table>






