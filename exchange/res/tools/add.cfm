<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res_cat
 where res_cat_hub_id = #session.hub#
 order by res_cat_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Resource</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header">Resource Name</td>
		       <td><input class="input_text" style="width: 600px;" type="text" name="res_name"></td></tr>
		   <tr><td valign=top class="feed_sub_header">Short Description</td>
		       <td><textarea class="input_textarea" style="width: 800px; height: 100px;" name="res_desc_short"></textarea></td></tr>
		   <tr><td valign=top class="feed_sub_header">Long Description</td>
		       <td><textarea class="input_textarea" style="width: 800px; height: 300px;" name="res_desc_long"></textarea></td></tr>
		   <tr><td class="feed_sub_header">Domain</td>
		       <td><input class="input_text" style="width: 300px;" type="text" name="res_domain">&nbsp;<span class="link_small_gray">i.e., www.websitename.org (do not include http://)</span></td></tr>
		   <tr><td class="feed_sub_header">Full URL</td>
		       <td><input class="input_text" style="width: 800px;" type="url" name="res_link"></td></tr>
		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" style="width: 800px;" type="text" name="res_keywords"></td></tr>

           <tr><td class="feed_sub_header" valign=top>Upload Document</td>
               <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="res_file"></td></tr>

	       <tr><td class="feed_sub_header">Category</td>
		   <td>
		   <select name="res_cat_id" class="input_select">
		   <cfoutput query="cats">
		    <option value=#res_cat_id# <cfif isdefined("i") and #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# is #res_cat_id#>selected</cfif>>#res_cat_name#
		   </cfoutput>
		   </select></td></tr>

		   <tr><td class="feed_sub_header">Display Order</td>
		       <td><input class="input_text" style="width: 100px;" required type="number" name="res_order" step=.1></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

