<cfinclude template="/exchange/security/check.cfm">

<cfquery name="res" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res
 where res_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#

</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">
        <cfinclude template="search_resources.cfm">
	  </div>

      <div class="main_box">

      <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td height=10></td></tr>
	   <tr>
		   <td width=120 valign=top>

			 <cfif res.res_image is "">
			  <img src="//logo.clearbit.com/#res.res_domain#" width=100 height=100 border=0 onerror="this.src='/images/no_logo.png'">
			 <cfelse>
			  <img src="#media_virtual#/#res.res_image#" width=100 height=100 border=0>
			 </cfif>

		  </td>

		  <td>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" style="padding-bottom: 0px; padding-top: 0px;"><b>#res.res_name#</b></td></tr>
		    <tr><td class="link_small_gray" style="padding-bottom: 10px;">Posted #dateformat(res.res_updated,'mmm dd, yyyy')#</td></tr>
			<tr><td colspan=2 class="feed_sub_header" style="padding-top: 5px; font-weight: normal;">#res.res_desc_short#</td></tr>

            <cfif trim(res.res_desc_long) is not "">
             <tr><td colspan=2><hr></td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;">#replace(res.res_desc_long,"#chr(10)#","<br>","all")#</td></tr>
            </cfif>

            <tr><td colspan=2><hr></td></tr>

            <cfif res.res_link is not "">
             <tr><td class="feed_sub_header" style="font-weight: normal;"><a href="#res.res_link#" target="_blank" rel="noopener">#res.res_link#</a></td></tr>
            </cfif>

            <cfif res.res_file is not "">
             <tr><td class="feed_sub_header" style="font-weight: normal;"><a href="#media_virtual#/#res.res_file#" target="_blank" rel="noopener"><b>Download Attachment</b></a></td></tr>
            </cfif>


		  </table>

		  </td></tr>

       </table>

       </cfoutput>

 	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>