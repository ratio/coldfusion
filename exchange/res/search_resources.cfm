<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res_cat
 where res_cat_hub_id = #session.hub#
 order by res_cat_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr><td valign=top>

    <table cellspacing=0 cellpadding=0 border=0 width=100%>
     <tr><td class="feed_header"><a href="index.cfm">Resources</a></td>
     <tr><td class="feed_sub_header" style="font-weight: normal;">Resources are tools, documents, articles and other form of information to help you.</td></tr>
   </table>

  </td><td valign=top>

    <table cellspacing=0 cellpadding=0 border=0 width=100%>
     <tr><td align=right>

     <img src="/images/ss_resources_1.png" height=80>
     <img src="/images/ss_resources_2.png" height=80>
     </td></tr>
   </table>

  </td></tr>

  <tr><td colspan=2><hr></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	<form action="results.cfm" method="post">

	<tr><td class="feed_sub_header" style="font-weight: normal;">

	<b>Search for Resources</b>

	&nbsp;&nbsp;
	<input type="text" name="keyword" class="input_text" placeholder="Type in a keyword..." style="width: 275px; height: 30px;">
	&nbsp;&nbsp;
	<b>related to</b>
	&nbsp;&nbsp;
	<select name="cat_type" class="input_select">
	<option value=0>All Categories
	<cfoutput query="cats">
	 <option value=#res_cat_id#>#res_cat_name#
	</cfoutput>
	&nbsp;&nbsp;<input type="submit" name="button" value="Search" class="button_blue">

	</td></tr>

	</form>
</table>
