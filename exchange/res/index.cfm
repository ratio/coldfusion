<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.resource_display")>
 <cfset session.resource_display = "Month">
</cfif>

<cfif session.resource_display is "Week">
 <cfset #from# = #dateformat(dateadd('d',-7,now()),'mm/dd/yyyy')#>
 <cfset #to# = #dateformat(dateadd('d',1,now()),'mm/dd/yyyy')#>
<cfelseif session.resource_display is "Month">
 <cfset #from# = #dateformat(dateadd('d',-30,now()),'mm/dd/yyyy')#>
 <cfset #to# = #dateformat(dateadd('d',1,now()),'mm/dd/yyyy')#>
<cfelseif session.resource_display is "Quarter">
 <cfset #from# = #dateformat(dateadd('d',-90,now()),'mm/dd/yyyy')#>
 <cfset #to# = #dateformat(dateadd('d',1,now()),'mm/dd/yyyy')#>
</cfif>

<cfquery name="resources" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res
 where res_hub_id = #session.hub# and
 res_updated between '#from#' and '#to#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">
        <cfinclude template="search_resources.cfm">
	  </div>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td valign=top width=20%>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		         <tr><td height=10></td></tr>
		         <tr><td class="feed_header" colspan=2>Resource Categories</td></tr>
		         <tr><td height=10></td></tr>
		         <tr><td colspan=2><hr></td></tr>
		         <tr><td height=10></td></tr>

                 <cfif cats.recordcount is 0>
                  <tr><td class="feed_sub_header" style="font-weight: normal;">No Categories were found.</td></tr>
                 <cfelse>

		         <cfloop query="cats">

					<cfquery name="count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select count(res_id) as total from res
					 where res_cat_id = #cats.res_cat_id# and
					       res_hub_id = #session.hub#
					</cfquery>

		         <cfoutput>

		          <tr>
	              <td width=70>

				     <a href="go.cfm?i=#encrypt(res_cat_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
				     <cfif cats.res_cat_image is "">
                       <img src="/images/icon_book.png" width=50 height=50>
				     <cfelse>
                       <img src="#media_virtual#/#cats.res_cat_image#" width=50 height=50 style="border-radius: 160px;">
				      </cfif>
				     </a>

		             </td>
		             <td class="feed_sub_header" style="margin-top: 0px; padding-top: 0px; padding-bottom: 0px;"><a href="go.cfm?i=#encrypt(res_cat_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#cats.res_cat_name# <cfif count.total GT 0>(#trim(count.total)#)</cfif></a></td>
		          </tr>

		          <tr><td height=20></td></tr>

		         </cfoutput>

		         </cfloop>

		         </cfif>

		        </table>

              </td><td width=40>&nbsp;</td><td valign=top width=75%>
                <form action="change.cfm">
		        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		         <tr><td class="feed_header" colspan=2>New Resources</td>
		             <td class="feed_sub_header" style="font-weight: normal;" align=right>
                     <b>Show Resources Posted</b>&nbsp;
                    			<select name="resource_display" class="input_select" onchange="form.submit();">
								<option value="week" <cfif session.resource_display is "week">selected</cfif>>This Week
								<option value="month" <cfif session.resource_display is "month">selected</cfif>>This Month
								<option value="quarter" <cfif session.resource_display is "quarter">selected</cfif>>This Quarter
                    </td>

		         </tr>
		         <tr><td colspan=3><hr></td></tr>

		        </table>
		        </form>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>

                    <cfif resources.recordcount is 0>
                     <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">No new Resources were found.</td></tr>
                    <cfelse>

                    <cfset count = 1>
					<cfoutput query="resources">

					   <tr>
						   <td width=90>

							 <a href="open.cfm?i=#encrypt(resources.res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

							 <cfif resources.res_image is "">
							  <img src="//logo.clearbit.com/#resources.res_domain#" width=70 height=70 border=0 onerror="this.src='/images/no_logo.png'">
							 <cfelse>
							  <img src="#media_virtual#/#resources.res_image#" width=70 height=70 border=0>
							 </cfif>

							 </a>

						  </td>

						  <td>

						  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                            <tr><td class="feed_sub_header" style="padding-bottom: 0px; padding-top: 0px;"><a href="open.cfm?i=#encrypt(resources.res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#res_name#</b></a></td>
                                <td class="feed_sub_header" align=right>#dateformat(res_updated,'mmm dd')#</td></tr>
                            <tr><td colspan=2 class="feed_sub_header" style="padding-top: 5px; font-weight: normal;">#res_desc_short#</td></tr>
                          </table>

                          </td></tr>

					   <cfif count LT resources.recordcount>
					   <tr><td height=10></td></tr>
					   <tr><td colspan=2><hr></td></tr>
					   </cfif>

					   <cfset count = count + 1>

					   </cfoutput>

					   </cfif>

                       <tr><td height=20></td></tr>

					   </table>


              </td></tr>

         </table>


 	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>