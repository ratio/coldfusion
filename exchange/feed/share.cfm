<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/profile_company.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

      <div class="main_box">

	      <form action="share_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">SHARE FEED</td>
               <td align=right><a href="/exchange/my_feeds.cfm"><img src="/images/delete.png" border=0 alt="Close" title="Close" width=20></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <form action="/exchange/feed/share_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		     <tr><td height=15></td></tr>

             <tr><td class="feed_sub_header" width=150>Email Address</td>
                 <td><input type="email" class="input_text" name="email" style="width: 350px;" required></td></tr>

             <tr>
                 <td class="feed_sub_header">Subject</td>
                 <td><input type="text" class="input_text" name="subject" style="width: 700px;" required></td></tr>

             <tr>
                 <td class="feed_sub_header" valign=top>Message</td>
                 <td><textarea name="message" class="input_textarea" style="width: 700px;" rows=8></textarea></td>
             </tr>

			 <tr><td height=10></td></tr>
			 <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Share" vspace=10></td></tr>

             <tr><td height=10></td></tr>

		     <cfoutput>
		     <input type="hidden" name="feed_id" value=#feed_id#>
		     <cfif isdefined("l")>
		      <input type="hidden" name="l" value=1>
		     </cfif>
		     </cfoutput>

            </form>

          </table>

		  <cfquery name="feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from feed
		   where feed_id = #feed_id#
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=15></td></tr>
           <tr><td class="feed_header">FEED DETAILS</td></tr>
           <tr><td><hr></td></tr>
           <tr><td height=10></td></tr>

		   <cfoutput>

           <cfif feed.feed_organization is not "">
		   <tr><td class="feed_sub_header">#feed.feed_organization#</td></tr>
		   <tr><td height=20></td></tr>
		   </cfif>

		   <tr><td class="feed_option"><b>#feed.feed_title#</b></td></tr>

		   <tr><td class="feed_option">#feed.feed_desc#</td></tr>

           <cfif #feed.feed_url# is not "">
           <tr><td height=20></td></tr>
		   <tr><td class="feed_option"><b>URL Reference</b></td></tr>
		   <tr><td class="feed_option"><a href="#feed.feed_url#" target="_blank" rel="noopener" rel="noreferrer"><u>#feed.feed_url#</u></a></td></tr>
		   </cfif>

		   <tr><td height=5></td></tr>
		   <tr><td class="feed_option">#feed.feed_desc_long#</td></tr>

		   </cfoutput>

		  </table>


          </form>

          </td></tr>

        </table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

