<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">ADD SOLICITATION TRACKER FEED</td><td align=right class="feed_option"><a href="/exchange/feed/opportunities.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
		  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td class="feed_sub_header" width=200>Solicitation Number</td>
              <td class="feed_option"><input type="text" class="input_text" name="usr_feed_solicitation_number" style="width: 250px;" maxlength="100" required></td>
          </tr>

           <tr><td height=10></td></tr>
           <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Solicitation"></td></tr>

           </table>

 		  </form>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

