<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="interest" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert feed_interest
	 (feed_interest_feed_id, feed_interest_usr_id, feed_interest_decision, feed_interest_decision_date)
	 values
	 (#feed_id#,#session.usr_id#, 1, #now()#)
	</cfquery>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete feed_usr
	 where feed_usr_feed_id = #feed_id# and
		   feed_usr_usr_id = #session.usr_id#
	</cfquery>

</cftransaction>

<cflocation URL="/exchange/index.cfm?u=2" addtoken="no">



