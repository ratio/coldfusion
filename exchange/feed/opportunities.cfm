<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">OPPORTUNITY FEEDS</td><td align=right class="feed_option"><a href="/exchange/my_feeds.cfm"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header"><a href="/exchange/feed/build.cfm">Feed Subscriptions</a>&nbsp;|&nbsp;<a href="/exchange/feed/opportunities.cfm"><u>OPPORTUNITY FEEDS</u></a></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <cfif isdefined("u")>
		    <cfif u is 1>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Opportunity Tracker Feed has been successfully created.</font></td></tr>
            <cfelseif u is 2>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Opportunity Tracker Feed has been successfully updated.</font></td></tr>
            <cfelseif u is 3>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Award Notification Tracker Feed has been successfully created.</font></td></tr>
            <cfelseif u is 4>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Award Notification Tracker Feed has been successfully updated.</font></td></tr>
            <cfelseif u is 5>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Solicitation Tracker Feed has been successfully created.</font></td></tr>
            <cfelseif u is 6>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Solicitation Tracker Feed has been successfully updated.</font></td></tr>
		    <cfelseif u is 8>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Feed has been successfully deleted.</font></td></tr>
		    </cfif>
		   </cfif>

            <tr><td height=10></td></tr>
            <tr><td colspan=5 class="feed_sub_header">FEDERAL PROCUREMENTS</td></tr>
            <tr><td colspan=5 class="feed_sub_header" style="font-weight: normal;">The Exchange posts Federal Procurements daily from <a href="http://www.fbo.gov" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.fbo.gov</u></b></a>.  FBO.Gov is a Federal Government procurement portal that posts and maintains solicitations that are issued to industry greater than $25,000.  To create or modify custom feeds for Federal Procurements please choose one of the options below.</td></tr>
            <tr><td height=10></td></tr>
            <tr><td colspan=5><hr></td></tr>

			<cfquery name="fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 1 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_name
			</cfquery>

			<tr><td valign=top width=33%>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td width=30><a href="/exchange/feed/fbo_add.cfm"><img src="/images/plus3.png" width=15 alt="Create Opportunity Feed" title="Create Opportunity Feed" border=0></a></td><td class="feed_sub_header">OPPORTUNITY TRACKER</td></tr>

            <cfif fbo.recordcount is 0>
              <tr><td></td><td class="feed_sub_header" style="font-weight: normal;" colspan=3>You have no opportunity feeds.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <cfoutput query="fbo">
                <tr><td></td><td class="feed_sub_header"><a href="/exchange/feed/fbo_edit.cfm?usr_feed_id=#usr_feed_id#"><u>#usr_feed_name#</u></a></td></tr>
                <tr><td></td><td class="feed_option">#usr_feed_desc#</td></tr>
                <tr><td><td><hr></td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td><td width=50>&nbsp;</td><td valign=top width=33%>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 2 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_name
			</cfquery>

            <tr><td width=30><a href="/exchange/feed/award_add.cfm"><img src="/images/plus3.png" width=15 alt="Add Award Notification Feed" title="Add Award Notification Feed" border=0></a></td><td class="feed_sub_header">AWARD NOTIFICATION TRACKER</td></tr>

            <cfif awards.recordcount is 0>
              <tr><td></td><td class="feed_sub_header" style="font-weight: normal;" colspan=3>You have no Award Notification feeds.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <cfoutput query="awards">
                <tr><td></td><td class="feed_sub_header"><a href="/exchange/feed/award_edit.cfm?usr_feed_id=#usr_feed_id#"><u>#usr_feed_name#</u></a></td></tr>
                <tr><td></td><td class="feed_option">#usr_feed_desc#</td></tr>
                <tr><td></td><td><hr></td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td><td width=50>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="sol" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 3 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_solicitation_number
			</cfquery>

            <tr><td width=30><a href="/exchange/feed/sol_add.cfm"><img src="/images/plus3.png" width=15 alt="Add Solicitation Number" title="Add Solicitation Number" border=0></a></td><td class="feed_sub_header">SOLICITATION TRACKER</td></tr>

            <cfif sol.recordcount is 0>
              <tr><td></td><td class="feed_sub_header" style="font-weight: normal;" colspan=3>You have no Solicitation Tracker feeds.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <cfoutput query="sol">
                <tr><td></td><td class="feed_sub_header"><a href="/exchange/feed/sol_edit.cfm?usr_feed_id=#usr_feed_id#"><u>#usr_feed_solicitation_number#</u></a></td></tr>
                <tr><td></td><td><hr></td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td></tr>

                   </table>

                   </td></tr>

</table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

