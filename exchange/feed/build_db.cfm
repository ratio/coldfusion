<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add FBO.Gov Feed">

  <cfquery name="add_fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   insert into usr_feed
   (usr_feed_type, usr_feed_usr_id, usr_feed_fbo_sb, usr_feed_sub_type, usr_feed_name, usr_feed_desc, usr_feed_type_id, usr_feed_created, usr_feed_updated, usr_feed_naics, usr_feed_fbo_psc, usr_feed_keyword, usr_feed_dept)
   values
   (1,#session.usr_id#,'#usr_feed_fbo_sb#','#usr_feed_sub_type#','#usr_feed_name#','#usr_feed_desc#',1,#now()#,#now()#,'#usr_feed_naics#','#usr_feed_fbo_psc#','#usr_feed_keyword#','#usr_feed_dept#')
  </cfquery>

  <cflocation URL="opportunities.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

  <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    delete usr_feed
    where usr_feed_usr_id = #session.usr_id# and
          usr_feed_id = #usr_feed_id#
  </cfquery>

  <cflocation URL="opportunities.cfm?u=8" addtoken="no">

<cfelseif #button# is "Save FBO.Gov Feed">

  <cfquery name="update_fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update usr_feed
   set usr_feed_fbo_sb = '#usr_feed_fbo_sb#',
       usr_feed_sub_type = '#usr_feed_sub_type#',
       usr_feed_name = '#usr_feed_name#',
       usr_feed_desc = '#usr_feed_desc#',
       usr_feed_type = 1,
       usr_feed_updated = #now()#,
       usr_feed_naics = '#usr_feed_naics#',
       usr_feed_fbo_psc = '#usr_feed_fbo_psc#',
       usr_feed_keyword = '#usr_feed_keyword#',
       usr_feed_dept = '#usr_feed_dept#'
   where usr_feed_usr_id = #session.usr_id# and
         usr_feed_id = #usr_feed_id#
  </cfquery>

  <cflocation URL="opportunities.cfm?u=2" addtoken="no">

<cfelseif #button# is "Add Award Feed">

  <cfquery name="add_fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   insert into usr_feed
   (usr_feed_type, usr_feed_duns, usr_feed_usr_id, usr_feed_fbo_sb, usr_feed_name, usr_feed_desc, usr_feed_type_id, usr_feed_created, usr_feed_updated, usr_feed_naics, usr_feed_fbo_psc, usr_feed_keyword, usr_feed_dept)
   values
   (2, '#usr_feed_duns#',#session.usr_id#,'#usr_feed_fbo_sb#','#usr_feed_name#','#usr_feed_desc#',2,#now()#,#now()#,'#usr_feed_naics#','#usr_feed_fbo_psc#','#usr_feed_keyword#','#usr_feed_dept#')
  </cfquery>

  <cflocation URL="opportunities.cfm?u=3" addtoken="no">

<cfelseif #button# is "Save Award Feed">

  <cfquery name="update_fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update usr_feed
   set usr_feed_fbo_sb = '#usr_feed_fbo_sb#',
       usr_feed_name = '#usr_feed_name#',
       usr_feed_desc = '#usr_feed_desc#',
       usr_feed_updated = #now()#,
       usr_feed_type = 2,
       usr_feed_naics = '#usr_feed_naics#',
       usr_feed_fbo_psc = '#usr_feed_fbo_psc#',
       usr_feed_keyword = '#usr_feed_keyword#',
       usr_feed_dept = '#usr_feed_dept#',
       usr_feed_duns = '#usr_feed_duns#'
   where usr_feed_usr_id = #session.usr_id# and
         usr_feed_id = #usr_feed_id#
  </cfquery>

  <cflocation URL="opportunities.cfm?u=4" addtoken="no">

<cfelseif #button# is "Add Solicitation">

  <cfquery name="add_fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   insert into usr_feed
   (usr_feed_type, usr_feed_solicitation_number, usr_feed_usr_id, usr_feed_type_id, usr_feed_created, usr_feed_updated)
   values
   (3, '#usr_feed_solicitation_number#',#session.usr_id#,3,#now()#,#now()#)
  </cfquery>

  <cflocation URL="opportunities.cfm?u=5" addtoken="no">

<cfelseif #button# is "Save Solicitation">

  <cfquery name="add_fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update usr_feed
   set usr_feed_solicitation_number = '#usr_feed_solicitation_number#',
       usr_feed_type = 3
   where usr_feed_id = #usr_feed_id# and
         usr_feed_usr_id = #session.usr_id#
  </cfquery>

  <cflocation URL="opportunities.cfm?u=6" addtoken="no">

<cfelseif #button# is "Update Alignments">

<cftransaction>

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (2,3,4)) and
	       (align_usr_id = #session.usr_id#)
	 <cfif isdefined("session.hub")>
	  and (align_hub_id = #session.hub#)
	 <cfelse>
	  and (align_hub_id is null)
	 </cfif>

	</cfquery>

	<cfif isdefined("market_id")>

		<cfloop index="m_element" list=#market_id#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 2, #m_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sector_id")>

		<cfloop index="s_element" list=#sector_id#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 3, #s_element#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topic_id")>

		<cfloop index="t_element" list=#topic_id#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 4, #t_element#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

</cftransaction>

  <cflocation URL="build.cfm?u=7" addtoken="no">

</cfif>
