<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfif usr.usr_role_id is 1>
	      <cfinclude template="/exchange/portfolio/recent.cfm">
      <cfelse>
	      <cfinclude template="/exchange/profile_company.cfm">
      </cfif>

      </td><td valign=top>

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/">WELCOME</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/my_feeds.cfm">THE PULSE</a></span>
          </div>

	  <div class="main_box_2">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">CATEGORY SUBSCRIPTIONS</td><td align=right class="feed_option" colspan=2><a href="/exchange/my_feeds.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=3><hr></td></tr>

		   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Below are the Categories that you are currently subscribed to.</td>
		       <td align=right class="feed_sub_header"><a href="categories.cfm"><img src="/images/icon_config.png" width=20 border=0 hspace=10 title="Configure Subscriptions" alt="Configure Subscriptions"><a href="categories.cfm">Change Subscriptions</a></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					  select lens_name, lens_desc from lens, align
					  where (lens.lens_id = align.align_type_value) and
							(align.align_usr_id = #session.usr_id#) and
							(align.align_type_id = 1) and
							(lens_hub_id = #session.hub#)
			  			     order by lens.lens_order
					</cfquery>

		   <cfif isdefined("u")>
		    <cfif u is 5>
		     <tr><td class="feed_sub_header" colspan=2><font color="green">Category subscriptions have been successfully updated.</font></td></tr>
		    </cfif>
		   </cfif>

		   <tr><td colspan=3>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr>
			   <td class="feed_sub_header" width=250>Category</td>
			   <td class="feed_sub_header" width=30></td>
			   <td class="feed_sub_header">Description</td>
			   <td class="feed_sub_header" align=right></td></tr>
		   <tr><td height=5></td></tr>
		   <cfset counter = 0>

		   <cfoutput query="lens">

			 <tr <cfif counter is 0>bgcolor="ffffff"<cfelse>bgcolor="e0e0e0"</cfif>>

				 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=200><b>#lens_name#</b></td>
				 <td></td>
				 <td class="feed_sub_header" style="font-weight: normal;" valign=top colspan=2>#lens_desc#</td>

			 </tr>
		   <cfif counter is 0>
			<cfset counter = 1>
		   <cfelse>
			<cfset counter = 0>
		   </cfif>

		   </cfoutput>

		   </table>

		   </td></tr>

       </table>

	  </div>

	  </td>




	  <td valign=top>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=45></td></tr>
	  </table>

      <cfif usr.usr_role_id is 1>

	  <cfinclude template="/exchange/network.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      <cfelse>

      <cfinclude template="/exchange/network.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </cfif>

		   </td>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

