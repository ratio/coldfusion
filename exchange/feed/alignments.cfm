<cfinclude template="/exchange/security/check.cfm">

<html>
<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">UPDATE FEED ALIGNMENTS</td><td align=right class="feed_option"><a href="/exchange/feed/build.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_option">Alignments allow you to filter opportunities in the Feed to your market, sector, and capabilities & services preferences.</td></tr>
           <tr><td height=10></td></tr>
		  </table>

	 <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from market
	   <cfif isdefined("session.hub")>
	    where market_hub_id = #session.hub#
	   <cfelse>
	    where market_hub_id is null
	   </cfif>
	   order by market_name
	  </cfquery>

	  <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from sector
	   <cfif isdefined("session.hub")>
	    where sector_hub_id = #session.hub#
	   <cfelse>
	    where sector_hub_id is null
	   </cfif>
	   order by sector_name
	  </cfquery>

	  <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from topic
	   <cfif isdefined("session.hub")>
	    where topic_hub_id = #session.hub#
	   <cfelse>
	    where topic_hub_id is null
	   </cfif>
	   order by topic_name
	  </cfquery>

	  <cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          align_hub_id = #session.hub# and
	         </cfif>
			 (align_type_id = 2)
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          align_hub_id = #session.hub# and
	         </cfif>
			 (align_type_id = 3)
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          align_hub_id = #session.hub# and
	         </cfif>
			 (align_type_id = 4)
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td colspan=2>

			<table cellspacing=0 cellpadding=0 border=0>

			<tr><td class="feed_sub_header">MARKETS</td>
				<td class="feed_sub_header">SECTORS</td>
				<td class="feed_sub_header">CAPABILITIES & SERVICES</td>
				</tr>

			<tr>

				 <td width=180 valign=top>
					  <select class="input_select" style="width: 170px; height: 150px;" name="market_id" multiple required>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=220 valign=top>
					  <select class="input_select" style="width: 200px; height: 250px;" name="sector_id" multiple required>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td valign=top>
					  <select class="input_select" style="width: 275px; height: 350px;" name="topic_id" multiple required>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
                <tr><td class="feed_option" colspan=2>To select multiple items please use the SHIFT or CTRL key.</td></tr>

				<tr><td height=15></td></tr>
				<tr><td><input class="button_blue_large" type="submit" name="button" value="Update Alignments"></td></tr>

 			  </table>

 		  </form>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

