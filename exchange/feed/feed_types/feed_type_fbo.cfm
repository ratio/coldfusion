  <cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=top width=110>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <cfif feed.feed_image_url is "">
			  <tr><td><a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/fbo_logo.png" valign=top align=top height=80 border=0 vspace=10></a></td></tr>
			 <cfelse>
			  <tr><td><a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#"><img src="/images/fbo_logo.png" align=top width=80 border=0 vspace=10></a></td></tr>
			 </cfif>
         </table>

	     </td><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr>
               <td class="feed_title" valign=top><a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(feed.feed_title)#</a></td>
               <td align=right width=100 valign=top>

				   <a href="/exchange/feed/share.cfm?feed_id=#feed.feed_id#"><img src="/images/icon_share.png" height=20 valign=middle alt="Share with Others" title="Share with Others" hspace=2></a>
				   <img src="/images/icon_green_add.png" height=20 valign=middle alt="Add to Portfolio" title="Add to Portfolio" border=0 hspace=2 style="cursor: pointer;" onclick="window.open('/exchange/include/save_opp.cfm?id=#feed.feed_fbo_id#&t=1','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"></a>
				   <a href="/exchange/feed/remove.cfm?feed_id=#feed.feed_id#" onclick="return confirm('Delete Feed?\r\nAre you sure you want to delete this opportunity from your Feed?');"><img src="/images/icon_trash.png" height=20 valign=middle alt="Delete Feed" title="Delete Feed"></a>
			   </td></tr>
           <tr>
               <td class="feed_option"><b>#feed.feed_value_1#</b><br>Solicitation ## - <cfif #feed.feed_value_2# is "">Not specified<cfelse>#ucase(feed.feed_value_2)#</cfif><br>
	                                   #feed.feed_value_3#</td></tr>
         </table>

	     </td></tr>

</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td height=10></td></tr>
	 <tr><td class="feed_sub_header" colspan=2>#ucase(feed.feed_organization)#</td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=2 class="feed_option">

	 <cfif #len(feed.feed_desc)# GT 200>
	  #left((replace(feed.feed_desc,"#chr(10)#","<br>","all")),'200')#... <a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer">read more >></a>
	 <cfelse>
	  #replace(feed.feed_desc,"#chr(10)#","<br>","all")#
	 </cfif>

	 </td></tr>

	 <cfif #feed.feed_url# is not "">

	  <tr><td height=10></td></tr>
	  <tr><td class="link_small_gray"><!---<a href="#feed.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#feed.feed_url#</a>---></td>
	      <td align=right class="link_small_gray" align=right>

		  			   <cfif #mdiff# LT 1>
		  				 < minute ago
		  			   <cfelseif #mdiff# GTE 1 and #mdiff# LT 2>
		  				#mdiff# minute ago
		  			   <cfelseif #mdiff# GTE 2 and #mdiff# LT 60>
		  				#mdiff# minutes ago
		  			   <cfelseif #mdiff# GT 60>
		  				<cfif #mdiff# LT 1440>
		  				 #round(evaluate(mdiff/60))# hours ago
		  				<cfelse>
		  				 <cfif #round(evaluate(mdiff/1440))# GT 1>
		  				   #round(evaluate(mdiff/1440))# days ago
		  				 <cfelse>
		  				   #round(evaluate(mdiff/1440))# days ago
		  				 </cfif>
		  				</cfif>
		  			   </cfif>
		  	 </b>

	 </td></tr>

     <tr><td height=5></td></tr>
	 </cfif>

  </table>

 </cfoutput>
