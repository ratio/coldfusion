<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

			<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from usr_feed
			 where usr_feed_usr_id = #session.usr_id# and
			       usr_feed_id = #usr_feed_id#
			</cfquery>

			<cfquery name="dept" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select distinct(fbo_agency) from fbo
			 order by fbo_agency
			</cfquery>

			<cfquery name="type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select distinct(fbo_type) from fbo
			 order by fbo_type
			</cfquery>

			<cfquery name="setaside" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select distinct(fbo_setaside) from fbo
			 order by fbo_setaside
			</cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">EDIT AWARD NOTIFICATION FEED</td><td align=right class="feed_option"><a href="/exchange/feed/opportunities.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
		  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

          <tr>
              <td class="feed_sub_header">Feed Name</td>
              <td class="feed_option"><input class="input_text" type="text" name="usr_feed_name" style="width:350px;" maxlength="100" value="#info.usr_feed_name#" required></td>
          </tr>

          <tr>
              <td class="feed_sub_header" valign=top>Description</td>
              <td class="feed_option"><textarea name="usr_feed_desc" class="input_textarea" style="width: 350px;" rows=3>#info.usr_feed_desc#</textarea></td>
          </tr>

          </cfoutput>

          <tr>
              <td class="feed_sub_header">Department</td>
              <td class="feed_option">
              <select name="usr_feed_dept" class="input_select" style="width:250px;">
               <option value=0>All
               <cfoutput query="dept">
                <option value="#fbo_agency#" <cfif #info.usr_feed_dept# is #fbo_agency#>selected</cfif>>#fbo_agency#
               </cfoutput>
              </select>
              </td></tr>

          <cfoutput>

          <tr>
              <td class="feed_sub_header">Company DUNS Number *</td>
              <td class="feed_option"><input class="input_text" type="text" name="usr_feed_duns" style="width:250px;" maxlength="299" value="#info.usr_feed_duns#">&nbsp;&nbsp;<a href="/exchange/marketplace/" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_search.png" height=18 alt="Company Lookup" title="Company Lookup"></a></td>
          </tr>

          <tr>
              <td class="feed_sub_header" width=225>NAICS Code(s) *</td>
              <td class="feed_option"><input class="input_text" type="text" name="usr_feed_naics" style="width:250px;" maxlength="299" value="#info.usr_feed_naics#">&nbsp;&nbsp;<a href="https://www.census.gov/eos/www/naics/" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup"></a></td>
          </tr>

           <tr>
               <td class="feed_sub_header">PS Code(s) *</td>
               <td class="feed_option"><input type="text" class="input_text" name="usr_feed_fbo_psc" style="width: 250px;" maxlength="299" value="#info.usr_feed_fbo_psc#">&nbsp;&nbsp;<a href="https://www.fbo.gov/index?s=getstart&mode=list&tab=list&tabmode=list&static=faqs##q4" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_search.png" height=18 alt="PSC Code Lookup" title="PSC Code Lookup"></a></td>
           </tr>

          <tr>
              <td class="feed_sub_header">Keyword</td>
              <td class="feed_option"><input type="text" name="usr_feed_keyword" class="input_text" style="width:250px;" maxlength="100" value="#info.usr_feed_keyword#"></td>
          </tr>

          </cfoutput>

           <tr>
               <td class="feed_sub_header">Set Aside</td>
               <td class="feed_option">
				<select name="usr_feed_fbo_sb" class="input_select" style="width:250px;">
				 <option value=0>No Preference
				 <cfoutput query="setaside">
				 <option value="#fbo_setaside#" <cfif #info.usr_feed_fbo_sb# is #fbo_setaside#>selected</cfif>>#fbo_setaside#
				 </cfoutput>
                </select></td>

           </tr>

           <tr><td height=10></td></tr>
           <tr><td></td><td>
           <input class="button_blue_large" type="submit" name="button" value="Save Award Feed">
           &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" onclick="return confirm('Delete Record.\r\nAre you sure you want to delete this item?');">

           </td></tr>
           <tr><td height=10></td></tr>
           <tr><td></td><td class="link_small_gray">* - Entering nothing will return all.  To select multiple values, seperate codes with a commma and no spaces.</td></tr>
           <tr><td height=10></td></tr>

           </table>

           <cfoutput>
            <input type="hidden" name="usr_feed_id" value=#usr_feed_id#>
           </cfoutput>

 		  </form>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

