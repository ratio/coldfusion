<cfinclude template="/exchange/security/check.cfm">

<cfset session.portfolio_id = #portfolio_id#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item
 where portfolio_item_id = #portfolio_item_id#
</cfquery>

<cfquery name="fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from fbo
   left join naics on naics_code = fbo_naics_code
   left join class_code on class_code_code = fbo_class_code
   where fbo_id = #port_info.portfolio_item_opp_id#
</cfquery>

<cfquery name="port_schedule" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item_schedule
 where portfolio_item_schedule_item_id = #portfolio_item_id# and
       portfolio_item_schedule_usr_id = #session.usr_id#
 order by portfolio_item_schedule_end_date ASC
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfif #session.portfolio_location# is "Home">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

       <cfelseif #session.portfolio_location# is "Navigator">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/nav_company.cfm">
		   <cfinclude template="/exchange/portfolio/nav_award.cfm">
           <cfinclude template="/exchange/portfolio/nav_opp.cfm">

       </cfif>

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
             <tr><td class="feed_header">#port_info.portfolio_item_name#</td>
                 <td class="feed_option" align=right>

				 <cfif #port_info.portfolio_item_usr_id# is #session.usr_id#>

                 <a href="/exchange/portfolio/portfolio_opp_edit.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/icon_config.png" width=20 border=0 alt="Edit" title="Edit"></a>

                 &nbsp;&nbsp;

                 </cfif>

                 <a href="/exchange/portfolio/portfolio_opp.cfm?portfolio_id=#portfolio_id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
             <tr><td colspan=2><hr></td></tr>
          </cfoutput>

           <cfif isdefined("u")>
            <tr><td height=5></td></tr>
            <cfif u is 1>
             <tr><td class="feed_sub_header"><font color="green"><b>Update successful.</b></font></td></tr>
            </cfif>
           </cfif>
          </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header">OPPORTUNITY SUMMARY</td><td class="feed_sub_header" align=right></td>
			    <td class="feed_option" align=right></td></tr>
			<tr><td height=10></td></tr>
		</table>



<!--- Display Summary --->

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td height=10></td></tr>
                <tr><td valign=top width=120>

                    </td><td valign=top>

			          <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<tr><td class="feed_option"><b>#fbo.fbo_agency#</b></td></tr>
						<tr><td class="feed_option"><b>Contracting Office</b> - <cfif #fbo.fbo_contracting_office# is "">Not specified<cfelse>#fbo.fbo_contracting_office#</cfif></td></tr>
						<tr><td height=15></td></tr>
                      </table>

                    </td>
                </tr>

                <tr><td colspan=3><hr></td></tr>

              </table>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr><td height=10></td></tr>
				 <tr><td class="feed_sub_header" colspan=2>#ucase(fbo.fbo_opp_name)#</td><td align=right></td></tr>
		         <tr><td valign=top>

		          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			         <tr>
			             <td class="feed_option" valign=top width=150><b>Solicitation ##</td>
			             <td class="feed_option" valign=top width=300>#fbo.fbo_solicitation_number#</td>
			             <td class="feed_option" valign=top width=125><b>Date Posted</td>
			             <td class="feed_option" valign=top width=400>#dateformat(fbo.fbo_pub_date_updated,'mm/dd/yyyy')#</td>
			         </tr>

			         <tr>
			             <td class="feed_option" valign=top><b>Notice Type</td>
			             <td class="feed_option" valign=top>#fbo.fbo_type#</td>
			             <td class="feed_option" valign=top><b>Response Date</td>
			             <td class="feed_option" valign=top>
			             <cfif isdate(fbo.fbo_response_date_original)>
			              #dateformat(fbo.fbo_response_date_original,'mm/dd/yyyy')# at #timeformat(fbo.fbo_response_date_original)#
			             <cfelse>
			             Unknown or N/A
			             </cfif></td>
			         </tr>

			         <tr>
			             <td class="feed_option" valign=top><b>Small Business</td>
			             <td class="feed_option" valign=top>#fbo.fbo_setaside_original#</td>

			             <td class="feed_option" valign=top><b>Primary POC(s)</td>
			             <td class="feed_option" valign=top>
			             <cfif #fbo.fbo_poc# is not "">#fbo.fbo_poc#<br></cfif>
			             <cfif #fbo.fbo_poc_secondary# is ""><cfelse>#fbo.fbo_poc_secondary#</cfif></td>

			         </tr>

			         <tr>
			             <td class="feed_option" valign=top><b>NAICS</td>
			             <td class="feed_option" valign=top>#fbo.fbo_naics_code# - #fbo.naics_code_description#</td>

			             <td class="feed_option" valign=top><b>Secondary POC</td>
			             <td class="feed_option" valign=top><cfif #fbo.fbo_poc_secondary# is "">Not specified<cfelse>#fbo.fbo_poc_secondary#</cfif></td>


			         </tr>

			         <tr>

			             <td class="feed_option" valign=top><b>Product or Service</td>
			             <td class="feed_option" valign=top>#fbo.fbo_class_code# - #fbo.class_code_name#</td>

			         </tr>

			         <tr>

			             <td class="feed_option" valign=top><b>Place of Performance</td>
			             <td class="feed_option" valign=top>#fbo.fbo_pop_city# #fbo.fbo_pop_state#,  #fbo.fbo_pop_zip#  #fbo.fbo_pop_country#</td>

			         </tr>


			        <tr><td class="feed_option" valign=top><b>Opportunity URL</b></td>
			            <td colspan=4 class="feed_option"><a href="#fbo.fbo_url#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>Click Here</u></a></td></tr>

                     </table>

                <tr><td colspan=3><hr></td></tr>


                 </cfoutput>










































			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfoutput>
			 <tr>
			     <td class="feed_sub_header">TIMELINE / SCHEDULE</td>
			     <td class="feed_option" align=right colspan=2>

			     <cfif #port_info.portfolio_item_usr_id# is #session.usr_id#>
			      <a href="/exchange/portfolio/activity_opp_add.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/icon_gannt.png" width=20 border=0 alt="Add Activity" title="Add Activity"></a>
                  &nbsp;
			      <a href="/exchange/portfolio/activity_opp_add.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><b>Add Activity</b></a>
			     </cfif></td>

			 </tr>
			 <tr><td height=4></td></tr>
			 </cfoutput>

			 <cfif port_schedule.recordcount is 0>
			   <tr><td class="feed_option">No activities have been added.</td></tr>
			 <cfelse>

			 <tr><td colspan=2>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['timeline']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'Name' });
        dataTable.addColumn({ type: 'string', id: 'Activity' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addColumn({ type: 'string', role: 'tooltip', id: 'link', 'p': {'html': true} });
        dataTable.addRows([

        <cfoutput query="port_schedule">
          [ 'Activities', '#portfolio_item_schedule_name#', new Date(#year(portfolio_item_schedule_start_date)#, #evaluate(month(portfolio_item_schedule_start_date)-1)#, #day(portfolio_item_schedule_start_date)#), new Date(#year(portfolio_item_schedule_end_date)#, #evaluate(month(portfolio_item_schedule_end_date)-1)#, #day(portfolio_item_schedule_end_date)#),'activity_opp_edit.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&activity_id=#portfolio_item_schedule_id#'],
        </cfoutput>

          ]);

    var options = {
      timeline: { showRowLabels: false, colorByRowLabel: true }
    };

 google.visualization.events.addListener(chart, 'select', function () {
    var selection = chart.getSelection();
    if (selection.length > 0) {
      window.open(dataTable.getValue(selection[0].row, 4), '_blank');
      console.log(dataTable.getValue(selection[0].row, 4));
    }
  });

  function drawChart1() {
    chart.draw(dataTable, options);
  }
  drawChart1();

      }
    </script>

    <div id="timeline" style="width: 100%; height: 100%;"></div>

   </td></tr>

   </cfif>

 <tr><td height=10></td></tr>
 <tr><td colspan=2><hr></td></tr>

 </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td class="feed_sub_header">NOTES & COMMMENTS</td>
			     <td align=right class="feed_option">

				 <cfif #port_info.portfolio_item_usr_id# is #session.usr_id#>

                 <cfoutput>
                 <a href="/exchange/portfolio/portfolio_opp_edit.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Notes & Comments" title="Edit Notes & Comments"></a>
                 &nbsp;&nbsp;<a href="/exchange/portfolio/portfolio_opp_edit.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><b>Update</b></a>
                 </cfoutput>


                 </cfif>

			     </td></tr>

             <cfif port_info.portfolio_item_comments is "">

             <tr><td class="feed_option">No notes or comments have been added.</td></tr>

             <cfelse>

             <tr><td class="feed_option">

                 <cfoutput>
                 #(replace(port_info.portfolio_item_comments,"#chr(10)#","<br>","all"))#
                 </cfoutput>

                 </td></tr>

             </cfif>

            </table>





    </td></tr>

</table>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>