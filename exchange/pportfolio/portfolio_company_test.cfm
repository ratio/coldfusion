<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfset page = "Open">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfquery name="port_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select portfolio_item_company_id from portfolio_item
  where portfolio_item_portfolio_id = #session.portfolio_id# and
		portfolio_item_type_id = 1
</cfquery>

<cfif port_list.recordcount is 0>
	<cfset plist = 0>
<cfelse>
	<cfset plist = valuelist(port_list.portfolio_item_company_id)>
</cfif>

<cfset end_date = #dateformat(now(),'mm/dd/yyyy')#>
<cfset start_date = dateformat(dateadd("yyyy",-3,end_date),'mm/dd/yyyy')>

 <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select company_id, company_logo, company_website, company_duns, company_name, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(awarding_agency_code)) as dept,  count(id) as awards, count(distinct(award_id_piid)) as contracts from company
  left join award_data on recipient_duns = company_duns
  where ((action_date between '#start_date#' and '#end_date#') or action_date is null) and
  company_id in (#plist#)
  group by company_id, company_duns, company_name, company_logo, company_website
  order by company_name
 </cfquery>

 <cfoutput>#plist#</cfoutput>
 <cfabort>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="recent.cfm">

       </td><td valign=top>

       <cfinclude template="port_header.cfm">

       <div class="main_box">

          <cfinclude template="portfolio_header.cfm">

		  <cfif companies.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif session.portfolio_access_level is 1>
	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.</td></tr>
           <cfelse>
	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.<br><br>To add Companies, go to <a href="/exchange/marketplace/partners/network_in.cfm"><b>In Network</b></a> or <a href="/exchange/marketplace/partners/network_out.cfm"><b>Out of Network</b></a> Partners and choose the <u>Add to Portfolio</u> option when viewing the Company profile.</td></tr>
	       </cfif>

          </table>

		  <cfelse>

		  <!--- Insert Graphs --->

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>
		  <tr><td class="feed_header" colspan=3>PORTFOLIO DASHBOARD</td></tr>
		  <tr><td height=12></td></tr>

          <tr>
              <td class="feed_sub_header" align=center><b>Footprint Diversity</b></td>
              <td class="feed_sub_header" align=center><b>Contract & Award Diversity</b></td>
          </tr>
          <tr><td width=48% align=center>

		  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		  <script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Company', 'Departments', 'Agencies'],
					<cfoutput query="companies">
				["'#company_name#'", #dept#, #agencies#],

				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {width: '85%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 10},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Dept & Agencies',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_1'));

			  chart.draw(data, options);
			}

			</script>

		  <div id="chart_div_1"></div>


</td><td width=48% align=center>

		  <script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Company', 'Contracts', 'Awards'],
				<cfoutput query="companies">
				["'#company_name#'", #contracts#, #awards#],

				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {width: '85%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 10},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Contracts & Awards',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_2'));

			  chart.draw(data, options);
			}

			</script>

		  <div id="chart_div_2"></div>

</td></tr>

<tr><td height=10></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td class="link_small_gray" colspan=3>Award information is limited to the last 3 years.</td></tr>
		  <tr><td class="feed_sub_header" colspan=3>PORTFOLIO DETAIL
		  <cfoutput>
			  <cfif #companies.recordcount# is 0>
			   ( No Companies Selected )
			  <cfelseif #companies.recordcount# is 1>
			   ( 1 Company )
			  <cfelse>
			   ( #companies.recordcount# Companies )
			  </cfif>
		  </cfoutput>

		  </td></tr>

		  <tr><td height=10></td></tr>

		   <tr>
			   <td></td>
			   <td class="feed_sub_header">Company</td>
			   <td></td>
			   <td class="feed_sub_header">DUNS</td>
			   <td class="feed_sub_header" align=center>Departments</td>
			   <td class="feed_sub_header" align=center>Agencies</td>
			   <td class="feed_sub_header" align=center>Contracts</td>
			   <td class="feed_sub_header" align=center>Awards</td>
		   </tr>

		  <cfset counter = 0>

			  <cfloop query="companies">

			    <cfquery name="innet" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			     select hub_comp_id from hub_comp
			     where hub_comp_hub_id = #session.hub# and
			           hub_comp_company_id = #companies.company_id#
			    </cfquery>


			  <cfoutput>

					   <cfif counter is 0>
					    <tr bgcolor="ffffff" height=20>
					   <cfelse>
					    <tr bgcolor="e0e0e0" height=20>
					   </cfif>

					        <td width=50>

							<cfif companies.company_logo is "">
							  <img src="//logo.clearbit.com/#companies.company_website#" width=30 border=0 onerror="this.src='/images/no_logo.png'">
							<cfelse>
							  <img src="#media_virtual#/#companies.company_logo#" width=30 border=0>
							</cfif>

                            </td>

                             <td class="feed_sub_header" width=250><a href="/exchange/portfolio/manage_company.cfm?comp_id=#companies.company_id#"><b>#ucase(companies.company_name)#</b></a></td>
                             <td width=100>

                             <cfif innet.recordcount GT 0>
                              <img src="/images/in_network.png" width=25 border=0 alt="In Network Company" title="In Network Company">
                             </cfif>


                             </td>
                             <td class="feed_sub_header" width=200 style="font-weight: normal;"><cfif #companies.company_duns# is "">Not Listed<cfelse>#companies.company_duns#</cfif></td>
                             <td class="feed_sub_header" width=100 style="font-weight: normal;" align=center>#numberformat(companies.dept,'999,999')#</td>
                             <td class="feed_sub_header" width=100 style="font-weight: normal;" align=center>#numberformat(companies.agencies,'999,999')#</td>
                             <td class="feed_sub_header" width=100 style="font-weight: normal;" align=center>#numberformat(companies.contracts,'999,999')#</td>
                             <td class="feed_sub_header" width=100 style="font-weight: normal;" align=center>#numberformat(companies.awards,'999,999')#</td>
                             <td class="text_xsmall" align=right width=50>

                             <cfif session.portfolio_access_level GT 1>
                             &nbsp;&nbsp;&nbsp;&nbsp;<a href="remove_comp.cfm?company_id=#company_id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this company from your portfolio?');"><img src="/images/delete.png" width=15 border=0 alt="Remove" title="Remove">
                             <cfelse>
                             &nbsp;
                             </cfif></td>

                         </tr>

                       <cfif counter is 0>
                        <cfset counter = 1>
                       <cfelse>
                        <cfset counter = 0>
                       </cfif>

			  </cfoutput>

			  </cfloop>

			  </td></tr>

		    </table>

		    </td></tr>

		    </table>

		  </cfif>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>