<cfinclude template="/exchange/security/check.cfm">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from person_portfolio
 where person_portfolio_id = #session.person_portfolio_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <tr><td valign=top width=185>

   <cfinclude template="/exchange/components/my_profile/profile.cfm">
   <cfinclude template="recent.cfm">

   </td><td valign=top>

   <cfinclude template="port_header.cfm">

   <div class="main_box">
	  <cfinclude template="portfolio_header.cfm">

<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select person_portfolio_item_person_id from person_portfolio_item
 where person_portfolio_item_portfolio_id = #session.person_portfolio_id# and
       person_portfolio_item_hub_id = #session.hub#
</cfquery>

<cfif list.recordcount is 0>
 <cfset person_list = 0>
<cfelse>
 <cfset person_list = valuelist(list.person_portfolio_item_person_id)>
</cfif>

<cfquery name="persons" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_id in (#person_list#)
 order by person_last_name
</cfquery>

		 <cfif persons.recordcount is 0>
			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">No people have been added to this Portfolio.</td></tr>
  	         </table>
		 <cfelse>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Last Name</b></a><cfif isdefined("sv") and sv is 1>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>First Name</b></a><cfif isdefined("sv") and sv is 2>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Title</b></a><cfif isdefined("sv") and sv is 3>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Company</b></a><cfif isdefined("sv") and sv is 4>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><i class="fa fa-share-alt" aria-hidden="true" alt="Relationships" title="Relationships"></i></td>
            <td class="feed_sub_header" align=right>Social Links</td>
            <td></td>
         </tr>

		 <cfset counter = 0>

		 <cfloop query="persons">

			<cfquery name="rels" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select count(person_rel_id) as total from person_rel
			 where person_rel_person_id = #persons.person_id#
			</cfquery>

		 <cfoutput>

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=30>
         <cfelse>
          <tr bgcolor="e0e0e0" height=30>
         </cfif>

             <td class="feed_option" style="font-weight: normal;" width=60><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#person_photo#" onerror="this.onerror=null; this.src='/images/private.png'" width=40 style="border-radius: 100px;"></a></td>
             <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#person_last_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#person_first_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;">#person_title#</td>
             <td class="feed_sub_header" style="font-weight: normal;"><a href="get_comp.cfm?id=#person_cb_company_id#" target="_blank" rel="noopener" rel="noreferrer">#person_company#</a></td>

             <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #rels.total# GT 0><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#rels.total#</b></a></cfif></td>

	         <td class="feed_sub_header" style="font-weight: normal;" align=right>

             <cfif person_facebook is not ""><a href="#person_facebook#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=25 hspace=5></a></cfif>
             <cfif person_twitter is not ""><a href="#person_twitter#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=25 hspace=5></a></cfif>
             <cfif person_linkedin is not ""><a href="#person_linkedin#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=25 hspace=5></a></cfif>

             </td>


         </tr>
         </cfoutput>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

         </table>

			 </cfif>






   </div>

   </td></tr>

    </table>

   </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>