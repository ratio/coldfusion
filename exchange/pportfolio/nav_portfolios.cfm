<!--- Portfolios --->

<div class="right_box">

	<center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><b>PORTFOLIO NAVIGATOR</b></td>
		      <td align=right valign=top></td></tr>
		  <tr><td height=10></td></tr>
		  </table>

		  <!--- Company Portfolios --->

		  <cfquery name="c_portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio
		   where portfolio_company_id = #session.company_id# and
			     (portfolio_access_id = 2 or portfolio_usr_id = #session.usr_id#) and
			     portfolio_type_id = 1
		   order by portfolio_name DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=3 class="feed_sub_header">Company Portfolios</td></tr>
          <tr><td height=10></td></tr>

          <cfset count = 1>

		  <cfloop query = "c_portfolios">

		  <cfquery name="item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(portfolio_item_id) as total from portfolio_item
		   where portfolio_item_portfolio_id = #c_portfolios.portfolio_id#
		  </cfquery>

		  <cfoutput>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/navigator.cfm?portfolio_id=#c_portfolios.portfolio_id#&portfolio_type_id=#c_portfolios.portfolio_type_id#"><img src="/images/icon_portfolio_company.png" width=18 border=0 alt="Company Portfolio" title="Company Porfolio"></a></td>
					   <td class="link_med_blue"><a href="/exchange/portfolio/navigator.cfm?portfolio_id=#c_portfolios.portfolio_id#&portfolio_type_id=#c_portfolios.portfolio_type_id#"><cfif len(c_portfolios.portfolio_name) GT 22>#left(c_portfolios.portfolio_name,'22')#...<cfelse>#c_portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
					</tr>

			  <cfif count is not c_portfolios.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

	    </cfoutput>

		  </td></tr>

		  </cfloop>

	</center>

	</table>

    <!--- Award Portfolios --->

		  <cfquery name="a_portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio
		   where portfolio_company_id = #session.company_id# and
			     (portfolio_access_id = 2 or portfolio_usr_id = #session.usr_id#) and
			     portfolio_type_id = 2
		   order by portfolio_name DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>
          <tr><td colspan=3 class="feed_sub_header">Award Portfolios</td></tr>
          <tr><td height=10></td></tr>

          <cfset count = 1>

		  <cfloop query = "a_portfolios">

		  <cfquery name="item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(portfolio_item_id) as total from portfolio_item
		   where portfolio_item_portfolio_id = #a_portfolios.portfolio_id#
		  </cfquery>

		  <cfoutput>

			   <tr>
				   <td width=30><a href="/exchange/portfolio/navigator.cfm?portfolio_id=#a_portfolios.portfolio_id#&portfolio_type_id=#a_portfolios.portfolio_type_id#"><img src="/images/icon_portfolio_award.png" width=18 border=0 alt="Award Portfolio" title="Award Porfolio"></a></td>
				   <td class="link_med_blue"><a href="/exchange/portfolio/navigator.cfm?portfolio_id=#a_portfolios.portfolio_id#&portfolio_type_id=#a_portfolios.portfolio_type_id#"><cfif len(a_portfolios.portfolio_name) GT 22>#left(a_portfolios.portfolio_name,'22')#...<cfelse>#a_portfolios.portfolio_name#</cfif></a></td>
				   <td class="link_med_blue" align=right>#item.total#</td>
			   </tr>

			  <cfif count is not a_portfolios.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

	    </cfoutput>

		  </td></tr>

		  </cfloop>

	</center>

	</table>

</div>

