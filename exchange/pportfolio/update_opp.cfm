<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="update_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio_item
	 set portfolio_item_comments = '#portfolio_item_comments#',
	     portfolio_item_name = '#portfolio_item_name#',
	     portfolio_item_updated = #now()#
	 where portfolio_item_id = #portfolio_item_id# and
		   portfolio_item_usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="update_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio
	 set portfolio_updated = #now()#
	 where portfolio_id = #portfolio_id# and
		   portfolio_usr_id = #session.usr_id#
	</cfquery>

</cftransaction>

<cflocation URL="/exchange/portfolio/manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&u=1" addtoken="no">
