<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item
 where portfolio_item_id = #portfolio_item_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfif #session.portfolio_location# is "Home">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

       <cfelseif #session.portfolio_location# is "Navigator">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/nav_company.cfm">
		   <cfinclude template="/exchange/portfolio/nav_award.cfm">

       </cfif>
      <td valign=top>

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
             <tr><td class="feed_header">PORTFOLIO AWARD UPDATE</td>
                 <td class="feed_option" align=right>

                 <a href="/exchange/portfolio/manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
             <tr><td colspan=2><hr></td></tr>
          </cfoutput>

          </table>

          <form action="/exchange/portfolio/update_opp.cfm" method="post">

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <cfoutput>
					 <tr><td class="feed_sub_header"><b>Opportunity Name</b></td></tr>
					 <tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  class="input_text" name="portfolio_item_name" size=60 value="#port_info.portfolio_item_name#"></td></tr>
					 <tr><td height=5></td></tr>
					 <tr><td class="feed_sub_header"><b>Notes & Comments</b><td></tr>
					 <tr><td height=5></td></tr>
					 <tr><td><textarea name="portfolio_item_comments" cols=120 rows=15 class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')" >#port_info.portfolio_item_comments#</textarea></td></tr>
					 <tr><td height=10></td></tr>
					 <tr><td><input type="submit" name="button" class="button_blue_large" value="Update">

					 </td></tr>
  					<input type="hidden" name="portfolio_item_id" value=#portfolio_item_id#>
					<input type="hidden" name="portfolio_id" value=#portfolio_id#>
				</cfoutput>

				</table>

			</form>

		 </td></tr>

       </table>

  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>