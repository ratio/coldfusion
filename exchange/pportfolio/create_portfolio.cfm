<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

		   <cfinclude template="/exchange/components/my_profile/profile.cfm">
		   <cfinclude template="recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <form action="save.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Create Portfolio</td>
            <td align=right class="feed_sub_header"><a href="/exchange/portfolio/">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=150><b>Portfolio Name</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  class="input_text"  style="width: 534px;" name="person_portfolio_name" style="width:300px;" placeholder="Please give this portfolio a name." required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="person_portfolio_desc" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  cols=70 rows=5 placeholder="Please provide a high level description of this portfolio."></textarea></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Focus Area(s)</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  class="input_text"  style="width: 534px;" name="person_portfolio_keywords" style="width:300px;" placeholder="Tags and keywords that frame the focus of this portfolio.">
             &nbsp;&nbsp;seperated with commas (i.e., Energy, Machine Learning, etc.)
             </td></tr>

		 <tr><td height=5></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Portfolio Image</td>
		 	 <td class="feed_sub_header" style="font-weight: normal;">

			  <input type="file" id="image" onchange="validate_img()" name="person_portfolio_image"></td></tr>

          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
          <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Create Portfolio"></td></tr>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>