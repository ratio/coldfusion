<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

 <cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from portfolio
  where portfolio_id = #session.portfolio_id#
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfif #session.portfolio_location# is "Home">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

       <cfelseif #session.portfolio_location# is "Navigator">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/nav_company.cfm">
		   <cfinclude template="/exchange/portfolio/nav_award.cfm">
           <cfinclude template="/exchange/portfolio/nav_opp.cfm">

       </cfif>

      </td><td valign=top>

      <cfif #session.portfolio_location# is "Home">
	      <cfinclude template="/exchange/portfolio/jump.cfm">
      </cfif>

		  <cfquery name="opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio_item
		   left join award_data on portfolio_item_value_id = id
                  where portfolio_item_portfolio_id = #session.portfolio_id# and
                        portfolio_item_type_id = 2
		  </cfquery>

      <div class="main_box">

          <cfinclude template="/exchange/portfolio/portfolio_header.cfm">

		  <cfif opps.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">No Awards have been added to this portfolio.  To add Awards, visit <a href="/exchange/awards/"><b>Federal Awards</b></a> and select <img src="/images/icon_green_add.png" width=20 hspace=5 align=absmiddle>&nbsp;<u>Save to Award Portfolio</u> option when viewing an Award profile.</td></tr>
          </table>

		  <cfelse>

		  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header">PORTFOLIO DASHBOARD</td>
				  <td class="feed_sub_header" align=right>
				  <a href="/exchange/portfolio/portfolio_timeline.cfm?portfolio_id=#session.portfolio_id#"><img src="/images/icon_gannt.png" height=20 border=0 alt="Schedule & Timeline" title="Schedule & Timeline"></a>
				  &nbsp;&nbsp;<a href="/exchange/portfolio/portfolio_timeline.cfm?portfolio_id=#session.portfolio_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>Integrated Schedule & Timeline</b></a>
				  </td></tr>
			  <tr><td height=20></td></tr>
		  </table>
		  </cfoutput>

		  <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>
               <td class="feed_option" align=center><b>By Department</b></td>
               <td class="feed_option" align=center><b>By Agency</b></td>
               <td class="feed_option" align=center><b>By NAICS</b></td>
               <td class="feed_option" align=center><b>By PSC</b></td>
               <td class="feed_option" align=center><b>By Pricing</b></td>
               <td class="feed_option" align=center><b>By Vendor</b></td>
            </tr>

            <tr><td height=10></td></tr>

            <tr>
               <td align=center>

				    <cfquery name="graph1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select awarding_agency_name, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #session.portfolio_id# and
								 federal_action_obligation >= 0 and
								 portfolio_item_type_id = 2
				    group by awarding_agency_name
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph1">
						   ['#awarding_agency_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
                        chartArea: {'width': '100%', 'height': '100%'},
                        width: 165,
                        pieHole: 0.4,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('dept_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="dept_graph"></div>

               </td>

               <td align=center>

				    <cfquery name="graph2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select awarding_sub_agency_name, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #session.portfolio_id# and
								 federal_action_obligation >= 0 and
								 portfolio_item_type_id = 2
				    group by awarding_sub_agency_name
				    </cfquery>


					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Agency', 'Award Value'],
						  <cfoutput query="graph2">
						   ['#awarding_sub_agency_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
                        pieHole: 0.4,
                        chartArea: {'width': '100%', 'height': '100%'},
                        width: 165,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('agency_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="agency_graph"></div>

               </td>

               <td align=center>

				    <cfquery name="graph3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select naics_code, naics_description, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #session.portfolio_id# and
								 federal_action_obligation >= 0 and
								 portfolio_item_type_id = 2
				    group by naics_code, naics_description
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph3">
						   ['#naics_code# - #naics_description#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
                        chartArea: {'width': '100%', 'height': '100%'},
                        width: 165,
                        pieHole: 0.4,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="naics_graph"></div>

               </td>

               <td align=center>

				    <cfquery name="graph4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select product_or_service_code, product_or_service_code_description, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #session.portfolio_id# and
								 federal_action_obligation >= 0 and
								 portfolio_item_type_id = 2
				    group by product_or_service_code, product_or_service_code_description
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph4">
						   ['#product_or_service_code# - #product_or_service_code_description#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
                        chartArea: {'width': '100%', 'height': '100%'},
                        pieHole: 0.4,
                        width: 165,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="psc_graph"></div>

               </td>

               <td align=center>

				    <cfquery name="graph5" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select type_of_contract_pricing, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #session.portfolio_id# and
								 federal_action_obligation >= 0 and
								 portfolio_item_type_id = 2
				    group by type_of_contract_pricing
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph5">
						   ['#type_of_contract_pricing#',#round(total)#],
						  </cfoutput>
						]);
						var options = {
						legend: 'none',
                        chartArea: {'width': '100%', 'height': '100%'},
                        pieHole: 0.4,
                        width: 165,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('pricing_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="pricing_graph"></div>

               </td>


               <td align=center>

				    <cfquery name="graph6" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select recipient_name, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #session.portfolio_id# and
								 federal_action_obligation >= 0 and
								 portfolio_item_type_id = 2
				    group by recipient_name
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Vendor', 'Award Value'],
						  <cfoutput query="graph6">
						   ['#recipient_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
                        chartArea: {'width': '100%', 'height': '100%'},
						legend: 'none',
						width: 165,
                        pieHole: 0.4,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('company_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="company_graph"></div>

               </td>

            </tr>

          </table>

          </center>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=30></td></tr>
		  <tr><td colspan=12><hr></td></tr>
		  <tr><td class="feed_sub_header" colspan=3>PORTFOLIO DETAIL <cfif #opps.recordcount# GT 0>( <cfoutput>#opps.recordcount#</cfoutput> Companies )</cfif></td></tr>
		  <tr><td height=10></td></tr>

		  <tr height=30>
		     <td class="feed_option"><b>Name</b></td>
		     <td class="feed_option"><b>Award #</b></td>
		     <td class="feed_option"><b>Vendor</b></td>
		     <td class="feed_option"><b>Department</b></td>
		     <td class="feed_option"><b>Agency</b></td>
		     <td class="feed_option" align=center><b>NAICS</b></td>
		     <td class="feed_option" align=center><b>PoP Start</b></td>
		     <td class="feed_option" align=center><b>PoP End</b></td>
		     <td class="feed_option" align=right><b>Award</b></td>
		     <td>&nbsp;</td>
		  </tr>

		  <cfset counter = 0>

			  <cfoutput query="opps">

                <cfif counter is 0>
                 <tr bgcolor="ffffff" height=25>
                <cfelse>
                 <tr bgcolor="e0e0e0" height=25>
                </cfif>

                    <td class="text_xsmall" valign=middle><a href="/exchange/portfolio/manage_award.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#session.portfolio_id#"><b>#ucase(portfolio_item_name)#</b></a></td>
                    <td class="text_xsmall" valign=middle><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>#award_id_piid#</b></a></td>
                    <td class="text_xsmall" valign=middle><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>#recipient_name#</b></a></td>
                    <td class="text_xsmall" valign=middle>#awarding_agency_name#</td>
                    <td class="text_xsmall" valign=middle>#awarding_sub_agency_name#</td>
                    <td class="text_xsmall" valign=middle align=center>#naics_code#</td>
                    <td class="text_xsmall" valign=middle align=center>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
                    <td class="text_xsmall" valign=middle align=center>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
                    <td class="text_xsmall" valign=middle align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
                    <td class="text_xsmall" valign=middle align=right>

                    <cfif opps.portfolio_item_usr_id is #session.usr_id#>

                    &nbsp;&nbsp;&nbsp;&nbsp;<a href="remove_award.cfm?id=#id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this award from your portfolio?');"><img src="/images/delete.png" width=12 border=0 alt="Remove" title="Remove">
                    <cfelse>
                    &nbsp;
                    </cfif>

                    </td>
                </tr>

                <cfif counter is 0>
                 <cfset counter = 1>
                <cfelse>
                 <cfset counter = 0>
                </cfif>

			  </cfoutput>

		    </table>

		  </cfif>

        </td></tr>

     </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>