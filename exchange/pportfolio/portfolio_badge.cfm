<style>
.scroll {
    height: 300px;
    background-color: ffffff;
    overflow:auto;
}
</style>

<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select person_portfolio_item_person_id from person_portfolio_item
 where person_portfolio_item_portfolio_id = #myportfolio.person_portfolio_id# and
       person_portfolio_item_hub_id = #session.hub#
</cfquery>

<cfif list.recordcount is 0>
 <cfset person_list = 0>
<cfelse>
 <cfset person_list = valuelist(list.person_portfolio_item_person_id)>
</cfif>

<cfquery name="persons" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_id in (#person_list#)
 order by person_last_name
</cfquery>

<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=middle width=70>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<cfif #myportfolio.person_portfolio_image# is "">
			  <tr><td><a href="refresh.cfm?i=#encrypt(myportfolio.person_portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/icon_portfolio_company.png" height=60 border=0 alt="Open" title="Open"></a>
			  </td></tr>
			<cfelse>
			  <tr><td><a href="refresh.cfm?i=#encrypt(myportfolio.person_portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="#media_virtual#/#myportfolio.person_portfolio_image#" height=60 border=0 alt="Open" title="Open"></a></td></tr>
			</cfif>
		 </table>

     <td width=20>&nbsp;</td><td valign=middle>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_title" valign=top><a href="refresh.cfm?i=#encrypt(myportfolio.person_portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><cfif len(myportfolio.person_portfolio_name) GT 40>#left(myportfolio.person_portfolio_name,'40')#...<cfelse>#myportfolio.person_portfolio_name#</cfif></a></td></tr>
		   <tr><td class="feed_option"><b>Portfolio Manager - #myportfolio.usr_first_name# #myportfolio.usr_last_name#</b></td></tr>
		 </table>

     </td></tr>

     <tr><td colspan=3><hr></td></tr>

    </table>

    </cfoutput>

    <div class="scroll">

		 <cfif persons.recordcount is 0>
			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">No people have been added to this Portfolio.</td></tr>
  	         </table>
		 <cfelse>

		 <cfset counter = 0>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfoutput query="persons">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=30>
         <cfelse>
          <tr bgcolor="e0e0e0" height=30>
         </cfif>

             <td class="feed_option" style="font-weight: normal;" width=60><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#person_photo#" onerror="this.onerror=null; this.src='/images/private.png'" width=40 style="border-radius: 100px;"></a></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=150><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#person_full_name#</a></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=250>#person_title#</td>
             <td class="feed_sub_header" style="font-weight: normal;" width=250><a href="get_comp.cfm?id=#person_cb_company_id#" target="_blank" rel="noopener" rel="noreferrer">#person_company#</a></td>
	         <td class="feed_sub_header" style="font-weight: normal;" width=125 align=right>

             <cfif person_facebook is not ""><a href="#person_facebook#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=25 hspace=5></a></cfif>
             <cfif person_twitter is not ""><a href="#person_twitter#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=25 hspace=5></a></cfif>
             <cfif person_linkedin is not ""><a href="#person_linkedin#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=25 hspace=5></a></cfif>

             </td>


         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </table>

			 </cfif>

       </div>
