 <cfquery name="port_comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select portfolio_item_id, portfolio_item_duns from portfolio_item
  where portfolio_item_type_id = 1
 </cfquery>
 
 <cfloop query="port_comp">
 
  <cfquery name="get_duns" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select company_id from company
   where company_duns = '#port_comp.portfolio_item_duns#'
  </cfquery>
 
   <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    update portfolio_item
    set portfolio_item_company_id = '#get_duns.company_id#'
    where portfolio_item_id = #port_comp.portfolio_item_id#
  </cfquery>
 
 </cfloop>
 
 Done