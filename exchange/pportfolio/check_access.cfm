<cfset portfolio_access = 0>
<cfset session.portfolio_access_level = 0>

<!--- Check for Admin access --->

<cfquery name="admin_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select portfolio_id from portfolio
 where portfolio_usr_id = #session.usr_id# and
       portfolio_id = #session.portfolio_id#
</cfquery>

<cfif admin_access.recordcount is 1>
	 <cfset portfolio_access = 1>
	 <cfset session.portfolio_access_level = 3>
<cfelse>

	 <!--- Check for user Access --->

	<cfquery name="user_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_access from sharing
	 where  sharing_portfolio_id = #session.portfolio_id# and
			sharing_hub_id = #session.hub# and
			sharing_to_usr_id = #session.usr_id#
	</cfquery>

	<cfif user_access.recordcount is 1>

	 	<cfset portfolio_access = 1>
	 	<cfset session.portfolio_access_level = #user_access.sharing_access#>

    <cfelse>

		<cfquery name="port_company_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from portfolio
		 where portfolio_id = #session.portfolio_id#
		</cfquery>

        <cfif port_company_access.portfolio_company_access is 1>
          <cfset portfolio_access = 1>
		  <cfset session.portfolio_access_level = 1>
        </cfif>

    </cfif>

</cfif>

<cfif portfolio_access is 0>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

