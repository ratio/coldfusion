<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script type="text/javascript">

/***********************************************
* Limit number of checked checkboxes script- by JavaScript Kit (www.javascriptkit.com)
* This notice must stay intact for usage
* Visit JavaScript Kit at http://www.javascriptkit.com/ for this script and 100s more
***********************************************/

function checkboxlimit(checkgroup, limit){
	var checkgroup=checkgroup
	var limit=limit
	for (var i=0; i<checkgroup.length; i++){
		checkgroup[i].onclick=function(){
		var checkedcount=0
		for (var i=0; i<checkgroup.length; i++)
			checkedcount+=(checkgroup[i].checked)? 1 : 0
		if (checkedcount>limit){
			alert("You can only select a maximum of "+limit+" companies")
			this.checked=false
			}
		}
	}
}

</script>

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfset page = "Open">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfquery name="port_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select portfolio_item_company_id from portfolio_item
  where portfolio_item_portfolio_id = #session.portfolio_id# and
		portfolio_item_type_id = 1
</cfquery>

<cfif port_list.recordcount is 0>
	<cfset plist = 0>
<cfelse>
	<cfset plist = valuelist(port_list.portfolio_item_company_id)>
</cfif>

<cfset end_date = #dateformat(now(),'mm/dd/yyyy')#>
<cfset start_date = dateformat(dateadd("yyyy",-3,end_date),'mm/dd/yyyy')>


 <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from company
  where company_id in (#plist#)
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="recent.cfm">

       </td><td valign=top>

       <cfinclude template="port_header.cfm">

       <div class="main_box">

          <cfinclude template="portfolio_header.cfm">

		  <cfif companies.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif session.portfolio_access_level GTE 2>
	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.</td></tr>
           <cfelse>
	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.<br><br>To add Companies, go to <a href="/exchange/marketplace/companies/network_in.cfm"><b>In Network</b></a> or <a href="/exchange/marketplace/companies/network_out.cfm"><b>Out of Network</b></a> Companies and choose the <u>Add to Portfolio</u> option when viewing the Company profile.</td></tr>
	       </cfif>

          </table>

		  <cfelse>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>

		  <tr><td class="feed_header" colspan=5>Portfolio Companies
		  <cfoutput>
			  <cfif #companies.recordcount# is 0>
			   ( No Companies Selected )
			  <cfelseif #companies.recordcount# is 1>
			   ( 1 Company )
			  <cfelse>
			   ( #companies.recordcount# Companies )
			  </cfif>
		  </cfoutput>

		  </td></tr>

		  <tr><td height=10></td></tr>

		   <tr>
			   <td class="feed_sub_header">Compare</td>
			   <td></td>
			   <td class="feed_sub_header" style="padding-left: 5px; padding-right: 5px;">Company</td>
			   <td></td>
			   <td class="feed_sub_header" style="padding-left: 5px; padding-right: 5px;">Website</td>
			   <td class="feed_sub_header" style="padding-left: 5px; padding-right: 5px;" align=center>DUNS</td>
			   <td class="feed_sub_header" style="padding-left: 5px; padding-right: 5px;">Keywords</td>
			   <td class="feed_sub_header" style="padding-left: 5px; padding-right: 5px;">Location</td>

		   </tr>

		  <cfset counter = 0>

		  <form name="compare" id="compare" action="portfolio_compare_set.cfm" method="post">

			  <cfloop query="companies">

			    <cfquery name="innet" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			     select hub_comp_id from hub_comp
			     where hub_comp_hub_id = #session.hub# and
			           hub_comp_company_id = #companies.company_id#
			    </cfquery>

			  <cfoutput>

					   <cfif counter is 0>
					    <tr bgcolor="ffffff" height=20>
					   <cfelse>
					    <tr bgcolor="e0e0e0" height=20>
					   </cfif>

					        <td><input type="checkbox" name="company_id" value=#companies.company_id# style="margin-left: 20px; margin-right: 20px; width: 20px; height: 20px;"></td>

					        <td width=50>

							<a href="/exchange/portfolio/manage_company.cfm?comp_id=#companies.company_id#">
							<cfif companies.company_logo is "">
							  <img src="//logo.clearbit.com/#companies.company_website#" width=30 border=0 onerror="this.src='/images/no_logo.png'">
							<cfelse>
							  <img src="#media_virtual#/#companies.company_logo#" width=30 border=0>
							</cfif>
							</a>

                            </td>

                             <td class="feed_sub_header" style="padding-left: 5px; padding-right: 5px;" width=300><a href="/exchange/portfolio/manage_company.cfm?comp_id=#companies.company_id#"><b>#ucase(companies.company_name)#</b></a></td>
                             <td width=100 align=center>

                             <cfif innet.recordcount GT 0>
                              <img src="/images/in_network.png" width=25 border=0 alt="In Network Company" title="In Network Company">
                             </cfif>

                             </td>
                             <td class="feed_sub_header" style="font-weight: normal; padding-left: 5px; padding-right: 5px;"><cfif #companies.company_website# is "">Not Listed<cfelse><a style="font-weight: normal;" href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#companies.company_website#</a></cfif></td>
                             <td class="feed_sub_header" style="font-weight: normal; padding-left: 5px; padding-right: 5px;" align=center><cfif #companies.company_duns# is "">Not Listed<cfelse>#companies.company_duns#</cfif></td>
                             <td class="feed_sub_header" style="font-weight: normal; padding-left: 5px; padding-right: 5px;" width=500>
                             <cfif #companies.company_keywords# is "">
                              No keywords found
                             <cfelse>#companies.company_keywords#
                             </cfif></td>
                             <td class="feed_sub_header" style="font-weight: normal; padding-left: 5px; padding-right: 5px;" width=150>#companies.company_city#<cfif #companies.company_state# is not "">, #companies.company_state#</cfif></td>
                             <td class="text_xsmall" align=right width=50>

                             <cfif session.portfolio_access_level GT 1>
                             &nbsp;&nbsp;&nbsp;&nbsp;<a href="remove_comp.cfm?company_id=#company_id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this company from your portfolio?');"><img src="/images/delete.png" width=15 border=0 alt="Remove" title="Remove">
                             <cfelse>
                             &nbsp;
                             </cfif></td>

                         </tr>

                       <cfif counter is 0>
                        <cfset counter = 1>
                       <cfelse>
                        <cfset counter = 0>
                       </cfif>

			  </cfoutput>

			  </cfloop>

			 <script type="text/javascript">

			 	//Syntax: checkboxlimit(checkbox_reference, limit)
			 	checkboxlimit(document.forms.compare.company_id, 5)

			</script>

			  </td></tr>

			  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>


			                <tr><td height=20></td></tr>
			                <tr><td><input type="submit" name="button" class="button_blue_large" value="Compare Companies" onclick="javascript:document.getElementById('page-loader').style.display='block';"></td></tr>

			  		      <tr><td height=10></td></tr>
			  		      <tr><td><hr></td></tr>

			  		      <tr><td class="link_small_gray">- Maximum of 5 companies can be selected for comparison.</td></tr>
			  		      <tr><td class="link_small_gray">- Customers, contracts and award amounts are based on the last 3 years of performance.</td></tr>
		    </table>

		    </form>

		    </td></tr>

		    </table>

		  </cfif>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>