<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company_intel
 where company_intel_id = #cid#
</cfquery>

<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from comments_rating
 order by comments_rating_value DESC
</cfquery>

<cfquery name="get_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select portfolio_item_id from portfolio_item
 where portfolio_item_portfolio_id = #session.portfolio_id# and
       portfolio_item_company_id = #comp_id#
</cfquery>

<cfquery name="company_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #comp_id#
</cfquery>

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item
 where portfolio_item_id = #get_info.portfolio_item_id#
</cfquery>

<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns = '#port_info.portfolio_item_duns#'
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="recent.cfm">

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">

			<cfif company_info.company_logo is "">
			  <img src="//logo.clearbit.com/#company_info.company_website#" height=50 onerror="this.src='/images/no_logo.png'" border=0>
			<cfelse>
			  <img src="#media_virtual#/#company_info.company_logo#" height=50 border=0>
			</cfif>
			</a>&nbsp;&nbsp;

            <cfif sams.recordcount is 0>
            <a href="/exchange/include/company_profile.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#company_info.company_name#</a>
            <cfelse>
            <a href="/exchange/include/federal_profile.cfm?duns=#sams.duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#company_info.company_name#</a>
            </cfif>

            </td>

                 <td class="feed_sub_header" align=right class="feed_sub_header">

                 <a href="manage_company.cfm?comp_id=#comp_id#">Return</a>

                 </td></tr>
             <tr><td colspan=2><hr></td></tr>

          </table>

       </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td class="feed_header">Edit Comment</td></tr>
           <tr><td height=10></td></tr>
          </table>

   <form action="add_comment_db.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfoutput>

		<tr><td class="feed_sub_header" width=200><b>Context</b></td>
			<td><input name="company_intel_context" class="input_text" type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')" size=100 value="#edit.company_intel_context#" maxlength="1000" required placeholder="What's the context of writing these comments?"></td>
		</tr>

		<tr><td class="feed_sub_header" valign=top><b>Description</b></td>
			<td><textarea name="company_intel_comments" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  rows=6 cols=101 placeholder="Please provide any comments related to this intel.">#edit.company_intel_comments#</textarea></td>
		</tr>

		<tr><td class="feed_sub_header"><b>Reference URL</b></td>
			<td><input name="company_intel_url" class="input_text" type="url" size=100 value="#edit.company_intel_url#" maxlength="1000"></td>
		</tr>

		</cfoutput>

		<tr><td height=10></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>

		<tr><td></td><td>
		<input class="button_blue_large" type="submit" name="button" value="Save Comment">
		&nbsp;
		<input class="button_blue_large" type="submit" name="button" value="Delete Comment" onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">

		</td></tr>

	   </table>

	   <cfoutput>
	   <input type="hidden" name="comp_id" value=#comp_id#>
	   <input type="hidden" name="company_intel_id" value=#cid#>
	   </cfoutput>

	   </form>



    </td></tr>

</table>




</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>