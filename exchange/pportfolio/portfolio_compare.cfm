<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">

</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 5>
</cfif>

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfset end_date = #dateformat(now(),'mm/dd/yyyy')#>
<cfset start_date = dateformat(dateadd("yyyy",-3,end_date),'mm/dd/yyyy')>

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfquery name="compare_data" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_id, company_name, company_logo, company_website,

 (select count(distinct(primary_place_of_performance_state_code)) from award_data where recipient_duns = company_duns and (action_date between '#start_date#' and '#end_date#')) as locations,
 (select sum(federal_action_obligation) from award_data where recipient_duns = company_duns and (action_date between '#start_date#' and '#end_date#')) as obligated,
 (select count(distinct(awarding_sub_agency_code)) from award_data where recipient_duns = company_duns and (action_date between '#start_date#' and '#end_date#')) as agency,
 (select count(distinct(awarding_agency_code)) from award_data where recipient_duns = company_duns and (action_date between '#start_date#' and '#end_date#')) as dept,
 (select count(distinct(award_id_piid)) from award_data where recipient_duns = company_duns and (action_date between '#start_date#' and '#end_date#')) as contracts
 <!--- (select count(id) from award_data where recipient_duns = company_duns and (action_date between '#start_date#' and '#end_date#')) as awards --->
 from company
 where company_id in (#session.compare_list#)
 order by company_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="recent.cfm">

       </td><td valign=top>

       <cfinclude template="port_header.cfm">

       <div class="main_box">

		<cfoutput>
		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><img src="/images/icon_portfolio_company.png" height=30>

			 &nbsp;&nbsp;<a href="portfolio_company.cfm">#ucase(port_info.portfolio_name)#</a></td>
			 <td class="feed_sub_header" align=right>

			 <cfif isdefined("d")>
				<a href="/exchange/portfolio/portfolio_dashboard.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';">Return</a>
			 <cfelse>
				<a href="/exchange/portfolio/portfolio_company.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';">Return</a>
		     </cfif>


			 </td></tr>

			 <cfif #port_info.portfolio_desc# is not "">
			  <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top colspan=2>#port_info.portfolio_desc#</td></tr>
			 </cfif>

			 <tr><td colspan=2><hr></td></tr>

		  </table>

		  </cfoutput>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td height=10></td></tr>
			  <tr><td class="feed_header" colspan=3>COMPARE COMPANIES</td></tr>
			  <tr><td height=12></td></tr>
		  </table>

		  <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select * from company
		   where company_id in (#session.compare_list#)
		   order by company_name
		  </cfquery>

		  <cfset colwidth = #evaluate(80/listlen(session.compare_list))#>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>
               <td width=20%>&nbsp;</td>

               <cfoutput query="companies">

                <td class="feed_sub_header" align=center width=#colwidth#%>

				<a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">
				<cfif companies.company_logo is "">
				  <img src="//logo.clearbit.com/#companies.company_website#" width=75 border=0 onerror="this.src='/images/no_logo.png'">
				<cfelse>
				  <img src="#media_virtual#/#companies.company_logo#" width=75 border=0>
				</cfif>
				</a>

                </td>
               </cfoutput>

            </tr>


            <tr>
               <td width=20%>&nbsp;</td>

               <cfoutput query="companies">
                 <td class="feed_sub_header" align=center width=#colwidth#><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#companies.company_name#</a></td>
               </cfoutput>

            </tr>


				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>

				<tr>
				   <td class="feed_sub_header">Total Revenue</td>

				   <cfoutput query="compare_data">
				   	<td class="feed_sub_header" align=center>#numberformat(obligated,'$999,999,999')#</td>
				   </cfoutput>

				</tr>

				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>

				<tr>
				   <td class="feed_sub_header">Departments</td>

				   <cfoutput query="compare_data">
				   	<td class="feed_sub_header" align=center>#numberformat(dept,'9,999')#</td>
				   </cfoutput>

				</tr>

				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>


				<tr>
				   <td class="feed_sub_header">Delivery Locations</td>

				   <cfoutput query="compare_data">
				   	<td class="feed_sub_header" align=center>

				   	#numberformat(locations,'9,999')#
				   	<cfif locations is 1>
				   	State
				   	<cfelse>
				   	States
				   	</cfif>
				   	</td>
				   </cfoutput>

				</tr>

				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>

				<tr>
				   <td class="feed_sub_header">Agencies</td>

				   <cfoutput query="compare_data">
				   	<td class="feed_sub_header" align=center>#numberformat(agency,'9,999')#</td>
				   </cfoutput>

				</tr>

				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>

				<tr>
				   <td class="feed_sub_header">Unique Contracts</td>

				   <cfoutput query="compare_data">
				   	<td class="feed_sub_header" align=center>#numberformat(contracts,'9,999')#</td>
				   </cfoutput>

				</tr>

				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>

				<tr>
				   <td class="feed_sub_header">Funding Received</td>

				   <cfloop query="compare_data">

					<cfquery name="funding" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					 select num_funding_rounds,total_funding_usd from company
					 left join cb_organizations on cb_organizations.uuid = company_cb_id
					 where company_id = #compare_data.company_id#
					</cfquery>

				   	<td class="feed_sub_header" align=center>

				     <cfif #funding.num_funding_rounds# is not "">

				     <cfoutput>
				     #funding.num_funding_rounds#
				     <cfif funding.num_funding_rounds is 1>
				      Round
				     <cfelse>
				     Rounds
				     </cfif><br>
				     #numberformat(funding.total_funding_usd,'$999,999,999')#
				     </cfoutput>

				     </cfif>

				   	</td>
				   </cfloop>

				</tr>

				<cfoutput>
				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
                </cfoutput>

<!---
				<tr>
				   <td class="feed_sub_header">Total Awards</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>

				<tr>
				   <td class="feed_sub_header">Subcontracts</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>

				<tr>
				   <td class="feed_sub_header">Subcontractors</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>

				<tr>
				   <td class="feed_sub_header">Grants</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>

				<tr>
				   <td class="feed_sub_header">SBIR/STTRs</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>

				<tr>
				   <td class="feed_sub_header">Patents</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>

				<tr>
				   <td class="feed_sub_header">Funding</td>
				</tr>

				<tr><td colspan=#evaluate(listlen(session.compare_list)+1)#><hr></td></tr>
 --->

          <tr><td colspan=2 class="link_small_gray">- Comparism data is based on the last 3 years of performance.</td></tr>

          </table>

	  </div>

      </td></tr>

    </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>