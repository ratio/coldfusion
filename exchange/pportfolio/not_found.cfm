<cfinclude template="/exchange/security/check.cfm">

<cfset session.portfolio_id = #portfolio_id#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item
 where portfolio_item_id = #portfolio_item_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
             <tr><td class="feed_header">#port_info.portfolio_item_name#</td>
                 <td class="feed_option" align=right>

                 <a href="/exchange/portfolio/portfolio_award.cfm?portfolio_id=#portfolio_id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
             <tr><td colspan=2><hr></td></tr>
          </cfoutput>

          </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header">AWARD SUMMARY</td><td class="feed_sub_header" align=right></td>
			    <td class="feed_sub_header" align=right>

			    </td></tr>
			<tr><td class="feed_sub_header" style="font-weight: normal;">Award information was not found.</td></tr>
			<tr><td height=10></td></tr>
		</table>

      </cfoutput>

    </td></tr>

</table>

</div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>