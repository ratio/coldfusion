<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save Portfolio">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select person_portfolio_image from person_portfolio
		  where person_portfolio_id = #session.person_portfolio_id# and
		        person_portfolio_usr_id = #session.usr_id#
		</cfquery>

		<cfif fileexists("#media_path#\#remove.person_portfolio_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.person_portfolio_image#">
		</cfif>

	</cfif>

	<cfif #person_portfolio_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select person_portfolio_image from person_portfolio
		  where person_portfolio_id = #session.person_portfolio_id#
		</cfquery>

		<cfif #getfile.person_portfolio_image# is not "">

		 <cfif fileexists("#media_path#\#getfile.person_portfolio_image#")>
		   <cffile action = "delete" file = "#media_path#\#getfile.person_portfolio_image#">
		 </cfif>

		</cfif>

		<cffile action = "upload"
		 fileField = "person_portfolio_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update person_portfolio
      set person_portfolio_name = '#person_portfolio_name#',
		  person_portfolio_desc = '#person_portfolio_desc#',
		  person_portfolio_keywords = '#person_portfolio_keywords#',

		  <cfif #person_portfolio_image# is not "">
		   person_portfolio_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   person_portfolio_image = null,
		  </cfif>

		  person_portfolio_updated = #now()#
      where person_portfolio_id = #session.person_portfolio_id# and
            person_portfolio_usr_id = #session.usr_id#
	</cfquery>

	<cflocation URL="open.cfm?u=1" addtoken="no">

<cfelseif #button# is "Create Portfolio">

	<cfif #person_portfolio_image# is not "">
		<cffile action = "upload"
		 fileField = "person_portfolio_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into person_portfolio
	 (
      person_portfolio_name,
      person_portfolio_keywords,
      person_portfolio_image,
      person_portfolio_desc,
      person_portfolio_usr_id,
      person_portfolio_company_id,
      person_portfolio_hub_id,
      person_portfolio_updated,
      person_portfolio_type_id
      )
      values
      (
      '#person_portfolio_name#',
      '#person_portfolio_keywords#',
       <cfif #person_portfolio_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
      '#person_portfolio_desc#',
       #session.usr_id#,
       <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
       <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
       #now()#,
       2
      )
	</cfquery>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete Portfolio">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select person_portfolio_image from person_portfolio
	  where person_portfolio_id = #session.portfolio_id# and
	        person_portfolio_usr_id = #session.usr_id#
	</cfquery>

    <cfif remove.portfolio_image is not "">

     <cfif fileexists("#media_path#\#remove.person_portfolio_image#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.person_portfolio_image#">
	 </cfif>

	</cfif>

	<cftransaction>

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete person_portfolio
		 where person_portfolio_id = #session.person_portfolio_id# and
			   person_portfolio_usr_id = #session.usr_id#
		</cfquery>

	</cftransaction>

	<cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>
