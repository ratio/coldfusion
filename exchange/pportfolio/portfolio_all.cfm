<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_company_id = #session.company_id# and
       (portfolio_access_id = 2 or portfolio_usr_id = #session.usr_id#)
 order by portfolio_updated DESC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">Portfolios</td>
                 <td class="feed_option" align=right><a href="create.cfm">Create Portfolio</a></td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option">Portfolios are groupings of Awards or Companies that you have added to one or more Portfolios.</td>
                 <td class="feed_option" align=right>
                 <b>Select View</b>&nbsp;&nbsp;
                 <select name="portfolio_access">
                  <option value=1>My Portfolios
                  <option value=2>Shared Portfolios
                  <option value=3>All Portfolios
                 </select>
                 &nbsp;&nbsp;<input type="submit" name="button" class="button_blue" style="font-size: 11px; height: 20px; width: 75px;" value="Refresh">
                  </td></tr>
             <tr><td height=5></td></tr>

           <cfif isdefined("u")>
            <cfif u is 1>
             <tr><td class="feed_option"><font color="green"><b>Portfolio has been added.</b></td></tr>
            <cfelseif u is 2>
             <tr><td class="feed_option"><font color="green"><b>Portfolio has been updated.</b></td></tr>
            <cfelseif u is 3>
             <tr><td class="feed_option"><font color="green"><b>Portfolio has been deleted.</b></td></tr>
            </cfif>

            <tr><td height=15></td></tr>

           <cfelse>

             <tr><td height=10></td></tr>

           </cfif>

          </table>

          <cfif portfolios.recordcount is 0>

              <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_option">You have not created any Portfolios.  Please <a href="create.cfm"><b>create one now</b></a>.</td></tr>
			  <tr><td height=10></td></tr>

			  <form action="db.cfm" method="post">

				 <tr><td class="feed_option"><b>Portfolio Name</b></td></tr>
				 <tr><td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  name="portfolio_name" size=60></td></tr>
				 <tr><td height=5></td></tr>
				 <tr><td class="feed_option"><b>Description</b></td></tr>
				 <tr><td class="feed_option"><textarea name="portfolio_desc" rows=5 cols=60></textarea></td></tr>
				 <tr><td height=5></td></tr>

             <tr><td class="feed_option"><b>Portfolio Type</b></td></tr>
             <tr><td class="feed_option">
             <select name="portfolio_type_id">
              <option value=1>Companies
              <option value=2>Awards
             </td></tr>

             <tr><td height=5></td></tr>

				 <tr><td class="feed_option"><b>Sharing</b></td></tr>
				 <tr><td class="feed_option">
				 <select name="portfolio_access_id">
				  <option value=1>Private (me only)
				  <option value=2>Me, and Company Users (view)
				 </td></tr>
				 <tr><td height=10></td></tr>
				 <tr><td><input type="submit" name="button" class="button_blue" style="font-size: 11px; height: 23px; width: 120px;" value="Create Portfolio">

			  </form>

			  </table>

          <cfelse>

			    <cfloop query="portfolios">

                <cfif portfolios.portfolio_type_id is 1>

                 <cfquery name="port_comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                  select  duns, legal_business_name, count(id) as awards, count(distinct(award_id_piid)) as contracts, sum(federal_action_obligation) as obligated from portfolio_item
                  left join sams on duns = portfolio_item_duns
                  left join award_data on recipient_duns = portfolio_item_duns
                  where portfolio_item_portfolio_id = #portfolio_id# and
                        portfolio_item_type_id = 1
                  group by duns, legal_business_name
                  order by legal_business_name
                 </cfquery>

			     <div class="portfolio_badge">

                 <cfquery name="owner" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                  select usr_first_name, usr_last_name from usr
                  where usr_id = #portfolios.portfolio_usr_id#
                 </cfquery>

                 <cfoutput>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_title" width=35><a href="portfolio_company.cfm?portfolio_id=#portfolios.portfolio_id#"><img src="/images/comp.png" height=20 border=0 title="Open" alt="Open"></a></td><td class="feed_title"><a href="portfolio_company.cfm?portfolio_id=#portfolios.portfolio_id#">#portfolios.portfolio_name#</a></td>
							<td class="feed_option" align=right>
							<cfif portfolios.portfolio_usr_id is #session.usr_id#>
							<a href="edit.cfm?portfolio_id=#portfolios.portfolio_id#"><img src="/images/icon_config.png" width=20 border=0 title="Edit" alt="Edit"></a>
							<cfelse>

							 <b>Shared by #owner.usr_first_name# #owner.usr_last_name#</b>
							</cfif></td></tr>
					   </table>

			     </cfoutput>

			     <div class="scroll_box">

			     <table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif port_comp.recordcount is 0>
					   <tr><td class="feed_option">No companies have been added to this portfolio.<br><br>To add a Company to this Portfolio, select Add to Portfolio in a Company's Marketplace profile.</td></tr>
					 <cfelse>

					   <tr><td height=5></td></tr>

					   <tr>
					       <td class="text_xsmall" width=150><b>Company</b></td>
					       <td class="text_xsmall"><b>DUNS</b></td>
					       <td class="text_xsmall" align=center><b>Contracts</b></td>
					       <td class="text_xsmall" align=center><b>Awards</b></td>
					       <td class="text_xsmall" align=right><b>Revenue</b></td>
					   </tr>

					   <tr><td height=5></td></tr>

					   <cfset counter = 0>

					   <cfoutput query="port_comp">

					   <cfif counter is 0>
					    <tr bgcolor="ffffff" height=20>
					   <cfelse>
					    <tr bgcolor="e0e0e0" height=20>
					   </cfif>

                             <td class="text_xsmall"><a href="/exchange/include/company_profile.cfm?id=0&duns=#duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#legal_business_name#</a></td>
                             <td class="text_xsmall">#duns#</td>
                             <td class="text_xsmall" align=center>#numberformat(contracts,'999,999')#</td>
                             <td class="text_xsmall" align=center>#numberformat(awards,'999,999')#</td>
                             <td class="text_xsmall" align=right>#numberformat(obligated,'$999,999,999')#</td>
                         </tr>

                       <cfif counter is 0>
                        <cfset counter = 1>
                       <cfelse>
                        <cfset counter = 0>
                       </cfif>

					   </cfoutput>

					   <tr><td colspan=5><hr></td></tr>

					 </cfif>

                 </table>

                 </div>

                 </div>

			    <cfelseif portfolios.portfolio_type_id is 2>

			     <div class="portfolio_badge">

                 <cfquery name="port_awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                  select * from portfolio_item
                  left join award_data on id = portfolio_item_value_id
                  where portfolio_item_portfolio_id = #portfolio_id# and
                        portfolio_item_type_id = 2
                 </cfquery>

                 <cfquery name="owner2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                  select usr_first_name, usr_last_name from usr
                  where usr_id = #portfolios.portfolio_usr_id#
                 </cfquery>

                 <cfoutput>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_title" width=35><a href="portfolio_award.cfm?portfolio_id=#portfolios.portfolio_id#"><img src="/images/awards.png" height=20 border=0 title="Open" alt="Open"></a></td><td class="feed_title"><a href="portfolio_award.cfm?portfolio_id=#portfolios.portfolio_id#">#portfolios.portfolio_name#</a></td>
							<td class="feed_option" align=right>
							<cfif portfolios.portfolio_usr_id is #session.usr_id#>
							<a href="edit.cfm?portfolio_id=#portfolios.portfolio_id#"><img src="/images/icon_config.png" width=20 border=0 title="Edit" alt="Edit"></a>
							<cfelse>
							 <b>Shared by #owner2.usr_first_name# #owner2.usr_last_name#</b>
							</cfif></td></tr>
					   </table>

				 </cfoutput>

				       <div class="scroll_box">

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					    <cfif port_awards.recordcount is 0>
					     <tr><td class="feed_option">No Awards have been added to Portfolio.<br><br>To add an award to this Portfolio, select Add to Portfolio when reviewing an Award Profle in Market Research or from within a Company's profile.</td></tr>
					    <cfelse>

					      <tr><td height=5></td></tr>

					      <cfset total = 0>

					      <tr>
					         <td class="text_xsmall" width=150><b>Opportunity Name</b></td>
					         <td class="text_xsmall"><b>Agency</b></td>
					         <td class="text_xsmall" align=center><b>PoP Start</b></td>
					         <td class="text_xsmall" align=center><b>PoP End</b></td>
					         <td class="text_xsmall" align=right><b>Award</b></td>
					      </tr>

					      <tr><td height=5></td></tr>

					    <cfset counter = 0>

					    <cfoutput query="port_awards">

					    <cfif counter is 0>
					     <tr bgcolor="ffffff" height=20>
					    <cfelse>
					     <tr bgcolor="e0e0e0" height=20>
					    </cfif>

					          <td class="text_xsmall" valign=top><a href="manage_award.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_item_portfolio_id#">#portfolio_item_name#</a></td>
					          <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
					          <td class="text_xsmall" valign=top align=center width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
					          <td class="text_xsmall" valign=top align=center width=75>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
					          <td class="text_xsmall" valign=top align=right width=75>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
					          </tr>

					          <cfset total = total + federal_action_obligation>

					          <cfif counter is 0>
					           <cfset counter = 1>
					          <cfelse>
					           <cfset counter = 0>
					          </cfif>

					     </cfoutput>

					     </div>

					     <cfoutput>

					      <tr><td colspan=5><hr></td></tr>

					      <tr><td class="text_xsmall" colspan=4><b>Total</b></td>
					          <td class="text_xsmall" align=right><b>#numberformat(total,'$999,999,999,999')#</b></td>
					      </tr>
					      </cfoutput>

					    </cfif>

					   </table>

					   </div>

			     </div>

                 </cfif>

			    </cfloop>

			    </cfif>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>