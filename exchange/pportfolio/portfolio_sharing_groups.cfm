<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing
 join sharing_group on sharing.sharing_group_id = sharing_group.sharing_group_id
 where sharing_portfolio_id = #session.portfolio_id# and
       sharing_hub_id = #session.hub#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">
        <cfoutput>
        SHARE PORTFOLIO - #ucase(port_info.portfolio_name)#
        </cfoutput>

        </td>
            <td align=right class="feed_sub_header"><a href="/exchange/portfolio/portfolio_company.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=5 class="feed_header">

          <img src="/images/icon_user.png" width=22>&nbsp;&nbsp;<a href="portfolio_sharing.cfm">Users</a>
          &nbsp;&nbsp;|&nbsp;&nbsp;
          <img src="/images/group.png" width=22>&nbsp;&nbsp;<a href="portfolio_sharing_groups.cfm"><u>My Groups</u></a>


          </td>

          <td align=right class="feed_sub_header"><a href="/exchange/groups/">Manage My Groups</a></td>

          </tr>

       </table>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("u")>
           <cfif u is 1>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">Group(s) have been successfully granted Share access.</td></tr>
           <cfelseif u is 2>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">Group Share access has been successfully removed.</td></tr>
           <cfelseif u is 3>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">Group rights have been successfully updated.</td></tr>
           </cfif>
          </cfif>

            <tr>
            <form action="share_group.cfm" method="post">

            <td class="feed_sub_header" style="font-weight: normal;">

            <cfif groups.recordcount is 0>
            	You have not shared this Portfolio with any Groups.
            <cfelse>
	            The following Groups have been granted Share access to your portfolio.
	        </cfif>

	        </td>

        <td align=right class="feed_sub_header" style="font-weight: normal;">

        <input type="submit" name="button" class="button_blue" value="Add Group">


          </td></form></tr>

         </table>

         <cfif groups.recordcount GT 0>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
             <td class="feed_sub_header">REMOVE</td>
             <td></td>
             <td class="feed_sub_header">GROUP NAME</td>
             <td class="feed_sub_header">DESCRIPTION</td>
             <td class="feed_sub_header">USERS</td>
             <td class="feed_sub_header" align=center>VIEW</td>
             <td class="feed_sub_header" align=center>UPDATE</td>
          </tr>

          <form action="share_remove_group.cfm" method="post">

          <cfloop query="groups">

			<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from sharing_group_usr
			 join usr on usr_id = sharing_group_usr_usr_id
			 where sharing_group_usr_group_id = #groups.sharing_group_id#
			 order by usr_full_name
			</cfquery>

			<cfif users.recordcount is 0>
			 <cfset group_users = "No users have been added to this Group">
			<cfelse>
			 <cfset group_users = valuelist(users.usr_full_name)>
			</cfif>

			<cfoutput>

             <tr>
                <td width=75>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="input_text" name="share_id" value=#encrypt(groups.sharing_group_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;"></td>
                <td width=40>&nbsp;&nbsp;</td>
                <td width=200 class="feed_sub_header">#ucase(groups.sharing_group_name)#</td>
                <td valign=top class="feed_sub_header" style="font-weight: normal;">#groups.sharing_group_desc#</td>
                <td valign=top class="feed_sub_header" style="font-weight: normal;">#group_users#</td>
			    <td align=center width=70><cfif groups.sharing_access is 1><img src="/images/icon_radio_checked.png" width=40><cfelse><a href="access_update_group.cfm?i=#encrypt(groups.sharing_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=1"><img src="/images/icon_radio_unchecked.png" width=40 border=0 alt="Select" title="Select"></a></cfif></td>
			    <td align=center width=70><cfif groups.sharing_access is 2><img src="/images/icon_radio_checked.png" width=40><cfelse><a href="access_update_group.cfm?i=#encrypt(groups.sharing_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=2"><img src="/images/icon_radio_unchecked.png" width=40 border=0 alt="Select" title="Select"></a></cfif></td>
			 </tr>

			 <tr><td colspan=7><hr></td></tr>

			</cfoutput>

			</cfloop>

            <tr><td height=10></td></tr>
            <tr><td class="link_small_gray" colspan=7><b>VIEW</b> - Users in Group can view all Companies and information recorded about Companies in the Portfolio.</td></tr>
            <tr><td class="link_small_gray" colspan=7><b>UPDATE</b> - Users in Group can add or remove Companies to the Portfolio.</td></tr>
            <tr><td height=20></td></tr>
            <tr><td colspan=5><input type="submit" name="button" class="button_blue_large" value="Remove Sharing" onclick="return confirm('Remove Sharing?\r\nAre you sure you want to remove sharing access from the selected Group(s)?');"></td></tr>


			</form>

			</table>

			</cfif>

          </td></tr>
          </table>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>