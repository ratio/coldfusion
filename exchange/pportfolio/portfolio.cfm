<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #portfolio_id# and
       portfolio_usr_id = #session.usr_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">Portfolios</td>
                 <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
             <tr><td height=15></td></tr>
          </table>

		  <cfquery name="opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio_item
		   left join award_data on portfolio_item_value_id = id
                  where portfolio_item_portfolio_id = #portfolio_id# and
                        portfolio_item_usr_id = #session.usr_id# and
                        portfolio_item_type_id = 1
		  </cfquery>

          <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <tr><td class="feed_sub_header">#info.portfolio_name#</td></tr>
				  <tr><td class="feed_option">#info.portfolio_desc#</td></tr>
				  <tr><td height=10></td></tr>
			  </table>

		  </cfoutput>

		  <cfif opps.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_option">No awards have been added to this portfolio.</td></tr>
          </table>

		  <cfelse>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>
               <td align=center>

				    <cfquery name="graph1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select awarding_agency_name, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #portfolio_id# and
								 portfolio_item_usr_id = #session.usr_id# and
								 portfolio_item_type_id = 1
				    group by awarding_agency_name
				    </cfquery>


					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph1">
						   ['#awarding_agency_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: 'By Department',
						chartArea: {left:2, top:40},
						pieHole: 0.4,
						height: 250,
						width: 250,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('dept_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="dept_graph"></div>

               </td>


               <td align=center>

				    <cfquery name="graph2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select awarding_sub_agency_name, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #portfolio_id# and
								 portfolio_item_usr_id = #session.usr_id# and
								 portfolio_item_type_id = 1
				    group by awarding_sub_agency_name
				    </cfquery>


					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Agency', 'Award Value'],
						  <cfoutput query="graph2">
						   ['#awarding_sub_agency_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: 'By Agency',
						chartArea: {left:2, top:40},
						pieHole: 0.4,
						height: 250,
						width: 250,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('agency_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="agency_graph"></div>

               </td>


               <td align=center>

				    <cfquery name="graph3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select naics_code, naics_description, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #portfolio_id# and
								 portfolio_item_usr_id = #session.usr_id# and
								 portfolio_item_type_id = 1
				    group by naics_code, naics_description
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph3">
						   ['#naics_code# - #naics_description#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: 'By NAICS',
						chartArea: {left:2, top:40},
						pieHole: 0.4,
						height: 250,
						width: 250,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="naics_graph"></div>

               </td>


               <td align=center>

				    <cfquery name="graph4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select product_or_service_code, product_or_service_code_description, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #portfolio_id# and
								 portfolio_item_usr_id = #session.usr_id# and
								 portfolio_item_type_id = 1
				    group by product_or_service_code, product_or_service_code_description
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph4">
						   ['#product_or_service_code# - #product_or_service_code_description#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: 'By PSC',
						chartArea: {left:2, top:40},
						pieHole: 0.4,
						height: 250,
						width: 250,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="psc_graph"></div>

               </td>

               <td align=center>

				    <cfquery name="graph5" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				    select type_of_contract_pricing, sum(federal_action_obligation) as total from portfolio_item
				    left join award_data on portfolio_item_value_id = id
					 	   where portfolio_item_portfolio_id = #portfolio_id# and
								 portfolio_item_usr_id = #session.usr_id# and
								 portfolio_item_type_id = 1
				    group by type_of_contract_pricing
				    </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="graph5">
						   ['#type_of_contract_pricing#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: 'By Pricing Type',
						chartArea: {left:2, top:40},
						pieHole: 0.4,
						height: 250,
						width: 250,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('pricing_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="pricing_graph"></div>

               </td>

            </tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr>
		     <td class="feed_option"><b>Award #</b></td>
		     <td class="feed_option"><b>Department</b></td>
		     <td class="feed_option"><b>Agency</b></td>
		     <td class="feed_option" align=center><b>NAICS</b></td>
		     <td class="feed_option" align=center><b>PoP Start</b></td>
		     <td class="feed_option" align=center><b>PoP End</b></td>
		     <td class="feed_option" align=right><b>Days Left</b></td>
		     <td class="feed_option" align=right><b>Award $</b></td>
		  </tr>

		  <cfset counter = 0>

			  <cfoutput query="opps">

                <cfif counter is 0>
                 <tr bgcolor="ffffff">
                <cfelse>
                 <tr bgcolor="e0e0e0">
                </cfif>

                    <td class="text_xsmall"><a href="manage.cfm?id=#id#">#award_id_piid#</a></td>
                    <td class="text_xsmall">#awarding_agency_name#</td>
                    <td class="text_xsmall">#awarding_sub_agency_name#</td>
                    <td class="text_xsmall" align=center>#naics_code#</td>
                    <td class="text_xsmall" align=center>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
                    <td class="text_xsmall" align=center>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
                    <td class="text_xsmall" align=right>#datediff("d",now(), period_of_performance_current_end_date)#</td>
                    <td class="text_xsmall" align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>

                </tr>

                <cfif counter is 0>
                 <cfset counter = 1>
                <cfelse>
                 <cfset counter = 0>
                </cfif>

			  </cfoutput>

		    </table>

		  </cfif>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>