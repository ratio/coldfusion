<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">Create Portfolio</td>
                 <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option" colspan=2>Portfolios allow you to select and group EXCHANGE awards, opportunities, grants, and opportunities.</td></tr>
             <tr><td height=10></td></tr>
          </table>

          <form action="db.cfm" method="post">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_option"><b>Portfolio Name</b></td></tr>
             <tr><td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  name="portfolio_name" size=60></td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option"><b>Description</b></td></tr>
             <tr><td class="feed_option"><textarea name="portfolio_desc" rows=5 cols=60></textarea></td></tr>

             <tr><td height=5></td></tr>
             <tr><td class="feed_option"><b>Portfolio Type</b></td></tr>
             <tr><td class="feed_option">
             <select name="portfolio_type_id">
              <option value=1>Companies
              <option value=2>Awards
             </td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option"><b>Company Portfolios</b> are a collection of companies that you are interested in assessing and analyzing for a specifc topic, capability or market.</td></tr>
             <tr><td class="feed_option"><b>Award Portfolios</b> are a collection of contract awards that are grouped based on topic, capability or market.</td></tr>
             <tr><td height=10></td></tr>
             <tr><td class="feed_option"><b>Sharing</b></td></tr>
             <tr><td class="feed_option">
             <select name="portfolio_access_id">
              <option value=1>Private (me only)
              <option value=2>Me, and Company Users (view)
             </td></tr>
             <tr><td height=10></td></tr>
             <tr><td><input type="submit" name="button" class="button_blue" style="font-size: 11px; height: 22px; width: 120px;" value="Create Portfolio">
          </table>

          </form>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>