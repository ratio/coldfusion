<cfquery name="get" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select person_portfolio_id from person_portfolio
 where person_portfolio_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfset session.person_portfolio_id = #get.person_portfolio_id#>

<!--- Update Recent --->

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_person_portfolio_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#get.person_portfolio_id#,#session.usr_id#,#session.company_id#,#session.hub#,#now()#)
</cfquery>

<cflocation URL="open.cfm" addtoken="no">

