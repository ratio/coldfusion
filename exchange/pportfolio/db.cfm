<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Create Portfolio">

	<cfquery name="insert_port" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into portfolio
	 (portfolio_type_id, portfolio_default, portfolio_desc, portfolio_access_id, portfolio_name, portfolio_usr_id, portfolio_company_id, portfolio_hub_id, portfolio_updated)
	 values
	 (#portfolio_type_id#, 1,'#portfolio_desc#', #portfolio_access_id#, '#portfolio_name#',#session.usr_id#,#session.company_id#,<cfif not isdefined("session.hub")>null<cfelse>#session.hub#</cfif>,#now()#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update_port" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio
	 set portfolio_name = '#portfolio_name#',
	     portfolio_desc = '#portfolio_desc#',
	     portfolio_access_id = #portfolio_access_id#
	 where portfolio_id = #portfolio_id# and
	       portfolio_usr_id = #session.usr_id#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete_port_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete portfolio
	 where portfolio_id = #portfolio_id# and
	       portfolio_usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="delete_port_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete portfolio_item
	 where portfolio_item_portfolio_id = #portfolio_id# and
	       portfolio_item_usr_id = #session.usr_id#
	</cfquery>

</cftransaction>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>