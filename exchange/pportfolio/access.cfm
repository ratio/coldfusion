<cfset portfolio_access = 0>
<cfset portfolio_access_level = 0>

<!--- Check Group Access --->

<cfquery name="group_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_access from sharing
 join sharing_group on sharing_group.sharing_group_id = sharing.sharing_group_id
 join sharing_group_usr on sharing_group_usr.sharing_group_usr_group_id = sharing.sharing_group_id
 where sharing_hub_id = #session.hub# and
       sharing_portfolio_id = #session.portfolio_id# and
       sharing_group_usr_usr_id = #session.usr_id# and
       sharing.sharing_group_id is not null
</cfquery>

<cfif group_access.recordcount is 1>
 <cfset portfolio_access = 1>
 <cfset portfolio_access_level = #group_access.sharing_access#>
</cfif>

<!--- Check User Access --->

<cfquery name="user_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_access from sharing
 where  sharing_portfolio_id = #session.portfolio_id# and
        sharing_hub_id = #session.hub# and
        sharing_to_usr_id = #session.usr_id#
</cfquery>

<cfif user_access.recordcount is 1>
 <cfset portfolio_access = 1>
 <cfset portfolio_access_level = #user_access.sharing_access#>
</cfif>

<cfif portfolio_access_level is 1>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>