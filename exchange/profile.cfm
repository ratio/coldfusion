<!--- Personal Profile --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr.usr_id = #session.usr_id#
</cfquery>

<div class="left_box">

  <center>
  <table cellspacing=0 cellpadding=0 border=0 width=100%>

   <cfoutput>

	 <cfif #info.usr_photo# is "">
	  <tr><td align=center><a href="/exchange/profile/edit.cfm"><img src="#image_virtual#headshot.png" width=100 border=0 title="Update Profile" alt="Update Profile"></a></td></tr>
	 <cfelse>
	  <tr><td align=center><a href="/exchange/profile/edit.cfm"><img style="border-radius: 0px;" src="#media_virtual#/#info.usr_photo#" width=150 border=0 title="Update Profile" alt="Update Profile"></a></td></tr>
	 </cfif>

  <tr><td height=10></td></tr>

  <tr><td class="feed_header" align=center style="padding-bottom: 0px; margin-bottom: 0px;"><a href="/exchange/profile/">#info.usr_first_name# #info.usr_last_name#</a></td></tr>

  <cfif #info.usr_company_name# is not "">
  	<tr><td class="feed_option" align=center><a href="/exchange/company/"><b>#ucase(info.usr_company_name)#</b></a></td></tr>
  </cfif>

  <cfif #info.usr_title# is not "">
  	<tr><td class="feed_sub_header" align=center style="padding-bottom: 0px; padding-top: 0px; font-weight: normal; font-size: 16px;">#info.usr_title#</td></tr>
  </cfif>

  </cfoutput>

  </td></tr>

 </table>
  </center>

    <cfquery name="hub_users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     select hub_xref_usr_id from hub_xref
     where hub_xref_hub_id = #session.hub# and
           hub_xref_active = 1 and
           hub_xref_usr_id <> #session.usr_id#
    </cfquery>

    <cfif hub_users.recordcount is 0>
     <cfset hub_user_list = 0>
    <cfelse>
     <cfset hub_user_list = valuelist(hub_users.hub_xref_usr_id)>
    </cfif>

    <cfquery name="user_views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select count(usr_view_id) as total from usr_view
	 where usr_view_usr_id = #session.usr_id# and
	       usr_view_by_usr_id in (#hub_user_list#) and
	       usr_view_hub_id = #session.hub#
    </cfquery>

    <cfquery name="mess" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select count(message_id) as total from message
	 where message_view_id = #session.usr_id# and
	       message_hub_id = #session.hub# and
	       message_read = 0
    </cfquery>

    <cfquery name="partners" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     select count(follow_id) as total from follow
     where follow_by_usr_id = #session.usr_id# and
           follow_hub_id = #session.hub#
    </cfquery>

	<cfquery name="people" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select count(usr_follow_id) as total from usr_follow
	  where usr_follow_usr_id = #session.usr_id# and
	        usr_follow_usr_follow_id in (#hub_user_list#) and
	        usr_follow_hub_id = #session.hub#
	</cfquery>

	<cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select count(usr_follow_id) as total from usr_follow
	  where usr_follow_usr_follow_id = #session.usr_id# and
	        usr_follow_usr_id in (#hub_user_list#) and
	        usr_follow_hub_id = #session.hub#
	</cfquery>

	  <center>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <cfoutput>

	  <tr><td><hr></td></tr>
	  <tr><td height=10></td></tr>

	  <tr><td align=center>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr>
          <td align=center><a href="/exchange/messages/"><img src="#image_virtual#icon_message.png" width=20 alt="New Message" title="New Messages" border=0></a></td>
          <td align=center><a href="/exchange/marketplace/people/set.cfm?v=1"><img src="#image_virtual#icon_people.png" width=20 alt="My Connections" title="My Connections" border=0></a></td>
          <td align=center><a href="/exchange/marketplace/people/set.cfm?v=2"><img src="#image_virtual#icon_follower.png" width=20 alt="Followers" title="Followers"></a></td>
          <td align=center><a href="/exchange/profile_views.cfm"><img src="#image_virtual#icon_view.png" width=20 alt="Who's Viewed Me" title="Who's Viewed Me" border=0></a></td>
          <td align=center><a href="/exchange/marketplace/companies/"><img src="#image_virtual#icon_company4.png" width=20 alt="Companies I Follow" title="Companies I Follow"></a></td>
       </tr>

       <tr>
          <td class="feed_sub_header" style="padding-bottom: 0px;" align=center><a href="/exchange/messages/" style="padding-bottom: 0px; font-weight: normal;">#mess.total#</a></td>
          <td class="feed_sub_header" style="padding-bottom: 0px;" align=center><a href="/exchange/marketplace/people/set.cfm?v=1" style="padding-bottom: 0px; font-weight: normal;">#people.total#</a></td>
          <td class="feed_sub_header" style="padding-bottom: 0px;" align=center><a href="/exchange/marketplace/people/set.cfm?v=2" style="padding-bottom: 0px; font-weight: normal;">#followers.total#</a></td>
          <td class="feed_sub_header" style="padding-bottom: 0px;" align=center><a href="/exchange/profile_views.cfm" style="padding-bottom: 0px; font-weight: normal;">#user_views.total#</a></td>
          <td class="feed_sub_header" style="padding-bottom: 0px;" align=center><a href="/exchange/marketplace/companies/" style="padding-bottom: 0px; font-weight: normal;">#partners.total#</a></td>
       </tr>

       </table>

	  </td></tr>

	   </cfoutput>

	  </table>
	  </center>
</div>