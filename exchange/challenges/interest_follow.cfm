<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="challenge_followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from follow
 join usr on usr_id = follow_by_usr_id
 where follow_challenge_id = #session.challenge_id#
 order by follow_date
</cfquery>

<style>
.small_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">
         <cfinclude template="challenge_header.cfm">
       </div>


       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Expessed Interest in Challenge</td>
            <td valign=top align=right></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

  	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

  	   <tr><td height=10></td></tr>

  	     <tr><td valign=top>

  	      <cfif challenge_followers.recordcount is 0>

  	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
  	        <tr><td class="feed_sub_header" style="font-weight: normal;">No users have expressed interest in this Challenge.</td></tr>
  	        <tr><td height=10></td></tr>
  	      </table>

  	      <cfelse>

			  <cfoutput query="challenge_followers">

			   <cfif challenge_followers.usr_profile_display is 2>

				<div class="smallbadge">

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <tr><td height=30></td></tr>
				  <tr><td align=center><img src="/images/headshot.png" height=120></td></tr>
				  <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
				 </table>

				</div>

			   <cfelse>

				<div class="small_badge">

					<table cellspacing=0 cellpadding=0 border=0 width=100%>

                     <tr><td valign=top width=115>

					<table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif #challenge_followers.usr_photo# is "">
					  <tr><td class="text_xsmall" width=85><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(challenge_followers.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=115 width=115 border=0 vspace=10></td></tr>
					 <cfelse>
					  <tr><td class="text_xsmall" width=85><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(challenge_followers.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" src="#media_virtual#/#challenge_followers.usr_photo#" height=115 width=115 vspace=10 border=0></td></tr>
					 </cfif>

                     <tr><td align=center>

                     <cfif #challenge_followers.usr_linkedin# is not "">
                      <a href="#challenge_followers.usr_linkedin#"><img src="/images/icon_linkedin.png" width=20></a>
                     </cfif>

                     <cfif #challenge_followers.usr_facebook# is not "">
                      <a href="#challenge_followers.usr_facebook#"><img src="/images/icon_facebook.png" width=20></a>
                     </cfif>

                     <cfif #challenge_followers.usr_twitter# is not "">
                      <a href="#challenge_followers.usr_twitter#"><img src="/images/icon_twitter.png" width=20></a>
                     </cfif>

                     </td></tr>

                     </table>

					 </td><td width=30>&nbsp;</td><td valign=top class="feed_sub_header">

					<table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 5px;"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(challenge_followers.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#challenge_followers.usr_first_name# #challenge_followers.usr_last_name#</a></td></tr>
					 <tr><td class="feed_option"><b>#challenge_followers.usr_title#</b></td></tr>
					 <tr><td height=10></td></tr>
				     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=20 valign=middle>&nbsp;&nbsp;#tostring(tobinary(challenge_followers.usr_email))#</td></tr>
				     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-phone_2.png" alt="Work Phone" title="Work Phone" width=20 valign=middle>&nbsp;&nbsp;<cfif #challenge_followers.usr_phone# is "">Not Provided<cfelse>#challenge_followers.usr_phone#</cfif></td></tr>
				     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon_cell_phone.png" alt="Cell Phone" title="Cell Phone" width=20 valign=middle>&nbsp;&nbsp;<cfif #challenge_followers.usr_cell_phone# is "">Not Provided<cfelse>#challenge_followers.usr_cell_phone#</cfif></td></tr>
					</table>

					</td></tr>

					</table>

				</div>

			 </cfif>

			 </cfoutput>

		 </cfif>

  	     </td></tr>

  	   </table>

	   </div>






















	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>