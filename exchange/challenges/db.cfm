<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Create Challenge">

	<cfif #challenge_image# is not "">
		<cffile action = "upload"
		 fileField = "challenge_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cftransaction>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into challenge
	 (
	  challenge_name,
	  challenge_process_id,
	  challenge_total_cash,
	  challenge_code,
	  challenge_need_id,
	  challenge_challenges,
	  challenge_use_case,
	  challenge_future_state,
	  challenge_url,
	  challenge_poc_name,
	  challenge_poc_title,
	  challenge_poc_phone,
	  challenge_poc_email,
	  challenge_keywords,
	  challenge_organization,
	  challenge_open,
	  challenge_public,
	  challenge_annoymous,
	  challenge_image,
	  challenge_desc,
	  challenge_start,
	  challenge_end,
	  challenge_created,
	  challenge_updated,
	  challenge_created_by_id,
	  challenge_hub_id,
	  challenge_company_id,
	  challenge_status
	 )
	 values (
	 '#challenge_name#',
	  #challenge_process_id#,
	 <cfif #challenge_total_cash# is "">null<cfelse>#challenge_total_cash#</cfif>,
	 'EC-#randrange(100000000,999999999)#',
	 <cfif isdefined("challenge_need_id")>#challenge_need_id#<cfelse>null</cfif>,
	 '#challenge_challenges#',
	 '#challenge_use_case#',
	 '#challenge_future_state#',
	 '#challenge_url#',
	 '#challenge_poc_name#',
	 '#challenge_poc_title#',
	 '#challenge_poc_phone#',
	 '#challenge_poc_email#',
	 '#challenge_keywords#',
	 '#challenge_organization#',
	  #challenge_open#,
	  #challenge_public#,
	  #challenge_annoymous#,
      <cfif #challenge_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	 '#challenge_desc#',
	 <cfif #challenge_start# is "">null<cfelse>'#challenge_start#'</cfif>,
	 <cfif #challenge_end# is "">null<cfelse>'#challenge_end#'</cfif>,
	  #now()#,
	  #now()#,
	  #session.usr_id#,
	  #session.hub#,
	  #session.company_id#,
	  #challenge_status#
	  )
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(challenge_id) as id from challenge
	</cfquery>

	</cftransaction>

	<cfset session.challenge_id = #max.id#>

	<cflocation URL="open.cfm?u=100" addtoken="no">

<cfelseif button is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_image from challenge
		  where challenge_id = #session.challenge_id#
		</cfquery>

         <cfif fileexists("#media_path#\#remove.challenge_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.challenge_image#">
         </cfif>

	</cfif>

	<cfif #challenge_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_image from challenge
		  where challenge_id = #session.challenge_id#
		</cfquery>

		<cfif #getfile.challenge_image# is not "">
         <cfif fileexists("#media_path#\#getfile.challenge_image#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.challenge_image#">
         </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "challenge_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update challenge
	 set challenge_name = '#challenge_name#',
	     challenge_organization = '#challenge_organization#',

		  <cfif #challenge_image# is not "">
		   challenge_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   challenge_image = null,
		  </cfif>

	     challenge_desc = '#challenge_desc#',
	     challenge_total_cash = <cfif #challenge_total_cash# is "">null<cfelse>#challenge_total_cash#</cfif>,
	     challenge_annoymous = #challenge_annoymous#,
	     challenge_poc_name = '#challenge_poc_name#',
	     challenge_process_id = #challenge_process_id#,
	     challenge_poc_title = '#challenge_poc_title#',
	     challenge_poc_phone = '#challenge_poc_phone#',
	     challenge_challenges = '#challenge_challenges#',
	     challenge_poc_email = '#challenge_poc_email#',
	     challenge_keywords = '#challenge_keywords#',
	     challenge_use_case = '#challenge_use_case#',
	     challenge_future_state = '#challenge_future_state#',
	     challenge_open = #challenge_open#,
	     challenge_public = #challenge_public#,
	     challenge_instructions = '#challenge_instructions#',
	     challenge_reward = '#challenge_reward#',
	     challenge_start = <cfif #challenge_start# is "">null<cfelse>'#challenge_start#'</cfif>,
	     challenge_end = <cfif challenge_end is "">null<cfelse>'#challenge_end#'</cfif>,
	     challenge_updated = #now()#,
	     challenge_status = #challenge_status#,
	     challenge_url = '#challenge_url#'
	 where challenge_id = #session.challenge_id#
	</cfquery>

    <cflocation URL="open.cfm?u=2" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select challenge_image from challenge
	  where (challenge_id = #session.challenge_id#)
	</cfquery>

	<cfif remove.challenge_image is not "">
         <cfif fileexists("#media_path#\#remove.challenge_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.challenge_image#">
         </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete challenge
	 where challenge_id = #session.challenge_id#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>
