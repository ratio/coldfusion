<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Update">

	<cfif #up_attachment# is not "">
		<cffile action = "upload"
		 fileField = "up_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cftransaction>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 insert into up
	 (
	  up_name,
	  up_desc,
	  up_date,
	  up_usr_id,
	  up_attachment,
	  up_url,
	  up_challenge_id,
	  up_post
	  )
	 values (
	 '#up_name#',
	 '#up_desc#',
	  #now()#,
	  #session.usr_id#,
      <cfif #up_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	 '#up_url#',
	  #session.challenge_id#,
	  #up_post#
	  )
	</cfquery>

	<cflocation URL="open.cfm?u=41" addtoken="no">

<cfelseif button is "Save Changes">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select up_attachment from up
		  where up_id = #update_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.up_attachment#")>
			<cffile action = "delete" file = "#media_path#\#remove.up_attachment#">
		</cfif>

	</cfif>

	<cfif #up_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select up_attachment from up
		  where up_id = #update_id#
		</cfquery>

		<cfif #getfile.up_attachment# is not "">
	        <cfif fileexists("#media_path#\#getfile.up_attachment#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.up_attachment#">
			</cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "up_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 update up
	 set up_name = '#up_name#',
	     up_desc = '#up_desc#',
	     up_date = #now()#,

		  <cfif #up_attachment# is not "">
		   up_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   up_attachment = null,
		  </cfif>

	     up_url = '#up_url#',
	     up_post = #up_post#
	 where up_id = #update_id# and
	       up_challenge_id = #session.challenge_id#
	</cfquery>

    <cflocation URL="open.cfm?u=42" addtoken="no">

<cfelseif button is "Delete Update">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select up_attachment from up
	  where up_id = #update_id#
	</cfquery>

	<cfif remove.up_attachment is not "">
        <cfif fileexists("#media_path#\#remove.up_attachment#")>
		 <cffile action = "delete" file = "#media_path#\#remove.up_attachment#">
		</cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 delete up
	 where up_id = #update_id# and
	  up_challenge_id = #session.challenge_id#
	</cfquery>

    <cflocation URL="open.cfm?u=43" addtoken="no">

</cfif>
