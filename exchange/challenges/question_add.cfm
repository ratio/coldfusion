<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">
        <cfinclude template="challenge_header.cfm">
       </div>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Add Question</td>
            <td align=right class="feed_sub_header"><a href="/exchange/challenges/interest_questions.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

       <form action="question_db.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfoutput>

         <tr><td class="feed_sub_header">Subject</td></tr>
         <tr><td><input type="text" name="question_subject" class="input_text" style="width: 600px;"></td></tr>

         <tr><td class="feed_sub_header">Question</td></tr>
         <tr><td><textarea name="question_text" class="input_textarea" style="width: 1100px; height: 150px;"></textarea></td></tr>

         <tr><td class="feed_sub_header">Answer</td></tr>
         <tr><td><textarea name="question_answer" class="input_textarea" style="width: 1100px; height: 150px;"></textarea></td></tr>

         <tr><td class="feed_sub_header">Display Publicly?</td></tr>

         <tr><td>
             <select name="question_public" class="input_select">
              <option value=0>No, keep private
              <option value=1>Yes, display publicly
             </select>

             </td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>

         <tr><td><input type="submit" name="button" value="Add Question" class="button_blue_large">&nbsp;&nbsp;


         </td></tr>

        </cfoutput>


  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>