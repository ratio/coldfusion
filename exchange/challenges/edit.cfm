<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 220px;
  font-size: 12;
  background-color: black;
  color: #fff;
  text-align: left;
  border-radius: 6px;
  padding-left: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 10px;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
  top: -5px;
  left: 105%;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

<cfquery name="challenge_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="process" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_process
 where challenge_process_hub_id = #session.hub#
 order by challenge_process_order
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>

		<td valign=top width=185 valign=top>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td>

      <td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Edit Challenge</td>
            <td valign=top align=right class="feed_sub_header"><a href="open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header">Challenge Name</td>
				 <td><input class="input_text" type="text" name="challenge_name" style="width: 600px;" required maxlength="500" value="#challenge_info.challenge_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=200>Challenge Organization</td>
				 <td><input class="input_text" type="text" name="challenge_organization" value="#challenge_info.challenge_organization#" style="width: 600px;" maxlength="500" placeholder="Name of the organization sponsoring this challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="challenge_desc" style="width: 900px; height: 75px;" placeholder="Please provide a short description of what this Challenge is intented to solve.">#challenge_info.challenge_desc#</textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Today's<br>Challenges</td>
				 <td><textarea class="input_textarea" name="challenge_challenges" style="width: 900px; height: 75px;" placeholder="Please describes the challenges associated with today's solution or process.">#challenge_info.challenge_challenges#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
				 <td><textarea class="input_textarea" name="challenge_use_case" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how the new solution will be used in the future.">#challenge_info.challenge_use_case#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Future State<br>Solution</td>
				 <td><textarea class="input_textarea" name="challenge_future_state" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state solution and any constraints.">#challenge_info.challenge_future_state#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="challenge_keywords" maxlength="500" style="width: 900px;" value="#challenge_info.challenge_keywords#" placeholder="Enter up to 5 keywords (seperated by commas) that are related to this Challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="challenge_url" maxlength="1000" style="width: 900px;" value="#challenge_info.challenge_url#" placeholder="Please provide the link the external URL / announcement."></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Challenge Reward</td>
				 <td><textarea class="input_textarea" name="challenge_reward" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the what Companies will receive by winning this Challenge.">#challenge_info.challenge_reward#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Participation<br>Instructions</td>
				 <td><textarea class="input_textarea" name="challenge_instructions" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide and / all instructions for Companies who participate in this Challenge.">#challenge_info.challenge_instructions#</textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" style="width: 165px;" value="#dateformat(challenge_info.challenge_start,'yyyy-mm-dd')#"></td></tr>

		 <tr><td class="feed_sub_header">End Date</td>
		     <td><input type="date" name="challenge_end" class="input_date" style="width: 165px;" value="#dateformat(challenge_info.challenge_end,'yyyy-mm-dd')#"></td></tr>

		 <tr><td class="feed_sub_header">Anonymous</td>
		     <td>
		     <select name="challenge_annoymous" class="input_select" style="width: 165px;">
		      <option value=0 <cfif challenge_info.challenge_annoymous is 0>selected</cfif>>No
		      <option value=1 <cfif challenge_info.challenge_annoymous is 1>selected</cfif>>Yes
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will hide the organization name when vieweing the Challenge.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Sharing</td>
		     <td>
		     <select name="challenge_public" class="input_select">
		      <option value=0 <cfif challenge_info.challenge_public is 0>selected</cfif>>No, keep private
		      <option value=1 <cfif challenge_info.challenge_public is 1>selected</cfif>>Yes, within this Exchange / Network
		      <option value=2 <cfif challenge_info.challenge_public is 2>selected</cfif>>Yes, on any Exchange / Network
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Determines who will see this Challenge.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Allow Questions</td>
		     <td>
		     <select name="challenge_open" class="input_select">
		      <option value=0 <cfif challenge_info.challenge_open is 0>selected</cfif>>No
		      <option value=1 <cfif challenge_info.challenge_open is 1>selected</cfif>>Yes
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" allows users to ask questions and accept the Challenge.</span>
			 </div>

		     </td></tr>


                <tr><td class="feed_sub_header" valign=top>Challenge Image</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #challenge_info.challenge_image# is "">
					  <input type="file" name="challenge_image">
					<cfelse>
					  <img src="#media_virtual#/#challenge_info.challenge_image#" width=150><br><br>
					  <input type="file" name="challenge_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove Image
					 </cfif>

					 </td></tr>

			  <tr>
				 <td class="feed_sub_header">Total Prizes</td>
				 <td><input type="number" class="input_text" name="challenge_total_cash" value=#challenge_info.challenge_total_cash# style="width: 150px;"></td>
		      </tr>

		 <tr><td class="feed_sub_header">Status</td>
		     <td>
		      <select name="challenge_status" class="input_select" style="width: 165px;">
		       <option value=3 <cfif challenge_info.challenge_status is 3>selected</cfif>>Pending
		       <option value=1 <cfif challenge_info.challenge_status is 1>selected</cfif>>Open
		       <option value=2 <cfif challenge_info.challenge_status is 2>selected</cfif>>Closed
		      </select>

		     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			   <span class="tooltiptext">Pending - Not Ready<br>Open - Active and Available<br>Closed - No Longer Active.</span>
			 </div>

		      </td></tr>

		 <tr><td class="feed_sub_header">Process</td>
		     <td class="feed_sub_header" style="font-weight: normal;">

             </cfoutput>

		     <select name="challenge_process_id" class="input_select">
		     <option value=0 <cfif #challenge_info.challenge_process_id# is 0>selected</cfif>>Select Process
		     <cfoutput query="process">
		      <option value=#challenge_process_id# <cfif #challenge_info.challenge_process_id# is #process.challenge_process_id#>selected</cfif>>#challenge_process_name#
             </cfoutput>
            </select>

            <cfoutput>

		     </td></tr>

              <tr><td colspan=4><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header">Contact Name</td>
				 <td><input class="input_text" type="text" name="challenge_poc_name" maxlength="200" style="width: 300px;" value="#challenge_info.challenge_poc_name#" required></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Title</td>
				 <td><input class="input_text" type="text" name="challenge_poc_title" maxlength="200" style="width: 300px;" value="#challenge_info.challenge_poc_title#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Email</td>
				 <td><input class="input_text" type="text" name="challenge_poc_email" maxlength="200" style="width: 300px;" value="#challenge_info.challenge_poc_email#" required></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Phone</td>
				 <td><input class="input_text" type="text" name="challenge_poc_phone" maxlength="100" style="width: 300px;" value="#challenge_info.challenge_poc_phone#"></td>
		      </tr>

		      </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
                         <input type="submit" name="button" class="button_blue_large" value="Update">&nbsp;&nbsp;
			             <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Challenge?\r\nAre you sure you want to delete this Challenge?');">

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>