<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into challenge_sourcing (challenge_sourcing_name, challenge_sourcing_hub_id, challenge_sourcing_order)
	 values ('#challenge_sourcing_name#',#session.hub#,#challenge_sourcing_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update challenge_sourcing
	 set challenge_sourcing_name = '#challenge_sourcing_name#',
	     challenge_sourcing_order = #challenge_sourcing_order#
	 where challenge_sourcing_id = #challenge_sourcing_id# and
	       challenge_sourcing_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete challenge_sourcing
	 where challenge_sourcing_id = #challenge_sourcing_id# and
	       challenge_sourcing_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

