<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #challenge_process_image# is not "">
		<cffile action = "upload"
		 fileField = "challenge_process_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into challenge_process (challenge_process_name, challenge_process_hub_id, challenge_process_order, challenge_process_image)
	 values ('#challenge_process_name#',#session.hub#,#challenge_process_order#,<cfif #challenge_process_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_process_image from challenge_process
		  where challenge_process_id = #challenge_process_id# and
		        challenge_process_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.challenge_process_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.challenge_process_image#">
		</cfif>

	</cfif>

	<cfif challenge_process_image is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_process_image from challenge_process
		  where challenge_process_id = #challenge_process_id# and
		        challenge_process_hub_id = #session.hub#
		</cfquery>

		<cfif #getfile.challenge_process_image# is not "">
         <cfif fileexists("#media_path#\#getfile.challenge_process_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.challenge_process_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "challenge_process_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update challenge_process
	 set challenge_process_name = '#challenge_process_name#',

		  <cfif #challenge_process_image# is not "">
		   challenge_process_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   challenge_process_image = null,
		  </cfif>

	     challenge_process_order = #challenge_process_order#

	 where challenge_process_id = #challenge_process_id# and
	       challenge_process_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_process_image from challenge_process
		  where challenge_process_id = #challenge_process_id# and
		        challenge_process_hub_id = #session.hub#
		</cfquery>

		<cfif remove.challenge_process_image is not "">
         <cfif fileexists("#media_path#\#remove.challenge_process_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.challenge_process_image#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete challenge_process
	 where challenge_process_id = #challenge_process_id# and
	       challenge_process_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

