<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 update challenge_metric
 set challenge_metric_pocs_accessed = <cfif #challenge_metric_pocs_accessed# is "">null<cfelse>#challenge_metric_pocs_accessed#</cfif>,
     challenge_metric_transition = #challenge_metric_transition#,
     challenge_metric_consignments = <cfif #challenge_metric_pocs_consignments# is "">null<cfelse>#challenge_metric_pocs_consignments#</cfif>,
     challenge_metric_cross_werx = #challenge_metric_cross_werx#,
     challenge_metric_learnings = <cfif #challenge_metric_learnings# is "">null<cfelse>#challenge_metric_learnings#</cfif>,
     challenge_metric_companies_sourced = <cfif #challenge_metric_companies_sourced# is "">null<cfelse>#challenge_metric_companies_sourced#</cfif>,
     challenge_metric_speed_to_solve = <cfif #challenge_metric_speed_to_solve# is "">null<cfelse>#challenge_metric_speed_to_solve#</cfif>
 where challenge_metric_hub_id = #session.hub# and
	   challenge_metric_challenge_id = #session.challenge_id#
</cfquery>

<cflocation URL="open.cfm?u=172" addtoken="no">
