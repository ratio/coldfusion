<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_learn
 where challenge_learn_id = #challenge_learn_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Learning</td>
            <td align=right><a href="/exchange/challenges/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

            <form action="learn_db.cfm" method="post" enctype="multipart/form-data" >

            <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header">Title</td>
				 <td><input class="input_text" type="text" name="challenge_learn_name" value="#edit.challenge_learn_name#" style="width: 800px;" required maxlength="300" placeholder="Please provide the title of this learning."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Learning</td>
				 <td><textarea class="input_textarea" name="challenge_learn_desc" style="width: 800px; height: 200px;" placeholder="Please add the full details of the learning.">#edit.challenge_learn_desc#</textarea></td>
		      </tr>

		      <input type="hidden" name="challenge_learn_id" value=#challenge_learn_id#>

	          <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=2>
                         <input type="submit" name="button" class="button_blue_large" value="Update">&nbsp;&nbsp;
			             <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Learning?\r\nAre you sure you want to delete this learning?');">

		      </td></tr>

             </table>

             </cfoutput>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>