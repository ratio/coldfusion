<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from attachment
 where attachment_id = #attachment_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>EDIT ATTACHMENT</td>
            <td valign=top align=right><a href="/exchange/challenges/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

            <form action="attachment_db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header">Attachment Name</td>
				 <td><input class="input_text" type="text" name="attachment_name" value="#info.attachment_name#" style="width: 600px;" required maxlength="200"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="attachment_desc" value="#info.attachment_desc#" style="width: 900px; height: 75px;" placeholder="Please provide a short description of this attachment."></textarea></td>
		      </tr>

                <tr><td class="feed_sub_header" valign=top>File</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #info.attachment_file# is "">
					  <input type="file" name="attachment_file">
					<cfelse>
					  <a href="#media_virtual#/#info.attachment_file#" target="_blank" rel="noopener" rel="noreferrer">#info.attachment_file#</a><br><br>
					  <input type="file" name="attachment_file"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove Attachment
					 </cfif>

					 </td></tr>

              <tr><td colspan=2><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
                         <input type="submit" name="button" class="button_blue_large" value="Save">&nbsp;&nbsp;
			             <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Attachment?\r\nAre you sure you want to delete this Attachment?');">

		      </td></tr>

             </table>

             <input type="hidden" name="attachment_id" value=#attachment_id#>

             </form>

         </cfoutput>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>