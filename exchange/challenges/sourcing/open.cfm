<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="company_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top width=100%>

       <div class="main_box">
	       <cfinclude template="/exchange/challenges/challenge_header.cfm">
       </div>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>Company Information</td>
             <td align=right class="feed_sub_header" valign=absmiddle>

                  <cfoutput>

	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/resource_home.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Full Company Profile</a>

	              &nbsp;|&nbsp;
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_dashboard.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Award Dashboard</a>

	              &nbsp;|&nbsp;

	             <a href="index.cfm">Return</a>

	             </cfoutput>

	             </td></tr>

		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
	    <cfif u is 1>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully added.</td></tr>
	    <cfelseif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully updated.</td></tr>
	    <cfelseif u is 3>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully deleted.</td></tr>
	    </cfif>
	   </cfif>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=150>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option">

                    <a href="open.cfm?id=#company_info.company_id#">
                    <cfif company_info.company_logo is "">
					  <img src="//logo.clearbit.com/#company_info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_info.company_logo#" width=100 border=0>
					</cfif>
					</a>

					</td>
			    </tr>

                <!--- Social Icons --->

                <tr><td height=10></td></tr>

                <tr><td>

                <cfif company_info.company_twitter_url is not "">
                 <a href="#company_info.company_twitter_url#" target="blank"><img src="/images/icon_twitter.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif company_info.company_linkedin_url is not "">
                 <a href="#company_info.company_linkedin_url#" target="blank"><img src="/images/icon_linkedin.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif company_info.company_facebook_url is not "">
                 <a href="#company_info.company_facebook_url#" target="blank"><img src="/images/icon_facebook.png" width=20 border=0 hspace=5></a>
                </cfif>

                </td></tr>

			    <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header">

                </td></tr>

			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(company_info.company_name)#</td>
	              <td class="feed_sub_header" align=right width=600>

	              </td></tr>

                 <tr><td class="feed_sub_header" colspan=2 style="padding-bottom: 0px;"><b>Overview</b></td></tr>
                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #company_info.company_about# is "">
					No overview found.
					<cfelse>
					#company_info.company_about#
					</cfif>

					</td>

			    </tr>

			    <cfif company_info.company_long_desc is not "">

                 <tr><td class="feed_sub_header" style="padding-bottom: 0px;" colspan=2><b>Description</b></td></tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" style="font-weight: normal;" colspan=2>
					<cfif #company_info.company_long_desc# is "">
					No full description found.
					<cfelse>
					#company_info.company_long_desc#
					</cfif>

					</td>
			    </tr>

			    </cfif>


                 <tr><td class="feed_sub_header" style="padding-bottom: 0px;"><b>Keywords / Tags</b></td>
                     <td class="feed_sub_header" style="padding-bottom: 0px;" align=right><b>Company Website</b></td></tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal; width: 900px;">
					<cfif #company_info.company_keywords# is "">
					No keywords found.
					<cfelse>
					#company_info.company_keywords#
					</cfif>

					</td>

					<td class="feed_sub_header" style="font-weight: normal;" align=right>


                    <cfif company_info.company_website is "">
                     Not listed
                    <cfelse>
					<u>
					<a href="#company_info.company_website#" style="font-weight: normal;"  target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(company_info.company_website) GT 40>
					#ucase(left(company_info.company_website,40))#...
					<cfelse>
					#ucase(company_info.company_website)#
					</cfif>
					</u>
					</a>
					</cfif>

					</td>
			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

       <!--- POC company_info.mation --->

	   <cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
		 select * from sams
		 where duns = '#company_info.company_duns#'
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=10></td></tr>
		   <tr><td class="feed_header">Points of Contact</td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
            <td class="feed_sub_header">Name</td>
            <td class="feed_sub_header">Title / Role</td>
            <td class="feed_sub_header">Email</td>
            <td class="feed_sub_header" align=right>Phone</td>
        </tr>

        <cfoutput>

        <cfset poc = 0>

        <cfif company_info.company_poc_phone is not "" and company_info.company_poc_email is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif company_info.company_poc_first_name is "" and company_info.company_poc_last_name is "">Unknown<cfelse>#company_info.company_poc_first_name# #company_info.company_poc_last_name#</cfif></td>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif #company_info.company_poc_title# is "">Unknown<cfelse>#company_info.company_poc_title#</cfif></td>
			   <td class="feed_sub_header" style="font-weight: normal;">#company_info.company_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#company_info.company_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.poc_fnme is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_fnme# #sams.poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_us_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.pp_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_fname# #sams.pp_poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.pp_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.alt_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_fname# #sams.alt_poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.akt_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.alt_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.elec_bus_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_us_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.elec_bus_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>


        <cfif poc is 0>
         <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Company contact information could be found.</td></tr>
        </cfif>

        </cfoutput>

        <tr><td colspan=4><hr></td></tr>

       </table>

       <!--- Company Feedback --->

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Company Comments</td>
	       <td class="feed_sub_header" align=right>

	       <cfoutput>
	       <img src="/images/plus3.png" width=15 hspace=10><a href="comment_add.cfm?id=#id#" align=absmiddle>Add Comments</a>
	       </cfoutput>

	       </td></tr>
       </table>

		<cfquery name="notes" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select * from company_intel
		 join usr on usr_id = company_intel_created_by_usr_id
		 left join comments_rating on company_intel_rating = comments_rating_id
		 where  company_intel_company_id = #id# and
		        company_intel_hub_id = #session.hub# and
		       (company_intel_created_by_usr_id = #session.usr_id# or company_intel_created_by_company_id = #session.company_id#)
		 order by company_intel_created_date DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif notes.recordcount is 0>

		  <tr><td class="feed_sub_header" style="font-weight: normal;">No comments or notes have been recorded.</td></tr>

	    <cfelse>

	   <cfloop query="notes">

	   <cfoutput>

		 <tr bgcolor="FFFFFF"><td valign=top width=100>

	     <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfif usr_profile_display is 2>
		 	  <tr><td align=center><img src="#image_virtual#/private.png" width=75 height=75 style="margin-top: 10px;" alt="Private Profile" title="Private Profile"></td></tr>
		 <cfelse>

	 		 <cfif #usr_photo# is "">
	 		  <tr><td valign=top><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=50 width=50 border=0 vspace=15 title="#usr_first_name# #usr_last_name#" alt="#usr_first_name# #usr_last_name#"></td></tr>
	 		 <cfelse>
	 		  <tr><td valign=top><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" vspace=15 src="#media_virtual#/#usr_photo#" height=80 width=80 border=0 title="#usr_first_name# #usr_last_name#" alt="#usr_first_name# #usr_last_name#"></tr>
             </cfif>

		 </cfif>

		 </table>

         </td><td valign=top>


	     <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=5></td></tr>
         <tr>

			<cfif #company_intel_created_by_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="comment_edit.cfm?id=#id#&company_intel_id=#company_intel_id#"><b>#company_intel_context#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#company_intel_context#</td>
			</cfif>

			<td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=75>

			<cfif #comments_rating_image# is "">
			 <b>No Rating</b>
			<cfelse>
			 <img src="#image_virtual#/#comments_rating_image#" height=16>
			</cfif>

			</td>

		 </tr>

		 <tr><td colspan=4 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;"><cfif #company_intel_comments# is "">No comments provided<cfelse>#company_intel_comments#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #company_intel_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#company_intel_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #company_intel_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#company_intel_url#" target="_blank" rel="noopener" rel="noreferrer">#company_intel_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#dateformat(company_intel_created_date,'mmm dd, yyyy')# at #timeformat(company_intel_created_date)#</td>
		   </tr>

		  </table>

		  </td></tr>

		   <tr><td colspan=2><hr></td></tr>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>