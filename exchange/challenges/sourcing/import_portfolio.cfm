<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.sub_tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.sub_tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.sub_main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>


<!--- Get Data --->

<cfquery name="selected_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_process_xref
 where challenge_process_xref_challenge_id = #session.challenge_id# and
       challenge_process_xref_process_id = #session.challenge_sourcing# and
       challenge_process_xref_hub_id = #session.hub#
</cfquery>

<cfif selected_list.recordcount is 0>
 <cfset comp_list = 0>
<cfelse>
 <cfset comp_list = valuelist(selected_list.challenge_process_xref_company_id)>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>


       <tr><td valign=top width=185>

        <cfinclude template="/exchange/components/my_profile/profile.cfm">
	    <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top width=100%>


       <div class="main_box">
	       <cfinclude template="/exchange/challenges/challenge_header.cfm">
       </div>

		  <div class="sub_tab_not_active" style="margin-left: 20px;">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/open.cfm">Summary</a></span>
		  </div>

		  <div class="sub_tab_active">
		   <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="index.cfm">Source Companies</a></span>
		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/sourcing/contact_list.cfm">Contact List</a></span>
		  </div>

      <div class="sub_main_box_2">
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">Import from Portfolio</td>
             <td align=right valign=absmiddle class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>
	   </table>

       <cfquery name="myportfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		select portfolio_usr_id, portfolio_image, portfolio_type_id, portfolio_access_id, portfolio_id, portfolio_name, usr_first_name, usr_last_name, portfolio_updated from portfolio
		left join usr on usr_id = portfolio_usr_id
		where (portfolio_usr_id = #session.usr_id# or (portfolio_company_id = #session.company_id# and portfolio_access_id = 2)) and
		       portfolio_type_id = 1 and
		       portfolio_hub_id = #session.hub#
               order by portfolio_name
       </cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif myportfolio.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No have not created any Portfolios.</td>
              <td align=right></td></tr>

         <cfelse>

         <form action="import_portfolio_db.cfm" method="post">

				<tr>
                    <td class="feed_sub_header">Select</td>
                    <td></td>
                    <td class="feed_sub_header">Company Name</td>
                    <td class="feed_sub_header">Description</td>
                    <td class="feed_sub_header">Website</td>
                    <td class="feed_sub_header">City</td>
                    <td class="feed_sub_header" align=center>State</td>
			    </tr>

			    <tr><td height=10></td></tr>

          <cfloop query="myportfolio">

           <cfoutput>
            <tr><td colspan=7 class="feed_sub_header" bgcolor="e0e0e0">&nbsp;#ucase(myportfolio.portfolio_name)#</td></tr>
           </cfoutput>

           <cfquery name="port_companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
            select portfolio_item_company_id from portfolio_item
            where portfolio_item_portfolio_id = #myportfolio.portfolio_id# and
                  portfolio_item_type_id = 1
           </cfquery>

           <cfif port_companies.recordcount is 0>
            <cfset port_list = 0>
           <cfelse>
            <cfset port_list = valuelist(port_companies.portfolio_item_company_id)>
           </cfif>


           <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from company
			  where company_id in (#port_list#)
			  order by company_name
           </cfquery>

			<cfif #companies.recordcount# is 0>

			 <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;">No companies exist in this portfolio.</td></tr>

		    <cfelse>

			<cfoutput>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

                <cfset row_counter = 0>
                <cfset count = 1>

				<cfoutput query="companies">

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td width=25>&nbsp;&nbsp;<input type="checkbox" name="selected_company" <cfif listfind(comp_list,companies.company_id)>checked</cfif> style="width: 22px; height: 22px;" value=#companies.company_id#></td>
					<td width=100 class="feed_option" align=center>

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header" width=300 style="padding-right: 15px;"><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option" style="padding-right: 15px;">

					<cfif companies.company_long_desc is "">
					Description not found.
					<cfelse>

					<cfif len(companies.company_long_desc GT 400)>
					#left(companies.company_long_desc,'400')#...
				    <cfelse>
					#companies.company_long_desc#
					</cfif>

					</cfif>

					</td>

					<td class="feed_option" style="padding-right: 15px;">
					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">

					<cfif companies.company_website is "">
					 Not found
					<cfelse>

					<cfif len(companies.company_website) GT 40>
					#ucase(left(companies.company_website,40))#...
					<cfelse>
					#ucase(companies.company_website)#
					</cfif>

					</cfif>

					</a>
					</td>

					<td class="feed_option">#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				<cfif count is not companies.recordcount>
				 <tr><td colspan=9><hr></td></tr>
				</cfif>

				<cfset count = count + 1>

				</cfoutput>

             </cfif>

          </cfloop>

          <tr><td colspan=8><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td colspan=3><input type="submit" name="button" value="Import" class="button_blue_large"></td></tr>

          </form>

         </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>