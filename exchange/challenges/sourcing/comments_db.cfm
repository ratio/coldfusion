<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Comment">

	<cfif #company_intel_attachment# is not "">
		<cffile action = "upload"
		 fileField = "company_intel_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert company_intel
	  (company_intel_context,
	   company_intel_challenge_id,
	   company_intel_company_id,
	   company_intel_comments,
	   company_intel_attachment,
	   company_intel_url,
	   company_intel_created_date,
	   company_intel_hub_id,
	   company_intel_rating,
	   company_intel_sharing,
	   company_intel_created_by_usr_id,
	   company_intel_created_by_company_id
       )
	  values
	  (
	   '#company_intel_context#',
	    #session.challenge_id#,
	    #id#,
	   '#company_intel_comments#',
	   <cfif #company_intel_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	   '#company_intel_url#',
	    #now()#,
	   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	    #company_intel_rating#,
	    #company_intel_sharing#,
	    #session.usr_id#,
	    <cfif session.company_id is 0 or not isdefined("session.company_id")>null<cfelse>#session.company_id#</cfif>
	   )
	</cfquery>

    <cfif l is 0>
		<cflocation URL="open.cfm?u=1&id=#id#" addtoken="no">
    <cfelseif l is 1>
		<cflocation URL="/exchange/challenges/sourcing/index.cfm?u=4" addtoken="no">
    </cfif>

<cfelseif #button# is "Delete Comment">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select company_intel_attachment from company_intel
		  where (company_intel_id = #company_intel_id#) and
				(company_intel_created_by_usr_id = #session.usr_id#)
		</cfquery>

		<cfif remove.company_intel_attachment is not "">
		 <cffile action = "delete" file = "#media_path#\#remove.company_intel_attachment#">
		</cfif>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete company_intel
		  where (company_intel_id = #company_intel_id#) and
				(company_intel_created_by_usr_id = #session.usr_id#)
		</cfquery>

	</cftransaction>

	<cflocation URL="open.cfm?u=3&id=#id#" addtoken="no">

<cfelseif #button# is "Update Comment">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select company_intel_attachment from company_intel
		  where company_intel_id = #company_intel_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.company_intel_attachment#">

	</cfif>

	<cfif #company_intel_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select company_intel_attachment from company_intel
		  where company_intel_id = #company_intel_id#
		</cfquery>

		<cfif #getfile.company_intel_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.company_intel_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "company_intel_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update company_intel
	  set company_intel_context = '#company_intel_context#',

		  <cfif #company_intel_attachment# is not "">
		   company_intel_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   company_intel_attachment = null,
		  </cfif>

	      company_intel_comments = '#company_intel_comments#',
	      company_intel_url = '#company_intel_url#',
	      company_intel_rating = #company_intel_rating#,
	      company_intel_sharing = #company_intel_sharing#

	  where (company_intel_id = #company_intel_id# ) and
	        (company_intel_created_by_usr_id = #session.usr_id#)
	</cfquery>

	<cflocation URL="open.cfm?u=2&id=#id#" addtoken="no">

</cfif>