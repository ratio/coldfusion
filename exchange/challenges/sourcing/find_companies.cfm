<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.sub_tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.sub_tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.sub_main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfif not isdefined("select")>
 <cfset select = 1>
</cfif>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfif not isdefined("session.challenge_sourcing")>

 <cfquery name="default" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select top(1) *from challenge_sourcing
  where challenge_sourcing_hub_id = #session.hb#
  order by challenge_sourcing_order ASC
 </cfquery>

 <cfset session.challenge_sourcing = #default.challenge_sourcing_id#>

</cfif>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<cfquery name="comp_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

 select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about, company.company_website, company_name, company_city, company_state from company
 where company_id = -1 and
       company_discoverable = 1

 <!--- Non --->

 <cfif session.non is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from company

	<cfif isdefined("session.add_filter")>
	 where contains((company_name, company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((company_name, company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((company_name, company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'#trim(session.auto_keywords)#')
	</cfif>

 </cfif>

 <!--- Awards --->

 <cfif session.awards is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from company
  join award_data on recipient_duns = company_duns

	<cfif isdefined("session.add_filter")>
	 where contains((recipient_name, award_description, awarding_agency_name, awarding_sub_agency_name),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((recipient_name, award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((recipient_name, award_description, awarding_agency_name, awarding_sub_agency_name),'#trim(session.auto_keywords)#')
	</cfif>

 </cfif>

 <!--- SBIRs --->

 <cfif session.sbirs is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from company
  join sbir on duns = company_duns
	<cfif isdefined("session.add_filter")>
	 where contains((company, award_title,abstract, research_keywords),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((company, award_title,abstract, research_keywords),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((company, award_title,abstract, research_keywords),'#trim(session.auto_keywords)#')
	</cfif>

  and company_duns <> ''
 </cfif>

 <!--- Universities --->

 <cfif session.university is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from company
  join autm on exchange_company_id = company_id

	<cfif isdefined("session.add_filter")>
	 where contains((abstract, description, projecttitle, shortdescription, keywords, potentialapplications),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((abstract, description, projecttitle, shortdescription, keywords, potentialapplications),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((abstract, description, projecttitle, shortdescription, keywords, potentialapplications),'#trim(session.auto_keywords)#')
	</cfif>

 </cfif>

 <!--- National Labs --->

 <cfif session.labs is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from company
  join lab_tech on lab_company_id = company_id

	<cfif isdefined("session.add_filter")>
	 where contains((availabletechnology, shortdescription, fulldescription, abstract, abstract2, abstract3),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((availabletechnology, shortdescription, fulldescription, abstract, abstract2, abstract3),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((availabletechnology, shortdescription, fulldescription, abstract, abstract2, abstract3),'#trim(session.auto_keywords)#')
	</cfif>

 </cfif>

 <!--- Grants --->

 <cfif session.grants is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from company
  join grants on recipient_duns = company_duns

	<cfif isdefined("session.add_filter")>
	 where contains((recipient_name, award_description),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((recipient_name, award_description),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((recipient_name, award_description),'#trim(session.auto_keywords)#')
	</cfif>


 </cfif>

 <!--- Patents --->

 <cfif session.patents is 1>
  union
  select distinct(company_id), company_logo, company_keywords, cast(company_long_desc as varchar(max)) as about,  company.company_website, company_name, company_city, company_state from patent
  left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
  left join company on company_name = patent_rawassignee.organization

	<cfif isdefined("session.add_filter")>
	 where contains((patent.patent_id, title, abstract),'#trim(session.auto_keywords)#')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((patent.patent_id, 	title, abstract),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((patent.patent_id, title, abstract),'#trim(session.auto_keywords)#')
	</cfif>
  and patent.patent_id is not null
 </cfif>

 order by company_name

</cfquery>

<cfif isdefined("s")>

 <cfif s is 1>

  <cfloop query="comp_info">

   <cftransaction>

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select challenge_process_xref_id from challenge_process_xref
		 where challenge_process_xref_company_id = #comp_info.company_id# and
		       challenge_process_xref_challenge_id = #session.challenge_id# and
		       challenge_process_xref_process_id = #session.challenge_sourcing# and
		       challenge_process_xref_hub_id = #session.hub#
		</cfquery>

		<cfif check.recordcount is 0>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into challenge_process_xref
			 (
			  challenge_process_xref_company_id,
			  challenge_process_xref_challenge_id,
			  challenge_process_xref_process_id,
			  challenge_process_xref_updated,
			  challenge_process_xref_hub_id,
			  challenge_process_xref_updated_by_usr_id
			  )
			  values
			  (
			  #comp_info.company_id#,
			  #session.challenge_id#,
			  #session.challenge_sourcing#,
			  #now()#,
			  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			  #session.usr_id#
			  )
			</cfquery>

		</cfif>

   </cftransaction>

  </cfloop>

 <cfelseif s is 0>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete challenge_process_xref
	 where challenge_process_xref_challenge_id = #session.challenge_id# and
		   challenge_process_xref_process_id =#session.challenge_sourcing# and
		   challenge_process_xref_hub_id =#session.hub#
	</cfquery>

 </cfif>
</cfif>

<cfquery name="sourcing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_sourcing
 where challenge_sourcing_hub_id = #session.hub#
 order by challenge_sourcing_order ASC
</cfquery>

<!--- Get Data --->

<cfquery name="selected_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_process_xref
 where challenge_process_xref_challenge_id = #session.challenge_id# and
       challenge_process_xref_process_id = #session.challenge_sourcing# and
       challenge_process_xref_hub_id = #session.hub#
</cfquery>

<cfif selected_list.recordcount is 0>
 <cfset comp_list = 0>
<cfelse>
 <cfset comp_list = valuelist(selected_list.challenge_process_xref_company_id)>
</cfif>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt comp_info.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(comp_info.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="filter_box.cfm">

       </td><td valign=top>

       <div class="main_box">
       <cfinclude template="/exchange/challenges/challenge_header.cfm">
       </div>

		  <div class="sub_tab_not_active" style="margin-left: 20px;">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/open.cfm">Summary</a></span>
		  </div>

		  <div class="sub_tab_active">
		   <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="index.cfm">Source Companies</a></span>
		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/sourcing/contact_list.cfm">Contact List</a></span>
		  </div>

      <div class="sub_main_box_2">

         <form action="set3.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=5></td></tr>

 		 <tr><td class="feed_header" valign=absmiddle>Search for Companies

 		 <cfoutput>

 		     (
 		      <cfif comp_info.recordcount is 0>
 		      No Companies Found
 		      <cfelseif comp_info.recordcount is 1>
 		      1 Company Found
 		      <cfelse>
 		      #numberformat(comp_info.recordcount,'99,999')# Companies Found
 		      </cfif>
 		      )

 		 </cfoutput>

 		      </td>

             <td class="feed_sub_header" align=right valign=top>

               <cfset counter1 = 1>
			   <cfoutput query="sourcing">
				<td width=120 class="feed_header" style="font-size: 22px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#challenge_sourcing_id#&start=#start#" style="font-size: 18px;">#challenge_sourcing_name#</a></td>
				<cfif counter1 LT #sourcing.recordcount#>
				<td width=10>&nbsp;</td>
				</cfif>
				<cfset counter1 = counter1 + 1>
			   </cfoutput>

             </tr>

        <cfoutput>

        <tr><td class="feed_sub_header" style="font-weight: normal;">

		  <input type="text" class="input_text" style="width: 300px;" <cfif isdefined("session.auto_keywords")>value="<cfoutput>#replace(session.auto_keywords,'"','','all')#</cfoutput>"</cfif> name="auto_keyword" required placeholder="search by name or capability">
		  <input type="submit" name="button" value="Search" class="button_blue">
		  &nbsp;&nbsp;<a href="import_portfolio.cfm">Import from Portfolio</a>

         &nbsp;&nbsp;&nbsp;&nbsp;

					<cfif comp_info.recordcount GT #perpage#>
						<b>Page</b> <b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt comp_info.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>
		 </cfoutput>

        </td>


        <td align=right>

               <cfset counter2 = 1>

			   <cfloop query="sourcing">

				<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(challenge_process_xref_id) as total from challenge_process_xref
				 where challenge_process_xref_hub_id = #session.hub# and
				       challenge_process_xref_challenge_id = #session.challenge_id# and
				       challenge_process_xref_process_id = #sourcing.challenge_sourcing_id#
				</cfquery>

			   <cfoutput>

                <cfif session.challenge_sourcing is sourcing.challenge_sourcing_id>
				<td width=100 height=40 class="feed_sub_header" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#&start=#start#" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				<cfelse>
				<td width=100 height=40 class="feed_sub_header" style="font-size: 20px; background-color: e0e0e0; color: 000000; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#&start=#start#" style="font-size: 20px; background-color: e0e0e0; color: 000000; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				</cfif>
				<cfif counter2 LT #sourcing.recordcount#>
				<td width=20><img src="/images/icon_spacer.png" width=20></td>
				</cfif>
				<cfset counter2 = counter2 + 1>
				</cfoutput>

				</cfloop>

               </tr>
         <tr><td height=10 class="link_small_gray">Select Companies to add to this Sourcing Phase.  When finished, click on the Sourcing Phase to the right to return to the process.</td></tr>
         <tr><td height=10></td></tr>

		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
	    <cfif u is 1>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully added.</td></tr>
	    <cfelseif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully removed.</td></tr>
	    <cfelseif u is 3>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully added.</td></tr>
	    <cfelseif u is 4>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully removed.</td></tr>
	    </cfif>
	   </cfif>

	   </form>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif comp_info.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies were found.</td>
              <td align=right></td></tr>

         <cfelse>

			<tr>

				<!--- <cfoutput>

				<td class="feed_sub_header" width=150 align=center>

				<a href="find_companies.cfm?s=1&u=10" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/select_all.png" height=22></a>
				<a href="find_companies.cfm?s=0&u=11" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/select_none.png" height=22>

				</td>

				</cfoutput> --->

				<td></td>
				<td class="feed_sub_header">Company Name</td>
				<td class="feed_sub_header">Description</td>
				<td class="feed_sub_header">Website</td>
				<td class="feed_sub_header">City</td>
				<td class="feed_sub_header" align=center>State</td>
				<td class="feed_sub_header" width=150 align=center>

				<cfoutput>

				<a href="find_companies.cfm?s=1&u=10" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/select_all.png" height=22></a>
				<a href="find_companies.cfm?s=0&u=11" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/select_none.png" height=22>

                </cfoutput>

                </td>
			</tr>

			<tr><td height=10></td></tr>

          <cfset row_counter = 0>

          <form action="remove_selected.cfm" method="post">

          <cfoutput query="comp_info" startrow="#url.start#" maxrows="#perpage#">

                <tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>

					<!---

					<td width=150 align=center>

                    <cfif listfind(comp_list,comp_info.company_id)>
                    <a href="check.cfm?u=0&company_id=#comp_info.company_id#"><img src="/images/icon_radio_check.png" width=30 border=0 alt="Unselect" title="Unselect"></a>
                    <cfelse>

                    <a href="check.cfm?u=1&company_id=#comp_info.company_id#"><img src="/images/icon_radio_uncheck.png" width=30 border=0 alt="Select" title="Select"></a>
                    </cfif>

					</td> --->

					<td width=60 class="feed_option">

                    <a href="open.cfm?id=#comp_info.company_id#&d=1">
					  <img src="//logo.clearbit.com/#comp_info.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					</a>


					</td>
					<td class="feed_sub_header" width=350 style="padding-right: 15px;"><a href="profile.cfm?id=#comp_info.company_id#&d=1">#ucase(comp_info.company_name)#</a></td>
					<td class="feed_option" width=400 style="padding-right: 15px;">

					<cfif about is "">

					 <cfif comp_info.company_keywords is "">
					  No description found
					 <cfelse>
					  #wrap(comp_info.company_keywords,100)#
					 </cfif>

					<cfelse>
					 <cfif len(about) GT 300>
					  #left(about,300)#...
					 <cfelse>
					  #about#
					 </cfif>
					</cfif>

					 </td>

					</td>

					<td class="feed_option" width=250>
					<cfif #comp_info.company_website# is "">
						 Not found
					<cfelse>
						<a href="#comp_info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
						<cfif len(comp_info.company_website) GT 40>
						<u>#ucase(left(comp_info.company_website,40))#...</u>
						<cfelse>
						<u>#ucase(comp_info.company_website)#</u>
						</cfif>
						</a>
					</cfif>
					</td>

					<td class="feed_option">#ucase(comp_info.company_city)#</a></td>
					<td class="feed_option" width=75 align=center>#ucase(comp_info.company_state)#</a></td>

					<td width=150 align=center>

                    <cfif listfind(comp_list,comp_info.company_id)>
                    <a href="check.cfm?u=0&company_id=#comp_info.company_id#"><img src="/images/icon_radio_check.png" width=30 border=0 alt="Unselect" title="Unselect"></a>
                    <cfelse>

                    <a href="check.cfm?u=1&company_id=#comp_info.company_id#"><img src="/images/icon_radio_uncheck.png" width=30 border=0 alt="Select" title="Select"></a>
                    </cfif>

					</td>


				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				 <tr><td colspan=10><hr></td></tr>

			</cfoutput>

          </form>

          </table>

         </cfif>

       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>