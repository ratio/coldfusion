<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->


<cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from comments_rating
 where comments_rating_hub_id = #session.hub#
 order by comments_rating_value DESC
</cfquery>

<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company_intel
 where company_intel_id = #company_intel_id# and
 company_intel_created_by_usr_id = #session.usr_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

       <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>EDIT COMMENTS</td>
             <td align=right valign=absmiddle><a href="open.cfm?id=#id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td>
		 <tr><td colspan=10><hr></td></tr>

	   </table>
	   </cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=125>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option">

                    <a href="open.cfm?id=#info.company_id#">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=100 border=0>
					</cfif>
					</a>

					</td>
			    </tr>

			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(info.company_name)#</td>
	              <td class="feed_sub_header" align=right></td></tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #info.company_about# is "">
					No Description Provided
					<cfelse>
					#info.company_about#
					</cfif>

					</td>

			    </tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #info.company_keywords# is "">
					No Keywords Provided
					<cfelse>
					#info.company_keywords#
					</cfif>

					</td>
			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

       <!--- Company Feedback --->

       <form action="comments_db.cfm" method="post" enctype="multipart/form-data">

<cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td class="feed_sub_header" width=200><b>Context</b></td>
			<td><input name="company_intel_context" class="input_text" value="#intel.company_intel_context#" type="text" size=100 maxlength="1000" required placeholder="What's the context of writing these comments?"></td>
		</tr>

		<tr><td class="feed_sub_header" valign=top><b>Description</b></td>
			<td><textarea name="company_intel_comments" class="input_textarea" rows=6 cols=101 placeholder="Please provide any comments related to this intel.">#intel.company_intel_comments#</textarea></td>
		</tr>

		<tr><td class="feed_sub_header"><b>Reference URL</b></td>
			<td><input name="company_intel_url" class="input_text" type="url" size=100 maxlength="1000" value="#intel.company_intel_url#"></td>
		</tr>

		<tr><td height=10></td></tr>

		<tr><td class="feed_sub_header" valign=top>Attachment</td>
			<td class="feed_sub_header" style="font-weight: normal;">

			<cfif #intel.company_intel_attachment# is "">
			  <input type="file" name="company_intel_attachment">
			<cfelse>
			  <a href="#media_virtual#/#intel.company_intel_attachment#" width=150>#intel.company_intel_attachment#</a><br><br>
			  <input type="file" name="company_intel_attachment"><br><br>
			  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove Attachment
			 </cfif>

			 </td></tr>

		<tr>

		<input type="hidden" name="company_intel_id" value=#intel.company_intel_id#>
		<input type="hidden" name="id" value=#id#>

</cfoutput>

		<td class="feed_sub_header">Rating</td>
		<td><select name="company_intel_rating" class="input_select">
		<option value=0>No Rating
		<cfoutput query="rating">
		 <option value=#comments_rating_id# <cfif #intel.company_intel_rating# is #comments_rating_id#>selected</cfif>>#comments_rating_name#
		</cfoutput>
		</select>

		</td></tr>

		<tr>
		<td class="feed_sub_header">Sharing</td>
		<td><select name="company_intel_sharing" class="input_select">
		 <option value=1 <cfif intel.company_intel_sharing is 1>selected</cfif>>Yes, anyone in Company can see
		 <option value=0 <cfif intel.company_intel_sharing is 0>selected</cfif>>No, display only to me
		</select>

		</td></tr>

		<tr><td height=10></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>

		<tr><td></td><td>

              <input type="submit" class="button_blue_large" name="button" value="Update Comment">&nbsp;&nbsp;
		      <input class="button_blue_large" type="submit" name="button" value="Delete Comment" vspace=10 onclick="return confirm('Delete Comment?\r\nAre you sure you want to delete this comment?');">

		</td></tr>

	   </form>

        </table>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>