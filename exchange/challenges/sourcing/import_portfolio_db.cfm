<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

<cfif isdefined("selected_company")>

    <cfset selected_list = listremoveduplicates(#selected_company#,',')>

	<cfloop index="i" list="#selected_list#">

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from challenge_process_xref
		 where challenge_process_xref_company_id = #i# and
		       challenge_process_xref_challenge_id = #session.challenge_id# and
		       challenge_process_xref_process_id = #session.challenge_sourcing# and
		       challenge_process_xref_hub_id = #session.hub#
		</cfquery>

		<cfif check.recordcount is 1>

		<cfelse>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into challenge_process_xref
			 (
			  challenge_process_xref_company_id,
			  challenge_process_xref_challenge_id,
			  challenge_process_xref_process_id,
			  challenge_process_xref_updated,
			  challenge_process_xref_hub_id,
			  challenge_process_xref_updated_by_usr_id
			  )
			  values
			  (
			  #i#,
			  #session.challenge_id#,
			  #session.challenge_sourcing#,
			  #now()#,
			  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			  #session.usr_id#
			  )
			</cfquery>

		</cfif>

	</cfloop>

</cfif>

</cftransaction>

<cflocation URL="/exchange/challenges/sourcing/index.cfm?u=1" addtoken="no">