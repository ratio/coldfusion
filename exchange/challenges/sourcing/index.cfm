<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.sub_tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.sub_tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.sub_main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfquery name="sourcing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_sourcing
 where challenge_sourcing_hub_id = #session.hub#
 order by challenge_sourcing_order ASC
</cfquery>

<cfif not isdefined("session.challenge_sourcing")>

	<cfquery name="sourcing_default" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select top(1) * from challenge_sourcing
	 where challenge_sourcing_hub_id = #session.hub#
	 order by challenge_sourcing_order ASC
	</cfquery>

    <cfset #session.challenge_sourcing# = #sourcing_default.challenge_sourcing_id#>

</cfif>

<cfquery name="next" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select top(1) * from challenge_sourcing
 where challenge_sourcing_id > #session.challenge_sourcing# and
       challenge_sourcing_hub_id = #session.hub#
 order by challenge_sourcing_order ASC
</cfquery>

<cfif next.recordcount is 0>
 <cfset next_id = #session.challenge_sourcing#>
 <cfset stop = 1>
<cfelse>
 <cfset next_id = #next.challenge_sourcing_id#>
</cfif>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select challenge_process_xref_company_id from challenge_process_xref
 where challenge_process_xref_challenge_id = #session.challenge_id# and
       challenge_process_xref_process_id = #session.challenge_sourcing# and
       challenge_process_xref_hub_id = #session.hub#
</cfquery>

<cfif comp.recordcount is 0>
 <cfset comp_list = 0>
<cfelse>
 <cfset comp_list = valuelist(comp.challenge_process_xref_company_id)>
</cfif>

<cfquery name="company_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id in (#comp_list#)
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">

       <cfinclude template="/exchange/challenges/challenge_header.cfm">
       </div>


		  <div class="sub_tab_not_active" style="margin-left: 20px;">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/open.cfm">Summary</a></span>
		  </div>

		  <div class="sub_tab_active">
		   <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="index.cfm">Source Companies</a></span>
		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/sourcing/contact_list.cfm">Contact List</a></span>
		  </div>

      <div class="sub_main_box_2">
	   <form action="set3.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=5></td></tr>

       <cfoutput>
         <tr><td class="feed_header" valign=absmiddle>Search for Companies</td>
       </cfoutput>

             <td class="feed_sub_header" align=right valign=top>

               <cfset counter1 = 1>
			   <cfoutput query="sourcing">
				<td width=120 class="feed_header" style="font-size: 22px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#challenge_sourcing_id#" style="font-size: 18px;">#challenge_sourcing_name#</a></td>
				<cfif counter1 LT #sourcing.recordcount#>
				<td width=10>&nbsp;</td>
				</cfif>
				<cfset counter1 = counter1 + 1>
			   </cfoutput>

             </tr>

         <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 10px; padding-bottom: 0px;">

		  <input type="text" class="input_text" style="width: 300px;" <cfif isdefined("session.auto_keywords")>value="<cfoutput>#session.auto_keywords#</cfoutput>"</cfif> name="auto_keyword" required placeholder="search by name or capability">
		  <input type="submit" name="button" value="Search" class="button_blue">
		  &nbsp;&nbsp;<a href="import_portfolio.cfm">Import from Portfolio</a>


        </td>
        <td align=right>

               <cfset counter2 = 1>

			   <cfloop query="sourcing">

				<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(challenge_process_xref_id) as total from challenge_process_xref
				 where challenge_process_xref_hub_id = #session.hub# and
				       challenge_process_xref_challenge_id = #session.challenge_id# and
				       challenge_process_xref_process_id = #sourcing.challenge_sourcing_id#
				</cfquery>

			   <cfoutput>

                <cfif session.challenge_sourcing is sourcing.challenge_sourcing_id>
				<td width=100 class="feed_sub_header" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				<cfelse>
				<td width=100 class="feed_sub_header" style="font-size: 20px; background-color: e0e0e0; color: 000000; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#" style="font-size: 20px; background-color: e0e0e0; color: 000000; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				</cfif>
				<cfif counter2 LT #sourcing.recordcount#>
				<td width=20><img src="/images/icon_spacer.png" width=20></td>
				</cfif>
				<cfset counter2 = counter2 + 1>
				</cfoutput>

				</cfloop>

               </tr>

       </table>

       </form>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td colspan=10><hr></td></tr>

         <cfif company_info.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been selected for this Sourcing Phase.  Please search for a Company above.</td>
              <td align=right></td></tr>

         <cfelse>

		   <cfif isdefined("u")>
			<cfif u is 1>
			 <tr><td class="feed_sub_header" style="color: green;" colspan=5>Selected Companies have been successfully added.</td></tr>
			<cfelseif u is 2>
			 <tr><td class="feed_sub_header" style="color: green;" colspan=5>Company has been successfully removed.</td></tr>
			<cfelseif u is 4>
			 <tr><td class="feed_sub_header" style="color: green;" colspan=5>Company comments have been saved.</td></tr>
			<cfelseif u is 5>
			 <tr><td class="feed_sub_header" style="color: green;" colspan=5>Company has been added to the next Sourcing process.</td></tr>

			</cfif>
		   </cfif>

			<tr>
                <!---
				<td class="feed_sub_header">Status</td>
				<td></td> --->
				<td></td>
				<td class="feed_sub_header">Company</td>
				<td class="feed_sub_header">Description</td>
				<td class="feed_sub_header">Website</td>
				<td class="feed_sub_header">City</td>
				<td class="feed_sub_header" align=center>State</td>
				<td class="feed_sub_header" align=center>Notes</td>
				<td class="feed_sub_header" align=center>Status</td>
			</tr>

			<tr><td height=10></td></tr>

          <cfset row_counter = 0>

          <form action="remove_selected.cfm" method="post">

          <cfloop query="company_info">

				<cfquery name="advance" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select challenge_process_xref_company_id from challenge_process_xref
				 where challenge_process_xref_challenge_id = #session.challenge_id# and
				       challenge_process_xref_company_id = #company_info.company_id# and
					   challenge_process_xref_process_id = #next_id# and
					   challenge_process_xref_hub_id = #session.hub#
				</cfquery>

                <tr

                <cfoutput>

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>

					<!--- <td width=2%>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="selected_company" style="width: 22px; height: 22px;" value=#company_info.company_id#></td> --->

					<td width=80 class="feed_option">

                    <a href="open.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer">
					  <img src="//logo.clearbit.com/#company_info.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					</a>

					</td>
					<td class="feed_sub_header" width=300 style="padding-right: 15px;"><a href="open.cfm?id=#company_info.company_id#">#ucase(company_info.company_name)#</a></td>
					<td class="feed_option" width=700 style="padding-right: 15px;">

					<cfif company_info.company_long_desc is "">

						Description not found.

					<cfelse>

						<cfif #company_info.company_long_desc# is not "">
						 <cfif #len(company_info.company_long_desc)# GT 400>#left(company_info.company_long_desc,400)#... <cfelse>#company_info.company_long_desc#</cfif>
						<cfelseif #company_info.company_keywords# is not "">
						 #company_info.company_keywords#
						<cfelseif #company_info.company_about# is not "">
						 #company_info.company_about#
						<cfelse>
						 #company_info.company_meta_keywords#
						</cfif>

					</cfif>

					</td>

					<td class="feed_option" width=250>

					<cfif company_info.company_website is "">
					 Not found
					<cfelse>

					<a href="#company_info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(company_info.company_website) GT 40>
					<u>#ucase(left(company_info.company_website,40))#...</u>
					<cfelse>
					<u>#ucase(company_info.company_website)#</u>
					</cfif>
					</a>
					</cfif>

					</td>

					<td class="feed_option">#ucase(company_info.company_city)#</a></td>
					<td class="feed_option" width=75 align=center>#ucase(company_info.company_state)#</a></td>
					<td class="feed_option" align=center width=100>

					<cfquery name="notes" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
					 select count(company_intel_id) as total from company_intel
					 left join usr on usr_id = company_intel_created_by_usr_id
					 left join comments_rating on company_intel_rating = comments_rating_value
					 where  company_intel_company_id = #company_info.company_id# and
							company_intel_hub_id = #session.hub# and
						   (company_intel_created_by_usr_id = #session.usr_id# or company_intel_created_by_company_id = #session.company_id#)
					</cfquery>

					<cfif notes.total GT 0>
					 <b>#notes.total#</b>
					</cfif>


					</td>

					<td align=right width=100 align=center>

                    <cfif isdefined("stop")>

						<img src="/images/icon_advance_selected.png" width=90 border=0>

                    <cfelse>

						<cfif advance.recordcount is 1>
						<img src="/images/icon_advance_next.png" width=90 border=0 alt="Moved Forward" title="Moved Forward">
						<cfelse>
						<a href="advance.cfm?company_id=#company_id#&next_id=#next_id#"><img src="/images/icon_advance.png" style="cursor: pointer;" width=85 border=0 alt="Move Forward" title="Move Forward"></a>
						</cfif>

					</cfif>

					</td>

					<td align=right width=70><a href="remove_company.cfm?id=#company_id#" onclick="return confirm('Remove Company?\r\nAre you sure you want to remove this Company from this Phase?');"><img src="/images/delete.png" width=20 alt="Remove from List" title="Remove from List"></a></td>

				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				 <tr><td colspan=10><hr></td></tr>

				</cfoutput>

			</cfloop>

		  <tr><td height=10></td></tr>
          <!---

          <tr><td colspan=3><input type="submit" name="button" value="Remove" class="button_blue_large"  onclick="return confirm('Remove Selected?\r\nAre you sure you want to remove these companies from Sourcing?');"></td></tr>

          --->

          </form>

          </table>

         </cfif>

       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>