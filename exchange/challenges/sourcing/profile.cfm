<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="selected" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need_comp
 where need_comp_challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="sel" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select challenge_process_xref_id from challenge_process_xref
 where challenge_process_xref_challenge_id = #session.challenge_id# and
       challenge_process_xref_process_id = #session.challenge_sourcing# and
       challenge_process_xref_company_id = #id#
</cfquery>

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>Company Information
		 </td>
             <td align=right valign=absmiddle><span class="feed_sub_header">&nbsp;&nbsp;&nbsp;<a href="find_companies.cfm">Return</a></span></td>
		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
			<cfif u is 1>
			 <tr><td class="feed_sub_header" style="color: green;" colspan=2>Company has been added to the Sourcing Phase.</td></tr>
			<cfelseif u is 2>
			 <tr><td class="feed_sub_header" style="color: red;" colspan=2>Company has been removed from the Sourcing Phase.</td></tr>
		   </cfif>
	   </cfif>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=140>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option">

                    <a href="open.cfm?id=#info.company_id#">
					  <img src="//logo.clearbit.com/#info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					</a>

					</td>
			    </tr>

                <!--- Social Icons --->

                <tr><td height=10></td></tr>

                <tr><td>

                <cfif info.company_twitter_url is not "">
                 <a href="#info.company_twitter_url#" target="blank"><img src="/images/icon_twitter.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif info.company_linkedin_url is not "">
                 <a href="#info.company_linkedin_url#" target="blank"><img src="/images/icon_linkedin.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif info.company_facebook_url is not "">
                 <a href="#info.company_facebook_url#" target="blank"><img src="/images/icon_facebook.png" width=20 border=0 hspace=5></a>
                </cfif>

                </td></tr>

			    <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header">

                <cfif sel.recordcount is 1>
	                <a href="select.cfm?id=#id#&u=2"><img src="/images/icon_checked.png" width=20 hspace=10 align=absmiddle alt="Unselect" title="Unselect" border=0></a><a href="select.cfm?id=#id#&u=2" alt="Unselect" title="Unselect">Selected</a>
                <cfelse>
	                <a href="select.cfm?id=#id#&u=1"><img src="/images/icon_unchecked.png" width=20 hspace=10 align=absmiddle alt="Select" title="Select" border=0></a><a href="select.cfm?id=#id#&u=1" alt="Select" title="Select">Select</a>
                </cfif>

                </td></tr>


			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(info.company_name)#</td>
	              <td class="feed_sub_header" align=right>

	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/resource_home.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Full Company Profile</a>

	              &nbsp;|&nbsp;
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_dashboard.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Award Dashboard</a>



	              </td></tr>

                 <tr><td class="feed_sub_header" style="padding-bottom: 0px;"><b>Overview</b></td></tr>
                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" style="font-weight: normal;" colspan=2>
					<cfif #info.company_about# is "">
					No overview found.
					<cfelse>
					#info.company_about#
					</cfif>

					</td>

			    </tr>

			    <cfif info.company_long_desc is not "">

                 <tr><td class="feed_sub_header" style="padding-bottom: 0px;"><b>Description</b></td></tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" style="font-weight: normal;" colspan=2>
					<cfif #info.company_long_desc# is "">
					No full description found.
					<cfelse>
					#info.company_long_desc#
					</cfif>

					</td>
			    </tr>

			    </cfif>

                 <tr><td class="feed_sub_header" style="padding-bottom: 0px;"><b>Keywords / Tags</b></td><td class="feed_option" align=right><b>Company Website</b></td></tr>
                 <tr>
					<td class="feed_sub_header" style="font-weight: normal;">
					<cfif #info.company_keywords# is "">
					No keywords found.
					<cfelse>
					#info.company_keywords#
					</cfif>

					</td>

			        <td class="feed_option" align=right>

					<a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<u>
					<cfif len(info.company_website) GT 40>
					#ucase(left(info.company_website,40))#...
					<cfelse>
					#ucase(info.company_website)#
					</cfif>
					</u>
					</a>
					</b>



			        </td>


			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

      <!--- Federal Awards --->

	  <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select action_date, award_id_piid, modification_number, recipient_name, recipient_duns, recipient_city_name, recipient_state_code, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, award_description, primary_place_of_performance_city_name, primary_place_of_performance_state_code, naics_code, product_or_service_code, product_or_service_code_description, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_current_end_date, federal_action_obligation, id from award_data
			where recipient_duns = '#info.company_duns#'
			<cfif isdefined("session.add_filter")>
			 and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'#trim(session.auto_keywords)#')
			 <cfloop index="i" list="#session.add_filter#">
			   <cfoutput>
			   and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(i)#"')
			   </cfoutput>
			 </cfloop>
			<cfelse>
			 and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'#trim(session.auto_keywords)#')
			</cfif>
			order by action_date DESC
	   </cfquery>

       <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <tr><td height=10></td></tr>
        <tr><td class="feed_header">Experience Snapshot</td></tr>
        <tr><td class="feed_sub_header" style="font-weight: normal;">
         The following experience was sourced from the Exchange for "<b><i>#replace(session.auto_keywords,'"','','all')#</b></i>"


		 <cfif isdefined("session.add_filter")>
			 <cfloop index="e" list="#session.add_filter#">

			 <cfoutput>
			 and "<b><i>#e#</i></b>"
			 </cfoutput>
			 </cfloop></span>
		 </cfif>

		 .


        </td></tr>
        <tr><td colspan=2><hr></td></tr>
        </table>

        </cfoutput>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Federal Awards <cfif awards.recordcount GT 0>(<cfoutput>#awards.recordcount#</cfoutput> Award<cfif #awards.recordcount# GT 1>s</cfif>)</cfif></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <cfif #awards.recordcount# is 0>
	     <tr><td class="feed_sub_header" style="font-weight: normal;">No Federal Awards were found that match your search criteria.</td></tr>
	    <cfelse>

          <tr height=50>
              <td class="feed_option"><b>Awad Data</b></td>
              <td class="feed_option"><b>Contract</b></td>
              <td class="feed_option"><b>Department / Agency</b></td>
              <td class="feed_option"><b>Description</b></td>
              <td class="feed_option"><b>State</b></td>
              <td class="feed_option"><b>End</b></td>
              <td class="feed_option" align=right><b>Value</b></td>
          </tr>

          <cfset counter = 0>

          <cfoutput query="awards">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_option" width=100 valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top width=100><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b/></a></td>
             <td class="feed_option" valign=top width=300>#ucase(awarding_agency_name)#<br>#awarding_office_name#</td>
             <td class="feed_option" valign=top>#award_description#</td>
             <td class="feed_option" valign=top width=100>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top width=100>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top align=right width=75>#numberformat(federal_action_obligation,'$999,999,999')#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

	    </cfif>

	   </table>

      <!--- SBIR --->

	  <cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select award_start_date, agency, department, award_title, abstract as description, research_keywords, program, phase, award_amount from sbir
			where duns = '#info.company_duns#'
			<cfif isdefined("session.add_filter")>
			 and contains((award_title,abstract, research_keywords, department, agency),'#trim(session.auto_keywords)#')
			 <cfloop index="i" list="#session.add_filter#">
			   <cfoutput>
			   and contains((award_title,abstract, research_keywords, department, agency),'"#trim(i)#"')
			   </cfoutput>
			 </cfloop>
			<cfelse>
			 and contains((award_title,abstract, research_keywords, department, agency),'#trim(session.auto_keywords)#')
			</cfif>
			order by award_start_date DESC
	   </cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td colspan=10><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">SBIR/STTR Awards <cfif sbir.recordcount GT 0>(<cfoutput>#sbir.recordcount#</cfoutput> Award<cfif sbir.recordcount GT 1>s</cfif>)</cfif></td></tr>
	   </table>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	    <cfif #sbir.recordcount# is 0>

	     <tr><td class="feed_sub_header" style="font-weight: normal;">No SBIR/STTR Awards were found that match your search criteria.</td></tr>

	    <cfelse>

          <tr height=50>
              <td class="feed_option"><b>Award Data</b></td>
              <td class="feed_option"><b>Customer</b></td>
              <td class="feed_option"><b>Description</b></td>
              <td class="feed_option" align=center><b>Program</b></td>
              <td class="feed_option" align=center><b>Phase</b></td>
              <td class="feed_option" align=right><b>Value</b></td>
          </tr>

         <cfset counter = 0>

         <cfoutput query="sbir">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_option" width=100 valign=top>#dateformat(award_start_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top width=225><b>#ucase(department)#</b><br>#agency#</td>
             <td class="feed_option" valign=top width=700><b>#ucase(award_title)#</b><br><br>#description#<b><cfif #research_keywords# is not ""><br><br><i>#research_keywords#</i><br><br></cfif></td>
             <td class="feed_option" valign=top width=75 align=center>#program#</td>
             <td class="feed_option" valign=top width=75 align=center>#phase#</td>
             <td class="feed_option" valign=top align=right width=75>#numberformat(award_amount,'$999,999,999')#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <tr><td height=10></td></tr>

	    </cfif>

	   </table>

      <!--- Federal Grants --->

	  <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select action_date, award_id_fain, recipient_name, recipient_duns, awarding_agency_name, awarding_sub_agency_name, federal_action_obligation, period_of_performance_start_date, period_of_performance_current_end_date, awarding_office_name, award_description, id from grants
			where recipient_duns = '#info.company_duns#'

			<cfif isdefined("session.add_filter")>
			 and contains((awarding_agency_name, awarding_sub_agency_name, award_description),'#trim(session.auto_keywords)#')
			 <cfloop index="i" list="#session.add_filter#">
			   <cfoutput>
			   and contains((awarding_agency_name, awarding_sub_agency_name, award_description),'"#trim(i)#"')
			   </cfoutput>
			 </cfloop>
			<cfelse>
			 and contains((awarding_agency_name, awarding_sub_agency_name, award_description),'#trim(session.auto_keywords)#')
			</cfif>
			order by action_date DESC

	   </cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td colspan=5><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Federal Grants <cfif grants.recordcount GT 0>(<cfoutput>#grants.recordcount#</cfoutput> Grant<cfif grants.recordcount GT 1>s</cfif>)</cfif></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <cfif #grants.recordcount# is 0>
	     <tr><td class="feed_sub_header" style="font-weight: normal;">No Federal Grants were found that match your search criteria.</td></tr>
	    <cfelse>

          <tr height=50>
              <td class="feed_option"><b>Award Data</b></td>
              <td class="feed_option"><b>Contract</b></td>
              <td class="feed_option"><b>Department / Agency</b></td>
              <td class="feed_option"><b>Description</b></td>
              <td class="feed_option"><b>State</b></td>
              <td class="feed_option"><b>End</b></td>
              <td class="feed_option" align=right><b>Value</b></td>
          </tr>

          <cfset counter = 0>

          <cfoutput query="grants">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_option" width=100 valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top width=100><a href="/exchange/include/grant_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_fain#</b></a></td>
             <td class="feed_option" valign=top width=300><b>#ucase(awarding_agency_name)#</b><br>#awarding_office_name#</td>
             <td class="feed_option" valign=top>#award_description#</td>
             <td class="feed_option" valign=top width=100>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top width=100>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top align=right width=75>#numberformat(federal_action_obligation,'$999,999,999')#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

	    </cfif>

	   </table>

      <!--- Patents --->

	  <cfquery name="patents" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
        select patent.patent_id, date, abstract as description, title, patent.type from patent
        left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
        left join company on company_name = patent_rawassignee.organization
        where company_id = #id#

		<cfif isdefined("session.add_filter")>
		 and contains((patent.patent_id, title, abstract),'#trim(session.auto_keywords)#')
		 <cfloop index="i" list="#session.add_filter#">
		   <cfoutput>
		   and contains((patent.patent_id, title, abstract),'"#trim(i)#"')
		   </cfoutput>
		 </cfloop>
		<cfelse>
		 and contains((patent.patent_id, title, abstract),'#trim(session.auto_keywords)#')
		</cfif>
	     and patent.patent_id is not null

        order by patent.date DESC
	   </cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td colspan=5><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Patents <cfif patents.recordcount GT 0>(<cfoutput>#patents.recordcount#</cfoutput> Patent<cfif #patents.recordcount# GT 1>s</cfif>)</cfif></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <cfif #patents.recordcount# is 0>
	     <tr><td class="feed_sub_header" style="font-weight: normal;">No Patents were found that match your search criteria.</td></tr>
	    <cfelse>

          <tr height=50>
              <td class="feed_option"><b>Granted</b></td>
              <td class="feed_option"><b>Patent ID</b></td>
              <td class="feed_option"><b>Description</b></td>
              <td class="feed_option" align=right><b>Type</b></td>
          </tr>

          <cfset counter = 0>

          <cfoutput query="patents">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_option" width=100 valign=top>#dateformat(date,'mm/dd/yyyy')#</td>
             <td class="feed_option" valign=top width=100><a href="/exchange/include/patent_information.cfm?patent_id=#patent_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#patent_id#</b></a></td>
             <td class="feed_option" valign=top><b>#ucase(title)#</b><br>#description#</td>
             <td class="feed_option" valign=top align=right>#ucase(type)#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

	    </cfif>

	   </table>


      <!--- Innovations - Universities --->

	  <cfquery name="university" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
        select id as inn_id, projecttitle, abstract as ab, shortdescription, description, contactinformationname, contactinformationemail, contactinformationphone from autm
        where exchange_company_id = #id#

			<cfif isdefined("session.add_filter")>
			 and contains((abstract, description, projecttitle, shortdescription, keywords, potentialapplications),'#trim(session.auto_keywords)#')
			 <cfloop index="i" list="#session.add_filter#">
			   <cfoutput>
			   and contains((abstract, description, projecttitle, shortdescription, keywords, potentialapplications),'"#trim(i)#"')
			   </cfoutput>
			 </cfloop>
			<cfelse>
			 and contains((abstract, description, projecttitle, shortdescription, keywords, potentialapplications),'#trim(session.auto_keywords)#')
			</cfif>

	   </cfquery>

	  <cfquery name="labs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
        select * from lab_tech
        where lab_company_id = #id#

			<cfif isdefined("session.add_filter")>
			 and contains((availabletechnology, shortdescription, fulldescription, abstract, abstract2, abstract3),'#trim(session.auto_keywords)#')
			 <cfloop index="i" list="#session.add_filter#">
			   <cfoutput>
			   and contains((availabletechnology, shortdescription, fulldescription, abstract, abstract2, abstract3),'"#trim(i)#"')
			   </cfoutput>
			 </cfloop>
			<cfelse>
			 and contains((availabletechnology, shortdescription, fulldescription, abstract, abstract2, abstract3),'#trim(session.auto_keywords)#')
			</cfif>

	   </cfquery>

       <cfset university_count = university.recordcount>
       <cfset lab_count = labs.recordcount>
       <cfset total_count = university_count + lab_count>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td colspan=5><hr></td></tr>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Innovations</td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <cfif #total_count# is 0>
	     <tr><td class="feed_sub_header" style="font-weight: normal;">No Innovations were found that match your search criteria.</td></tr>
	    <cfelse>

          <cfset counter = 0>

          <cfoutput query="university">

          <tr>
             <td class="feed_sub_header"><a href="/exchange/research/universities/innovation.cfm?i=#inn_id#&u_name=no&r=no" target="_blank" rel="noopener" rel="noreferrer"><b>#projecttitle#</b></a></td>
          </tr>

          <tr>
             <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#ab#</td>
          </tr>

          <tr>
             <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#contactinformationname#<br>#contactinformationemail#<br>#contactinformationphone#</td>
          </tr>

          <tr><td><hr></td></tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput query="labs">

          <tr>
             <td class="feed_sub_header"><a href="/exchange/include/labtech_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#availabletechnology#</b></a></td>
          </tr>

          <tr>
             <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#shortdescription#</td>
          </tr>

         <cfif LaboratoryRepresentitive is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Lab Representative</b> - #LaboratoryRepresentitive#</td></tr>
         </cfif>


         <cfif RepresentativeTitle is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Title</b> - #RepresentativeTitle#</td></tr>
         </cfif>

         <cfif RepresentativeEmail is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Email</b> - #RepresentativeEmail#</td></tr>
         </cfif>

         <cfif phone is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Phone</b> - #phone#</td></tr>
         </cfif>


          <tr><td><hr></td></tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

	    </cfif>

	   </table>
























	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>