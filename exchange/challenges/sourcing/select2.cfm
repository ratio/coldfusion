<cfif s is 1>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 insert into need_comp
		 (need_comp_company_id, need_comp_challenge_id, need_comp_added, need_comp_added_by, need_comp_hub_id, need_comp_selected_company_id)
		 values
		 (
		  <cfif isdefined("session.company_id")>#session.company_id#<cfelse>null</cfif>,
		  #session.challenge_id#,
		  #now()#,
		  #session.usr_id#,
		  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
		  #id#
		  )
		</cfquery>

<cfelse>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 delete need_comp
	 where need_comp_challenge_id = #session.challenge_id# and
	       need_comp_selected_company_id = #id#
	</cfquery>

</cfif>

<cfif isdefined("l")>
	<cflocation URL="/exchange/challenges/sourcing/" addtoken="no">
<cfelse>
	<cflocation URL="/exchange/challenges/sourcing/open.cfm?id=#id#" addtoken="no">
</cfif>