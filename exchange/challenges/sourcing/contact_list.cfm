<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.sub_tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.sub_tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.sub_main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfif not isdefined("session.challenge_sourcing")>
 <cfset #session.challenge_sourcing# = 1>
</cfif>

<cfquery name="next" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select top(1) * from challenge_sourcing
 where challenge_sourcing_id > #session.challenge_sourcing# and
       challenge_sourcing_hub_id = #session.hub#
 order by challenge_sourcing_order ASC
</cfquery>

<cfif next.recordcount is 0>
 <cfset next_id = #session.challenge_sourcing#>
 <cfset stop = 1>
<cfelse>
 <cfset next_id = #next.challenge_sourcing_id#>
</cfif>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select challenge_process_xref_company_id from challenge_process_xref
 where challenge_process_xref_challenge_id = #session.challenge_id# and
       challenge_process_xref_process_id = #session.challenge_sourcing# and
       challenge_process_xref_hub_id = #session.hub#
</cfquery>

<cfif comp.recordcount is 0>
 <cfset comp_list = 0>
<cfelse>
 <cfset comp_list = valuelist(comp.challenge_process_xref_company_id)>
</cfif>

<cfquery name="company_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id in (#comp_list#)
</cfquery>

<cfquery name="sourcing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_sourcing
 where challenge_sourcing_hub_id = #session.hub#
 order by challenge_sourcing_order ASC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">
       <cfinclude template="/exchange/challenges/challenge_header.cfm">
       </div>

      <cfoutput>

		  <div class="sub_tab_not_active" style="margin-left: 20px;">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/open.cfm">Summary</a></span>
		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="index.cfm">Source Companies</a></span>
		  </div>

		  <div class="sub_tab_active">
		   <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="challenges_outside.cfm">Contact List</a></span>
		  </div>

	  </cfoutput>

       <div class="sub_main_box_2">


	   <form action="set3.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=5></td></tr>

         <tr><td></td>

             <td class="feed_sub_header" align=right valign=top>

               <cfset counter1 = 1>
			   <cfoutput query="sourcing">
				<td width=120 class="feed_header" style="font-size: 22px;" align=center><a href="/exchange/challenges/set_contacts.cfm?challenge_sourcing=#challenge_sourcing_id#" style="font-size: 18px;">#challenge_sourcing_name#</a></td>
				<cfif counter1 LT #sourcing.recordcount#>
				<td width=10>&nbsp;</td>
				</cfif>
				<cfset counter1 = counter1 + 1>
			   </cfoutput>

             </tr>

         <tr><td class="feed_header" height=50>Select Phase to see Company Contacts</td>
        <td align=right>

               <cfset counter2 = 1>

			   <cfloop query="sourcing">

				<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(challenge_process_xref_id) as total from challenge_process_xref
				 where challenge_process_xref_hub_id = #session.hub# and
				       challenge_process_xref_challenge_id = #session.challenge_id# and
				       challenge_process_xref_process_id = #sourcing.challenge_sourcing_id#
				</cfquery>

			   <cfoutput>

                <cfif session.challenge_sourcing is sourcing.challenge_sourcing_id>
				<td width=100 class="feed_sub_header" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_contacts.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				<cfelse>
				<td width=100 class="feed_sub_header" style="font-size: 20px; background-color: e0e0e0; color: 000000; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_contacts.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#" style="font-size: 20px; background-color: e0e0e0; color: 000000; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				</cfif>
				<cfif counter2 LT #sourcing.recordcount#>
				<td width=20><img src="/images/icon_spacer.png" width=20></td>
				</cfif>
				<cfset counter2 = counter2 + 1>
				</cfoutput>

				</cfloop>

               </tr>

       </table>

       </form>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif company_info.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been selected for this Sourcing Phase.  Please search for a Company above.</td>
              <td align=right></td></tr>

         <cfelse>

			<tr><td height=10></td></tr>

          <cfset row_counter = 0>

          <cfset clipboard = "">

          <cfloop query="company_info">

	       <cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
	   	    select * from sams
		    where duns = '#company_info.company_duns#'
		   </cfquery>

                <tr bgcolor="e0e0e0">

                <cfoutput>

					<td width=60 class="feed_option">

                    <a href="open.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer">
					  <img src="//logo.clearbit.com/#company_info.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					</a>

					</td>
					<td colspan=4 class="feed_sub_header" style="padding-right: 15px;"><a href="open.cfm?id=#company_info.company_id#">#ucase(company_info.company_name)#</a></td>

				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

                <tr><td colspan=10>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<cfoutput>

					<cfset poc = 0>

					<cfif company_info.company_poc_phone is not "" and company_info.company_poc_email is not "">
						<tr>
						   <td width=5%>&nbsp;</td>
						   <td class="feed_sub_header" style="font-weight: normal;"><cfif company_info.company_poc_first_name is "" and company_info.company_poc_last_name is "">Unknown<cfelse>#company_info.company_poc_first_name# #company_info.company_poc_last_name#</cfif></td>
						   <td class="feed_sub_header" style="font-weight: normal;"><cfif #company_info.company_poc_title# is "">Unknown<cfelse>#company_info.company_poc_title#</cfif></td>
						   <td class="feed_sub_header" style="font-weight: normal;">#company_info.company_poc_phone#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" align=right>#company_info.company_poc_email#</td>
						</tr>
						<cfset poc = 1>

					<cfif listfind(clipboard,company_info.company_poc_email)>
					<cfelse>
					<cfset clipboard = listappend(clipboard,company_info.company_poc_email)>
					</cfif>

					</cfif>

					<cfif sams.poc_fnme is not "">
						<tr>
						   <td width=5%>&nbsp;</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_fnme# #sams.poc_lname#</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_title#</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_us_phone#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.poc_email#</td>
						</tr>
						<cfset poc = 1>

					<cfif listfind(clipboard,sams.poc_email)>
					<cfelse>
					<cfset clipboard = listappend(clipboard,sams.poc_email)>
					</cfif>
					</cfif>

					<cfif sams.pp_poc_fname is not "">
						<tr>
						   <td width=5%>&nbsp;</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_fname# #sams.pp_poc_lname#</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_title#</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_phone#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.pp_poc_email#</td>
						</tr>
						<cfset poc = 1>


					<cfif listfind(clipboard,sams.pp_poc_email)>
					<cfelse>
					<cfset clipboard = listappend(clipboard,sams.pp_poc_email)>
					</cfif>

					</cfif>

					<cfif sams.alt_poc_fname is not "">
						<tr>
						   <td width=5%>&nbsp;</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_fname# #sams.alt_poc_lname#</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_title#</td>
						   <td class="feed_sub_header" style="font-weight: normal;">#sams.akt_poc_phone#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.alt_poc_email#</td>
						</tr>
						<cfset poc = 1>

					<cfif listfind(clipboard,sams.alt_poc_email)>
					<cfelse>
					<cfset clipboard = listappend(clipboard,sams.alt_poc_email)>
					</cfif>

					</cfif>

					<cfif sams.elec_bus_poc_fname is not "">
						<tr>
						   <td width=5%>&nbsp;</td>
						   <td class="feed_sub_header" style="font-weight: normal;" width=25%>#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" width=25%#sams.elec_bus_poc_title#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" width=20%>#sams.elec_bus_poc_us_phone#</td>
						   <td class="feed_sub_header" style="font-weight: normal;" width=25% align=right>#sams.elec_bus_poc_email#</td>
						</tr>
						<cfset poc = 1>

					<cfif listfind(clipboard,sams.elec_bus_poc_email)>
					<cfelse>
					<cfset clipboard = listappend(clipboard,sams.elec_bus_poc_email)>
					</cfif>

					</cfif>

					<cfif poc is 0>
					 <tr><td colspan=5 class="feed_sub_header" style="font-weight: normal;">No Company contact information could be found.</td></tr>
					</cfif>

					</cfoutput>

				   </table>

                </td></tr>

				</cfoutput>

			</cfloop>

		  <tr><td height=10></td></tr>

          </table>

          <cfoutput>

<script>
function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
</script>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td><hr></td></tr>
	       <tr><td class="feed_sub_header">Copy Email Addresses to Clipboard</td></tr>
	       <tr><td><textarea name="clipboard" class="input_textarea" id="myInput" style="width: 100%; height: 100px;">#clipboard#</textarea></td></tr>
	       <tr><td height=10></td></tr>
	       <tr><td><input type="submit" name="button" class="button_blue_large" value="Copy to Clipboard" onclick="myFunction()"></td></tr>
	      </table>

	      </cfoutput>

         </cfif>

       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>