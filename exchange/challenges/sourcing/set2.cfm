<cfif isdefined("non")>
 <cfset session.non = 1>
<cfelse>
 <cfset session.non = 0>
</cfif>

<cfif isdefined("awards")>
 <cfset session.awards = 1>
<cfelse>
 <cfset session.awards = 0>
</cfif>

<cfif isdefined("sbirs")>
 <cfset session.sbirs = 1>
<cfelse>
 <cfset session.sbirs = 0>
</cfif>

<cfif isdefined("grants")>
 <cfset session.grants = 1>
<cfelse>
 <cfset session.grants = 0>
</cfif>

<cfif isdefined("patents")>
 <cfset session.patents = 1>
<cfelse>
 <cfset session.patents = 0>
</cfif>

<cfif isdefined("university")>
 <cfset session.university = 1>
<cfelse>
 <cfset session.university = 0>
</cfif>

<cfif isdefined("labs")>
 <cfset session.labs = 1>
<cfelse>
 <cfset session.labs = 0>
</cfif>

<cfif isdefined("challenges")>
 <cfset session.challenges = 1>
<cfelse>
 <cfset session.challenges = 0>
</cfif>

<cflocation URL="find_companies.cfm" addtoken="no">