<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="process" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_process
 where challenge_process_hub_id = #session.hub#
 order by challenge_process_order
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>


      <td width=185 valign=top>

	        <cfinclude template="/exchange/components/my_profile/profile.cfm">
	        <cfinclude template="recent_challenges.cfm">

       </td>


      <td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Create Challenge</td>
            <td valign=top align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header">Challenge Name</td>
				 <td><input class="input_text" type="text" name="challenge_name" style="width: 600px;" required maxlength="500"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=200>Challege Organization</td>
				 <td><input class="input_text" type="text" name="challenge_organization" style="width: 600px;" maxlength="500" required placeholder="Name of the organization sponsoring this challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="challenge_desc" style="width: 900px; height: 75px;" placeholder="Please provide a short description of what this Challenge is intented to solve."></textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Today's<br>Challenges</td>
				 <td><textarea class="input_textarea" name="challenge_challenges" style="width: 900px; height: 75px;" placeholder="Please describes the challenges associated with today's solution or process."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
				 <td><textarea class="input_textarea" name="challenge_use_case" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how the new solution will be used in the future."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Future State<br>Solution</td>
				 <td><textarea class="input_textarea" name="challenge_future_state" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state solution and any constraints."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="challenge_keywords" maxlength="1500" style="width: 900px;" placeholder="Enter up to 5 keywords (seperated by commas) that are related to this Challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="challenge_url" maxlength="1000" style="width: 900px;" placeholder="Please provide the link the external URL / announcement."></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Challenge Reward</td>
				 <td><textarea class="input_textarea" name="challenge_reward" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the what Companies will receive by winning this Challenge."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Participation<br>Instructions</td>
				 <td><textarea class="input_textarea" name="challenge_instructions" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide and / all instructions for Companies who participate in this Challenge."></textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">End Date</td>
		     <td><input type="date" name="challenge_end" class="input_date" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">Anonymous</td>
		     <td>
		     <select name="challenge_annoymous" class="input_select" style="width: 165px;">
		      <option value=0>No
		      <option value=1>Yes
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will hide the organization name when viewing the Challenge.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Sharing</td>
		     <td>
		     <select name="challenge_public" class="input_select">
		      <option value=0>No, keep private
		      <option value=1>Yes, within this Exchange / Network
		      <option value=2>Yes, on any Exchange / Network
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Determines who will see this Challenge.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Allow Questions</td>
		     <td>
		     <select name="challenge_open" class="input_select">
		      <option value=0>No
		      <option value=1>Yes
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" allows users to ask questions and accept the Challenge.</span>
			 </div>

		     </td></tr>

			  <tr>
				 <td class="feed_sub_header">Total Prizes</td>
				 <td><input type="number" class="input_text" name="challenge_total_cash" style="width: 150px;"></td>
		      </tr>

              <tr><td class="feed_sub_header" valign=top>Challenge Image</td>
                  <td colspan=3 class="feed_sub_header" style="font-weight: normal;"><input type="file" name="challenge_image"></td></tr>

		 <tr><td class="feed_sub_header">Status</td>
		     <td>
		      <select name="challenge_status" class="input_select" style="width: 165px;">
		       <option value=3>Pending
		       <option value=1>Open
		       <option value=2>Closed
		      </select>

		     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			   <span class="tooltiptext">Pending - Not Ready<br>Open - Active and Available<br>Closed - No Longer Active.</span>
			 </div>

		      </td></tr>

         </cfoutput>

		 <tr><td class="feed_sub_header">Process</td>
		     <td class="feed_sub_header" style="font-weight: normal;">

		     <select name="challenge_process_id" class="input_select">
		      <option value=0>Select Process
		      <cfoutput query="process">
		       <option value=#challenge_process_id#>#challenge_process_name#
		      </cfoutput>
            </select>

		     </td></tr>

           <cfoutput>

              <tr><td colspan=4><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header">Contact Name</td>
				 <td><input class="input_text" type="text" name="challenge_poc_name" maxlength="200" style="width: 300px;" required></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Title</td>
				 <td><input class="input_text" type="text" name="challenge_poc_title" maxlength="200" style="width: 300px;"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Email</td>
				 <td><input class="input_text" type="text" name="challenge_poc_email" maxlength="200" style="width: 300px;" required></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Phone</td>
				 <td><input class="input_text" type="text" name="challenge_poc_phone" maxlength="100" style="width: 300px;"></td>
		      </tr>

		      </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
              <input type="submit" class="button_blue_large" name="button" value="Create Challenge">&nbsp;&nbsp;

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>