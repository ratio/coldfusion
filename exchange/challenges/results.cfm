<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfif not isdefined("sv")>
 <cfset sv = 60>
</cfif>

<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from challenge
 left join challenge_process on challenge_process.challenge_process_id = challenge.challenge_process_id
 left join hub on hub_id = challenge_sponsor_id
 where challenge_hub_id = #session.hub#

 <cfif session.challenge_search_community is not 0>
  and challenge_sponsor_id = #session.challenge_search_community#
 </cfif>

 <cfif #session.challenge_search_keyword# is not "">
  and contains((challenge_poc_name, challenge_name, challenge_organization, challenge_name, challenge_keywords),'"#trim(session.challenge_search_keyword)#"')
 </cfif>

	<cfif #sv# is 1>
	 order by challenge_name ASC
	<cfelseif #sv# is 10>
	 order by challenge_name DESC
	<cfelseif #sv# is 2>
	 order by hub_name ASC
	<cfelseif #sv# is 20>
	 order by hub_name DESC
	<cfelseif #sv# is 3>
	 order by challenge_process_order ASC
	<cfelseif #sv# is 30>
	 order by challenge_process_order DESC
	<cfelseif #sv# is 4>
	 order by challenge_status ASC
	<cfelseif #sv# is 40>
	 order by challenge_status DESC
	<cfelseif #sv# is 5>
	 order by challenge_poc_name ASC
	<cfelseif #sv# is 50>
	 order by challenge_poc_name DESC
	<cfelseif #sv# is 6>
	 order by challenge_updated ASC
	<cfelseif #sv# is 60>
	 order by challenge_updated DESC
	<cfelseif #sv# is 7>
	 order by challenge_organization ASC
	<cfelseif #sv# is 70>
	 order by challenge_organization DESC
    </cfif>

</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      <cfif usr.hub_xref_role_id is 1>
	      <cfinclude template="/exchange/portfolio/recent.cfm">
      <cfelse>
	      <cfinclude template="/exchange/profile_company.cfm">
      </cfif>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
			<td class="feed_header">SEARCH RESULTS</td>
			<td class="feed_sub_header" align=right><img src="/images/plus3.png" width=10 width=10 hspace=10 align=absmiddle><a href="/exchange/challenges/new.cfm">Create Challenge</a></td>
	    </tr>
	    <tr><td colspan=2><hr></td></tr>
	   </table>

        <cfinclude template="challenge_search.cfm">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	    <tr><td colspan=2><hr></td></tr>

       </table>

        <cfif challenges.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Challenges were found.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
               <td></td>


               <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>">NAME / DESCRIPTION</a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <!--- <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>">COMMUNITY / SPONSOR</a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td> --->

               <td class="feed_sub_header">
               <a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>">COMMUNITY</a>&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif>

               /&nbsp;

               <a href="results.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>">SPONSOR</a>&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=10></cfif>

               </td>

               <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>">PROCESS</a>&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>">STAGE</a>&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>">LEAD</a>&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>">UPDATED</a>&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>

           </tr>

           <cfloop query="challenges">

			   <cfoutput>

				   <tr>

                      <td width=80>

						<cfif challenges.challenge_image is "">
						  <a href="/exchange/challenges/set.cfm?challenge_id=#challenge_id#"><img src="/images/stock_challenge.png" vspace=10 width=60 border=0></a>
						<cfelse>
						  <a href="/exchange/challenges/set.cfm?challenge_id=#challenge_id#"><img src="#media_virtual#/#challenges.challenge_image#" vspace=10 width=60 border=0></a>
						</cfif>

					   </td>

					   <td class="feed_sub_header" style="font-weight: normal;" valign=middle><a href="/exchange/challenges/set.cfm?challenge_id=#challenge_id#"><b>#ucase(challenges.challenge_name)#</b></a>
					   <br>#challenges.challenge_desc#
					   <br><span class="link_small_gray">#challenges.challenge_keywords#</span></td>

					   <td class="feed_sub_header" style="font-weight: normal;"><b><cfif challenges.hub_name is "">No Sponsor<cfelse>#ucase(challenges.hub_name)#</cfif></b><br><cfif challenges.challenge_organization is "">Organization Not Specified<cfelse>#challenges.challenge_organization#</cfif></td>


					   <td width=100 class="feed_sub_header" align=center><a href="/exchange/challenges/set.cfm?challenge_id=#challenge_id#"><cfif #challenge_process_id# is 0>Not Started<cfelse><img src="/images/process_#challenge_process_id#.png" width=100 align=center border=0 alt="#challenges.challenge_process_name#" title="#challenges.challenge_process_name#"></cfif></a></td>
					   <td width=100 class="feed_sub_header" style="font-weight: normal;" hspace=10 align=center>


						  <cfif challenge_status is 0>
						   Preparing
						  <cfelseif challenge_status is 1>
						   Launched
						  <cfelse>
						   Closed
						  </cfif>


					   </td>
					   <td align=center width=100 class="feed_sub_header" style="font-weight: normal;"><cfif #challenge_poc_name# is "">TBD<cfelse>#challenge_poc_name#</cfif></td>
					   <td align=center width=125 class="feed_sub_header" style="font-weight: normal;">#dateformat(challenge_updated,'mm/dd/yyyy')#</td>
					   </tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=7><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>