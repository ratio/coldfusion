<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="cquestions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from question
 join usr on usr_id = question_usr_id
 where question_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">
        <cfinclude template="challenge_header.cfm">
       </div>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Update Question</td>
            <td valign=top align=right class="feed_sub_header"><a href="/exchange/challenges/interest_questions.cfm">All Questions</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

       <form action="question_db.cfm" method="post">

        <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>

			 <cfif #cquestions.usr_photo# is "">
			  <td width=75><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(cquestions.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#headshot.png" height=50 width=50 border=0></td>
			 <cfelse>
			  <td width=75><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(cquestions.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" src="#media_virtual#/#cquestions.usr_photo#" height=50 width=50 border=0></td>
			 </cfif>

         <td class="feed_sub_header" style="font-weight: normal;"><b>From: #cquestions.usr_first_name# #cquestions.usr_last_name#</b><br>on #dateformat(cquestions.question_date_submitted,'mmm d, yyyy')# at #timeformat(cquestions.question_date_submitted,'short')#</td>
         </tr>
        </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>


         <tr><td class="feed_sub_header">Subject</td></tr>
         <tr><td><input type="text" name="question_subject" class="input_text" style="width: 600px;" value="#cquestions.question_subject#"></td></tr>


         <tr><td class="feed_sub_header">Question</td></tr>
         <tr><td><textarea name="question_text" class="input_textarea" style="width: 1100px; height: 150px;">#cquestions.question_text#</textarea></td></tr>


         <tr><td class="feed_sub_header">Answer</td></tr>
         <tr><td><textarea name="question_answer" class="input_textarea" style="width: 1100px; height: 150px;">#cquestions.question_answer#</textarea></td></tr>

         <tr><td class="feed_sub_header">Display Publicly?</td></tr>

         <tr><td>
             <select name="question_public" class="input_select">
              <option value=0 <cfif #cquestions.question_public# is 0>selected</cfif>>No, keep private
              <option value=1 <cfif #cquestions.question_public# is 1>selected</cfif>>Yes, display publicly
             </select>

             </td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>

         <input type="hidden" name="i" value=#i#>

         <tr><td><input type="submit" name="button" value="Save" class="button_blue_large">&nbsp;&nbsp;
         		      <input class="button_blue_large" type="submit" name="button" value="Delete Question" vspace=10 onclick="return confirm('Delete Question?\r\nAre you sure you want to delete this Question?');">


         </td></tr>

        </cfoutput>


  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>