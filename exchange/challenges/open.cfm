<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset home = 1>


<style>
.sub_tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.sub_tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.sub_main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_usr_id, recent_challenge_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#session.usr_id#,#session.challenge_id#,#session.company_id#,#session.hub#,#now()#)
</cfquery>

<cfquery name="sourcing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_sourcing
 where challenge_sourcing_hub_id = #session.hub#
 order by challenge_sourcing_order ASC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">
       <cfinclude template="/exchange/challenges/challenge_header.cfm">
       </div>

		  <div class="sub_tab_active" style="margin-left: 20px;">
		   <span class="feed_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/open.cfm">Summary</a></span>
		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/sourcing/index.cfm">Source Companies</a></span>
		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/challenges/sourcing/contact_list.cfm">Contact List</a></span>

		  </div>

       <div class="sub_main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
	       <tr><td class="feed_header">Challenge Summary</td>
	           <td class="feed_sub_header" align=right><img src="/images/icon_edit.png" width=20 hspace=10><a href="edit.cfm">Edit Challenge</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif isdefined("u")>
	    <cfif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge has been successfully updated.</td></tr>
        <cfelseif u is 10>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Attachment has been successfully added.</td></tr>
        <cfelseif u is 20>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Attachment has been successfully updated.</td></tr>
        <cfelseif u is 30>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Attachment has been successfully deleted.</td></tr>
        <cfelseif u is 41>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Update has been successfully added.</td></tr>
        <cfelseif u is 42>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Update has been successfully saved.</td></tr>
        <cfelseif u is 43>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Update has been successfully deleted.</td></tr>
        <cfelseif u is 100>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge has been successfully created.</td></tr>

        <cfelseif u is 71>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge learning has been successfully saved.</td></tr>
        <cfelseif u is 72>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge learning has been successfully deleted.</td></tr>
        <cfelseif u is 73>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge learning has been successfully created.</td></tr>

        </cfif>
	   </cfif>

       <tr><td valign=top width=48%>

       <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header">Description</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">#challenge_info.challenge_desc#</td></tr>
             <tr><td><hr></td></tr>
			 <tr><td class="feed_sub_header">Todays Challenges</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #challenge_info.challenge_challenges# is "">Not Provided<cfelse>#challenge_info.challenge_challenges#</cfif></td></tr>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header">Use Case Scenario</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_use_case# is "">Not Provided<cfelse>#challenge_info.challenge_use_case#</cfif></td></tr>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header">Future State Solution</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_future_state# is "">Not Provided<cfelse>#challenge_info.challenge_future_state#</cfif></td></tr>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header">Reward</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_reward# is "">Not Provided<cfelse>#challenge_info.challenge_reward#</cfif></td></tr>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header">Instructions</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_instructions# is "">Not Provided<cfelse>#challenge_info.challenge_instructions#</cfif></td></tr>

		   </table>

		   </cfoutput>

           </td><td width=50>&nbsp;</td>

           <td valign=top width=48%>

  	       <table cellspacing=0 cellpadding=0 border=0 width=100%>



       <cfoutput>
         <tr>
       </cfoutput>

               <cfset counter1 = 1>
			   <cfoutput query="sourcing">
				<td width=120 class="feed_header" style="font-size: 22px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#challenge_sourcing_id#" style="font-size: 18px;">#challenge_sourcing_name#</a></td>
				<cfif counter1 LT #sourcing.recordcount#>
				<td width=10>&nbsp;</td>
				</cfif>
				<cfset counter1 = counter1 + 1>
			   </cfoutput>

             </tr>

        <tr><td height=10></td></tr>

        <tr height=40>

               <cfset counter2 = 1>
			   <cfloop query="sourcing">

				<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(challenge_process_xref_id) as total from challenge_process_xref
				 where challenge_process_xref_hub_id = #session.hub# and
				       challenge_process_xref_challenge_id = #session.challenge_id# and
				       challenge_process_xref_process_id = #sourcing.challenge_sourcing_id#
				</cfquery>

			   <cfoutput>

				<td width=100 class="feed_sub_header" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;" align=center><a href="/exchange/challenges/set_sourcing.cfm?challenge_sourcing=#sourcing.challenge_sourcing_id#" style="font-size: 20px; background-color: green; color: ffffff; padding-top: 0px; padding-bottom: 0px;">#comp.total#</a></td>
				<cfif counter2 LT #sourcing.recordcount#>
				<td width=20>&nbsp;</td>
				</cfif>
				<cfset counter2 = counter2 + 1>
				</cfoutput>

				</cfloop>
               </tr>

           <tr><td height=10></td></tr>

             </table>

  	       <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header">Keywords</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_keywords# is "">Not Provided<cfelse>#challenge_info.challenge_keywords#</cfif></td>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header">Reference URL</td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_url# is "">Not Provided<cfelse><u><a href="#challenge_info.challenge_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#challenge_info.challenge_url#</a></u></cfif></td></tr>
             <tr><td><hr></td></tr>
             <tr><td class="feed_sub_header" width=200>Point of Contact</td></tr>

             <cfif #trim(challenge_info.challenge_poc_name)# is not "">
	             <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#challenge_info.challenge_poc_name#</td></tr>
             </cfif>

             <cfif #trim(challenge_info.challenge_poc_title)# is not "">
                 <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#challenge_info.challenge_poc_title#</td></tr>
             </cfif>

             <cfif #trim(challenge_info.challenge_poc_email)# is not "">
             	<tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#challenge_info.challenge_poc_email#</td></tr>
             </cfif>

             <cfif #trim(challenge_info.challenge_poc_phone)# is not "">
             	<tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#challenge_info.challenge_poc_phone#</td></tr>
             </cfif>

           </cfoutput>

           </table>

           </td></tr>

        </table>

       <cfoutput>

			<cfquery name="attachments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from attachment
			 join usr on usr_id = attachment_created_by
			 where attachment_challenge_id = #session.challenge_id#
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">Attachments</td>
                 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="attachment_add.cfm">Add Attachment</a></td></tr>
           </table>

       </cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif attachments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No attachments have been added.</td></tr>
             <cfelse>

              <tr>
                  <td class="feed_sub_header">Name</td>
                  <td class="feed_sub_header">Description</td>
                  <td class="feed_sub_header">File</td>
                  <td class="feed_sub_header" align=center>Added</td>
                  <td class="feed_sub_header" align=right>By</td>
              </tr>

              <cfset counter = 0>

              <cfoutput query="attachments">

              <cfif counter is 0>
               <tr bgcolor="FFFFFF">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top width=200><b><a href="attachment_edit.cfm?attachment_id=#attachment_id#">#attachment_name#</a></b></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_desc# is "">Not Provided<cfelse>#attachment_desc#</cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_file# is "">No attachment<cfelse><a href="#media_virtual#/#attachment_file#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#attachment_file#</a></cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=center width=120>#dateformat(attachment_updated,'mm/dd/yyyy')#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=right width=150>#usr_first_name# #usr_last_name#</td>

                  </tr>

                  <cfif counter is 0>
                   <cfset counter = 1>
                  <cfelse>
                   <cfset counter = 0>
                  </cfif>

              </cfoutput>

             </cfif>

			<cfquery name="updates" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from up
			 left join usr on usr_id = up_usr_id
			 where up_challenge_id = #session.challenge_id#
			 order by up_date DESC
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">Challenge Updates</td>
                 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="update_add.cfm">Add Update</a></td></tr>
           </table>



	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif updates.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No Updates have been recorded.</td></tr>
	    <cfelse>

       <cfset counter = 1>

	   <cfloop query="updates">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #up_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="update_edit.cfm?update_id=#up_id#"><b>#ucase(up_name)#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#ucase(up_name)#</td>
			</cfif>
			<td class="feed_sub_header" valign=top align=right width=75>
			<cfif up_post is 0>
			 NOT POSTED
			<cfelse>
			 POSTED
			</cfif>
			</td>

		 </tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfif #up_desc# is "">No update provided<cfelse>#up_desc#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #up_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#up_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #up_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#up_url#" target="_blank" rel="noopener" rel="noreferrer">#up_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#usr_first_name# #usr_last_name# | #dateformat(up_date,'mm/dd/yyyy')# at #timeformat(up_date)#</td>
		   </tr>

           <cfif counter LT updates.recordcount>
		   <tr><td colspan=2><hr></td></tr>
		   </cfif>

           </cfoutput>

           <cfset counter = counter + 1>

         </cfloop>

	    </cfif>

	   </table>

		<cfquery name="learn" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from challenge_learn
		 left join usr on usr_id = challenge_learn_usr_id
		 where challenge_learn_challenge_id = #session.challenge_id#
		 order by challenge_learn_updated DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td height=10></td></tr>
		 <tr><td colspan=2><hr></td></tr>
		 <tr>
			 <td class="feed_sub_header">Challenge Learnings</td>
			 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="learn_add.cfm">Add Learning</a></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif learn.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No learnings have been recorded.</td></tr>
	    <cfelse>

	   <cfloop query="learn">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #challenge_learn_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="learn_edit.cfm?challenge_learn_id=#challenge_learn_id#"><b>#ucase(challenge_learn_name)#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#ucase(challenge_learn_name)#</td>
			</cfif>

		   <td align=right class="feed_sub_header" style="font-weight: normal;">#usr_first_name# #usr_last_name# | #dateformat(challenge_learn_updated,'mm/dd/yyyy')#</td>

		 </tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfif #challenge_learn_desc# is "">No learning provided.<cfelse>#challenge_learn_desc#</cfif></td></tr>

		   <tr><td colspan=2><hr></td></tr>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>

		<cfquery name="check_metric" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select challenge_metric_id from challenge_metric
		 where challenge_metric_challenge_id = #session.challenge_id#
		</cfquery>

		<cfif check_metric.recordcount is 0>
			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into challenge_metric
			 (challenge_metric_hub_id, challenge_metric_challenge_id)
			 values
			 (#session.hub#,#session.challenge_id#)
			</cfquery>

		</cfif>

		<cfquery name="metric" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from challenge_metric
		 where challenge_metric_challenge_id = #session.challenge_id#
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td height=10></td></tr>
		 <tr><td colspan=2><hr></td></tr>
		 <tr>
			 <td class="feed_sub_header">Challenge Metrics</td>
			 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="metric_update.cfm">Update Metrics</a></td></tr>
	   </table>


	   <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td height=20></td></tr>

			<tr height=50>

			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center><cfif #metric.challenge_metric_pocs_accessed# is "">0<cfelse>#metric.challenge_metric_pocs_accessed#</cfif></td>
			<td width=20>&nbsp;</td>
			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center>

            <cfif #metric.challenge_metric_transition# is "">
             No
            <cfelse>
             <cfif #metric.challenge_metric_transition# is 0>
              No
             <cfelse>
              Yes
             </cfif>
            </cfif>

            </td>
			<td width=20>&nbsp;</td>

			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center><cfif #metric.challenge_metric_consignments# is "">0<cfelse>#metric.challenge_metric_consignments#</cfif></td>

			<td width=20>&nbsp;</td>


			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center>

            <cfif #metric.challenge_metric_cross_werx# is "">
             No
            <cfelse>
             <cfif #metric.challenge_metric_cross_werx# is 0>
              No
             <cfelse>
              Yes
             </cfif>
            </cfif>

            </td>

			<td width=20>&nbsp;</td>

			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center><cfif #metric.challenge_metric_learnings# is "">0<cfelse>#metric.challenge_metric_learnings#</cfif></td>
			<td width=20>&nbsp;</td>

			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center><cfif #metric.challenge_metric_companies_sourced# is "">0<cfelse>#metric.challenge_metric_companies_sourced#</cfif></td>
			<td width=20>&nbsp;</td>

			<td class="feed_header" style="font-size: 24px; background-color: green; color: FFFFFF;" align=center><cfif #metric.challenge_metric_speed_to_solve# is "">0<cfelse>#metric.challenge_metric_speed_to_solve#</cfif> Days</td>

			</tr>


			<tr>
             <td valign=top class="feed_sub_header" align=center width=13%><b>Experts & POC's Accessed</b></td>
             <td></td>
             <td valign=top class="feed_sub_header" align=center width=13%><b>Transition to Program of Record?</b></td>
             <td></td>
             <td valign=top class="feed_sub_header" align=center width=13%><b>## of Consignments</b></td>
             <td></td>
             <td valign=top class="feed_sub_header" align=center width=13%><b>Cross Collaboration</b></td>
             <td></td>
             <td valign=top class="feed_sub_header" align=center width=13%><b>## of Learnings</b></td>
             <td></td>
             <td valign=top class="feed_sub_header" align=center width=13%><b>## of Companies Sourceds</b></td>
             <td></td>
             <td valign=top class="feed_sub_header" align=center width=13%><b>Speed to Solve</b></td>
            </tr>


		   </table>

  	   </cfoutput>






       </td></tr>

           </table>





	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>