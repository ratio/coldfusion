
<cfquery name="challenge_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 left join challenge_process on challenge_process.challenge_process_id = challenge.challenge_process_id
 left join hub on hub_id = challenge_sponsor_id
 where challenge_id = #session.challenge_id#
</cfquery>

<cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="/exchange/challenges/open.cfm">#challenge_info.challenge_name#</a></td>
           <td align=right class="feed_sub_header">

            <img src="/images/icon_list.png" width=18 hspace=10><a href="/exchange/challenges/index.cfm">All Challlenges</a>

           </td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td colspan=10><hr></td></tr>

       <tr><td valign=middle width=100>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif challenge_info.challenge_image is "">
				  <img src="#image_virtual#/stock_challenge.png" width=75>
				<cfelse>
				  <img src="#media_virtual#/#challenge_info.challenge_image#" width=75>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                <td class="feed_sub_header">Exchange / Network</td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header" align=center>Process</td>
                <td class="feed_sub_header" align=center>Start Date</td>
                <td class="feed_sub_header" align=center>End Date</td>
                <td class="feed_sub_header" align=right>Updated</td>
                <td class="feed_sub_header" align=right>Status</td>

             </tr>

             <tr>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" width=150>#session.network_name#</td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #challenge_info.challenge_organization# is "">Not Identified<cfelse>#challenge_info.challenge_organization#</cfif></td>

			    <td align=center class="feed_sub_header" style="font-weight: normal;" valign=top>

			    <cfif #challenge_info.challenge_process_id# is "" or #challenge_info.challenge_process_id# is 0>
			    Not Started
			    <cfelse>
			     <cfif challenge_info.challenge_process_image is "">
			      #challenge_info.challenge_process_name#
			     <cfelse>
			     <img src="#media_virtual#/#challenge_info.challenge_process_image#" width=100 border=0 alt="#challenge_info.challenge_process_name#" title="#challenge_info.challenge_process_name#"></a>
			    </cfif>
			    </cfif>

			    </td>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120><cfif challenge_info.challenge_start is "">TBD<cfelse>#dateformat(challenge_info.challenge_start,'mm/dd/yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120><cfif challenge_info.challenge_end is "">TBD<cfelse>#dateformat(challenge_info.challenge_end,'mm/dd/yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=120>#dateformat(challenge_info.challenge_updated,'mm/dd/yyyy')#</td>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=120>

			     <cfif #challenge_info.challenge_status# is 1>
			      <img src="/images/icon_open.png" width=80>
			     <cfelseif challenge_info.challenge_status is 2>
			      <img src="/images/icon_closed.png" width=80>
			     <cfelseif challenge_info.challenge_status is 3>
			      <img src="/images/icon_pending.png" width=80>
			     </cfif>

			    </td>

             </tr>

          </table>

         </td></tr>

       </table>

</cfoutput>