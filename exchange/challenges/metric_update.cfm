<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="metric" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_metric
 where challenge_metric_challenge_id = #session.challenge_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Update Metrics</td>
            <td align=right class="feed_sub_header"><a href="/exchange/challenges/open.cfm">Return</td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

            <form action="metric_save.cfm" method="post">

            <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header" width=250>Experts & POC's Accessed</td>
				 <td><input class="input_text" type="number" name="challenge_metric_pocs_accessed" style="width: 100px;" value="#metric.challenge_metric_pocs_accessed#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=250>Transition to Program of Record</td>
				 <td>
				 <select name="challenge_metric_transition" class="input_select" style="width: 100px;">
				  <option value=0 <cfif #metric.challenge_metric_transition# is 0>selected</cfif>>No
				  <option value=1 <cfif #metric.challenge_metric_transition# is 1>selected</cfif>>Yes
				 </select>
				 </td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=250>Number of Consignments</td>
				 <td><input class="input_text" type="number" name="challenge_metric_pocs_consignments" style="width: 100px;" value="#metric.challenge_metric_consignments#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=250>Cross Collaboration</td>
				 <td>
				 <select name="challenge_metric_cross_werx" class="input_select" style="width: 100px;">
				  <option value=0 <cfif #metric.challenge_metric_cross_werx# is 0>selected</cfif>>No
				  <option value=1 <cfif #metric.challenge_metric_cross_werx# is 1>selected</cfif>>Yes
				 </select>
				 </td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=275>Number of Learnings</td>
				 <td><input class="input_text" type="number" name="challenge_metric_learnings" style="width: 100px;" value="#metric.challenge_metric_learnings#"></td>
		      </tr>


			  <tr>
				 <td class="feed_sub_header" width=250>Number of Companies Sourced</td>
				 <td><input class="input_text" type="number" name="challenge_metric_companies_sourced" style="width: 100px;" value="#metric.challenge_metric_companies_sourced#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=250>Days to Solve</td>
				 <td><input class="input_text" type="number" name="challenge_metric_speed_to_solve" style="width: 100px;" value="#metric.challenge_metric_speed_to_solve#"></td>
		      </tr>


	          <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=2>
                         <input type="submit" name="button" class="button_blue_large" value="Update Metrics">

		      </td></tr>

             </table>

             </cfoutput>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>