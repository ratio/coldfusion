<cfquery name="sourced" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select count(need_comp_id) as total from need_comp
 where need_comp_challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="social" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select distinct(recent_usr_id) as id from recent
 where recent_challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="usr_followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select count(follow_id) as total from follow
 where follow_challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="challenged" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select count(need_comp_id) as total from need_comp
 where need_comp_challenge_id = #session.challenge_id# and
       need_comp_contender = 1
</cfquery>

<cfquery name="questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select count(question_id) as total from question
 where question_challenge_id = #session.challenge_id#
</cfquery>

<div class="left_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header">Challenge Dashboard</td>
	      <td align=right></td></tr>
	  <tr><td colspan=2><hr></td></tr>
      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <cfoutput>

		  <tr>
			  <td class="feed_sub_header" align=center style="padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/challenges/interest_social.cfm" style="font-size: 28px;">#social.recordcount#</a></span></td>
			  <td class="feed_sub_header" align=center style="padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/challenges/interest_follow.cfm" style="font-size: 28px;">#usr_followers.total#</a></span></td>
			  <td class="feed_sub_header" align=center style="padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/challenges/interest_questions.cfm" style="font-size: 28px;">#questions.total#</a></span></td>
		  </tr>

		  <tr>
			  <td class="link_small_gray" align=center><a href="/exchange/challenges/interest_social.cfm">Viewed<br>Challenge</a></td>
			  <td class="link_small_gray" align=center><a href="/exchange/challenges/interest_follow.cfm">Expressed<br>Interest</a></td>
			  <td class="link_small_gray" align=center><a href="/exchange/challenges/interest_questions.cfm">Questions<br>Submitted</td>
		  </tr>

          <!---

		  <tr><td height=5></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr>
			  <td class="feed_sub_header" align=center style="padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/challenges/sourcing/" style="font-size: 28px;">#sourced.total#</a></td>
			  <td class="feed_sub_header" align=center style="padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/challenges/sourcing/" style="font-size: 28px;">#challenged.total#</a></span></td>
		  </tr>

		  <tr>
			  <td class="link_small_gray" align=center><a href="/exchange/challenges/sourcing/">CANIDATE<br>COMPANIES</td>
			  <td class="link_small_gray" align=center><a href="/exchange/challenges/sourcing/">COMPANIES<br>SELECTED</a></td>
		  </tr>

		  --->

	  </cfoutput>

	  </table>
	  </center>

</div>


