<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Add Update</td>
            <td align=right><a href="/exchange/challenges/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

            <form action="update_db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header">Title</td>
				 <td><input class="input_text" type="text" name="up_name" style="width: 600px;" required maxlength="500" placeholder="Please provide a title / purpose for this update."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Update</td>
				 <td><textarea class="input_textarea" name="up_desc" style="width: 900px; height: 200px;" placeholder="Please add the full details of the update."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="up_url" maxlength="1000" style="width: 900px;" placeholder="If required - http://www..."></td>
		      </tr>

              <tr><td class="feed_sub_header" valign=top>Attachment</td>
                    <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="up_attachment">

					 </td></tr>

                <tr><td class="feed_sub_header" valign=top>Post Online</td>
                    <td class="feed_sub_header" style="font-weight: normal;">
                    <select name="up_post" class="input_select">
                     <option value=0>No, keep hidden
                     <option value=1>Yes, allow others to see
                    <seect>

					 </td></tr>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=2>
                         <input type="submit" name="button" class="button_blue_large" value="Add Update">

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>