<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 60>
</cfif>

<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from challenge
 left join challenge_process on challenge_process.challenge_process_id = challenge.challenge_process_id
 left join hub on hub_id = challenge_hub_id
 where challenge_hub_id = #session.hub#

	<cfif #sv# is 1>
	 order by challenge_name ASC
	<cfelseif #sv# is 10>
	 order by challenge_name DESC
	<cfelseif #sv# is 2>
	 order by challenge_organization ASC
	<cfelseif #sv# is 20>
	 order by challenge_organization DESC
	<cfelseif #sv# is 3>
	 order by challenge_process_order ASC
	<cfelseif #sv# is 30>
	 order by challenge_process_order DESC
	<cfelseif #sv# is 4>
	 order by challenge_status ASC
	<cfelseif #sv# is 40>
	 order by challenge_status DESC
	<cfelseif #sv# is 5>
	 order by challenge_poc_name ASC
	<cfelseif #sv# is 50>
	 order by challenge_poc_name DESC
	<cfelseif #sv# is 6>
	 order by challenge_updated ASC
	<cfelseif #sv# is 60>
	 order by challenge_updated DESC
	<cfelseif #sv# is 7>
	 order by challenge_organization ASC
	<cfelseif #sv# is 70>
	 order by challenge_organization DESC
    </cfif>

</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="recent_challenges.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
			<td class="feed_header">Challenges</td>
			<td class="feed_sub_header" align=right><img src="/images/plus3.png" width=10 width=10 hspace=10 align=absmiddle><a href="/exchange/challenges/new.cfm">Create Challenge</a></td>
	    </tr>
	    <tr><td colspan=2><hr></td></tr>
	   </table>

        <cfinclude template="challenge_search.cfm">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	    <tr><td colspan=2><hr></td></tr>

        <cfif isdefined("u")>
         <cfif u is 3>
          <tr><td class="feed_sub_header" style="color: green;">Challenge has been successfully deleted.</td></tr>
         </cfif>
        </cfif>

       </table>

        <cfif challenges.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Challenges have been created.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
               <td></td>
               <td colspan=7>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
               <td class="feed_sub_header" width=300><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>">Challenge Name / Description</a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" width=200><a href="index.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>">Organization</a>&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" width=200><a href="index.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>">Exchange / Network</a>&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" width=100 align=center><a href="index.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>">Process</a>&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" width=100 align=center><a href="index.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>">Lead</a>&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" width=125 align=center><a href="index.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>">Updated</a>&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" width=120 align=right><a href="index.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>">Status</a>&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>


           </tr>

           <tr><td height=10></td></tr>

           </table>

           </td></tr>

           <cfloop query="challenges">

			   <cfoutput>

				   <tr>

                    <td width=120>

        			<table cellspacing=0 cellpadding=0 border=0 width=100%>
        			 <tr><td>

						<cfif challenges.challenge_image is "">
						  <a href="/exchange/challenges/set.cfm?i=#encrypt(challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/stock_challenge.png" width=100 border=0 style="margin-top: 20px;"></a>
						<cfelse>
						  <a href="/exchange/challenges/set.cfm?i=#encrypt(challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#challenges.challenge_image#" width=100 border=0 style="margin-top: 20px;"></a>
						</cfif>

						</td><tr>
        			</table>

			        </td><td>

			         <table cellspacing=0 cellpadding=0 border=0 width=100%>

                     <tr>
					   <td width=300 class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;" valign=middle><a href="/exchange/challenges/set.cfm?i=#encrypt(challenges.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#challenges.challenge_name#</b></a></td>
					   <td width=200 class="feed_sub_header" style="font-weight: normal;">#challenges.challenge_organization#</td>
					   <td width=200 class="feed_sub_header" style="font-weight: normal;">#challenges.hub_name#</td>

					   <td width=100 class="feed_sub_header" align=center style="font-weight: normal;">
					   <cfif #challenge_process_id# is 0>
					     Not Started
					   <cfelse>
					    <cfif #challenge_process_image# is "">
					     #challenge_process_name#
					    <cfelse>
					     <img src="#media_virtual#/#challenge_process_image#" width=80 align=center border=0 alt="#challenges.challenge_process_name#" title="#challenges.challenge_process_name#">
					    </cfif>
					   </cfif>
					   </td>

					   <td align=center width=100 class="feed_sub_header" style="font-weight: normal;"><cfif #challenge_poc_name# is "">TBD<cfelse>#challenge_poc_name#</cfif></td>
					   <td align=center width=125 class="feed_sub_header" style="font-weight: normal;">#dateformat(challenge_updated,'mm/dd/yyyy')#</td>

						<td class="feed_sub_header" align=right width=120>
						 <cfif #challenge_status# is 1>
						  <img src="/images/icon_open.png" width=80>
						 <cfelseif challenge_status is 2>
						  <img src="/images/icon_closed.png" width=80>
						 <cfelseif challenge_status is 3>
						  <img src="/images/icon_pending.png" width=80>
						 </cfif></td>

					   </tr>

					   <tr>
					       <td colspan=6 class="feed_sub_header" style="font-weight: normal; margin-top: 0px; padding-top: 0px;">#challenges.challenge_desc#</td></tr>

					   <tr><td colspan=6 class="link_small_gray">#challenges.challenge_keywords#</td></tr>
				</table>

				</td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=2><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>