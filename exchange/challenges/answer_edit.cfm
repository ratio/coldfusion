<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from question
 where question_id = #question_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

      </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>ANSWER QUESTION</td>
            <td valign=top align=right><a href="/exchange/challenges/interest_questions.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

       <form action="question_db.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfoutput>
         <tr><td class="feed_sub_header">QUESTION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;"><b>#ucase(questions.question_subject)#</b><br>#questions.question_text#</td></tr>

         <tr><td class="feed_sub_header">ANSWER</td></tr>
         <tr><td><textarea name="question_answer" class="input_textarea" style="width: 1100px; height: 200px;"></textarea></td></tr>

         <tr><td class="feed_sub_header">DISPLAY PUBLICLY?</td></tr>

         <tr><td>
             <select name="question_public" class="input_select">
              <option value=0>No, keep private
              <option value=1>Yes, display publicly
             </select>

             </td></tr>

         <tr><td height=20></td></tr>

         <input type="hidden" name="question_id" value=#question_id#>

         <tr><td><input type="submit" name="button" value="Save" class="button_blue_large"></td></tr>

        </cfoutput>


  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>