<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="cquestions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from question
 left join usr on usr_id = question_usr_id
 where question_challenge_id = #session.challenge_id#
 order by question_date_submitted DESC
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/challenges/challenge_activity.cfm">

       </td><td valign=top>

       <div class="main_box">
        <cfinclude template="challenge_header.cfm">
       </div>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Questions & Answers</td>
            <td valign=top align=right class="feed_sub_header">

            <img src="/images/plus3.png" alt="Add Question" title="Add Question" width=15 border=0 hspace=10><a href="/exchange/challenges/question_add.cfm">Add Question</a>

            </td></tr>

        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif cquestions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been asked.</td><tr>
        <cfelse>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;" colspan=2>Question has been successfully added.</td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_sub_header" style="color: green;" colspan=2>Question has been successfully updated.</td></tr>
          <cfelseif u is 3>
           <tr><td class="feed_sub_header" style="color: green;" colspan=2>Question has been successfully deleted.</td></tr>
          </cfif>
        </cfif>


        <cfoutput query="cquestions">

         <tr>
			 <cfif #usr_photo# is "">
			  <td width=75><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(cquestions.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/headshot.png" height=50 width=50 border=0></td>
			 <cfelse>
			  <td width=75><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(cquestions.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" src="#media_virtual#/#usr_photo#" height=50 width=50 border=0></td>
			 </cfif>

         <td class="feed_sub_header" style="font-weight: normal;"><b>#cquestions.usr_first_name# #cquestions.usr_last_name#</b><br>#dateformat(cquestions.question_date_submitted,'mmm d, yyyy')# at #timeformat(cquestions.question_date_submitted,'short')#</td>
             <td class="feed_sub_header" align=right style="font-weight: normal;">
             <a href="question_update.cfm?i=#encrypt(cquestions.question_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
             <img src="/images/icon_edit.png" width=20 hspace=10></a>
             <a href="question_update.cfm?i=#encrypt(cquestions.question_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Update</a>
             &nbsp;|&nbsp;
             <b><cfif cquestions.question_public is "" or cquestions.question_public is 0>Private<cfelse>Public</cfif></b></td>

             </tr>
         <tr><td></td><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>#cquestions.question_subject#</b><br>#replace(cquestions.question_text,"#chr(10)#","<br>","all")#</td></tr>

         <tr><td></td><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>Answer</b><br>

         <cfif cquestions.question_answer is "">
          Not Answered.
         <cfelse>
          #replace(cquestions.question_answer,"#chr(10)#","<br>","all")#
         </cfif>

         </td></tr>

        <tr><td colspan=3><hr></td></tr>
        <tr><td height=10></td></tr>

        </cfoutput>
        </cfif>






  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>