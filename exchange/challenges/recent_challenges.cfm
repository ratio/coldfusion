<div class="left_box">

		  <cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="10">
		   select distinct(challenge_id), challenge_name, max(recent_date), cast(challenge_desc as varchar(max)) challenge_desc, challenge_image from recent
		   join challenge on challenge_id = recent_challenge_id
		   where recent_usr_id = #session.usr_id# and
		   recent_challenge_id is not null and
		   challenge_hub_id = #session.hub#
		   group by challenge_id, challenge_name, challenge_image, cast(challenge_desc as varchar(max))
		   order by max(recent_date) DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><a href="/exchange/portfolio"><b>Recently Viewed</b></a></td>
		      <td align=right valign=top></td></tr>
		  <tr><td><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif recent.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Challengess have been viewed.</td></tr>
          <cfelse>

          <tr><td height=10></td></tr>

          <cfset count = 1>

		  <cfoutput query="recent">

			   <tr>
				   <td width=30><a href="/exchange/challenges/set.cfm?i=#encrypt(recent.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

				   <cfif #recent.challenge_image# is "">
					 <img src="#image_virtual#/stock_challenge.png" width=18 border=0>
				   <cfelse>
					 <img src="#media_virtual#/#recent.challenge_image#" width=18>
				   </cfif>
				   </a></td>

				   <td class="link_med_blue"><a href="/exchange/challenges/set.cfm?i=#encrypt(recent.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(recent.challenge_name) GT 20>#left(recent.challenge_name,'20')#...<cfelse>#recent.challenge_name#</cfif></a></td>
				</tr>

			   <tr><td></td><td class="link_small_gray" style="padding-left: 0px;" colspan=2>

                  <cfif len(recent.challenge_desc) GT 50>
                   #left(recent.challenge_desc,50)#...
                  <cfelse>
                   #recent.challenge_desc#
                  </cfif>

			   </td></tr>

			   <cfif count LT recent.recordcount>
			   	<tr><td colspan=2><hr></td></tr>
			   </cfif>

			   <cfset count = count + 1>

			  </cfoutput>

          </cfif>

	</center>

	</table>

</div>