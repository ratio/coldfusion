<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Learning">

	<cftransaction>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 insert into challenge_learn
	 (
	  challenge_learn_name,
	  challenge_learn_desc,
	  challenge_learn_updated,
	  challenge_learn_challenge_id,
	  challenge_learn_usr_id
	  )
	 values (
	 '#challenge_learn_name#',
	 '#challenge_learn_desc#',
	  #now()#,
	  #session.challenge_id#,
	  #session.usr_id#
	  )
	</cfquery>

	<cflocation URL="open.cfm?u=71" addtoken="no">

<cfelseif button is "Update">


	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 update challenge_learn
	 set challenge_learn_name = '#challenge_learn_name#',
	     challenge_learn_desc = '#challenge_learn_desc#',
	     challenge_learn_updated = #now()#
	 where challenge_learn_id = #challenge_learn_id#
	</cfquery>

    <cflocation URL="open.cfm?u=72" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 delete challenge_learn
	 where challenge_learn_id = #challenge_learn_id#
	</cfquery>

    <cflocation URL="open.cfm?u=73" addtoken="no">

</cfif>
