<cfinclude template="/exchange/security/check.cfm">

<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select follow_company_id from follow
 where follow_by_usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 	  <div class="exchange_filter_box">

	      <cfinclude template="search_companies.cfm">

	  </div>

      <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Exchange Companies and Organizations</td>
	           <td align=right class="feed_option">

	           </td>
           </tr>

           <tr><td>&nbsp;</td></tr>

          </table>

	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>