<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>
<cfset #session.search_id# = 0>
<cfset #session.network_all# = "Yes">

<cfif not isdefined("session.comp_view")>
 <cfset session.comp_view = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}

.partner_badge {
    width: 31%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 150px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.partner_badge_title {
    font-family: calibri, arial;
    font-size: 14px;
    padding-bottom: 10px;
    color: 000000;
    font-weight: bold;
}
.partner_badge_text {
    font-family: calibri, arial;
    font-size: 12px;
    color: 000000;
}

</style>

<cfquery name="partner_tiers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_tier
 where partner_tier_hub_id = #session.hub#
 order by partner_tier_order
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="partner_programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_program
 where partner_program_hub_id = #session.hub#
 order by partner_program_order
</cfquery>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
		  <!--- <cfinclude template="/exchange/marketplace/recent.cfm"> --->
	      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

		<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select search_id, search_name from search
		 where search_usr_id = #session.usr_id#
		 order by search_name
		</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td colspan=2 class="feed_header"><a href="network_in.cfm">In Network Companies</a></td></tr>
		<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">In Network Companies are organizations that have been included in this Exchange.</td></tr>
		<tr><td colspan=2><hr></td></tr>

		<tr>

        <form action="network_in_set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
			<td>

			   <span class="feed_sub_header">Search for Company&nbsp;&nbsp;</span>

				&nbsp;

			   <select name="search_type" class="input_select" style="width: 150px;">
				 <option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>By Name
				 <option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>By Capability
				</select>

				<input class="input_text" type="text" style="width: 250px;" placeholder = "keyword" name="keyword" <cfif isdefined("session.keyword")>value="<cfoutput>#replace(session.keyword,'"','','all')#</cfoutput>"</cfif>>

			   <select name="search_tier" class="input_select" style="width: 150px;">
			   <option value=0>All Companies
			   <cfoutput query="partner_tiers">
			     <option value=#partner_tier_id# <cfif isdefined("session.search_tier") and session.search_tier is #partner_tiers.partner_tier_id#>selected</cfif>>#partner_tier_name#
			   </cfoutput>
				</select>

			   <select name="search_program" class="input_select" style="width: 200px;">
			   <option value=0>All Companies
			   <cfoutput query="partner_programs">
			     <option value=#partner_program_id# <cfif isdefined("session.search_tier") and session.search_program is #partner_programs.partner_program_id#>selected</cfif>>#partner_program_name#
			   </cfoutput>
				</select>

			    <select name="search_state" class="input_select">
				 <option value=0>All States
				 <cfoutput query="states">
				  <option value="#state_abbr#" <cfif isdefined("session.search_state") and session.search_state is state_abbr>selected</cfif>>#state_name#
				 </cfoutput>
				</select>

				<input class="button_blue" type="submit" name="button" value="Search">

			</td>
        </form>

        </tr>

</table>

		</div>

		<div class="main_box">

			<cfquery name="comp_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select hub_comp_company_id from hub_comp
			 where hub_comp_hub_id = #session.hub#
		    </cfquery>

            <cfif comp_list.recordcount is 0>
             <cfset #clist# = 0>
            <cfelse>
             <cfset #clist# = #valuelist(comp_list.hub_comp_company_id)#>
            </cfif>

            <cfset listcount = listlen(clist)>
            <cfset counter = 1>

			<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
             select * from company
             where company_id in (#clist#)
             order by company_name
            </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td height=10></td></tr>

              <tr><td class="feed_header">All Companies</td>
                  <td class="feed_sub_header" align=right>
                  <a href="network_in.cfm"><img src="/images/icon_align.png" width=20 hspace=5 alt="Recently Viewed" valign=top title="Recently Viewed" border=0></a>
                  <a href="network_in.cfm">Recently Viewed</a></td></tr>
              <tr><td colspan=2><hr></td></tr>

			 <cfif companies.recordcount is 0>

               <tr><td class="feed_sub_header" style="font-weight: normal;">No In Network Companies have been identified.</td></tr>

			 <cfelse>


              <tr><td height=20></td></tr>
			  <tr><td valign=top colspan=2>

              <p>
				 <cfloop query="companies">

				  <div class="partner_badge">
				   <cfinclude template="company_badge.cfm">
				  </div>

				 </cfloop>

			 </cfif>

			</td></tr>

		 </table>
		 </center>

	  </div>

      </td>

      </tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>