<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>
<cfset #session.search_id# = 0>

<cfif not isdefined("session.comp_view")>
 <cfset session.comp_view = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}

.partner_badge {
    width: 31%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 150px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.partner_badge_title {
    font-family: calibri, arial;
    font-size: 14px;
    padding-bottom: 10px;
    color: 000000;
    font-weight: bold;
}
.partner_badge_text {
    font-family: calibri, arial;
    font-size: 12px;
    color: 000000;
}

</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

		<div class="main_box">

		   <cfif session.comp_view is 1>

		   <cfquery name="clist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select hub_comp_company_id as company_id from hub_comp
		    where hub_comp_hub_id = #session.hub#
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 2>

		   <cfquery name="clist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select follow_company_id as company_id from follow
		    where follow_by_usr_id = #session.usr_id# and
		          follow_hub_id = #session.hub#
		   </cfquery>

		   <cfelseif session.comp_view is 3>

			<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select hub_id from hub
			 where hub_parent_hub_id = #session.hub#
			</cfquery>

			<cfif child.recordcount is 0>
			 <cfset child_list = 0>
			<cfelse>
			 <cfset child_list = #valuelist(child.hub_id)#>
			</cfif>

		   <cfquery name="clist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select hub_comp_company_id as company_id from hub_comp
		    join company on company_id = hub_comp_company_id
            where hub_comp_hub_id = #session.hub# or hub_comp_hub_id in (#child_list#)
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 4>

    	   <cfquery name="clist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select distinct(portfolio_item_company_id) as company_id from portfolio_item
		    where portfolio_item_usr_id = #session.usr_id# and
		    portfolio_item_company_id is not null
		   </cfquery>

		   <cfif clist.recordcount is 0>
		    <cfset comp_list = 0>
		   <cfelse>
		    <cfset comp_list = valuelist(clist.company_id)>
		   </cfif>


		   <cfquery name="clist" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		    select company_id from company
		    where company_id in (#comp_list#)
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 5>

		   <cfquery name="clist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select featured_company_id as company_id from featured
			 where featured_active = 1
		     and featured_hub_id = #session.hub#
			 order by featured_order ASC
           </cfquery>

		   </cfif>

		   <cfif clist.recordcount is 0>
		    <cfset company_list = 0>
		   <cfelse>
		    <cfset company_list = valuelist(clist.company_id)>
		   </cfif>

		   <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
             select * from company
             where company_id in (#company_list#)
             order by company_name
           </cfquery>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <cfoutput>
			   <tr>

			       <form action="refresh_comp_view.cfm" method="post">
			       <td class="feed_header">Companies</td>

				   <td align=right>
				   <span class="feed_sub_header">Change View</span>&nbsp;
				   <select name="comp_view" class="input_select" onchange='if(this.value != 0) { this.form.submit(); }'>
				    <option value=2 <cfif session.comp_view is 2>selected</cfif>>Companies I Follow
				    <option value=4 <cfif session.comp_view is 4>selected</cfif>>Companies in My Portfolio(s)
				   </select>

				   </td>
			   </tr>
               <tr><td height=10></td></tr>
               <tr><td colspan=2><hr></td></tr>

               <tr><td class="feed_sub_header">

			       <cfif session.comp_view is 1>
			        Favorite Companies
			       <cfelseif session.comp_view is 2>
			        Companies I Follow
			       <cfelseif session.comp_view is 3>
			        Companies in this Network
			       <cfelseif session.comp_view is 4>
			        Companies in my Portfolio(s)
			       <cfelseif session.comp_view is 5>
			        Featured Companies
			       </cfif> <cfif #companies.recordcount# GT 0>(#companies.recordcount#)</cfif>

               </td></tr>

    		   </cfoutput>
			  </table>

			 <cfif companies.recordcount is 0>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies were found.</td></tr>
              </table>

			 <cfelse>
             <p>
				 <cfloop query="companies">

				  <div class="partner_badge">
				   <cfinclude template="/exchange/marketplace/companies/company_badge.cfm">
				  </div>

				 </cfloop>

			 </cfif>

			</td></tr>

		 </table>
		 </center>

	  </div>

      </td>

      </tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>