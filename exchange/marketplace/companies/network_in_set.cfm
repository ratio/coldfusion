<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Clear">

	<cfset StructDelete(Session,"search_program")>
	<cfset StructDelete(Session,"keyword")>
	<cfset StructDelete(Session,"search_tier")>
	<cfset StructDelete(Session,"search_type")>
	<cfset StructDelete(Session,"search_state")>

	<cflocation URL="network_in.cfm" addtoken="no">

<cfelseif button is "Search">

	<cfset search_string = #replace(keyword,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'(','',"all")#>
	<cfset search_string = #replace(search_string,')','',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
	<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	<cfset search_string = #replace(search_string,'"(','("',"all")#>
	<cfset search_string = #replace(search_string,')"','")',"all")#>
	<cfset #session.keyword# = "#search_string#">

    <cfset #session.search_program# = #search_program#>
    <cfset #session.search_tier# = #search_tier#>
	<cfset #session.search_type# = #search_type#>

    <cfif search_state is not 0>
     <cfset session.search_state = '#search_state#'>
    </cfif>

	<cfif #session.search_type# is 1>
	 <cflocation URL="network_in_results.cfm" addtoken="no">
	<cfelse>
	 <cflocation URL="network_in_capability.cfm" addtoken="no">
	</cfif>

<cfelse>

 <cfif isdefined("session.add_filter")>

  <cfset session.add_filter = #listappend(session.add_filter,"#add_filter#")#>

 <cfelse>

  <cfset session.add_filter = "#add_filter#">

 </cfif>

 <cflocation URL="network_in_capability.cfm" addtoken="no">

</cfif>
