<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>
<cfset #session.search_id# = 0>

<cfif not isdefined("session.comp_view")>
 <cfset session.comp_view = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}

.partner_badge {
    width: 31%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 325px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.partner_badge_title {
    font-family: calibri, arial;
    font-size: 14px;
    padding-bottom: 10px;
    color: 000000;
    font-weight: bold;
}
.partner_badge_text {
    font-family: calibri, arial;
    font-size: 12px;
    color: 000000;
}

</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
		  <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

		<div class="main_box">

		   <cfif session.comp_view is 1>

		   <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select * from hub_comp
		    join company on company_id = hub_comp_company_id
		    where hub_comp_hub_id = #session.hub#
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 2>

		   <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select * from follow
		    join company on company_id = follow_company_id
		    where follow_by_usr_id = #session.usr_id#
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 3>

			<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select hub_id from hub
			 where hub_parent_hub_id = #session.hub#
			</cfquery>

			<cfif child.recordcount is 0>
			 <cfset child_list = 0>
			<cfelse>
			 <cfset child_list = #valuelist(child.hub_id)#>
			</cfif>

		   <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select * from hub_comp
		    join company on company_id = hub_comp_company_id
            where hub_comp_hub_id = #session.hub# or hub_comp_hub_id in (#child_list#)
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 4>

		   <cfquery name="port_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select distinct(portfolio_item_company_id) from portfolio_item
		    where portfolio_item_usr_id = #session.usr_id# and
		    portfolio_item_company_id is not null
		   </cfquery>

		   <cfif port_list.recordcount is 0>
		    <cfset comp_list = 0>
		   <cfelse>
		    <cfset comp_list = valuelist(port_list.portfolio_item_company_id)>
		   </cfif>


		   <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select * from company
		    where company_id in (#comp_list#)
		    order by company_name
		   </cfquery>

		   <cfelseif session.comp_view is 5>

		   <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from featured
			 join company on company_id = featured_company_id
			 where featured_active = 1
		     and featured_hub_id = #session.hub#
			 order by featured_order ASC
           </cfquery>

		   </cfif>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <cfoutput>
			   <tr>

			       <form action="refresh_comp_view.cfm" method="post">
			       <td class="feed_header">

			       <cfif session.comp_view is 1>
			        FAVORITE PARTNERS
			       <cfelseif session.comp_view is 2>
			        PARTNERS I FOLLOW
			       <cfelseif session.comp_view is 3>
			        COMPANIES IN THIS NETWORK
			       <cfelseif session.comp_view is 4>
			        PARTNERS IN MY PORTFOLIO(S)
			       <cfelseif session.comp_view is 5>
			        FEATURED PARTNERS
			       </cfif> <cfif #companies.recordcount# GT 0>(#companies.recordcount#)</cfif></td>

				   <td align=right>
				   <span class="feed_sub_header">Change View</span>&nbsp;
				   <select name="comp_view" class="input_select" onchange='if(this.value != 0) { this.form.submit(); }'>
				    <option value=2 <cfif session.comp_view is 2>selected</cfif>>Partners I Follow
				    <option value=4 <cfif session.comp_view is 4>selected</cfif>>Partners in My Portfolio(s)
				   </select>

				   </td>
			   </tr>
               <tr><td height=10></td></tr>
               <tr><td colspan=2><hr></td></tr>
    		   </cfoutput>
			  </table>

			 <cfif companies.recordcount is 0>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header" style="font-weight: normal;">No Partners were found.</td></tr>
              </table>

			 <cfelse>
             <p>
				 <cfloop query="companies">

				  <div class="partner_badge">
				   <cfinclude template="/exchange/marketplace/company_badge.cfm">
				  </div>

				 </cfloop>

			 </cfif>

			</td></tr>

		 </table>
		 </center>

	  </div>

      </td>

      </tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>