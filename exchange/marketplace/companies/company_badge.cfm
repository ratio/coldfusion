<cfquery name="products" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(product_id) as total from product
 where product_company_id = #companies.company_id#
</cfquery>

<cfquery name="customers" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(customer_id) as total from customer
 where customer_company_id = #companies.company_id#
</cfquery>

<cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(follow_id) as total from follow
 where follow_company_id = #companies.company_id# and
       follow_hub_id = #session.hub#
</cfquery>

<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select follow_id from follow
 where follow_company_id = #companies.company_id# and
	   follow_by_usr_id = #session.usr_id# and
	   follow_hub_id = #session.hub#
</cfquery>

<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(company_intel_id) as total from company_intel
 left join comments_rating on comments_rating_value = company_intel_rating
 left join usr on usr_id = company_intel.company_intel_created_by_usr_id
 where company_intel_company_id = #companies.company_id# and
       company_intel_hub_id = #session.hub# and
       (company_intel_created_by_usr_id = #session.usr_id# or company_intel_created_by_company_id = #session.company_id#)
</cfquery>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select portfolio_id from portfolio
 left join portfolio_item on portfolio_item_portfolio_id = portfolio_id
 left join usr on usr_id = portfolio_usr_id
 where portfolio_usr_id = #session.usr_id# and
	   portfolio_company_id = #session.company_id# and
	   portfolio_type_id = 1 and
	   portfolio_item_company_id = #companies.company_id#
</cfquery>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfoutput>
		   <tr><td width=90 height=75 valign=middle>

		   <cfif company_long_desc is "">
		    <cfset #desc# = "No Description Found">
		   <cfelse>
		    <cfset #desc# = #company_long_desc#>
		   </cfif>

			<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer" alt="#desc#" title="#desc#">
			<cfif companies.company_logo is "">
			  <img src="//logo.clearbit.com/#companies.company_website#" width=70 border=0 onerror="this.src='/images/no_logo.png'">
			<cfelse>
			  <img src="#media_virtual#/#companies.company_logo#" width=70 border=0>
			</cfif>
			</a>

			</td><td class="feed_title" valign=middle style="padding-right: 0px; margin-right: 0px;">

			<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer" style="padding-right: 0px; margin-right: 0px;">
			<cfif len(company_name) GT 50>#ucase(left(company_name,50))#...<cfelse>#ucase(company_name)#</cfif></a>
			<br>
			<span class="feed_option">

			<cfif #companies.company_city# is "" or #companies.company_state# is "">

			 <cfif #companies.company_city# is "">
			  #ucase(companies.company_state)#
			 <cfelse>
			  #ucase(companies.company_state)#
			 </cfif>

			<cfelse>
			 <b>#ucase(companies.company_city)#, #ucase(companies.company_state)#</b>
			</cfif>
			</span>

			</td>
			   <td align=right valign=top width=65>

			   <cfif #follow.recordcount# is 1>
				<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/small_follow.png" width=20 alt="You are followign this Company" title="You are following this Company" border=0 align=middle></a>
			   </cfif>

			   &nbsp;<img src="/images/plus3.png" style="cursor: pointer;" width=18 align=middle alt="Add to Portfolio" title="Add to Portfolio" onclick="window.open('/exchange/include/save_company.cfm?id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=525'); return false;">

			   </td></tr>

		   <tr><td colspan=3><hr></td></tr>

		   <tr><td class="feed_option" colspan=3>Followers <b>#followers.total#</b> | Products <b>#products.total#</b> | Customers <b>#customers.total#</b> | Comments <b>#intel.total#</b></td></tr>
	   </cfoutput>

	</table>

