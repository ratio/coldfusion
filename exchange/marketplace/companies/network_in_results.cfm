<cfinclude template="/exchange/security/check.cfm">
<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="partner_tiers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_tier
 where partner_tier_hub_id = #session.hub#
 order by partner_tier_order
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="partner_programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_program
 where partner_program_hub_id = #session.hub#
 order by partner_program_order
</cfquery>

<cfset perpage = 100>

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
		  <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

		<div class="main_box">


<table cellspacing=0 cellpadding=0 border=0 width=100%>


		<tr><td colspan=2 class="feed_header"><a href="network_in.cfm">In Network Companies</a></td></tr>
		<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">In Network Companies are organizations that have been included in this Exchange.</td></tr>
		<tr><td colspan=2><hr></td></tr>

		<tr>

        <form action="network_in_set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
			<td>

			   <span class="feed_sub_header">Search for Company&nbsp;&nbsp;</span>

				&nbsp;

			   <select name="search_type" class="input_select" style="width: 150px;">
				 <option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>By Name
				 <option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>By Capability
				</select>

				<input class="input_text" type="text" style="width: 250px;" placeholder = "keyword" name="keyword" <cfif isdefined("session.keyword")>value="<cfoutput>#replace(session.keyword,'"','','all')#</cfoutput>"</cfif>>

			   <select name="search_tier" class="input_select" style="width: 150px;">
			   <option value=0>All Companies
			   <cfoutput query="partner_tiers">
			     <option value=#partner_tier_id# <cfif isdefined("session.search_tier") and session.search_tier is #partner_tiers.partner_tier_id#>selected</cfif>>#partner_tier_name#
			   </cfoutput>
				</select>

			   <select name="search_program" class="input_select" style="width: 200px;">
			   <option value=0>All Companies
			   <cfoutput query="partner_programs">
			     <option value=#partner_program_id# <cfif isdefined("session.search_tier") and session.search_program is #partner_programs.partner_program_id#>selected</cfif>>#partner_program_name#
			   </cfoutput>
				</select>

			    <select name="search_state" class="input_select">
				 <option value=0>All States
				 <cfoutput query="states">
				  <option value="#state_abbr#" <cfif isdefined("session.search_state") and session.search_state is state_abbr>selected</cfif>>#state_name#
				 </cfoutput>
				</select>

				<input class="button_blue" type="submit" name="button" value="Search">&nbsp;
				<input class="button_blue" type="submit" name="button" value="Clear">
			</td>
        </form>

        </tr>

</table>

		</div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #session.search_program# is 0>

				<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select hub_comp_company_id from hub_comp
				 left join company_extend on company_extend_company_id = hub_comp_company_id
				 where hub_comp_hub_id = #session.hub#

				 <cfif session.search_tier is not 0>
				  and company_extend_tier_id = #session.search_tier#
				 </cfif>

			    </cfquery>

            <cfelse>

				<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select partner_program_xref_partner_id from partner_program_xref
				 where partner_program_xref_hub_id = #session.hub# and
					   partner_program_xref_program_id = #session.search_program#
				</cfquery>

				<cfif programs.recordcount is 0>
				 <cfset program_list = 0>
				<cfelse>
				 <cfset program_list = valuelist(programs.partner_program_xref_partner_id)>
				</cfif>

				<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select hub_comp_company_id from hub_comp
				 left join company_extend on company_extend_company_id = hub_comp_company_id
				 where hub_comp_hub_id = #session.hub# and
					   hub_comp_company_id in (#program_list#)

				 <cfif session.search_tier is not 0>
				  and company_extend_tier_id = #session.search_tier#
				 </cfif>

				</cfquery>

            </cfif>

			<cfif list.recordcount is 0>
			 <cfset comp_list = 0>
			<cfelse>
			 <cfset comp_list = valuelist(list.hub_comp_company_id)>
			</cfif>

  				<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

				select company_name, company_keywords, company_logo, company_website, company_id, company_duns, company_city, company_zip, company_state,
				((select isnull(sum(subaward_amount),0) from award_data_sub table1 where table1.subawardee_duns = table3.company_duns) +
				 (select isnull(sum(federal_action_obligation),0) from award_data table2 where table2.recipient_duns = table3.company_duns) +
				 (select isnull(sum(award_amount),0) from sbir table4 where table4.duns = table3.company_duns and table4.duns <> '')
				 ) as total
				 from company table3
				 where (company_discoverable is null or company_discoverable = 1) and
				 company_id in (#comp_list#)

				 <cfif #session.keyword# is not '""'>
				  and (contains((company_name, company_duns),'#trim(session.keyword)#'))
				 </cfif>

				<cfif session.search_state is not 0>
				 and company_state = '#session.search_state#'
				</cfif>

				group by company_name, company_keywords, company_duns, company_website, company_id, company_logo, company_city, company_state, company_zip

		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by company_name DESC
							<cfelseif #sv# is 10>
							 order by company_name ASC
							<cfelseif #sv# is 2>
							 order by company_city ASC
							<cfelseif #sv# is 20>
							 order by company_city DESC
							<cfelseif #sv# is 3>
							 order by company_state ASC
							<cfelseif #sv# is 30>
							 order by company_state DESC
							<cfelseif #sv# is 4>
							 order by company_zip ASC
							<cfelseif #sv# is 40>
							 order by company_zip DESC
							<cfelseif #sv# is 6>
							 order by company_duns ASC
							<cfelseif #sv# is 60>
							 order by company_duns DESC
							<cfelseif #sv# is 7>
							 order by total DESC
							<cfelseif #sv# is 70>
							 order by total ASC
							<cfelseif #sv# is 8>
							 order by total DESC
							<cfelseif #sv# is 80>
							 order by total ASC
                            </cfif>

                         <cfelse>

                           <!--- order by name ASC --->

                         </cfif>

				</cfquery>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(companies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>


           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>

           <cfoutput>
           		<tr><td class="feed_header">Search Results

           		<cfif companies.recordcount is 0>

           		<cfelseif companies.recordcount is 1>
           		( 1 Company )
           		<cfelse>
           		(#trim(numberformat(companies.recordcount,'999,999,999'))# Companies)
           		</cfif>
           		</td>
           		<td align=right class="feed_sub_header">

                <cfif companies.recordcount GT 0>
           			<a href="/exchange/marketplace/partners/network_results.cfm?export=1"><img src="/images/icon_export_excel.png" hspace=10 alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>
           			<a href="/exchange/marketplace/partners/network_results.cfm?export=1">Export to Excel</a>
           		</cfif>

           		</td></tr>
           		<tr><td colspan=2><hr></td></tr>
           		<tr><td class="feed_sub_header">

           		<cfif session.keyword is not '""'>
           		<b>You searched for companies matching <i>"#replace(session.keyword,'"','','all')#"</i></b>
           		</cfif>

           		</td>


				    <td class="feed_sub_header" align=right>

                    <cfif companies.recordcount GT 0>

				    Page -&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

					</cfif>

				</td>

           		</tr>
           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header"><a href="network_in_results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
                    <td class="feed_sub_header">Keywords</td>
                    <td class="feed_sub_header"><a href="network_in_results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
                    <td class="feed_sub_header">Website</td>
                    <td class="feed_sub_header"><a href="network_in_results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
                    <td class="feed_sub_header" align=center><a href="network_in_results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
                    <td class="feed_sub_header" align=right width=100><a href="network_in_results.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Federal Awards</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				</tr>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #count# = 1>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

				<tr>

					<td class="feed_option">

                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40>
					</cfif>

					</td>
					<td class="feed_sub_header" width=200><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option" width=350>#ucase(companies.company_keywords)#</td>
					<td class="feed_option" width=100><cfif companies.company_duns is "">Unknown<cfelse>#ucase(companies.company_duns)#</cfif></a></td>
					<td class="feed_option"><a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer"><cfif len(companies.company_website) GT 40>#ucase(left(companies.company_website,40))#...<cfelse>#ucase(companies.company_website)#</cfif></a></td>
					<td class="feed_option" width=100>#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
<!---					<td class="feed_option" align=center>#companies.company_zip#</a></td> --->
					<td class="feed_option" width=120 align=right><b>#numberformat(companies.total,'$999,999,999,999')#</b></td>
				</tr>

				  <cfif count is not companies.recordcount>
				   <tr><td colspan=8><hr></td></tr>
				  <cfelse>
				   <tr><td height=5></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

            <tr><td colspan=8>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td height=10></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
			  <tr><td class="link_small_gray" colspan=2>The Exchange collects and integrates information about companies from multiple sources.  As a result, duplicate companies may exist.</td></tr>
			  <tr><td height=15></td></tr>

			  <tr><td width=60>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td width=100><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/cb_logo.png" width=40 alt="Crunchbase" title="Crunchbase"></a></td></tr>
				  </table>

				  </td><td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b>Crunchbase</b></a></td><td align=right class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.crunchbase.com</u></b></a></td></tr>
				   <tr><td class="link_small_gray" colspan=2>The Exchange uses Crunchbase information, specifically company names, tags, descriptions and keywords, to assist users on searching for companies.</td></tr>
				  </table>

				  </td></tr>

			</table>

            </td></tr>

			<cfelse>

			<tr><td class="feed_sub_header" style="font-weight: normal;">No companies were found.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>

		  </table>

	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>