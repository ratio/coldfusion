<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

<style>
/* Style The Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  right: 0;
  font-size: 13px;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
</style>

<cfset perpage = 100>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
		  <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="search_companies_network.cfm">
		</div>

      <div class="main_box">

		  <cfquery name="in_network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select hub_comp_company_id from hub_comp
		   where hub_comp_hub_id = #session.hub#
		  </cfquery>

		  <cfif in_network.recordcount is 0>
		   <cfset in_network_list = 0>
		  <cfelse>
		   <cfset in_network_list = valuelist(in_network.hub_comp_company_id)>
		  </cfif>

  			<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

                <cfif session.search_socio is 0>

					select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company

					<cfif isdefined("session.add_filter")>
					 where contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'#trim(session.keyword)#')
					 <cfloop index="i" list="#session.add_filter#">
					   <cfoutput>
					   and contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(i)#"')
					   </cfoutput>
					 </cfloop>
					<cfelse>
					 where contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'#trim(session.keyword)#')
					</cfif>

					<cfif session.search_state is not 0>
					 and company.company_state = '#session.search_state#'
					</cfif>

					union

                </cfif>

				select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company
				join award_data on recipient_duns = company_duns
                join sams on duns = company_duns

                <cfif isdefined("session.add_filter")>
				   where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'#trim(session.keyword)#')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				    and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'#trim(session.keyword)#')
                </cfif>

                <cfif session.search_socio is not 0>

                 <cfif session.search_socio is "8a">
                   and sba_bus_type_string like '%A6%'
                 <cfelse>
                   and biz_type_string like '%#session.search_socio#%'
                 </cfif>

                </cfif>


				<cfif session.search_state is not 0>
				 and company.company_state = '#session.search_state#'
				</cfif>

 			    union

				select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company
				join sbir on sbir.duns = company_duns
                join sams on sams.duns = company_duns
                <cfif isdefined("session.add_filter")>
				  where contains((award_title, abstract, research_keywords),'#trim(session.keyword)#')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				     and contains((award_title, abstract, research_keywords),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				where contains((award_title, abstract, research_keywords),'#trim(session.keyword)#')
                </cfif>

               and (sbir.duns <> '' or company_duns <> '') and
               company.company_duns is not null

                <cfif session.search_socio is not 0>

                 <cfif session.search_socio is "8a">
                   and sba_bus_type_string like '%A6%'
                 <cfelse>
                   and biz_type_string like '%#session.search_socio#%'
                 </cfif>

                </cfif>

					<cfif session.search_state is not 0>
					 and company.company_state = '#session.search_state#'
					</cfif>

				 <cfif isdefined("sv")>

					<cfif #sv# is 1>
					 order by company_name DESC
					<cfelseif #sv# is 10>
					 order by company_name ASC
					<cfelseif #sv# is 2>
					 order by company_city ASC
					<cfelseif #sv# is 20>
					 order by company_city DESC
					<cfelseif #sv# is 3>
					 order by company_state ASC
					<cfelseif #sv# is 30>
					 order by company_state DESC
					<cfelseif #sv# is 4>
					 order by company_zip ASC
					<cfelseif #sv# is 40>
					 order by company_zip DESC
					<cfelseif #sv# is 7>
					 order by source ASC, company_name ASC
					<cfelseif #sv# is 70>
					 order by source DESC, company_name ASC
					<cfelseif #sv# is 8>
					 order by company_keywords ASC, company_meta_keywords ASC
					<cfelseif #sv# is 80>
					 order by company_keywords ASC, company_meta_keywords ASC
					</cfif>

				 <cfelse>

				   order by company_name ASC

				 </cfif>

			</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>

			<cfparam name="url.start" default="1">
			<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
				<cfset url.start = 1>
			</cfif>

			<cfset totalPages = ceiling(companies.recordCount / perpage)>
			<cfset thisPage = ceiling(url.start / perpage)>

           <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="out_set.cfm" method="post">
           		<tr><td class="feed_header">Search Results

           		<cfif companies.recordcount is 0>
           		( No Companies )
           		<cfelseif companies.recordcount is 1>
           		( 1 Company )
           		<cfelse>
           		(#trim(numberformat(companies.recordcount,'999,999,999'))# Companies)
           		</cfif>

           		</td>
           		    <td class="feed_sub_header" style="font-weight: normal;" align=right>

		            <b>Add Filter</b>&nbsp;&nbsp;

                    <input class="input_text" type="text" style="width: 200px;" name="add_filter" required>
            &nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Add"></td>

           </form>

         		</tr>
           		<tr><td colspan=2><hr></td></tr>
           </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           		<tr><td class="feed_sub_header"><b>You searched for company capabilities matching "<i>#replace(session.keyword,'"','','all')#

           		<cfif isdefined("session.add_filter")>

           		 <cfloop index="i" list="#session.add_filter#">
           		  and "#i#"
           		 </cfloop>



           		</cfif>

           		"

           		</i></b></td>
           		    <td class="feed_sub_header" align=right valign=top>

					<cfif companies.recordcount GT #perpage#>
						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

				</td><td class="feed_sub_header" align=right width=200>

	                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="capability.cfm?export=1"><img src="/images/icon_export_excel.png" hspace=10 border=0 alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>
                    <a href="capability.cfm?export=1">Export to Excel</a>
           		    </td>

           		</tr>

      		    <cfif isdefined("session.add_filter")>

      		        <tr><td height=10></td></tr>

					<tr><td clsss="feed_option">

						 <cfloop index="i" list="#session.add_filter#">
						   <cfset size = evaluate(10*len(i))>
						   <input class="button_blue" style="font-size: 11px; height: 25px; width: auto" type="submit" name="button" value="X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#ucase(i)#&nbsp;&nbsp;"  onclick="location.href = 'remove.cfm?i=#i#'">&nbsp;&nbsp;
						 </cfloop>

					</td></tr>

          		</cfif>

           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header"><a href="capability.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td></td>
                    <td class="feed_sub_header">Keywords</td>
                    <td class="feed_sub_header">Website</td>
                    <td class="feed_sub_header"><a href="capability.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td class="feed_sub_header" align=center><a href="capability.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td></td>

			    </tr>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #row_counter# = 0>
                <cfset count = 1>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td class="feed_option">

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header" width=250><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td width=80 align=left><cfif listfind(in_network_list,companies.company_id)><img src="/images/in_network.png" style="cursor: pointer;" width=30 hspace=10 alt="In Network Partner" title="In Network Partner"><cfelse></cfif></td>
					<td class="feed_option">

					<cfif companies.company_keywords is "">
					#ucase(wrap(companies.company_meta_keywords,30,0))#
					<cfelse>
					#ucase(wrap(companies.company_keywords,30,0))#
					</cfif>

					</td>

					<td class="feed_option">


					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(companies.company_website) GT 40>
					#ucase(left(companies.company_website,40))#...
					<cfelse>
					#ucase(companies.company_website)#
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>

					<td width=50 align=right>
					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=8>
					  <div class="dropdown-content" style="width: 250px; text-align: left;">
						<a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer"><i class="fa fa-fw fa-pie-chart"></i>&nbsp;&nbsp;Company Profile</a>
						<a href="/exchange/include/save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-dollar"></i>&nbsp;&nbsp;Add to Opportunity</a>
						<a href="/exchange/include/save_company.cfm" onclick="window.open('/exchange/include/save_company.cfm?comp_id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
					  </div>
					</div>
					</td>
				</tr>

				 <tr><td colspan=9><hr></td></tr>

				</cfoutput>


            <tr><td colspan=7>

            <cfoutput>
            <tr>
           		    <td colspan=7 class="feed_sub_header">

					<cfif companies.recordcount GT #perpage#>
						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

				</td></tr>
			</cfoutput>





			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
			  <tr><td class="link_small_gray" colspan=2>The Exchange collects and integrates information about companies from multiple sources.  As a result, duplicate companies may exist.</td></tr>
			  <tr><td height=15></td></tr>

			  <tr><td width=60>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/cb_logo.png" width=40 alt="Crunchbase" title="Crunchbase"></a></td></tr>
				  </table>

				  </td><td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b>Crunchbase</b></a></td><td align=right class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.crunchbase.com</u></b></a></td></tr>
				   <tr><td class="link_small_gray" colspan=2>The Exchange uses Crunchbase information, specifically company names, tags, descriptions and keywords, to assist users on searching for companies.</td></tr>
				  </table>

				  </td></tr>

			</table>

            </td></tr>

			<cfelse>

			<tr><td class="feed_sub_header">No companies were found.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>
		  </table>

	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>