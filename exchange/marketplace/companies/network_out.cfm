<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>
<cfset #session.search_id# = 0>

<cfif not isdefined("session.comp_view")>
 <cfset session.comp_view = 2>
</cfif>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=14" rel="stylesheet" type="text/css">
</head><div class="center">

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}

.partner_badge {
    width: 31%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 150px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.partner_badge_title {
    font-family: calibri, arial;
    font-size: 14px;
    padding-bottom: 10px;
    color: 000000;
    font-weight: bold;
}
.partner_badge_text {
    font-family: calibri, arial;
    font-size: 12px;
    color: 000000;
}

</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

		<div class="main_box">

<script type="text/javascript">
    function clearThis(target){
        target.value= "";
    }
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td colspan=2 class="feed_header"><a href="network_out.cfm">Company Marketplace</a></td></tr>
		<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Search over 2 million Companies.</td></tr>
		<tr><td colspan=2><hr></td></tr>

		<tr>

        <form action="out_set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
			<td>

			   <span class="feed_sub_header">Find Company&nbsp;&nbsp;</span>

				&nbsp;

			   <select name="search_type" class="input_select" style="width: 150px;">
				 <option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>By Name
				 <option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>By Capability
				</select>
				&nbsp;

				<input class="input_text" type="text" style="width: 275px;" placeholder = "keyword" name="keyword" <cfif isdefined("session.keyword")>value="<cfoutput>#session.keyword#</cfoutput>"</cfif> required onfocus="clearThis(this)">
				&nbsp;

			    <select name="search_socio" class="input_select" style="width: 250px;">
				 <option value=0 <cfif isdefined("session.search_socio") and session.search_socio is "0">selected</cfif>>All Business Types
				 <option value="8a" <cfif isdefined("session.search_socio") and session.search_socio is "8a">selected</cfif>>8a Certified
				 <option value="A2" <cfif isdefined("session.search_socio") and session.search_socio is "A2">selected</cfif>>Woman Owned
				 <option value="8E" <cfif isdefined("session.search_socio") and session.search_socio is "8E">selected</cfif>>Woman Owned - Economically Disadvangated
				 <option value="A5" <cfif isdefined("session.search_socio") and session.search_socio is "A5">selected</cfif>>Veteran Owned
				 <option value="QF" <cfif isdefined("session.search_socio") and session.search_socio is "QF">selected</cfif>>Veteran Owned - Service Disabled
				 <option value="NB" <cfif isdefined("session.search_socio") and session.search_socio is "NB">selected</cfif>>Native American Owned
				 <option value="PI" <cfif isdefined("session.search_socio") and session.search_socio is "PI">selected</cfif>>Hispanic Owned
				 <option value="OY" <cfif isdefined("session.search_socio") and session.search_socio is "OY">selected</cfif>>Black Owned
				 <option value="FR" <cfif isdefined("session.search_socio") and session.search_socio is "FR">selected</cfif>>Asian Pacific Owned
				 <option value="23" <cfif isdefined("session.search_socio") and session.search_socio is "23">selected</cfif>>Minority Owned
				</select>

			    <select name="search_state" class="input_select">
				 <option value=0>All States
				 <cfoutput query="states">
				  <option value="#state_abbr#" <cfif isdefined("session.search_state") and session.search_state is state_abbr>selected</cfif>>#state_name#
				 </cfoutput>
				</select>

				<input class="button_blue" type="submit" name="button" value="Search">
			</td>
        </form>

        <form action="/exchange/marketplace/refresh.cfm" method="post">
			<td align=right>
		</td>
        </tr>

       </table>

		</div>

		<div class="main_box">

			<cfquery name="comp_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="16">
             select distinct(recent_company_id), max(recent_date) from recent
             where recent_usr_id = #session.usr_id# and
                   recent_company_id is not null and
			 recent_company_id not in
			(select hub_comp_company_id from hub_comp
		     where hub_comp_hub_id = #session.hub#)
			 group by recent_company_id
			 order by max(recent_date) DESC
			 </cfquery>

            <cfif comp_list.recordcount is 0>
             <cfset #clist# = 0>
            <cfelse>
             <cfset #clist# = #valuelist(comp_list.recent_company_id)#>
            </cfif>

            <cfset listcount = listlen(clist)>
            <cfset counter = 1>

			<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="16">
             select * from company
             where company_id in (#clist#)
             <cfif listlen(clist) GT 0>
             order by case company_id
             <cfloop index="i" list="#clist#">
              when <cfoutput>#i# then #counter#</cfoutput>
              <cfset counter = counter + 1>
             </cfloop>
             END
             </cfif>
            </cfquery>

  		     <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td height=10></td></tr>
			  <tr><td class="feed_header">Last Viewed Companies

			  <cfoutput>
			  <cfif companies.recordcount is 0>
			  <cfelseif companies.recordcount is 15>
			    (Top 15)
			  <cfelse>
			    (#companies.recordcount#)
			  </cfif>
			  </cfoutput>

              </td></tr>
			  <tr><td><hr></td></tr>
			  <tr><td height=20></td></tr>
			  <tr><td valign=top class="feed_sub_header" style="font-weight: normal;">

                 <cfif #companies.recordcount# is 0>
                  You have not viewed any Companies.
                 <cfelse>

				 <cfloop query="companies">

				  <div class="partner_badge">
				   <cfinclude template="company_badge.cfm">
				  </div>

				 </cfloop>

				 </cfif>

	  </div>

      </td>

      </tr>

  </table>

  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>