<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style style="text/css">
    .break {
        word-wrap: break-word;
     }
  	.hoverTable{
		width:100%;
		border-collapse:collapse;
	}
	.hoverTable td{
		padding:7px; border:#e0e0e0 1px solid;
	}
	/* Define the default color for all the table rows */
	.hoverTable tr{
		background: #ffffff;
	}
	/* Define the hover highlight color for the table row */
    .hoverTable tr:hover {
          background-color: #e0e0e0;
    }
</style>

<cfquery name="partner_tiers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_tier
 where partner_tier_hub_id = #session.hub#
 order by partner_tier_order
</cfquery>

<cfquery name="partner_programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_program
 where partner_program_hub_id = #session.hub#
 order by partner_program_order
</cfquery>

<cfset perpage = 200>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
		  <!--- <cfinclude template="/exchange/marketplace/recent.cfm"> --->
		  <cfinclude template="/exchange/portfolio/recent.cfm">


      </td><td valign=top>

      <div class="main_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>


		<tr><td colspan=2 class="feed_header"><a href="network_in.cfm">IN NETWORK PARTNERS</a></td></tr>
		<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">In Network Partners are companies that have been qualified by Booz Allen legal and subcontracts as authorized subcontractors.</td></tr>
		<tr><td colspan=2><hr></td></tr>

		<tr>

        <form action="network_in_set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
			<td>

			   <span class="feed_sub_header">Search for Partner&nbsp;&nbsp;</span>

				&nbsp;

			   <select name="search_type" class="input_select" style="width: 150px;">
				 <option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>By Name
				 <option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>By Capability
				</select>

				<input class="input_text" type="text" style="width: 200px;" placeholder = "keyword" name="keyword" <cfif isdefined("session.keyword")>value="<cfoutput>#session.keyword#</cfoutput>"</cfif> required onfocus="clearThis(this)">

			   <select name="search_tier" class="input_select" style="width: 150px;">
			   <option value=0>All Partner Tiers
			   <cfoutput query="partner_tiers">
			     <option value=#partner_tier_id# <cfif isdefined("session.search_tier") and session.search_tier is #partner_tiers.partner_tier_id#>selected</cfif>>#partner_tier_name#
			   </cfoutput>
				</select>

			   <select name="search_program" class="input_select" style="width: 200px;">
			   <option value=0>All Partner Programs
			   <cfoutput query="partner_programs">
			     <option value=#partner_program_id# <cfif isdefined("session.search_tier") and session.search_program is #partner_programs.partner_program_id#>selected</cfif>>#partner_program_name#
			   </cfoutput>
				</select>

				<input class="button_blue" type="submit" name="button" value="Search">

			</td>
        </form>

        </tr>

</table>
</div>
      <div class="main_box">


			<cfif #session.search_program# is 0>

				<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select hub_comp_company_id from hub_comp
				 left join company_extend on company_extend_company_id = hub_comp_company_id
				 where hub_comp_hub_id = #session.hub#

				 <cfif session.search_tier is not 0>
				  and company_extend_tier_id = #session.search_tier#
				 </cfif>

			</cfquery>

            <cfelse>

				<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select partner_program_xref_partner_id from partner_program_xref
				 where partner_program_xref_hub_id = #session.hub# and
					   partner_program_xref_program_id = #session.search_program#
				</cfquery>

				<cfif programs.recordcount is 0>
				 <cfset program_list = 0>
				<cfelse>
				 <cfset program_list = valuelist(programs.partner_program_xref_partner_id)>
				</cfif>

				<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select hub_comp_company_id from hub_comp
				 left join company_extend on company_extend_company_id = hub_comp_company_id
				 where hub_comp_hub_id = #session.hub# and
					   hub_comp_company_id in (#program_list#)

				 <cfif session.search_tier is not 0>
				  and company_extend_tier_id = #session.search_tier#
				 </cfif>

				</cfquery>

            </cfif>

			<cfif list.recordcount is 0>
			 <cfset comp_list = 0>
			<cfelse>
			 <cfset comp_list = valuelist(list.hub_comp_company_id)>
			</cfif>

  			<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

				select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company
                join product on product_company_id = company_id
                where

                <cfif isdefined("session.add_filter")>
				 contains((product_desc, product_keywords, product_name),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
                   and contains((product_desc, product_keywords, product_name),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 contains((product_desc, product_keywords, product_name),'"#trim(session.keyword)#"')
                </cfif>
                and product_public = 1
				and company.company_id in (#comp_list#)

				union

				select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company
                where

                <cfif isdefined("session.add_filter")>
				 contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
                   and contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(session.keyword)#"')
                </cfif>
				and company.company_id in (#comp_list#)
				union

				select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company
				join award_data on recipient_duns = company_duns
				where

                <cfif isdefined("session.add_filter")>
				   contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				    and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(session.keyword)#"')
                </cfif>
				and company.company_id in (#comp_list#)

 			    union

				select company.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo from company
				join sbir on duns = company_duns
                where
                <cfif isdefined("session.add_filter")>
				  contains((award_title, abstract, research_keywords),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				     and contains((award_title, abstract, research_keywords),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 contains((award_title, abstract, research_keywords),'"#trim(session.keyword)#"')
                </cfif>

               and (sbir.duns <> '' or company_duns <> '') and
               company.company_duns is not null
  			   and company.company_id in (#comp_list#)

		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by company_name DESC
							<cfelseif #sv# is 10>
							 order by company_name ASC
							<cfelseif #sv# is 2>
							 order by company_city ASC
							<cfelseif #sv# is 20>
							 order by company_city DESC
							<cfelseif #sv# is 3>
							 order by company_state ASC
							<cfelseif #sv# is 30>
							 order by company_state DESC
							<cfelseif #sv# is 4>
							 order by company_zip ASC
							<cfelseif #sv# is 40>
							 order by company_zip DESC
							<cfelseif #sv# is 7>
							 order by source ASC, company_name ASC
							<cfelseif #sv# is 70>
							 order by source DESC, company_name ASC
							<cfelseif #sv# is 8>
							 order by company_keywords ASC, company_meta_keywords ASC
							<cfelseif #sv# is 80>
							 order by company_keywords ASC, company_meta_keywords ASC
                            </cfif>

                         <cfelse>

                           order by company_name ASC

                         </cfif>

			</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>

			<cfparam name="url.start" default="1">
			<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
				<cfset url.start = 1>
			</cfif>

			<cfset totalPages = ceiling(companies.recordCount / perpage)>
			<cfset thisPage = ceiling(url.start / perpage)>

           <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="network_in_set.cfm" method="post">
           		<tr><td class="feed_header">SEARCH RESULTS

           		<cfif companies.recordcount is 0>

           		<cfelseif companies.recordcount is 1>
           		( 1 Partner Found )
           		<cfelse>
           		(#trim(numberformat(companies.recordcount,'999,999,999'))# Partners Found)
           		</cfif>

           		</td>
           		    <td class="feed_sub_header" style="font-weight: normal;" align=right>

		            <b>Add Filter</b>&nbsp;&nbsp;

                    <input class="input_text" type="text" style="width: 200px;" name="add_filter" required>
            &nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Add"></td>

           </form>

         		</tr>
           		<tr><td colspan=2><hr></td></tr>
           </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           		<tr><td class="feed_sub_header"><b>You searched for company capabilities matching <i>"#session.keyword#"

           		<cfif isdefined("session.add_filter")>

           		 <cfloop index="i" list="#session.add_filter#">
           		  and "#i#"
           		 </cfloop>

           		</cfif>

           		</i></b></td>
           		    <td class="feed_sub_header" align=right valign=top>

					<cfif companies.recordcount GT #perpage#>
						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

				</td><td class="feed_sub_header" align=right>

	                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/exchange/marketplace/network_in_capability.cfm?export=1" ><img src="/images/icon_export_excel.png" hspace=10 alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>

	                <a href="/exchange/marketplace/network_in_capability.cfm?export=1">Export to Excel</a>

           		    </td>

           		</tr>

      		    <cfif isdefined("session.add_filter")>

      		        <tr><td height=10></td></tr>

					<tr><td clsss="feed_option">

						 <cfloop index="i" list="#session.add_filter#">
						   <cfset size = evaluate(10*len(i))>
						   <input class="button_blue" style="font-size: 11px; height: 25px; width: auto" type="submit" name="button" value="X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#ucase(i)#&nbsp;&nbsp;"  onclick="location.href = 'network_in_remove.cfm?i=#i#'">&nbsp;&nbsp;
						 </cfloop>

					</td></tr>

          		</cfif>

           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header" width=200><a href="network_in_capability.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
                    <td class="feed_sub_header"><b>Keywords</b></td>
                    <td class="feed_sub_header"><b>Website</b></td>
                    <td class="feed_sub_header"><a href="network_in_capability.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
                    <td class="feed_sub_header" width=75 align=center><a href="network_in_capability.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
			    </tr>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #row_counter# = 0>
                <cfset count = 1>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td class="feed_option">

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header"><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option">

					<cfif companies.company_keywords is "">
					#ucase(wrap(companies.company_meta_keywords,30,0))#
					<cfelse>
					#ucase(wrap(companies.company_keywords,30,0))#
					</cfif>

					</td>

					<td class="feed_option">


					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(companies.company_website) GT 40>
					#ucase(left(companies.company_website,40))#...
					<cfelse>
					#ucase(companies.company_website)#
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				 <tr><td colspan=9><hr></td></tr>

				<cfset count = count + 1>

				</cfoutput>


            <tr><td colspan=7>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
			  <tr><td class="link_small_gray" colspan=2>The Exchange collects and integrates information about companies from multiple sources.  As a result, duplicate companies may exist.</td></tr>
			  <tr><td height=15></td></tr>

			  <tr><td width=60>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/cb_logo.png" width=40 alt="Crunchbase" title="Crunchbase"></a></td></tr>
				  </table>

				  </td><td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b>Crunchbase</b></a></td><td align=right class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.crunchbase.com</u></b></a></td></tr>
				   <tr><td class="link_small_gray" colspan=2>The Exchange uses Crunchbase information, specifically company names, tags, descriptions and keywords, to assist users on searching for companies.</td></tr>
				  </table>

				  </td></tr>

			</table>

            </td></tr>

			<cfelse>

			<tr><td class="feed_sub_header" style="font-weight: normal;">No companies were found.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>
		  </table>

	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>