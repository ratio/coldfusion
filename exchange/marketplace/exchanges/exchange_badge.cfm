<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(hub_xref_id) as total from hub_xref
 where hub_xref_hub_id = #ex.hub_id# and
       hub_xref_active = 1
</cfquery>

<cfquery name="mem" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_xref_id from hub_xref
 where hub_xref_hub_id = #ex.hub_id# and
       hub_xref_usr_id = #session.usr_id# and
       hub_xref_active = 1
</cfquery>

<cfquery name="communities" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(comm_id) as total from comm
 where comm_hub_id = #ex.hub_id#
</cfquery>

<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(distinct(usr_company_id)) as total from comm_xref
 join usr on usr_id = comm_xref_usr_id
 where comm_xref_hub_id = #ex.hub_id# and
	   comm_xref_active = 1
</cfquery>

<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=top width=130>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfif #ex.hub_logo# is "">
		  <tr><td width=110><a href="explore.cfm?i=#encrypt(ex.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/stock_exchange.png" width=110 border=0 vspace=20></td></tr>
		 <cfelse>
		  <tr><tdwidth=110><a href="explore.cfm?i=#encrypt(ex.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#ex.hub_logo#" style="border-radius: 8px;" width=110 border=0 vspace=20></td></tr>
		 </cfif>

		 </table>

	 <td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td height=15></td></tr>

		 <tr><td valign=top>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;"><a href="explore.cfm?i=#encrypt(ex.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#ex.hub_name#</a></td></tr>
			 <tr><td class="link_small_gray" style="padding-top: 0px; padding-bottom: 0px;"><cfif #exchanges.hub_city# is not "">#ex.hub_city#, </cfif>#ex.hub_state#</td></tr>
		 </table>

		 <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<cfif mem.recordcount is 1>
				 <tr><td align=right><a href="/exchange/go.cfm?i=#encrypt(ex.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/member.png" alt="Open Exchange" title="Open Exchange" border=0 height=30></a>
				 </td></tr>
                <cfelse>
				 <tr><td align=right>

				 <cfif ex.hub_join is 1>
				  <a href="join.cfm?i=#encrypt(ex.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/join.png" height=30 vspace=5 alt="Join" title="Join" border=0></a>
				 </cfif>

				 </td></tr>
			    </cfif>
			 </table>

		 </td></tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

		 <cfif len(ex.hub_desc GT 165)>
		  #left(ex.hub_desc,'165')#...
		 <cfelse>
		 #ex.hub_desc#
		 </cfif>

		 </td></tr>

		</table>

	</td></tr>

	</table>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>


	<tr><td colspan=2><hr></td></tr>

	<tr>

	    <td class="feed_sub_header" style="font-weight: normal;">

	     Members <b>#members.total#</b>&nbsp;|&nbsp;
	     Companies <b>#companies.total#</b>&nbsp;|&nbsp;
	     Communities <b>#communities.total#</b>

	    </td>

	    </tr>

	<tr><td height=10></td></tr>

	</table>

</cfoutput>