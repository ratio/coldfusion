<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="h" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_join = 1
</cfquery>

<cfif h.recordcount is 0>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Exchanges & Networks</td>
                <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
            <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">Exchanges and Networks and communities of people, companies and capabilities who are focused on one or more markets, sectors or capabilites.</td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=20></td></tr>
          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=130>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif #exchanges.hub_logo# is "">
			  <tr><td valign=top width=110><img src="/images/stock_exchange.png" width=110 border=0 vspace=20></td></tr>
			 <cfelse>
			  <tr><td valign=top width=110><img src="#media_virtual#/#h.hub_logo#" style="border-radius: 8px;" width=110 vspace=20 border=0></td></tr>
			 </cfif>

			 </table>

		   <td valign=top>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td height=15></td></tr>
			 <tr><td class="feed_header" style="padding-top: 0px; padding-bottom: 0px;">#h.hub_name#</a></td></tr>
			 <tr><td class="link_small_gray" style="padding-top: 0px; padding-bottom: 0px;"><cfif #h.hub_city# is not "">#h.hub_city#, </cfif>#h.hub_state#</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">#h.hub_desc#</td></tr>
			</table>

		  </td></tr>

		  <tr><td colspan=3><hr></td></tr>
		  <tr><td height=10></td></tr>
		 </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_header">Access Request</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">We have already received your request to access this <cfif h.hub_parent_hub_id is "">Exchange<cfelse>Network</cfif>.  You will receive an email when your request has been approved or if we have any questions about your request.  If you have not received
			  an email from us please contact Customer Support.</td></tr>
			  <tr><td height=10></td></tr>

			  <tr><td class="feed_sub_header" style="font-weight: normal;">Thank You</td></tr>


		 </table>

         </cfoutput>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>