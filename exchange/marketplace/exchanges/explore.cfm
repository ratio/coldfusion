<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.company_badge {
    width: 13%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 160px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
.member_badge {
    width: 11%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 250px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
.community_badge {
    width: 22%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 250px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

<cfquery name="h" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_xref
 join usr on usr_id = hub_xref_usr_id
 where hub_xref_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_xref_active = 1
 order by usr_last_name
</cfquery>

<cfquery name="communities" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comm
 where comm_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="comp_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(usr_company_id) from hub_xref
 join usr on usr_id = hub_xref_usr_id
 where hub_xref_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	   hub_xref_active = 1 and
	   usr_company_id is not null
</cfquery>

<cfif comp_list.recordcount is 0>
 <cfset clist = 0>
<cfelse>
 <cfset clist = valuelist(comp_list.usr_company_id)>
</cfif>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id in (#clist#)
</cfquery>

<cfquery name="mem" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_xref_id from hub_xref
 where hub_xref_hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_xref_usr_id = #session.usr_id# and
       hub_xref_active = 1
</cfquery>


<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Exchanges & Networks</td>
                <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
            <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">Exchanges and Networks and communities of people, companies and capabilities who are focused on one or more markets, sectors or capabilites.</td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=20></td></tr>
          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=130>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif #exchanges.hub_logo# is "">
			  <tr><td valign=top width=110><img src="/images/stock_exchange.png" width=110 border=0 vspace=20></td></tr>
			 <cfelse>
			  <tr><td valign=top width=110><img src="#media_virtual#/#h.hub_logo#" style="border-radius: 8px;" width=110 vspace=20 border=0></td></tr>
			 </cfif>

			 </table>

		   <td valign=top>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td height=15></td></tr>
			 <tr><td class="feed_header" style="padding-top: 0px; padding-bottom: 0px;">#h.hub_name#</a></td></tr>
			 <tr><td class="link_small_gray" style="padding-top: 0px; padding-bottom: 0px;"><cfif #h.hub_city# is not "">#h.hub_city#, </cfif>#h.hub_state#</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">#h.hub_desc#</td></tr>
			</table>

			</td><td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<cfif mem.recordcount is 1>
				 <tr><td align=right>

				 <a href="/exchange/go.cfm?i=#encrypt(h.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/member.png" height=30 vspace=5></a></td></tr>
                <cfelse>
				 <tr><td align=right>

				 <cfif h.hub_join is 1>
				   <a href="join.cfm?i=#encrypt(h.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/join.png" height=30 vspace=5 border=0 alt="Join" title="Join"></a>
				 </cfif>

				 </td></tr>
			    </cfif>
			 </table>

			</td></tr>

		  </td></tr>

		  <tr><td colspan=3><hr></td></tr>
		  <tr><td height=10></td></tr>
		 </table>

         </cfoutput>

	     <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header">Members</td></tr>
		  <tr><td height=20></td></tr>
		 </table>

		 <cfif members.recordcount is 0>
		     <table cellspacing=0 cellpadding=0 border=0 width=100%>
		      <tr><td class="feed_sub_header" style="font-weight: normal;">No Members are part of this Exchange or Network.</td></tr>
		     </table>
		 <cfelse>
		 <cfoutput query="members">
		  <div class="member_badge">

		     <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <cfif #members.usr_photo# is "">
			   <tr><td align=center valign=top><img src="/images/headshot.png" height=100 width=100 border=0 vspace=10></td></tr>
			  <cfelse>
			   <tr><td align=center valign=top><img style="border-radius: 8px;" vspace=10 src="#media_virtual#/#members.usr_photo#" height=100 width=100 border=0></td></tr>
			  </cfif>
		      <tr><td class="feed_option" align=center><b>#members.usr_first_name# #members.usr_last_name#</b></td></tr>
		      <tr><td class="feed_option" align=center>#members.usr_title#</td></tr>
		      <tr><td class="link_small_gray" align=center>#members.usr_company_name#</td></tr>
		     </table>


		  </div>
		 </cfoutput>
		 </cfif>

	     <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td><hr></td></tr>
		  <tr><td height=10></td></tr>
		  <tr><td class="feed_header">Companies Represented</td></tr>

		  <cfif companies.recordcount is 0>
	       <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies are part of the Exchange or Network.</td></tr>
          </cfif>

		  <tr><td height=20></td></tr>
		 </table>

		 <cfif companies.recordcount GT 0>
		 <cfoutput query="companies">
		  <div class="company_badge">

	     <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td align=center>
                    <cfif company_logo is "">
					  <a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener"><img src="//logo.clearbit.com/#company_website#" onerror="this.src='/images/no_logo.png'" width=75 style="padding-right: 5px; padding-top: 5px;" border=0></a>
					<cfelse>
                      <a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener"><img src="#media_virtual#/#company_logo#" width=75 style="padding-right: 5px; padding-top: 5px;" alt="#company_name# Profile" title="#company_name# Profile"></a>
					</cfif>
         </td><tr>

          <tr><td align=center class="feed_sub_header">
               #company_name#
         </td><tr>

         </table>

		  </div>
		 </cfoutput>
		 </cfif>

	     <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td><hr></td></tr>
		  <tr><td height=10></td></tr>
		  <tr><td class="feed_header">Communities</td></tr>

		  <cfif communities.recordcount is 0>
		      <tr><td class="feed_sub_header" style="font-weight: normal;">No Communities are part of the Exchange or Network.</td></tr>
          </cfif>

		  <tr><td height=20></td></tr>
		 </table>

		 <cfif communities.recordcount GT 0>

		 <cfoutput query="communities">
		  <div class="community_badge">

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td width=70>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td>

					<cfif #communities.comm_logo# is "">
					  <img src="#image_virtual#stock_community.png" width=60 border=0>
					<cfelse>
					  <img src="#media_virtual#/#communities.comm_logo#" width=60 border=0>
					</cfif>

					</td></tr>

				  </table>

				</td><td>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					 <tr><td>

						 <table cellspacing=0 cellpadding=0 border=0 width=100%>
						  <tr><td class="feed_title">#communities.comm_name#</td></tr>
						  <tr><td class="feed_option">
						  <b>

						   <cfif #communities.comm_city# is "" and #communities.comm_state# is "">
						   <br>
						   <cfelse>
							 #communities.comm_city#
							<cfif #communities.comm_state# is not 0>#communities.comm_state#</cfif>
						  </cfif>

						  </b></td></tr>

						 </table>

					 </td></tr>
				  </table>

			   </td></tr>

			   <tr><td colspan=2><hr></td></tr>

			   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">
			   <cfif communities.comm_desc is "">
				No description provided.
			   <cfelse>
			   <cfif len(communities.comm_desc) GT 180>#left(communities.comm_desc,'180')#...<cfelse>#communities.comm_desc#</cfif></td></tr>
			   </cfif>

			 </table>

		  </div>
		 </cfoutput>

		 </cfif>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>