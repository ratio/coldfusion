<cfinclude template="/exchange/security/check.cfm">

<cfquery name="h" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       hub_join = 1
</cfquery>

<cfif h.recordcount is 0>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cftransaction>

	<cfquery name="send" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into hub_xref
	 (
	  hub_xref_usr_id,
	  hub_xref_role_id,
	  hub_xref_hub_id,
	  hub_xref_active,
	  hub_xref_joined,
	  hub_xref_join_message,
	  hub_xref_usr_role
	  )
	  values
	  (#session.usr_id#,
	   #h.hub_default_role_id#,
	   #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	   1,
	   #now()#,
	  '#join_message#',
	  0
	  )
	</cfquery>

</cftransaction>

<cfset #user_email# = #tostring(tobinary(user.usr_email))#>
<cfset #hub_email# = #h.hub_support_email#>

<!--- Email Manager --->

<cfmail from="Exchange <noreply@ratio.exchange>"
		  to="#hub_email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="New User - #h.hub_name#">
<html>
<head>
<title><cfoutput>#h.hub_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;">#user.usr_first_name# #user.usr_last_name# has joined #h.hub_name#.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#join_message#</td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<!--- Email User --->

<cfmail from="#h.hub_name# <noreply@ratio.exchange>"
		  to="#user_email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="Welcome to #h.hub_name#">
<html>
<head>
<title><cfoutput>#h.hub_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header">Hi #user.usr_first_name#,</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal;">Thank you for joining #h.hub_name#.  We look forward to seeing you online.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header">#h.hub_support_name#</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal;">#h.hub_support_email#</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal;">#h.hub_support_phone#</td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<cflocation URL="join.cfm?u=1&i=#i#" addtoken="no">