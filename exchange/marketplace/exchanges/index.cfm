<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.exchange_badge {
    width: 47%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 225px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

<cfif not isdefined("session.exchange_filter")>
 <cfset session.exchange_filter = 1>
</cfif>

<cfif session.exchange_filter is 1>

	<cfquery name="hub_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select hub_xref_hub_id as hubid from hub_xref
	 where hub_xref_usr_id = #session.usr_id# and
		   hub_xref_active = 1
	 union
	 select hub_id as hubid from hub
	 where hub_type_id in (1,2)
	</cfquery>

<cfelse>

	<cfquery name="hub_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select hub_xref_hub_id as hubid from hub_xref
	 where hub_xref_usr_id = #session.usr_id# and
		   hub_xref_active = 1
	</cfquery>

</cfif>

<cfif hub_list.recordcount is 0>
 <cfset ex_list = 0>
<cfelse>
 <cfset ex_list = #valuelist(hub_list.hubid)#>
</cfif>

<cfquery name="ex" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id in (#ex_list#)
 order by hub_name
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Exchanges & Networks</td><td class="feed_sub_header">&nbsp;</td></tr>
            <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">Exchanges and Networks and communities of people, companies and capabilities who are focused on one or more markets, sectors or capabilites.</td></tr>
            <tr><td colspan=2><hr></td></tr>
            <form action="refresh.cfm" method="post">

            <tr><td align=right class="feed_sub_header" style="font-weight: normal;"><b>Fiter Exchanges & Networks</b>
            &nbsp;
            <select name="exchange_filter" class="input_select" onchange="form.submit();">
             <option value=1 <cfif session.exchange_filter is 1>selected</cfif>>All Exchanges & Networks
             <option value=2 <cfif session.exchange_filter is 2>selected</cfif>>My Exchanges & Networks
            </select>

            </td></tr>
            </form>


            <tr><td height=10></td></tr>
          </table>

          <cfloop query="ex">
            <div class="exchange_badge">
             <cfinclude template="exchange_badge.cfm">
            </div>
          </cfloop>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>