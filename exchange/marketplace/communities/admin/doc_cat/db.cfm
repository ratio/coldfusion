<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #doc_cat_image# is not "">
		<cffile action = "upload"
		 fileField = "doc_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into doc_cat (doc_cat_name, doc_cat_hub_id, doc_cat_order, doc_cat_image)
	 values ('#doc_cat_name#',#session.hub#,#doc_cat_order#,<cfif #doc_cat_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select doc_cat_image from doc_cat
		  where doc_cat_id = #doc_cat_id# and
		        doc_cat_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.doc_cat_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.doc_cat_image#">
		</cfif>

	</cfif>

	<cfif doc_cat_image is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select doc_cat_image from doc_cat
		  where doc_cat_id = #doc_cat_id# and
		        doc_cat_hub_id = #session.hub#
		</cfquery>

		<cfif #getfile.doc_cat_image# is not "">
         <cfif fileexists("#media_path#\#getfile.doc_cat_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.doc_cat_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "doc_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update doc_cat
	 set doc_cat_name = '#doc_cat_name#',

		  <cfif #doc_cat_image# is not "">
		   doc_cat_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   doc_cat_image = null,
		  </cfif>

	     doc_cat_order = #doc_cat_order#

	 where doc_cat_id = #doc_cat_id# and
	       doc_cat_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select doc_cat_image from doc_cat
		  where doc_cat_id = #doc_cat_id# and
		        doc_cat_hub_id = #session.hub#
		</cfquery>

		<cfif remove.doc_cat_image is not "">
         <cfif fileexists("#media_path#\#remove.doc_cat_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.doc_cat_image#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete doc_cat
	 where doc_cat_id = #doc_cat_id# and
	       doc_cat_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

