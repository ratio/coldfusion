<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #cal_cat_image# is not "">
		<cffile action = "upload"
		 fileField = "cal_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into cal_cat (cal_cat_name, cal_cat_hub_id, cal_cat_order, cal_cat_image)
	 values ('#cal_cat_name#',#session.hub#,#cal_cat_order#,<cfif #cal_cat_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select cal_cat_image from cal_cat
		  where cal_cat_id = #cal_cat_id# and
		        cal_cat_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.cal_cat_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.cal_cat_image#">
		</cfif>

	</cfif>

	<cfif cal_cat_image is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select cal_cat_image from cal_cat
		  where cal_cat_id = #cal_cat_id# and
		        cal_cat_hub_id = #session.hub#
		</cfquery>

		<cfif #getfile.cal_cat_image# is not "">
         <cfif fileexists("#media_path#\#getfile.cal_cat_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.cal_cat_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "cal_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update cal_cat
	 set cal_cat_name = '#cal_cat_name#',

		  <cfif #cal_cat_image# is not "">
		   cal_cat_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   cal_cat_image = null,
		  </cfif>

	     cal_cat_order = #cal_cat_order#

	 where cal_cat_id = #cal_cat_id# and
	       cal_cat_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select cal_cat_image from cal_cat
		  where cal_cat_id = #cal_cat_id# and
		        cal_cat_hub_id = #session.hub#
		</cfquery>

		<cfif remove.cal_cat_image is not "">
         <cfif fileexists("#media_path#\#remove.cal_cat_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.cal_cat_image#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete cal_cat
	 where cal_cat_id = #cal_cat_id# and
	       cal_cat_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

