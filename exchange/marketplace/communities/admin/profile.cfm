<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

        <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">Community Profile</td>
				       <td align=right class="feed_sub_header"><a href="/exchange/marketplace/communities/admin/">Tools Menu</a></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>
				  </table>

      <form action="profile_db.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Community Name</b></td>
					  <td><input class="input_text" type="text" style="width: 400px;" maxlength=200 required name="comm_name" value="#edit.comm_name#"></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" valign=top><b>Description</b></td>
					  <td><textarea class="input_textarea" name="comm_desc" style="width: 600px; height: 150px;" required>#edit.comm_desc#</textarea></td>
			   </tr>

			   <tr>
					  <td class="feed_sub_header" width=15%><b>Tag Line</b></td>
					  <td><input class="input_text" style="width: 600px;" type="text" maxlength=100 name="comm_tag_line" value="#edit.comm_tag_line#"></td>
			   </tr>

		   <tr><td class="feed_sub_header"><b>Keywords</b></td>
		       <td><input class="input_text" type="text" style="width: 500px;" name="comm_tags" size=70 value="#edit.comm_tags#" maxlength="200"></td></tr>
		   <tr><td></td>
		       <td class="link_small_gray">seperate tags with a comma</td></tr>

			   <tr>
					  <td class="feed_sub_header" valign=top><b>Type</b></td>
					  <td class="feed_sub_header" style="font-weight: normal;">
					  Open&nbsp;<input type="radio" name="comm_type_id" value=3 <cfif #edit.comm_type_id# is 3>checked</cfif>>&nbsp;&nbsp;&nbsp;
					  Standard&nbsp;<input type="radio" name="comm_type_id" value=1 <cfif #edit.comm_type_id# is 1>checked</cfif>>&nbsp;&nbsp;&nbsp;
					  Private&nbsp;<input type="radio" name="comm_type_id" value=2 <cfif #edit.comm_type_id# is 2>checked</cfif>></td>

			   </tr>

			   <tr><td></td>
			       <td class="link_small_gray"><u>Open Communities</u> do not require approval to join.  <u>Standard Communities</u> require approval.  <u>Private Communities</u> are invite only.</td></tr>
			   <tr>

			   <tr><td height=10></td></tr>

			   <tr>
				<td class="feed_sub_header" valign=top>Modules</td>
                <td valign=top>

			    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <tr><td width=35><input type="checkbox" name="comm_calendar" style="width: 22px; height: 22px;" <cfif edit.comm_calendar is 1>checked</cfif>></td>
			  	    <td class="feed_sub_header" style="font-weight: normal;">Calendar</td></tr>

			    <tr><td width=35><input type="checkbox" name="comm_documents" style="width: 22px; height: 22px;" <cfif edit.comm_documents is 1>checked</cfif>></td>
				    <td class="feed_sub_header" style="font-weight: normal;">Documents</td></tr>

			    <tr><td width=35><input type="checkbox" name="comm_opportunity" style="width: 22px; height: 22px;" <cfif edit.comm_opportunity is 1>checked</cfif>></td>
				    <td class="feed_sub_header" style="font-weight: normal;">Opportunity Board</td></tr>

			    </table>

			   </td></tr>

               <tr><td height=5></td></tr>
			   <tr><td class="feed_sub_header" valign=top><b>Icon / Image / Logo</b></td>
			   <td valign=top class="feed_sub_header" style="font-weight: normal;" valign=absmiddle>

					<cfif #edit.comm_logo# is "">
					  <input type="file" name="comm_logo">
					<cfelse>
					  <img style="border-radius: 0px;" src="#media_virtual#/#edit.comm_logo#" width=100>
					  <br><br>
					  <input type="file" name="comm_logo"><br><br>
					  <input type="checkbox" style="width: 22px; height: 22px;" name="remove_photo">&nbsp;or, check to remove logo
					 </cfif>

		       </td></tr>

		       <tr><td height=10></td></tr>

		   <tr><td class="feed_sub_header"><b>City, State, Zip</b></td>
		   <td><input class="input_text" type="text" name="comm_city" size=20 value="#edit.comm_city#" maxlength="50">&nbsp;&nbsp;

           </cfoutput>

		   <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		    select * from state
		    order by state_name
		   </cfquery>

		   <select name="comm_state" class="input_select">
		    <option value=0>Select State
		    <cfoutput query="states">
		     <option value="#state_abbr#" <cfif #edit.comm_state# is #state_abbr#>selected</cfif>>#state_name#
		    </cfoutput>
		   </select>

		   <cfoutput>

		   <input class="input_text" type="text" style="width: 100px;" name="comm_zip" size=10 value="#edit.comm_zip#" maxlength="10">

		   </cfoutput>

		   </td></tr>

		   <cfquery name="admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		    select * from usr
		    join hub_xref on hub_xref_usr_id = usr_id
		    where hub_xref_hub_id = #session.hub# and
		          hub_xref_active = 1
		    order by usr_last_name, usr_first_name
		   </cfquery>


		   <tr><td class="feed_sub_header"><b>Community Manager</b></td>
		   <td>
		   <select name="comm_owner_id" class="input_select">
		   <cfoutput query="admin">
		    <option value=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# <cfif #usr_id# is #edit.comm_owner_id#>selected</cfif>>#usr_last_name#, #usr_first_name# (#tostring(tobinary(admin.usr_email))#)
		   </cfoutput>
		   </select>

		   </td></tr>

		   <cfoutput>
		   <input type="hidden" name="current_owner_id" value=#encrypt(edit.comm_owner_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

			   <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

			   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>
           </cfoutput>

	   </table>

	   </form>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>