<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif isdefined("remove_banner_attachment")>

			<cfquery name="remove2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select comm_banner from comm
			  where comm_id = #session.community_id#
			</cfquery>

			<cfif fileexists("#media_path#\#remove2.comm_banner#")>
			<cffile action = "delete" file = "#media_path#\#remove2.comm_banner#">
			</cfif>

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update comm
		 set comm_banner = null
		 where comm_id = #session.community_id#
		</cfquery>

	</cfif>

	<cfif comm_banner is not "">

		<cfquery name="getfile2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_banner from comm
		  where comm_id = #session.community_id#
		</cfquery>

		<cfif #getfile2.comm_banner# is not "">
		 <cfif fileexists("#media_path#\#getfile2.comm_banner#")>
			<cffile action = "delete" file = "#media_path#\#getfile2.comm_banner#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "comm_banner"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

    <cfif comm_banner is not "">

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update comm
		 set comm_banner = '#cffile.serverfile#'
		 where comm_id = #session.community_id#
		</cfquery>

	</cfif>

    <cflocation URL="banner.cfm" addtoken="no">

</cfif>