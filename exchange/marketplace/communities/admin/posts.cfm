<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 4>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
       <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

	  <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Manage Posts</td><td class="feed_sub_header" align=right><a href="index.cfm">Tools Menu</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

        <cfif isdefined("u")>
         <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">Post has been successfully added.</td></tr>
         <cfelseif u is 2>
           <tr><td class="feed_sub_header" style="color: green;">Post has been successfully updated.</td></tr>
         <cfelseif u is 3>
           <tr><td class="feed_sub_header" style="color: green;">Post has been deleted.</td></tr>
         </cfif>
        </cfif>

	    </table>

	    <cfquery name="feeds" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from feed
		 join usr on usr_id = feed_created_usr_id
		 where feed_hub_id = #session.community_id#

         <cfif sv is 1>
          order by feed_title ASC
         <cfelseif sv is 10>
          order by feed_title DESC
         <cfelseif sv is 2>
          order by hub_name ASC, feed_updated DESC
         <cfelseif sv is 20>
          order by hub_name DESC, feed_updated DESC
         <cfelseif sv is 3>
          order by usr_last_name ASC, usr_first_name ASC, feed_updated DESC
         <cfelseif sv is 30>
          order by usr_last_name DESC, usr_first_name ASC, feed_updated DESC
         <cfelseif sv is 4>
          order by feed_updated DESC
         <cfelseif sv is 40>
          order by feed_updated ASC
         </cfif>

		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #feeds.recordcount# GT 0>

        <cfset counter = 0>

         <tr>
            <td class="feed_sub_header" valign=top><a href="posts.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>">Title</a></td>
            <td class="feed_sub_header" valign=top>Description</td>
            <td class="feed_sub_header" valign=top><a href="posts.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>">Member</a></td>
            <td class="feed_sub_header" valign=top><a href="posts.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>">Updated</a></td>
            <td></td>
         </tr>

         <cfoutput query="feeds">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=40>
         <cfelse>
          <tr bgcolor="e0e0e0" height=40>
         </cfif>

              <td class="feed_option">#feed_title#</td>
              <td class="feed_option">#feed_desc#</td>
              <td class="feed_option">#usr_last_name#, #usr_first_name#</td>
              <td class="feed_option">#dateformat(feed_created,'mm/dd/yyyy')# at #timeformat(feed_created)#</td>
              <td align=right><a href="delete.cfm?feed_id=#feed_id#"><img src="/images/icon_trash.png" width=15 border=0 hspace=10 alt="Delete Post" title="Delete Post" onclick="return confirm('Delete Post.\r\nAre you sure you want to delete this record?');"></a>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

        <cfelse>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Posts exist.</td></tr>

        </cfif>

        </table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>