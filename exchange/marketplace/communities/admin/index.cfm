<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="hub_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_id = #session.community_id#
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

        <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">Community Management Tools</td>
				       <td align=right class="feed_sub_header"><a href="/exchange/marketplace/communities/s.cfm?i=#encrypt(session.community_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">GO TO COMMUNITY</a></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>
				  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr><td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;"><li><a href="/exchange/marketplace/communities/admin/profile.cfm">Community Profile</a></td></tr>
                   <tr><td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;"><li><a href="/exchange/marketplace/communities/admin/banner.cfm">Community Banner</a></td></tr>
                   <tr><td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;"><li><a href="/exchange/marketplace/communities/admin/members.cfm">Members</a></td></tr>
                   <tr><td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;"><li><a href="/exchange/marketplace/communities/admin/posts.cfm">Manage Posts</a></td></tr>


                   <tr><td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;"><li><a href="/exchange/marketplace/communities/admin/cal_cat/">Calendar Categories</a></td></tr>
                   <tr><td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;"><li><a href="/exchange/marketplace/communities/admin/doc_cat">Document Categories</a></td></tr>

                  </table>

				  </cfoutput>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>