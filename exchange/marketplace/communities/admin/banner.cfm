<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

        <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">Community Banner</td>
				       <td align=right class="feed_sub_header"><a href="/exchange/marketplace/communities/admin/">Tools Menu</a></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>
				  </table>

					<form action="banner_db.cfm" method="post" enctype="multipart/form-data" >

					   <cfoutput>

					   <tr><td class="feed_sub_header" style="font-weight: normal;">

							<cfif #edit.comm_banner# is "">
							  <input type="file" id="image" name="comm_banner" onchange="validate_img()">
							<cfelse>
							  <img src="#media_virtual#/#edit.comm_banner#" width=800>
							  <br><br>
							  <b>Change Banner</b><br><br>
							  <input type="file" id="image" name="comm_banner" onchange="validate_img()"><br><br>
							  <input type="checkbox" name="remove_banner_attachment">&nbsp;or, check to remove banner
							 </cfif>

					   </td></tr>

					   <tr><td height=10></td></tr>

					   <tr><td class="link_small_gray">Preferred size is 1,200 pixels width, 300 pixcels height.</td></tr>

					   <tr><td height=10></td></tr>
					   <tr><td colspan=2><hr></td></tr>
					   <tr><td height=10></td></tr>

					   <tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10>

					   </cfoutput>
					   </form>


                 </table>

				  </cfoutput>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>