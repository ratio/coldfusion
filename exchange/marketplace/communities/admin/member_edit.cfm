<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfquery name="member" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm_xref
  join usr on usr_id = comm_xref_usr_id
  where comm_xref_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        comm_xref_comm_id = #session.community_id#
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

        <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr><td class="feed_header">Member Edit</td>
						   <td align=right class="feed_sub_header"><a href="members.cfm">Return</a></td></tr>
					   <tr><td colspan=2><hr></td></tr>
					   <tr><td height=10></td></tr>
					  </table>

                  <form action="member_edit_db.cfm" method="post">
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                     <tr>
                        <td class="feed_sub_header" style="padding-top: 3px; padding-bottom: 3px;">#member.usr_first_name# #member.usr_last_name#</td>
                     </tr>

                     <tr>
                        <td class="feed_sub_header" style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;">#member.usr_company_name#</td>
                     </tr>


                     <tr>
                        <td class="feed_sub_header" style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;">#tostring(tobinary(member.usr_email))#</td>
                     </tr>

                     <tr>
                        <td class="feed_sub_header">Join Message</td>
                     </tr>

                     <tr>
                        <td class="feed_sub_header" style="font-weight: normal;"><cfif #member.comm_xref_join_message# is "">No message.<cfelse>#member.comm_xref_join_message#</cfif></td>
                     </tr>

                     <tr><td><hr></td></tr>

                     <tr>
                        <td class="feed_sub_header">Approved to Join Community?</td>
                     </tr>

                     <tr>
                        <td>
                           <select name="comm_xref_active" class="input_select">
                            <option value=0 <cfif member.comm_xref_active is 0>selected</cfif>>No
                            <option value=1 <cfif member.comm_xref_active is 1>selected</cfif>>Yes
                           </select>
                        </td>
                     </tr>

                     <tr><td height=10></td></tr>

                     <tr>
                        <td class="feed_sub_header">Community Role?</td>
                     </tr>

                     <tr>
                        <td>
                           <select name="comm_xref_usr_role" class="input_select">
                            <option value=0 <cfif member.comm_xref_usr_role is 0>selected</cfif>>Normal User
                            <option value=1 <cfif member.comm_xref_usr_role is 1>selected</cfif>>Community Manager
                           </select>
                        </td>
                     </tr>

                     <tr><td height=10></td></tr>

                     <tr>
                        <td class="feed_sub_header">Access Rights</td>
                     </tr>

                     <tr><td>

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>

                      <tr><td width=35><input type="checkbox" name="comm_xref_message" style="width: 22px; height: 22px;" <cfif member.comm_xref_message is 1>checked</cfif>></td>
                          <td class="feed_sub_header" style="font-weight: normal;">Post Messages</td></tr>

                      <tr><td width=35><input type="checkbox" name="comm_xref_calendar" style="width: 22px; height: 22px;" <cfif member.comm_xref_calendar is 1>checked</cfif>></td>
                          <td class="feed_sub_header" style="font-weight: normal;">Post Calendar Events</td></tr>

                      <tr><td width=35><input type="checkbox" name="comm_xref_documents" style="width: 22px; height: 22px;" <cfif member.comm_xref_documents is 1>checked</cfif>></td>
                          <td class="feed_sub_header" style="font-weight: normal;">Post Documents</td></tr>

                      <tr><td width=35><input type="checkbox" name="comm_xref_opportunity" style="width: 22px; height: 22px;" <cfif member.comm_xref_opportunity is 1>checked</cfif>></td>
                          <td class="feed_sub_header" style="font-weight: normal;">Post Opportunities</td></tr>

					  </table>


                     </td></tr>



                     <tr><td><hr></td></tr>

                     <tr>
                        <td class="feed_sub_header" valign=absmiddle><input type="checkbox" style="width: 22px; height: 22px;" name="send_email">&nbsp;&nbsp;&nbsp;Check to send the below email message to the Member letting them know a change has been made to their account.</td>
                     </tr>

                     <tr>
                        <td class="feed_sub_header">Subject</td>
                     </tr>

                     <tr>
                        <td>
                           <input type="text" class="input_text" style="width: 700px;" name="email_subject" value="#edit.comm_name# Community - Account Update">
                        </td>
                     </tr>

                     <tr>
                        <td class="feed_sub_header">Message</td>
                     </tr>

                     <tr>
                        <td>
                           <textarea name="email_message" class="input_textarea" style="width: 700px; height: 150px;">Hi #member.usr_first_name#, a change has been made to your account within the #edit.comm_name# Community.  #chr(10)##chr(10)#*** Please include the details of the account change here ***.#chr(10)# #chr(10)#Thank You,#chr(10)#Community Manager</textarea>
                        </td>
                     </tr>

                     <tr><td><hr></td></tr>

                     <tr><td height=10></td></tr>


                     </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

                     <tr><td>

                     <input class="button_blue_large" type="submit" name="button" value="Update">&nbsp;&nbsp;
                     <input class="button_blue_large" type="submit" name="button" value="Cancel">


                     </td><td align=right><input class="button_blue_large" type="submit" name="button" value="Remove User From Community" onclick="return confirm('Remove User From Community?\r\nAre you sure you want to remove this user from the Community?');"></td></tr>


                     <input type="hidden" name="i" value=#i#>

                  </table>
                  </form>

                  </cfoutput>


			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>