<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

   <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr
	join hub_xref on hub_xref_usr_id = usr_id
	where hub_xref_hub_id = #session.hub# and
	      hub_xref_active = 1

	<cfif isdefined("session.user_keyword")>
	  and (usr_first_name like '%#session.user_keyword#%' or
		   usr_last_name like '%#session.user_keyword#%' or
		   usr_full_name like '%#session.user_keyword#%')
	</cfif>

	order by usr.usr_last_name
   </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Members</td>
			   <td align=right class="feed_sub_header"><a href="members.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		</table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;">Please select the users you wish to add to this Community.  Users that are added will be granted regular access.</td></tr>

		   <tr>
		       <td></td>
		       <td></td>
		       <td class="feed_sub_header">Name</td>
		       <td class="feed_sub_header">Email</td>
		       <td class="feed_sub_header" align=center>Active</td>
		   </tr>

	    <cfset counter = 0>

	    <form action="members_add_db.cfm" method="post">

	    <cfloop query="agencies">

		   <cfquery name="usr_check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from comm_xref
			  join usr on usr_id = comm_xref_usr_id
			  where comm_xref_comm_id = #session.community_id# and
			        comm_xref_usr_id = #agencies.usr_id#
		   </cfquery>

	    <cfoutput>

	       <cfif counter is 0>
	        <tr bgcolor="ffffff">
	       <cfelse>
	        <tr bgcolor="e0e0e0">
	       </cfif>

			 <td width=100 class="feed_sub_header" align=center>

			 <cfif usr_check.recordcount is 0>
			  <input type="checkbox" name="member_id" value=#encrypt(agencies.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="margin-left: 5px; width: 22px; height: 22px;">
			 <cfelse>
              Member
			 </cfif>
			 </td>

			 <cfif #usr_photo# is "">
			  <td width=75><img src="#image_virtual#headshot.png" height=40 width=40 border=0 style="margin-left: 20px; margin-right: 40px;"></td>
			 <cfelse>
			  <td width=75><img style="border-radius: 2px; margin-left: 20px; margin-right: 40px;" src="#media_virtual#/#agencies.usr_photo#" height=40 width=40 border=0></td>
			 </cfif>

              <td class="feed_sub_header">#agencies.usr_last_name#, #agencies.usr_first_name#</td>
	          <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(agencies.usr_email))#</td>
	          <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #agencies.hub_xref_active# is 1><b><font color="green">Yes</font><cfelse><font color="red">No</font></cfif></b></td>

	       </tr>

	    <cfif counter is 0>
	     <cfset counter = 1>
	    <cfelse>
	     <cfset counter = 0>
	    </cfif>

	    </cfoutput>

	    </cfloop>

	    <tr><td height=10></td></tr>
	    <tr><td colspan=6><hr></td></tr>
	    <tr><td height=10></td></tr>

	    <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Add to Community" onclick="return confirm('Add Members to Community?\r\nAre you sure you want to add the selected members to this Community?');"></td></tr></td></tr>

	    </form>

	    </table>



	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>