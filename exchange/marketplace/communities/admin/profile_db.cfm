<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

        <cfif isdefined("remove_photo")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select comm_logo from comm
			  where comm_id = #session.community_id#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.comm_logo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.comm_logo#">
        	</cfif>

        </cfif>

		<cfif #comm_logo# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select comm_logo from comm
			  where comm_id = #session.community_id#
			</cfquery>

			<cfif #getfile.comm_logo# is not "">
             <cfif FileExists("#media_path#\#getfile.comm_logo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.comm_logo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "comm_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

    <cftransaction>

    <cfif comm_owner_id is current_owner_id>
    <cfelse>

		<!--- Update Old Owners Access --->

		<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update comm_xref
		 set comm_xref_usr_role = 0
		 where comm_xref_comm_id = #session.community_id# and
			   comm_xref_usr_id = #decrypt(current_owner_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<!--- Check to see if new owner is a member --->

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select comm_xref_id from comm_xref
		 where comm_xref_usr_id = #decrypt(comm_owner_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			   comm_xref_comm_id = #session.community_id#
		</cfquery>

		<cfif check.recordcount is 1>

			<!--- If user exists, update record --->

			<cfquery name="update_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update comm_xref
			 set comm_xref_usr_role = 1
			 where comm_xref_comm_id = #session.community_id# and
				   comm_xref_usr_id = #decrypt(comm_owner_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
			</cfquery>

		<cfelse>

			<!--- User does not exist, create record --->

			<cfquery name="insert_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into comm_xref
			 (comm_xref_usr_id, comm_xref_comm_id, comm_xref_active, comm_xref_usr_role, comm_xref_joined)
			 values(#decrypt(comm_owner_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#, #session.community_id#, 1, 1,#now()#)
			</cfquery>

		</cfif>

    </cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update comm
	 set comm_name = '#comm_name#',
	     comm_desc = '#comm_desc#',
	     comm_calendar = <cfif isdefined("comm_calendar")>1<cfelse>null</cfif>,
	     comm_documents = <cfif isdefined("comm_documents")>1<cfelse>null</cfif>,
	     comm_opportunity = <cfif isdefined("comm_opportunity")>1<cfelse>null</cfif>,
	     comm_city = '#comm_city#',
	     comm_zip = '#comm_zip#',
	     comm_state = '#comm_state#',
	     comm_tags = '#comm_tags#',
	     comm_tag_line = '#comm_tag_line#',

		  <cfif #comm_logo# is not "">
		   comm_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_photo")>
		   comm_logo = null,
		  </cfif>

	     comm_updated = #now()#,
         comm_owner_id = #decrypt(comm_owner_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
         comm_type_id = #comm_type_id#
	 where comm_id = #session.community_id#
	</cfquery>

	</cftransaction>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Cancel">

    <cflocation URL="index.cfm" addtoken="no">

</cfif>

