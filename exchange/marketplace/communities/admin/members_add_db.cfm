<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("member_id")>

<cfloop index="m" list="#member_id#">

<cftransaction>

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into comm_xref
  (
   comm_xref_hub_id,
   comm_xref_usr_id,
   comm_xref_comm_id,
   comm_xref_active,
   comm_xref_usr_role,
   comm_xref_calendar,
   comm_xref_documents,
   comm_xref_message,
   comm_xref_joined
  )
  values
  (
   #session.hub#,
   #decrypt(m,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
   #session.community_id#,
   1,
   0,
   1,
   1,
   1,
   #now()#
   )
</cfquery>

</cftransaction>

</cfloop>

</cfif>

<cflocation URL="members.cfm?u=4" addtoken="no">