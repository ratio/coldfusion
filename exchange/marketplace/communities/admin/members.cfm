<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm_xref
  join usr on usr_id = comm_xref_usr_id
  where comm_xref_comm_id = #session.community_id#
  order by usr_last_name, usr_first_name
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">

        <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr><td class="feed_header">Members</td>
						   <td align=right class="feed_sub_header">

						   <a href="members_add.cfm">Add Members</a>

						   &nbsp;|&nbsp;

						   <a href="/exchange/marketplace/communities/admin/">Tools Menu</a></td></tr>
					   <tr><td colspan=2><hr></td></tr>

					   <cfif isdefined("u")>
					    <cfif u is 1>
						    <tr><td class="feed_sub_header" style="color: green;">Member has been successfully updated.</td></tr>
					    <cfelseif u is 3>
						    <tr><td class="feed_sub_header" style="color: green;">Member has been successfully removed from this Community.</td></tr>
					    <cfelseif u is 3>
						    <tr><td class="feed_sub_header" style="color: green;">Selected Members have been added to this Community.</td></tr>
					    </cfif>
					   <cfelse>
					    <tr><td height=10></td></tr>
					   </cfif>



					  </table>
                  </cfoutput>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

                   <cfif members.recordcount is 0>
                    <tr><td class="feed_sub_header" style="font-weight: normal;"></td></tr>
                   <cfelse>
                    <tr>
                        <td></td>
                        <td class="feed_sub_header">Name</td>
                        <td class="feed_sub_header">Company</td>
                        <td class="feed_sub_header" align=center>Joined</td>
                        <td class="feed_sub_header" align=center>Approved</td>
                        <td class="feed_sub_header">Role</td>
                    </tr>

                    <cfset counter = 0>
                    <cfoutput query="members">

                    <cfif counter is 0>
                     <tr bgcolor="FFFFFF" height=40>
                    <cfelse>
                     <tr bgcolor="E0E0E0" height=40>
                    </cfif>

			 <cfif #usr_photo# is "">
			  <td width=75><img src="#image_virtual#headshot.png" height=40 width=40 border=0 style="margin-left: 20px; margin-right: 40px;"></td>
			 <cfelse>
			  <td width=75><img style="border-radius: 2px; margin-left: 20px; margin-right: 40px;" src="#media_virtual#/#usr_photo#" height=40 width=40 border=0></td>
			 </cfif>

                       <td class="feed_sub_header" style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;"><a href="member_edit.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#usr_last_name#, #usr_first_name#</b></a></td>
                       <td class="feed_sub_header" style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;">#usr_company_name#</td>
                       <td class="feed_sub_header" align=center style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;">#dateformat(comm_xref_joined,'mm/dd/yyyy')#</td>
                       <td class="feed_sub_header" align=center style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;"><cfif comm_xref_active is 0>No<cfelse>Yes</cfif></td>
                       <td class="feed_sub_header" style="font-weight: normal; padding-top: 3px; padding-bottom: 3px;">
                       <cfif comm_xref_usr_role is 0>
                        Normal User
                       <cfelseif comm_xref_usr_role is 1>
                        Community Manager
                       </cfif>
                       </td>

                    </tr>

                    <cfif counter is 0>
                     <cfset counter = 1>
                    <cfelse>
                     <cfset counter = 0>
                    </cfif>

                    </cfoutput>

                   </cfif>

                 </table>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>