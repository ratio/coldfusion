<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}

.partner_badge {
    width: 31%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 150px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.partner_badge_title {
    font-family: calibri, arial;
    font-size: 14px;
    padding-bottom: 10px;
    color: 000000;
    font-weight: bold;
}
.partner_badge_text {
    font-family: calibri, arial;
    font-size: 12px;
    color: 000000;
}

</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      <cfif usr.hub_xref_role_id is 1>
	      <cfinclude template="/exchange/portfolio/recent.cfm">
      <cfelse>
	      <cfinclude template="/exchange/profile_company.cfm">
      </cfif>

      </td><td valign=top>

		<div class="main_box">

		 <cfquery name="comp_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select distinct(usr_company_id) from usr
		  join comm_xref on comm_xref_usr_id = usr_id
		  where comm_xref_comm_id = #session.community_id# and
				comm_xref_active = 1 and
				usr_company_id is not null
		 </cfquery>

		 <cfif comp_list.recordcount is 0>
		  <cfset clist = 0>
		 <cfelse>
		  <cfset clist = valuelist(comp_list.usr_company_id)>
		 </cfif>

			 <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from company
			  where company_id in (#clist#)
			 </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <cfoutput>
			   <tr>

			       <td class="feed_header">COMPANIES IN COMMUNITY</td>

				   <td align=right class="feed_sub_header">
                     <a href="open.cfm">Return</a>

				   </td>
			   </tr>
               <tr><td height=10></td></tr>
               <tr><td colspan=2><hr></td></tr>

               <tr><td class="feed_sub_header">

               </td></tr>

    		   </cfoutput>
			  </table>

			 <cfif companies.recordcount is 0>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header" style="font-weight: normal;">No Partners were found.</td></tr>
              </table>

			 <cfelse>

				 <cfloop query="companies">

				  <div class="partner_badge">
				   <cfinclude template="/exchange/marketplace/companies/company_badge.cfm">
				  </div>

				 </cfloop>

			 </cfif>

			</td></tr>

		 </table>
		 </center>

	  </div>

      </td>

      </tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>