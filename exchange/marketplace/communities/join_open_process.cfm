<cfquery name="owner" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_email, comm_name from comm
 join usr on usr_id = comm_owner_id
 where comm_id = #session.community_id#
</cfquery>

<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="cinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comm
 where comm_id = #session.community_id#
</cfquery>

<cftransaction>

	<cfquery name="send" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into comm_xref
	 (
	  comm_xref_usr_id,
	  comm_xref_comm_id,
	  comm_xref_active,
	  comm_xref_joined,
	  comm_xref_join_message,
	  comm_xref_usr_role
	  )
	  values
	  (#session.usr_id#,
	   #session.community_id#,
	   1,
	   #now()#,
	  '#join_message#',
	  0
	  )
	</cfquery>

</cftransaction>

<cfif isdefined("test_email")>
	<cfset #to# = #test_email#>
<cfelse>
	<cfset #to# = #tostring(tobinary(owner.usr_email))#>
</cfif>

<!--- Email Manager --->

<cfmail from="Exchange <noreply@ratio.exchange>"
		  to="#to#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="New User - #cinfo.comm_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;">#user.usr_first_name# #user.usr_last_name# has joined the #cinfo.comm_name# Community.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#join_message#</td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<!--- Email User --->

<cfif isdefined("test_email")>
	<cfset #to# = #test_email#>
<cfelse>
	<cfset #to# = #tostring(tobinary(user.usr_email))#>
</cfif>

<cfmail from="#cinfo.comm_name# <noreply@ratio.exchange>"
		  to="#to#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="Welcome to the the #cinfo.comm_name# Community">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;">Hi #user.usr_first_name#,</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">Thank you for joining the #cinfo.comm_name# Community.  We look forward to seeing you online.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">Thank you,</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">Community Administrator</td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<cflocation URL="open.cfm?u=2" addtoken="no">