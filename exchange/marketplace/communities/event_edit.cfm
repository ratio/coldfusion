<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">
<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm_cal
  where comm_cal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
 </cfquery>

 <cfquery name="comm_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfquery name="comm_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #comm_profile.comm_owner_id#
 </cfquery>

  <cfquery name="cal_cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from cal_cat
   where cal_cat_hub_id = #session.hub#
   order by cal_cat_order
 </cfquery>

<!--- Check to see if you're a member of this comm --->

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_news.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="open.cfm">Posts</a></span>
          </div>

          <cfif comm_profile.comm_calendar is 1>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_calendar.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="calendar.cfm">Shared Calendar</a></span>
          </div>

          </cfif>

          <cfif comm_profile.comm_documents is 1>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_document.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="docs.cfm">Document Library</a></span>
          </div>

          </cfif>

          <cfif comm_profile.comm_opportunity is 1>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_tile.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="opps.cfm">Opportunity Board</a></span>
          </div>

          </cfif>

		<div class="main_box_2">
		<center>

<style>
.image_home {
    <cfif #comm_profile.comm_banner# is "">
    background-image: url('<cfoutput>#image_virtual#/comm_stock_banner.jpg</cfoutput>');
    <cfelse>
    background-image: url('<cfoutput>#media_virtual#/#comm_profile.comm_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_comm_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 10px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}
.comm_join_button {
    font-family: calibri, arial;
    font-size: 20px;
    border-radius: 2px;
    width: auto;
    height: 40px;
    border: 0px;
    padding-left: 15px;
    padding-right: 15px;
    margin-right: 40px;
    background-color: #FFFFFF;
    curson: pointer;
    color: #000000;
    font-weight: bold;
}
.comm_right_box {
    width: 300px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(comm_profile.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_profile.comm_name#</a></td>
				       <td align=right class="feed_sub_header"><a href="calendar.cfm">Return</a></td></tr>
                   <tr><td height=10></td></tr>
				  </table>
				  </cfoutput>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
                   <tr><td height=10></td></tr>
                  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
		   <tr><td class="feed_header">Edit Event</td><td align=right class="feed_option"></td></tr>
		   <tr><td colspan=2><hr></td></tr>
          </table>

          <form action="event_db.cfm" method="post" enctype="multipart/form-data" >

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

          <tr><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header"><b>Title</b></td></tr>
			   <tr><td colspan=2><input type="text" class="input_text" name="comm_cal_name" value="#edit.comm_cal_name#" style="width: 750px;" required maxlength="100"></td></tr>

			   <tr><td class="feed_sub_header"><b>Short Description</b></td></tr>
			   <tr><td colspan=2><textarea name="comm_cal_short" class="input_textarea" style="width: 750px;" rows=2 placeholder="Will be displayed on the Event summary page.">#edit.comm_cal_short#</textarea></td></tr>

			   <tr><td class="feed_sub_header"><b>Full Description</b></td></tr>
			   <tr><td colspan=2><textarea name="comm_cal_desc" class="input_textarea" style="width: 750px;" rows=6 placeholder="Will be displayed on the Event detail page.">#edit.comm_cal_desc#</textarea></td></tr>

			   <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Date</b>

                   &nbsp;<input type="date" class="input_text" name="comm_cal_date" value="#edit.comm_cal_date#" required>

                   &nbsp;&nbsp;<b>From</b>&nbsp;&nbsp;&nbsp;&nbsp;<input type="time" class="input_text" value=#timeformat(edit.comm_cal_start)# name="comm_cal_start">&nbsp;&nbsp;
                   &nbsp;&nbsp;<b>To</b>&nbsp;&nbsp;&nbsp;&nbsp;<input type="time" class="input_text" value=#timeformat(edit.comm_cal_end)# name="comm_cal_end">
                   &nbsp;&nbsp;
                   <select name="comm_cal_timezone" class="input_select">
                    <option value="EST" <cfif edit.comm_cal_timezone is "EST">selected</cfif>>EST
                    <option value="CST" <cfif edit.comm_cal_timezone is "CST">selected</cfif>>CST
                    <option value="MST" <cfif edit.comm_cal_timezone is "MST">selected</cfif>>MST
                    <option value="PST" <cfif edit.comm_cal_timezone is "PST">selected</cfif>>PST
                   </select>
                   </td></tr>


              </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header" width=400>Point of Contact</td>
			       <td class="feed_sub_header">Address</td></tr>
			   <tr><td><textarea name="comm_cal_poc" class="input_textarea" style="width: 350px;" rows=4>#edit.comm_cal_poc#</textarea></td>
			       <td><textarea name="comm_cal_address" class="input_textarea" style="width: 350px;" rows=4>#edit.comm_cal_address#</textarea></td>
			   </tr>

              </table>

              </cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_sub_header">Category</td></tr>
               <tr><td>

               <select name="comm_cal_cat_id" class="input_select" style="width: 200px;">
               <cfoutput query="cal_cat">
               <option value=#cal_cat_id# <cfif #edit.comm_cal_cat_id# is #cal_cat_id#>selected</cfif>>#cal_cat_name#
               </cfoutput>
               </select>

               </td></tr>

               <cfoutput>

			   <tr><td class="feed_sub_header">Website</td></tr>
			   <tr><td colspan=2><input type="url" name="comm_cal_url" value="#edit.comm_cal_url#" class="input_text" style="width: 750px;" maxlength="200"></td></tr>
               <tr><td class="link_small_gray">External website address for more information.</td></tr>

                <tr><td class="feed_sub_header" valign=top>Picture / Image</td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.comm_cal_image# is "">
					  <input type="file" name="comm_cal_image">
					<cfelse>
					  <img src="#media_virtual#/#edit.comm_cal_image#" width=150><br><br>
					  <input type="file" name="comm_cal_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					 </cfif>

					 </td></tr>

			   <tr><td class="link_small_gray" colspan=2>Will be displayed to the left of the event information.  For best results image should be square in dimension.</td></tr>
			   <tr><td height=10></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>
			   <tr><td colspan=2>
			   <input type="submit" name="button" value="Save" class="button_blue_large">
			   &nbsp;&nbsp;
			   <input type="submit" name="button" value="Delete" class="button_blue_large" onclick="return confirm('Delete Event?\r\nAre you sure you want to delete this Event?');">

			   </td></tr>

			  </table>

		  </td></tr>

          </table>

          <input type="hidden" name="i" value=#i#>

          </cfoutput>

 		  </form>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=45></td></tr>
        </table>

           <cfinclude template="about.cfm">
           <cfinclude template="network.cfm">
           <cfinclude template="companies.cfm">
           <cfinclude template="manager.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>