<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_cal_image from comm_cal
		  where comm_cal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.comm_cal_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.comm_cal_image#">
		</cfif>

	</cfif>

	<cfif #comm_cal_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_cal_image from comm_cal
		  where comm_cal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.comm_cal_image# is not "">
         <cfif fileexists("#media_path#\#getfile.comm_cal_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.comm_cal_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "comm_cal_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update comm_cal
      set comm_cal_name = '#comm_cal_name#',
      	  comm_cal_short = '#comm_cal_short#',
      	  comm_cal_cat_id = #comm_cal_cat_id#,

		  <cfif #comm_cal_image# is not "">
		   comm_cal_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   comm_cal_image = null,
		  </cfif>

      	  comm_cal_desc = '#comm_cal_desc#',
      	  comm_cal_date = '#comm_cal_date#',
          comm_cal_start = <cfif isdefined("comm_cal_start")>'#comm_cal_start#'<cfelse>null</cfif>,
          comm_cal_end = <cfif isdefined("comm_cal_end")>'#comm_cal_end#'<cfelse>null</cfif>,
      	  comm_cal_poc = '#comm_cal_poc#',
      	  comm_cal_address = '#comm_cal_address#',
      	  comm_cal_url = '#comm_cal_url#',
      	  comm_cal_updated = #now()#,
      	  comm_cal_timezone = '#comm_cal_timezone#'
	  where comm_cal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	  </cfquery>

      <cflocation URL="event_details.cfm?i=#i#&u=1" addtoken="no">

<cfelseif #button# is "Post">

	<cfquery name="user_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select usr_first_name, usr_last_name, usr_company_id from usr
	 where usr_id = #session.usr_id#
	</cfquery>

	<cfif #comm_cal_image# is not "">
		<cffile action = "upload"
		 fileField = "comm_cal_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="post" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert comm_cal
	  (
       comm_cal_name,
       comm_cal_cat_id,
       comm_cal_short,
       comm_cal_image,
       comm_cal_desc,
       comm_cal_date,
       comm_cal_start,
       comm_cal_end,
       comm_cal_comm_id,
       comm_cal_poc,
       comm_cal_address,
       comm_cal_url,
       comm_cal_usr_id,
       comm_cal_created,
       comm_cal_updated,
       comm_cal_timezone
	  )
	  values
	  (
      '#comm_cal_name#',
       #comm_cal_cat_id#,
      '#comm_cal_short#',
	  <cfif comm_cal_image is "">null<cfelse>'#cffile.serverfile#'</cfif>,
      '#comm_cal_desc#',
      '#comm_cal_date#',
      <cfif comm_cal_start is "">null<cfelse>'#comm_cal_start#'</cfif>,
      <cfif comm_cal_end is "">null<cfelse>'#comm_cal_end#'</cfif>,
       #session.community_id#,
      '#comm_cal_poc#',
      '#comm_cal_address#',
      '#comm_cal_url#',
       #session.usr_id#,
       #now()#,
       #now()#,
      '#comm_cal_timezone#'
	  )
	</cfquery>

	<cflocation URL="calendar.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_cal_image from comm_cal
		  where comm_cal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif remove.comm_cal_image is not "">
         <cfif fileexists("#media_path#\#remove.comm_cal_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.comm_cal_image#">
         </cfif>
		</cfif>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete comm_cal
		  where comm_cal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cflocation URL="calendar.cfm?u=3" addtoken="no">

</cfif>

