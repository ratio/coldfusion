<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(comm_xref_id) as total from comm_xref
  join usr on usr_id = comm_xref_usr_id
  where comm_xref_comm_id = #session.community_id# and
        comm_xref_active = 1
 </cfquery>

<div class="comm_right_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfoutput>
			<tr><td class="feed_header"><a href="members.cfm">Community Members<cfif #members.total# GT 0> (#members.total#)</cfif></a></td></tr>
			</cfoutput>
            <tr><td colspan=3><hr></td></tr>

            <cfif members.total is 0>
             <tr><td class="feed_sub_header" style="font-weight: normal;">No members have been added to this Community.</td></tr>
            </cfif>

			 <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select distinct(comm_xref_usr_id), comm_xref_joined from comm_xref
			   join usr on usr_id = comm_xref_usr_id
			   where comm_xref_comm_id = #session.community_id# and
					 comm_xref_active = 1
			   order by comm_xref_joined DESC
			 </cfquery>

			 <cfif list.recordcount is 0>
			  <cfset people_list = 0>
			 <cfelse>
			  <cfset people_list = valuelist(list.comm_xref_usr_id)>
			 </cfif>

			 <cfquery name="pics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select top(18) * from usr
			   where usr_id in (#people_list#)
			 </cfquery>

			 <tr><td colspan=3>

			 <cfoutput query="pics">

			     <cfif usr_profile_display is 2>
					  <img style="border-radius: 150px;" src="#image_virtual#/headshot.png" width=37 height=37 border=0 alt="Private Profile" vspace=5 title="Private Profile">
			     <cfelse>
					 <cfif #usr_photo# is "">
					  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 150px;" vspace=5 src="#image_virtual#/headshot.png" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 <cfelse>
					  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 150px;" vspace=5  src="#media_virtual#/#usr_photo#" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 </cfif>
				 </cfif>
			 </cfoutput>
                  <cfif members.total GT 1>
	                  <a href="members.cfm"><img style="border-radius: 150px;" src="/images/all.png" width=35 height=35 vspace=5  border=0 alt="All Members" title="All Members"></a>
                  </cfif>
             </td></tr>

		</table>

    </td></tr>

</table>

</div>