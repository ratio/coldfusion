<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=13" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfset member = 0>
 <cfset admin = 0>
 <cfset start_date = #dateadd("d",-1,now())#>
 <cfif not isdefined("session.calendar_filter")>
  <cfset session.calendar_filter = 0>
 </cfif>

 <cfquery name="comm_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfquery name="events" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm_cal
  where comm_cal_comm_id = #session.community_id# and
        comm_cal_date >= '#dateformat(start_date,'mm/dd/yyyy')#'

        <cfif session.calendar_filter is not 0>
         and comm_cal_cat_id = #session.calendar_filter#
        </cfif>

 </cfquery>

 <cfif comm_profile.comm_owner_id is #session.usr_id#>
  <cfset member = 1>
  <cfset admin = 1>
 </cfif>

 <cfquery name="membership" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm_xref
  where comm_xref_usr_id = #session.usr_id# and
        comm_xref_comm_id = #session.community_id# and
        comm_xref_active = 1
 </cfquery>


  <cfquery name="cal_cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from cal_cat
   where cal_cat_hub_id = #session.hub#
   order by cal_cat_order
 </cfquery>

 <cfif membership.recordcount GT 0>
  <cfset member = 1>
 </cfif>
<style>

.image_home {
    <cfif #comm_profile.comm_banner# is "">
    background-image: url('<cfoutput>#image_virtual#/comm_stock_banner.jpg</cfoutput>');
    <cfelse>
    background-image: url('<cfoutput>#media_virtual#/#comm_profile.comm_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.comm_right_box {
    width: 300px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
.event_badge {
    width: 47%;
    display: inline-block;
    height: 175px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 0px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    background-color: #ffffff;
}
</style>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_news.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="open.cfm">Posts</a></span>
          </div>

          <cfif comm_profile.comm_calendar is 1>
          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_calendar.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="calendar.cfm">Shared Calendar</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_documents is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_document.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="docs.cfm">Document Library</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_opportunity is 1>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_tile.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="opps.cfm">Opportunity Board</a></span>
          </div>

          </cfif>


		<div class="main_box_2">

        <center>

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(comm_profile.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_profile.comm_name#</a></td>
			   <td align=right class="feed_sub_header">

		       <cfif membership.comm_xref_calendar is 1>
		       <img src="/images/plus3.png" width=15 hspace=10><a href="event_add.cfm">Add Event</a>
		       </cfif>

			   <img src="/images/icon_list.png" width=20 hspace=10><a href="/exchange/marketplace/communities/">All Communities</a></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
		   <tr><td height=10></td></tr>
		  </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="set_calendar_filter.cfm" method="post">

		   <tr><td class="feed_header" valign=middle>Shared Calendar</td>

		       <td align=right class="feed_sub_header" style="font-weight: normal;" valign=middle>
		       <b>Filter by:</b>&nbsp;&nbsp;

               </cfoutput>

               <select name="calendar_filter" class="input_select" style="width: 200px;" onchange=form.submit()>
               <option value=0>All Categories
               <cfoutput query="cal_cat">
               <option value=#cal_cat_id# <cfif session.calendar_filter is cal_cat_id>selected</cfif>>#cal_cat_name#
               </cfoutput>
               </select>

               <cfoutput>

		       </td>
               </tr>

           </form>

		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <cfif isdefined("u")>
		    <cfif u is 1>
		     <tr><td class="feed_sub_header" style="color: green;">Event has been successfully added.</td></tr>
		    <cfelseif u is 3>
		     <tr><td class="feed_sub_header" style="color: red;">Event has been successfully deleted.</td></tr>
		    </cfif>
		    <tr><td height=10></td></tr>
		   </cfif>

		  </table>

		  </cfoutput>

			  <cfif events.recordcount is 0>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">No events have been posted.</td></tr>
			  </table>

			  <cfelse>

			  <br>

			  <cfoutput query="events">



			  <div class="event_badge">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>

			     <td valign=top width=175>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr><td>

                    <a href="event_details.cfm?i=#encrypt(comm_cal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

					<cfif comm_cal_image is "">
					<img src="#image_virtual#stock_event.png" width=150 height=150>
					<cfelse>
					<img src="#media_virtual#/#comm_cal_image#" width=150 height=150>

                    </a>

					</td></tr>

                </cfif>

				  </table>
				 </td><td valign=top>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                    <tr>

                <tr><td class="feed_header"><a href="event_details.cfm?i=#encrypt(comm_cal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_cal_name#</a></td></tr>
                <tr><td class="link_small_gray" style="font-size: 16px;">
                #dateformat(comm_cal_date,'mmm d')#

                <cfif comm_cal_start is "" and comm_cal_end is "">
                <cfelse>
                @
                #timeformat(comm_cal_start,'short')#<cfif #comm_cal_end# is not ""> - #timeformat(comm_cal_end,'short')# #comm_cal_timezone#</cfif>
                </cfif>

                </td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">#replace(comm_cal_short,"#chr(10)#","<br>","all")#</td></tr>

				<!---
                <tr><td class="feed_sub_header">#replace(comm_cal_poc,"#chr(10)#","<br>","all")#</td></tr>
                <tr><td class="feed_sub_header">#replace(comm_cal_address,"#chr(10)#","<br>","all")#</td></tr> --->


                    </tr>
				  </table>
				 </td>
		      </tr>

				</table>

               </div>

			  </cfoutput>

				<cfset x = events.recordcount>
				<cfif x mod 2 is 0>
				<cfelse>
				  <div class="event_badge">
				  </div>
				</cfif>
			  </cfif>

		 </center>

	  </div>

      </td><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=45></td></tr>
        </table>

		<cfinclude template="about.cfm">

        <cfif member is 1>
			<cfinclude template="network.cfm">
			<cfinclude template="companies.cfm">
	    </cfif>

		<cfinclude template="manager.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>