<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

	  <div class="main_box">

		 <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select distinct(comm_xref_usr_id) from comm_xref
		   where comm_xref_comm_id = #session.community_id# and
		         comm_xref_active = 1
		 </cfquery>

		 <cfif list.recordcount is 0>
		  <cfset people_list = 0>
		 <cfelse>
		  <cfset people_list = valuelist(list.comm_xref_usr_id)>
		 </cfif>

		 <cfquery name="people" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from usr
		   where usr_id in (#people_list#)
		 </cfquery>

		<cfset perpage = 50>

		<cfparam name="url.start" default="1">
		<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt people.recordCount or round(url.start) neq url.start>
			<cfset url.start = 1>
		</cfif>

		<cfset totalPages = ceiling(people.recordCount / perpage)>
		<cfset thisPage = ceiling(url.start / perpage)>

		  <center>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <cfoutput>

				   <tr><td class="feed_header">Community Members</td>
				       <td class="feed_sub_header" align=right><a href="open.cfm">Return</a></td></tr>

                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

				<cfif people.recordcount GT #perpage#>

                   <tr>

				   <td class="feed_sub_header">

						Page
							<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

							<cfif url.start gt 1>
								<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
								<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
							<cfelse>
							</cfif>

							<cfif (url.start + perpage - 1) lt people.recordCount>
								<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
								<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
							<cfelse>
							</cfif>

				   </td>

                   </tr>

                   	<tr><td height=10></td></tr>
				   <cfelse>
                   	<tr><td height=10></td></tr>
				   </cfif>

				   </cfoutput>

				  </table>

            <cfif people.recordcount is 0>

<!---
				<table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td class="feed_sub_header" style="font-weight: normal;">

                   <cfif session.people_view is 1>
				   		You are not following any People.  To find People, please visit the <a href="/exchange/marketplace/people/set.cfm?v=3"><b>Marketplace<b></a></b>.
				   <cfelseif session.people_view is 2>
				   		No People are following you.
				   <cfelse>
				     No People were found.
				   </cfif>

                  </td></tr>
                </table> --->

            <cfelse>


	        <cfoutput query="people" startrow="#url.start#" maxrows="#perpage#">

			<div class="profile_badge">
			 <cfinclude template="/exchange/marketplace/people/badge.cfm">
			</div>

	   	   </cfoutput>


	     </cfif>

		 </td></tr>

		 </table>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>