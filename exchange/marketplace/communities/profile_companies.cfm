<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="hub_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_id = #cid#
 </cfquery>

 <cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  left join company on company_id = usr_company_id
  where usr_id = #hub_profile.hub_owner_id#
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

		<div class="main_box">

        <center>

<style>
.image_home {
    <cfif #hub_profile.hub_banner# is "">
    background-image: url('/images/hub_stock_banner.jpg');
    <cfelse>
    background-image: url('#media_virtual#/<cfoutput>#hub_profile.hub_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_hub_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 0px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}
.hub_join_button {
    font-family: calibri, arial;
    font-size: 20px;
    border-radius: 2px;
    width: auto;
    height: 40px;
    border: 0px;
    padding-left: 15px;
    padding-right: 15px;
    margin-right: 40px;
    background-color: #FFFFFF;
    curson: pointer;
    color: #000000;
    font-weight: bold;
}
.company_badge_small {
    width: 29%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 300px;
    padding-top: 20px;
    padding-bottom: 20px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 15px;
    margin-right: 15px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #FFFFFF;
}
</style>

 <cfinclude template="hub_stats_query.cfm">

		  <table cellspacing=0 cellpadding=0 border=0 width=99%>

			  <tr><td valign=top>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header"><img src="/images/icon_network.png" height=18 align=absmiddle>&nbsp;&nbsp;<a href="profile.cfm?hub_id=#hub_profile.hub_id#">#ucase(hub_profile.hub_name)#</a></td>
				       <td align=right><a href="/exchange/marketplace/communities/"><img src="/images/delete.png" width=20 border=0></a></td></tr>

				  <tr><td>&nbsp;</td></tr>
				  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home">
                   <td>
                   <cfinclude template="hub_header.cfm">
                   </td>
                   </tr>
                  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=20></td></tr>

				   <tr><td width=5></td><td valign=top width=73%>

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_header">PARTNERS</td></tr>
                        <tr><td><hr></td></tr>

						<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select * from hub_comp
						 join company on company_id = hub_comp_company_id
						 where hub_comp_hub_id = #cid# or hub_comp_hub_id in (#child_list#)
						</cfquery>

                        <cfif comp.recordcount is 0>
                         <tr><td class="feed_sub_header" style="font-weight: normal;">There are no Partners registered with this Community.</td></tr>
                        <cfelse>

                        <tr><td height=20></td></tr>
                        <tr><td>

					    <cfloop query="comp">

						<div class="company_badge_small">
							<cfinclude template="company_badge.cfm">
						</div>

						</cfloop>

                        </td></tr>

                        </cfif>

                       </table>

				       </td><td width=50>&nbsp;</td><td valign=top width=20%>

				       <cfinclude template="hub_stats.cfm">

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td height=10></td></tr>
                        <tr><td class="feed_header" align=center>MANAGER</td></tr>
                        <tr><td height=10></td></tr>
	                    <cfif #hub_admin.usr_photo# is "">
	                     <tr><td align=center><a href="/exchange/profile/"><img src="/images/headshot.png" width=125 border=0></a></td></tr>
	                    <cfelse>
	                     <tr><td align=center><a href="/exchange/member/profile.cfm?h=#hub_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#hub_admin.usr_photo#" width=125 border=0></a></td></tr>
	                    </cfif>

                        <tr><td height=10></td></tr>

                        <tr><td class="feed_sub_header" align=center><b>#hub_admin.usr_first_name# #hub_admin.usr_last_name#</b></td></tr>

					    <cfif #hub_admin.usr_company_id# is not 0>
						 <tr><td class="feed_option" align=center><b>#hub_admin.company_name#</b></td></tr>
					    </cfif>

					    <tr><td class="feed_option" align=center>#hub_admin.usr_email#</td></tr>
					    <tr><td class="feed_option" align=center>#hub_admin.usr_phone#</td></tr>

                       </table>

				       </td>
				   </tr>
				   </table>

				  </cfoutput>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>