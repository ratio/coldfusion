<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=13" rel="stylesheet" type="text/css">
</head><div class="center">

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

 <cfset member = 0>
 <cfset admin = 0>

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="comm_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfif comm_profile.comm_owner_id is #session.usr_id#>
  <cfset member = 1>
  <cfset admin = 1>
 </cfif>

 <cfinclude template="check_access.cfm">

  <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from deal
   left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
   join usr on usr_id = deal_owner_id
   left join deal_type on deal_type.deal_type_id = deal.deal_type_id
   left join deal_priority on deal_priority.deal_priority_id = deal.deal_priority_id
   where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from naics
   where naics_code = '#deal.deal_naics#'
  </cfquery>

  <cfquery name="activity" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from deal_activity
   where deal_activity_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfquery name="attachments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from attachment
   left join usr on usr_id = attachment_created_by
   where attachment_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfquery name="comments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from deal_comment
   join usr on usr_id = deal_comment_usr_id
   where deal_comment_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
   order by deal_comment_date DESC
  </cfquery>

<style>
.image_home {
    <cfif #comm_profile.comm_banner# is "">
    background-image: url('<cfoutput>#image_virtual#/comm_stock_banner.jpg</cfoutput>');
    <cfelse>
    background-image: url('<cfoutput>#media_virtual#/#comm_profile.comm_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.comm_right_box {
    width: 300px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
.event_badge {
    width: 47%;
    display: inline-block;
    height: 175px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 0px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    background-color: #ffffff;
}
</style>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_news.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="open.cfm">Posts</a></span>
          </div>

          <cfif comm_profile.comm_calendar is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_calendar.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="calendar.cfm">Shared Calendar</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_documents is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_document.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="docs.cfm">Document Library</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_opportunity is 1>
          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_calendar.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="opps.cfm">Opportunity Board</a></span>
          </div>
          </cfif>

		<div class="main_box_2">

        <center>

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(comm_profile.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_profile.comm_name#</a></td>
			   <td align=right class="feed_sub_header">

               <cfif comm_manager is 1>
		        <img src="/images/plus3.png" width=15 hspace=10><a href="edit.cfm?i=#encrypt(deal.deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Edit Opportunity</a>
		       </cfif>

			   <img src="/images/icon_list.png" width=20 hspace=10><a href="opps.cfm">All Opportunities</a></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
		   <tr><td height=10></td></tr>
		  </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=10></td></tr>

		   <tr><td class="feed_header" valign=middle><cfoutput>#deal.deal_name#</cfoutput></td>
		       <td class="feed_sub_header" align=right>
		       <cfoutput>
		       	<img src="/images/icon_pin.png" style="cursor: pointer; margin-top: 7px;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('pin.cfm?i=#i#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
		       </cfoutput>
		       </td></tr>

		   <tr><td colspan=2><hr></td></tr>

		   </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=10></td></tr>

		   <cfif isdefined("u")>
		    <cfif u is 1>
		     <tr><td class="feed_sub_header" style="color: green;">Activity has been successfully added.</td></tr>
		    <cfelseif u is 2>
		     <tr><td class="feed_sub_header" style="color: red;">Activity has been successfully updated.</td></tr>
		    <cfelseif u is 3>
		     <tr><td class="feed_sub_header" style="color: red;">Activity has been successfully deleted.</td></tr>
		    <cfelseif u is 4>
		     <tr><td class="feed_sub_header" style="color: green;">Attachment has been successfully deleted.</td></tr>
		    <cfelseif u is 5>
		     <tr><td class="feed_sub_header" style="color: green;">Attachment has been successfully deleted.</td></tr>
		    <cfelseif u is 6>
		     <tr><td class="feed_sub_header" style="color: red;">Attachment has been successfully deleted.</td></tr>
		    <cfelseif u is 7>
		     <tr><td class="feed_sub_header" style="color: green;">Comment has been successfully deleted.</td></tr>
		    <cfelseif u is 8>
		     <tr><td class="feed_sub_header" style="color: green;">Comment has been successfully deleted.</td></tr>
		    <cfelseif u is 9>
		     <tr><td class="feed_sub_header" style="color: red;">Comment has been successfully deleted.</td></tr>
		    <cfelseif u is 10>
		     <tr><td class="feed_sub_header" style="color: green;">Opportunity has been successfully updated.</td></tr>

		    </cfif>
		    <tr><td height=10></td></tr>
		   </cfif>

		  </table>

		  </cfoutput>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
		    <td class="feed_sub_header" style="padding-right: 10px;" width=100>Posted By</td>
		    <td class="feed_sub_header">Customer</td>
		    <td class="feed_sub_header" align=center>Type</td>
		    <td class="feed_sub_header" align=center>Est Release Date</td>
		    <td class="feed_sub_header" align=center>Est Award Date</td>
		    <td class="feed_sub_header" align=right>Potential Value</td>
         </tr>

        <cfoutput>

         <tr>

			<cfif #deal.usr_photo# is "">
			 <td width=95><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(deal.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/headshot.png" height=40 width=40 border=0 alt="#deal.usr_full_name#" title="#deal.usr_full_name#"></td>
			<cfelse>
			 <td width=95><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(deal.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" src="#media_virtual#/#deal.usr_photo#"  alt="#deal.usr_full_name#" title="#deal.usr_full_name#" height=40 width=40 border=0></td>
			</cfif>

            <td class="feed_sub_header" style="font-weight: normal;" width=600>#deal.deal_customer_name#</td>
            <td class="feed_sub_header" style="font-weight: normal;" align=center>#deal.deal_type#</td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center width=200><cfif #deal.deal_release_date# is "">TBD<cfelse>#dateformat(deal.deal_release_date,'mm/dd/yyyy')#</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center width=200><cfif #deal.deal_close_date# is "">TBD<cfelse>#dateformat(deal.deal_close_date,'mm/dd/yyyy')#</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=right width=150><cfif #deal.deal_value_total# is "">TBD<cfelse>#numberformat(deal.deal_value_total,'$999,999,999,999')#</cfif></td>
         </tr>

        </cfoutput>

	   </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=10></td></tr>
          <cfoutput>

           <cfif trim(deal.deal_desc) is not "">
           	<tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;">#deal.deal_desc#</td></tr>
           <cfelse>
            <tr><td height=10></td></tr>
           </cfif>

           <cfif trim(deal.deal_keywords) is not "">
           	<tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;"><b>Capabilities Needed</b> - #deal.deal_keywords#</td></tr>
           <cfelse>
            <tr><td height=10></td></tr>
           </cfif>

           <tr><td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;">
                 <b>Current Soliciation ##</b> - <cfif #deal.deal_current_sol# is "">Not Specified<cfelse>#deal.deal_current_sol#</cfif>
                 <br>
                 <b>Past Soliciation Number ##</b> - <cfif deal.deal_past_sol is "">Not Specified<cfelse>#deal.deal_past_sol#</cfif>

                 <br>
                 <b>Existing Contract ##</b> - <cfif deal.deal_contract_number is "">Not Specified<cfelse>#deal.deal_contract_number#</cfif>

                 </td>
             </tr>

            </table>

            </td><td width=30>&nbsp;</td><td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;">
                 <b>Place of Performance ##</b> -

                 <cfif deal.deal_pop_city is "" and (deal.deal_pop_state is "" or deal.deal_pop_state is 0)>
                  Not specified
                 <cfelse>
                 <cfif deal.deal_pop_city is not "">#deal.deal_pop_city#,</cfif> #deal.deal_pop_state#
                 </cfif>
                 <br>
                 <b>NAICS Code ##</b> - <cfif #naics.naics_code# is "">Not Specified<cfelse>(#naics.naics_code#) - #naics.naics_code_description#</cfif>

                 <br>
                 <b>PSC Code ##</b> - <cfif deal.deal_psc is "">Not Specified<cfelse>#deal.deal_psc#</cfif>

                 </td>
             </tr>

            </table>


           </td></tr>

           	<tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;"><b>Reference URL</b> - <cfif trim(deal.deal_url) is "">Not specified<cfelse><a href="#deal.deal_url#" style="font-weight: normal;" target="_blank"><u>#deal.deal_url#</u></a></cfif></td></tr>


           </cfoutput>

          </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td colspan=3><hr></td></tr>
          <tr><td valign=top class="feed_sub_header">Schedule</td>
              <td class="feed_sub_header" align=right>

              <cfoutput>

                 <cfif comm_manager is 1>
					<a href="/exchange/pipeline/deals/activity_add.cfm?l=c&i=#i#"><img src="/images/plus3.png" width=15 hspace=10 border=0 alt="Add Activity" title="Add Activity"></a>
					<a href="/exchange/pipeline/deals/activity_add.cfm?l=c&i=#i#">Add Activity</a>
				  </cfif>

			  </cfoutput>

              </td></tr>

        <cfif activity.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No schedule has been created.</td></tr>
        <cfelse>

          <tr><td colspan=2>

			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript">
			  google.charts.load('current', {'packages':['timeline']});
			  google.charts.setOnLoadCallback(drawChart);
			  function drawChart() {
				var container = document.getElementById('timeline');
				var chart = new google.visualization.Timeline(container);
				var dataTable = new google.visualization.DataTable();

				dataTable.addColumn({ type: 'string', id: 'Name' });
				dataTable.addColumn({ type: 'string', id: 'Activity' });
				dataTable.addColumn({ type: 'date', id: 'Start' });
				dataTable.addColumn({ type: 'date', id: 'End' });
				dataTable.addColumn({ type: 'string', role: 'tooltip', id: 'link', 'p': {'html': true} });
				dataTable.addRows([

				<cfoutput query="activity">
	            <cfif comm_manager is 1>
				  [ 'Activities', '#deal_activity_name#', new Date(#year(deal_activity_start_date)#, #evaluate(month(deal_activity_start_date)-1)#, #day(deal_activity_start_date)#), new Date(#year(deal_activity_end_date)#, #evaluate(month(deal_activity_end_date)-1)#, #day(deal_activity_end_date)#),'/exchange/pipeline/deals/activity_edit.cfm?l=c&i=#i#&id=#deal_activity_id#'],
				<cfelse>
				  [ 'Activities', '#deal_activity_name#', new Date(#year(deal_activity_start_date)#, #evaluate(month(deal_activity_start_date)-1)#, #day(deal_activity_start_date)#), new Date(#year(deal_activity_end_date)#, #evaluate(month(deal_activity_end_date)-1)#, #day(deal_activity_end_date)#),''],
				</cfif>
				</cfoutput>

				  ]);

			var options = {
			  timeline: { showRowLabels: false, colorByRowLabel: true }
			};

		 google.visualization.events.addListener(chart, 'select', function () {
			var selection = chart.getSelection();
			if (selection.length > 0) {
			  window.open(dataTable.getValue(selection[0].row, 4), '_self');
			  console.log(dataTable.getValue(selection[0].row, 4));
			}
		  });

		  function drawChart1() {
			chart.draw(dataTable, options);
		  }
		  drawChart1();

			  }
			</script>

			<div id="timeline" style="width: 100%; height: 150;"></div>

        </td></tr>

        </cfif>

        </table>


       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=2><hr></td></tr>
         <tr><td class="feed_sub_header">Attachments</td>
             <td class="feed_sub_header" align=right>

            <cfif comm_manager is 1>
             <cfoutput>
             	<a href="/exchange/pipeline/deals/attachment_add.cfm?l=c&i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a><a href="/exchange/pipeline/deals/attachment_add.cfm?l=c&i=#i#">Add Attachment</a>
             </cfoutput>
            </cfif>

            </td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif attachments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No attachments have been added.</td></tr>
             <cfelse>

              <tr>
                  <td></td>
                  <td class="feed_sub_header">Title</td>
                  <td class="feed_sub_header">Description</td>
                  <td class="feed_sub_header">Attachment</td>
                  <td class="feed_sub_header" align=right>Added</td>
              </tr>

              <cfset counter = 0>

              <cfoutput query="attachments">

              <cfif counter is 0>
               <tr bgcolor="FFFFFF">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>

                 <td width=60>

				 <cfif #usr_photo# is "">
				  <img src="/images/headshot.png" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#">
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#usr_photo#" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
				 </cfif>

				 </td>

                  <td class="feed_sub_header" style="font-weight: normal;" width=200>#attachment_name#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_desc# is "">Not Provided<cfelse>#attachment_desc#</cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_file# is "">No attachment<cfelse><a href="#media_virtual#/#attachment_file#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;"><u>#attachment_file#</u></a></cfif></td>
                  <td class="feed_sub_header" align=right style="font-weight: normal;" width=120>#dateformat(attachment_updated,'mmm dd, yyyy')#</td>

                  <td align=right width=40>
		            <cfif comm_manager is 1>
	                  <a href="/exchange/pipeline/deals/attachment_edit.cfm?l=c&i=#i#&attachment_id=#attachment_id#"><img src="/images/icon_edit.png" width=20 alt="Edit" title="Edit"></a>
                    </cfif>
                    </td>

                  </tr>

                  <cfif counter is 0>
                   <cfset counter = 1>
                  <cfelse>
                   <cfset counter = 0>
                  </cfif>

              </cfoutput>

             </cfif>
          </table>


       <!--- Comments --->

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td class="feed_sub_header">Comments</td>
             <td class="feed_sub_header" align=right>

            <cfif comm_manager is 1>
             <cfoutput>
             	<a href="/exchange/pipeline/deals/deal_comment_add.cfm?l=c&i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a><a href="/exchange/pipeline/deals/deal_comment_add.cfm?l=c&i=#i#">Add Comment</a>
             </cfoutput>
            </cfif>

            </td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif comments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No comments have been added.</td></tr>
             <cfelse>

              <tr>
                  <td></td>
                  <td class="feed_sub_header">Comment</td>
                  <td class="feed_sub_header" align=right>Updated</td>
              </tr>

              <cfset counter = 0>

              <cfoutput query="comments">

              <cfif counter is 0>
               <tr bgcolor="FFFFFF">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>

                 <td width=60>

				 <cfif #usr_photo# is "">
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#usr_photo#" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
				 </cfif>

				 </td>

                  <td class="feed_sub_header" style="font-weight: normal;">#deal_comment_text#</td>

                  <td class="feed_sub_header" align=right style="font-weight: normal;" width=120>#dateformat(deal_comment_date,'mmm dd, yyyy')#</td>

                  <td align=right width=40>
		            <cfif comm_manager is 1>
	                  <a href="/exchange/pipeline/deals/deal_comment_edit.cfm?l=c&i=#i#&deal_comment_id=#deal_comment_id#"><img src="/images/icon_edit.png" width=20 alt="Edit" title="Edit"></a>
                    </cfif>
                    </td>

                  </tr>

                  <cfif counter is 0>
                   <cfset counter = 1>
                  <cfelse>
                   <cfset counter = 0>
                  </cfif>

              </cfoutput>

             </cfif>
          </table>


      </td><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=45></td></tr>
        </table>

		<cfinclude template="about.cfm">

        <cfif member is 1>
			<cfinclude template="network.cfm">
			<cfinclude template="companies.cfm">
	    </cfif>

		<cfinclude template="manager.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>