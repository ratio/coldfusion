<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="hub_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_id = #cid#
 </cfquery>

 <cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  left join company on company_id = usr_company_id
  where usr_id = #hub_profile.hub_owner_id#
 </cfquery>

 <cfinclude template="hub_stats_query.cfm">

<!--- Check to see if you're a member of this hub --->

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

		<div class="main_box">

        <center>

<style>
.image_home {
    <cfif #hub_profile.hub_banner# is "">
    background-image: url('/images/hub_stock_banner.jpg');
    <cfelse>
    background-image: url('#media_virtual#/<cfoutput>#hub_profile.hub_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_hub_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 10px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}
.hub_join_button {
    font-family: calibri, arial;
    font-size: 20px;
    border-radius: 2px;
    width: auto;
    height: 40px;
    border: 0px;
    padding-left: 15px;
    padding-right: 15px;
    margin-right: 40px;
    background-color: #FFFFFF;
    curson: pointer;
    color: #000000;
    font-weight: bold;
}
</style>

		  <table cellspacing=0 cellpadding=0 border=0 width=99%>

			  <tr><td valign=top>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header"><a href="profile.cfm?hub_id=#hub_profile.hub_id#">#ucase(hub_profile.hub_name)#</a></td>
				       <td align=right>

				       <cfif isdefined("session.hub")>
				        <a href="/exchange/marketplace/communities/">
				       <cfelse>
				        <a href="/exchange/marketplace/communities/">
				       </cfif>

				       <img src="/images/delete.png" width=20 border=0></a></td></tr>
				  <tr><td>&nbsp;</td></tr>
				  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home">

                   <td>

                   <cfinclude template="hub_header.cfm">

                   </td>

                   </tr>

                  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=20></td></tr>

				   <tr><td width=5></td><td valign=top width=73%>

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_header">ABOUT</td></tr>
                        <cfif #hub_profile.hub_tag_line# is not "">
	                        <tr><td class="feed_sub_header" style="font-weight: normal;">#hub_profile.hub_tag_line#</td></tr>
                        </cfif>
                        <tr><td><hr></td></tr>
                        <tr><td class="feed_sub_header">Overview</td></tr>

                        <cfif isdefined("u")>
                         <tr><td class="feed_sub_header"><font color="green">Request has been sent.  You will be contacted soon with more details about Hub membership.</td></tr>
                        </cfif>

                        <tr><td class="feed_option" style="font-size: 16px;">#hub_profile.hub_desc#</td></tr>
                        <tr><td height=10></td></tr>
                        <tr><td class="feed_option" style="font-size: 16px;">#hub_profile.hub_long_desc#</td></tr>
				        <tr><td><hr></td></tr>
				        <tr><td class="feed_sub_header">Location</td></tr>
				        <tr><td class="feed_option" style="font-size: 16px;">#hub_profile.hub_city#, #hub_profile.hub_state#</td></tr>
                        <tr><td><hr></td></tr>
                        <tr><td class="feed_sub_header">Focus Areas</td></tr>
                        <tr><td class="feed_option" style="font-size: 16px;"><cfif #hub_profile.hub_tags# is "">Not provided<cfelse>#hub_profile.hub_tags#</cfif></td></tr>
                        <tr><td><hr></td></tr>
                        <tr><td class="feed_sub_header">Website</td></tr>
                        <tr><td class="feed_option" style="font-size: 16px;"><cfif #hub_profile.hub_login_page# is "">Not provided<cfelse><a href="#hub_profile.hub_login_page#" target="_blank" rel="noopener" rel="noreferrer"><u>#hub_profile.hub_login_page#</u></a></cfif></td></tr>
                       </table>

				       </td><td width=50>&nbsp;</td><td valign=top width=20%>

				       <cfinclude template="hub_stats.cfm">

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td height=10></td></tr>
                        <tr><td class="feed_header" align=center>MANAGER</td></tr>
                        <tr><td height=10></td></tr>
	                    <cfif #hub_admin.usr_photo# is "">
	                     <tr><td align=center><a href="/exchange/profile/"><img src="/images/headshot.png" width=125 border=0></a></td></tr>
	                    <cfelse>
	                     <tr><td align=center><a href="/exchange/member/profile.cfm?h=#hub_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#hub_admin.usr_photo#" width=125 border=0></a></td></tr>
	                    </cfif>

                        <tr><td height=10></td></tr>

                        <tr><td class="feed_sub_header" align=center><b>#hub_admin.usr_first_name# #hub_admin.usr_last_name#</b></td></tr>

					    <cfif #hub_admin.usr_company_id# is not 0>
						 <tr><td class="feed_option" align=center><b>#hub_admin.company_name#</b></td></tr>
					    </cfif>

					    <tr><td class="feed_option" align=center>#hub_admin.usr_email#</td></tr>
					    <tr><td class="feed_option" align=center>#hub_admin.usr_phone#</td></tr>

                       </table>

				       </td>
				   </tr>
				   </table>

				  </cfoutput>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>