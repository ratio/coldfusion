<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="usr_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
 </cfquery>

 <cfquery name="hub_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_id = #cid#
 </cfquery>

 <cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  left join company on company_id = usr_company_id
  where usr_id = #hub_profile.hub_owner_id#
 </cfquery>

<!--- Get Members --->

 <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select count(usr_id) as total from usr
   left join hub_xref on hub_xref_usr_id = usr_id
   left join company on company_id = usr_company_id
   where hub_xref_hub_id = #cid# and
	     hub_xref_active = 1
 </cfquery>

<!--- Get Hub Companies --->

 <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(company_id) as total from company
  where company_hub_id = #cid#
 </cfquery>

<!--- Get Child Companies --->

 <cfquery name="children" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(hub_id) as total from hub
  where hub_parent_hub_id = #cid#
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

		<div class="main_box">

        <center>

<style>
.image_home {
    <cfif #hub_profile.hub_banner# is "">
    background-image: url('/images/hub_stock_banner.jpg');
    <cfelse>
    background-image: url('#media_virtual#/<cfoutput>#hub_profile.hub_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_hub_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 0px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}
.hub_join_button {
    font-family: calibri, arial;
    font-size: 20px;
    border-radius: 2px;
    width: auto;
    height: 40px;
    border: 0px;
    padding-left: 15px;
    padding-right: 15px;
    margin-right: 40px;
    background-color: #FFFFFF;
    curson: pointer;
    color: #000000;
    font-weight: bold;
}
</style>

		  <table cellspacing=0 cellpadding=0 border=0 width=99%>

			  <tr><td valign=top>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">JOIN COMMUNITY</td>
				       <td align=right><a href="/exchange/marketplace/communities/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
				  <tr><td>&nbsp;</td></tr>
				  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home">

                   <td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                    <tr><td height=10></td></tr>
                    <tr><td style="padding-left: 30px;"><a href="/exchange/marketplace/hubs/profile.cfm?hub_id=#hub_id#"><img src="#media_virtual#/#hub_profile.hub_logo#" width=150 border=0></a></td>
                        <td align=right>

                        </td></tr>

                    <tr><td height=20></td></tr>
                    <tr><td class="marketplace_hub_header" valign=bottom>#hub_profile.hub_name#</td></tr>
                  </table>

                   </td>

                   </tr>

                  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=20></td></tr>

				   <tr><td width=5></td><td valign=top width=73%>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header"><b>Thank you for your interest in joining this Community.</b></td></tr>
			   <tr><td><hr></td></tr>
               <form action="join_db.cfm" method="post">
				  <tr><td class="feed_sub_header" style="font-weight: normal;">This is a standard Community that requires approval from the Community Administrator to join.   Please tell us a little information and you and your company and why you are interested in joining our HUB.</td></tr>
                  <tr><td height=5></td></tr>
                  <tr><td class="feed_sub_header" style="font-weight: normal;"><b>From: </b><cfoutput>#usr_info.usr_first_name# #usr_info.usr_last_name#</cfoutput></td></tr>
                  <tr><td height=5></td></tr>
                  <tr><td class="feed_sub_header" style="font-weight: normal;">Please tell us a little about yourself and why you want to join this Community.</td></tr>
                  <tr><td class="feed_option"><textarea name="message" style="width: 900px; height: 110px;" class="input_textarea"></textarea></td></tr>
                  <tr><td height=5></td></tr>
                  <tr><td><input class="button_blue_large" type="submit" name="button" value="Join Community"></td></tr>

                  <cfoutput>
                   <input type="hidden" name="id" value=#cid#>
                  </cfoutput>

               </form>

               </table>

				       </td><td width=50>&nbsp;</td><td valign=top width=20%>

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>

					    <tr><td class="feed_header" colspan=3 align=center>ECOSYSTEM</td></tr>
					    <tr><td height=10></td></tr>

					    <tr>
                            <td class="feed_header" align=center><a href="/exchange/marketplace/hubs/profile_members.cfm?hub_id=#hub_id#" style="font-size: 55px;">#members.total#</a></td>
					        <td class="feed_header" align=center><a href="/exchange/marketplace/hubs/profile_companies.cfm?hub_id=#hub_id#" style="font-size: 55px;">#companies.total#</a></td>
					        <td class="feed_header" align=center><a href="/exchange/marketplace/hubs/profile_hubs.cfm?hub_id=#hub_id#" style="font-size: 55px;">#children.total#</a></td>
                        </tr>

					    <tr>
					        <td class="link_med_gray" align=center>Members</td>
					        <td class="link_med_gray" align=center>Partners</td>
					        <td class="link_med_gray" align=center>Communities</td>
					    </tr>

					    <tr><td height=20></td></tr>
					    <tr><td colspan=4><hr></td></tr>

				       </table>

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td height=10></td></tr>
                        <tr><td class="feed_header" align=center>MANAGER</td></tr>
                        <tr><td height=10></td></tr>
	                    <cfif #hub_admin.usr_photo# is "">
	                     <tr><td align=center><a href="/exchange/profile/"><img src="/images/headshot.png" width=125 border=0></a></td></tr>
	                    <cfelse>
	                     <tr><td align=center><a href="/exchange/member/profile.cfm?h=#hub_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#hub_admin.usr_photo#" width=125 border=0></a></td></tr>
	                    </cfif>

                        <tr><td height=10></td></tr>

                        <tr><td class="feed_sub_header" align=center><b>#hub_admin.usr_first_name# #hub_admin.usr_last_name#</b></td></tr>

					    <cfif #hub_admin.usr_company_id# is not 0>
						 <tr><td class="feed_option" align=center><b>#hub_admin.company_name#</b></td></tr>
					    </cfif>

					    <tr><td class="feed_option" align=center>#hub_admin.usr_email#</td></tr>
					    <tr><td class="feed_option" align=center>#hub_admin.usr_phone#</td></tr>

                       </table>

				       </td>
				   </tr>
				   </table>

				  </cfoutput>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>