<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("hub_id")>

   <cfquery name="ismember" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from hub_xref
	where hub_xref_usr_id = #session.usr_id# and
		  hub_xref_hub_id = #hub_id# and
		  hub_xref_active = 1
   </cfquery>

   <cfif ismember.recordcount is 1>
    <cfset session.hub = #hub_id#>
    <cflocation URL="/exchange/" addtoken="no">
   <cfelse>
    <cflocation URL="/exchange/hubs/join.cfm?hub_id=#hub_id#" addtoken="no">
    <cfabort>
   </cfif>

</cfif>

