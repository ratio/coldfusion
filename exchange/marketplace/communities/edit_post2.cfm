<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <cfquery name="feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from feed
   where feed_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from lens
   where lens_hub_id = #session.hub#
   order by lens_order
  </cfquery>

 <cfquery name="hub_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_id = #session.community_id#
 </cfquery>

 <cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #hub_profile.hub_owner_id#
 </cfquery>

<!--- Check to see if you're a member of this hub --->

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>

		<div class="main_box">

        <center>

<style>
.image_home {
    <cfif #hub_profile.hub_banner# is "">
    background-image: url('/images/hub_stock_banner.jpg');
    <cfelse>
    background-image: url('#media_virtual#/<cfoutput>#hub_profile.hub_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_hub_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 10px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}
.hub_join_button {
    font-family: calibri, arial;
    font-size: 20px;
    border-radius: 2px;
    width: auto;
    height: 40px;
    border: 0px;
    padding-left: 15px;
    padding-right: 15px;
    margin-right: 40px;
    background-color: #FFFFFF;
    curson: pointer;
    color: #000000;
    font-weight: bold;
}
.comm_right_box {
    width: 350px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(hub_profile.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#ucase(hub_profile.hub_name)# COMMUNITY</a></td>
				       <td align=right class="feed_sub_header"><a href="/exchange/marketplace/communities/">All Communities</a></td></tr>
                   <tr><td height=10></td></tr>
				  </table>
				  </cfoutput>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
                   <tr><td height=10></td></tr>
                  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
		   <tr><td class="feed_header">EDIT POST</td><td align=right class="feed_option"><a href="open.cfm"><img src="/images/delete.png" border=0 width=20></a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
          </table>



          <form action="post_db.cfm" method="post" enctype="multipart/form-data" >

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td valign=top>

          <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_sub_header"><b>Title</b></td></tr>
				   <tr><td colspan=2><input type="text" class="input_text" name="feed_title" style="width: 750px;" value="#feed.feed_title#" required maxlength="100"></td></tr>
				   <tr><td class="feed_sub_header"><b>Message</b></td></tr>
				   <tr><td colspan=2><textarea name="feed_desc" class="input_textarea" style="width: 750px;" rows=5>#feed.feed_desc#</textarea></td></tr>
				   <tr><td class="feed_sub_header"><b>URL Reference</b></td></tr>
				   <tr><td colspan=2><input type="url" name="feed_URL" class="input_text" style="width: 750px;" value="#feed.feed_url#" maxlength="200"></td></tr>
                   <tr><td class="feed_sub_header" valign=top>Picture / Image</td></tr>
                   <tr><td class="feed_sub_header" style="font-weight: normal;">

                   <cfif #feed.feed_pic# is "">
					  <input type="file" name="feed_pic">
					<cfelse>
					  <img src="#media_virtual#/#feed.feed_pic#" width=150><br><br>
					  <input type="file" name="feed_pic"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
					 </cfif>

                   </td></tr>

                   <tr><td class="link_small_gray">Image will be auto scaled to 900px (width) by 250px (height)</td></tr>

          </cfoutput>

  			       <tr><td class="feed_sub_header">Post Category</td>
  			           <td class="feed_sub_header">Tags or Keywords</td></tr>
			       <tr><td width=400>
					  <select class="input_select" name="feed_category_id" style="width: 350px;" required>
					   <cfoutput query="lens">
						<option value=#lens_id# <cfif feed.feed_category_id is lens_id>selected</cfif>>#lens_name#
					   </cfoutput>
					  </select>

           <cfoutput>

					  </td><td><input type="text" class="input_text" name="feed_keywords" style="width: 350px;" value="#feed.feed_keywords#" required maxlength="100"></td>

           <input type="hidden" name="i" value="#i#">

           </cfoutput>

					  </tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>
				   <tr><td colspan=2><input type="submit" name="button" value="Save" class="button_blue_large"></td></tr>

			  </table>

		  </td></tr>

          </table>

 		  </form>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td><td valign=top>

           <cfinclude template="network.cfm">
           <cfinclude template="about.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>