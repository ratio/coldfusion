<cfinclude template="/exchange/security/check.cfm">

<cfif option is 1>

    <cfset pipeline_id = #decrypt(deal_pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

<cfelse>

	<cfquery name="insert_pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pipeline
	 (
	  pipeline_name,
	  pipeline_usr_id,
	  pipeline_hub_id,
	  pipeline_created,
	  pipeline_updated
	 )
	 values
	 (
	 '#pipeline_name#',
	  #session.usr_id#,
	  #session.hub#,
	  #now()#,
	  #now()#
	 )
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(pipeline_id) as id from pipeline
	</cfquery>

	<cfset pipeline_id = #max.id#>

</cfif>

<cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from deal
  where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select deal_id from deal
 where deal_pipeline_id = #pipeline_id# and
	   deal_hub_id = #session.hub#
</cfquery>


	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_url,
	  deal_desc,
	  deal_created,
	  deal_updated,
	  deal_owner_id,
	  deal_pipeline_id,
	  deal_hub_id,
	  deal_keywords,
	  deal_release_date,
	  deal_close_date,
	  deal_value_total,
	  deal_contract_number,
	  deal_customer_name,
	  deal_dept_code,
	  deal_agency_code,
	  deal_office_code,
	  deal_pop_city,
	  deal_pop_state,
	  deal_naics,
	  deal_psc,
	  deal_past_sol,
	  deal_current_sol,
	  deal_award_id,
	  deal_contract_id,
	  deal_sbir_id,
	  deal_grant_id,
	  deal_challenge_id,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal.deal_url#',
	 '#deal_desc#',
	  #now()#,
	  #now()#,
	  #session.usr_id#,
	  #pipeline_id#,
	  #session.hub#,
	 '#deal.deal_keywords#',
	 <cfif deal.deal_release_date is "">null<cfelse>'#deal.deal_release_date#'</cfif>,
	 <cfif deal.deal_close_date is "">null<cfelse>'#deal.deal_close_date#'</cfif>,
	 <cfif deal.deal_value_total is "">null<cfelse>#deal.deal_value_total#</cfif>,

	 '#deal.deal_contract_number#',
	 '#deal.deal_customer_name#',
	 '#deal.deal_dept_code#',
	 '#deal.deal_agency_code#',
	 '#deal.deal_office_code#',
	 '#deal.deal_pop_city#',
	 '#deal.deal_pop_state#',
	 '#deal.deal_naics#',
	 '#deal.deal_psc#',
	 '#deal.deal_past_sol#',
	 '#deal.deal_current_sol#',
     <cfif deal.deal_award_id is "">null<cfelse>#deal.deal_award_id#</cfif>,
	 <cfif deal.deal_contract_id is "">null<cfelse>#deal.deal_contract_id#</cfif>,
	 <cfif deal.deal_sbir_id is "">null<cfelse>#deal.deal_sbir_id#</cfif>,
	 <cfif deal.deal_grant_id is "">null<cfelse>#deal.deal_grant_id#</cfif>,
	 <cfif deal.deal_challenge_id is "">null<cfelse>#deal.deal_challenge_id#</cfif>,
	 '#deal.deal_type#'
	  )
	</cfquery>

<cfif isdefined("send_notification")>

	<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfquery name="from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="to" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from pipeline
	 join usr on usr_id = pipeline_usr_id
	 where pipeline_id = #pipeline_id#
    </cfquery>

	<cfmail from="#from.usr_first_name# #from.usr_last_name# <noreply@ratio.exchange>"
			  to="#tostring(tobinary(to.usr_email))#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#from.usr_first_name# #from.usr_last_name# posted an opportunity to your Opportunity Board">
	<html>
	<head><cfoutput>
	<title>#h_info.hub_name#</title>
	</head></cfoutput>
	<body class="body">

	<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
	 <tr><td style="feed_sub_header"><b>Hi #to.usr_first_name#,</b></td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;">#from.usr_first_name# #from.usr_last_name# posted an opportunity to your Opportunity Board.</td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;"><b>Opportunity Details</b></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;">#deal_name#</td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;">#deal_desc#</td></tr>
	</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: ffffff;">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">The Opportunity has been successfully saved.</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>