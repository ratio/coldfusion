<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Join HUB">

	<cftransaction>

			<cfquery name="user_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from usr
			 where usr_id = '#session.usr_id#'
			</cfquery>

			<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 delete comm_xref
			 where comm_xref_comm_id = #session.community_id# and
				   comm_xref_usr_id = #session.usr_id#
			</cfquery>

			<cfquery name="add_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert comm_xref(comm_xref_usr_id, comm_xref_comm_id, comm_xref_active, comm_xref_default, comm_xref_joined, comm_xref_usr_role)
			 values(#session.usr_id#,#session.community_id#,0,null,#now()#,2)
			</cfquery>

			<cfquery name="comm_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from comm
			 left join usr on usr_id = hub_owner_id
			 where comm_id = #session.community_id#
			</cfquery>

	</cftransaction>

	<cfmail from="EXCHANGE <noreply@ratio.exchange>"
			  to="#hub_info.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="New HUB User - #hub_info.hub_name#">

	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head><div class="center">
	<body class="body">
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <cfoutput>

		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>You have a new user who registered for the #hub_info.hub_name# Hub.</b></td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>User Name</b> - #user_info.usr_first_name# #user_info.usr_last_name#</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Email Address</b> - #user_info.usr_email#</td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Message</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(message,"#chr(10)#","<br>","all")#</b></td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">EXCHANGE Support Team</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
		 <tr><td>&nbsp;</td></tr>
	 </cfoutput>
	</table>
	</body>
	</html>
	</cfmail>

	<cfmail from="#hub_info.hub_name# <noreply@ratio.exchange>"
			  to="#user_info.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Welcome to #hub_info.hub_name#">

	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head><div class="center">
	<body class="body">
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <cfoutput>

		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank you for joining the #hub_info.hub_name# HUB.</b></td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">We are processing your request and will email you when your access has been approved.  We look forward to seeing you online and if you have any questions please don't hestiate to ask.</td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>#hub_info.hub_support_name#</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_email#</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_phone#</td></tr>

	 </cfoutput>
	</table>

	</body>
	</html>
	</cfmail>

    <!--- Notify HUB Admins --->

    <cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select hub_xref_usr_id, usr_email from hub_xref
	   left join usr on usr_id = hub_xref_usr_id
	   where hub_xref_usr_role = 1 and
	         hub_xref_hub_id = #id# and
	         hub_xref_usr_id <> #hub_info.hub_owner_id#
	</cfquery>

    <cfloop query="hub_admin">

	<cfmail from="EXCHANGE <noreply@ratio.exchange>"
			  to="#hub_admin.usr_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="New HUB User - #hub_info.hub_name#">

	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head><div class="center">
	<body class="body">
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <cfoutput>

		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>You have a new user who registered for the #hub_info.hub_name# Hub.</b></td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>User Name</b> - #user_info.usr_first_name# #user_info.usr_last_name#</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Email Address</b> - #user_info.usr_email#</td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Message</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(message,"#chr(10)#","<br>","all")#</b></td></tr>
         <tr><td>&nbsp;</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">EXCHANGE Support Team</td></tr>
		 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
		 <tr><td>&nbsp;</td></tr>
	 </cfoutput>
	</table>
	</body>
	</html>
	</cfmail>

    </cfloop>

	<cflocation URL="/exchange/marketplace/hubs/profile.cfm?hub_id=#id#&u=1" addtoken="no">

</cfif>

