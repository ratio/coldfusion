<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="manager" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #comm_profile.comm_owner_id#
 </cfquery>

  <cfquery name="access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select comm_xref_usr_id from comm_xref
   where comm_xref_comm_id = #session.community_id# and
   comm_xref_usr_role = 1 and
   comm_xref_usr_id = #session.usr_id#
 </cfquery>

<div class="comm_right_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_header">Community Manager</td>
			    <td class="feed_header" align=right>
			        <cfif access.recordcount is 1>
			    	<a href="/exchange/marketplace/communities/admin/"><img src="/images/icon_config.png" width=20 border=0 alt="Manager Tools" title="Manager Tools"></a>
			    	</cfif>
			    </td></tr>
           	<tr><td colspan=2><hr></td></tr>
           	<tr><td height=5></td></tr>
        </table>

 	   <cfoutput>
 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=90>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <cfif #manager.usr_photo# is "">
				  <tr><td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(manager.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/headshot.png" width=75 border=0></a></td></tr>
				 <cfelse>
				  <tr><td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(manager.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 180px;" src="#media_virtual#/#manager.usr_photo#" width=75 border=0></a></td></tr>
				 </cfif>
			   </table>

		   </td><td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   	<tr><td class="feed_header" style="padding-top: 0px; padding-bottom: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(manager.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#manager.usr_first_name# #manager.usr_last_name#</a></td></tr>
			   	<tr><td class="feed_option" style="font-weight: normal; padding-top: 2px; padding-bottom: 0px;"><b>#ucase(manager.usr_company_name)#</b></td>
			   	<tr><td class="feed_option" style="font-weight: normal; padding-top: 3px; padding-bottom: 0px;">#manager.usr_title#</td></tr>
			   	<tr><td class="feed_option" style="font-weight: normal; padding-top: 3px; padding-bottom: 0px;">#tostring(tobinary(manager.usr_email))#</td></tr>
			   </table>

		   </td></tr>

		</table>
		</cfoutput>

    </td></tr>

</table>

</div>