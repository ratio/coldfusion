<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=13" rel="stylesheet" type="text/css">
</head><div class="center">

<cfinclude template = "/exchange/include/header.cfm">

<cfset member = 0>
<cfset admin = 0>

<cfset start_date = #dateadd("d",-1,now())#>

<cfif not isdefined("session.document_filter")>
 <cfset session.document_filter = 0>
</cfif>

<cfif not isdefined("session.document_keyword")>
 <cfset session.document_keyword = 0>
</cfif>

<cfquery name="comm_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comm
 where comm_id = #session.community_id#
</cfquery>

<cfquery name="documents" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comm_doc
 join usr on usr_id = comm_doc_usr_id
 where comm_doc_comm_id = #session.community_id#

       <cfif session.document_keyword is not 0>
        and (comm_doc_name like '%#session.document_keyword#%' or comm_doc_desc like '%#session.document_keyword#%')
       </cfif>

       <cfif session.document_filter is not 0>
        and comm_doc_cat_id = #session.document_filter#
       </cfif>

</cfquery>

<cfif comm_profile.comm_owner_id is #session.usr_id#>
 <cfset member = 1>
 <cfset admin = 1>
</cfif>

<cfquery name="membership" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comm_xref
 where comm_xref_usr_id = #session.usr_id# and
       comm_xref_comm_id = #session.community_id# and
       comm_xref_active = 1
</cfquery>

<cfquery name="doc_cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from doc_cat
 where doc_cat_hub_id = #session.hub#
 order by doc_cat_order
</cfquery>

 <cfif membership.recordcount GT 0>
  <cfset member = 1>
 </cfif>

<style>
.image_home {
    <cfif #comm_profile.comm_banner# is "">
    background-image: url('<cfoutput>#image_virtual#/comm_stock_banner.jpg</cfoutput>');
    <cfelse>
    background-image: url('<cfoutput>#media_virtual#/#comm_profile.comm_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.comm_right_box {
    width: 300px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_news.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="open.cfm">Posts</a></span>
          </div>

          <cfif comm_profile.comm_calendar is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_calendar.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="calendar.cfm">Shared Calendar</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_documents is 1>
          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_document.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="docs.cfm">Document Library</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_opportunity is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_calendar.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="opps.cfm">Opportunity Board</a></span>
          </div>
          </cfif>

		<div class="main_box_2">

        <center>

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(comm_profile.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_profile.comm_name#</a></td>
			   <td align=right class="feed_sub_header">

		       <cfif membership.comm_xref_documents is 1>
		       <img src="/images/plus3.png" width=15 hspace=10><a href="doc_add.cfm">Add Document</a>
		       </cfif>

			   <img src="/images/icon_list.png" width=20 hspace=10><a href="/exchange/marketplace/communities/">All Communities</a></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
		   <tr><td height=10></td></tr>
		  </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="set_document_filter.cfm" method="post">

		   <tr><td class="feed_header" valign=middle>Document Library</td>

		       <td align=right class="feed_sub_header" style="font-weight: normal;" valign=middle>
		       <b>Search:</b>&nbsp;&nbsp;

		       <input type="text" name="document_keyword" <cfif session.document_keyword is not 0>value="#session.document_keyword#"</cfif> class="input_text" placeholder="enter keyword" style="width: 200px;">

		       &nbsp;&nbsp;

               </cfoutput>

               <select name="document_filter" class="input_select" style="width: 200px;">
               <option value=0>All Categories
               <cfoutput query="doc_cat">
               <option value=#doc_cat_id# <cfif session.document_filter is doc_cat_id>selected</cfif>>#doc_cat_name#
               </cfoutput>
               </select>

               &nbsp;<input type="submit" name="button" class="button_blue" value="Search">
               &nbsp;<input type="submit" name="button" class="button_blue" value="Clear">

               <cfoutput>

		       </td>
               </tr>

           </form>

		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <cfif isdefined("u")>
		    <cfif u is 1>
		     <tr><td class="feed_sub_header" style="color: green;">Document has been successfully added.</td></tr>
		    <cfelseif u is 2>
		     <tr><td class="feed_sub_header" style="color: green;">Document has been successfully updated.</td></tr>
		    <cfelseif u is 3>
		     <tr><td class="feed_sub_header" style="color: red;">Document has been successfully deleted.</td></tr>
		    </cfif>
		    <tr><td height=10></td></tr>
		   </cfif>

		  </table>

		  </cfoutput>

			  <cfif documents.recordcount is 0>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">No documents have been posted.</td></tr>
			  </table>

			  <cfelse>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <cfoutput query="documents">

			  <tr><td width=90 valign=top>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr>
				 <cfif #usr_photo# is "">
				  <td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="margin-top: 15px;" src="#image_virtual#headshot.png" height=65 width=65 border=0 alt="#usr_full_name#" title="#usr_full_name#"></td></tr>
				 <cfelse>
				  <td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="margin-top: 15px; border-radius: 2px;" src="#media_virtual#/#usr_photo#" height=65 width=65 border=0 alt="#usr_full_name#" title="#usr_full_name#"></td></tr>
				 </cfif>
				 </tr>

			 </table>

			 </td><td>

             <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" style="padding-bottom: 5px;">

                <cfif comm_doc_file is not "">
	                <a href="#media_virtual#/#comm_doc_file#" target="_blank" rel="noopener" rel="noreferrer">#comm_doc_name#</a>
                <cfelse>
	                <a href="#comm_doc_url#" target="_blank" rel="noopener" rel="noreferrer">#comm_doc_name#</a>
                </cfif>

                </td>

                <td class="feed_sub_header" align=right>#dateformat(comm_doc_created,'mmm d, yyyy')#</td></tr>
                <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#replace(comm_doc_desc,"#chr(10)#","<br>","all")#</td></tr>

                <cfif comm_doc_file is not "">

					<tr><td class="link_small_gray" style="font-weight: normal; padding-top: 0px;">

					<cfset myfile = #media_path# & "\" & #comm_doc_file#>
					<cfset fileinfo = #getfileinfo(myfile)#>
					<cfset filesize = #numberformat(evaluate(fileinfo.size/1000),'9,999')#>
					<cfset fileextension = #listlast(myfile,".")#>


					<a href="#media_virtual#/#comm_doc_file#" target="_blank" rel="noopener" rel="noreferrer">Download (#ucase(fileextension)#,#filesize#k)

					</td>

				<cfelse>
					<tr><td class="link_small_gray" style="font-weight: normal; padding-top: 0px;">
					 <a href="#comm_doc_url#" target="_blank" rel="noopener" rel="noreferrer">#comm_doc_url#</a>
					</td>
				</cfif>

				<td align=right class="link_small_gray">

				<cfif #comm_doc_usr_id# is #session.usr_id#>
				 <a href="doc_edit.cfm?i=#encrypt(comm_doc_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Edit</a>
				</cfif>

				</td>


                </tr>

                </table>

                </td></tr>

                <tr><td colspan=2><hr></td></tr>

			  </cfoutput>

			  </table>

			  </cfif>

		 </center>

	  </div>

      </td><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=45></td></tr>
        </table>

		<cfinclude template="about.cfm">

        <cfif member is 1>
			<cfinclude template="network.cfm">
			<cfinclude template="companies.cfm">
	    </cfif>

		<cfinclude template="manager.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>