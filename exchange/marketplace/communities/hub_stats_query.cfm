
<!--- Get Members --->

 <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from usr
   left join hub_xref on hub_xref_usr_id = usr_id
   left join company on company_id = usr_company_id
   where hub_xref_hub_id = #cid# and
	     hub_xref_active = 1
   order by usr_last_name, usr_first_name
 </cfquery>

<!--- Get Hub Companies --->

<cfquery name="child" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_id from hub
 where hub_parent_hub_id = #cid#
</cfquery>

<cfif child.recordcount is 0>
 <cfset child_list = 0>
<cfelse>
 <cfset child_list = #valuelist(child.hub_id)#>
</cfif>

<cfquery name="comp1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(distinct(hub_comp_id)) as total from hub_comp
 where hub_comp_hub_id = #cid# or hub_comp_hub_id in (#child_list#)
</cfquery>

<!--- Get Child Companies --->

 <cfquery name="children" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_parent_hub_id = #cid#
  order by hub_name
 </cfquery>