<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>
<cfset #session.search_id# = 0>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=13" rel="stylesheet" type="text/css">
</head><div class="center">

<style>
.child_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 320px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select comm_id from comm
   join comm_xref on comm_xref_comm_id = comm_id and
        comm_xref_hub_id = #session.hub# and
        comm_xref_usr_id = #session.usr_id# and
        comm_type_id = 2
 </cfquery>

 <cfif private.recordcount is 0>
  <cfset private_list = 0>
 <cfelse>
  <cfset private_list = valuelist(private.comm_id)>
 </cfif>

 <cfquery name="children" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_hub_id = #session.hub#
        and (comm_type_id = 1 or comm_type_id = 3 or comm_owner_id = #session.usr_id# or comm_id in (#private_list#))

	   <cfif isdefined("session.network_keyword")>
		and comm_name like '%#session.network_keyword#%'
	   </cfif>

  order by comm_name
 </cfquery>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/components/my_communities/index.cfm">

      </td><td valign=top>

		<div class="main_box">

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <cfoutput>
				   <tr>

				       <form action="set.cfm" method="post">
				       <td class="feed_header">Communities</td>

				       <td align=right>
				       <span class="feed_sub_header">Find Community</span>&nbsp;&nbsp;
				       <input type="text" class="input_text" name="network_keyword" style="width: 200px;" <cfif isdefined("session.network_keyword")>value="#session.network_keyword#"<cfelse> placeholder="keyword" </cfif>>
				       &nbsp;
				       <input type="submit" name="button" class="button_blue" value="Search">
				       &nbsp;
				       <input type="submit" name="button" class="button_blue" value="All">

				       </td>

				       </form>

				   </tr>

			  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Communities are groups of People and Companies focused on a specific market, sector or capability.</td></tr>

				   </cfoutput>
				  </table>
        </div>

		<div class="main_box">

		  <center>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		       <cfif isdefined("session.network_keyword")>
              <tr><td class="feed_header">Search Results

              <cfoutput>
              <cfif children.recordcount is 1>
               - 1 Community Found
              <cfelseif children.recordcount GT 1>
               #children.recordcount# Communities Found
              </cfif>
              </cfoutput>


              </td></tr>

              </cfif>
              <tr><td height=10></td></tr>

			  <tr><td valign=top>

                  <cfif children.recordcount is 0>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr><td class="feed_sub_header" style="font-weight: normal;">No Communities were found.</td></tr>
                  </table>

                  <cfelse>
                  <p>

                        <cfloop query="children">

                        <!--- Get Members --->

						 <cfquery name="child_members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						   select count(usr_id) as total from usr
						   left join comm_xref on comm_xref_usr_id = usr_id
						   where comm_xref_comm_id = #children.comm_id# and
								 comm_xref_active = 1
						 </cfquery>

                        <!--- Get comm Companies --->

						<cfquery name="child_companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select distinct(usr_company_id) from comm_xref
						 join usr on usr_id = comm_xref_usr_id
						 where comm_xref_comm_id = #children.comm_id# and
						       comm_xref_active = 1
						</cfquery>

                        <!--- Get opps --->

						<cfquery name="opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select count(deal_id) as total from deal
						 where deal_comm_id = #children.comm_id#
						</cfquery>

						<div class="child_badge">

				        <cfoutput>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							   <tr><td width=70>

 							     <table cellspacing=0 cellpadding=0 border=0 width=100%>

								   <tr><td><a href="s.cfm?i=#encrypt(children.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

									<cfif #children.comm_logo# is "">
									  <img src="#image_virtual#/stock_community.png" width=60 border=0>
									<cfelse>
									  <img src="#media_virtual#/#children.comm_logo#" width=60 border=0>
									</cfif>

									</a>

									</td></tr>

							      </table>

							    </td><td>

									 <table cellspacing=0 cellpadding=0 border=0 width=100%>
									 <tr><td>

										 <table cellspacing=0 cellpadding=0 border=0 width=100%>
										  <tr><td class="feed_title"><a href="/exchange/marketplace/communities/s.cfm?i=#encrypt(children.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#children.comm_name#</a></td></tr>
										  <tr><td class="feed_option">
										  <b>

										   <cfif #children.comm_city# is "" and #children.comm_state# is "">
										   <br>
										   <cfelse>
										     #children.comm_city#
										    <cfif #children.comm_state# is not 0>#children.comm_state#</cfif>
										  </cfif>

										  </b></td></tr>
										  <tr><td class="feed_option" style="padding-top: 0px;">
										  Members <b>#child_members.total#</b>&nbsp;|&nbsp;
										  Companies <b>#child_companies.recordcount#</b>&nbsp;|&nbsp;
										  Opportunities <b>#opps.total#</b>

										  </td></tr>
										 </table>

									 </td></tr>
								  </table>

                               </td></tr>

							   <tr><td colspan=2><hr></td></tr>

							   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">
							   <cfif children.comm_desc is "">
							    No description provided.
							   <cfelse>
							   <cfif len(children.comm_desc) GT 370>#left(children.comm_desc,'370')#...<cfelse>#children.comm_desc#</cfif></td></tr>
							   </cfif>


                             </table>

 					      </cfoutput>
 					    </div>

                        </cfloop>

                       </cfif>

             </td></tr>
         </table>

     </td></tr>

  </table>


  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>