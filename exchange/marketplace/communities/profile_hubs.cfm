<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

 <cfquery name="hub_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub
  where hub_id = #cid#
 </cfquery>

 <cfquery name="hub_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  left join company on company_id = usr_company_id
  where usr_id = #hub_profile.hub_owner_id#
 </cfquery>

 <cfinclude template="hub_stats_query.cfm">

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top>

		<div class="main_box">

        <center>

<style>
.image_home {
    <cfif #hub_profile.hub_banner# is "">
    background-image: url('/images/hub_stock_banner.jpg');
    <cfelse>
    background-image: url('#media_virtual#/<cfoutput>#hub_profile.hub_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_hub_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 0px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}
.hub_join_button {
    font-family: calibri, arial;
    font-size: 20px;
    border-radius: 2px;
    width: auto;
    height: 40px;
    border: 0px;
    padding-left: 15px;
    padding-right: 15px;
    margin-right: 40px;
    background-color: #FFFFFF;
    curson: pointer;
    color: #000000;
    font-weight: bold;
}
.child_badge {
    width: 98%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 325px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

		  <table cellspacing=0 cellpadding=0 border=0 width=99%>

			  <tr><td valign=top>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header"><a href="profile.cfm?hub_id=#hub_profile.hub_id#">#ucase(hub_profile.hub_name)#</a></td>
				       <td align=right><a href="/exchange/marketplace/communities/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
				  <tr><td>&nbsp;</td></tr>
				  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home">

                   <td>

                   <cfinclude template="hub_header.cfm">

                   </td>

                   </tr>

                  </table>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=20></td></tr>

				   <tr><td width=5></td><td valign=top width=73%>

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_header">COMMUNITIES</td></tr>
                        <tr><td><hr></td></tr>
                       </table>

                       <cfif children.recordcount is 0>

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_sub_header" style="font-weight: normal;">There are no sub communities associated with this Community.</td></tr>
                       </table>

                       <cfelse>

                        <cfloop query="children">

                        <!--- Get Members --->

						 <cfquery name="child_members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						   select count(usr_id) as total from usr
						   left join hub_xref on hub_xref_usr_id = usr_id
						   left join company on company_id = usr_company_id
						   where hub_xref_hub_id = #children.hub_id# and
								 hub_xref_active = 1
						 </cfquery>

                        <!--- Get Hub Companies --->

						<cfquery name="child_companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select count(company_id) as total from company
						 where company_hub_id = #children.hub_id#
						</cfquery>

						<cfquery name="ismember" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select * from hub_xref
						 where hub_xref_usr_id = #session.usr_id# and
						 hub_xref_hub_id = #children.hub_id#
						</cfquery>

                        <p>
						<div class="child_badge">

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							   <cfoutput>

							   <tr><td width=120>

 							     <table cellspacing=0 cellpadding=0 border=0 width=100%>

								   <tr><td>

                                    <a href="/exchange/marketplace/hubs/profile.cfm?hub_id=#children.hub_id#">

									<cfif #children.hub_logo# is "">
									  <img src="/images/no_logo.png" width=60 border=0>
									<cfelse>
									  <img src="#media_virtual#/#children.hub_logo#" width=100 border=0>
									</cfif>

									</a>

									</td></tr>

							      </table>

							    </td><td>

									 <table cellspacing=0 cellpadding=0 border=0 width=100%>
									  <tr><td>
									 <table cellspacing=0 cellpadding=0 border=0 width=100%>
									  <tr><td class="feed_title"><a href="/exchange/marketplace/hubs/profile.cfm?hub_id=#children.hub_id#">#children.hub_name#</a></td></tr>
									  <tr><td class="feed_option"><b>#children.hub_city#, #children.hub_state#</b></td></tr>
									  <tr><td class="feed_option" style="padding-top: 0px;">Members <b>#child_members.total#</b> | Companies <b>#child_companies.total#</b></td></tr>
                                     </table>

									 </td><td align=right width=50 valign=top>

									 <table cellspacing=0 cellpadding=0 border=0 width=100%>
									  <tr><td height=10></td></tr>
									  <tr><td>

										 <cfif ismember.recordcount is 1>
										   <input class="button_blue" type="submit" name="button" value="Member" style="background-color: green;" onclick="window.open('/exchange/marketplace/hubs/set.cfm?hub_id=#children.hub_id#');">
										 <cfelse>
										   <input class="button_blue" type="submit" name="button" value="Join" onclick="window.open('/exchange/marketplace/hubs/profile.cfm?hub_id=#children.hub_id#');">
										 </cfif>

									 </td></tr>
                                     </table>

									 </td></tr>
                                     </table>

						         </td></tr>
							     </table>

 							     <table cellspacing=0 cellpadding=0 border=0 height=140 width=100%>
								   <tr><td colspan=2><hr></td></tr>
								   <tr><td colspan=2 class="feed_option" valign=top height=75><b>Overview</b><br><cfif len(children.hub_desc) GT 270>#left(children.hub_desc,'270')#...<cfelse>#children.hub_desc#</cfif></td></tr>
								   <tr><td colspan=2><hr></td></tr>
								   <tr><td colspan=2 class="feed_option" height=60 valign=top><b>Focus Area(s)</b><br><cfif #children.hub_tags# is "">Not provided<cfelse>#children.hub_tags#</cfif></td></tr>
							     </table>

 					   </cfoutput>

						</div>

                        </cfloop>

                       </cfif>

				       </td><td width=50>&nbsp;</td><td valign=top width=20%>

					   <cfinclude template="hub_stats.cfm">

				       <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td height=10></td></tr>
                        <tr><td class="feed_header" align=center>MANAGER</td></tr>
                        <tr><td height=10></td></tr>
	                    <cfif #hub_admin.usr_photo# is "">
	                     <tr><td align=center><a href="/exchange/profile/"><img src="/images/headshot.png" width=125 border=0></a></td></tr>
	                    <cfelse>
	                     <tr><td align=center><a href="/exchange/member/profile.cfm?h=#hub_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#hub_admin.usr_photo#" width=125 border=0></a></td></tr>
	                    </cfif>

                        <tr><td height=10></td></tr>

                        <tr><td class="feed_sub_header" align=center><b>#hub_admin.usr_first_name# #hub_admin.usr_last_name#</b></td></tr>

					    <cfif #hub_admin.usr_company_id# is not 0>
						 <tr><td class="feed_option" align=center><b>#hub_admin.company_name#</b></td></tr>
					    </cfif>

					    <tr><td class="feed_option" align=center>#hub_admin.usr_email#</td></tr>
					    <tr><td class="feed_option" align=center>#hub_admin.usr_phone#</td></tr>

                       </table>

				       </td>
				   </tr>
				   </table>

				  </cfoutput>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>