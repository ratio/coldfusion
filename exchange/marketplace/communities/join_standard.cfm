<cfif isdefined("u")>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header">Thank you for requesting access to this Community.<td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">We are processing your request now and will let you know if we have any questions.<td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">Thank you,</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">Community Manager</td></tr>
		</table>

<cfelse>

    <cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     select * from comm_xref
     where comm_xref_usr_id = #session.usr_id# and
           comm_xref_comm_id = #session.community_id# and
           comm_xref_active = 0
    </cfquery>

    <cfif check.recordcount is 1>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td height=10></td></tr>
			 <tr><td class="feed_header">You have already requested to join the <cfoutput>#comm_profile.comm_name#</cfoutput> Community.<td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">Your request to join this Community has already been received.   We are still processing it and you will be notified when it is approved or if we have any questions.</td></tr>

			 <tr><td class="feed_sub_header" style="font-weight: normal;">Thank You.</td></tr>


			 <tr><td class="feed_sub_header" style="font-weight: normal;">Community Manager</td></tr>

			 <tr><td height=5></td></tr>

		</table>

    <cfelse>

	<form action="join_standard_process.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td height=10></td></tr>
			 <tr><td class="feed_header">Welcome to the <cfoutput>#comm_profile.comm_name#</cfoutput> Community<td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">
				 This Community requires approval from the Community Manager before joining.  Please provide a brief description of your background and why you would like to join this Community.
				 </td></tr>

			 <cfoutput>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><b>From: </b> #info.usr_first_name# #info.usr_last_name#</td></tr>
			 </cfoutput>

			 <tr><td><textarea name="join_message" class="input_textarea" style="width: 900px; height: 150px;"></textarea></td></tr>
			 <tr><td height=10></td></tr>
			 <tr><td><input type="submit" name="button" value="Request Approval" class="button_blue_large"></td></tr>
			 <tr><td height=5></td></tr>

		</table>

	</form>

	</cfif>

</cfif>