<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="about" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

<div class="comm_right_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>
				<tr><td class="feed_header">About this Community</td></tr>
            	<tr><td colspan=3><hr></td></tr>
		    	<tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #trim(about.comm_desc)# is "">No description has been provided.<cfelse>#about.comm_desc#</cfif></td></tr>
	    	 	<tr><td class="feed_sub_header" style="font-weight: normal;"><b>Location</b> -
		    	 	<cfif #about.comm_city# is "" and (#about.comm_state# is "" or #about.comm_state# is 0)>
		    	 	 Not Specified
		    	 	<cfelse>
		    	 	 #about.comm_city#
		    	 	 <cfif #about.comm_state# is not 0>#about.comm_state#</cfif>
                    </cfif>
		    	 	</td></tr>

		    </cfoutput>

		</table>

    </td></tr>

</table>

</div>