<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select deal_image from deal
		  where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif fileexists("#media_path#\#remove.deal_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.deal_image#">
		</cfif>

	</cfif>

	<cfif #deal_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select deal_image from deal
		  where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.deal_image# is not "">

		 <cfif fileexists("#media_path#\#getfile.deal_image#")>
		   <cffile action = "delete" file = "#media_path#\#getfile.deal_image#">
		 </cfif>

		</cfif>

		<cffile action = "upload"
		 fileField = "deal_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update deal
      set deal_name = '#deal_name#',
          deal_url = '#deal_url#',
          deal_past_sol = '#deal_past_sol#',
          deal_current_sol = '#deal_current_sol#',
          deal_dept_code = '#deal_dept_code#',
          deal_type = '#deal_type#',
          deal_agency_code = '#deal_agency_code#',
          deal_pop_city = '#deal_pop_city#',
          deal_pop_state = '#deal_pop_state#',
          deal_naics = '#deal_naics#',
          deal_psc = '#deal_psc#',
          deal_customer_name = '#deal_customer_name#',
          deal_contract_number = '#deal_contract_number#',
		  deal_desc = '#deal_desc#',
		  deal_keywords = '#deal_keywords#',
		  deal_release_date = <cfif #deal_release_date# is "">null<cfelse>'#deal_release_date#'</cfif>,
		  deal_close_date = <cfif #deal_close_date# is "">null<cfelse>'#deal_close_date#'</cfif>,
		  deal_value_total = <cfif #deal_value_total# is "">null<cfelse>#deal_value_total#</cfif>,

		  <cfif #deal_image# is not "">
		   deal_image = '#cffile.serverfile#',
		  <cfelse>
		   deal_image = null,
		  </cfif>

		  deal_updated = #now()#

      where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cflocation URL="opp_open.cfm?i=#i#&u=10" addtoken="no">

<cfelseif #button# is "Add Opportunity">

	<cfif #deal_image# is not "">
		<cffile action = "upload"
		 fileField = "deal_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
      deal_name,
      deal_url,
      deal_past_sol,
      deal_current_sol,
	  deal_pop_city,
	  deal_pop_state,
	  deal_naics,
	  deal_psc,
      deal_customer_name,
      deal_contract_number,
      deal_release_date,
      deal_close_date,
      deal_value_total,
      deal_keywords,
      deal_desc,
      deal_dept_code,
      deal_agency_code,
      deal_image,
      deal_owner_id,
      deal_hub_id,
      deal_created,
      deal_updated,
      deal_type,
      deal_comm_id
      )
      values
      (
      '#deal_name#',
      '#deal_url#',
      '#deal_past_sol#',
      '#deal_current_sol#',
      '#deal_pop_city#',
      '#deal_pop_state#',
      '#deal_naics#',
      '#deal_psc#',
      '#deal_customer_name#',
      '#deal_contract_number#',
      <cfif #deal_release_date# is "">null<cfelse>'#deal_release_date#'</cfif>,
      <cfif #deal_close_date# is "">null<cfelse>'#deal_close_date#'</cfif>,
      <cfif #deal_value_total# is "">null<cfelse>#deal_value_total#</cfif>,
      '#deal_keywords#',
      '#deal_desc#',
      '#deal_dept_code#',
      '#deal_agency_code#',
       <cfif #deal_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
       #session.usr_id#,
       <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
       #now()#,
       #now()#,
      '#deal_type#',
       #session.community_id#
      )
	</cfquery>

   	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   	 select max(deal_id) as id from deal
   	</cfquery>

    <cfif #deal_keywords# is not "">

		<cfloop index="k" list="#deal_keywords#">

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into partner_snapshot
			 (
			  partner_snapshot_keyword,
			  partner_snapshot_deal_id
			  )
			  values
			  (
			  '#k#',
			   #max.id#
			   )
			</cfquery>

		</cfloop>

	</cfif>

	<cflocation URL="opps.cfm?u=10" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select deal_image from deal
	  where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cfif remove.deal_image is not "">

     <cfif fileexists("#media_path#\#remove.deal_image#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.deal_image#">
	 </cfif>

	</cfif>

	<cftransaction>

		<cfquery name="delete_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete deal
		 where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfquery name="delete_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete partner_snapshot
		 where partner_snapshot_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

	</cftransaction>

	<cflocation URL="opps.cfm?u=5" addtoken="no">

</cfif>
