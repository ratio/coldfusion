<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_doc_file from comm_doc
		  where comm_doc_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.comm_doc_file#")>
			<cffile action = "delete" file = "#media_path#\#remove.comm_doc_file#">
		</cfif>

	</cfif>

	<cfif #comm_doc_file# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_doc_file from comm_doc
		  where comm_doc_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif #getfile.comm_doc_file# is not "">
         <cfif fileexists("#media_path#\#getfile.comm_doc_file#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.comm_doc_file#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "comm_doc_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update comm_doc
      set comm_doc_name = '#comm_doc_name#',
      	  comm_doc_cat_id = #comm_doc_cat_id#,

		  <cfif #comm_doc_file# is not "">
		   comm_doc_file = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   comm_doc_file = null,
		  </cfif>

      	  comm_doc_desc = '#comm_doc_desc#'
	  where comm_doc_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	  </cfquery>

      <cflocation URL="docs.cfm?u=2" addtoken="no">

<cfelseif #button# is "Post">

	<cfif #comm_doc_file# is not "">
		<cffile action = "upload"
		 fileField = "comm_doc_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="post" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert comm_doc
	  (
       comm_doc_name,
       comm_doc_url,
       comm_doc_cat_id,
       comm_doc_desc,
       comm_doc_file,
       comm_doc_comm_id,
       comm_doc_hub_id,
       comm_doc_usr_id,
       comm_doc_created
	  )
	  values
	  (
      '#comm_doc_name#',
      '#trim(comm_doc_url)#',
       #comm_doc_cat_id#,
      '#comm_doc_desc#',
	  <cfif comm_doc_file is "">null<cfelse>'#cffile.serverfile#'</cfif>,
       #session.community_id#,
       #session.hub#,
       #session.usr_id#,
       #now()#
	  )
	</cfquery>

	<cflocation URL="docs.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select comm_doc_file from comm_doc
		  where comm_doc_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cfif remove.comm_doc_file is not "">
         <cfif fileexists("#media_path#\#remove.comm_doc_file#")>
			 <cffile action = "delete" file = "#media_path#\#remove.comm_doc_file#">
         </cfif>
		</cfif>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete comm_doc
		  where comm_doc_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

		<cflocation URL="docs.cfm?u=3" addtoken="no">

</cfif>

