<cfinclude template="/exchange/security/check.cfm">

<div class="comm_right_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		 <cfquery name="comp_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select distinct(usr_company_id) from usr
		  join comm_xref on comm_xref_usr_id = usr_id
		  where comm_xref_comm_id = #session.community_id# and
				comm_xref_active = 1 and
				usr_company_id is not null
		 </cfquery>

		 <cfif comp_list.recordcount is 0>
		  <cfset clist = 0>
		 <cfelse>
		  <cfset clist = valuelist(comp_list.usr_company_id)>
		 </cfif>

		 <cfquery name="logos" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select * from company
		  where company_id in (#clist#)
		 </cfquery>


		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfoutput>
				<tr><td class="feed_header"><a href="company_profiles.cfm">Companies in Community<cfif #comp_list.recordcount# GT 0> (#comp_list.recordcount#)</cfif></a></td></tr>
			 </cfoutput>

             <tr><td colspan=3><hr></td></tr>

             <cfif comp_list.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies are part of this Community.</td></tr>
             </cfif>

			 <tr><td>

			 <cfoutput query="logos">

                    <cfif company_logo is "">
					  <a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="//logo.clearbit.com/#company_website#" onerror="this.src='/images/no_logo.png'" width=50 style="padding-right: 5px; padding-top: 5px;" border=0></a>
					<cfelse>
                      <a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#company_logo#" width=50 style="padding-right: 5px; padding-top: 5px;" alt="#company_name# Profile" title="#company_name# Profile"></a>
					</cfif>

			 </cfoutput>

             <cfif comp_list.recordcount GT 14>
               <a href="company_profiles.cfm" border=0 alt="All Companies" title="All Companies"><img src="/images/all.png" width=40 style="padding-top: 5px;"></a>
             </cfif>

             </td></tr>

		</table>

    </td></tr>

</table>

</div>