<cfquery name="owner" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_email, comm_name from comm
 join usr on usr_id = comm_owner_id
 where comm_id = #session.community_id#
</cfquery>

<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cftransaction>

	<cfquery name="send" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into comm_xref
	 (
	  comm_xref_usr_id,
	  comm_xref_comm_id,
	  comm_xref_active,
	  comm_xref_joined,
	  comm_xref_join_message,
	  comm_xref_usr_role
	  )
	  values
	  (#session.usr_id#,
	   #session.community_id#,
	   0,
	   #now()#,
	  '#join_message#',
	  0
	  )
	</cfquery>

</cftransaction>

<cfif isdefined("test_email")>
	<cfset #to# = #test_email#>
<cfelse>
	<cfset #to# = #tostring(tobinary(owner.usr_email))#>
</cfif>

<!--- Email Manager --->

<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfmail from="#owner.comm_name# <noreply@ratio.exchange>"
		  to="#to#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="Access Request - #owner.comm_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;">#user.usr_first_name# #user.usr_last_name# has requested access to the #owner.comm_name# Community in the #h_info.hub_name#.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#join_message#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">To grant or deny this user access to the Community, please login to the #h_info.hub_name# and select Members within Community Management tools.</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td><a href="#h_info.hub_login_page#"><img src="#image_virtual#/email_login.png" height=30></a></td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<!--- Email User --->

<cfif isdefined("test_email")>
	<cfset #to# = #test_email#>
<cfelse>
	<cfset #to# = #tostring(tobinary(user.usr_email))#>
</cfif>

<cfmail from="#owner.comm_name# <noreply@ratio.exchange>"
		  to="#to#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="Access Request - #owner.comm_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;">Hi #user.usr_first_name#,</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">We've received your request to join the #owner.comm_name# Community.  We are processing it now and will let you know if we have any questions.</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">Thank you,</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">Community Administrator</td></tr>
</table>

</cfoutput>

</body>
</html>

</cfmail>

<cflocation URL="open.cfm?u=1" addtoken="no">