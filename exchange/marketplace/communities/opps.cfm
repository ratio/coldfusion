<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=13" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">
 <cfinclude template = "check_access.cfm">

 <cfset member = 0>
 <cfset admin = 0>

 <cfif not isdefined("session.opp_filter")>
  <cfset session.opp_filter = 0>
 </cfif>

 <cfquery name="opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from deal
  join usr on usr_id = deal_owner_id
  where deal_comm_id = #session.community_id#

  <cfif session.opp_filter is 1>
   and deal_type = 'Contract'
  <cfelseif session.opp_filter is 2>
   and deal_type = 'Grant'
  <cfelseif session.opp_filter is 3>
   and deal_type = 'Award'
  <cfelseif session.opp_filter is 4>
   and deal_type = 'SBIR/STTR'
  <cfelseif session.opp_filter is 5>
   and deal_type = 'Challenge'
  <cfelseif session.opp_filter is 6>
   and deal_type = 'Other'
  </cfif>

  order by deal_updated DESC

 </cfquery>

 <cfquery name="comm_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfif comm_profile.comm_owner_id is #session.usr_id#>
  <cfset member = 1>
  <cfset admin = 1>
 </cfif>

<style>

.image_home {
    <cfif #comm_profile.comm_banner# is "">
    background-image: url('<cfoutput>#image_virtual#/comm_stock_banner.jpg</cfoutput>');
    <cfelse>
    background-image: url('<cfoutput>#media_virtual#/#comm_profile.comm_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.comm_right_box {
    width: 300px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
.event_badge {
    width: 47%;
    display: inline-block;
    height: 175px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 0px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    background-color: #ffffff;
}
</style>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_news.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="open.cfm">Posts</a></span>
          </div>

          <cfif comm_profile.comm_calendar is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_calendar.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="calendar.cfm">Shared Calendar</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_documents is 1>
          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_document.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="docs.cfm">Document Library</a></span>
          </div>
          </cfif>

          <cfif comm_profile.comm_opportunity is 1>
          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_calendar.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="opps.cfm">Opportunity Board</a></span>
          </div>
          </cfif>

		<div class="main_box_2">

        <center>

		  <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(comm_profile.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_profile.comm_name#</a></td>
			   <td align=right class="feed_sub_header">

               <cfif comm_manager is 1>
		       <img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Opportunity</a>
		       </cfif>

			   <img src="/images/icon_list.png" width=20 hspace=10><a href="/exchange/marketplace/communities/">All Communities</a></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
		   <tr><td height=10></td></tr>
		  </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="set_opp_filter.cfm" method="post">

		   <tr><td class="feed_header" valign=middle>Opportunity Board</td>

		       <td align=right class="feed_sub_header" style="font-weight: normal;" valign=middle>
		       <b>Filter by:</b>&nbsp;&nbsp;

               </cfoutput>

               <select name="opp_filter" class="input_select" onchange=form.submit()>
               <option value=0>All Types
               <option value=1 <cfif session.opp_filter is 1>selected</cfif>>Contracts
               <option value=2 <cfif session.opp_filter is 2>selected</cfif>>Grants
               <option value=3 <cfif session.opp_filter is 3>selected</cfif>>Awards
               <option value=4 <cfif session.opp_filter is 4>selected</cfif>>SBIR/STTRs
               <option value=5 <cfif session.opp_filter is 5>selected</cfif>>Challenges
               <option value=6 <cfif session.opp_filter is 6>selected</cfif>>Other


               </select>

               <cfoutput>

		       </td>
               </tr>

           </form>

		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <cfif isdefined("u")>
		    <cfif u is 1>
		     <tr><td class="feed_sub_header" style="color: green;">Document has been successfully added.</td></tr>
		    <cfelseif u is 3>
		     <tr><td class="feed_sub_header" style="color: red;">Document has been successfully deleted.</td></tr>
		    </cfif>
		    <tr><td height=10></td></tr>
		   </cfif>

		  </table>

		  </cfoutput>

			  <cfif opps.recordcount is 0>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities have been posted.</td></tr>
			  </table>

			  <cfelse>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
                  <td class="feed_sub_header">Posted By</td>
                  <td class="feed_sub_header">Name</td>
                  <td class="feed_sub_header">Customer</td>
                  <td class="feed_sub_header">Type</td>
                  <td class="feed_sub_header" align=right>Posted</td>
                  <td></td>
               </tr>

              <cfset count = 1>

			  <cfoutput query="opps">

               <tr>

					 <cfif #usr_photo# is "">
					  <td width=75><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 border=0 alt="#usr_full_name#" title="#usr_full_name#"></td>
					 <cfelse>
					  <td width=75><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 180px;" src="#media_virtual#/#usr_photo#"  alt="#usr_full_name#" title="#usr_full_name#" height=40 width=40 border=0></td>
					 </cfif>

                  <td class="feed_sub_header" valign=top style="padding-right: 20px;" width=400><a href="opp_open.cfm?i=#encrypt(deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#deal_name#</a></td>
                  <td class="feed_sub_header" valign=top style="font-weight: normal; padding-right: 20px;" width=200>#deal_customer_name#</td>
                  <td class="feed_sub_header" valign=top style="font-weight: normal;" width=75>#deal_type#</td>
                  <td class="feed_sub_header" valign=top style="font-weight: normal;" width=75 align=right>#dateformat(deal_created,'mmm dd')#</td>
                  <td align=right width=35 valign=top><img src="/images/icon_pin.png" style="cursor: pointer; margin-top: 7px;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('pin.cfm?i=#encrypt(deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"></td>

               </tr>

               <cfif count LT #opps.recordcount#>
                <tr><td colspan=6><hr></td></tr>
               </cfif>

               <cfset count = count + 1>


              </cfoutput>

              </table>

              </cfif>

      </td><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=45></td></tr>
        </table>

		<cfinclude template="about.cfm">

        <cfif member is 1>
			<cfinclude template="network.cfm">
			<cfinclude template="companies.cfm">
	    </cfif>

		<cfinclude template="manager.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>