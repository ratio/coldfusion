<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 20px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	display: inline-block;
	margin-left: -4px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	padding-right: 20px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
.event_badge {
    width: 47%;
    display: inline-block;
    height: 175px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 0px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    background-color: #ffffff;
}
</style>


 <cfinclude template = "/exchange/include/header.cfm">

  <cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from lens
   where lens_hub_id = #session.hub#
   order by lens_order
  </cfquery>

 <cfquery name="comm_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  where comm_id = #session.community_id#
 </cfquery>

 <cfquery name="comm_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #comm_profile.comm_owner_id#
 </cfquery>

<!--- Check to see if you're a member of this comm --->

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td><td valign=top width=100%>


          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_news.png" width=22 align=absmiddle>&nbsp;&nbsp;<a href="open.cfm">Posts</a></span>
          </div>

          <cfif comm_profile.comm_calendar is 1>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_calendar.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="calendar.cfm">Shared Calendar</a></span>
          </div>

          </cfif>

          <cfif comm_profile.comm_documents is 1>


          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_document.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="docs.cfm">Document Library</a></span>
          </div>

          </cfif>

          <cfif comm_profile.comm_opportunity is 1>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_tile.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="opps.cfm">Opportunity Board</a></span>
          </div>

          </cfif>


		<div class="main_box_2">
        <center>

<style>
.image_home {
    <cfif #comm_profile.comm_banner# is "">
    background-image: url('/images/comm_stock_banner.jpg');
    <cfelse>
    background-image: url('#media_virtual#/<cfoutput>#comm_profile.comm_banner#</cfoutput>');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
.marketplace_comm_header {
    font-family: calibri, arial;
    text-align: bottom;
    padding-bottom: 10px;
    padding-left: 30px;
    font-size: 55px;
    font-weight: bold;
    color: #FFFFFF;
}

.comm_right_box {
    width: 300px;
    height: auto;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 10px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=100%>

				  <cfoutput>
				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header"><a href="s.cfm?i=#encrypt(comm_profile.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#comm_profile.comm_name#</a></td>
				       <td align=right class="feed_sub_header"><a href="open.cfm">Return</a></td></tr>
                   <tr><td height=10></td></tr>
				  </table>
				  </cfoutput>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr class="image_home"><td><cfinclude template="hub_header.cfm"></td></tr>
                   <tr><td height=10></td></tr>
                  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
		   <tr><td class="feed_header">Post Message</td><td align=right class="feed_option"></td></tr>
		   <tr><td colspan=2><hr></td></tr>
          </table>

          <form action="post_db.cfm" method="post" enctype="multipart/form-data" >

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_sub_header"><b>Title</b></td></tr>
				   <tr><td colspan=2><input type="text" class="input_text" name="feed_title" style="width: 750px;" required maxlength="100"></td></tr>
				   <tr><td class="feed_sub_header"><b>Message</b></td></tr>
				   <tr><td colspan=2><textarea name="feed_desc" class="input_textarea" style="width: 750px;" rows=5></textarea></td></tr>
				   <tr><td class="feed_sub_header"><b>URL Reference</b></td></tr>
				   <tr><td colspan=2><input type="url" name="feed_URL" class="input_text" style="width: 750px;" maxlength="200"></td></tr>
                   <tr><td class="feed_sub_header" valign=top>Picture / Image</td></tr>
                   <tr><td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="feed_pic"></td></tr>

                   <tr><td class="link_small_gray">Image will be auto scaled to 900px (width) by 250px (height)</td></tr>


  			       <tr><td class="feed_sub_header">Post Category</td>
  			           <td class="feed_sub_header">Tags or Keywords</td></tr>
			       <tr><td width=400>
					  <select class="input_select" name="feed_category_id" style="width: 350px;" required>
					   <cfoutput query="lens">
						<option value=#lens_id#>#lens_name#
					   </cfoutput>
					  </select></td><td><input type="text" class="input_text" name="feed_keywords" style="width: 350px;" maxlength="100"></td>

					  </tr>

                   <tr><td height=20></td></tr>
				   <tr><td colspan=2><input type="submit" name="button" value="Post" class="button_blue_large"></td></tr>

			  </table>

		  </td></tr>

          </table>

 		  </form>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=45></td></tr>
        </table>

           <cfinclude template="about.cfm">
           <cfinclude template="network.cfm">
           <cfinclude template="companies.cfm">
           <cfinclude template="manager.cfm">

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>