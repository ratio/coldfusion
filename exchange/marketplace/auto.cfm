
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head><div class="center">
<body>

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  order by usr_last_name
</cfquery>

    <h4>HTML 5 browsers</h4>
    <label>
        Enter First Name<br />
        <input type="text" name="firstName" id="firstName" list="listFirstNames" maxlength="20" style="width:15%;">
        <datalist id="listFirstNames">
            <cfoutput query="usr">
                <option value="#usr.usr_id#">#usr.usr_last_name#
            </cfoutput>
        </datalist>
    </label>
    <h4>Legacy and HTML 5 browsers</h4>
    <label>
        Enter First Name<br />
        <input type="text" name="firstName" id="firstName" list="listFirstNames" maxlength="20" style="width:15%;"><br />
    </label>
    <datalist id="listFirstNames">
        <label>
            OR select one from the list below:<br />
            <select name="firstName">
                <cfoutput query="usr">
                    <option value="#usr.usr_id#">#usr.usr_last_name#
                </cfoutput>
            </select>
        </label>
    </datalist>
</body>
</html>