<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <cfif isdefined("keyword")>
       <cfset #session.keyword# = "#keyword#">
      </cfif>

 	  <div class="exchange_filter_box">
	      <cfinclude template="search_companies.cfm">
	  </div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  				<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">

				  select company_duns as duns, company_id as id, company_name as name, company_duns as duns, company_city as city, company_zip as zip, company_state as state, 'EXCHANGE' as Source from company
				  where company_name like '#session.keyword#%' and
                        (company_discoverable is null or company_discoverable = 1)

                  union
				  select duns as duns, '0', legal_business_name as name, duns as duns, city_1 as city, zip_1 as zip, state_1 as state, 'SAM.Gov' as Source from sams
				  where (legal_business_name like '#session.keyword#%' or poc_email like '#session.keyword#%' or alt_poc_email like '#session.keyword#%' or duns = '#session.keyword#')

		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by name DESC
							<cfelseif #sv# is 10>
							 order by name ASC
							<cfelseif #sv# is 2>
							 order by city ASC
							<cfelseif #sv# is 20>
							 order by city DESC
							<cfelseif #sv# is 3>
							 order by state ASC
							<cfelseif #sv# is 30>
							 order by state DESC
							<cfelseif #sv# is 4>
							 order by zip ASC
							<cfelseif #sv# is 40>
							 order by zip DESC
							<cfelseif #sv# is 6>
							 order by duns ASC
							<cfelseif #sv# is 60>
							 order by duns DESC
                            </cfif>

                         <cfelse>

                           order by name ASC

                         </cfif>

				</cfquery>

           <cfoutput>
           <tr><td class="feed_header">Search Results (#numberformat(companies.recordcount,'999,999,999')# )</td></tr>
           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td class="feed_option"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a></td>
                    <td class="feed_option"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a></td>
                    <td class="feed_option"><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
                    <td class="feed_option"><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a></td>
                    <td class="feed_option"><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a></td>
                    <td class="feed_option"><a href="results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Source</b></a></td>
                    <td class="feed_option" align=right><b>Total Awards</b></td>
                    <td class="feed_option" align=right><b>Total Value</b></td>
				</tr>

            </cfoutput>

				<cfset #row_counter# = 0>

				<cfloop query="companies">

				<cfquery name="total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(id) as awards, sum(federal_action_obligation) as value from award_data
				 where recipient_duns = '#companies.duns#'
				</cfquery>





				<cfoutput>

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="e0e0e0"
				</cfif>
				>
					<td class="feed_option" width=300>

					<cfif id is 0>
					 <a href="/exchange/include/company_profile.cfm?id=#id#&duns=#companies.duns#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.name)#</a>
					<cfelse>
					 <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.name)#</a>
                    </cfif>
					 </td>
					<td class="feed_option" width=150>#ucase(companies.city)#</a></td>
					<td class="feed_option" width=50>#ucase(companies.state)#</a></td>
					<td class="feed_option" width=75>#companies.zip#</a></td>
					<td class="feed_option" width=75>#companies.duns#</a></td>
					<td class="feed_option" width=100>#companies.source#</td>
					<td class="feed_option" width=100 align=right>#numberformat(total.awards,'999,999,999')#</td>
					<td class="feed_option" width=100 align=right>

					<cfif #usr.usr_exchange_admin# is 1>

					<a href="/exchange/include/award_summary.cfm?id=0&duns=#companies.duns#" target="_blank" rel="noopener" rel="noreferrer">#numberformat(total.value,'$999,999,999,999')#</a>

					<cfelse>

					#numberformat(total.value,'$999,999,999,999')#

					</cfif>

					</td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				</cfoutput>

				</cfloop>

			<cfelse>

			<tr><td class="feed_option">No companies exist.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>
		  </table>

	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>