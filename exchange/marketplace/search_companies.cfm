<cfinclude template="/exchange/security/check.cfm">

<script type="text/javascript">
    function clearThis(target){
        target.value= "";
    }
</script>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select search_id, search_name from search
 where search_usr_id = #session.usr_id#
 order by search_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr>

        <form action="/exchange/marketplace/set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
			<td>
			   <span class="feed_header">SEARCH MARKETPLACE&nbsp;&nbsp;</span>
			   <select name="search_type" class="input_select" style="width: 150px;">
				 <option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>Companies
				 <option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>Capabilities
				</select>
				&nbsp;
				<input class="input_text" type="text" style="width: 175px;" placeholder = "keyword" name="keyword" <cfif isdefined("session.keyword")>value="<cfoutput>#session.keyword#</cfoutput>"</cfif> required onfocus="clearThis(this)">
				&nbsp;<input class="button_blue" type="submit" name="button" value="Search">
			</td>
        </form>

        <form action="/exchange/marketplace/refresh.cfm" method="post">
			<td align=right>
				   <span class="feed_sub_header">My Marketplaces</span>&nbsp;&nbsp;
				   <select name="search_id" class="input_select" onchange="this.form.submit()" style="width: 175px;">
					   <cfif views.recordcount GT 0>
					   <option value=0>Select Marketplace
					   <cfoutput query="views">
						<option value=#search_id# <cfif isdefined("session.search_id") and session.search_id is #search_id#>selected</cfif>>#ucase(search_name)#
					   </cfoutput>
					   <cfelse>
						<option value=0>No Markets Created
					   </cfif>
				   </select></form>
				   &nbsp;<input class="button_blue" type="submit" name="button" value="Create" onclick="window.location = '/exchange/marketplace/create_view.cfm'";>
			</td>


        </tr>

</table>

