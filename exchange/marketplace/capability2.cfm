<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style style="text/css">
    .break {
        word-wrap: break-word;
     }
  	.hoverTable{
		width:100%;
		border-collapse:collapse;
	}
	.hoverTable td{
		padding:7px; border:#e0e0e0 1px solid;
	}
	/* Define the default color for all the table rows */
	.hoverTable tr{
		background: #ffffff;
	}
	/* Define the hover highlight color for the table row */
    .hoverTable tr:hover {
          background-color: #e0e0e0;
    }
</style>

<style>
#page-loader {
position: absolute;
top: 50;
bottom: 0;
left: 0;
right: 0;
z-index: 10000;
display: none;
text-align: center;
width: 100%;
padding-top: 300px;
background-color: rgba(255, 255, 255, 0.7);
}
</style>

<style>
#lds-roller {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-roller div {
  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  transform-origin: 32px 32px;
}
.lds-roller div:after {
  content: " ";
  display: block;
  position: absolute;
  width: 6px;
  height: 6px;
  border-radius: 50%;
  background: #fff;
  margin: -3px 0 0 -3px;
}
.lds-roller div:nth-child(1) {
  animation-delay: -0.036s;
}
.lds-roller div:nth-child(1):after {
  top: 50px;
  left: 50px;
}
.lds-roller div:nth-child(2) {
  animation-delay: -0.072s;
}
.lds-roller div:nth-child(2):after {
  top: 54px;
  left: 45px;
}
.lds-roller div:nth-child(3) {
  animation-delay: -0.108s;
}
.lds-roller div:nth-child(3):after {
  top: 57px;
  left: 39px;
}
.lds-roller div:nth-child(4) {
  animation-delay: -0.144s;
}
.lds-roller div:nth-child(4):after {
  top: 58px;
  left: 32px;
}
.lds-roller div:nth-child(5) {
  animation-delay: -0.18s;
}
.lds-roller div:nth-child(5):after {
  top: 57px;
  left: 25px;
}
.lds-roller div:nth-child(6) {
  animation-delay: -0.216s;
}
.lds-roller div:nth-child(6):after {
  top: 54px;
  left: 19px;
}
.lds-roller div:nth-child(7) {
  animation-delay: -0.252s;
}
.lds-roller div:nth-child(7):after {
  top: 50px;
  left: 14px;
}
.lds-roller div:nth-child(8) {
  animation-delay: -0.288s;
}
.lds-roller div:nth-child(8):after {
  top: 45px;
  left: 10px;
}
@keyframes lds-roller {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>

<div id="page-loader">
    <h2><font color="000000" style="font-family: arial;">Loading results...</font></h2>
</div>

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

<cfset perpage = 200>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/marketplace/menu.cfm">
      <cfinclude template="/exchange/marketplace/recent.cfm">
      <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/marketplace/search_companies.cfm">
		</div>

      <div class="main_box">

  			<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				select company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Exchange' as source from company

                <cfif isdefined("session.add_filter")>
				 where contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
                   and contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 where contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(session.keyword)#"')
                </cfif>

				union

				select company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Federal Awards' as source from award_data
				join company on company_duns = recipient_duns

                <cfif isdefined("session.add_filter")>
				   where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				    and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(session.keyword)#"')
                </cfif>

 			    union

				select company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Federal SBIR' as source from sbir
				join company on company_duns = sbir.duns
                where (sbir.duns <> '' or company.company_duns <> '') and

                <cfif isdefined("session.add_filter")>
				  and contains((award_title, abstract, research_keywords),'"#trim(session.keyword)#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				     and contains((award_title, abstract, research_keywords),'"#trim(i)#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				where contains((award_title, abstract, research_keywords),'"#trim(session.keyword)#"')
                </cfif>

		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by company_name DESC
							<cfelseif #sv# is 10>
							 order by company_name ASC
							<cfelseif #sv# is 2>
							 order by company_city ASC
							<cfelseif #sv# is 20>
							 order by company_city DESC
							<cfelseif #sv# is 3>
							 order by company_state ASC
							<cfelseif #sv# is 30>
							 order by company_state DESC
							<cfelseif #sv# is 4>
							 order by company_zip ASC
							<cfelseif #sv# is 40>
							 order by company_zip DESC
							<cfelseif #sv# is 7>
							 order by source ASC, company_name ASC
							<cfelseif #sv# is 70>
							 order by source DESC, company_name ASC
							<cfelseif #sv# is 8>
							 order by company_keywords ASC, company_meta_keywords ASC
							<cfelseif #sv# is 80>
							 order by company_keywords ASC, company_meta_keywords ASC
                            </cfif>

                         <cfelse>

                           order by company_name ASC

                         </cfif>

			</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>

			<cfparam name="url.start" default="1">
			<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
				<cfset url.start = 1>
			</cfif>

			<cfset totalPages = ceiling(companies.recordCount / perpage)>
			<cfset thisPage = ceiling(url.start / perpage)>

           <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="set.cfm" method="post">
           		<tr><td class="feed_header">SEARCH RESULTS

           		<cfif companies.recordcount is 0>
           		( No Companies )
           		<cfelseif companies.recordcount is 1>
           		( 1 Company )
           		<cfelse>
           		(#trim(numberformat(companies.recordcount,'999,999,999'))# Companies)
           		</cfif>

           		</td>
           		    <td class="feed_sub_header" align=right>

		            <b>Add Filter</b>&nbsp;&nbsp;

                    <input class="input_text" type="text" style="width: 200px;" name="add_filter" required>
            &nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Add"></td>

           </form>

         		</tr>
           		<tr><td colspan=2><hr></td></tr>
           </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           		<tr><td class="feed_sub_header"><b>You searched for company capabilities matching <i>"#session.keyword#"

           		<cfif isdefined("session.add_filter")>

           		 <cfloop index="i" list="#session.add_filter#">
           		  and "#i#"
           		 </cfloop>

           		</cfif>

           		</i></b></td>
           		    <td class="feed_sub_header" align=right valign=top>

					<cfif companies.recordcount GT #perpage#>
						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

				</td><td class="feed_option" align=right width=50>

	                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/exchange/marketplace/capability.cfm?export=1"><img src="/images/icon_export_excel.png" alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>

           		    </td>

           		</tr>

      		    <cfif isdefined("session.add_filter")>

      		        <tr><td height=10></td></tr>

					<tr><td clsss="feed_option">

						 <cfloop index="i" list="#session.add_filter#">
						   <cfset size = evaluate(10*len(i))>
						   <input class="button_blue" style="font-size: 11px; height: 25px; width: auto" type="submit" name="button" value="X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#ucase(i)#&nbsp;&nbsp;"  onclick="location.href = 'remove.cfm?i=#i#'">&nbsp;&nbsp;
						 </cfloop>

					</td></tr>

          		</cfif>

           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header"><a href="capability.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td class="feed_sub_header"><b>Tags</b></td>
                    <td class="feed_sub_header"><b>Website</b></td>
                    <td class="feed_sub_header"><a href="capability.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td class="feed_sub_header" align=center><a href="capability.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
<!---                    <td class="feed_sub_header" align=center><a href="capability.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a></td> --->
                    <td class="feed_sub_header" align=right><a href="capability.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Source</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
			    </tr>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #row_counter# = 0>
                <cfset count = 1>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td class="feed_option">

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header"><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option">

					<cfif companies.company_keywords is "">
					#ucase(wrap(companies.company_meta_keywords,30,0))#
					<cfelse>
					#ucase(wrap(companies.company_keywords,30,0))#
					</cfif>

					</td>

					<td class="feed_option">


					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(companies.company_website) GT 40>
					#ucase(left(companies.company_website,40))#...
					<cfelse>
					#ucase(companies.company_website)#
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
					<td class="feed_option" align=right width=100>#companies.source#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				<cfif count is not companies.recordcount>
				 <tr><td colspan=9><hr></td></tr>
				</cfif>

				<cfset count = count + 1>

				</cfoutput>


            <tr><td colspan=7>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
			  <tr><td class="link_small_gray" colspan=2>The Exchange collects and integrates information about companies from multiple sources.  As a result, duplicate companies may exist.</td></tr>
			  <tr><td height=15></td></tr>

			  <tr><td width=60>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/cb_logo.png" width=40 alt="Crunchbase" title="Crunchbase"></a></td></tr>
				  </table>

				  </td><td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b>Crunchbase</b></a></td><td align=right class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.crunchbase.com</u></b></a></td></tr>
				   <tr><td class="link_small_gray" colspan=2>The Exchange uses Crunchbase information, specifically company names, tags, descriptions and keywords, to assist users on searching for companies.</td></tr>
				  </table>

				  </td></tr>

			</table>

            </td></tr>

			<cfelse>

			<tr><td class="feed_sub_header">No companies were found.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>
		  </table>

	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>