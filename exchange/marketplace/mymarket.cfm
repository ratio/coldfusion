<cfinclude template="/exchange/security/check.cfm">

<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from search
 where search_id = #session.search_id#
       and search_usr_id = #session.usr_id#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select search_id, search_name from search
 where search_usr_id = #session.usr_id#
 order by search_name
</cfquery>

				<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select company_id, company_duns, company_name, pri_naics, company_city, company_zip, poc_email, company_state, company_website from company
				  left join sams on duns = company_duns
                  where company_id > 0

                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and (company_state = '#search.search_state_list#')
                   <cfelse>
                   and (
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      company_state = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                  <cfif listlen(search.search_zip_list) is 1>
                    and (company_zip = '#search.search_zip_list#')
                   <cfelse>
                   and (
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      company_zip = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_naics_list) GT 0>
                   <cfif listlen(search.search_naics_list) is 1>

                    and ( pri_naics = '#search.search_naics_list#' )
                   <cfelse>
                   and (
                    <cfset #ncounter# = 1>
                    <cfloop index="nelement" list=#search.search_naics_list#>
                      pri_naics = '#trim(nelement)#'
                      <cfif #ncounter# LT listlen(search.search_naics_list)> or </cfif>
                      <cfset #ncounter# = #ncounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

				<cfif isdefined("sv")>
				 <cfif #sv# is 1>
				  order by company_name DESC
				 <cfelseif #sv# is 10>
				  order by company_name ASC
				 <cfelseif #sv# is 2>
				  order by company_city ASC
				 <cfelseif #sv# is 20>
				  order by company_city DESC
				 <cfelseif #sv# is 3>
				  order by company_state ASC
				 <cfelseif #sv# is 30>
				  order by company_state DESC
				 <cfelseif #sv# is 4>
				  order by company_zip ASC
				 <cfelseif #sv# is 40>
				  order by company_zip DESC
				 <cfelseif #sv# is 5>
				  order by company_website ASC
				 <cfelseif #sv# is 50>
				  order by company_website DESC
				 <cfelseif #sv# is 6>
				  order by company_duns ASC
				 <cfelseif #sv# is 60>
				  order by company_duns DESC
				 <cfelseif #sv# is 7>
				  order by poc_email ASC
				 <cfelseif #sv# is 70>
				  order by poc_email DESC
				 <cfelseif #sv# is 8>
				  order by pri_naics ASC
				 <cfelseif #sv# is 80>
				  order by pri_naics DESC

				 </cfif>
				<cfelse>
                  order by legal_business_name ASC
				</cfif>

				</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>
<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset perpage = 200>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(companies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <cfinclude template="/exchange/include/header.cfm">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/marketplace/menu.cfm">
      <cfinclude template="/exchange/marketplace/recent.cfm">
      <cfinclude template="/exchange/marketplace/portfolios.cfm">

	   </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/marketplace/search_companies.cfm">
		</div>

      <div class="main_box">

          <cfoutput>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">#search.search_name# (#trim(numberformat(companies.recordcount,'9,999'))# Companies) </td>
	           <td align=right class="feed_header" align=right valign=top>

	           <a href="/exchange/marketplace/edit_view.cfm?group_id=#session.search_id#"><img src="/images/icon_config.png" align=absmiddle width=20 border=0 alt="Configure Marketplace" title="Configure Marketplace"></a>&nbsp;&nbsp;&nbsp;
	           <a href="/exchange/marketplace/mymarket.cfm?export=1"><img src="/images/icon_export_excel.png" alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>

	           </td></tr>

           	<tr><td class="feed_option" valign=middle>#search.search_description#</td>

		   <td align=right class="feed_sub_header">

			<cfoutput>
				<cfif companies.recordcount GT #perpage#>
					<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=20 valign=top></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt companies.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=20 valign=top></a>
					<cfelse>
					</cfif>
				</cfif>
			</cfoutput>

		   </td></tr>

           <tr><td colspan=2><hr></td></tr>

          </table>

          </cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			  <tr>

			  <cfoutput>

				 <td class="feed_sub_header" height=50><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_sub_header"><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_sub_header" align=center><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_sub_header" align=center><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_sub_header"><b>Website</b></td>
				 <td class="feed_sub_header"><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_sub_header"><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Contact Email</b></a></td>
				 <td class="feed_sub_header" align=right><a href="mymarket.cfm?group_id=#session.search_id#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>

			  </cfoutput>

			  </tr>

				<cfset #row_counter# = 0>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">
				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="e0e0e0"
				</cfif>
				>
					<td class="feed_option" width=300 height=30><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(companies.company_name)#</b></a></td>
					<td class="feed_option" width=150>#ucase(companies.company_city)#</a></td>
					<td class="feed_option" width=50 align=center>#companies.company_state#</a></td>
					<td class="feed_option" width=75 align=center>#companies.company_zip#</a></td>
					<td class="feed_option">

					<cfif #len(companies.company_website)# GT 30>
					 <cfset cn = #left(companies.company_website,'30')# & "...">
					<cfelse>
					 <cfset cn = #companies.company_website#>
					</cfif>
					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">#cn#</a></td>

					<td class="feed_option" width=100>#companies.company_duns#</td>
					<td class="feed_option" width=100>

					<cfif #len(companies.poc_email)# GT 30>
					 <cfset em = #left(companies.poc_email,'30')# & "...">
					<cfelse>
					 <cfset em = #companies.poc_email#>
					</cfif>
					<a href="mailto:#companies.poc_email#">#em#</a></td>

					<td class="feed_option" width=100 align=right>#companies.pri_naics#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				</cfoutput>

			<cfelse>

			<tr><td class="feed_option">No companies exist.</td></tr>

			</cfif>

			</table>

         </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>