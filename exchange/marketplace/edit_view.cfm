<cfinclude template="/exchange/security/check.cfm">

<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from search
  where search_id = #group_id# and
        search_usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/marketplace/menu.cfm">
      <cfinclude template="/exchange/marketplace/recent.cfm">
      <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

      <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Configure Marketplace</td>
	           <td align=right class="feed_option"><a href="/exchange/marketplace/mymarket.cfm"><img src="/images/delete.png" border=0 width=20></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="save_view.cfm" method="post">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="form_header" width=150><b>Marketplace Name</b></td></td></tr>
           <tr><td><input class="input_text" type="text" name="search_name" size=40 value="#search.search_name#" maxlength="200" required></td></tr>

           <tr><td class="form_header" width=150 valign=top><b>Description</b></td></tr>
           <tr><td><textarea name="search_description" cols=90 rows=3 class="input_textarea">#search.search_description#</textarea></td></tr>

          <tr><td class="form_header" width=100><b>States to Include</b></td></tr>
          <tr><td class="feed_option"><input class="input_text" type="text" name="search_state_list" size=40 value="#search.search_state_list#" maxlength="200">&nbsp;*&nbsp;i.e., VA, MD</td></tr>

          <tr><td class="form_header" width=100><b>Zip Codes</b></td></tr>
          <tr><td class="feed_option"><input class="input_text" type="text" name="search_zip_list" size=40 maxlength="1000" value="#search.search_zip_list#">&nbsp;*&nbsp;i.e., 22553, 22102</td></tr>

          <tr><td class="form_header" width=100><b>NAICS Code(s)</b></td></tr>
          <tr><td><input type="text" class="input_text" name="search_naics_list" size=40 maxlength="1000" value="#search.search_naics_list#">&nbsp;*</td></tr>

          </cfoutput>

          <tr><td height=10>&nbsp;</td></tr>
	      <tr><td class="text_xsmall" colspan=2><b><i>* - for multiple options, seperate each value with a comma.</i></b></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td colspan=2>
          <input class="button_blue" type="submit" name="button" value="Update">&nbsp;&nbsp;
          <input class="button_blue" type="submit" name="button" value="Delete" onclick="return confirm('Delete Search?\r\nAre you sure you want to delete this search?');">&nbsp;&nbsp;
          </td></tr>

          <cfoutput>

          <input type="hidden" name="search_id" value=#search.search_id#>

          </cfoutput>

          </table>

          </form>


	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>