<cfinclude template="/exchange/security/check.cfm">

<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from search
 where search_id = #search_id# and
       search_usr_id = #session.usr_id#
</cfquery>

  			<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">

				select pri_naics, a.company_website, company_city, company_state, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'No Awards' as source from company a
				left join sams on duns = a.company_duns
				where (contains((company_meta_keywords, company_about, company_history, company_keywords, company_homepage_text),'"#search.search_keyword#"'))

                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and (company_state = '#search.search_state_list#')
                   <cfelse>
                   and (
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      company_state = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                  <cfif listlen(search.search_zip_list) is 1>
                    and (company_zip = '#search.search_zip_list#')
                   <cfelse>
                   and (
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      company_zip = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_naics_list) GT 0>
                   <cfif listlen(search.search_naics_list) is 1>
                    and ( pri_naics = '#search.search_naics_list#' )
                   <cfelse>
                   and (
                    <cfset #ncounter# = 1>
                    <cfloop index="nelement" list=#search.search_naics_list#>
                      pri_naics = '#trim(nelement)#'
                      <cfif #ncounter# LT listlen(search.search_naics_list)> or </cfif>
                      <cfset #ncounter# = #ncounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                union

				select naics_code, b.company_website, company_city, company_state, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Prime Award' as source from company b
				left join award_data on recipient_duns = b.company_duns
                where (contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#search.search_keyword#"'))

                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and (company_state = '#search.search_state_list#')
                   <cfelse>
                   and (
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      company_state = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                  <cfif listlen(search.search_zip_list) is 1>
                    and (company_zip = '#search.search_zip_list#')
                   <cfelse>
                   and (
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      company_zip = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_naics_list) GT 0>
                   <cfif listlen(search.search_naics_list) is 1>

                    and ( naics_code = '#search.search_naics_list#' )
                   <cfelse>
                   and (
                    <cfset #ncounter# = 1>
                    <cfloop index="nelement" list=#search.search_naics_list#>
                      naics_code = '#trim(nelement)#'
                      <cfif #ncounter# LT listlen(search.search_naics_list)> or </cfif>
                      <cfset #ncounter# = #ncounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

 			    union

				select '' as naics, c.company_website, company_city, company_state, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'SBIR Award' as source from company c
				left join sbir on sbir.duns = c.company_duns
                where (contains((award_title, abstract, research_keywords),'"#search.search_keyword#"'))

                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and (company_state = '#search.search_state_list#')
                   <cfelse>
                   and (
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      company_state = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                  <cfif listlen(search.search_zip_list) is 1>
                    and (company_zip = '#search.search_zip_list#')
                   <cfelse>
                   and (
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      company_zip = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

 			    union

				select subaward_naics_code, d.company_website, company_city, company_state, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Subcontract Award' as source from company d
				left join award_data_sub on subawardee_duns = d.company_duns
                where (contains((subaward_description),'"#search.search_keyword#"'))

                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and (company_state = '#search.search_state_list#')
                   <cfelse>
                   and (
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      company_state = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                  <cfif listlen(search.search_zip_list) is 1>
                    and (company_zip = '#search.search_zip_list#')
                   <cfelse>
                   and (
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      company_zip = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_naics_list) GT 0>
                   <cfif listlen(search.search_naics_list) is 1>

                    and ( subaward_naics_code = '#search.search_naics_list#' )
                   <cfelse>
                   and (
                    <cfset #ncounter# = 1>
                    <cfloop index="nelement" list=#search.search_naics_list#>
                      subaward_naics_code = '#trim(nelement)#'
                      <cfif #ncounter# LT listlen(search.search_naics_list)> or </cfif>
                      <cfset #ncounter# = #ncounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>
		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by company_name DESC
							<cfelseif #sv# is 10>
							 order by company_name ASC
							<cfelseif #sv# is 2>
							 order by company_city ASC
							<cfelseif #sv# is 20>
							 order by company_city DESC
							<cfelseif #sv# is 3>
							 order by company_state ASC
							<cfelseif #sv# is 30>
							 order by company_state DESC
							<cfelseif #sv# is 4>
							 order by company_zip ASC
							<cfelseif #sv# is 40>
							 order by company_zip DESC
							<cfelseif #sv# is 7>
							 order by source ASC, company_name ASC
							<cfelseif #sv# is 70>
							 order by source DESC, company_name ASC
                            </cfif>

                         <cfelse>

                           order by company_name ASC

                         </cfif>

			</cfquery>

<!---
				<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select distinct(duns), legal_business_name, pri_naics, city_1, zip_1, poc_email, state_1, corp_url, poc_us_phone from sams
                  where duns is not null

                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and (state_1 = '#search.search_state_list#')
                   <cfelse>
                   and (
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      state_1 = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                  <cfif listlen(search.search_zip_list) is 1>
                    and (zip_1 = '#search.search_zip_list#')
                   <cfelse>
                   and (
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      zip_1 = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_naics_list) GT 0>
                   <cfif listlen(search.search_naics_list) is 1>

                    and ( pri_naics = '#search.search_naics_list#' )
                   <cfelse>
                   and (
                    <cfset #ncounter# = 1>
                    <cfloop index="nelement" list=#search.search_naics_list#>
                      pri_naics = '#trim(nelement)#'
                      <cfif #ncounter# LT listlen(search.search_naics_list)> or </cfif>
                      <cfset #ncounter# = #ncounter# + 1>
                    </cfloop>
                    )
                 </cfif>
                 </cfif>

				<cfif isdefined("sv")>
				 <cfif #sv# is 1>
				  order by legal_business_name DESC
				 <cfelseif #sv# is 10>
				  order by legal_business_name ASC
				 <cfelseif #sv# is 2>
				  order by city_1 ASC
				 <cfelseif #sv# is 20>
				  order by city_1 DESC
				 <cfelseif #sv# is 3>
				  order by state_1 ASC
				 <cfelseif #sv# is 30>
				  order by state_2 DESC
				 <cfelseif #sv# is 4>
				  order by zip_1 ASC
				 <cfelseif #sv# is 40>
				  order by zip_1 DESC

				 <cfelseif #sv# is 5>
				  order by corp_url ASC
				 <cfelseif #sv# is 50>
				  order by corp_url DESC

				 <cfelseif #sv# is 6>
				  order by duns ASC
				 <cfelseif #sv# is 60>
				  order by duns DESC

				 <cfelseif #sv# is 7>
				  order by poc_email ASC
				 <cfelseif #sv# is 70>
				  order by poc_email DESC

				 <cfelseif #sv# is 8>
				  order by pri_naics ASC
				 <cfelseif #sv# is 80>
				  order by pri_naics DESC

				 </cfif>
				<cfelse>
                  order by legal_business_name ASC
				</cfif>

				</cfquery> --->

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><cfoutput>#search.search_name#</cfoutput></td>
	           <td align=right class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
	       <tr><td height=5></td></tr>
           <cfoutput>
           <tr><td class="feed_option" valign=top><cfif #search.search_description# is "">No saved search description provided<cfelse>#search.search_description#</cfif>
           </cfoutput>
           </td>
               <td align=right class="feed_option">

	           <form action="refresh.cfm">

	           <cfoutput>
	               <a href="edit_view.cfm?search_id=#search_id#">Edit Search</a>&nbsp;&nbsp;
               </cfoutput>


	           <select name="option" class="input_white">

               <!---<cfif views.recordcount GT 0>
               <cfoutput query="views">
                <option value= V#search_id# <cfif #search_id# is #search_id#>selected</cfif>>#search_name#
               </cfoutput>

               </cfif> --->

	           </select>

	           &nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Refresh">&nbsp;&nbsp;
                           <input class="button_blue" style="font-size: 11px; height: 20px; width: 75px;" type="submit" name="button" value="New Search">
               </form>

	           </td>

           </tr>

           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

          </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>
            <cfif #companies.recordcount# is 0>
            	<tr><td colspan=8 class="feed_sub_header">No companies found</td></tr>
            <cfelse>
            	<tr><td colspan=8 class="feed_sub_header">#numberformat(companies.recordcount,'999,999')# companies found</td></tr>
            </cfif>

            <tr><td height=10></td></tr>
            </cfoutput>

			<cfif #companies.recordcount# GT 0>

			  <tr>

			  <cfoutput>

				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a></td>
				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a></td>
				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a></td>
				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Website</b></a></td>
				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a></td>
				 <td class="feed_option"><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a></td>
				 <td class="feed_option" align=right><a href="group.cfm?search_id=#search_id#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Source</b></a></td>

			  </cfoutput>

			  </tr>

				<cfset #row_counter# = 0>

				<cfoutput query="companies">
				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="e0e0e0"
				</cfif>
				>
					<td class="feed_option" width=300><a href="/exchange/include/company_profile.cfm?duns=#companies.company_duns#" target="_blank" rel="noopener" rel="noreferrer">#companies.company_name#</a></td>
					<td class="feed_option" width=150>#companies.company_city#</a></td>
					<td class="feed_option" width=50>#companies.company_state#</a></td>
					<td class="feed_option" width=75>#companies.company_zip#</a></td>
					<td class="feed_option">

					<cfif #len(companies.company_website)# GT 30>
					 <cfset cn = #left(companies.company_website,'30')# & "...">
					<cfelse>
					 <cfset cn = #companies.company_website#>
					</cfif>
					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">#cn#</a></td>

					<td class="feed_option" width=100>

					<cfif isdefined("companies.pri_naics")>
					 #companies.pri_naics#
					</cfif>

					<cfif isdefined("companies.naics_code")>
					 #companies.naics_code#
					</cfif>

					<cfif isdefined("companies.subaward_naics_code")>
					 #companies.subaward_naics_code#
					</cfif>

					</td>
					<td class="feed_option" width=100>#companies.company_duns#</td>
					<td class="feed_option" width=150 align=right>#companies.source#</td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				</cfoutput>

			<cfelse>

			<tr><td class="feed_option">No companies exist.</td></tr>

			</cfif>

			</table>





















	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>