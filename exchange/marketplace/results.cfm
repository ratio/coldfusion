<cfinclude template="/exchange/security/check.cfm">
<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset perpage = 100>

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

<style>
/* Style The Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  right: 0;
  font-size: 13px;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
</style>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

		  <cfinclude template="/exchange/components/my_profile/profile.cfm">
		  <!--- <cfinclude template="/exchange/marketplace/recent.cfm"> --->
		  <cfinclude template="/exchange/portfolio/recent.cfm">


      </td><td valign=top>

	  <div class="main_box">
	   <cfinclude template="/exchange/marketplace/search_companies_network.cfm">
	  </div>

      <div class="main_box">

  <cfquery name="in_network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select hub_comp_company_id from hub_comp
   where hub_comp_hub_id = #session.hub#
  </cfquery>

  <cfif in_network.recordcount is 0>
   <cfset in_network_list = 0>
  <cfelse>
   <cfset in_network_list = valuelist(in_network.hub_comp_company_id)>
  </cfif>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

	select company_name, company_logo, company_website, company_id, company_duns, company_city, company_zip, company_state,
	((select isnull(sum(subaward_amount),0) from award_data_sub table1 where table1.subawardee_duns = table3.company_duns) +
	 (select isnull(sum(federal_action_obligation),0) from award_data table2 where table2.recipient_duns = table3.company_duns) +
	 (select isnull(sum(award_amount),0) from sbir table4 where table4.duns = table3.company_duns and table4.duns <> '')
	 ) as total
	 from company table3
	 where (company_discoverable is null or company_discoverable = 1) and
	(contains((company_name, company_duns),'#trim(session.keyword)#'))
	group by company_name, company_duns, company_website, company_id, company_logo, company_city, company_state, company_zip

			 <cfif isdefined("sv")>

				<cfif #sv# is 1>
				 order by company_name DESC
				<cfelseif #sv# is 10>
				 order by company_name ASC
				<cfelseif #sv# is 2>
				 order by company_city ASC
				<cfelseif #sv# is 20>
				 order by company_city DESC
				<cfelseif #sv# is 3>
				 order by company_state ASC
				<cfelseif #sv# is 30>
				 order by company_state DESC
				<cfelseif #sv# is 4>
				 order by company_zip ASC
				<cfelseif #sv# is 40>
				 order by company_zip DESC
				<cfelseif #sv# is 6>
				 order by company_duns ASC
				<cfelseif #sv# is 60>
				 order by company_duns DESC
				<cfelseif #sv# is 7>
				 order by total DESC
				<cfelseif #sv# is 70>
				 order by total ASC
				<cfelseif #sv# is 8>
				 order by total DESC
				<cfelseif #sv# is 80>
				 order by total ASC
				</cfif>

			 <cfelse>

			   <!--- order by name ASC --->

			 </cfif>

	</cfquery>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(companies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>


           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>

           <cfoutput>
           		<tr><td class="feed_header">SEARCH RESULTS

           		<cfif companies.recordcount is 0>
           		( No Companies )
           		<cfelseif companies.recordcount is 1>
           		( 1 Company )
           		<cfelse>
           		(#trim(numberformat(companies.recordcount,'999,999,999'))# Companies)
           		</cfif>
           		</td>
           		<td align=right class="feed_sub_header">

           		<a href="/exchange/marketplace/results.cfm?export=1"><img src="/images/icon_export_excel.png" hspace=10 alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>
           		<a href="/exchange/marketplace/results.cfm?export=1">Export to Excel</a>

           		</td></tr>


           		<tr><td colspan=2><hr></td></tr>
           		<tr><td class="feed_sub_header"><b>You searched for companies matching <i>"#session.keyword#"</i></b></td>


				    <td class="feed_sub_header" align=right>

				    Page -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				</td>

           		</tr>
           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td></td>
                    <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td class="feed_sub_header">Website</td>
                    <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td class="feed_sub_header" align=right width=150><a href="results.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Awards</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
                    <td></td>
				</tr>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #count# = 1>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

				<tr>

					<td class="feed_option">

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" border=0 width=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" border=0 width=40>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header" width=300><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td align=right width=50><cfif listfind(in_network_list,companies.company_id)><img src="/images/in_network.png" style="cursor: pointer;" width=30 hspace=10 alt="In Network Partner" title="In Network Partner"><cfelse></cfif></td>
					<td class="feed_option" width=100 align=center>#ucase(companies.company_duns)#</td>
					<td class="feed_option"><a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer"><cfif len(companies.company_website) GT 40>#ucase(left(companies.company_website,40))#...<cfelse>#ucase(companies.company_website)#</cfif></a></td>
					<td class="feed_option" width=150>#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
					<td class="feed_option" width=75 align=right><b>#numberformat(companies.total,'$999,999,999,999')#</b></td>

					<td width=50 align=right>
					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=8>
					  <div class="dropdown-content" style="width: 175px; text-align: left;">
						<a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer"><i class="fa fa-fw fa-pie-chart"></i>&nbsp;&nbsp;Company Profile</a>
						<a href="/exchange/include/save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-dollar"></i>&nbsp;&nbsp;Add to Deal</a>
						<a href="/exchange/include/save_company.cfm" onclick="window.open('/exchange/include/save_company.cfm?comp_id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
					  </div>
					</div>
					</td>

				</tr>

				  <cfif count is not companies.recordcount>
				   <tr><td colspan=9><hr></td></tr>
				  <cfelse>
				   <tr><td height=5></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

            <tr><td colspan=9>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td height=10></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
			  <tr><td class="link_small_gray" colspan=2>The Exchange collects and integrates information about companies from multiple sources.  As a result, duplicate companies may exist.</td></tr>
			  <tr><td height=15></td></tr>

			  <tr><td width=60>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/cb_logo.png" width=40 alt="Crunchbase" title="Crunchbase"></a></td></tr>
				  </table>

				  </td><td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b>Crunchbase</b></a></td><td align=right class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.crunchbase.com</u></b></a></td></tr>
				   <tr><td class="link_small_gray" colspan=2>The Exchange uses Crunchbase information, specifically company names, tags, descriptions and keywords, to assist users on searching for companies.</td></tr>
				  </table>

				  </td></tr>

			</table>

            </td></tr>

			<cfelse>

			<tr><td class="feed_sub_header" style="font-weight: normal;">No companies were found.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>

		  </table>

	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>