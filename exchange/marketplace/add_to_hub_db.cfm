<cfinclude template="/exchange/security/check.cfm">

<cfflush interval = "100">

<cfloop index="element" list=#add#>

<cftransaction>

	<cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from sams
	 where duns = '#element#'
	</cfquery>

	<cfquery name="user_insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into usr
	 (usr_first_name, usr_last_name, usr_title, usr_email, usr_created, usr_updated, usr_validation, usr_setting_notification, usr_phone, usr_active)
	 values
	 ('#sams.poc_fnme#','#sams.poc_lname#','#sams.poc_title#','#sams.poc_email#',#now()#,#now()#,0,1,'#sams.poc_us_phone#',0)
	</cfquery>

	<cfquery name="usrmax" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(usr_id) as id from usr
	</cfquery>

	<cfquery name="company_insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into company
     (company_name, company_website, company_admin_id, company_city, company_state, company_zip, company_address_l1, company_address_l2, company_duns, company_updated)
     values
     ('#sams.legal_business_name#','#sams.corp_url#',#usrmax.id#,'#sams.city_1#','#sams.state_1#','#sams.zip_1#','#sams.phys_address_l1#','#sams.phys_address_line_2#','#sams.duns#',#now()#)
    </cfquery>

	<cfquery name="compmax" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(company_id) as id from company
	</cfquery>

	<cfquery name="update_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     update usr
     set usr_company_id = #compmax.id#
     where usr_id = #usrmax.id#
    </cfquery>

	<cfquery name="hub_xref" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into hub_xref
     (hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_default, hub_xref_usr_role)
     values
     (#usrmax.id#,#session.hub#,0,null,2)
    </cfquery>

</cftransaction>

<cfoutput>#sams.legal_business_name#</cfoutput><br>

</cfloop>

Done (whew)
<cfabort>

