<cfinclude template="/exchange/security/check.cfm">

<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select follow_company_id from follow
 where follow_by_usr_id = #session.usr_id#
</cfquery>

<cfset follow_list = #valuelist(follow.follow_company_id)#>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 where company_id > 0
 <cfif isdefined ("o")>
   <cfif #o# is 2>
   and company_id in (#follow_list#)
  </cfif>
 </cfif>
 order by company_name
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 	  <div class="exchange_filter_box">

	      <cfinclude template="search_companies.cfm">

	  </div>

      <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Exchange Companies and Organizations</td>
	           <td align=right class="feed_option">

	           <form action="refresh.cfm">

	           <select name="option" style="font-family: arial; font-size: 12px; height: 22px; width: 200px; color: a0a0a0;">
	            <option value=1>New Companies (last 60 days)
	            <option value=2 <cfif isdefined("o") and o is 2>selected</cfif>>Companies I follow
	           </select>

	           &nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Refresh">&nbsp;&nbsp;
                           <input class="button_blue" style="font-size: 11px; height: 20px; width: 75px;" type="submit" name="button" value="Create View">
               </form>

	           </td>
           </tr>

           <tr><td>&nbsp;</td></tr>

          </table>
          </form>

	    <cfloop query="comp">

		<cfquery name="capabilities" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select topic_name from topic, align
		 where topic.topic_id = align.align_type_value and
		       align.align_type_id = 4 and align.align_company_id = #comp.company_id#
		</cfquery>

		<cfset #capability_list# = #valuelist(capabilities.topic_name,', ')#>

		<cfquery name="customers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(customer_id) as total from customer
		 where customer_company_id = #comp.company_id#
		</cfquery>

		<cfquery name="products" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(product_id) as total from product
		 where product_company_id = #comp.company_id#
		</cfquery>

		<cfquery name="partners" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(partner_id) as total from partner
		 where partner_company_id = #comp.company_id#
		</cfquery>

		<cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(follow_id) as total from follow
		 where follow_company_id = #comp.company_id#
		</cfquery>

        <div class="badge">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

 	       <cfoutput>
			   <tr><td class="feed_title"><a href="/exchange/include/company_profile.cfm?id=#company_id#"><font size=2><b>#company_name#</b></font></a></td>
			       <td align=right>

				   <cfif #listcontains(follow_list,#comp.company_id#)#>
					<a href="unfollow.cfm?c=#company_id#<cfif isdefined("o")>&o=#o#</cfif>"><img src="/images/small_unfollow.png" width=20 alt="Unfollow" title="Unfollow" border=0></a>
				   <cfelse>
					<a href="follow.cfm?c=#company_id#"<cfif isdefined("o")>&o=#o#</cfif>><img src="/images/small_follow.png" width=20 alt="Follow" title="Follow" border=0></a>
				   </cfif>
			       </td></tr>

               <tr><td colspan=2 class="text_xsmall"><cfif #comp.company_city# is "" and #comp.company_state# is "">Location not defined<cfelse><b>#comp.company_city#, #comp.company_state#</b></cfif></td></tr>
               <tr><td height=3></td></tr>
			   <tr><td colspan=2 class="text_xsmall" height=90 valign=top align=top><cfif comp.company_about is "">Not Defined<cfelse><cfif len(comp.company_about) GT 250>#left(comp.company_about,250)#...<cfelse>#comp.company_about#</cfif></cfif></td></tr>
               <tr><td height=5></td></tr>
               <tr><td class="text_xsmall"><b>Capabilities & Services</b></td></tr>
               <tr><td class="text_xsmall" valign=top height=50><cfif #capability_list# is "">Not defined<cfelse><cfif len(capability_list) GT 100>#left(capability_list,100)#...<cfelse>#capability_list#</cfif></cfif></td></tr>
               <tr><td colspan=2 align=right>

               <table cellspacing=0 cellpadding=0 border=0 width=100% style="background-color: c0c0c0; border-radius: 10px; padding-right: 10px; padding-top:3px; padding-bottom:3px;">
               <tr>
                  <td width=70%>&nbsp;</td>
                  <td align=center><img src="/images/small_followers.png" width=15 alt="Followers" title="Followers"></td>
                  <td align=center><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/small_products.png" width=15 alt="Products & Services" title="Products & Services" border=0></a></td>
                  <td align=center><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/small_customers.png" width=15 alt="Customers" title="Customers" border=0></td>
                  <td align=center><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/small_partners.png" width=15 alt="Partners" title="Partners" border=0></td>
               </tr>

               <tr>
                  <td width=70%>&nbsp;</td>
                  <td align=center class="text_xsmall">#followers.total#</td>
                  <td align=center class="text_xsmall">#products.total#</td>
                  <td align=center class="text_xsmall">#customers.total#</td>
                  <td align=center class="text_xsmall">#partners.total#</td>
               </tr>
               </table>

               </td></tr>

           </cfoutput>

		</table>

		</div>

		</cfloop>

	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>