<cfinclude template="/exchange/security/check.cfm">

<script type="text/javascript">
    function clearThis(target){
        target.value= "";
    }
</script>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select search_id, search_name from search
 where search_usr_id = #session.usr_id#
 order by search_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>


		<tr><td colspan=2 class="feed_header">Company Marketplace</td></tr>
		<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Search over 1.5 million Companies.</td></tr>
		<tr><td colspan=2><hr></td></tr>

		<tr>

        <form action="/exchange/marketplace/set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
			<td>

			   <span class="feed_sub_header">Find Company&nbsp;&nbsp;</span>

			   <select name="search_type" class="input_select" style="width: 150px;">
				 <option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>By Name
				 <option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>By Capability
				</select>
				&nbsp;

				<input class="input_text" type="text" style="width: 275px;" placeholder = "keyword" name="keyword" <cfif isdefined("session.keyword")>value="<cfoutput>#session.keyword#</cfoutput>"</cfif> required onfocus="clearThis(this)">

			    <select name="search_socio" class="input_select" style="width: 250px;">
				 <option value=0 <cfif isdefined("session.search_socio") and session.search_socio is "0">selected</cfif>>All Business Types
				 <option value="8a" <cfif isdefined("session.search_socio") and session.search_socio is "8a">selected</cfif>>8a Certified
				 <option value="A2" <cfif isdefined("session.search_socio") and session.search_socio is "A2">selected</cfif>>Woman Owned
				 <option value="8E" <cfif isdefined("session.search_socio") and session.search_socio is "8E">selected</cfif>>Woman Owned - Economically Disadvangated
				 <option value="A5" <cfif isdefined("session.search_socio") and session.search_socio is "A5">selected</cfif>>Veteran Owned
				 <option value="QF" <cfif isdefined("session.search_socio") and session.search_socio is "QF">selected</cfif>>Veteran Owned - Service Disabled
				 <option value="NB" <cfif isdefined("session.search_socio") and session.search_socio is "NB">selected</cfif>>Native American Owned
				 <option value="PI" <cfif isdefined("session.search_socio") and session.search_socio is "PI">selected</cfif>>Hispanice Owned
				 <option value="OY" <cfif isdefined("session.search_socio") and session.search_socio is "OY">selected</cfif>>Black Owned
				 <option value="FR" <cfif isdefined("session.search_socio") and session.search_socio is "FR">selected</cfif>>Asian Pacific Owned
				 <option value="23" <cfif isdefined("session.search_socio") and session.search_socio is "23">selected</cfif>>Minority Owned
				</select>

				&nbsp;<input class="button_blue" type="submit" name="button" value="Search">
			</td>
        </form>

        <!---

        <form action="/exchange/marketplace/refresh.cfm" method="post">
			<td align=right>
				   <span class="feed_sub_header">My Marketplaces</span>&nbsp;&nbsp;
				   <select name="search_id" class="input_select" onchange="this.form.submit()" style="width: 175px;">
					   <cfif views.recordcount GT 0>
					   <option value=0>Select Marketplace
					   <cfoutput query="views">
						<option value=#search_id# <cfif isdefined("session.search_id") and session.search_id is #search_id#>selected</cfif>>#ucase(search_name)#
					   </cfoutput>
					   <cfelse>
						<option value=0>No Markets Created
					   </cfif>
				   </select></form>
				   &nbsp;<input class="button_blue" type="submit" name="button" value="Create" onclick="window.location = '/exchange/marketplace/create_view.cfm'";>
		 --->

		</td>
        </tr>

</table>

