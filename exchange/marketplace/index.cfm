<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>
<cfset #session.search_id# = 0>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.featured_scroll_box {
    height: 150px;
    background-color: ffffff;
    overflow:auto;
}
.featured_badge_block {
    width: 44%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 390px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 3px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<style>
#page-loader {
position: absolute;
top: 50;
bottom: 0;
left: 0;
right: 0;
z-index: 10000;
display: none;
text-align: center;
width: 100%;
padding-top: 300px;
background-color: rgba(255, 255, 255, 0.7);
}
</style>

<style>
#lds-roller {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-roller div {
  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  transform-origin: 32px 32px;
}
.lds-roller div:after {
  content: " ";
  display: block;
  position: absolute;
  width: 6px;
  height: 6px;
  border-radius: 50%;
  background: #fff;
  margin: -3px 0 0 -3px;
}
.lds-roller div:nth-child(1) {
  animation-delay: -0.036s;
}
.lds-roller div:nth-child(1):after {
  top: 50px;
  left: 50px;
}
.lds-roller div:nth-child(2) {
  animation-delay: -0.072s;
}
.lds-roller div:nth-child(2):after {
  top: 54px;
  left: 45px;
}
.lds-roller div:nth-child(3) {
  animation-delay: -0.108s;
}
.lds-roller div:nth-child(3):after {
  top: 57px;
  left: 39px;
}
.lds-roller div:nth-child(4) {
  animation-delay: -0.144s;
}
.lds-roller div:nth-child(4):after {
  top: 58px;
  left: 32px;
}
.lds-roller div:nth-child(5) {
  animation-delay: -0.18s;
}
.lds-roller div:nth-child(5):after {
  top: 57px;
  left: 25px;
}
.lds-roller div:nth-child(6) {
  animation-delay: -0.216s;
}
.lds-roller div:nth-child(6):after {
  top: 54px;
  left: 19px;
}
.lds-roller div:nth-child(7) {
  animation-delay: -0.252s;
}
.lds-roller div:nth-child(7):after {
  top: 50px;
  left: 14px;
}
.lds-roller div:nth-child(8) {
  animation-delay: -0.288s;
}
.lds-roller div:nth-child(8):after {
  top: 45px;
  left: 10px;
}
@keyframes lds-roller {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>

<div id="page-loader">
    <h2><font color="000000" style="font-family: arial;">Loading results...</font></h2>
</div>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/marketplace/menu.cfm">
      <cfinclude template="/exchange/marketplace/recent.cfm">
      <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/marketplace/search_companies.cfm">
		</div>

		<div class="main_box">

		<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from featured
		 join company on company_id = featured_company_id
		 where featured_active = 1

		 <cfif isdefined("session.hub")>
		  and featured_hub_id = #session.hub#
		 <cfelse>
		  and featured_hub_id is null
		 </cfif>

         order by featured_order ASC
		</cfquery>

		  <center>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <cfoutput>
				   <tr><td class="feed_header" width=30><img src="/images/icon_featured.png" width=20></td><td class="feed_header">FEATURED COMPANIES<cfif comp.recordcount GT 0> (#trim(comp.recordcount)#)</cfif></td>
					   <td align=right class="feed_option" colspan=2></td>
				   </tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=20></td></tr>
				   </cfoutput>

				  </table>

				<cfif isdefined("v")>

				<cfelse>

					<cfif #comp.recordcount# is 0>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					  <tr><td class="feed_sub_header"><b>No Featured Companies have been selected.</b></td></tr>
					 </table>

					</cfif>

					<center>

					<cfloop query="comp">
						<div class="featured_badge_block">
							<cfinclude template="/exchange/marketplace/featured_badge.cfm">
						</div>
					</cfloop>
					</center>

				</cfif>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>