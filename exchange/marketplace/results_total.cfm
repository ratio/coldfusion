<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <cfif isdefined("keyword")>
       <cfset #session.keyword# = "#keyword#">
      </cfif>

 	  <div class="exchange_filter_box">
	      <cfinclude template="search_companies.cfm">
	  </div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  				<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">

select company_name, company_logo, company_website, company_id, company_duns, company_city, company_zip, company_state,
(select sum(subaward_amount) from award_data_sub table1 where table1.subawardee_duns = table3.company_duns) as sub,
(select sum(federal_action_obligation) from award_data table2 where table2.recipient_duns = table3.company_duns) as prime,
((select sum(subaward_amount) from award_data_sub table1 where table1.subawardee_duns = table3.company_duns) +
 (select sum(federal_action_obligation) from award_data table2 where table2.recipient_duns = table3.company_duns)) as total
 from company table3
 where (company_discoverable is null or company_discoverable = 1) and
(contains((company_name, company_duns),'"#session.keyword#"'))
group by company_name, company_website, company_id, company_logo, company_duns, company_city, company_state, company_zip








<!---
  				<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select company_logo, company_website, count(award_data.id) as total, sum(award_data.federal_action_obligation) as amount, company_id, company_name, company_duns, company_city, company_zip, company_state from company
				  left join award_data on recipient_duns = company_duns
				  where (company_discoverable is null or company_discoverable = 1) and
				  (contains((company_name, company_duns),'"#session.keyword#"'))
				  group by company_name, company_website, company_id, company_logo, company_duns, company_city, company_state, company_zip --->

		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by name DESC
							<cfelseif #sv# is 10>
							 order by name ASC
							<cfelseif #sv# is 2>
							 order by city ASC
							<cfelseif #sv# is 20>
							 order by city DESC
							<cfelseif #sv# is 3>
							 order by state ASC
							<cfelseif #sv# is 30>
							 order by state DESC
							<cfelseif #sv# is 4>
							 order by zip ASC
							<cfelseif #sv# is 40>
							 order by zip DESC
							<cfelseif #sv# is 6>
							 order by duns ASC
							<cfelseif #sv# is 60>
							 order by duns DESC
							<cfelseif #sv# is 7>
							 order by sub desc
							<cfelseif #sv# is 70>
							 order by sub asc
							<cfelseif #sv# is 8>
							 order by prime DESC
							<cfelseif #sv# is 80>
							 order by prime ASC
							<cfelseif #sv# is 9>
							 order by total DESC
							<cfelseif #sv# is 90>
							 order by total ASC
                            </cfif>

                         <cfelse>

                           <!--- order by name ASC --->

                         </cfif>

				</cfquery>

           <cfoutput>
           		<tr><td class="feed_header">Search Results (#numberformat(companies.recordcount,'999,999,999')# )</td></tr>
           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

			<tr><td colspan=8>&nbsp;</td>
			    <td colspan=2 align=right class="feed_sub_header">Federal Contract Awards</td></tr>
			<tr><td height=10></td></tr>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a></td>
                    <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a></td>
                    <td class="feed_sub_header"><b>Website</b></td>
                    <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a></td>
                    <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
                    <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a></td>
                    <td class="feed_sub_header" align=right width=100><a href="results.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Sub</b></a></td>
                    <td class="feed_sub_header" align=right width=100><a href="results.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Prime</b></a></td>
                    <td class="feed_sub_header" align=right width=100><a href="results.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Total</b></a></td>
				</tr>
				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #row_counter# = 0>

				<cfloop query="companies">
<!---
					<cfquery name="total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select count(id) as awards, sum(federal_action_obligation) as value from award_data
					 where recipient_duns = '#companies.duns#'
					</cfquery> --->

				<cfoutput>

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td class="feed_option">

                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40>
					</cfif>

					</td>
					<td class="feed_sub_header" width=400><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</td>
					<td class="feed_option" width=100>#ucase(companies.company_duns)#</td>
					<td class="feed_option"><cfif len(companies.company_website) GT 40>#ucase(left(companies.company_website,40))#...<cfelse>#ucase(companies.company_website)#</cfif></td>
					<td class="feed_option" width=150>#ucase(companies.company_city)#</td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</td>
					<td class="feed_option" width=100 align=center>#companies.company_zip#</td>
					<td class="feed_option" align=right>#numberformat(companies.sub,'$999,999,999')#</td>
					<td class="feed_option" align=right>#numberformat(companies.prime,'$999,999,999')#</td>
					<td class="feed_option" align=right>#numberformat(companies.total,'$999,999,999')#</td>

<!---					<a href="/exchange/include/award_summary.cfm?duns=#companies.company_duns#" target="_blank" rel="noopener" rel="noreferrer">#numberformat(companies.prime,'$999,999,999,999')#</a> --->

				</tr>

				<tr><td colspan=10><hr></td></tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				</cfoutput>

				</cfloop>

			<cfelse>

			<tr><td class="feed_option">No companies exist.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>
		  </table>

	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>