<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/marketplace/menu.cfm">
      <cfinclude template="/exchange/marketplace/recent.cfm">
      <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

      <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Create Marketplace</td>
	           <td align=right class="feed_option"><a href="/exchange/marketplace/"><img src="/images/delete.png" border=0 width=20 alt="Cancel" title="Cancel"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="save_view.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="form_header" width=150><b>Marketplace Name</b></td></td></tr>
           <tr><td><input class="input_text" type="text" name="search_name" size=40 maxlength="200" required></td></tr>

           <tr><td class="form_header" width=150 valign=top><b>Description</b></td></tr>
           <tr><td><textarea name="search_description" cols=90 rows=3 class="input_textarea"></textarea></td></tr>

          <tr><td class="form_header" width=100><b>States to Include</b></td></tr>
          <tr><td class="feed_option"><input class="input_text" type="text" name="search_state_list" size=40 maxlength="200">&nbsp;*&nbsp;i.e., VA, MD</td></tr>

          <tr><td class="form_header" width=100><b>Zip Codes</b></td></tr>
          <tr><td class="feed_option"><input class="input_text" type="text" name="search_zip_list" size=40 maxlength="1000">&nbsp;*&nbsp;i.e., 22553, 22102</td></tr>

          <tr><td class="form_header" width=100><b>NAICS Code(s)</b></td></tr>
          <tr><td><input type="text" class="input_text" name="search_naics_list" size=40 maxlength="1000">&nbsp;*</td></tr>

          <tr><td height=10>&nbsp;</td></tr>
	      <tr><td class="text_xsmall" colspan=2><b><i>* - for multiple options, seperate each value with a comma.</i></b></td></tr>
          <tr><td height=10>&nbsp;</td></tr>
          <tr><td colspan=2>
          <input class="button_blue" type="submit" name="button" value="Create">
          </td></tr>

          </table>

          </form>


	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>