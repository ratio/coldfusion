<cfinclude template="/exchange/security/check.cfm">

<cfset location = 2>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}
.follow_badge {
    width: 47%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 300px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/marketplace/menu.cfm">
      <cfinclude template="/exchange/marketplace/recent.cfm">
      <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

		  <div class="main_box">
		   <cfinclude template="/exchange/marketplace/search_companies.cfm">
		  </div>

		  <div class="main_box">

		<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select follow_company_id from follow
		 where follow_by_usr_id = #session.usr_id# and
		 follow_company_id is not null
		</cfquery>

		<cfif #follow.recordcount# is 0>
		 <cfset #follow_list# = 0>
		<cfelse>
		 <cfset follow_list = #valuelist(follow.follow_company_id)#>
		</cfif>

		<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from company
		 where (company_discoverable is null or company_discoverable = 1)
		 and company_id in (#follow_list#)
		 order by company_name
		</cfquery>

		  <center>
		  <table cellspacing=0 cellpadding=0 border=0 width=99%>

			  <tr><td valign=top>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <cfoutput>
				   <tr><td class="feed_header" width=30><img src="/images/small_follow.png" width=20></td><td class="feed_header">COMPANIES I FOLLOW <cfif #companies.recordcount# GT 0>(#companies.recordcount#)</cfif></td>
					   <td align=right class="feed_option">
				   <tr><td colspan=2><hr></td></tr>
				   <tr><td height=20></td></tr>
				   </cfoutput>

				  </table>

				  </form>

				<cfif isdefined("v")>

				<cfelse>

					<cfif #companies.recordcount# is 0>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					  <tr><td class="feed_sub_header" style="font-weight: normal;">You are not following any companies.</td></tr>
					 </table>

					</cfif>

					<center>
					<cfloop query="companies">
						<div class="follow_badge">
							<cfinclude template="/exchange/marketplace/company_badge.cfm">
						</div>
					</cfloop>
					</center>

				</cfif>

			  </td></tr>

		 </table>
		 </center>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>