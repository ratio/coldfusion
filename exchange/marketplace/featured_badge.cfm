<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select follow_company_id from follow
 where follow_by_usr_id = #session.usr_id# and
       follow_company_id = #comp.company_id#
</cfquery>

<cfquery name="capabilities" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select topic_name from topic, align
 where topic.topic_id = align.align_type_value and
	   align.align_type_id = 4 and align.align_company_id = #comp.company_id#
</cfquery>

<cfset #capability_list# = #valuelist(capabilities.topic_name,', ')#>

<cfquery name="products" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(product_id) as total from product
 where product_company_id = #comp.company_id#
</cfquery>

<cfquery name="customers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(customer_id) as total from customer
 where customer_company_id = #comp.company_id#
</cfquery>

<cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(follow_id) as total from follow
 where follow_company_id = #comp.company_id#
</cfquery>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select portfolio_id from portfolio
 left join portfolio_item on portfolio_item_portfolio_id = portfolio_id
 left join usr on usr_id = portfolio_usr_id
 where portfolio_usr_id = #session.usr_id# and
	   portfolio_company_id = #session.company_id# and
	   portfolio_type_id = 1 and
	   portfolio_item_company_id = #comp.company_id#
</cfquery>

<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(company_intel_id) as total from company_intel
 left join comments_rating on comments_rating_value = company_intel_rating
 left join usr on usr_id = company_intel.company_intel_created_by_usr_id
 where company_intel_company_id = #comp.company_id# and
       (company_intel_created_by_usr_id = #session.usr_id# or company_intel_created_by_company_id = #session.company_id#)
</cfquery>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	<tr><td height=5></td></tr>

	   <cfoutput>

           <tr><td valign=middle width=130>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

			   <tr><td>

					<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif comp.company_logo is "">
					  <img src="//logo.clearbit.com/#comp.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
					  <img src="#media_virtual#/#comp.company_logo#" width=100 border=0>
					</cfif>
					</a>

			   </td></tr>

		  </table>

		   <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr><td class="feed_title"><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(company_name)#</a></td>

			   <td align=right valign=top width=100>

			   <cfif #follow.recordcount# GT 0>
				<a href="unfollow.cfm?id=#company_id#&l=#location#"><img src="/images/small_unfollow.png" width=20 alt="Stop Following Company" title="Stop Following Company" border=0 align=middle></a>
			   <cfelse>
				<a href="follow.cfm?id=#company_id#&l=#location#"><img src="/images/small_follow.png" width=20 alt="Follow Company" title="Follow Company" border=0 align=middle></a>
			   </cfif>

			   <cfif check.recordcount GT 0>
			     &nbsp;<img src="/images/icon_green_check.png" style="cursor: pointer;" width=20 align=middle alt="Exists in Portfolio(s)" title="Exists in Portfolio(s)" onclick="window.open('/exchange/include/save_company.cfm?id=#comp.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=525'); return false;">
			   <cfelse>
			     &nbsp;<img src="/images/plus3.png" style="cursor: pointer;" width=18 align=middle alt="Add to Portfolio" title="Add to Portfolio" onclick="window.open('/exchange/include/save_company.cfm?id=#comp.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=525'); return false;">
			   </cfif>

			   </td>

               </tr>
               <tr><td class="feed_option">

				<cfif #comp.company_city# is "" or #comp.company_state# is "">

				 <cfif #comp.company_city# is "">
				  #comp.company_state#
				 <cfelse>
				  #comp.company_state#
				 </cfif>

				<cfelse>
				 <b>#comp.company_city#, #comp.company_state#</b>
				</cfif>

			   </td></tr>

		       <tr><td colspan=3 class="feed_option" valign=top><cfif comp.company_about is "">Companay description not provided.<cfelse><cfif len(comp.company_about) GT 250>#left(comp.company_about,250)#...<cfelse>#comp.company_about#</cfif></cfif></td></tr>
		   </table>

		   </td></tr>

           <tr><td colspan=3><hr></td></tr>

           <tr><td colspan=3>

               <div class="featured_scroll_box">

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td colspan=3 class="feed_option"><b>#comp.featured_title#</b></td></tr>
			   <tr><td colspan=3 class="feed_option">#comp.featured_comments#</td></tr>

			   <tr><td colspan=3><hr></td></tr>

			   <cfif comp.company_keywords is not "">
				   <tr><td colspan=2 class="feed_option" height=40><b>Expertise: </b>#comp.company_keywords#</td></tr>
			   <cfelse>
				   <tr><td colspan=2 class="feed_option" height=40><b>Expertise: </b>Not provided.</td></tr>
			   </cfif>

			   </table>

			   </div>

           </td></tr>

			   <tr><td colspan=3><hr></td></tr>
               <tr><td colspan=3 class="feed_option">Followers <b>#followers.total#</b> | Customers <b>#customers.total#</b> | Products <a href="/exchange/include/profile.cfm?l=3"><b>#products.total#</b></a> | Company Intel <a href="/exchange/include/profile.cfm?l=30"><b>#intel.total#</b></a></td></tr>

           <tr><td colspan=3>

           </td></tr>


		   </table>

 </cfoutput>