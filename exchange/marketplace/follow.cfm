<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("id")>

	<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into follow
	 (follow_company_id, follow_hub_id, follow_by_usr_id, follow_date)
	  values(#id#, #session.hub#, #session.usr_id#,#now()#)
	</cfquery>

<cfelse>

	<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into follow
	 (follow_company_id, follow_hub_id, follow_by_usr_id, follow_date)
	  values(#session.company_profile_id#, #session.hub#, #session.usr_id#,#now()#)
	</cfquery>

</cfif>

<cfif isdefined("l")>
 <cfif l is 1>
	<cflocation URL="/exchange/marketplace/" addtoken="no">
 <cfelseif l is 2>
	<cflocation URL="/exchange/marketplace/hubs/hub_companies.cfm" addtoken="no">
 <cfelseif l is 3>
	<cflocation URL="/exchange/include/profile.cfm?u=3" addtoken="no">
 </cfif>
</cfif>

