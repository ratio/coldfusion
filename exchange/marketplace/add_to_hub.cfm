<cfinclude template="/exchange/security/check.cfm">

<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from search
 where search_id = #group_id#
       and search_usr_id = #session.usr_id#
</cfquery>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select search_id, search_name from search
 where search_usr_id = #session.usr_id#
 order by search_name
</cfquery>

<cfif not isdefined("sv")>
 <cfset #sv# = 10>
</cfif>

<cfif not isdefined("select")>
 <cfset select = 0>
</cfif>

				<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select distinct(duns), legal_business_name, pri_naics, city_1, zip_1, poc_email, state_1, corp_url, poc_us_phone from sams
                  where duns is not null
                  <cfif listlen(search.search_state_list) GT 0>
                   <cfif listlen(search.search_state_list) is 1>
                    and state_1 = '#search.search_state_list#'
                   <cfelse>
                   and
                    <cfset #scounter# = 1>
                    <cfloop index="selement" list=#search.search_state_list#>
                      state_1 = '#trim(selement)#'
                      <cfif #scounter# LT listlen(search.search_state_list)> or </cfif>
                      <cfset #scounter# = #scounter# + 1>
                    </cfloop>
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_zip_list) GT 0>
                   <cfif listlen(search.search_zip_list) is 1>
                    and zip_1 = '#search.search_zip_list#'
                   <cfelse>
                   and
                    <cfset #zcounter# = 1>
                    <cfloop index="zelement" list=#search.search_zip_list#>
                      zip_1 = '#trim(zelement)#'
                      <cfif #zcounter# LT listlen(search.search_zip_list)> or </cfif>
                      <cfset #zcounter# = #zcounter# + 1>
                    </cfloop>
                 </cfif>
                 </cfif>

                  <cfif listlen(search.search_naics_list) GT 0>
                   <cfif listlen(search.search_naics_list) is 1>
                    and pri_naics = '#search.search_naics_list#'
                   <cfelse>
                   and
                    <cfset #ncounter# = 1>
                    <cfloop index="nelement" list=#search.search_naics_list#>
                      pri_naics = '#trim(nelement)#'
                      <cfif #ncounter# LT listlen(search.search_naics_list)> or </cfif>
                      <cfset #ncounter# = #ncounter# + 1>
                    </cfloop>
                 </cfif>
                 </cfif>

				<cfif isdefined("sv")>
				 <cfif #sv# is 1>
				  order by legal_business_name DESC
				 <cfelseif #sv# is 10>
				  order by legal_business_name ASC
				 <cfelseif #sv# is 2>
				  order by city_1 ASC
				 <cfelseif #sv# is 20>
				  order by city_1 DESC
				 <cfelseif #sv# is 3>
				  order by state_1 ASC
				 <cfelseif #sv# is 30>
				  order by state_2 DESC
				 <cfelseif #sv# is 4>
				  order by zip_1 ASC
				 <cfelseif #sv# is 40>
				  order by zip_1 DESC

				 <cfelseif #sv# is 5>
				  order by corp_url ASC
				 <cfelseif #sv# is 50>
				  order by corp_url DESC

				 <cfelseif #sv# is 6>
				  order by duns ASC
				 <cfelseif #sv# is 60>
				  order by duns DESC

				 <cfelseif #sv# is 7>
				  order by poc_email ASC
				 <cfelseif #sv# is 70>
				  order by poc_email DESC

				 <cfelseif #sv# is 8>
				  order by pri_naics ASC
				 <cfelseif #sv# is 80>
				  order by pri_naics DESC


				 </cfif>
				<cfelse>
                  order by legal_business_name ASC
				</cfif>

				</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		  <cfoutput>
           <tr><td class="feed_header">#search.search_name# (#numberformat(companies.recordcount,'999,999')# Companies)</td>
               <td class="feed_option" align=right><a href="group.cfm?group_id=#group_id#">Return</a></td></tr>
          </cfoutput>

           <tr><td>&nbsp;</td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			  <tr>

			  <cfoutput>

                 <td class="feed_option" align=center><a href="add_to_hub.cfm?group_id=#group_id#&sv=#sv#&select=<cfif select is 0>1<cfelse>0</cfif>"><b>Select</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Website</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Contact Email</b></a></td>
				 <td class="feed_option"><a href="add_to_hub.cfm?group_id=#group_id#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a></td>

			  </cfoutput>

			  </tr>

				<cfset #row_counter# = 0>

				<form action="add_to_hub_db.cfm" method="post">

				<cfoutput query="companies">
				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="e0e0e0"
				</cfif>
				>
					<td class="feed_option" align=center width=75><input type="checkbox" name="add" value="#companies.duns#" <cfif #select# is 1>checked</cfif>></td>
					<td class="feed_option" width=300><a href="/exchange/include/company_profile.cfm?id=0&duns=#companies.duns#" target="_blank" rel="noopener" rel="noreferrer">#companies.legal_business_name#</a></td>
					<td class="feed_option" width=150>#companies.city_1#</a></td>
					<td class="feed_option" width=50>#companies.state_1#</a></td>
					<td class="feed_option" width=75>#companies.zip_1#</a></td>
					<td class="feed_option">

					<cfif #len(companies.corp_url)# GT 30>
					 <cfset cn = #left(companies.corp_url,'30')# & "...">
					<cfelse>
					 <cfset cn = #companies.corp_url#>
					</cfif>
					<a href="#companies.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#cn#</a></td>

					<td class="feed_option" width=100>#companies.duns#</td>
					<td class="feed_option" width=100>

					<cfif #len(companies.poc_email)# GT 30>
					 <cfset em = #left(companies.poc_email,'30')# & "...">
					<cfelse>
					 <cfset em = #companies.poc_email#>
					</cfif>
					<a href="mailto:#companies.poc_email#">#em#</a></td>

					<td class="feed_option" width=100>#companies.pri_naics#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

                <input type="hidden" name="group_id" value=#group_id#>

				</cfoutput>

		        <tr><td height=10></td></tr>
		        <tr><td colspan=2><input class="button_blue" type="submit" name="button" value="Add Companies to Hub" vspace=10 style="width: 150px;"></td></tr>

				</form>

			<cfelse>

			<tr><td class="feed_option">No companies exist.</td></tr>

			</cfif>

			</table>





















	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>