<cfinclude template="/exchange/security/check.cfm">




<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style style="text/css">
    .break {
        word-wrap: break-word;
     }
  	.hoverTable{
		width:100%;
		border-collapse:collapse;
	}
	.hoverTable td{
		padding:7px; border:#e0e0e0 1px solid;
	}
	/* Define the default color for all the table rows */
	.hoverTable tr{
		background: #ffffff;
	}
	/* Define the hover highlight color for the table row */
    .hoverTable tr:hover {
          background-color: #e0e0e0;
    }
</style>


  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  			<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				select a.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Exchange' as source from company a

                <cfif isdefined("session.add_filter")>
				 where contains((company_meta_keywords, company_about, company_history, company_keywords, company_homepage_text),'"#session.keyword#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
                   and contains((company_meta_keywords, company_about, company_history, company_keywords, company_homepage_text),'"#i#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 where contains((company_meta_keywords, company_about, company_history, company_keywords, company_homepage_text),'"#session.keyword#"')
                </cfif>

				union

				select b.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Federal Awards' as source from company b
				left join award_data on recipient_duns = b.company_duns

                <cfif isdefined("session.add_filter")>
				   where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#session.keyword#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				    and contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#i#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				 where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#session.keyword#"')
                </cfif>

 			union

				select c.company_website, company_meta_keywords, company_id, company_name, company_keywords, company_duns, company_city, company_state, company_zip, company_logo, 'Federal SBIR' as source from company c
				left join sbir on sbir.duns = c.company_duns

                <cfif isdefined("session.add_filter")>
				  where contains((award_title, abstract, research_keywords),'"#session.keyword#"')
                 <cfloop index="i" list="#session.add_filter#">
                   <cfoutput>
				     and contains((award_title, abstract, research_keywords),'"#i#"')
                   </cfoutput>
                 </cfloop>
                <cfelse>
				where contains((award_title, abstract, research_keywords),'"#session.keyword#"')
                </cfif>

		    		     <cfif isdefined("sv")>

							<cfif #sv# is 1>
							 order by company_name DESC
							<cfelseif #sv# is 10>
							 order by company_name ASC
							<cfelseif #sv# is 2>
							 order by company_city ASC
							<cfelseif #sv# is 20>
							 order by company_city DESC
							<cfelseif #sv# is 3>
							 order by company_state ASC
							<cfelseif #sv# is 30>
							 order by company_state DESC
							<cfelseif #sv# is 4>
							 order by company_zip ASC
							<cfelseif #sv# is 40>
							 order by company_zip DESC
							<cfelseif #sv# is 7>
							 order by source ASC, company_name ASC
							<cfelseif #sv# is 70>
							 order by source DESC, company_name ASC
                            </cfif>

                         <cfelse>

                           order by company_name ASC

                         </cfif>

			</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_companies_to_excel.cfm">
           </cfif>


           <cfoutput>

           <form action="set.cfm" method="post">
           		<tr><td class="feed_header">SEARCH RESULTS

           		<cfif companies.recordcount is 0>
           		( No Companies )
           		<cfelseif companies.recordcount is 1>
           		( 1 Company )
           		<cfelse>
           		(#trim(numberformat(companies.recordcount,'999,999,999'))# Companies)
           		</cfif>

           		</td>
           		    <td class="feed_sub_header" align=right>

		            <b>Add Filter</b>&nbsp;&nbsp;

                    <input class="input_text" type="text" style="width: 200px;" name="add_filter">
            &nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Add"></td>

           </form>

         		</tr>
           		<tr><td colspan=2><hr></td></tr>
           		<tr><td class="feed_option"><b>You searched for company capabilities matching <i>"#session.keyword#"

           		<cfif isdefined("session.add_filter")>

           		 <cfloop index="i" list="#session.add_filter#">
           		  and "#i#"
           		 </cfloop>

           		</cfif>


           		</i></b></td>
           		    <td class="feed_option" align=right>

	                &nbsp;&nbsp;&nbsp;<a href="/exchange/marketplace/capability.cfm"><img src="/images/icon_list.png" alt="List View" title="List View" width=23 border=0 align=absmiddle></a>
	                &nbsp;&nbsp;&nbsp;<a href="/exchange/marketplace/trend.cfm"><img src="/images/icon_trend.png" alt="Buying Patterns" title="Buying Patterns" width=23 border=0 align=absmiddle></a>
	                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/exchange/marketplace/capability.cfm?export=1"><img src="/images/icon_export_excel.png" alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>

           		    </td>

           		</tr>

      		    <cfif isdefined("session.add_filter")>

      		        <tr><td height=10></td></tr>

					<tr><td clsss="feed_option">

						 <cfloop index="i" list="#session.add_filter#">
						   <cfset size = evaluate(10*len(i))>
						   <input class="button_blue" style="font-size: 11px; height: 25px; width: auto" type="submit" name="button" value="X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#ucase(i)#&nbsp;&nbsp;"  onclick="location.href = 'remove.cfm?i=#i#'">&nbsp;&nbsp;
						 </cfloop>

					</td></tr>

          		</cfif>

           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>&nbsp;</td></tr>

			<tr><td colspan=4>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif #companies.recordcount# GT 0>

			<cfoutput>

				<tr>
                    <td width=80></td>
                    <td class="feed_sub_header"><a href="capability.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a></td>
                    <td class="feed_sub_header"><b>Tags</b></td>
                    <td class="feed_sub_header"><b>Website</b></td>
                    <td class="feed_sub_header"><a href="capability.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a></td>
                    <td class="feed_sub_header" align=center><a href="capability.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
<!---                    <td class="feed_sub_header" align=center><a href="capability.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zip</b></a></td> --->
                    <td class="feed_sub_header" align=right><a href="capability.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Source</b></a></td>
			    </tr>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

				<cfset #row_counter# = 0>
                <cfset count = 1>

				<cfloop query="companies">

				<cfoutput>

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td class="feed_option">

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header"><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option">

					<cfif companies.company_keywords is "">
					#ucase(companies.company_meta_keywords)#
					<cfelse>
					#ucase(wrap(companies.company_keywords,40,0))#
					</cfif>

					</td>

					<td class="feed_option">
					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(companies.company_website) GT 40>
					#ucase(left(companies.company_website,40))#...
					<cfelse>
					#ucase(companies.company_website)#
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
					<td class="feed_option" align=right width=100>#companies.source#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				<cfif count is not companies.recordcount>
				 <tr><td colspan=9><hr></td></tr>
				</cfif>

				<cfset count = count + 1>

				</cfoutput>

				</cfloop>

			<cfelse>

			<tr><td class="feed_option">No companies exist.</td></tr>

			</cfif>

			</table>

			</td></tr>

	    </table>

           </td></tr>
		  </table>

	  </div>

	  </td></tr>

 </table>

 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>