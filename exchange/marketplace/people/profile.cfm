<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfset member = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

<cfif profile.recordcount is 0>
	<table cellspacing=0 cellpadding=0 border=0 width=700 bgcolor="ffffff">
     <tr><td>User not found.</td></tr>
    </table>
    <cfabort>
</cfif>

<cfquery name="certs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select certification_name from usr_cert
  join certification on certification_id = usr_cert_cert_id
  where usr_cert_hub_id = #session.hub# and
        usr_cert_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
        order by certification_name
</cfquery>

<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfif profile.usr_id is not session.usr_id>
	<cfquery name="view" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into usr_view
	 (
	  usr_view_usr_id,
	  usr_view_by_usr_id,
	  usr_view_by_company_id,
	  usr_view_date,
	  usr_view_hub_id
	  )
	  values
	  (
	  #profile.usr_id#,
	  #session.usr_id#,
	  #session.company_id#,
	  #now()#,
	  #session.hub#
	  )
	</cfquery>
</cfif>

<cfif isdefined("f")>

 <cfif f is 1>

	<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert usr_follow(usr_follow_usr_id, usr_follow_usr_follow_id, usr_follow_date, usr_follow_hub_id)
	 values
	 (#session.usr_id#,#member#,#now()#,#session.hub#)
    </cfquery>

	<cfquery name="get_follower_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select usr_first_name, usr_last_name, usr_email from usr
	 where usr_id = #session.usr_id#
    </cfquery>

	<cfquery name="get_followed_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select usr_first_name, usr_last_name, usr_email from usr
	 where usr_id = #member#
    </cfquery>

    <cfif isdefined("test_email")>
     <cfset #to# = #test_email#>
    <cfelse>
     <cfset #to# = #tostring(tobinary(get_followed_info.usr_email))#>
    </cfif>

	<cfmail from="#h_info.hub_name# <noreply@ratio.exchange>"
			  to="#to#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#get_followed_info.usr_first_name#, you are now being followed by #get_follower_info.usr_first_name# #get_follower_info.usr_last_name# on the #h_info.hub_name#">
	<html>
	<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	</head>
	<body class="body">

	<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>Hi #get_followed_info.usr_first_name# #get_followed_info.usr_last_name#</b></td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">You are now being followed by #get_follower_info.usr_first_name# #get_follower_info.usr_last_name# on the #h_info.hub_name#.</td></tr>
	 <tr><td height=30></td></tr>
	 <tr><td><a href="#h_info.hub_login_page#"><b>Login to #h_info.hub_name#</b></a></td></tr>
	 <tr><td height=30></td></tr>
	 <tr><td>#h_info.hub_support_name#</td></tr>
	 <tr><td>#h_info.hub_support_phone#</td></tr>
	 <tr><td>#h_info.hub_support_email#</td></tr>

	</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>

 <cfelse>
 	<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 	 delete usr_follow
 	 where usr_follow_usr_id = #session.usr_id# and
 	 usr_follow_usr_follow_id = #member# and
 	 usr_follow_hub_id = #session.hub#
    </cfquery>
 </cfif>
</cfif>

<cfif profile.usr_company_id is not "">

	<cfquery name="usr_company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from company
	 where company_id = #profile.usr_company_id#
	</cfquery>

</cfif>

<cfquery name="following" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_follow
 where usr_follow_hub_id = #session.hub# and
       usr_follow_usr_id = #session.usr_id# and
       usr_follow_usr_follow_id = #profile.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box" style="padding-top: 30px; padding-left: 30px; padding-right: 30px;">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=20%>

		   <cfinclude template="left.cfm">

           </td><td width=40>&nbsp;</td><td valign=top width=100%>

           <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_header" style="font-size: 40px; padding-bottom: 0px;">#profile.usr_first_name# #profile.usr_last_name#</td>
			      <td class="feed_header" align=right>

			      <cfif session.usr_id is not #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

					  <input type="submit" name="button" class="button_blue" value="Send Message" onclick="window.open('message.cfm?i=#i#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=525'); return false;">
					  &nbsp;&nbsp;

					  <cfif following.recordcount GT 0>
					  <input type="submit" name="button" class="button_blue" style="background-color: gray;" value="UnFollow" onClick="location.href='profile.cfm?f=0&i=#i#'">
                      <cfelse>
					  <input type="submit" name="button" class="button_blue" style="background-color: green;" value="Follow" onClick="location.href='profile.cfm?f=1&i=#i#'">
					  </cfif>

			      </cfif>

			      </td></tr>
			  <tr>

			      <td class="feed_sub_header" style="color: green;" align=right colspan=2>
			      <cfif isdefined("f")>
			       <cfif f is 1>
			        You are now following #profile.usr_first_name# #profile.usr_last_name#
			       <cfelse>
			        You have stopped following #profile.usr_first_name# #profile.usr_last_name#
			       </cfif>
			      </cfif>

			       </td>
			  </tr>

              </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <cfif profile.usr_company_name is not "">
				<tr><td class="feed_sub_header" colspan=2 style="padding-top: 0px;">#ucase(profile.usr_company_name)#</td></tr>
			  </cfif>

			  <cfif profile.usr_title is not "">
				<tr><td class="feed_sub_header" colspan=2>#profile.usr_title#</td>
				    <td class="feed_sub_header" align=right>#profile.usr_keywords#</td></tr>
			  </cfif>

			  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>


              <tr><td><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr><td class="feed_header">About</td></tr>
              <cfif profile.usr_about is "">
				<tr><td class="feed_sub_header" style="font-weight: normal;">No information available.</td></tr>
              <cfelse>
				<tr><td class="feed_sub_header" style="font-weight: normal;">#profile.usr_about#</td></tr>
			  </cfif>

			   <tr><td><hr></td></tr>
			   <tr><td height=10></td></tr>

			  <tr><td class="feed_header">Experience</td></tr>
              <cfif profile.usr_experience is "">
				<tr><td class="feed_sub_header" style="font-weight: normal;">No information available.</td></tr>
              <cfelse>
				<tr><td class="feed_sub_header" style="font-weight: normal;">#profile.usr_experience#</td></tr>
			  </cfif>

			   <tr><td><hr></td></tr>
			   <tr><td height=10></td></tr>

			  <tr><td class="feed_header">Education</td></tr>
              <cfif profile.usr_education is "">
				<tr><td class="feed_sub_header" style="font-weight: normal;">No information available.</td></tr>
              <cfelse>
				<tr><td class="feed_sub_header" style="font-weight: normal;">#profile.usr_education#</td></tr>
			  </cfif>

			   <tr><td><hr></td></tr>
			   <tr><td height=10></td></tr>

			   <tr><td class="feed_header">Hobbies</td></tr>
               <cfif profile.usr_hobbies is "">
				 <tr><td class="feed_sub_header" style="font-weight: normal;">No information available.</td></tr>
               <cfelse>
				 <tr><td class="feed_sub_header" style="font-weight: normal;">#profile.usr_hobbies#</td></tr>
			   </cfif>

			   <tr><td><hr></td></tr>
			   <tr><td height=10></td></tr>

			   </cfoutput>

			   <tr><td class="feed_header" colspan=2>Certifications</td></tr>

			   <tr><td class="feed_sub_header" style="font-weight: normal;">

				   <cfif certs.recordcount is 0>
					No Certifications Listed
				   <cfelse>

					 <cfoutput query="certs">
					  <li>#certification_name#</li>
					 </cfoutput>

				   </cfif></td></tr>

			   </table>

			   </td></tr>

		   <tr><td height=20></td></tr>

			</table>

            </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

