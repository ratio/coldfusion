<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="contact" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header"></td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
</table>

<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	<tr><td valign=top width=200>

		<table cellspacing=0 cellpadding=0 width=0 width=100%>

		 <cfif #contact.usr_photo# is "">
		  <tr><td class="text_xsmall" align=center><img src="/images/headshot.png" width=175 border=0></td></tr>
		 <cfelse>
		  <tr><td class="text_xsmall" align=center><img style="border-radius: 2px;" src="#media_virtual#/#contact.usr_photo#" width=175></td></tr>
		 </cfif>

		 <tr><td height=10></td></tr>
		 <tr><td class="feed_option" colspan=2 align=center><b>#contact.usr_first_name# #contact.usr_last_name#</b></td></tr>
		 <tr><td class="feed_option" colspan=2 align=center><b>#ucase(contact.usr_company_name)#</b></td></tr>

	     <cfif contact.usr_title is not "">
 		  <tr><td class="feed_option" colspan=2 align=center>#contact.usr_title#</td></tr>
		 </cfif>

		</table>

	   </td><td valign=top>

       <form action="message_send.cfm" method="post">

		<table cellspacing=0 cellpadding=0 width=0 width=100%>
          <tr><td class="feed_header" style="font-size: 30px;">Message #contact.usr_first_name#</td></tr>
          <tr><td height=10></td></tr>
          <tr><td class="feed_sub_header">Subject</td></tr>
          <tr><td><input type="text" class="input_text" style="width: 700px;" name="message_subject"></td></tr>
          <tr><td class="feed_sub_header">Message</td></tr>
          <tr><td><textarea name="message_message" class="input_textarea" style="width: 700px; height: 250px;"></textarea></td></tr>
          <tr><td height=10></td></tr>
          <tr><td><input type="submit" class="button_blue_large" value="Send Message"></td></tr>

		</table>

		<cfoutput>
		 <input type="hidden" name="i" value=#i#>
		</cfoutput>

		</form>

	   </td></tr>

	</table>

</cfoutput>

</center>

</body>
</html>

