<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

	<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfquery name="to" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #session.usr_id#
	</cfquery>

    <cfset m_date = #now()#>

        <!--- Message to User --->

		<cfquery name="insert_from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into message
		 (
		 message_from_id,
		 message_to_id,
		 message_message,
		 message_date,
		 message_updated,
		 message_hub_id,
		 message_read,
		 message_from_first_name,
		 message_from_last_name,
		 message_view_id,
		 message_display_id
		 )
		 values
		 (
		 #session.usr_id#,
		 #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		 '#message_message#',
		  #m_date#,
		  #m_date#,
		  #session.hub#,
		  0,
		 '#from.usr_first_name#',
		 '#from.usr_last_name#',
		  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		  #session.usr_id#
		 )
		</cfquery>

        <!-- Message to Me --->

		<cfquery name="insert_from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into message
		 (
		 message_from_id,
		 message_to_id,
		 message_message,
		 message_date,
		 message_updated,
		 message_hub_id,
		 message_read,
		 message_from_first_name,
		 message_from_last_name,
		 message_view_id,
		 message_display_id
		 )
		 values
		 (
		 #session.usr_id#,
		 #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		 '#message_message#',
		  #m_date#,
		  #m_date#,
		  #session.hub#,
		  1,
		 '#from.usr_first_name#',
		 '#from.usr_last_name#',
		  #session.usr_id#,
		  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		 )
		</cfquery>

	<cfif isdefined("test_email")>
	 <cfset to_email = #test_email#>
	<cfelse>
	 <cfset to_email = #tostring(tobinary(to.usr_email))#>
	</cfif>

	<cfmail from="Message <noreply@ratio.exchange>"
			  to="#to_email#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#to.usr_first_name#, you received a message from #from.usr_first_name# #from.usr_last_name#">
	<html>
	<head><cfoutput>
	<title>#h_info.hub_name#</title>
	</head></cfoutput>
	<body class="body">

	<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>Hi #to.usr_first_name#,</b></td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">You received a message from #from.usr_first_name# #from.usr_last_name#<cfif from.usr_company_name is not "">, from #from.usr_company_name#,</cfif> through the #h_info.hub_name#.  To view or respond to this message, please login.</td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#message_message#</td></tr>
	 <tr><td>&nbsp;</td></tr>
	 <tr><td><a href="#h_info.hub_login_page#"><img src="#image_virtual#/email_login.png" height=30></a></td></tr>
	</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>

         <tr><td class="feed_header">Message has been sent.</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>