<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 170px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/marketplace/people/search_people.cfm">
		</div>

		<div class="main_box">

		 <cfquery name="people" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select usr_id, usr_first_name, usr_last_name, usr_profile_display, usr_photo, usr_title, usr_email, usr_updated from usr
		   join hub_xref on hub_xref_usr_id = usr_id
           where hub_xref_hub_id = #session.hub# and
                 hub_xref_active = 1 and
                 (usr_profile_display = 1 or usr_profile_display is null)
           <cfif isdefined("session.people_keyword")>
            and contains((usr_full_name, usr_first_name, usr_last_name, usr_about, usr_background, usr_keywords, usr_experience),'"#trim(session.people_keyword)#"')
           </cfif>


                 <cfif usr.hub_xref_role_id is 1>
                   <cfif usr.hub_xref_usr_role is 0>
                    and hub_xref_role_id = 1
                   </cfif>
                 </cfif>

		   order by usr_last_name, usr_first_name
		 </cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt people.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(people.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

		  <center>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <cfoutput>

				   <tr><td class="feed_header">
			   		SEARCH RESULTS <cfif #people.recordcount# GT 0>(#people.recordcount#)</cfif>


				   </td>
					   <td align=right>

					   </td>
				   </tr>

                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

				<cfif people.recordcount GT #perpage#>

                   <tr>

				   <td class="feed_sub_header">

						Page
							<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

							<cfif url.start gt 1>
								<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
								<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
							<cfelse>
							</cfif>

							<cfif (url.start + perpage - 1) lt people.recordCount>
								<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
								<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
							<cfelse>
							</cfif>

				   </td>

                   </tr>

                   	<tr><td height=10></td></tr>
				   <cfelse>
                   	<tr><td height=10></td></tr>
				   </cfif>

				   </cfoutput>

				  </table>

            <cfif people.recordcount is 0>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td class="feed_sub_header" style="font-weight: normal;">

                   <cfif session.people_view is 1>
				   		You are not following any People.
				   <cfelseif session.people_view is 2>
				   		No People are following you.
				   <cfelse>
				     No People were found.
				   </cfif>


                  </td></tr>
                </table>

            <cfelse>

	        <cfoutput query="people" startrow="#url.start#" maxrows="#perpage#">

			 <cfquery name="followed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select usr_follow_id from usr_follow
			  where usr_follow_hub_id = #session.hub# and
			  usr_follow_usr_id = #session.usr_id# and
			  usr_follow_usr_follow_id = #people.usr_id#
			 </cfquery>

            <cfset i = #encrypt(people.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

			<div class="profile_badge">

			<cfif people.usr_profile_display is 2>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td align=center><img src="/images/private.png" width=175></td></tr>
                  <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
                </table>


			<cfelse>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr><td valign=top width=130>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif #people.usr_photo# is "">
					  <tr><td align=center valign=top width=110><a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img src="/images/headshot.png" height=110 width=110 border=0 vspace=15></td><td width=20>&nbsp;</td></tr>
					 <cfelse>
					  <tr><td align=center valign=top width=110><a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img style="border-radius: 2px;" vspace=15 src="#media_virtual#/#people.usr_photo#" height=110 width=110 border=0><td width=20>&nbsp;</td></tr>
					 </cfif>

					 </table>

				 <td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=15></td></tr>
				 <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#i#">#people.usr_first_name# #people.usr_last_name#</a></td>
				     <td align=right valign=top>
				     <cfif #followed.recordcount# is 1>
				     <a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img src="/images/small_follow.png" width=20 alt="You are following this person" title="You are following this person" border=0></a>
				     </cfif>
				     </td></tr>

				 <cfif #people.usr_title# is not "">
				 	<tr><td class="feed_option" style="padding-top: 5px; padding-bottom: 8px;" colspan=2>#people.usr_title#</td></tr>
				 </cfif>

			    <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=20 valign=middle>&nbsp;&nbsp;#tostring(tobinary(people.usr_email))#</td></tr>
				<tr><td height=10></td></tr>
				<tr><td colspan=3 class="text_xsmall"><font color="gray" colspan=2><i>Last updated on #dateformat(people.usr_updated,'mmm dd, yyyy')#</i></font></td></tr>
				</table>

				</td></tr>

				</table>

				</cfif>

			</div>

	   	   </cfoutput>

	     </cfif>

		 </td></tr>

		 </table>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>