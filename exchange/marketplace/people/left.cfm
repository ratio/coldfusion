<table cellspacing=0 cellpadding=0 border=0 width=100%>

  <cfoutput>

    <tr><td valign=top>

    <table cellspacing=0 cellpadding=0 width=0 width=100%>
     <cfif #profile.usr_photo# is "">
      <tr><td class="text_xsmall" align=center><img src="/images/headshot.png" width=225 height=225 border=0></td></tr>
     <cfelse>
      <tr><td class="text_xsmall" align=center><img style="border-radius: 180px;" src="#media_virtual#/#profile.usr_photo#" width=225 height=225></td></tr>
     </cfif>

     <tr><td height=20></td></tr>

	 <tr><td align=center>
	 <cfif #profile.usr_linkedin# is not "">
	  <a href="#profile.usr_linkedin#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=30 hspace=5 align=absmiddle></a>
	 </cfif>
	 <cfif #profile.usr_facebook# is not "">
	  <a href="#profile.usr_facebook#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=30 hspace=5 align=absmiddle></a>
	 </cfif>
	 <cfif #profile.usr_twitter# is not "">
	  <a href="#profile.usr_twitter#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=30 hspace=5 align=absmiddle></a>
	 </cfif>
	 </td></tr>

      <tr><td height=15></td></tr>
      <tr><td><hr></td></tr>
      <tr><td class="feed_sub_header">Contact Information</td></tr>
      <tr><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=25 valign=middle>&nbsp;&nbsp;<a href="mailto:#tostring(tobinary(profile.usr_email))#" style="font-weight: normal;">#tostring(tobinary(profile.usr_email))#</a></td></tr>
      <tr><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon-phone_2.png" alt="Work Phone" title="Work Phone" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.usr_phone# is "">Not Provided<cfelse>#profile.usr_phone#</cfif></td></tr>
	  <tr><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon_cell_phone.png" alt="Cell Phone" title="Cell Phone" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.usr_cell_phone# is "">Not Provided<cfelse>#profile.usr_cell_phone#</cfif></td></tr>

      <tr><td><hr></td></tr>
      <tr><td class="feed_sub_header">Location</td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">
	   <cfif profile.usr_city is "" and profile.usr_state is "">
	    Not Specified
	   <cfelse>
	   <cfif profile.usr_city is "" and profile.usr_state is 0>
	    Not Specified
	   <cfelse>
	   #profile.usr_city# <cfif #profile.usr_state# is not 0>#profile.usr_state#</cfif>
	   </cfif>
	   </cfif>
	   </td></tr>


    </table>

  </cfoutput>
</table>