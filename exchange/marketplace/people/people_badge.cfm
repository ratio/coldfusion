<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfoutput>

	<cfif people.usr_profile_display is 2>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td align=center><img src="/images/private.png" width=100></td></tr>
		  <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
		</table>

	<cfelse>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td valign=top width=130>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif #people.usr_photo# is "">
			  <tr><td align=center valign=top width=110><a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img src="/images/headshot.png" height=110 width=110 border=0 vspace=15></td><td width=20>&nbsp;</td></tr>
			 <cfelse>
			  <tr><td align=center valign=top width=110><a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img style="border-radius: 2px;" vspace=15 src="#media_virtual#/#people.usr_photo#" height=110 width=110 border=0><td width=20>&nbsp;</td></tr>
			 </cfif>

			     <tr><td align=center>
			     <cfif #people.usr_linkedin# is not "">
			      <a href="#people.usr_linkedin#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=22 hspace=2 align=absmiddle></a>
			     </cfif>
			     <cfif #people.usr_facebook# is not "">
			      <a href="#people.usr_facebook#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=22 hspace=2 align=absmiddle></a>
			     </cfif>
			     <cfif #people.usr_twitter# is not "">
			      <a href="#people.usr_twitter#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=22 hspace=2 align=absmiddle></a>
			     </cfif>
			     </td></tr>

			 </table>

		 <td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td height=15></td></tr>
		 <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#i#">#people.usr_first_name# #people.usr_last_name#</a></td>
			 <td align=right valign=top>
			 <cfif #followed.recordcount# is 1>
			 <a href="/exchange/marketplace/people/profile.cfm?i=#i#"><img src="/images/small_follow.png" width=20 alt="You are following this person" title="You are following this person" border=0></a>
			 </cfif>
			 </td></tr>

		 <cfif #people.usr_title# is not "">
			<tr><td class="feed_option" style="padding-top: 5px; padding-bottom: 8px;" colspan=2>#people.usr_title#</td></tr>
		 </cfif>

			     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-phone_2.png" alt="Work Phone" title="Work Phone" width=20 valign=middle>&nbsp;&nbsp;<cfif #people.usr_phone# is "">Not Provided<cfelse>#people.usr_phone#</cfif></td></tr>
			     <tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon_cell_phone.png" alt="Cell Phone" title="Cell Phone" width=20 valign=middle>&nbsp;&nbsp;<cfif #people.usr_cell_phone# is "">Not Provided<cfelse>#people.usr_cell_phone#</cfif></td></tr>


		<tr><td class="feed_option" style="font-weight: normal;" colspan=2><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=20 valign=middle>&nbsp;&nbsp;#decrypt(people.usr_email,session.key, "AES/CBC/PKCS5Padding", "HEX")#</td></tr>
		<tr><td height=10></td></tr>
		</table>

		</td></tr>

		</table>

		</cfif>

</cfoutput>