<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.people_view")>
 <cfset session.people_view = 1>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset StructDelete(Session,"search_type")>
<cfset StructDelete(Session,"people_keyword")>

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/marketplace/people/search_people.cfm">
		</div>

		<div class="main_box">

         <cfif session.people_view is 1>

		 <cfquery name="people_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select distinct(usr_follow_usr_follow_id) as usr_id from usr_follow
		   join hub_xref on hub_xref_usr_id = usr_follow_usr_follow_id
		   join usr on usr_id = usr_follow_usr_follow_id
		   where usr_follow_usr_id = #session.usr_id# and
		         (usr_profile_display = 1 or usr_profile_display is null) and
		         hub_xref_active = 1 and
		         usr_follow_hub_id = #session.hub#
           <cfif isdefined("session.people_keyword")>
            and contains((usr_full_name, usr_first_name, usr_last_name, usr_about, usr_background, usr_keywords, usr_experience),'"#trim(session.people_keyword)#"')
           </cfif>
		 </cfquery>

         <cfelseif session.people_view is 2>

		 <cfquery name="people_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select distinct(usr_follow_usr_id) as usr_id from usr_follow
		   join usr on usr_id = usr_follow_usr_id
		   join hub_xref on hub_xref_usr_id = usr_follow_usr_id
		   where usr_follow_usr_follow_id = #session.usr_id# and
		         (usr_profile_display = 1 or usr_profile_display is null) and
		         hub_xref_active = 1 and
		         usr_follow_hub_id = #session.hub#
           <cfif isdefined("session.people_keyword")>
            and contains((usr_full_name, usr_first_name, usr_last_name, usr_about, usr_background, usr_keywords, usr_experience),'"#trim(session.people_keyword)#"')
           </cfif>
		 </cfquery>

         <cfelseif session.people_view is 3>

		 <cfquery name="people_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select distinct(usr_id) as usr_id from usr
		   join hub_xref on hub_xref_usr_id = usr_id
           where hub_xref_hub_id = #session.hub# and
		         (usr_profile_display = 1 or usr_profile_display is null) and
                 hub_xref_active = 1
           <cfif isdefined("session.people_keyword")>
            and contains((usr_full_name, usr_first_name, usr_last_name, usr_about, usr_background, usr_keywords, usr_experience),'"#trim(session.people_keyword)#"')
           </cfif>
		 </cfquery>

		 </cfif>

		 <cfif people_list.recordcount is 0>
		  <cfset plist = 0>
		 <cfelse>
		  <cfset plist = #valuelist(people_list.usr_id)#>
		 </cfif>

		 <cfquery name="people" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select * from usr
           where usr_id in (#plist#)
           order by usr_last_name, usr_first_name
         </cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt people.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(people.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

		  <center>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <cfoutput>

				  <form action="/exchange/marketplace/people/set.cfm" method="post">
				   <tr><td class="feed_header">
				   <cfif session.people_view is 1>
				   		Connections <cfif #people.recordcount# GT 0>(#people.recordcount#)</cfif>
				   <cfelseif session.people_view is 2>
				   		Followers <cfif #people.recordcount# GT 0>(#people.recordcount#)</cfif>
				   <cfelseif session.people_view is 3>
				   		All People <cfif #people.recordcount# GT 0>(#people.recordcount#)</cfif>
				   </cfif>

				   </td>
					   <td align=right>

					   <span class="feed_sub_header">Change View</span>&nbsp;&nbsp;

					   <select name="v" class="input_select" onchange="form.submit()";>
					    <option value=3 <cfif isdefined("session.people_view") and session.people_view is 3>selected</cfif>>All People
						<option value=1 <cfif isdefined("session.people_view") and session.people_view is 1>selected</cfif>>My Connections
					    <option value=2 <cfif isdefined("session.people_view") and session.people_view is 2>selected</cfif>>My 	Followers
					   </select>

					   </td>
				   </tr>

				   </form>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

				<cfif people.recordcount GT #perpage#>

                   <tr>

				   <td class="feed_sub_header">

						Page
							<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

							<cfif url.start gt 1>
								<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
								<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
							<cfelse>
							</cfif>

							<cfif (url.start + perpage - 1) lt people.recordCount>
								<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
								<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
							<cfelse>
							</cfif>

				   </td>

                   </tr>

                   	<tr><td height=10></td></tr>
				   <cfelse>
                   	<tr><td height=10></td></tr>
				   </cfif>

				   </cfoutput>

				  </table>

            <cfif people.recordcount is 0>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td class="feed_sub_header" style="font-weight: normal;">

                   <cfif session.people_view is 1>
				   		You are not following any People.
				   <cfelseif session.people_view is 2>
				   		No People are following you.
				   <cfelse>
				     No People were found.
				   </cfif>


                  </td></tr>
                </table>

            <cfelse>

	        <cfoutput query="people" startrow="#url.start#" maxrows="#perpage#">

			<div class="profile_badge">
			 <cfinclude template="/exchange/marketplace/people/badge.cfm">
			</div>

	   	   </cfoutput>

	     </cfif>

		 </td></tr>

		 </table>

	  </div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>