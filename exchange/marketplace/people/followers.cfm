<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <center>
	  <div class="main_box">

		  <cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from follow
		   left join usr on usr_id = follow_by_usr_id
		   left join company on company_id = usr_company_id
		   where follow_company_id = #session.company_id#
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">FOLLOWERS <cfif followers.recordcount GT 0>(<cfoutput>#followers.recordcount#</cfoutput>)</cfif></td><td align=right class="feed_option"><a href="/exchange/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

          <cfif followers.recordcount is 0>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header">You have no followers yet.</td></tr>
			</table>

          <cfelse>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td height=20></td></tr>
			</table>

	      <cfloop query="followers">

          <cfif followers.usr_profile_display is 2>

			<div class="follower_badge">

			 <center>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td height=30></td></tr>
	          <tr><td align=center><img src="/images/headshot.png" height=200></td></tr>
	          <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
	         </table>

	         </center>


            </div>


          <cfelse>

			<div class="follower_badge">

            <!--- Start Badge --->

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfoutput>
		   <tr><td width=120 height=140>

			<cfif #followers.usr_photo# is "">
			 <a href="/exchange/member/profile.cfm?i=#followers.follow_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/headshot.png" width=100 border=0></a>
			<cfelse>
			 <a href="/exchange/member/profile.cfm?i=#followers.follow_id#" target="_blank" rel="noopener" rel="noreferrer"><img style="border-radius: 0px;" src="#media_virtual#/#followers.usr_photo#" width=100 border=0></a>
			</cfif>

			</td><td class="feed_title" valign=top>

	        <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td class="feed_title"><a href="/exchange/member/profile.cfm?i=#followers.follow_id#" target="_blank" rel="noopener" rel="noreferrer">#followers.usr_first_name# #followers.usr_last_name#</a></td><td align=right width=30><cfif #followers.usr_linkedin# is not ""><a href="#followers.usr_linkedin#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/linkedin.png" height=20 border=0></a></cfif></td></tr>
            <tr><td class="feed_option" colspan=2><cfif #followers.company_name# is not ""><a href="/exchange/include/company_profile.cfm?id=#followers.company_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#followers.company_name#</a></b></cfif><br><b>#followers.usr_title#</b></td></tr>

            <tr><td class="feed_option" colspan=2><b>#followers.usr_city#, #followers.usr_state#</b></td></tr>
            <tr><td class="feed_option" colspan=2>#followers.usr_email#<br>#followers.usr_phone#</td></tr>

	        </table>

		   </td></tr>

		   <tr><td colspan=2><hr></td></tr>

		   <cfif #followers.company_name# is "">
		     <tr><td colspan=2 class="feed_option"><b>Background / Expertise</b></td></tr>
		     <tr><td colspan=2 class="feed_option" height=100 valign=top><cfif #followers.usr_experience# is "">No information provided.<cfelse>#followers.usr_experience#</cfif></td></tr>
		   <cfelse>
		     <tr><td colspan=2 class="feed_option"><b>Company Information</b></td></tr>
		     <tr><td colspan=2 class="feed_option" height=100 valign=top><cfif #followers.company_about# is "">No information provided.<cfelse><cfif len(followers.company_about) GT 300>#left(followers.company_about,'300')#...<cfelse>#followers.company_about#</cfif></cfif></td></tr>
		   </cfif>

		   <cfset start = #followers.follow_date#>
		   <cfset end = #now()#>

		   <cfset following_days = datediff("d",start,end)>

		   <tr><td colspan=2 class="feed_option">Following<b>&nbsp;&nbsp;#following_days# Days</b></td></tr>

	   </cfoutput>

	</table>

           <!--- End Badge --->

         	</div>

         	</cfif>

		  </cfloop>

		  </cfif>

	  </div>
	  </center>

      </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

