<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Search">

    <cfset search_string = #replace(keyword,chr(34),'',"all")#>
    <cfset search_string = #replace(search_string,'''','',"all")#>
    <cfset search_string = #replace(search_string,')','',"all")#>
    <cfset search_string = #replace(search_string,'(','',"all")#>
    <cfset search_string = #replace(search_string,',','',"all")#>
    <cfset search_string = #replace(search_string,':','',"all")#>
    <cfset search_string = '"' & #search_string#>
    <cfset search_string = #search_string# & '"'>
    <cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
    <cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
	<cfset #session.keyword# = "#search_string#">

	<cfset #session.search_type# = #search_type#>
	<cfif #session.search_type# is 1>
	 <cflocation URL="results.cfm" addtoken="no">
	<cfelse>
	 <cflocation URL="capability.cfm" addtoken="no">
	</cfif>

<cfelse>

 <cfif isdefined("session.add_filter")>

  <cfset session.add_filter = #listappend(session.add_filter,"#add_filter#")#>

 <cfelse>

  <cfset session.add_filter = "#add_filter#">

 </cfif>

 <cflocation URL="capability.cfm" addtoken="no">

</cfif>
