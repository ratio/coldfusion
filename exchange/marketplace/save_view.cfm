<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Create">

	  <cftransaction>

	  <cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert search
		  (
		  search_name,
		  search_description,
		  search_usr_id,
		  search_state_list,
		  search_zip_list,
		  search_naics_list,
		  search_updated
		  )
		  values
		  (
		  '#search_name#',
		  '#search_description#',
		   #session.usr_id#,
		  '#search_state_list#',
		  '#search_zip_list#',
		  '#search_naics_list#',
		   #now()#
		  )
		</cfquery>

	    <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select max(search_id) as id from search
	    </cfquery>

	    <cfset #session.search_id# = #max.id#>

	</cftransaction>

  	   <cflocation URL="/exchange/marketplace/mymarket.cfm?group_id=#max.id#" addtoken="no">

<cfelseif #button# is "Update">

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update search
		  set search_name = '#search_name#',
		      search_description = '#search_description#',
		      search_state_list = '#search_state_list#',
		      search_updated = #now()#,
		      search_zip_list = '#search_zip_list#',
		      search_naics_list = '#search_naics_list#'
		  where search_id = #search_id# and
		        search_usr_id = #session.usr_id#
		</cfquery>

		<cflocation URL="/exchange/marketplace/mymarket.cfm?group_id=#search_id#" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete search
		  where search_id = #search_id# and
		        search_usr_id = #session.usr_id#
		</cfquery>

	    <cflocation URL="index.cfm" addtoken="no">

<cfelseif #button# is "Cancel">

	<cflocation URL="/exchange/marketplace/" addtoken="no">

</cfif>