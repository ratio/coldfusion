<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_owner_id
 where need_hub_id = #session.hub#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>

			<td class="feed_header">NEEDS</td>
			<td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 align=absmiddle hspace=10><a href="add.cfm">Add Need</a></td>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Needs allow you to engage the network to find companies and organizations that may be able to help you solve a need.</td></tr>

	    </tr>

	    <tr><td colspan=2><hr></td></tr>

       </table>

        <cfif agencies.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No needs have been created.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfloop query="agencies">

			   <cfoutput>

			   <tr><td width=100 valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr>
						<td align=center valign=top>

						<cfif agencies.need_image is "">
						  <a href="set.cfm?i=#encrypt(agencies.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/stock_need.png" width=75 border=0 vspace=15></a>
						<cfelse>
						  <a href="set.cfm?i=#encrypt(agencies.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" width=75 border=0 vspace=15><img src="#media_virtual#/#agencies.need_image#" width=100 style="margin-top: 15px;"></a>
						</cfif>

					   </td>

					</tr>

			    </table>

			   </td><td width=30>&nbsp;</td><td valign=top>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" valign=middle><a href="set.cfm?i=#encrypt(agencies.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#ucase(agencies.need_name)#</b></a></td>
					    <td align=right class="feed_sub_header"><a href="set.cfm?i=#encrypt(agencies.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#agencies.need_status#</a></td></tr>
					<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=10>#agencies.need_desc#</td></tr>
					<tr><td class="link_small_gray"><b>Owner</b> : #agencies.usr_first_name# #agencies.usr_last_name#&nbsp;|&nbsp;<b>Keywords</b> : #agencies.need_keywords#</td>
						<td class="link_small_gray" align=right><b>Updated On</b> : #dateformat(agencies.need_updated,'mm/dd/yyyy')#</td></tr>
				   </table>

               </td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>