<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=3" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #session.need_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>

      <td width=185 valign=top>
         <cfinclude template="/exchange/components/my_profile/profile.cfm">
       </td>

      <td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Edit Need</td>
            <td valign=top align=right class="feed_sub_header"><a href="open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

            <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header" width=175>Name / Title</td>
				 <td><input class="input_text" type="text" value="#edit.need_name#" name="need_name" style="width: 600px;" required maxlength="500" placeholder="Name or Title for this need."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Organization</td>
				 <td><input class="input_text" type="text" value="#edit.need_organization#" name="need_organization" style="width: 600px;" maxlength="500" placeholder="Name of organization that is requesting the need."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Abstract</td>
				 <td><textarea class="input_textarea" name="need_desc" style="width: 600px; height: 100px;" required placeholder="Please provide a short narrative of what this need is for.">#edit.need_desc#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Need Description</td>
				 <td><textarea class="input_textarea" name="need_desc_full" style="width: 900px; height: 400px;" required placeholder="Please provide the full description of the need.">#edit.need_desc_full#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="need_keywords" value="#edit.need_keywords#" maxlength="500" style="width: 900px;" required placeholder="Keywords that are related to this Need (seperated by a comma)."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="need_url" value="#edit.need_url#" maxlength="1000" style="width: 900px;" placeholder="Website / URL to find out more information."></td>
		      </tr>

		      </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr><td colspan=4><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Instructions</td>
				 <td><textarea class="input_textarea" name="need_instructions" style="width: 900px; height: 100px;" placeholder="Please provide the instructions for responding to this Need.">#edit.need_instructions#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Allow Questions?</td>
				 <td>

                 <select name="need_commenting" class="input_select" style="width: 100px;">
                  <option value=0>No
                  <option value=1 <cfif #edit.need_commenting# is 1>selected</cfif>>Yes
                 </select>

			     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			      <span class="tooltiptext">Selecting "Yes" will allow Members to submit questions regarding your Need.</span>
			     </div>

				 </td></tr>


			  <tr>
				 <td class="feed_sub_header" width=175>Questions Due By</td>
				 <td><input class="input_date" type="date" name="need_date_questions" value="#edit.need_date_questions#" style="width: 200px;">

				 <span class="link_small_gray">Leave blank if questions are not allowed.</span>

				 </td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" width=175>Response Due By</td>
				 <td><input class="input_date" type="date" value="#edit.need_date_response#" name="need_date_response" style="width: 200px;"></td>
		      </tr>

			 <tr><td class="feed_sub_header">Sharing</td>
				 <td>
				 <select name="need_public" class="input_select">
				  <option value=0>No, keep private
				  <option value=1 <cfif edit.need_public is 1>selected</cfif>>Yes, within this Exchange / Network
				  <option value=2 <cfif edit.need_public is 2>selected</cfif>>Yes, on any Exchange / Network
				 </select>

				 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
				  <span class="tooltiptext">Determines who will see this Need.</span>
				 </div>

				 </td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Status</td>
				 <td>

                 <select name="need_status" class="input_select" style="width: 150px;">
                  <option value="Working">Working
                  <option value="Open" <cfif #edit.need_status# is "Open">selected</cfif>>Open
                  <option value="Closed" <cfif #edit.need_status# is "Closed">selected</cfif>>Closed
                 </select>

			     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			      <span class="tooltiptext" style="width: 300px;"><b>Working</b> - In draft, not ready for prime time.<br><b>Open</b> - Posted and discoverable.<br><b>Closed</b> - Complete.</span>
			     </div>

				 </td></tr>

              <tr><td class="feed_sub_header" valign=top>Display Image</td>
                  <td colspan=3 class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.need_image# is "">
					  <input type="file" name="need_image">
					<cfelse>
					  <img src="#media_virtual#/#edit.need_image#" width=100><br><br>
					  <input type="file" name="need_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					</cfif>

                  </td></tr>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>

              <input type="submit" class="button_blue_large" name="button" value="Update Need">&nbsp;&nbsp;
		      <input class="button_blue_large" type="submit" name="button" value="Delete Need" vspace=10 onclick="return confirm('Delete Need?\r\nDeleting this Need will remove it and any requirements created.  Are you sure you want to continue?');">


              </td></tr>

             </table>

             </cfoutput>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>







