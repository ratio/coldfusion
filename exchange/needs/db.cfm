<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Need">

	<cfif #need_image# is not "">
		<cffile action = "upload"
		 fileField = "need_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

    <cftransaction>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into need
		  (
           need_name,
           need_public,
           need_organization,
           need_desc,
           need_desc_full,
           need_keywords,
           need_url,
           need_instructions,
           need_commenting,
           need_date_questions,
           need_date_response,
           need_created,
           need_updated,
           need_status,
           need_image,
           need_owner_id,
           need_hub_id
		  )
		 values
		 (
         '#need_name#',
          #need_public#,
         '#need_organization#',
         '#need_desc#',
         '#need_desc_full#',
         '#need_keywords#',
         '#need_url#',
         '#need_instructions#',
          #need_commenting#,
          <cfif #need_date_questions# is "">null<cfelse>'#need_date_questions#'</cfif>,
          <cfif #need_date_response# is "">null<cfelse>'#need_date_response#'</cfif>,
          #now()#,
          #now()#,
          '#need_status#',
          <cfif #need_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	      #session.usr_id#,
          #session.hub#

		  )
		</cfquery>

		<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select max(need_id) as id from need
		</cfquery>

    </cftransaction>

    <cfset session.need_id = #max.id#>

	<cflocation URL="open.cfm" addtoken="no">

<cfelseif #button# is "Delete Need">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select need_image from need
		  where (need_id = #session.need_id#)
		</cfquery>

		<cfif remove.need_image is not "">
			<cfif fileexists("#media_path#\#remove.need_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.need_image#">
			</cfif>
		</cfif>

		<cftransaction>
			<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  delete req
			  where req_need_id = #session.need_id#
			</cfquery>
			<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  delete need
			  where need_id = #session.need_id#
			</cfquery>
		</cftransaction>

	<cflocation URL="index.cfm" addtoken="no">

<cfelseif #button# is "Update Need">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select need_image from need
		  where need_id = #session.need_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.need_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.need_image#">
	    </cfif>

	</cfif>

	<cfif #need_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select need_image from need
		  where need_id = #session.need_id#
		</cfquery>

		<cfif #getfile.need_image# is not "">
			<cfif fileexists("#media_path#\#getfile.need_image#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.need_image#">
			</cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "need_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update need
	  set need_name = '#need_name#',

		  <cfif #need_image# is not "">
		   need_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   need_image = null,
		  </cfif>

          need_desc = '#need_desc#',
          need_public = #need_public#,
          need_desc_full = '#need_desc_full#',
          need_instructions = '#need_instructions#',
          need_commenting = #need_commenting#,
          need_organization = '#need_organization#',
          need_status = '#need_status#',
          need_url = '#need_url#',
          need_keywords = '#need_keywords#',
          need_date_questions = <cfif need_date_questions is "">null<cfelse>'#need_date_questions#'</cfif>,
          need_date_response = <cfif need_date_response is "">null<cfelse>'#need_date_response#'</cfif>,
          need_updated = #now()#
      where need_id = #session.need_id#
	</cfquery>

</cfif>

<cflocation URL="open.cfm?u=2" addtoken="no">