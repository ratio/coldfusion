<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Save">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update question
	 set question_answer = '#question_answer#',
	     question_updated = #now()#,
		 question_subject = '#question_subject#',
		 question_text = '#question_text#',
		 question_public = #question_public#
	 where question_id = #question_id#
	</cfquery>

	<cflocation URL="questions.cfm?u=2" addtoken="no">

<cfelseif #button# is "Add Question">

	<cfquery name="add" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      insert into question
      (
       question_answer,
       question_updated,
       question_subject,
       question_text,
       question_public,
       question_usr_id,
       question_date_submitted,
       question_need_id
       )
       values
       (
       '#question_answer#',
        #now()#,
       '#question_subject#',
       '#question_text#',
        #question_public#,
        #session.usr_id#,
        #now()#,
        #session.need_id#
        )
    </cfquery>

	<cflocation URL="questions.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete Question">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete question
	 where question_id = #question_id#
	</cfquery>

	<cflocation URL="questions.cfm?u=3" addtoken="no">

</cfif>

