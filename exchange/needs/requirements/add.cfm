<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">


<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>
       <td width=185 valign=top>
         <cfinclude template="/exchange/components/my_profile/profile.cfm">
       </td>

       <td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Add Requirement</td>
            <td valign=top align=right class="feed_sub_header"><a href="/exchange/needs/open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

        <form action="db.cfm" method="post" enctype="multipart/form-data" >

		<table cellspacing=0 cellpadding=0 border=0 width=95%>

              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" width=100>Number</td>
				 <td><input class="input_text" type="text" name="req_number" style="width: 140px;" maxlength="20" placeholder="Requirement ID"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Name</td>
				 <td><input class="input_text" type="text" name="req_name" style="width: 800px;" placeholder="Requirement Name" required></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="req_desc" style="width: 1000px; height: 200px;" placeholder="Please provide a short description of this requirement."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="req_keywords" style="width: 600px;" placeholder="i.e., Machine Learning, Energy, etc. (seperated by commas)"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Link / URL</td>
				 <td><input class="input_text" type="url" name="req_link" style="width: 600px;"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Start Date</td>
				 <td><input class="input_date" type="date" name="req_start_date" style="width: 175px;"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">End Date</td>
				 <td><input class="input_date" type="date" name="req_end_date" style="width: 175px;"></td>
		      </tr>

              <tr><td class="feed_sub_header" valign=top>Attachment</td>
                  <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="req_attachment"></td></tr>

			  <tr>
				 <td class="feed_sub_header">Estimated Cost</td>
				 <td><input class="input_text" type="number" name="req_est_cost" style="width: 150px;"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Priority</td>
				 <td>

				 <select name="req_priority" class="input_select" style="width: 150px;">
				   <option value=1>Low
				   <option value=2>Medium
				   <option value=3>High
				   <option value=4>Critical
				 </select>

				 </td>
		      </tr>



              <tr><td height=10></td></tr>

              <tr><td colspan=2><hr></td></tr>

              <tr><td></td><td><input type="submit" name="button" class="button_blue_large" value="Add">

		</table>

		</form>




	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>