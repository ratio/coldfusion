<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="need_questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from question
 join usr on usr_id = question_usr_id
 where question_need_id = #session.need_id#
 order by question_date_submitted DESC
</cfquery>

<cfquery name="need_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_owner_id
 where need_id = #session.need_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/needs/need_activity.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Need Questions & Answers</td>
            <td valign=top align=right class="feed_sub_header">

            <img src="/images/plus3.png" alt="Add Question" title="Add Question" width=15 border=0 hspace=10><a href="/exchange/needs/question_add.cfm">Add Question</a>&nbsp;|&nbsp;


            <a href="/exchange/needs/open.cfm">Need Summary</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif need_questions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been asked.</td><tr>
        <cfelse>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">Question has been successfully added.</td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_sub_header" style="color: green;">Question has been successfully updated.</td></tr>
          <cfelseif u is 3>
           <tr><td class="feed_sub_header" style="color: green;">Question has been successfully deleted.</td></tr>
          </cfif>
        </cfif>

        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td valign=top>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif need_info.need_image is "">
				  <img src="/images/stock_need.png" width=75>
				<cfelse>
				  <img src="#media_virtual#/#need_info.need_image#" width=75>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

         <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr>
					<td class="feed_sub_header">Name</td>
					<td class="feed_sub_header" align=center>Questions?</td>
					<td class="feed_sub_header" align=center>Questions Due</td>
					<td class="feed_sub_header" align=center>Responses Due</td>
					<td class="feed_sub_header" align=center>Sharing</td>
					<td class="feed_sub_header" align=right>Status</td>
				 </tr>

				 <tr>
					<td class="feed_sub_header" valign=top style="font-weight: normal;">#need_info.need_name#</td>
					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=100>

					<cfif need_info.need_commenting is 0>
					 No
					<cfelse>
					 Yes
					</cfif>

					</td>
					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=150>

					<cfif need_info.need_date_questions is "">
					 TBD
					<cfelse>
					#dateformat(need_info.need_date_questions,'mmm dd, yyyy')#
					</cfif>

					</td>
					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=150>

					<cfif need_info.need_date_response is "">
					 TBD
					<cfelse>
					#dateformat(need_info.need_date_response,'mmm dd, yyyy')#
					</cfif>

					</td>

					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>

					<cfif need_info.need_public is 1>
					 This Exchange
					<cfelseif need_info.need_public is 2>
					 All Exchanges
					<cfelse>
					 Private
					</cfif>

					</td>

					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=75>#need_info.need_status#</td>

				 </tr>

			  </table>

          </cfoutput>

          </td></tr>

        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td colspan=2><hr></td></tr>

        <cfoutput query="need_questions">

         <tr><td class="feed_sub_header"><b>QUESTION</b></td>
             <td class="feed_sub_header" align=right><img src="/images/icon_edit.png" width=20 hspace=10><a href="question_update.cfm?question_id=#need_questions.question_id#">Update</a></b>
             </tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;"><b>#ucase(need_questions.question_subject)#</b><br>#need_questions.question_text#</td></tr>

         <tr><td class="feed_sub_header" colspan=2><b>ANSWER</b></td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">

         <cfif need_questions.question_answer is "">
          Not Answered.
         <cfelse>
          #need_questions.question_answer#
         </cfif>

         </td></tr>

         <tr><td class="feed_option"><b>Submitted on: </b>#dateformat(need_questions.question_date_submitted,'mm/dd/yyyy')# at #timeformat(need_questions.question_date_submitted)#&nbsp;|&nbsp;<b>By: </b>#need_questions.usr_first_name# #need_questions.usr_last_name#</td>
             <td class="feed_option" align=right>

             <B><cfif need_questions.question_public is "" or need_questions.question_public is 0>PRIVATE<cfelse>DISPLAYED PUBLICLY</cfif></B>

             </td></tr>
         <tr><td colspan=2><hr></td></tr>
        </cfoutput>
        </cfif>

  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>