<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into need_stage (need_stage_name, need_stage_hub_id, need_stage_order, need_stage_image)
	 values ('#need_stage_name#',#session.hub#,#need_stage_order#,'#need_stage_image#')
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update need_stage
	 set need_stage_name = '#need_stage_name#',
	     need_stage_order = #need_stage_order#,
	     need_stage_image = '#need_stage_image#'
	 where need_stage_id = #need_stage_id# and
	       need_stage_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete need_stage
	 where need_stage_id = #need_stage_id# and
	       need_stage_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

