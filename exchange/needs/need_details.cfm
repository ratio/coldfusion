<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="need_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_owner_id
 where need_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_need_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,#session.usr_id#,#session.company_id#,#session.hub#,#now()#)
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       	<cfinclude template="question.cfm">

       </td><td valign=top>

       <div class="main_box">

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Need Summary</td>
            <td class="feed_sub_header" align=right>
              <a href="#cgi.http_referer#">Return</a>
            </td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

       </cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif isdefined("u")>
	    <cfif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Need has been successfully updated.</td></tr>
        <cfelseif u is 10>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Requirement has been successfully added.</td></tr>
        <cfelseif u is 20>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Requirement has been successfully updated.</td></tr>
        <cfelseif u is 30>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Requirement has been successfully deleted.</td></tr>
        </cfif>
	   </cfif>

       <tr><td height=10></td></tr>

       <tr><td valign=middle width=100>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
              <cfoutput>
				<cfif need_info.need_image is "">
				  <img src="/images/stock_need.png" width=75>
				<cfelse>
				  <img src="#media_virtual#/#need_info.need_image#" width=75>
				</cfif>
			  </cfoutput>

			    </td></tr>
            </table>

         </td><td valign=top>

         <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr>
					<td class="feed_sub_header">Name</td>
					<td class="feed_sub_header" align=center>Questions Due</td>
					<td class="feed_sub_header" align=center>Responses Due</td>
					<td class="feed_sub_header" align=right>Status</td>
				 </tr>

				 <tr>
					<td class="feed_sub_header" valign=top style="font-weight: normal;">#need_info.need_name#</td>
					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=150>

					<cfif need_info.need_date_questions is "">
					 TBD
					<cfelse>
					#dateformat(need_info.need_date_questions,'mmm dd, yyyy')#
					</cfif>

					</td>
					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=150>

					<cfif need_info.need_date_response is "">
					 TBD
					<cfelse>
					#dateformat(need_info.need_date_response,'mmm dd, yyyy')#
					</cfif>

					</td>

					<td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=75>#need_info.need_status#</td>

				 </tr>

			  </table>

          </cfoutput>

         </td></tr>

         </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>


         <cfoutput>

         <tr><td colspan=2><hr></td></tr>

         <tr><td class="feed_sub_header" colspan=2>Organization</td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">

         <cfif trim(need_info.need_organization) is "">
          Not provided
         <cfelse>
          #need_info.need_organization#
         </cfif>

         </td></tr>


         <tr><td class="feed_sub_header" colspan=2>Abstract</td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">#need_info.need_desc#</td></tr>

         <tr><td class="feed_sub_header" colspan=2>Need Description</td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">#need_info.need_desc_full#</td></tr>

         <tr><td class="feed_sub_header" colspan=2>Instructions</td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">

         <cfif #need_info.need_instructions# is "">
          Not provided
         <cfelse>
         #need_info.need_instructions#
         </cfif>

         </td></tr>

         <tr>
         <td class="feed_sub_header">Keywords</td>
         <td class="feed_sub_header">Reference URL</td>

         </tr>
         <tr>
         <td class="feed_sub_header" style="font-weight: normal;" width=50%>

         <cfif need_info.need_keywords is "">
          Not provided
         <cfelse>
         #need_info.need_keywords#
         </cfif>

         </td>
         <td class="feed_sub_header" style="font-weight: normal;" width=50%>

         <cfif #need_info.need_url# is "">
          Not provided
         <cfelse>
          <a href="#need_info.need_url#" target"_blank">#need_info.need_url#</a>
         </cfif>

          </td>

         </tr>

         </cfoutput>

         <tr><td colspan=10><hr></td></tr>

       </table>

 	   <cfquery name="req" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select * from req
	     where req_need_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	   </cfquery>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header">Need Requirements</td>
             <td class="feed_sub_header" align=right>

             </td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif req.recordcount is 0>
			   <tr><td class="feed_sub_header" colspan=4 style="font-weight: normal;">No Requirements have been added for this Need.</td></tr>
		    <cfelse>

				<tr>
				 <td class="feed_sub_header">ID / #</td>
				 <td class="feed_sub_header">Name</td>
				 <td class="feed_sub_header">Priority</td>
				 <td class="feed_sub_header" align=center>Start Date</td>
				 <td class="feed_sub_header" align=center>End Date</td>
				</tr>

				<cfset counter = 0>

                       <cfloop query="req">

                       <cfoutput>

						 <cfif counter is 0>
						 <tr bgcolor="FFFFFF">
						 <cfelse>
						 <tr bgcolor="E0E0E0">
						 </cfif>

							<td class="feed_sub_header" valign=top width=75 style="font-weight: normal;"><cfif #req.req_number# is "">TBD<cfelse>#req.req_number#</cfif></td>
							<td class="feed_sub_header" valign=top width=600 style="font-weight: normal;"><b><cfif req.req_name is "">Not Provided<cfelse>#req.req_name#</cfif></b></a></td>
							<td class="feed_sub_header" valign=top width=100 style="font-weight: normal;">

							<cfif #req.req_priority# is 1>
							 Low
							<cfelseif #req.req_priority# is 2>
							 Medium
							<cfelseif #req.req_priority# is 3>
							 High
							<cfelse>
							 Critical
							</cfif>

							</td>

			                <td class="feed_sub_header" valign=top align=center width=125 style="font-weight: normal;"><cfif #req.req_start_date# is "">TBD<cfelse>#dateformat(req.req_start_date,'mm/dd/yyyy')#</cfif></td>
			                <td class="feed_sub_header" valign=top align=center width=125 style="font-weight: normal;"><cfif #req.req_end_date# is "">TBD<cfelse>#dateformat(req.req_end_date,'mm/dd/yyyy')#</cfif></td>

						 </tr>


						 <cfif counter is 0>
						 <tr bgcolor="FFFFFF">
						 <cfelse>
						 <tr bgcolor="E0E0E0">
						 </cfif>

                            <td></td>
							<td class="feed_sub_header" valign=top colspan=5 style="font-weight: normal;"><cfif req.req_desc is "">Description not provided<cfelse>#replace(req.req_desc,"#chr(10)#","<br>","all")#</cfif></td>

                         </tr>

						 <cfif counter is 0>
						  <cfset counter = 1>
						 <cfelse>
						  <cfset counter = 0>
						 </cfif>

                       </cfoutput>

                       </cfloop>

		    </cfif>

		    </table>


		<cfquery name="questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from question
		 where question_public = 1 and
		 question_need_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
         order by question_date_submitted DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td colspan=2><hr></td></tr>
		 <tr>
			 <td class="feed_sub_header">Need Questions & Answers</td>
			 <td class="feed_sub_header" align=right></td></tr>
	   </table>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfif questions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been submitted or posted for this Need.</td></tr>
         <cfelse>

         <cfoutput query="questions">

          <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><b>Q.  #questions.question_subject#</b><br>#questions.question_text#</td>
              <td class="feed_sub_header" valign=top align=right>#dateformat(questions.question_updated,'mm/dd/yyyy')#, #timeformat(questions.question_updated)#</td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>A.  </b><cfif trim(questions.question_answer) is "">No response<cfelse>#questions.question_answer#</cfif></td></tr></tr>
          <tr><td colspan=2><hr></td></tr>

         </cfoutput>

         </cfif>

           </table>

	   </div>

	  </td></tr>

	  </table>









	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>