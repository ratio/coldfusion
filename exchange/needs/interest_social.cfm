<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="need_views" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select count(usr_id) as total, usr_id, usr_first_name, usr_profile_display, usr_phone, usr_email, usr_photo, usr_title, usr_last_name, usr_email from recent
 left join usr on usr_id = recent_usr_id
 where recent_need_id = #session.need_id#
 group by usr_id, usr_first_name, usr_profile_display, usr_phone, usr_email, usr_photo, usr_title, usr_last_name, usr_email
 order by total DESC
</cfquery>

<style>
.small_badge {
    width: 300px;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>
		<td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/needs/need_activity.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">User Views</td>
            <td valign=top align=right class="feed_sub_header"><a href="open.cfm">Need Summary</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

  	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=10></td></tr>
  	     <tr><td valign=top width=48%>

  	      <cfif need_views.recordcount is 0>

  	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
  	        <tr><td class="feed_sub_header" style="font-weight: normal;">No views have been recorded for this Need.</td></tr>
  	        <tr><td height=10></td></tr>
  	      </table>

  	      <cfelse>

			  <cfoutput query="need_views">

			   <cfif need_views.usr_profile_display is 2>

				<div class="smallbadge">

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <tr><td height=30></td></tr>
				  <tr><td align=center><img src="/images/headshot.png" height=120></td></tr>
				  <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
				 </table>

				</div>

			   <cfelse>

				<div class="small_badge">

					<table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif #need_views.usr_photo# is "">
					  <tr><td class="text_xsmall" width=85><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(need_views.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=75 width=75 border=0></td><td width=20>&nbsp;</td>
					 <cfelse>
					  <tr><td class="text_xsmall" width=85><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(need_views.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" src="#media_virtual#/#need_views.usr_photo#" height=75 width=75 border=0><td width=20>&nbsp;</td>
					 </cfif>

					 <td valign=top class="feed_sub_header">

					<table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <tr><td class="feed_sub_header" style="padding-top: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(need_views.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#need_views.usr_first_name# #need_views.usr_last_name#</a></td></tr>
					 <tr><td class="feed_option">#need_views.usr_title#<br></td></tr>
					 <tr><td class="feed_option">#tostring(tobinary(need_views.usr_email))#</td></tr>
					 <tr><td class="feed_option">#need_views.usr_phone#</td></tr>
					</table>

					</td></tr>

					</table>

				</div>

			 </cfif>

			 </cfoutput>

		 </cfif>

  	     </td></tr>

  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>