<cfinclude template="/exchange/security/check.cfm">

<cfset session.lab_department = #lab_department#>
<cfset session.lab_keyword = #trim(lab_keyword)#>

 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into keyword_search
  (
   keyword_search_usr_id,
   keyword_search_company_id,
   keyword_search_hub_id,
   keyword_search_keyword,
   keyword_search_date,
   keyword_search_area
   )
   values
   (
   #session.usr_id#,
   #session.company_id#,
   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  '#trim(lab_keyword)#',
   #now()#,
   51
   )
 </cfquery>

<cflocation URL="results.cfm" addtoken="no">