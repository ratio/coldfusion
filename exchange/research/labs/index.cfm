<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">LABS & LAB TECHNOLOGIES</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

			<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select count(id) as total, department from lab_tech
			 where department is not null
			 group by department
			 order by department
			</cfquery>

          <cfloop query="department">

			<cfquery name="labs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select count(id) as total, federallab from lab_tech
			 where department = '#department.department#'
			 group by federallab
			 order by federallab
			</cfquery>

            <cfoutput>
             <tr><td height=5></td></tr>
             <tr><td class="feed_sub_header" bgcolor="e0e0e0" height=40>#ucase(department.department)#</td>
                 <td class="feed_sub_header" bgcolor="e0e0e0" height=40 align=right>#numberformat(department.total,'99,999')#</td></tr>
             <tr><td height=5></td></tr>
            </cfoutput>

            <cfset counter = 0>

            <cfloop query="labs">

             <cfoutput>


             <cfif counter is 0>
              <tr bgcolor="ffffff">
             <cfelse>
              <tr bgcolor="f0f0f0">
             </cfif>


             <td class="feed_option"><a href="detail_lab.cfm?lab=#federallab#"><b>#ucase(labs.federallab)#</b></a></td>
                 <td class="feed_option" align=right>#numberformat(labs.total,'9,999')#</td></tr>

             </cfoutput>

             <cfif counter is 0>
              <cfset counter = 1>
             <cfelse>
              <cfset counter = 0>
             </cfif>

            </cfloop>

             <tr><td height=10></td></tr>


          </cfloop>

        </table>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>