<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 30>
</cfif>

<cfquery name="state" datasource="#lake_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 order by department_name
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 order by agency_name
</cfquery>

<cfquery name="vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cv
 order by cv_name
</cfquery>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

 select recipient_name, recipient_duns, count(distinct(award_id_piid)) as contracts from award_data
 where contains((award_data.award_description),'#session.partner_keyword#')

       <cfif session.partner_state is not 0>

        <cfif session.partner_location is 1>
         and recipient_state_code = '#session.partner_state#'
        <cfelseif session.partner_location is 2>
         and primary_place_of_performance_state_code = '#session.partner_state#'
        </cfif>

       </cfif>

       <cfif session.partner_dept is not 0>
        and awarding_agency_code = '#session.partner_dept#'
       </cfif>

       <cfif session.partner_agency is not 0>
        and awarding_sub_agency_code = '#session.partner_agency#'
       </cfif>

       <cfif session.partner_vehicle is not 0>
        and parent_award_id like '#session.partner_vehicle#%'
       </cfif>

       <cfif session.partner_socio is not 0>

          <cfif session.partner_socio is "8a">
           and c8a_program_participant = 't'
          <cfelseif session.partner_socio is "A2">
           and woman_owned_business = 't'
          <cfelseif session.partner_socio is "8E">
           and economically_disadvantaged_women_owned_small_business = 't'
          <cfelseif session.partner_socio is "A5">
           and veteran_owned_business = 't'
          <cfelseif session.partner_socio is "QF">
           and service_disabled_veteran_owned_business = 't'
          <cfelseif session.partner_socio is "NB">
           and native_american_owned_business = 't'
          <cfelseif session.partner_socio is "PI">
           and hispanic_american_owned_business = 't'
          <cfelseif session.partner_socio is "OY">
           and black_american_owned_business = 't'
          <cfelseif session.partner_socio is "FR">
           and asian_pacific_american_owned_business = 't'
          <cfelseif session.partner_socio is "23">
           and minority_owned_business = 't'
          <cfelseif session.partner_socio is "AN">
           and alaskan_native_owned_corporation_or_firm = 't'
          </cfif>

       </cfif>

       group by recipient_name, recipient_duns

		<cfif #sv# is 1>
		 order by recipient_name ASC
		<cfelseif #sv# is 10>
		 order by recipient_name DESC
		<cfelseif #sv# is 2>
		 order by recipient_duns ASC
		<cfelseif #sv# is 20>
		 order by recipient_duns DESC
		<cfelseif #sv# is 3>
		 order by contracts ASC
		<cfelseif #sv# is 30>
		 order by contracts DESC
        </cfif>

</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="search.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Partner Finder -

           <cfoutput>

           <cfif companies.recordcount is 1>
           1 Company Found
           <cfelseif companies.recordcount GT 1>
           #companies.recordcount# Companies Found
           <cfelse>
           </cfif>

           </cfoutput>

           </td>
	           <td class="feed_sub_header" style="font-weight: normal;" align=right><a href="index.cfm">Return</a></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">The following Partner(s) were found that match the search criteria you used.</td></tr>
	          <tr><td colspan=2><hr></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif companies.recordcount is 0>
            <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies were found.</td></tr>

           <cfelse>

            <tr><td height=10></td></tr>
            <tr>

              <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
              <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Duns</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
              <td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Contracts</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
              <td></td>
              <td></td>

            </tr>

            <tr><td height=10></td></tr>

	         <cfset counter = 0>

             <cfoutput query="companies">

              <cfif counter is 0>
               <tr bgcolor="ffffff">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>

                 <td class="feed_sub_header"><a href="open.cfm?duns=#recipient_duns#">#recipient_name#</a></td>
                 <td class="feed_sub_header">#recipient_duns#</td>
                 <td class="feed_sub_header" align=center>#contracts#</td>

					<td width=50 align=right>
					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=7>
					  <div class="dropdown-content" style="width: 250px; text-align: left;">
						<a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><i class="fa fa-fw fa-pie-chart"></i>&nbsp;&nbsp;Company Profile</a>
						<a href="save_deal.cfm?duns=#recipient_duns#" onclick="window.open('save_deal.cfm?duns=#recipient_duns#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-dollar"></i>&nbsp;&nbsp;Add to Opportunity</a>
						<a href="save_company.cfm?duns=#recipient_duns#" onclick="window.open('save_company.cfm?duns=#recipient_duns#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
					  </div>
					</div>
					</td>

					<td width=10>&nbsp;</td>

              </tr>

              <cfif counter is 0>
               <cfset counter = 1>
              <cfelse>
               <cfset counter = 0>
              </cfif>

			</cfoutput>

           </cfif>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>