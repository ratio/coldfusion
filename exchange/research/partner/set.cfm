<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(partner_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfset session.partner_keyword = #search_string#>
<cfset session.partner_state = #partner_state#>
<cfset session.partner_dept = #partner_dept#>
<cfset session.partner_agency = #partner_agency#>
<cfset session.partner_location = #partner_location#>
<cfset session.partner_vehicle = #partner_vehicle#>
<cfset session.partner_socio = #partner_socio#>

<cflocation URL="results.cfm" addtoken="no">
