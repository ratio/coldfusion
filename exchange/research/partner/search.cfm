<div class="left_box">

  <center>
  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header">Filter Options</td></tr>
   <tr><td><hr></td></tr>
  </table>

<cfquery name="state" datasource="#lake_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 order by department_name
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 order by agency_name
</cfquery>

<cfquery name="vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cv
 order by cv_name
</cfquery>

          <form action="set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header">Partner Capabilites?</td></tr>
           <cfoutput>
            <tr><td><input type="text" class="input_text" style="width: 215px;" value="#replace(session.partner_keyword,'"','','all')#" required name="partner_keyword" placeholder="example - machine learning or artificial intelligence"></td></tr>
           </cfoutput>
           <tr><td class="feed_sub_header">In what State?</td></tr>
           <tr><td>

                   <select name="partner_state" class="input_select" style="width: 215px;" onchange="form.submit()">
					   <option value=0>Any State
					   <cfoutput query="state">
					   <option value="#state_abbr#" <cfif session.partner_state is state_abbr>selected</cfif>>#state_name#
					   </cfoutput>
                   </select>

          </td></tr>

          <tr><td height=10></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
               <td width=20 style="padding-left: 5px;"><input type="radio" onchange="form.submit();" name="partner_location" value=1 style="width: 15px; height: 15px;" <cfif session.partner_location is 1>checked</cfif>></td>
               <td width=150 class="feed_option">Headquartered</td></tr>
           <tr>
               <td width=20 style="padding-left: 5px;"><input type="radio" onchange="form.submit();" name="partner_location" value=2 style="width: 15px; height: 15px;" <cfif session.partner_location is 2>checked</cfif>></td>
               <td width=150 class="feed_option">Delivering Work</td></tr>

           <tr><td width=20 style="padding-left: 5px;"><input type="radio" onchange="form.submit();" name="partner_location" value=3 style="width: 15px; height: 15px;" <cfif session.partner_location is 3>checked</cfif>></td>
               <td class="feed_option">Either</td>

           </tr>

          </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header">Socio Economic Status?</td></tr>
           <tr><td>
                   <select name="partner_socio" class="input_select" style="width: 215px;" onchange="javascript:document.getElementById('page-loader').style.display='block'; form.submit();">
					 <option value=0>No Status Selected
					 <option value="8a" <cfif session.partner_socio is "8a">selected</cfif>>8a Certified
					 <option value="A2" <cfif session.partner_socio is "A2">selected</cfif>>Woman Owned
					 <option value="8E" <cfif session.partner_socio is "8E">selected</cfif>>Woman Owned - Economically Disadvangated
					 <option value="A5" <cfif session.partner_socio is "A5">selected</cfif>>Veteran Owned
					 <option value="QF" <cfif session.partner_socio is "QF">selected</cfif>>Veteran Owned - Service Disabled
					 <option value="NB" <cfif session.partner_socio is "NB">selected</cfif>>Native American Owned
					 <option value="PI" <cfif session.partner_socio is "PI">selected</cfif>>Hispanic Owned
					 <option value="OY" <cfif session.partner_socio is "OY">selected</cfif>>Black Owned
					 <option value="FR" <cfif session.partner_socio is "FR">selected</cfif>>Asian Pacific Owned
					 <option value="AN" <cfif session.partner_socio is "AN">selected</cfif>>Alaskan Native Owned
					 <option value="23" <cfif session.partner_socio is "23">selected</cfif>>Minority Owned
				   </select>
          </td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header">At a specific Department?</td></tr>
           <tr><td>
                   <select name="partner_dept" class="input_select" style="width: 215px;" onchange="javascript:document.getElementById('page-loader').style.display='block'; form.submit();">
                   <option value=0>Any Department
				    <cfoutput query="department">
				     <option value="#department_code#" <cfif session.partner_dept is #department_code#>selected</cfif>>#department_name#</option>
				    </cfoutput>
				   </select>
          </td></tr>

           <tr><td class="feed_sub_header">At a specific Agency?</td></tr>
           <tr><td>
                   <select name="partner_agency" class="input_select" style="width: 215px;" onchange="javascript:document.getElementById('page-loader').style.display='block'; form.submit();">
                   <option value=0>Any Agency
				    <cfoutput query="agency">
				     <option value="#agency_code#" <cfif session.partner_agency is #agency_code#>selected</cfif>>#agency_name#</option>
				    </cfoutput>
				   </select>
          </td></tr>

          <tr><td height=10></td></tr>

           <tr><td class="feed_sub_header">Contract Vehicle?</td></tr>
           <tr><td>
                   <select name="partner_vehicle" class="input_select" style="width: 215px;" onchange="javascript:document.getElementById('page-loader').style.display='block'; form.submit();">
                   <option value=0>Any Contract Vehicle
                   <cfoutput query="vehicles">
                   <option value="#cv_pattern#" <cfif session.partner_vehicle is cv_pattern>selected</cfif>>#cv_name#
                   </cfoutput>
                   </select>

          </td></tr>

          <tr><td height=10></td></tr>
          <tr><td><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td><input type="submit" name="button" class="button_blue_large" value="Refresh"></td></tr>

          </table>

          </form>























  </center>
</div>

