<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="buying" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_type = 'Buying Pattern' and
        insight_hub_id = #session.hub#
  order by insight_order
</cfquery>

<cfquery name="state" datasource="#lake_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 order by department_name
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 order by agency_name
</cfquery>

<cfquery name="vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cv
 order by cv_name
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Partner Finder</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right><a href="index.cfm">Return</a>
	              </td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Partner Finder allows you to search for companies that you may want to partner with for a specific opportunity.
           To find a potential Partner please use the Partner wizard below.</td></tr>

	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header">What kind of capabilites are you looking for the Partner to assist with?</td></tr>
           <tr><td><input type="text" class="input_text" style="width: 500px;" name="partner_keyword" placeholder="example - machine learning or artificial intelligence"></td></tr>

           <tr><td height=10></td></tr>

           <tr><td class="feed_sub_header">In what State?</td></tr>
           <tr><td>
                   <select name="partner_state" class="input_select">
                   <option value=0>Any State
                   <cfoutput query="state">
                   <option value="#state_abbr#">#state_name#
                   </cfoutput>
                   </select>

          </td></tr>

          <tr><td height=10></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
               <td width=40><input type="radio" name="partner_location" value=1 style="width: 22px; height: 22px;" checked></td>
               <td width=150 class="feed_sub_header">Headquartered</td>

               <td width=40><input type="radio" name="partner_location" value=2 style="width: 22px; height: 22px;"></td>
               <td width=150 class="feed_sub_header">Delivering Work</td>

               <td width=40><input type="radio" name="partner_location" value=3 style="width: 22px; height: 22px;"></td>
               <td class="feed_sub_header">Either</td>

           </tr>


          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header">At a specific Department?</td></tr>
           <tr><td>
                   <select name="partner_dept" class="input_select" style="width: 400px;">
                   <option value=0>Any Departments
				    <cfoutput query="department">
				     <option value="'#department_code#'">#department_name#</option>
				    </cfoutput>
				   </select>
          </td></tr>

          <tr><td height=10></td></tr>

           <tr><td class="feed_sub_header">At a specific Agency?</td></tr>
           <tr><td>
                   <select name="partner_agency" class="input_select" style="width: 400px;">
                   <option value=0>Any Agencies
				    <cfoutput query="agency">
				     <option value="'#agency_code#'">#agency_name#</option>
				    </cfoutput>
				   </select>
          </td></tr>

          <tr><td height=10></td></tr>

           <tr><td class="feed_sub_header">On a specific Contract Vehicle?</td></tr>
           <tr><td>
                   <select name="partner_vehicle" class="input_select">
                   <option value=0>Any Contract Vehicle
                   <cfoutput query="vehicles">
                   <option value="1">#cv_name#
                   </cfoutput>
                   </select>

          </td></tr>



          <tr><td height=10></td></tr>
          <tr><td><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td><input type="submit" name="button" class="button_blue_large" value="Find Potential Partners"></td></tr>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>