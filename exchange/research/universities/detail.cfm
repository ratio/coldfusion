<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header"><cfoutput>#u_name# Technologies</cfoutput></td>
              <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

			<cfquery name="tech" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select id, projecttitle, keywords, organization, description, posteddate, abstract as tech_abstract from autm
			 where organization = '#u_name#'
			 order by posteddate DESC
			</cfquery>

		  <cfset counter = 0>

             <tr>
                <td class="feed_sub_header">INNOVATION</td>
                <td class="feed_sub_header" align=right width=125>DATE POSTED</td>
             </tr>

             <tr><td colspan=2><hr></td></tr>

          <cfloop query="tech">

            <cfoutput>
             <cfif counter is 0>
              <cfset color="FFFFFF">
             <cfelse>
              <cfset color="E0E0E0">
             </cfif>

             <tr bgcolor=#color#><td class="feed_sub_header" valign=top><a href="innovation.cfm?i=#tech.id#&u_name=#u_name#">#ucase(tech.projecttitle)#</a></td>
             <td class="feed_sub_header" align=right valign=top><a href="innovation.cfm?i=#tech.id#&u_name=#u_name#">#dateformat(posteddate,'mm/dd/yyyy')#</a></td>

             </tr>
             <tr bgcolor=#color#><td class="feed_option" colspan=2>
             <cfif #description# is "" or #description# is "None">

             	<cfif tech_abstract is "" or tech_abstract is "None">
             	 No description found.
             	<cfelse>
             	 #tech_abstract#
             	</cfif>

             <cfelse>
              #description#
             </cfif>

             </td></tr>
             <cfif tech.keywords is "" or tech.keywords is "None">
             <cfelse>
             <tr bgcolor=#color#><td class="feed_option" colspan=2><i>#keywords#</i></td></tr>
             </cfif>
             <tr bgcolor=#color#><td class="feed_option" height=10 colspan=2></td></tr>


            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>


            </cfoutput>

             <tr><td height=10></td></tr>

          </cfloop>

        </table>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>