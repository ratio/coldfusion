<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Clear">
 <cfset StructDelete(Session,"u_keyword")>
 <cfset StructDelete(Session,"u_name")>
 <cflocation URL="index.cfm" addtoken="no">>
</cfif>

<cfset search_string = #replace(u_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

<cfset session.u_keyword = "#search_string#">
<cfset session.u_name = #u_name#>

<cflocation URL="results.cfm" addtoken="no">

