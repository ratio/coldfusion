<cfinclude template="/exchange/security/check.cfm">

<cfquery name="inn" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from autm
 where id = #i#
</cfquery>

<cfquery name="ab" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select abstract as inn_abstract from autm
 where id = #i#
</cfquery>

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	   <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">INNOVATION DETAILS</td>
             <td class="feed_sub_header" align=right>

             <cfif not isdefined("r")>

             <cfif u_name is "no">
             <a href="results.cfm">Return</a>
             <cfelse>
             <a href="detail.cfm?u_name=#u_name#">Return</a>
             </cfif>

             </cfif>


             </td>

         </tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=10></td></tr>

         <tr><td class="feed_header">#ucase(inn.projecttitle)#</td></tr>
         <tr><td height=10></td></tr>


         <tr><td class="feed_sub_header">ABSTRACT</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif ab.inn_abstract is "" or ab.inn_abstract is "None">
          No abstract provided.
         <cfelse>
          #ab.inn_abstract#
         </cfif>
         </td></tr>

         <tr><td class="feed_sub_header">SHORT DESCRIPTION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.shortdescription is "" or inn.shortdescription is "None">
          No description provided.
         <cfelse>
          #inn.shortdescription#
         </cfif>
         </td></tr>

         <tr><td class="feed_sub_header">FULL DESCRIPTION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.description is "" or inn.description is "None">
          No description provided.
         <cfelse>
          #inn.description#
         </cfif>
         </td></tr>




         <tr><td class="feed_sub_header">KEYWORDS</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.keywords is "" or inn.keywords is "None">
          No keywords provided.
         <cfelse>
          #inn.keywords#
         </cfif>
         </td></tr>



         <tr><td class="feed_sub_header">WEBSITE</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.websiteaddress is "" or inn.websiteaddress is "None">
          No website provided.
         <cfelse>
          <a href="#inn.websiteaddress#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#inn.websiteaddress#</a>
         </cfif>
         </td></tr>


         <tr><td class="feed_sub_header">UNIVERSITY / ORGANIZATION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.organization is "" or inn.organization is "None">
          No Unversity or organization name provided.
         <cfelse>
          #inn.organization#
         </cfif>
         </td></tr>


         <tr><td><hr></td></tr>

         <tr><td class="feed_sub_header">ADVANTAGES</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.advantages is "" or inn.advantages is "None">
          No advantages applications provided.
         <cfelse>
          #inn.advantages#
         </cfif>
         </td></tr>

         <tr><td class="feed_sub_header">POTENTIAL APPLICATIONS</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.potentialapplications is "" or inn.potentialapplications is "None">
          No potential applications provided.
         <cfelse>
          #inn.potentialapplications#
         </cfif>
         </td></tr>


         <tr><td class="feed_sub_header">ADDITIONAL INFORMATION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif inn.additionalinformation is "" or inn.additionalinformation is "None">
          No additional information provided.
         <cfelse>
          #inn.additionalinformation#
         </cfif>
         </td></tr>


         <tr><td><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td valign=top>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td colspan=2 class="feed_sub_header">PRIMARY CONTACT INFORMATION</td></tr>
                   <tr>
                      <td class="feed_sub_header" width=150>Name</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.contactinformationname# is "">Unknown<cfelse>#inn.contactinformationname#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Title</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.contactinformationtitle# is "">Unknown<cfelse>#inn.contactinformationtitle#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Department</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.contactinformationdepartment# is "">Unknown<cfelse>#inn.contactinformationdepartment#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Email</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.contactinformationemail# is "">Unknown<cfelse>#inn.contactinformationemail#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Phone</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.contactinformationphone# is "">Unknown<cfelse>#inn.contactinformationphone#</cfif></td></tr>

				 </table>

             </td><td width=50>&nbsp;</td><td valign=top>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td colspan=2 class="feed_sub_header">PRINCIPAL INVESTIGATORS (PI)</td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>PI ##1 - Name</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.principalinvestigatorname# is "">Unknown<cfelse>#inn.principalinvestigatorname#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Department</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.principalinvestigatordepartment# is "">Unknown<cfelse>#inn.principalinvestigatordepartment#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>PI ##2 - Name</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.principalinvestigatorname2# is "">Unknown<cfelse>#inn.principalinvestigatorname2#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Department</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.principalinvestigatordepartment2# is "">Unknown<cfelse>#inn.principalinvestigatordepartment2#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>PI ##3 - Name</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.principalinvestigationname3# is "">Unknown<cfelse>#inn.principalinvestigationname3#</cfif></td></tr>

                   <tr>
                      <td class="feed_sub_header" width=150>Department</td>
                      <td class="feed_sub_header" style="font-weight: normal;"><cfif #inn.principalinvestigatordepartment3# is "">Unknown<cfelse>#inn.principalinvestigatordepartment3#</cfif></td></tr>




				 </table>

             </td></tr>

       </table>

       </cfoutput>


	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

