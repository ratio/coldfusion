<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">University Technologies</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

			<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select count(id) as total, organization from autm
			 group by organization
			 order by organization
			</cfquery>

		  <cfset counter = 0>

          <cfloop query="department">

            <cfoutput>
             <cfif counter is 0>
              <tr bgcolor="FFFFFF">
             <cfelse>
              <tr bgcolor="E0E0E0">
             </cfif>

             <td class="feed_sub_header" height=40><cfif department.organization is ""><a href="detail.cfm?u_name=UNKNOWN">UNKNOWN</a><cfelse><a href="detail.cfm?u_name=#organization#">#ucase(department.organization)#</a></cfif></a></td>
             <td class="feed_sub_header" height=40 align=right>#numberformat(department.total,'99,999')#</td>

             </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>


            </cfoutput>

             <tr><td height=10></td></tr>


          </cfloop>

        </table>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>