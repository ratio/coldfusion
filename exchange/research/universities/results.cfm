<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

 <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

	select id, projecttitle, organization, abstract as tech_abstract, keywords, description, posteddate from autm
    where id > 0

      <cfif #session.u_name# is not 0>
       and organization = '#session.u_name#'
      </cfif>

      <cfif #session.u_keyword# is not "">
	  and contains((*),'#trim(session.u_keyword)#')
	  </cfif>

    <cfif sv is 1>
     order by projecttitle ASC
    <cfelseif sv is 10>
     order by projecttitle DESC
    <cfelseif sv is 2>
    <cfelseif sv is 20>
    <cfelseif sv is 3>
     order by organization ASC
    <cfelseif sv is 30>
     order by organization DESC
    <cfelseif sv is 4>
     order by posteddate DESC
    <cfelseif sv is 40>
     order by posteddate ASC
    <cfelseif sv is 5>
    <cfelseif sv is 50>
    <cfelseif sv is 6>
    <cfelseif sv is 60>

    </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>

	 <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
             <td class="feed_header" valign=middle colspan=3>Search Results <cfif #agencies.recordcount# GT 0>(#numberformat(agencies.recordcount,'99,999')#)</cfif></td>
         </tr>
         <tr><td colspan=3><hr></td></tr>
        </cfoutput>
         <tr><td height=10></td></tr>

          <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <tr>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>PROJECT TITLE</b></a><cfif isdefined("sv") and sv is 1>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>UNIVERSITY</b></a><cfif isdefined("sv") and sv is 3>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>DATE POSTED</b></a><cfif isdefined("sv") and sv is 4>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <cfset color="ffffff">
         <cfelse>
          <cfset color="e0e0e0">
         </cfif>

         <tr bgcolor="#color#">

             <td class="feed_sub_header" valign=top><a href="innovation.cfm?i=#id#&u_name=no">#ucase(replaceNoCase(projecttitle,session.u_keyword,"<span style='background:yellow'>#ucase(session.u_keyword)#</span>","all"))#</a></td>
             <td class="feed_sub_header" width=300 valign=top>#ucase(organization)#</td>
             <td class="feed_sub_header" width=150 valign=top align=right>#dateformat(posteddate,'mm/dd/yyyy')#</td></tr>

         <tr bgcolor="#color#"><td class="feed_option" colspan=3>

             <cfif #description# is "" or #description# is "None">

             	<cfif tech_abstract is "" or tech_abstract is "None">
             	 No description found.
             	<cfelse>

                 #ucase(replaceNoCase(tech_abstract,session.u_keyword,"<span style='background:yellow'>#ucase(session.u_keyword)#</span>","all"))#

             	</cfif>

             <cfelse>
                 #ucase(replaceNoCase(description,session.u_keyword,"<span style='background:yellow'>#ucase(session.u_keyword)#</span>","all"))#
             </cfif> </td>


  <!---           #ucase(replaceNoCase(description,session.u_keyword,"<span style='background:yellow'>#ucase(session.u_keyword)#</span>","all"))#</b></a> --->

         </tr>

             <cfif keywords is "" or keywords is "None">
             <cfelse>
             <tr bgcolor=#color#><td class="feed_option" colspan=3><i>Tags - #ucase(replaceNoCase(keywords,session.u_keyword,"<span style='background:yellow'>#ucase(session.u_keyword)#</span>","all"))#</i></td></tr>
             </cfif>
             <tr bgcolor=#color#><td class="feed_option" height=10 colspan=3></td></tr>


         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

        </table>

 	 </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>