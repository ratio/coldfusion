<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

 <cfquery name="lab_info" datasource="#datasource#" username="#username#" password="#password#">
  select * from labs
  where federallab = '#lab#'
 </cfquery>

 <cfquery name="agencies" datasource="#datasource#" username="#username#" password="#password#">

	select lab_tech.id, lab_tech.department, lab_tech.federallab, lab_tech.availabletechnology, lab_tech.shortdescription, lab_tech.fulldescription, labs.imageurl from lab_tech
	join labs on labs.federallab = lab_tech.federallab
    where lab_tech.federallab = '#lab#'

    <cfif sv is 1>
     order by lab_tech.department ASC, lab_tech.federallab ASC
    <cfelseif sv is 10>
     order by lab_tech.department DESC, lab_tech.federallab ASC
    <cfelseif sv is 2>
    <cfelseif sv is 20>
    <cfelseif sv is 3>
    <cfelseif sv is 30>
    <cfelseif sv is 4>
    <cfelseif sv is 40>
    <cfelseif sv is 5>
    <cfelseif sv is 50>
    <cfelseif sv is 6>
    <cfelseif sv is 60>

    </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

<table>
<tr><td>

<cfoutput>
<font size=3>#lab#</font>
</cfoutput>

</td></tr>
</table>


  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=top>

	 <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr><td class="feed_header" valign=middle>Lab Technologies Found <cfif #agencies.recordcount# GT 0>(#trim(numberformat(agencies.recordcount,'99,999'))#)</cfif></td>
             <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
         <tr><td colspan=2><hr></td></tr>
        </cfoutput>

         <tr><td height=10></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" colspan=2>LAB INFORMATION</td></tr>
         <tr><td height=10></td></tr>

         <cfoutput>

          <tr><td width=150>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td>

                <cfif #lab_info.imageurl# is "">
                <img src="/images/app_innovation.png" width=120>
                <cfelse>
                <img src="#lab_info.imageurl#" width=120>
                </cfif>

                </td></tr>
               </table>

              </td><td valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header">#ucase(lab_info.department)#</td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">#ucase(lab_info.federallab)#<br>#ucase(lab_info.addressstreet)#<br>#ucase(lab_info.city)#, #ucase(lab_info.state)#  #ucase(lab_info.zipcode)#</td></tr>
               </table>

              </td><td valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header">REPRESENTATIVE</td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">
                #ucase(lab_info.LaboratoryRepresentative)#
                <br>
                #ucase(lab_info.RepresentativeEmail)#
                <br>
                #lab_info.phone#<br>
                </td></tr>
               </table>

              </td></tr>

         </td></tr>

          <tr><td height=10></td></tr>
		  <tr><td class="feed_sub_header" colspan=2><a href="##" onclick="toggle_visibility('foo');">More Lab Information ...</a></td></tr>
          <tr><td height=10></td></tr>

         </table>

        <div id="foo" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>
       	  <tr><td colspan=3><hr></td></tr>
       	  <tr><td class="feed_option" style="font-weight: normal;" colspan=3><b>DESCRIPTION</b><br>#lab_info.fulldescription#</td></tr>
       	  <tr><td class="feed_option" style="font-weight: normal;" colspan=3><b>MISSION</b><br>#lab_info.mission#</td></tr>
       	  <tr><td class="feed_option" style="font-weight: normal;" colspan=3><b>TECHNOLOGY AREAS</b><br><cfif #lab_info.techareas# is "">Not Provided<cfelse>#lab_info.techareas#</cfif></td></tr>
       	  <tr><td class="feed_option" style="font-weight: normal;" colspan=3><b>SECURITY LAB</b><br><cfif #lab_info.securitylab# is "">UNKNOWN<cfelse>#ucase(lab_info.securitylab)#</cfif></b></td></tr>
         </table>
       </div>

       </cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=10></td></tr>
     	 <tr><td colspan=3><hr></td></tr>
     	 <tr><td height=10></td></tr>

         <cfoutput>
         <tr><td class="feed_header" colspan=2>LAB TECHNOLOGIES (#numberformat(agencies.recordcount,'9,999')#)</td></tr>
         </cfoutput>

          <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=90>
         <cfelse>
          <tr bgcolor="e0e0e0" height=90>
         </cfif>

             <td class="feed_option" style="font-weight: normal;">

             <a href="/exchange/include/labtech_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(availabletechnology)#</b></a>
             <br>
             #shortdescription#

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

        </table>

 	 </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>