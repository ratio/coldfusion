<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

	 <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		<form action="set_name.cfm" method="post">
		<tr><td class="feed_header"><img src="/images/icon_search.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;Name Research</td></tr>
		<tr><td><hr></td></tr>
		<tr><td height=5></td></tr>
		<tr>

		  <td>

		  <cfoutput>

		  <span class="feed_sub_header"><b>First Name</b></span>&nbsp;&nbsp;
		  <input type="text" name="first_name" <cfif isdefined("session.first_name")>value="#session.first_name#"</cfif> class="input_text" style="width: 150px;" placeholder="first name" maxlength="100">

		  <span class="feed_sub_header"><b>Last Name</b></span>&nbsp;&nbsp;
		  <input type="text" name="last_name" <cfif isdefined("session.last_name")>value="#session.last_name#"</cfif> class="input_text" style="width: 150px;" placeholder="last name" required maxlength="100">
		  </cfoutput>
		  &nbsp;&nbsp;
		  <input class="button_blue" type="submit" name="button" value="Search">
		  </td></tr>

		  <tr><td height=5></td></tr>

		  <tr><td colspan=3 class="link_small_gray">Please enter the full first or last name or the first few characters.</td></tr>
		</form>
		</table>
	  </div>

	 <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle>Search Results</td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">Please search above to find names.</td></tr>
        </table>

 	 </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>