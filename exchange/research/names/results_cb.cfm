<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

 <cfquery name="cb_people" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from cb_people
	where cb_people.last_name like '%#session.last_name#%'


    <cfif sv is 1>
     order by last_name ASC, first_name ASC
    <cfelseif sv is 10>
     order by last_name DESC, first_name ASC
    <cfelseif sv is 2>
     order by first_name ASC, last_name ASC
    <cfelseif sv is 20>
     order by first_name DESC, last_name ASC
    <cfelseif sv is 3>
     order by featured_job_title ASC, last_name ASC, first_name ASC
    <cfelseif sv is 30>
     order by featured_job_title DESC, last_name ASC, first_name ASC
    <cfelseif sv is 4>
     order by phone ASC, last_name ASC, first_name ASC
    <cfelseif sv is 40>
     order by phone DESC, last_name ASC, first_name ASC
    <cfelseif sv is 5>
     order by email ASC, last_name ASC, first_name ASC
    <cfelseif sv is 50>
     order by email DESC, last_name ASC, first_name ASC
    <cfelseif sv is 6>
     order by featured_job_organization_name ASC, last_name ASC, first_name ASC
    <cfelseif sv is 60>
     order by featured_job_organization_name DESC, last_name ASC, first_name ASC
    </cfif>










 </cfquery>

 <cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select count(distinct(id)) as total from sbir
	where contact_name like '%#session.last_name#%'
 </cfquery>

 <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select poc_fnme as first_name, poc_lname as last_name, legal_business_name as company, poc_title as title, poc_us_phone as phone, poc_email as email from sams
	where duns is not null
	<cfif session.last_name is not "">and poc_lname like '#session.last_name#%'</cfif>
	<cfif session.first_name is not "">and poc_fnme like '#session.first_name#%'</cfif>
	union
	select alt_poc_fname as first_name, alt_poc_lname as last_name, legal_business_name as company, alt_poc_title as title, akt_poc_phone as phone, alt_poc_email as email from sams
	where duns is not null
	<cfif session.last_name is not "">and alt_poc_lname like '#session.last_name#%'</cfif>
	<cfif session.last_name is not "">and alt_poc_fname like '#session.first_name#%'</cfif>
	union
	select pp_poc_fname as first_name, pp_poc_lname as last_name, legal_business_name as company, pp_poc_title as title, pp_poc_phone as phone, pp_poc_email as email from sams
	where duns is not null
	<cfif session.last_name is not "">and pp_poc_lname like '#session.last_name#%'</cfif>
	<cfif session.last_name is not "">and pp_poc_fname like '#session.first_name#%'</cfif>
	union
	select elec_bus_poc_fname as first_name, elec_bus_poc_lnmae as last_name, legal_business_name as company, elec_bus_poc_title as title, elec_bus_poc_us_phone as phone, elec_bus_poc_email as email from sams
	where duns is not null
	<cfif session.last_name is not "">and elec_bus_poc_lnmae like '#session.last_name#%'</cfif>
	<cfif session.last_name is not "">and elec_bus_poc_fname like '#session.first_name#%'</cfif>

    <cfif sv is 1>
     order by last_name ASC, first_name ASC
    <cfelseif sv is 10>
     order by last_name DESC, first_name ASC

    <cfelseif sv is 2>
     order by first_name ASC, last_name ASC
    <cfelseif sv is 20>
     order by first_name DESC, last_name ASC
    <cfelseif sv is 3>
     order by title ASC, last_name ASC, first_name ASC
    <cfelseif sv is 30>
     order by title DESC, last_name ASC, first_name ASC
    <cfelseif sv is 4>
     order by phone ASC, last_name ASC, first_name ASC
    <cfelseif sv is 40>
     order by phone DESC, last_name ASC, first_name ASC
    <cfelseif sv is 5>
     order by email ASC, last_name ASC, first_name ASC
    <cfelseif sv is 50>
     order by email DESC, last_name ASC, first_name ASC
    <cfelseif sv is 6>
     order by company ASC, last_name ASC, first_name ASC
    <cfelseif sv is 60>
     order by company DESC, last_name ASC, first_name ASC

    </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

	 <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		<form action="set_name.cfm" method="post">
		<tr><td class="feed_header"><img src="/images/icon_search.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;Name Research</td></tr>
		<tr><td><hr></td></tr>
		<tr><td height=5></td></tr>
		<tr>

		  <td>

		  <cfoutput>

		  <span class="feed_sub_header"><b>First Name</b></span>&nbsp;&nbsp;
		  <input type="text" name="first_name" <cfif isdefined("session.first_name")>value="#session.first_name#"</cfif> class="input_text" style="width: 150px;" placeholder="first name" maxlength="100">

		  <span class="feed_sub_header"><b>Last Name</b></span>&nbsp;&nbsp;
		  <input type="text" name="last_name" <cfif isdefined("session.last_name")>value="#session.last_name#"</cfif> class="input_text" style="width: 150px;" placeholder="last name" maxlength="100">
		  </cfoutput>
		  &nbsp;&nbsp;
		  <input class="button_blue" type="submit" name="button" value="Search">
		  </td></tr>

		  <tr><td height=5></td></tr>

		  <tr><td colspan=3 class="link_small_gray">Please enter the full first or last name or the first few characters.</td></tr>
		</form>
		</table>
	  </div>

	 <div class="main_box">

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle colspan=3>
             <a href="results.cfm">SAM.Gov Results (#numberformat(agencies.recordcount,'999,999')#)</a>&nbsp;&nbsp;|&nbsp;&nbsp;
             <a href="results_other.cfm">Other Results (#numberformat(sbir.total,'99,999')#)</a>&nbsp;&nbsp;|&nbsp;&nbsp;
             <a href="results_cb.cfm"><u>Crunchbase Results (#numberformat(cb_people.recordcount,'99,999')#)</u></a>
             </td></tr>
        </table>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        </cfoutput>

         <tr><td height=10></td></tr>

          <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="results_cb.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>LAST NAME</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header"><a href="results_cb.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>FIRST NAME</b></a><cfif isdefined("sv") and sv is 2>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header"><a href="results_cb.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TITLE</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header"><a href="results_cb.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>COMPANY</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="cb_people">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=30>
         <cfelse>
          <tr bgcolor="e0e0e0" height=30>
         </cfif>

             <td class="feed_option" style="font-weight: normal;" width=60><img src="#logo_url#" width=40></td>

             <td class="feed_sub_header" style="font-weight: normal;" width=150>#last_name#</td>
             <td class="feed_sub_header" style="font-weight: normal;" width=150>#first_name#</td>
             <td class="feed_sub_header" style="font-weight: normal;" width=250>#featured_job_title#</td>
             <td class="feed_sub_header" style="font-weight: normal;" width=350>#featured_job_organization_name#</td>

             <td class="feed_sub_header" style="font-weight: normal;" width=125 align=right>

             <cfif facebook_url is not ""><a href="#facebook_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=30 hspace=5></a></cfif>
             <cfif twitter_url is not ""><a href="#twitter_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=30 hspace=5></a></cfif>
             <cfif linkedin_url is not ""><a href="#linkedin_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=30 hspace=5></a></cfif>

             </td>


         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

        </table>














 	 </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>