<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

 <cfquery name="investor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from cb_organizations
	where uuid = '#i#'
 </cfquery>

 <cfquery name="investments" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select cb_organizations.uuid, cb_organizations.logo_url, cb_organizations.name as name, count(cb_funding_rounds.uuid) as rounds, sum(cb_funding_rounds.raised_amount_usd) as funded_amount from cb_investments
    join cb_funding_rounds on cb_funding_rounds.uuid = cb_investments.funding_round_uuid
    join cb_organizations on cb_organizations.uuid = cb_funding_rounds.org_uuid
	where investor_uuid = '#investor.uuid#'
	group by cb_organizations.uuid, cb_organizations.logo_url, cb_organizations.name
	order by funded_amount DESC
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>


	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	 <div class="main_box">

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td valign=top width=115>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td align=center><a href="#investor.cb_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#investor.logo_url#" width=95 border=0></a></td></tr>
		          <tr><td height=10></td></tr>
		          <tr><td align=center>

		          </td></tr>
		        </table>

              </td><td valign=top><td width=20>&nbsp;</td><td valign=top>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>
                   <tr><td class="feed_header" style="padding-bottom: 0px;"><a href="#investor.cb_url#" target="_blank" rel="noopener" rel="noreferrer">#ucase(investor.name)#</a></td>
                       <td class="feed_sub_header" style="padding-bottom: 0px;" align=right>

						   <cfif investor.linkedin_url is not "">
							<a href="#investor.linkedin_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=22></a>
						   </cfif>

						   <cfif investor.facebook_url is not "">
							<a href="#investor.facebook_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=22></a>
						   </cfif>

						   <cfif investor.twitter_url is not "">
							<a href="#investor.twitter_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=22></a>
						   </cfif>

                       </td>

                       </tr>
                   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#investor.short_description#</td></tr>
                   <tr><td class="link_small_gray" style="font-weight: normal;" colspan=2>
                   #investor.region#
                   &nbsp;|&nbsp;
                   #investor.category_groups_list#
                   &nbsp;|&nbsp;
                   Founded in #dateformat(investor.founded_on,'yyyy')#
                   </td></tr>
		        </table>

              </td></tr>

        </table>
        </cfoutput>

 	 </div>

	 <div class="main_box">

	 <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">INVESTMENT PORTFOLIO #investments.recordcount#</td></tr>
        </table>

     </cfoutput>

     <table cellspacing=0 cellpadding=0 border=0 width=100%>


     <cfoutput query="investments">
       <tr>
           <td class="feed_sub_header"><img src="#logo_url#"></td>
           <td class="feed_sub_header">#name#</td>
           <td class="feed_sub_header">#rounds#</td>
           <td class="feed_sub_header" align=right>#numberformat(funded_amount,'$999,999,999,999')#</td>
       </tr>
     </cfoutput>

     </table>

     </div>


    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>