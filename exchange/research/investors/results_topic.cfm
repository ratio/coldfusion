<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 50>
</cfif>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

  select cast(cb_organizations.short_description as varchar(max)) as description, cb_organizations.region, cb_organizations.total_funding_usd, cb_organizations.last_funding_on, cb_organizations.category_groups_list, cb_organizations.name, cb_organizations.uuid, cb_organizations.cb_url, cb_organizations.logo_url, count(cb_funding_rounds.uuid) as investments, sum(cb_funding_rounds.raised_amount_usd) as raised, max(cb_funding_rounds.post_money_valuation_usd) as evaluation from cb_investments
  join cb_funding_rounds on cb_funding_rounds.uuid = cb_investments.funding_round_uuid
  join cb_organizations on cb_organizations.uuid = cb_funding_rounds.org_uuid
  where (cb_organizations.category_groups_list like '#session.invest_keyword#%' or
          cb_organizations.category_list like '#session.invest_keyword#%')
  group by cast(cb_organizations.short_description as varchar(max)), cb_organizations.region, cb_organizations.total_funding_usd, cb_organizations.last_funding_on, cb_organizations.category_groups_list, cb_organizations.name, cb_organizations.uuid, cb_organizations.cb_url, cb_organizations.logo_url

    <cfif sv is 1>
     order by name ASC
    <cfelseif sv is 10>
     order by name DESC
    <cfelseif sv is 2>
     order by category_groups_list ASC
    <cfelseif sv is 20>
     order by category_groups_list DESC
    <cfelseif sv is 3>
     order by category_groups_list ASC
    <cfelseif sv is 30>
     order by category_groups_list DESC
    <cfelseif sv is 4>
     order by region ASC
    <cfelseif sv is 40>
     order by region DESC
    <cfelseif sv is 5>
     order by last_funding_on ASC
    <cfelseif sv is 50>
     order by last_funding_on DESC
    <cfelseif sv is 6>
     order by investments DESC
    <cfelseif sv is 60>
     order by investments ASC
    <cfelseif sv is 7>
     order by evaluation ASC
    <cfelseif sv is 70>
     order by evaluation DESC
    <cfelseif sv is 8>
     order by total_funding_usd ASC
    <cfelseif sv is 80>
     order by total_funding_usd DESC
    </cfif>

</cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(companies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>


	 <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
             <td class="feed_header">Search Results <cfif #companies.recordcount# GT 0>(#numberformat(companies.recordcount,'99,999')#)</cfif></td>

             <td class="feed_sub_header" align=right>
				<cfif companies.recordcount GT #perpage#>
					<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt companies.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>
            </td>

         </tr>
        </cfoutput>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>


          <cfif companies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>COMPANY</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TAGS</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>REGION</b></a><cfif isdefined("sv") and sv is 4>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>LAST FUNDING</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>ROUNDS</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>RAISED</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="results_topic.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>MAX EVAL</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
         </tr>

		 <cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

         <tr>
             <td width=90><a href="/exchange/include/company_profile.cfm?i=#uuid#&cb=y" target="_blank" rel="noopener" rel="noreferrer"><img src="#logo_url#" width=70 border=0 alt="#description#" title="#description#"><a></td>
             <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/include/company_profile.cfm?i=#uuid#&cb=y" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(name)#</b></a></td>
             <td class="feed_sub_header" style="font-weight: normal;">#ucase(category_groups_list)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center>#ucase(region)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=150>#dateformat(last_funding_on,'mmm dd, yyyy')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(investments,'999,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(total_funding_usd,'$999,999,999,,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(evaluation,'$999,999,999,,999')#</td>

         </tr>

         <tr><td colspan=9><hr></td></tr>

         </cfoutput>

         </cfif>

        </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" colspan=2><b>Content Attribution</b></td></tr>

			  <tr><td width=60>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/cb_logo.png" width=40 alt="Crunchbase" title="Crunchbase"></a></td></tr>
				  </table>

				  </td><td>

				  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b>Crunchbase</b></a></td><td align=right class="feed_option"><a href="http://www.crunchbase.com" target="_blank" rel="noopener" rel="noreferrer"><b><u>http://www.crunchbase.com</u></b></a></td></tr>
				   <tr><td class="link_small_gray" colspan=2>Investment and investor information provided by Crunchbase.</td></tr>
				  </table>

				  </td></tr>

			</table>

 	 </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>