<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 50>
</cfif>

 <cfquery name="investor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from cb_investors
  join cb_organizations on cb_organizations.uuid = cb_investors.uuid
  where cb_investors.uuid = '#i#'
 </cfquery>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select cast(cb_organizations.short_description as varchar(max)) as description, cb_organizations.city, cb_organizations.region, cb_organizations.uuid, cb_organizations.name, cb_organizations.logo_url, cb_organizations.cb_url, cb_organizations.category_groups_list,
  sum(cb_funding_rounds.raised_amount_usd) as raised,
  max(cb_funding_rounds.post_money_valuation) as max,
  max(cb_organizations.last_funding_on) as last_funding_on,
  count(cb_investments.uuid) as rounds
  from cb_investments
  join cb_funding_rounds on cb_funding_rounds.uuid = cb_investments.funding_round_uuid
  join cb_organizations on cb_organizations.uuid = cb_funding_rounds.org_uuid
  join company on company.company_cb_id = cb_organizations.uuid
  where cb_investments.investor_uuid = '#i#'
  group by cast(cb_organizations.short_description as varchar(max)), cb_organizations.city, cb_organizations.region, cb_organizations.uuid, cb_organizations.name, cb_organizations.logo_url, cb_organizations.cb_url, cb_organizations.category_groups_list

   <cfif sv is 1>
     order by name ASC
    <cfelseif sv is 10>
     order by name DESC
    <cfelseif sv is 2>
     order by category_groups_list ASC
    <cfelseif sv is 20>
     order by category_groups_list DESC
    <cfelseif sv is 3>
     order by category_groups_list ASC
    <cfelseif sv is 30>
     order by category_groups_list DESC
    <cfelseif sv is 4>
     order by region ASC, city ASC
    <cfelseif sv is 40>
     order by region DESC, city ASC
    <cfelseif sv is 5>
     order by last_funding_on ASC
    <cfelseif sv is 50>
     order by last_funding_on DESC
    <cfelseif sv is 6>
     order by rounds ASC
    <cfelseif sv is 60>
     order by rounds DESC
    <cfelseif sv is 7>
     order by max ASC
    <cfelseif sv is 70>
     order by max DESC
    <cfelseif sv is 8>
     order by raised ASC
    <cfelseif sv is 80>
     order by raised DESC
    </cfif>

</cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(companies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	 <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle>INVESTOR PROFILE</td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=20></td></tr>
        </table>

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td valign=top width=145>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>
                 <tr><td height=10></td></tr>
		         <tr><td align=center><a href="#investor.cb_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#investor.logo_url#" width=110 border=0></a></td></tr>
                 <tr><td height=25></td></tr>
                 <tr><td align=center>
                 <cfif investor.linkedin_url is not "">
                  <a href="#investor.linkedin_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_linkedin.png" width=25 border=0 alt="LinkedIn" title="LinkedIn"></a>
                  </cfif>

                  <cfif investor.twitter_url is not "">
                  <a href="#investor.twitter_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_twitter.png" width=25 border=0 alt="Twitter" title="Twitter"></a>
                  </cfif>

                  <cfif investor.facebook_url is not "">
                  <a href="#investor.facebook_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_facebook.png" width=25 border=0 alt="Facebook" title="Facebook"></a>
                  </cfif>
                  </td></tr>

		        </table>

             </td><td width=20>&nbsp;<td valign=top>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td class="feed_header" style="padding-bottom: 0px;"><a href="#investor.cb_url#" target="_blank" rel="noopener" rel="noreferrer">#ucase(investor.name)#</a></td>
                      <td class="feed_sub_header" style="padding-bottom: 0px;" align=right>Founded: #dateformat(investor.founded_on,'mmmm dd, yyyy')#</td></tr>

                  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 10px;" colspan=2>#investor.short_description#</td></tr>
                  <tr>
                      <td class="feed_sub_header" style="font-weight: normal; padding-top: 5px;">Tags: #investor.category_list#</td></tr>
                  <tr>
                      <td class="feed_sub_header" style="font-weight: normal; padding-top: 5px;">#investor.city#, #investor.region#, #investor.country_code#</td></tr>
                  <tr>
                      <td class="feed_sub_header" style="font-weight: normal; padding-top: 5px;">Phone: #investor.phone#</td></tr>

		        </table>

             </td>
         </tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=3><hr></td></tr>


        </table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr><td height=10></td></tr>
         <tr>
             <td class="feed_header">INVESTMENT PORTFOLIO
             <cfif #companies.recordcount# is 1>
              (1 Company)
             <cfelseif #companies.recordcount# GT 0>
             (#numberformat(companies.recordcount,'99,999')# Companies)
             <cfelseif #companies.recordcount# is 0>
             ( No Companies )
             </cfif></td>

             <td class="feed_sub_header" align=right>
				<cfif companies.recordcount GT #perpage#>
					<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#&i=#i#">
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt companies.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#&i=#i#">
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>
            </td>

         </tr>
        </cfoutput>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif companies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <cfoutput>
         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>COMPANY</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TAGS</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>LOCATION</b></a><cfif isdefined("sv") and sv is 4>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>LAST FUNDING</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>ROUNDS</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>RAISED</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="investor_profile.cfm?i=#i#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>MAX EVAL</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
         </tr>
         </cfoutput>

		 <cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

         <tr>
             <td width=90><a href="/exchange/include/company_profile.cfm?i=#uuid#&cb=y" target="_blank" rel="noopener" rel="noreferrer"><img src="#logo_url#" width=70 border=0 alt="#description#" title="#description#"></a></td>
             <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/include/company_profile.cfm?i=#uuid#&cb=y" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(name)#</b></a></td>
             <td class="feed_sub_header" style="font-weight: normal;">#ucase(category_groups_list)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=250>#ucase(city)#, #ucase(region)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=150>#dateformat(last_funding_on,'mmm dd, yyyy')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(rounds,'999,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(raised,'$999,999,999,,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(max,'$999,999,999,,999')#</td>

         </tr>

         <tr><td colspan=9><hr></td></tr>

         </cfoutput>

         </cfif>

        </table>

        <cfinclude template="cb_attribution.cfm">


 	 </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>