<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

 <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

	select lab_tech.id, lab_tech.department, lab_tech.federallab, lab_tech.availabletechnology, lab_tech.shortdescription, lab_tech.fulldescription, labs.imageurl from lab_tech
	join labs on labs.federallab = lab_tech.federallab
    where lab_tech.id > 0

      <cfif #session.lab_department# is not 0>
       and lab_tech.department = '#session.lab_department#'
      </cfif>

      <cfif #session.lab_keyword# is not "">
	  and contains((lab_tech.availabletechnology, lab_tech.shortdescription, lab_tech.fulldescription),'"#session.lab_keyword#"')
	  </cfif>

    <cfif sv is 1>
     order by lab_tech.department ASC, lab_tech.federallab ASC
    <cfelseif sv is 10>
     order by lab_tech.department DESC, lab_tech.federallab ASC
    <cfelseif sv is 2>
    <cfelseif sv is 20>
    <cfelseif sv is 3>
    <cfelseif sv is 30>
    <cfelseif sv is 4>
    <cfelseif sv is 40>
    <cfelseif sv is 5>
    <cfelseif sv is 50>
    <cfelseif sv is 6>
    <cfelseif sv is 60>

    </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>


	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>


	 <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
             <td class="feed_header" valign=middle colspan=3>Search Results <cfif #agencies.recordcount# GT 0>(#numberformat(agencies.recordcount,'99,999')#)</cfif></td>
         </tr>
        </cfoutput>
         <tr><td height=10></td></tr>

          <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>
          <cfelse>

         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>DEPARTMENT</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TECHNOLOGY</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=90>
         <cfelse>
          <tr bgcolor="e0e0e0" height=90>
         </cfif>

             <td width=100><img src="#imageurl#" width=70></td>
             <td class="feed_option" style="font-weight: normal;" width=300><b>#ucase(department)#</b><br>#federallab#</td>
             <td class="feed_option" style="font-weight: normal;">

             <a href="/exchange/include/labtech_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(replaceNoCase(availabletechnology,session.lab_keyword,"<span style='background:yellow'>#ucase(session.lab_keyword)#</span>","all"))#</b></a>
             <br>
             #replaceNoCase(shortdescription,session.lab_keyword,"<span style='background:yellow'>#ucase(session.lab_keyword)#</span>","all")#

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

        </table>

 	 </div>

    </td><td valign=top>
	  <cfinclude template="/exchange/awards/awards_menu.cfm">
	  </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>