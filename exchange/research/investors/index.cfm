<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

	  <div class="main_box">
	   <cfinclude template="search.cfm">
	  </div>

      <div class="main_box">

	   <cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		select * from recent
		where recent_usr_id = #session.usr_id# and
			  recent_hub_id = #session.hub# and
			  recent_investor_id is not null
		order by recent_date DESC
	   </cfquery>

	   <cfif recent.recordcount is 0>
		<cfset r_list = 0>
	   <cfelse>
		<cfset r_list = valuelist(recent.recent_investor_id)>
	   </cfif>

       <cfset listcount = listlen(r_list)>
       <cfset counter = 1>

	   <cfquery name="list" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select company_id, cast(cb_organizations.short_description as varchar(max)) as description, cb_organizations.city, cb_organizations.region, cb_organizations.uuid, cb_organizations.name, cb_organizations.logo_url, cb_organizations.cb_url, cb_organizations.category_groups_list,
		  sum(cb_funding_rounds.raised_amount_usd) as raised,
		  max(cb_funding_rounds.post_money_valuation) as max,
		  max(cb_organizations.last_funding_on) as last_funding_on,
		  count(cb_investments.uuid) as rounds
		  from cb_investments
		  join cb_funding_rounds on cb_funding_rounds.uuid = cb_investments.funding_round_uuid
		  join cb_organizations on cb_organizations.uuid = cb_funding_rounds.org_uuid
		  join company on company.company_cb_id = cb_organizations.uuid
		  where company_id in (#r_list#)
		  group by company_id, cast(cb_organizations.short_description as varchar(max)), cb_organizations.city, cb_organizations.region, cb_organizations.uuid, cb_organizations.name, cb_organizations.logo_url, cb_organizations.cb_url, cb_organizations.category_groups_list

          <cfif listlen(r_list) GT 0>
           order by case company_id
          <cfloop index="i" list="#r_list#">
           when <cfoutput>#i# then #counter#</cfoutput>
          <cfset counter = counter + 1>
          </cfloop>
          END
          </cfif>

       </cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">RECENTLY VIEWED INVESTMENTS</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif list.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No recently viewed Investments.</td></tr>
          <cfelse>

         <cfoutput>
         <tr>
            <td></td>
            <td class="feed_sub_header">COMPANY</td>
            <td class="feed_sub_header">TAGS</td>
            <td class="feed_sub_header" align=center>LOCATION</td>
            <td class="feed_sub_header" align=center>LAST FUNDING</td>
            <td class="feed_sub_header" align=right>ROUNDS</td>
            <td class="feed_sub_header" align=right>RAISED</td>
            <td class="feed_sub_header" align=right>MAX EVAL</td>
         </tr>
         </cfoutput>

		 <cfoutput query="list">

         <tr>
             <td width=90><a href="/exchange/include/company_profile.cfm?i=#uuid#&cb=y" target="_blank" rel="noopener" rel="noreferrer"><img src="#logo_url#" width=70 border=0 alt="#description#" title="#description#"></a></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=300><a href="/exchange/include/company_profile.cfm?i=#uuid#&cb=y" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(name)#</b></a></td>
             <td class="feed_sub_header" style="font-weight: normal;">#ucase(category_groups_list)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=250>#ucase(city)#, #ucase(region)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=150>#dateformat(last_funding_on,'mmm dd, yyyy')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(rounds,'999,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(raised,'$999,999,999,,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(max,'$999,999,999,,999')#</td>

         </tr>

         <tr><td colspan=9><hr></td></tr>

         </cfoutput>

         </cfif>

        </table>

        <cfinclude template="cb_attribution.cfm">

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>