<cfset app_id = "SBANALYZE">
<cfinclude template="/exchange/security/check.cfm">

<cfset from = #replace(session.eighta_from,'-','',"all")#>
<cfset to = #replace(session.eighta_to,'-','',"all")#>

<cfset search_from = 'A6' & #from#>
<cfset search_to = 'A6' & #to#>

<cfif not isdefined("sv")>
 <cfset sv=2>
</cfif>

<cfset perpage = 100>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select legal_business_name, duns, city_1, state_1, cage_code, sba_bus_type_string
 from sams
 where SBA_Bus_Type_String like 'A6%'
 and sba_bus_type_string between '#search_from#' and '#search_to#'

 <cfif sv is 1>
  order by legal_business_name ASC
 <cfelseif sv is 10>
  order by legal_business_name DESC
 <cfelseif sv is 2>
  order by sba_bus_type_string ASC
 <cfelseif sv is 20>
  order by sba_bus_type_string DESC
 <cfelseif sv is 3>
  order by city_1 ASC
 <cfelseif sv is 30>
  order by city_1 DESC
 <cfelseif sv is 4>
  order by state_1 ASC, city_1 asc
 <cfelseif sv is 40>
  order by state_1 desc, city_1 asc
 <cfelseif sv is 5>
  order by duns asc
 <cfelseif sv is 50>
  order by duns desc
 <cfelseif sv is 6>
  order by cage_code asc
 <cfelseif sv is 60>
  order by cage_code desc
 </cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Socio Economic Analyzer</td>
              <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">Socio Economic Businesses are companies and organizations that meet Small Business Administration (SBA) criteria and qualifications.</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
         <form action="8a_set.cfm" method="post">
            <td class="feed_header">8(a) Certified Businesses</td>

            <td class="feed_sub_header" align=center>

            <cfoutput>
					<cfif agencies.recordcount GT #perpage#>
						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>
            </cfoutput>
            </td>

            <td align=right class="feed_sub_header" style="font-weight: normal;">

            <cfoutput>
            <b>Expiring between:</b>
            &nbsp;&nbsp;

            <input type="date" class="input_date" name="eighta_from" <cfif isdefined("session.eighta_from")>value=#session.eighta_from#</cfif> required style="width: 170px;">

            <b>&nbsp;and&nbsp;</b>

            <input type="date" class="input_date" name="eighta_to" <cfif isdefined("session.eighta_to")>value=#session.eighta_to#</cfif> required style="width: 170px;">
            </cfoutput>
            &nbsp;

            <input type="submit" name="button" value="Search" class="button_blue">
          </form>

          &nbsp;&nbsp;&nbsp;

             <cfoutput>
              <a href="8a_results.cfm?export=1"><img src="#image_virtual#/icon_export_excel.png" width=20 hspace=10></a>
              <a href="8a_results.cfm?export=1">Export to Excel</a>
             </cfoutput>

           </td>

         </tr>

        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No 8(a) Companies were found within your search dates.</td></tr>
         <cfelse>

         <tr>

            <td class="feed_sub_header"><a href="8a_results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="8a_results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>DUNS</b></a>&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="8a_results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Cage Code</b></a>&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="8a_results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Expires</b></a>&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="8a_results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>City</b></a>&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="8a_results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>State</b></a>&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>

         </tr>

         <cfset counter = 0>

		 <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="e0e0e0">
         </cfif>

            <td class="feed_sub_header" width=500><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer">#legal_business_name#</a></td>

            <td class="feed_sub_header" style="font-weight: normal;" width=100>#duns#</td>
            <td class="feed_sub_header" style="font-weight: normal;" width=100>#cage_code#</td>

            <td class="feed_sub_header" style="font-weight: normal;" width=150>

            <cfset #year# = #mid(sba_bus_type_string,3,4)#>
            <cfset #month# = #mid(sba_bus_type_string,7,2)#>
            <cfset #day# = #mid(sba_bus_type_string,9,2)#>

            <cfset expires = #month# & "/" & #day# & "/" & #year#>

            #expires#


            </td>

            <td class="feed_sub_header" style="font-weight: normal;" width=225>#city_1#</td>
            <td class="feed_sub_header" style="font-weight: normal;" align=center width=50>#state_1#</td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>






         </cfif>

        </table>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>