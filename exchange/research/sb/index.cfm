<cfset app_id = "SBANALYZE">
<cfinclude template="/exchange/security/check.cfm">

<cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from totals
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Socio Economic Analyzer</td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">Socio Economic Businesses are companies and organizations that meet Small Business Administration (SBA) criteria and qualifications.</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=10></td></tr>

	     <cfoutput>

	     <tr>
	        <td align=center><a href="8a.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sba_8a.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=A2" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sba_wo.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=8E" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sba_wo_ed.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=A5" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sba_vo.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=QF" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sba_sdvob.png" height=75></a></td>
	     </tr>

	     <tr><td height=20></td></tr>

	     <tr>
	        <td class="feed_header" align=center><a href="8a.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_8a_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=A2" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_wosb_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=8E" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_edwosb_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=A5" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_veteran_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=QF" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_sdvob_companies,'99,999')# Companies</a></td>

	     </tr>

         <tr><td height=25></td></tr>
	     <tr><td colspan=5><hr></td></tr>
         <tr><td height=25></td></tr>

	     <tr>
	        <td align=center><a href="results.cfm?code=NB" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sba_naob.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=PI" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sb_hispanic.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=OY" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sb_black.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=FR" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sb_ap.png" height=75></a></td>
	        <td align=center><a href="results.cfm?code=23" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/sb_minority.png" height=75></a></td>
	     </tr>

	     <tr><td height=20></td></tr>

	     <tr>
	        <td class="feed_header" align=center><a href="results.cfm?code=NB" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_naob_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=PI" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_hispanic_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=OY" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_black_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=FR" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_asian_companies,'99,999')# Companies</a></td>
	        <td class="feed_header" align=center><a href="results.cfm?code=23" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(totals.total_minority_companies,'99,999')# Companies</a></td>

	     </tr>

	     </cfoutput>

        </table>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>