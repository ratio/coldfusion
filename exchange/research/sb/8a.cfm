<cfset app_id = "SBANALYZE">
<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 30>
</cfif>

<cfquery name="eighta" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(duns) as total from sams
  where iseighta = 1
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select recipient_state_code, recipient_state_name, count(distinct(recipient_duns)) as vendors, count(distinct(award_id_piid)) as contracts, sum(federal_action_obligation) as total from award_data
 join sams on duns = recipient_duns
 where action_date between '01/01/2020' and '12/31/2020' and
       recipient_state_code is not null and
       iseighta = 1
 group by recipient_state_code, recipient_state_name

 <cfif sv is 1>
  order by recipient_state_name ASC
 <cfelseif sv is 10>
  order by recipient_state_name DESC
 <cfelseif sv is 2>
  order by vendors ASC
 <cfelseif sv is 20>
  order by vendors DESC
 <cfelseif sv is 3>
  order by contracts ASC
 <cfelseif sv is 30>
  order by contracts DESC
 <cfelseif sv is 4>
  order by total ASC
 <cfelseif sv is 40>
  order by total DESC
 </cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Socio Economic Analyzer</td>
              <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">Socio Economic Businesses are companies and organizations that meet Small Business Administration (SBA) criteria and qualifications.</td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
         <form action="8a_set.cfm" method="post">
            <td class="feed_header">8(a) Certified Businesses</td>
            <td align=right class="feed_sub_header" style="font-weight: normal;">

            <b>Expiring between:</b>
            &nbsp;&nbsp;

            <input type="date" class="input_date" name="eighta_from" required style="width: 170px;">

            <b>&nbsp;and&nbsp;</b>

            <input type="date" class="input_date" name="eighta_to" required style="width: 170px;">

            &nbsp;

            <input type="submit" name="button" value="Search" class="button_blue">
          </form>

             <cfoutput>

             &nbsp;&nbsp;&nbsp;

             <a href="8a.cfm?export=1"><img src="#image_virtual#/icon_export_excel.png" width=20 hspace=10></a>
             <a href="8a.cfm?export=1">Export to Excel</a>
             </cfoutput>

         </td>


         </tr>

        </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif agencies.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No 8(a) Companies were found within your search dates.</td></tr>
         <cfelse>

         <tr>

         <cfoutput>

            <td class="feed_sub_header"><a href="8a.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>State</b></a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>

            <td class="feed_sub_header" align=center><a href="8a.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Companies</b></a>&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="8a.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Contracts</b></a>&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="8a.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>2020 Awards</b></a>&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>

         </cfoutput>

         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="e0e0e0">
         </cfif>

            <td class="feed_sub_header"><a href="8acompanies.cfm?state=#recipient_state_code#">#recipient_state_name#</a></td>
            <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(vendors,'99,999')#</td>
            <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(contracts,'99,999')#</td>
            <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(total,'$999,999,999')#</td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         </cfif>

        </table>

	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>