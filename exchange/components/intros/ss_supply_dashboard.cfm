<style>
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  width: 300px;
  padding-top: 10px;
  padding-bottom: 10px;
  text-align: left;
  box-shadow: 1px 8px 16px 1px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<cfquery name="options" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=5></td></tr>
  <tr><form action="search_set.cfm" method="post">
     <td class="feed_header"><cfoutput>#session.network_name#</cfoutput></td>

     <!---

	  <td class="feed_sub_header" align=right>
		 <div class="tooltip" style="text-decoration-line: underline; text-decoration-style: dashed;"><a href="/exchange/company/strength.cfm" style="text-decoration-line: underline; text-decoration-style: dashed;">Company Profile Strength</a>
		  <span class="tooltiptext">Increase your profile strength to get discovered by buyers and partners.</span>
		 </div>
	  </td>

	  <cfoutput>
	    <td align=right width=120><a href="/exchange/company/strength.cfm"><progress value="#score#" max="#score_total#" style="width: 100px; height: 28px;" alt="#score_percent#%" title="#score_percent#%" align=absmiddle></progress></a></td>
	  </cfoutput>
     --->

     <td class="feed_sub_header" align=right class="feed_sub_header"><a href="#" onclick="HelpHero.startTour('X3WALkUAQov')"><img src="/images/icon_tour.png" width=20 hspace=5 border=0></a><a href="#" onclick="HelpHero.startTour('X3WALkUAQov')">Take a Tour</a></td>

	     </form>
     </tr>
  <tr><td height=10></td></tr>
  <tr><td colspan=4><hr></td></tr>
  <tr><td height=10></td></tr>
  </tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td height=20></td></tr>
 <tr><td width=25% valign=top>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/marketplace/people/">Get Connected</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/marketplace/people/"><img src="/images/network_in.png" height=150 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
         <tr><td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Connect with mentors, alumni, and partners to help you grow and accelerate your business.</td></tr>
	   </table>

   </td><td valign=top width=25%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/opps/">Get Funded</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/opps/"><img src="/images/research.png" height=150 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
         <tr><td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Find philanthropic and award funding, search for opportunities, innovate with non-dillutive capital.</td></tr>
	   </table>

   </td><td valign=top width=25%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/awards/">Engage the Market</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/awards/"><img src="/images/network_out.png" height=150 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
         <tr><td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Search for companies and organizations who are buying your products, analyze competition.</td></tr>
	   </table>

   </td><td valign=top width=25%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/enablement/">Get Support</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/enablement/"><img src="/images/help.png" height=150 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
         <tr><td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Sign up for programs to help your business grow, request support, and track your progress.</td></tr>
	   </table>


 </td></tr>

</table>

