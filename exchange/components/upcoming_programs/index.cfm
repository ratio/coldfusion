<div class="left_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr><td class="feed_header">Upcoming Programs</td>
   <td align=right><img src="/images/icon_search.png" width=20></td></tr>
</tr>
<tr><td colspan=2><hr></td></tr>
<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Upcoming programs that may be of interest to you.</td></tr>
<tr><td height=10></td></tr>

<tr><td>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr>
	  <td width=50 valign=top><img src="/images/ss-pitch.png" width=40 vspace=5></td>
	  <td valign=top>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_option"><b>Federal Contractors</b></td></tr>
		<tr><td class="feed_option">Pitch to companies and organizations that have existing contracts with the Federal Government.</td></tr>
		<tr><td><hr></td></tr>
	      </table>

	  </td>
       </tr>

      </table>


      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr>
	  <td width=50 valign=top><img src="/images/ss-webinar.png" width=40 vspace=5></td>
	  <td valign=top>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_option"><b>Learn to win SBIRs</b></td></tr>
		<tr><td class="feed_option">Learn from the experts on how to respond to a SBIR/STTR soliciation and receive non-dillutive capital.</td></tr>
		<tr><td><hr></td></tr>
	      </table>

	  </td>
       </tr>

      </table>

   </td>
</tr>

</table>

</div>