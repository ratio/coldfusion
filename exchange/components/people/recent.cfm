<div class="left_box">

  <cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="10">
   select distinct(recent_person_id) from recent
   where (recent_usr_id = #session.usr_id# and
          recent_hub_id = #session.hub#) and
          recent_person_id is not null
  </cfquery>

  <cfif recent.recordcount is 0>
   <cfset recent_list = 0>
  <cfelse>
   <cfset recent_list = valuelist(recent.recent_person_id)>
  </cfif>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header" valign=bottom><b>Recently Viewed People</b></td>
	   <td align=right valign=top></td></tr>
   <tr><td><hr></td></tr>
  </table>

  <cfif recent_list is 0>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_sub_header" style="font-weight: normal;">You have not viewed any People.</td></tr>
  </table>

  <cfelse>

  <cfquery name="list" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="5">
   select * from person
   where person_id in (#recent_list#)
  </cfquery>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfset count = 1>

		 <cfoutput query="list">

          <tr bgcolor="ffffff" height=30>
             <td class="feed_option" style="font-weight: normal;" width=40><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#person_photo#" width=35 style="border-radius: 100px;"></a></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=150><a href="/exchange/people/profile.cfm?i=#encrypt(person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#person_full_name#</a>
             <br><span class="link_small_gray">#person_company#</span></td>
         </tr>

         <cfif count LT list.recordcount>
          <tr><td colspan=2><hr></td></tr>
         </cfif>

         <cfset count = count + 1>



         </cfoutput>

  </table>

 </cfif>


</div>