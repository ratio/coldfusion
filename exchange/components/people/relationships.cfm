<cfquery name="type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from person_type
  order by person_type_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr>
		<cfoutput query="type">
		  <td class="feed_sub_header" align=center>#person_type_name#</td>
		</cfoutput>
	</tr>

	<tr>

	<cfloop query="type">

		<cfquery name="rel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select count(person_rel_id) as total from person_rel
		  where person_rel_type_id = #type.person_type_id#
		</cfquery>

		<cfoutput>
			<td class="big_number" align=center style="background-color: 0C314D; font-size: 24px; color: FFFFFF; width: 150px;"><cfif rel.total is 0>0<cfelse><a href="/exchange/components/people/open.cfm?type=#person_type_id#" style="font-size: 24px; color: ffffff;">#numberformat(rel.total,'9,999')#</a></cfif></td>
		</cfoutput>


    </cfloop>

    </tr>

</table>
