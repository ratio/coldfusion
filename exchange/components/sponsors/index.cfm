<cfquery name="page_sponsor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select top(1) * from hub_sponsor
 where hub_sponsor_hub_id = #session.hub# and
	   hub_sponsor_active = 1
 order by newid()
</cfquery>

<cfif page_sponsor.recordcount is 1>
	<cfset sponsor = 1>
</cfif>

<cfoutput>
	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <tr><td height=10></td></tr>
	 <tr>

	     <cfif page_sponsor.hub_sponsor_file is not "">

			 <td valign=middle width=125>
					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					  <tr><td valign=top>

					  <cfif page_sponsor.hub_sponsor_url is "">
						  <img src="#media_virtual#/#page_sponsor.hub_sponsor_file#" width=100 border=0>
					  <cfelse>
						  <a href="#page_sponsor.hub_sponsor_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#page_sponsor.hub_sponsor_file#" width=100 border=0>
					  </cfif>

					  </td></tr>
					 </table>
			 </td>

		 </cfif>

		 <td valign=middle>

			  <cfquery name="getsponsors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select count(hub_sponsor_id) as total from hub_sponsor
			   where hub_sponsor_hub_id = #session.hub# and
					 hub_sponsor_active = 1
			  </cfquery>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_header" valign=middle style="padding-bottom: 0px;">

			  <cfif page_sponsor.hub_sponsor_url is "">
			    #page_sponsor.hub_sponsor_name#
			  <cfelse>
			    <a href="#page_sponsor.hub_sponsor_url#" target="_blank" rel="noopener" rel="noreferrer">#page_sponsor.hub_sponsor_name#</a>
			  </cfif>

			  </td>
			      <td class="feed_sub_header" valign=middle align=right>

			      <cfif #getsponsors.total# GT 1>
			       <a href="/exchange/sponsors/index.cfm">More Sponsors >></a>
			      </cfif>

			      </td></tr>
			  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;" colspan=2>#page_sponsor.hub_sponsor_desc#</td></tr>

			 </table>

		 </td></tr>
	 </table>
 </cfoutput>

<!--- Insert Sponsor View Record --->

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into sponsor_view
 (
 sponsor_view_hub_id,
 sponsor_view_sponsor_id,
 sponsor_view_usr_id,
 sponsor_view_company_id,
 sponsor_view_date,
 sponsor_view_action
 )
 values
 (
 #session.hub#,
 #page_sponsor.hub_sponsor_id#,
 #session.usr_id#,
 #session.company_id#,
 #now()#,
 1
 )
</cfquery>