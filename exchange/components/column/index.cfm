<cfif location is 'right'>
 <div class="right_box">
</cfif>

<cfquery name="columns" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from columns
  left join app on app_id = columns_link_app_id
  where columns_role_id = #usr.hub_xref_role_id# and
        columns_hub_id = #session.hub# and

 		<cfif location is 'left'>
 	       columns_home_page_section_id = #hp_left.home_section_id#
 		<cfelseif location is 'center'>
 	       columns_home_page_section_id = #hp_center.home_section_id#
 		<cfelseif location is 'right'>
 	       columns_home_page_section_id = #hp_right.home_section_id#
 		</cfif>

  order by columns_order
</cfquery>

<cfif columns.recordcount is 0>
 <cfset column_width = 100>
<cfelse>
 <cfset column_width = evaluate(100/columns.recordcount)>
</cfif>

 <cfif columns.recordcount GT 0>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td height=10></td></tr>

 <tr>
 <cfoutput query="columns">

 <td class="feed_header" valign=middle align=center width=#column_width#%>

   <cfif #columns_link_app_id# is 0>
   	<a href="#columns_link#">#columns_name#</a>
   <cfelse>
   	<a href="#app_path#">#columns_name#</a>
   </cfif>

 </td>

 </cfoutput>

 </tr>

 <tr><td height=10></td></tr>

 <tr>
 <cfoutput query="columns">

 <td align=center valign=top>

  <cfif #columns_link_app_id# is 0>
   	<a href="#columns_link#"><img src="#media_virtual#/#columns_image#" border=0 style="height: 100px; width: auto;"></a>
  <cfelse>
   	<a href="#app_path#"><img src="#media_virtual#/#columns_image#" border=0 style="height: 100px; width: auto;"></a>
  </cfif>

</td>
</cfoutput>

</tr>

<tr><td height=10></td></tr>

<tr>
 <cfoutput query="columns">
    <td valign=top class="feed_sub_header" style="font-weight: normal; padding-left: 10px; padding-right: 10px;" align=center>#columns_desc#</td>
 </cfoutput>
</tr>

</table>

<cfelse>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td class="feed_sub_header" style="font-weight: normal;">No content has been created for this section.</td></tr>

 </table>

</cfif>

<cfif location is 'right'>
 </div>
</cfif>