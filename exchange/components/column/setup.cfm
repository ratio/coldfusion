<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<cfquery name="columns" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from columns
  where columns_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        columns_hub_id = #session.hub# and
        columns_home_page_section_id = #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        columns_component_id = #component_id#
  order by columns_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Column Editor - #ucase(profile.hub_role_name)#</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/admin/profiles/home.cfm?i=#i#">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Column has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Column has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Column has been successfully deleted.</td>
        </cfif>
       </cfif>

        <tr><td height=10></td></tr>

		<tr>
		   <td class="feed_header">Columns</td>
		   <td align=right class="feed_sub_header">
		   <a href="add.cfm?i=#i#&component_id=#component_id#&h=#h#"><img src="/images/plus3.png" hspace=10 width=20 border=0></a>
		   <a href="add.cfm?i=#i#&component_id=#component_id#&h=#h#">Add Column</a>
		   </td>
	    </tr>
	    <tr><td height=20></td></tr>

	    </table>

	    </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif columns.recordcount is 0>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No Columns have been added</td></tr>

        <cfelse>

         <tr><td height=10></td></tr>

         <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr>

		<cfset width = #evaluate(100/columns.recordcount)#>

 		<cfoutput query="columns">

 		  <td width=#width#% valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		        <tr><td class="feed_header" align=center><a href="edit.cfm?column_id=#columns_id#&i=#i#&h=#h#&component_id=#component_id#">#columns_name#</a></td></tr>
 		        <tr><td height=20></td></tr>
 		        <tr><td align=center><a href="edit.cfm?column_id=#columns_id#&i=#i#&h=#h#&component_id=#component_id#">

 		        <img src="#media_virtual#/#columns_image#" border=0 style="max-width: 100%; padding-left: 10px; padding-right: 10px;"></a>

 		        </td></tr>

				 <tr><td height=10></td></tr>
			   </table>

 		   </td>

 		</cfoutput>

         </tr>

        <tr>

 		<cfoutput query="columns">

 		  <td width=#width#% valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		         <tr><td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">#columns_desc#</td></tr>
			   </table>

 		   </td>

 		</cfoutput>

         </tr>






        </cfif>

        </table>








	  </div>

	  </td></tr>
	  </table>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>