<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        hub_role_hub_id = #session.hub#
</cfquery>

<cfquery name="columns" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from columns
  where columns_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
        columns_hub_id = #session.hub# and
        columns_component_id = #component_id#
  order by columns_order
</cfquery>

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from app_category
  order by app_category_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Column Editor - #profile.hub_role_name#</td>
	       <td class="feed_sub_header" align=right><a href="#cgi.http_referer#">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>
	   </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Add Column</td></tr>
        <tr><td height=10></td></tr>
       </table>

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=150><b>Title</b></td>
             <td class="feed_option"><input type="text" class="input_text"  style="width: 534px;" name="columns_name" style="width:300px;" required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="columns_desc" class="input_textarea" style="width: 600px; height: 100px;"></textarea></td>
             </td></tr>

		 <tr><td class="feed_sub_header" valign=top>Image</td>
		 	 <td class="feed_sub_header" style="font-weight: normal;">

			  <input type="file" name="columns_image"></td></tr>

         <tr><td></td><td class="link_small_gray">The width of images will resize automatically.  For best results, images should be square (i.e., 120x120, 240x240, etc.)</td></tr>

         <tr><td class="feed_sub_header" width=150><b>Link To App</b></td>
			<td class="feed_sub_header" style="font-weight: normal;">

			<select name="columns_link_app_id" class="input_select" style="width: 300px;">
			<option value=0>Select App
			<cfloop query="cats">

			<cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from app
			  join hub_app on hub_app_app_id = app_id
			  where app_category_id = #cats.app_category_id# and
			  hub_app_hub_id = #session.hub# and
			  app_active = 1
			  order by app_name
			</cfquery>

			<cfif apps.recordcount GT 0>

			<cfoutput>
			 <optgroup label="#app_category_name#">
			</cfoutput>

			<cfoutput query="apps">
			  <option value=#app_id#>&nbsp;&nbsp;#app_name#
			</cfoutput>

			</cfif>

			</cfloop>
			</select>

			<b>Or, enter URL below</b>

			</td>
		 </tr>

         <tr><td class="feed_sub_header" width=150>&nbsp;</td>
             <td class="feed_option"><input type="text" class="input_text"  style="width: 534px;" placeholder="enter URL if not selecting an App" name="columns_link" style="width:800px;"></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Order (left to right)</b></td>
             <td class="feed_option"><input type="number" class="input_text" name="columns_order" style="width:75px;" required></td>
             </td></tr>



          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
          <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add"></td></tr>

       <cfoutput>
       <input type="hidden" name="i" value="#i#">
       <input type="hidden" name="h" value="#h#">
       <input type="hidden" name="component_id" value=#component_id#>
       </cfoutput>


       </form>

      </table>


	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>