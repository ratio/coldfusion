<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select columns_image from columns
		  where columns_id = #column_id#
		</cfquery>

		<cfif fileexists("#media_path#\#remove.columns_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.columns_image#">
		</cfif>

	</cfif>

	<cfif #columns_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select columns_image from columns
		  where columns_id = #column_id#
		</cfquery>

		<cfif #getfile.columns_image# is not "">

		 <cfif fileexists("#media_path#\#getfile.columns_image#")>
		   <cffile action = "delete" file = "#media_path#\#getfile.columns_image#">
		 </cfif>

		</cfif>

		<cffile action = "upload"
		 fileField = "columns_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update columns
      set columns_name = '#columns_name#',
		  columns_desc = '#columns_desc#',

		  <cfif #columns_image# is not "">
		   columns_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   columns_image = null,
		  </cfif>

          columns_order = #columns_order#,
	      columns_link = '#columns_link#',
	      columns_link_app_id = #columns_link_app_id#

      where columns_id = #column_id#
	</cfquery>

	<cflocation URL="setup.cfm?component_id=#component_id#&i=#i#&u=2&h=#h#" addtoken="no">

<cfelseif #button# is "Add">

	<cfif #columns_image# is not "">
		<cffile action = "upload"
		 fileField = "columns_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into columns
	 (
      columns_name,
      columns_home_page_section_id,
      columns_desc,
      columns_image,
      columns_order,
      columns_component_id,
      columns_role_id,
      columns_hub_id,
      columns_link,
      columns_link_app_id
      )
      values
      (
      '#columns_name#',
       #decrypt(h,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
      '#columns_desc#',
       <cfif #columns_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
       #columns_order#,
       #component_id#,
       #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
       #session.hub#,
      '#columns_link#',
       #columns_link_app_id#
      )
	</cfquery>

	<cflocation URL="setup.cfm?component_id=#component_id#&i=#i#&u=1&h=#h#" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select columns_image from columns
	  where columns_id = #column_id#
	</cfquery>

    <cfif remove.columns_image is not "">

     <cfif fileexists("#media_path#\#remove.columns_image#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.columns_image#">
	 </cfif>

	</cfif>

	<cftransaction>

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete columns
		 where columns_id = #column_id#
		</cfquery>

	</cftransaction>

	<cflocation URL="setup.cfm?component_id=#component_id#&i=#i#&u=3&h=#h#" addtoken="no">

</cfif>
