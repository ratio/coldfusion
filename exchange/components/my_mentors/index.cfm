<div class="left_box">

<cfquery name="advisors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from advisor
 join usr on usr_id = advisor_advisor_id
 where advisor_usr_id = #session.usr_id# and
       advisor_hub_id = #session.hub#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header">My Advisors</td>
    <td align=right></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <cfif advisors.recordcount is 0>

	 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">You do not have any Advisors.</td></tr>

 <cfelse>

 <tr><td colspan=2>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <cfset count = 1>

	<cfoutput query="advisors">

	<tr><td height=10></td></tr>

	<tr><td valign=top width=65 valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfif #advisors.usr_photo# is "">
		  <tr><td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" width=60 height=60 border=0 title="Update Profile" alt="Update Profile"></a></td></tr>
		 <cfelse>
		  <tr><td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 0px;" src="#media_virtual#/#advisors.usr_photo#" width=50 height=50 border=0 title="Update Profile" alt="Update Profile"></a></td></tr>
		 </cfif>

	</table>

	</td><td valign=top>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;" valign=top><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#usr_first_name# #usr_last_name#</a></td></tr>
      <tr><td class="feed_option" style="padding-top: 0px; padding-bottom: 0px;">#advisor_role#</td></tr>
      <tr><td class="link_small_gray" style="padding-top: 0px; padding-bottom: 0px;">#usr_company_name#, #usr_title#</td></tr>

    </table>

	</td></tr>

	<cfif count LT #advisors.recordcount#>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=2><hr></td></tr>
	 <cfset count = count + 1>
	</cfif>

	<tr><td height=10></td></tr>

	</cfoutput>

	</table>

</td></tr>

 </cfif>
</table>

</div>
