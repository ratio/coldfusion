<div class="left_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header">Join the Discussion</td>
    <td align=right></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr>
     <td align=center><a href="https://seedspotcommunity.slack.com" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/slack.png" width=140 border=0 alt="Slack" title="Slack"></a></td>
 </tr>
 <tr><td height=10></td></tr>
 <tr><td class="link_small_gray" align=center><a href="https://seedspotcommunity.slack.com" target="_blank" rel="noopener" rel="noreferrer">We're now on Slack.  Click here to join our ecosystem.</a></td></tr>
</table>

</div>
