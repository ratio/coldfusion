<!--- Comm Profile --->

<cfquery name="my_comms" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comm
 join comm_xref on comm_xref_comm_id = comm_id
 where comm_xref_active = 1 and
       comm_xref_usr_id = #session.usr_id# and
       comm_hub_id = #session.hub#
</cfquery>

<div class="left_box">

  <center>
  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header"><b><a href="/exchange/marketplace/communities/">My Communities</a></b></td>
      <td class="feed_header" align=right><a href="/exchange/marketplace/communities/"><img src="/images/icon_search.png" width=20 border=0 alt="Find Community" title="Find Community"></a></td></tr>
  <tr><td colspan=2><hr></td></tr>
  </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <cfset count = 1>

  <cfif #my_comms.recordcount# is 0>
   <tr><td class="feed_sub_header" style="font-weight: normal;">You have not joined any <a href="/exchange/marketplace/communities/"><b>Communities</b></a>.</td></tr>
  <cfelse>

  <cfoutput query="my_comms">

   <tr><td width=40>

		<cfif #my_comms.comm_logo# is "">
				<a href="/exchange/marketplace/communities/s.cfm?i=#encrypt(my_comms.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/stock_community.png" width=30 border=0></a>
		<cfelse>
				<a href="/exchange/marketplace/communities/s.cfm?i=#encrypt(my_comms.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#my_comms.comm_logo#" width=30 border=0></a>
		</cfif>

   </td>
   <td class="link_med_blue"><a href="/exchange/marketplace/communities/s.cfm?i=#encrypt(my_comms.comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#my_comms.comm_name#</a></td></tr>

   <cfif count is not #my_comms.recordcount#>
   <tr><td colspan=2><hr></td></tr>
   </cfif>
   <cfset count = count + 1>

  </cfoutput>

  </cfif>

  </table>
  </center>

</div>