<!--- Company Profile --->

<cfif session.company_id is 0>

	<div class="left_box" style="margin-top: 25px;">
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header"><a href="/exchange/company/">My Company</a></td></tr>
	   <tr><td><hr></td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created or associated yourself with a company.  <a href="/exchange/company/setup/"><b>Start here</b></a>.</td></tr>
	  </table>
    </div>

<cfelse>

	<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
      select * from company
      where company_id = #session.company_id#
    </cfquery>

	<cfquery name="news" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select count(news_id) as total from news
	 where news_company_id = #session.company_id#
	</cfquery>

	<cfquery name="cviews" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select count(cview_id) as total from cview
	 where cview_company_id = #session.company_id# and
	       cview_hub_id = #session.hub#
	</cfquery>

	<cfquery name="products" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select count(product_id) as product_total from product
	  where product_company_id = #session.company_id#
	</cfquery>

	<cfquery name="customers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select count(customer_id) as customer_total from customer
	  where customer_company_id = #session.company_id#
	</cfquery>

	<cfquery name="partners" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select count(partner_id) as total from partner
	  where partner_company_id = #session.company_id#
	</cfquery>

	<cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select count(follow_id) as total from follow
	  where follow_company_id = #session.company_id# and
	        follow_hub_id = #session.hub#
	</cfquery>

	<div class="left_box" style="margin-top: 25px;">

	  <center>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <cfoutput>

	  <cfif #company.company_logo# is "">
	   <tr><td align=center><a href="/exchange/company/"><img src="//logo.clearbit.com/#company.company_website#" width=120 border=0 alt="Company Profile" title="Company Profile" onerror="this.src='/images/no_logo.png'"></a></td></tr>
	  <cfelse>
	   <tr><td align=center><a href="/exchange/company/"><img src="#media_virtual#/#company.company_logo#" width=120 border=0 alt="Company Profile" title="Company Profile"></a></td></tr>
	  </cfif>

	  <tr><td height=10></td></tr>
	  <tr><td class="feed_title" align=center><a href="/exchange/company/"><b>#ucase(company.company_name)#</b></a></td></tr>
	  <tr><td height=5></td></tr>

	  <tr><td><hr></td></tr>

	  <tr><td align=center>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/views.cfm">Company Views</a></td>
		  <td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;" align=center><b>#cviews.total#</b></td></tr>

	  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/followers.cfm">Followers</a></td>
		  <td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;" align=center><b>#followers.total#</b></td></tr>

	  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/company/products/index.cfm?l=3">Products & Services</a></td>
		  <td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;" align=center><b>#products.product_total#</b></td></tr>

	  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/company/customers/index.cfm?l=4">Customer Snapshots</a></td>
		  <td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;" align=center><b>#customers.customer_total#</b></td></tr>

	  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/company/partners/index.cfm?l=5">Partners</a></td>
		  <td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;" align=center><b>#partners.total#</b></td></tr>

	  <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;"><a href="/exchange/company/news/">News & Updates</a></td>
		  <td class="feed_sub_header" style="font-weight: normal; padding-top: 2px; padding-bottom: 2px;" align=center><b>#news.total#</b></td></tr>


		</table>
	  </td></tr>

	   </cfoutput>

	  </table>
	  </center>

	</div>

</cfif>