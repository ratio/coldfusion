<style>
.program_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select top(6) * from program
 join program_cat on program_cat.program_cat_id = program.program_cat_id
 where program_hub_id = #session.hub#
 order by program_date ASC
</cfquery>

<cfif programs.recordcount is 0>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_sub_header" style="font-weight: normal;">No programs or events are available.</td></tr>
</table>

<cfelse>

<cfoutput query="programs">
<br>
<div class="program_badge">

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=10></td></tr>
 <tr><td width=90 valign=top><img src="#media_virtual#/#programs.program_cat_image#" width=75></td>

     <td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header" style="padding-top: 0px;">#programs.program_name#</td></tr>
          <tr><td class="link_small_gray" valign=top><img src="/images/icon_date.png" width=18>&nbsp;&nbsp;#dateformat(programs.program_date,'mmm d, yyyy')#, #programs.program_time#</td></tr>
          <tr><td class="link_small_gray" valign=top><img src="/images/icon_location.png" width=18>&nbsp;&nbsp;<cfif #programs.program_location# is "">Not specified<cfelse>#programs.program_location#</cfif></td></tr>

		</table>

     </td></tr>
 <tr><td height=5></td></tr>
 <tr><td colspan=2 class="feed_option">
      <cfif len(programs.program_desc GT 220)>
       #left(programs.program_desc,220)#...
      <cfelse>
       #programs.program_desc#
      </cfif>
     </td></tr>
 <tr><td height=5></td></tr>

 </table>

</div>

</cfoutput>

<cfif programs.recordcount GT 6>
 <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_sub_header" align=right><a href="/exchange/programs/">See more Programs...</a></td></tr>
 </table>
</cfif>

</cfif>



