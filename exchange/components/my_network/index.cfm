<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="people_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select distinct(usr_follow_usr_follow_id) as usr_id from usr_follow
   join hub_xref on hub_xref_usr_id = usr_follow_usr_follow_id
   join usr on usr_id = usr_follow_usr_follow_id
   where usr_follow_usr_id = #session.usr_id# and
		 hub_xref_active = 1 and
		 usr_follow_hub_id = #session.hub#
 </cfquery>

 <cfif people_list.recordcount is 0>
  <cfset p_list = 0>
 <cfelse>
  <cfset p_list = valuelist(people_list.usr_id)>
 </cfif>

<div class="left_box">

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>
			<tr><td class="feed_header"><a href="/exchange/marketplace/people/set.cfm?v=1">My Network <cfif #people_list.recordcount# GT 0>(#people_list.recordcount#)</cfif></a></td></tr>
			</cfoutput>
            <tr><td colspan=3><hr></td></tr>

            <cfif people_list.recordcount is 0>

             <tr><td class="feed_sub_header" style="font-weight: normal;">You are not following any <a href="/exchange/marketplace/people/"><b>People</b></a>.</td></tr>

            <cfelse>

			 <cfquery name="pics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select top(9) * from usr
			  where usr_id in (#p_list#)
			 </cfquery>

			 <tr><td colspan=3>

			 <cfoutput query="pics">

			     <cfif usr_profile_display is 2>
					  <img style="border-radius: 150px;" src="/images/headshot.png" width=37 height=37 border=0 alt="Private Profile" vspace=5  title="Private Profile">
			     <cfelse>
					 <cfif #usr_photo# is "">
					  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 <cfelse>
					  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 150px;" vspace=5  src="#media_virtual#/#usr_photo#" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 </cfif>
				 </cfif>
			 </cfoutput>
                  <cfif people_list.recordcount GT 1>
	                  <a href="/exchange/marketplace/people/set.cfm?v=1"><img style="border-radius: 150px;" src="/images/all.png" width=35 height=35 vspace=5  border=0 alt="All Members" title="All Members"></a>
                  </cfif>
             </td></tr>

             </cfif>

		</table>

    </td></tr>

</table>

</div>