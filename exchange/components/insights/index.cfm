<div class="left_box">

<cfquery name="insights" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_type = 'Buying Pattern' and
        insight_hub_id = #session.hub# and
        insight_active = 1 and
        insight_display = 1
  order by insight_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header" valign=bottom><a href="/exchange/insights/"><b>Insights</b></a></td>
     <td align=right valign=top></td></tr>
 <tr><td><hr></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfif insights.recordcount is 0>

    <tr><td class="feed_sub_header" style="font-weight: normal;">No Insights have been created.</td></tr>

<cfelse>

	<tr><td height=10></td></tr>

	<cfset count = 1>

	<cfoutput query="insights">

	<tr>

	<cfif #insights.insight_image# is "">
	 <td width=40><a href="/exchange/insights/run_insight.cfm?i=#encrypt(insights.insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="#image_virtual#/insight.png" width=30 border=0 alt="Open" title="Open"></a></td>
	<cfelse>
	 <td width=40><a href="/exchange/insights/run_insight.cfm?i=#encrypt(insights.insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="#media_virtual#/#insight.insight_image#" width=30 border=0 alt="Open" title="Open"></a></td>
	</cfif>

	 <td class="link_med_blue"><a href="/exchange/insights/run_insight.cfm?i=#encrypt(insights.insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><cfif len(insights.insight_name) GT 40>#left(insights.insight_name,'40')#...<cfelse>#insights.insight_name#</cfif></a></td>

	</tr>

    <cfif insights.insight_desc is not "">
     <tr><td></td><td class="link_small_gray">#insights.insight_desc#</td></tr>
    </cfif>

	<cfif count LT insights.recordcount>
	 <tr><td colspan=2><hr></td></tr>
	</cfif>

	<cfset count = count + 1>

	</cfoutput>

</cfif>

</table>

</div>