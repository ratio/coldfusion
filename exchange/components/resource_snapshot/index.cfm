<cfquery name="suggested" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res
 where res_hub_id = #session.hub#
 order by res_id ASC
</cfquery>

<cfquery name="new" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from res
 where res_hub_id = #session.hub#
 order by res_id DESC
</cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td align=middle>

     <img src="/images/ss_resources_1.png" height=100>

	 </td><td width=30>&nbsp;</td><td valign=middle>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <form action="/exchange/res/results.cfm" method="post">

		<tr><td class="feed_header" align=center>Find Resources</td></tr>
		<tr><td height=5></td></tr>
		<tr><td align=center><input type="text" name="keyword" class="input_text" placeholder="Type in a keyword..." style="width: 375px; height: 30px;"></td></tr>
		<tr><td height=10></td></tr>
		<tr><td align=center><input type="submit" name="button" value="Search" class="button_blue_large"></td></tr>
     <input type="hidden" name="cat_type" value=0>
     </form>
	 </table>

	 </td><td width=30>&nbsp;</td><td valign=middle>

     <img src="/images/ss_resources_2.png" height=100>


	 </td></tr>

     <tr><td height=10></td></tr>
     <tr><td colspan=5><hr></td></tr>

 </table>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td valign=top>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=10></td></tr>
  <tr><td class="feed_header" colspan=2>New Resources</td></tr>
  <tr><td height=20></td></tr>

   <cfoutput query="new">
    <tr>

        <td width=50 valign=middle>


          <a href="/exchange/res/open.cfm?i=#encrypt(res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
          <cfif new.res_image is "">
 		  <img src="//logo.clearbit.com/#new.res_domain#" width=30 height=30 border=0 onerror="this.src='/images/no_logo.png'">
 		 <cfelse>
           <img src="#media_virtual#/#new.res_image#" width=30 height=30 border=0>
 		 </cfif>
 		 </a>
       </td>

       <td class="feed_sub_header" style="padding-bottom: 0px; padding-top: 0px;" valign=middle><a href="/exchange/res/open.cfm?i=#encrypt(res_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#new.res_name#</a>
       <span class="link_small_gray" style="font-weight: normal;"><br>#new.res_desc_short#</span>

        </td>
    </tr>
    <tr><td height=10></td></tr>
    <tr><td colspan=2><hr></td></tr>
  </cfoutput>

 </table>

 </td></tr>

 <tr><td colspan=4 class="feed_sub_header" align=right><a href="/exchange/res/">See more Resources ...</a></td></tr>

 </table>
