<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub# and
        snapshot_type_id = 2
  order by snapshot_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header" valign=middle>Topics</td><td align=right valign=middle>

     <span class="feed_sub_header">
	     <cfif opp_snapshot.recordcount LT 5>
		     <a href="/exchange/components/home_page/snapshot_opportunities/add.cfm"><img src="/images/plus3.png" width=15 alt="Add Topic" title="Add Topic" border=0 hspace=5></a>
		     <a href="/exchange/components/home_page/snapshot_opportunities/add.cfm">Add Topic</a>
	     </cfif>
     </span>

     </td></tr>
     <tr><td height=10></td></tr>
</table>

	<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

	  <center>

          <cfif opp_snapshot.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any Opportunity Topics.  <a href="/exchange/components/home_page/snapshot_opportunities/add.cfm"><b>Click here</b></a> to add an Opportunity Topic.</td></tr>
          </table>

          <cfelse>

          <cfset counter = 1>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr>
               <td></td>
			   <td class="feed_sub_header" colspan=5 align=center><img src="/images/icon_box_green.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;Open Opportunities</td>
			   <td class="feed_sub_header" colspan=2 align=center><img src="/images/icon_box_blue.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;Expiring Contracts</td>
			   <td class="feed_sub_header" colspan=2 align=center><img src="/images/icon_box_gray.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;Potential Buyers</td>
			   <td></td>
		    </tr>

			   <tr>

				  <td class="feed_sub_header">&nbsp;Maximum of 5 Topics</td>
				  <td class="feed_sub_header" align=center>Contracts</td>
				  <td class="feed_sub_header" align=center>SBIR/STTRs</td>
				  <td class="feed_sub_header" align=center>Grants</td>
				  <td class="feed_sub_header" align=center>Challenges</td>
				  <td class="feed_sub_header" align=center>Needs</td>
				  <td class="feed_sub_header" align=center>1-12 Mo</td>
				  <td class="feed_sub_header" align=center>1-2 Yrs</td>
				  <td class="feed_sub_header" align=center>Clients</td>
				  <td class="feed_sub_header" align=center>Incumbents</td>
			      <td></td>
			   </tr>

			   <tr><td colspan=11><hr></td></tr>

              <cfloop query="opp_snapshot">

				<cfquery name="needs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
				 select count(need_id) as total from need
				 where need_public = 1 and
				       need_hub_id = #session.hub#
				 and contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
				</cfquery>

				<cfquery name="challenges" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
				 select count(challenge_id) as total from challenge
				 where challenge_public = 1
				 and challenge_end >= '#dateformat(now(),'mm/dd/yyyy')#' and
				     contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
				</cfquery>

				<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
				 select count(need_id) as total from need
				 where need_status = 'Open' and
		         contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
				</cfquery>

				<cfset demand_total = evaluate(needs.total + challenges.total)>

	          <cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

			  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(fbo_solicitation_number)) as total from fbo
			   where contains((fbo_opp_name, fbo_desc),'#trim(opp_snapshot.snapshot_keyword)#')
			   and fbo_inactive_date_updated > '#fb_date#'
			   and (fbo_type not like '%Award%')
			  </cfquery>

			  <cfquery name="awards_next9months" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data
			   where period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and
			         contains((awarding_agency_name, awarding_sub_agency_name, product_or_service_code_description, award_description),'#trim(opp_snapshot.snapshot_keyword)#')
			  </cfquery>

			  <cfquery name="awards_next2" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data
			   where period_of_performance_current_end_date between '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
			         contains((awarding_agency_name, awarding_sub_agency_name, product_or_service_code_description, award_description),'#trim(opp_snapshot.snapshot_keyword)#')
			  </cfquery>

			  <cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(recipient_duns)) as total from award_data
			   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#') and
                      contains((awarding_agency_name, awarding_sub_agency_name, product_or_service_code_description, award_description),'#trim(opp_snapshot.snapshot_keyword)#')
			  </cfquery>

			  <cfquery name="buyers" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(awarding_sub_agency_code)) as total from award_data
			   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#') and
                      contains((awarding_agency_name, awarding_sub_agency_name, product_or_service_code_description, award_description),'#trim(opp_snapshot.snapshot_keyword)#')
			  </cfquery>

			  <!--- SBIR --->

			  <cfquery name="sbir_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as total from opp_sbir
			   where contains((*),'#trim(opp_snapshot.snapshot_keyword)#') and
			   closedate > #now()#
			  </cfquery>

			  <!--- Grants --->

			  <cfquery name="grants_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(opp_grant_id) as total from opp_grant
			   where contains((*),'#trim(opp_snapshot.snapshot_keyword)#') and
			   closedate > #now()#
			  </cfquery>

              <cfoutput>

			   	<tr bgcolor="ffffff">
				  <td class="feed_sub_header" width=20%>&nbsp;<a href="/exchange/components/home_page/snapshot_opportunities/summary.cfm?i=#encrypt(opp_snapshot.snapshot_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#opp_snapshot.snapshot_name#</a>
				  <span class="link_small_gray" style="font-weight: normal;"><br>&nbsp;<i>#replace(opp_snapshot.snapshot_keyword,'"','',"all")#</i></span>
				  </td>

				  <td class="big_number" align=center style="background-color: A1B400; font-size: 24px; color: FFFFFF;"><cfif #fbo.total# is 0>0<cfelse><a style="color: FFFFFF;" href="/exchange/components/home_page/snapshot_opportunities/view10.cfm?i=#opp_snapshot.snapshot_id#&l=2">#fbo.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: A1B400; font-size: 24px; color: FFFFFF;"><cfif #sbir_total.total# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view_sbirs.cfm?i=#opp_snapshot.snapshot_id#&l=2">#sbir_total.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: A1B400; font-size: 24px; color: FFFFFF;"><cfif #grants_total.total# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view_grants.cfm?i=#opp_snapshot.snapshot_id#&l=2">#grants_total.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: A1B400; font-size: 24px; color: FFFFFF;"><cfif #demand_total# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view40.cfm?i=#opp_snapshot.snapshot_id#&l=2">#demand_total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: A1B400; font-size: 24px; color: FFFFFF;"><cfif #needs.total# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view50.cfm?i=#opp_snapshot.snapshot_id#&l=2">#needs.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: 13B0BF; font-size: 24px; color: FFFFFF;"><cfif #awards_next9months.awards# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view20.cfm?i=#opp_snapshot.snapshot_id#&type=3&l=2">#awards_next9months.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: 13B0BF; font-size: 24px; color: FFFFFF;"><cfif #awards_next2.awards# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view20.cfm?i=#opp_snapshot.snapshot_id#&type=5&l=2">#awards_next2.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: 0C314D; font-size: 24px; color: FFFFFF;"><cfif #buyers.total# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view35.cfm?i=#opp_snapshot.snapshot_id#&l=2">#buyers.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: 0C314D; font-size: 24px; color: FFFFFF;"><cfif #vendors.total# is 0>0<cfelse><a style="color: FFFFFF;"href="/exchange/components/home_page/snapshot_opportunities/view30.cfm?i=#opp_snapshot.snapshot_id#&l=2">#vendors.total#</a></cfif></td>
		          <td align=center>

					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=6 hspace=10>
					  <div class="dropdown-content" style="width: 200px; text-align: left; padding-left: 0px;">
						<a href="/exchange/components/home_page/snapshot_opportunities/summary.cfm?i=#encrypt(opp_snapshot.snapshot_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;Summary</a>
						<a href="/exchange/components/home_page/snapshot_opportunities/edit.cfm?i=#encrypt(opp_snapshot.snapshot_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Update Topic</a>
						<a href="/exchange/components/home_page/snapshot_opportunities/delete.cfm?i=#opp_snapshot.snapshot_id#"  onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">&nbsp;<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;Delete Topic</a>
					  </div>
					</div>

		          <!---
		          <a href="/exchange/components/home_page/snapshot_opportunities/delete.cfm?i=#opp_snapshot.snapshot_id#" onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"><img src="/images/delete.png" alt="Delete" title="Delete" border=0 width=15></a>
		          --->

		          </td>

			   </tr>

			   <tr>
				  <td class="feed_option" colspan=6 align=center></td>
				  <td class="feed_option" style="font-weight: bold;" align=center>#ltrim(numberformat(awards_next9months.total,'$999,999,999'))#</td>
				  <td class="feed_option" style="font-weight: bold;" align=center>#ltrim(numberformat(awards_next2.total,'$999,999,999'))#</td>
				  <td colspan=3></td>
			   </tr>

               </cfoutput>

             </cfloop>

	          </table>

             </cfif>

