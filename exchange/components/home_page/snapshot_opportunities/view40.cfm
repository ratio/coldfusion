<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfquery name="cinfo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from challenge
 where challenge_public = 1
 and challenge_end >= '#dateformat(now(),'mm/dd/yyyy')#'
 and contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
</cfquery>

<cfset needs_total = 0>
<cfset challenges_total = 0>

<cfset demand_total = evaluate(cinfo.recordcount)>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">CHALLENGES</a></td></tr>
          <tr><td><hr></td></tr>
          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <cfif isdefined("u")>
				  <cfif u is 1>
				   <tr><td class="feed_sub_header" colspan=3 style="color: green;">The selected Opportunity has been pinned to your board.</td></tr>
				  </cfif>
				 </cfif>

          <tr><td height=10></td></tr>

          <cfif cinfo.recordcount is 0>
           <tr><td class="feed_sub_header">No Challenges were found.</td></tr>
          <cfelse>

           <cfloop query="cinfo">

			   <cfoutput>

               <tr>

				<td valign=top width=170>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td align=center>

				<cfif cinfo.challenge_type_id is 1>

					<cfif cinfo.challenge_image is "">
					  <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/stock_challenge.png" vspace=10 width=125 border=0></a>
					<cfelse>

					 <cfif left(cinfo.challenge_image,4) is "http">

					  <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#cinfo.challenge_image#" vspace=10 width=150 border=0></a>

					 <cfelse>

					  <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="http://www.challenge.gov#cinfo.challenge_image#" vspace=10 width=150 border=0></a>

					 </cfif>

					</cfif>

				<cfelse>

					<cfif cinfo.challenge_image is "">
					  <a href="/exchange/opps/challenge_detail.cfm?challenge_id=#cinfo.challenge_id#"><img src="/images/stock_challenge.png" vspace=10 width=125 border=0></a>
					<cfelse>
					  <a href="/exchange/opps/challenge_detail.cfm?challenge_id=#cinfo.challenge_id#"><img src="#media_virtual#/#cinfo.challenge_image#" vspace=10 width=125 border=0></a>
					</cfif>

				</cfif>

			    </td></tr>

			    <tr><td class="feed_sub_header" align=center>
			     <cfif #cinfo.challenge_end# is "">
			      Open
			     <cfelse>
					 <cfif #cinfo.challenge_end# GTE #dateformat(now(),'mm/dd/yyyy')#>
					  Open
					 <cfelse>
					  Closed
					 </cfif>
			     </cfif>
			     </td></tr>

			    </table>

			    </td>

			    <td width=30>&nbsp;</td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_header">

                <cfif cinfo.challenge_type_id is 1>
	                <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(cinfo.challenge_name)#</b></a>
                <cfelse>
	                <a href="/exchange/opps/challenge_detail.cfm?challenge_id=#cinfo.challenge_id#"><b>#ucase(cinfo.challenge_name)#</b></a>
                </cfif>

                  <td class="feed_sub_header" align=right width=150>

                    Prize:
                    <cfif #cinfo.challenge_total_cash# is not "">

						<cfif cinfo.challenge_currency is "$">$
						<cfelseif cinfo.challenge_currency is "I">INR
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelse>
						</cfif>

                      #numberformat(cinfo.challenge_total_cash,'999,999,999')#
                    <cfelse>
                      Not Provided
                    </cfif>

                  </td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#cinfo.challenge_id#&t=challenge','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>


                  </tr>

                <cfif cinfo.challenge_organization is not "">
                  <tr><td class="feed_sub_header" colspan=2><b><cfif #cinfo.challenge_annoymous# is 1>SPONSOR: PRIVATE ORGANIZATION	<cfelse>SPONSOR: #ucase(cinfo.challenge_organization)#</cfif></b></td></tr>
                </cfif>

                </td>

                <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#cinfo.challenge_desc#</td></tr>

                <cfif cinfo.challenge_reward is not "">
	                <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>Reward - </b>#cinfo.challenge_reward#</td></tr>
                </cfif>

                <tr><td class="link_small_gray"><b>Keywords&nbsp;: </b><cfif cinfo.challenge_keywords is "">Not Defined<cfelse>#lcase(cinfo.challenge_keywords)#</cfif>&nbsp;&nbsp;|&nbsp;&nbsp;<b>Close Date&nbsp;:&nbsp;</b><cfif cinfo.challenge_end is "">Unknown<cfelse>#dateformat(cinfo.challenge_end,'mmm dd, yyyy')#</cfif></td></tr>
		        </table>

		        </td></tr>

             <tr><td height=10></td></tr>
             <tr><td colspan=7><hr></td></tr>
             <tr><td height=10></td></tr>

		   </cfoutput>

          </cfloop>

          </cfif>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>