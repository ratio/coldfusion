<cfinclude template="/exchange/security/check.cfm">

	<cfset search_string = #replace(snapshot_keyword,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,')','',"all")#>
	<cfset search_string = #replace(search_string,'(','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

<cfif button is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	insert into snapshot
	(
	 snapshot_name,
	 snapshot_keyword,
	 snapshot_usr_id,
	 snapshot_hub_id,
	 snapshot_type_id
	 )
	 values
	 (
	 '#snapshot_name#',
	 '#search_string#',
	  #session.usr_id#,
	  #session.hub#,
	  2
	  )
	</cfquery>

    <cflocation URL="/exchange/index.cfm" addtoken="no">>

<cfelseif button is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	update snapshot
	set snapshot_keyword = '#search_string#',
	    snapshot_name = '#snapshot_name#'
	where snapshot_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="/exchange/index.cfm" addtoken="no">>

</cfif>