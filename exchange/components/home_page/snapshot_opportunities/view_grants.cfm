<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.grant_recent")>
 <cfset #session.grant_recent# = 1>
</cfif>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<cfif not isdefined("session.grant_status")>
 <cfset session.grant_status = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<!--- Lookups --->

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select opp_grant_id, opportunitytitle, department, agencyname, opportunitynumber, fundinginstrumenttype, awardfloor, awardceiling, closedate, orgname_logo from opp_grant
 left join orgname on orgname_name = department
 where closedate > #now()#
 and contains((opportunitytitle, department, description, agencyname),'#trim(opp_snapshot.snapshot_keyword)#')

	<cfif #sv# is 1>
	 order by department ASC, agencyname ASC
	<cfelseif #sv# is 10>
	 order by department DESC, agencyname ASC
	<cfelseif #sv# is 2>
	 order by opportunitynumber ASC
	<cfelseif #sv# is 20>
     order by opportunitynumber DESC
	<cfelseif #sv# is 3>
	 order by opportunitytitle ASC
	<cfelseif #sv# is 30>
	 order by opportunitytitle DESC
	<cfelseif #sv# is 4>
	 order by fundinginstrumenttype ASC
	<cfelseif #sv# is 40>
	 order by fundinginstrumenttype DESC
	<cfelseif #sv# is 5>
	 order by awardfloor ASC
	<cfelseif #sv# is 50>
	 order by awardfloor DESC
	<cfelseif #sv# is 6>
	 order by awardceiling ASC
	<cfelseif #sv# is 60>
	 order by awardceiling DESC
	<cfelseif #sv# is 7>
	 order by closedate ASC
	<cfelseif #sv# is 70>
	 order by closedate DESC
	</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="grant_refresh.cfm" method="post">
         <cfoutput>
			 <tr><td class="feed_header">Grants (#ltrim(numberformat(agencies.recordcount,'99,999'))#)</td>

				 <td class="feed_sub_header" align=right width=300>
				 <a href="view_grants.cfm?export=1&sv=#sv#&i=#i#"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" hspace=10 title="Export to Excel"></a>
				 <a href="view_grants.cfm?export=1&sv=#sv#&i=#i#">Export to Excel</a>

				</td></tr>
				</cfoutput>
            </form>

         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr height=50>
            <td></td>
            <td class="feed_sub_header"><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Title</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header""><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Organization</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header""><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Opportunity ##</b></a><cfif isdefined("sv") and sv is 2>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"" align=center><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Funding Type</b></a><cfif isdefined("sv") and sv is 4>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"" align=right><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Award Floor</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"" align=right><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Award Ceiling</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"" align=right><a href="view_grants.cfm?i=#i#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Close Date</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td>&nbsp;</td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

			<td width=70>
			<a href="/exchange/include/grant_new_information.cfm?id=#opp_grant_id#" vspace=15 target="_blank" rel="noopener" rel="noreferrer">
            <cfif #orgname_logo# is "">
              <img src="#image_virtual#/icon_usa.png" width=40 border=0>
            <cfelse>
              <img src="#image_virtual#/#orgname_logo#" width=40 vspace=15 border=0>
            </cfif>
            </a>
            </td>

             <td class="feed_sub_header" width=450><b><a href="/exchange/include/grant_new_information.cfm?id=#opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer">#opportunitytitle#</a></b></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=400>#ucase(department)#<br>#agencyname#</td>
             <td class="feed_sub_header" style="font-weight: normal;" width=120>#opportunitynumber#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=75>#fundinginstrumenttype#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=75>#numberformat(awardfloor,'$999,999,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=75>#numberformat(awardceiling,'$999,999,999')#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right>#dateformat(closedate,'mm/dd/yyyy')#</td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#opp_grant_id#&t=grant','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>


         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

        </table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>