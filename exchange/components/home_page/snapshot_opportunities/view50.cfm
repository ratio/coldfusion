<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_public = 1 and
       need_status = 'Open' and
       contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Needs</a></td></tr>
          <tr><td><hr></td></tr>
          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td height=10></td></tr>

          <cfif needs.recordcount is 0>
           <tr><td class="feed_sub_header">No Needs were found.</td></tr>
          <cfelse>

        <cfloop query="needs">

         <cfoutput>

          <tr bgcolor="ffffff" height=70>


			<td width=150 valign=top>

			<cfif needs.need_image is "">
			  <a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#stock_need.png" style="margin-top: 15px;" width=125 border=0></a>
			<cfelse>
			  <a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#needs.need_image#" style="margin-top: 15px;" width=125></a>
			</cfif>
            </td>

            <td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>

             <td class="feed_sub_header" width=450><b><a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#needs.need_name#</a></b></td>
                 <td align=right width=40>
			     </td>
			</tr>

			</table>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#needs.need_desc#</td></tr>
			<tr><td class="link_small_gray"><b>Keywords - </b><i>#needs.need_keywords#</i></b></td>
			    <td class="feed_sub_header" align=right>Response Due -

			    <cfif #needs.need_date_response# is "">
			     Not provided
			    <cfelse>
			     #dateformat(needs.need_date_response,'mmm dd, yyyy')#
			    </cfif>


			    </td></tr>
			</table>

			</td>

		   </cfoutput>

         </tr>

         </cfloop>







          </cfif>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>