<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<!--- Lookups --->

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_sbir
 left join orgname on orgname_name = agency
 where <!--- posteddate between '#session.opp_search_start#' and '#session.opp_search_end#' and --->
       closedate >= #now()# and
       contains((solicitationnumber,title,objective, description, phasei, phaseii, phaseiii, keywords ),'#opp_snapshot.snapshot_keyword#')
 order by closedate
</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

<div class="main_box">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
			 <tr><td class="feed_header">SBIR/STTRs (#agencies.recordcount#)</td>
				 <td class="feed_sub_header" align=right>
				 <a href="view_agencies.cfm?i=#i#&export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" hspace=10 title="Export to Excel"></a>
				 <a href="view_agencies.cfm?i=#i#&export=1">Export to Excel</a>


				 </td></tr>
		 </cfoutput>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

	       <cfif agencies.recordcount is 0>
                <tr><td class="feed_sub_header" style="font-weight: normal;">No SBIR/STTRs were found.</td></tr>
               <cfelse>

            <tr>
               <td></td>
               <td class="feed_sub_header">Opportunity Name</td>
               <td class="feed_sub_header">Organization</td>
               <td class="feed_sub_header" align=right>Closes In</td>
            </tr>

            <cfset counter = 0>

            <cfloop query="agencies">

   			 <cfoutput>

               <cfif agencies.source is "HHS">

  			 <cfif counter is 0>
  			  <tr bgcolor="ffffff" height=40>
  			 <cfelse>
  			  <tr bgcolor="e0e0e0" height=40>
  			 </cfif>

                <td width=70>

   			 	<cfif agencies.orgname_logo is "">
   			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
   			 	<cfelse>
   			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#agencies.orgname_logo#" align=top width=40 border=0></a>
   			 	</cfif>

                  </td>


   				 <td class="feed_sub_header" style="font-weight: normal;" width=800>

                    <a href="/exchange/include/sbir_hhs.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif agencies.objective is not "">
   					 <cfif len(agencies.objective) GT 500>
   					  #left(agencies.objective,500)#...
   					 <cfelse>
   					  #agencies.objective#
   					 </cfif>
   				 <cfelse>
   				  Objective not provided.
   				 </cfif>
   				 </a>

   				 </td>


   				<td class="feed_sub_header" style="font-weight: normal;">

   				<cfif agencies.agency is "" or agencies.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(agencies.agency)#</cfif>
   				<span class="link_small_gray"><br>#ucase(agencies.department)#</span></td>

                <cfelseif agencies.source is "DOD">

                <tr><td width=70>

   				 <cfif agencies.orgname_logo is "">
   				  <a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
   				 <cfelse>
   				  <a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#agencies.orgname_logo#" align=top width=40 border=0></a>
   				 </cfif>

                    </td>


   				 <td class="feed_sub_header" style="font-weight: normal;">

                    <a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif agencies.objective is not "">
   					 <cfif len(agencies.objective) GT 500>
   					  #left(agencies.objective,500)#...
   					 <cfelse>
   					  #agencies.objective#
   					 </cfif>
   				  <cfelse>
   				   Description not provided.
   				  </cfif>
   				  </a>

   			      <cfif agencies.keywords is not ""><br>
                     <br><span class="link_small_gray">KEYWORDS: </b>#ucase(agencies.keywords)#</span>
                    </cfif>

   				 </td>

   				<td class="feed_sub_header" style="font-weight: normal;">

   				<cfif agencies.agency is "" or agencies.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(agencies.agency)#</cfif>
   				<span class="link_small_gray"><br>#ucase(agencies.department)#</span></td>

                </cfif>

                   <cfset days = datediff("d",now(), agencies.closedate)>

  				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#days# Days</td>

                   <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.id#&t=sbir','targetWindow','toolbar=no,location=no, titlebar=0,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
  			     </td>

   				</tr>

  			 <cfif counter is 0>
  			  <cfset counter = 1>
  			 <cfelse>
  			  <cfset counter = 0>
  			 </cfif>

         </cfoutput>

               </cfloop>

               </cfif>

               <tr><td height=20></td></tr>

              </table>

         </div>

       </table>

       </td></tr>

       </table>

	  </div>

 </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>