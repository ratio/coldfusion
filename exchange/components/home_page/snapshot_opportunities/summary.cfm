<cfinclude template="/exchange/security/check.cfm">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pingroup
 where pingroup_usr_id = #session.usr_id# and
       pingroup_hub_id = #session.hub#
 order by pingroup_name
</cfquery>

<cfquery name="needs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from need
 where need_public = 1 and
	   need_hub_id = #session.hub#
 and contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
</cfquery>

<cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select opp_grant_id, opportunitytitle, department, agencyname, opportunitynumber, fundinginstrumenttype, awardfloor, awardceiling, closedate, orgname_logo from opp_grant
 left join orgname on orgname_name = department
 where closedate > #now()#
 and contains((opportunitytitle, department, description, agencyname),'#trim(opp_snapshot.snapshot_keyword)#')
</cfquery>

<cfquery name="challenges" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from challenge
 where challenge_public = 1
 and challenge_end >= '#dateformat(now(),'mm/dd/yyyy')#' and
	 challenge_hub_id = #session.hub# and
	 contains((*),'#trim(opp_snapshot.snapshot_keyword)#')
</cfquery>

<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics_code
 left join orgname on orgname_name = fbo_agency
 where (contains((fbo_opp_name, fbo_desc),'#trim(opp_snapshot.snapshot_keyword)#') )
 and fbo_response_date_original >= '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select * from award_data
left join orgname on orgname_name = awarding_sub_agency_name
where period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#'
	 and contains((awarding_agency_name, awarding_sub_agency_name, product_or_service_code_description, award_description),'#trim(opp_snapshot.snapshot_keyword)#')
order by period_of_performance_current_end_date
</cfquery>

<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_sbir
 left join orgname on orgname_name = agency
 where contains((solicitationnumber,title,objective, description, phasei, phaseii, phaseiii, keywords ),'#trim(opp_snapshot.snapshot_keyword)#') and
 closedate > #now()#
</cfquery>

<cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select opp_grant_id, opportunitytitle, department, agencyname, opportunitynumber, fundinginstrumenttype, awardfloor, awardceiling, closedate, orgname_logo from opp_grant
 left join orgname on orgname_name = department
 where closedate >= #now()#
 and contains((opportunitytitle, department, description, agencyname),'#trim(opp_snapshot.snapshot_keyword)#')
 order by closedate
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box">

			  <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">OPPORTUNITY SUMMARY</td>
			       <td align=right class="feed_sub_header">
			       <a href="edit.cfm?i=#i#&l=s"><img src="/images/icon_edit.png" width=20 border=0 hspace=5>
			       <a href="edit.cfm?i=#i#&l=s">Edit Topic</a>
			      </td></tr>
			   <tr><td colspan=2><hr></td></tr>
			  </table>
			  </cfoutput>

             <cfset keyword = #replace(opp_snapshot.snapshot_keyword,'"','',"all")#>

 			 <cfoutput>
 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td height=10></td></tr>
              <tr><td colspan=2 class="feed_header" style="font-weight: normal; padding-bottom: 0px;"><b>#ucase(opp_snapshot.snapshot_name)#</b><td></tr>
              <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><i>#keyword#</i><td></tr>

	         <cfif isdefined("u")>
	          <cfif u is 1>
	           <tr><td class="feed_sub_header" style="color: green;">The selected Opportunity has been pinned to your board.</td></tr>
	          </cfif>
	         </cfif>

             </table>
             </cfoutput>

             </div>

	  <div class="main_box">

             <!--- Fbo --->

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" style="padding-bottom: 0px;" colspan=2>BIDS</td></tr>
			  <tr><td colspan=10><hr></td></tr>

             <cfif fbo.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities were found.</td></tr>
             <cfelse>

             <cfset counter = 0>

		     <tr>
                <td></td>
                <td class="feed_sub_header">Opportunity Name</td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header">Type</td>
                <td class="feed_sub_header">Small Business</td>
                <td class="feed_sub_header" align=right>Date Posted</td>
                <td class="feed_sub_header" align=right>Closes In</td>
                <td></td>
             </tr>

			 <cfloop query="fbo">

			 <cfoutput>

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=40>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=40>
			 </cfif>

				 <td width=70 class="table_row" valign=middle>

				 <a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">
				<cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#fbo.orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>

				 </td>
				 <td class="feed_sub_header"><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo.fbo_opp_name#</a></b><span class="link_small_gray"><br>#class_code_name#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;" valign=middle>#fbo.fbo_agency#<span class="link_small_gray"><br>#fbo.fbo_dept#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;">#fbo.fbo_notice_type#</td>
				 <td class="feed_sub_header" style="font-weight: normal;"><cfif fbo.fbo_setaside_original is "">Not specified<cfelse>#fbo.fbo_setaside_original#</cfif></td>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right>#dateformat(fbo.fbo_pub_date_updated,'mm/dd/yyyy')#</td>

                  <cfset due_date = #fbo.fbo_response_date_original#>


                 <cfset days = datediff("d",now(), due_date)>

				 <td class="feed_sub_header" style="font-weight: normal;" align=right>#days# Days</td>

                </cfoutput>

                 <td align=right width=40>
					<div class="dropdown">
					  <img src="/images/icon_pin.png" style="cursor: pointer;" width=30>

					  <div class="dropdown-content" style="width: 275px; top: 6; text-align: left; right: 10;">
						<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pin to Board</b><br><br>
						<cfoutput query="boards">
							<a href="pintoboard.cfm?id=#fbo.fbo_id#&board=#boards.pingroup_id#&t=procurement&l=summary&i=#i#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;&nbsp;#boards.pingroup_name#</a>
						</cfoutput>
					  </div>
					</div>
			      </td>

			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

              </cfloop>

             </cfif>

             </table>

         </div>

	  <div class="main_box">

             <!--- Grants --->

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" style="padding-bottom: 0px;" colspan=2>GRANTS</td></tr>
			  <tr><td colspan=10><hr></td></tr>

             <cfif grants.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities were found.</td></tr>
             <cfelse>

             <cfset counter = 0>

		     <tr>
                <td></td>
                <td class="feed_sub_header">Grant Name</td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header">Opprotunity #</td>
               <td class="feed_sub_header" align=center>Funding</td>
                <td class="feed_sub_header" align=right>Award Ceiling</td>

                <td class="feed_sub_header" align=right>Closes In</td>
                <td></td>
             </tr>

        <cfloop query="grants">

         <cfoutput>
         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

			<td width=70 valign=top>
			<a href="/exchange/include/grant_new_information.cfm?id=#grants.opp_grant_id#" vspace=15 target="_blank" rel="noopener" rel="noreferrer">
            <cfif #grants.orgname_logo# is "">
              <img src="#image_virtual#/icon_usa.png" width=40 border=0>
            <cfelse>
              <img src="#image_virtual#/#grants.orgname_logo#" width=40 vspace=15 border=0>
            </cfif>
            </a>
            </td>

             <td class="feed_sub_header" width=450><b><a href="/exchange/include/grant_new_information.cfm?id=#grants.opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer">#grants.opportunitytitle#</a></b></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=400>#ucase(grants.agencyname)#<span class="link_small_gray"><br>#grants.agencyname#</span></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=120>#grants.opportunitynumber#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=75>#grants.fundinginstrumenttype#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#numberformat(grants.awardceiling,'$999,999,999')#</td>

                 <cfset days = datediff("d",now(),grants.closedate)>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#days# Days</td>


           </cfoutput>

                 <td align=right width=40>
					<div class="dropdown">
					  <img src="/images/icon_pin.png" style="cursor: pointer;" width=30>

					  <div class="dropdown-content" style="width: 275px; top: 6; text-align: left; right: 10;">
						<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pin to Board</b><br><br>
						<cfoutput query="boards">
							<a href="pintoboard.cfm?id=#grants.opp_grant_id#&board=#boards.pingroup_id#&t=grant&l=summary&i=#i#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;&nbsp;#boards.pingroup_name#</a>
						</cfoutput>
					  </div>
					</div>
			      </td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

            </cfif>

             <tr><td height=20></td></tr>
             <tr><td colspan=8><hr></td></tr>

             </table>

         </div>

		 <div class="main_box">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_sub_header" style="padding-bottom: 0px;" colspan=2>SBIR/STTRs</td></tr>
			  <tr><td colspan=10><hr></td></tr>

             <cfif sbir.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities were found.</td></tr>
             <cfelse>

          <tr>
             <td></td>
             <td class="feed_sub_header">Opportunity Name</td>
 				 <td width=30>&nbsp;</td>
             <td class="feed_sub_header">Organization</td>
             <td class="feed_sub_header" align=right>Close Date</td>
          </tr>

          <cfset counter = 0>

          <cfloop query="sbir">

 			 <cfoutput>

             <cfif sbir.source is "HHS">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=40>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=40>
			 </cfif>

              <td width=70>

 			 	<cfif sbir.orgname_logo is "">
 			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
 			 	<cfelse>
 			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#sbir.orgname_logo#" align=top width=40 border=0></a>
 			 	</cfif>

                </td>


 				 <td class="feed_sub_header" style="font-weight: normal;" width=800>

                  <a href="/exchange/include/sbir_hhs.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer">
                  <cfif sbir.objective is not "">
 					 <cfif len(sbir.objective) GT 1000>
 					  #left(sbir.objective,1000)#...
 					 <cfelse>
 					  #sbir.objective#
 					 </cfif>
 				 <cfelse>
 				  Objective not provided.
 				 </cfif>
 				 </a>

 				 </td>

 				 <td width=30>&nbsp;</td>

 				<td class="feed_sub_header" style="font-weight: normal;">

 				<cfif sbir.agency is "" or sbir.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(sbir.agency)#</cfif>
 				<span class="link_small_gray"><br>#ucase(sbir.department)#</span></td>

              <cfelseif sbir.source is "DOD">

              <tr><td width=70>

 				 <cfif sbir.orgname_logo is "">
 				  <a href="/exchange/include/sbir_dod.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
 				 <cfelse>
 				  <a href="/exchange/include/sbir_dod.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#sbir.orgname_logo#" align=top width=40 border=0></a>
 				 </cfif>

                  </td>

 				 <td width=30>&nbsp;</td>

 				 <td class="feed_sub_heade" style="font-weight: normal;">

                  <a href="/exchange/include/sbir_dod.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer">
                  <cfif sbir.description is not "">
 					 <cfif len(sbir.description) GT 600>
 					  #left(sbir.description,600)#...
 					 <cfelse>
 					  #sbir.description#
 					 </cfif>
 				  <cfelse>
 				   Description not provided.
 				  </cfif>
 				  </a>

 			      <cfif sbir.keywords is not ""><br>
                   <br><span class="link_small_gray">KEYWORDS: </b>#ucase(sbir.keywords)#</span>
                  </cfif>

 				 </td>

              </cfif>

                 <cfset days = datediff("d",now(), sbir.closedate)>

				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#days# Days</td>


              </cfoutput>

                <td align=right width=40>
					<div class="dropdown">
					  <img src="/images/icon_pin.png" style="cursor: pointer;" width=30>

					  <div class="dropdown-content" style="width: 275px; top: 6; text-align: left; right: 10;">
						<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pin to Board</b><br><br>
						<cfoutput query="boards">
							<a href="pintoboard.cfm?id=#sbir.id#&board=#boards.pingroup_id#&t=sbir&l=summary&i=#i#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;&nbsp;#boards.pingroup_name#</a>
						</cfoutput>
					  </div>
					</div>
			      </td>

 				</tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

             </cfloop>

             </cfif>

             <tr><td height=20></td></tr>
             <tr><td colspan=7><hr></td></tr>

            </table>

            </div>

		  <div class="main_box">


            <!--- Awards --->

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header" style="padding-bottom: 0px;" colspan=2>EXPIRING CONTRACTS</td></tr>
			  <tr><td colspan=10><hr></td></tr>

             <cfif awards.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities were found.</td></tr>
             <cfelse>

             <cfset counter = 0>

		     <tr>
                <td></td>
                <td class="feed_sub_header">Opportunity Name</td>
                <td></td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header">Incumbent</td>
                <td class="feed_sub_header" align=right>Total Value</td>
                <td class="feed_sub_header" align=right>Expires</td>
                <td></td>
             </tr>

			 <cfloop query="awards">

             <cfoutput>
			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=40>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=40>
			 </cfif>

				 <td width=70 class="table_row" valign=middle>

				 <a href="/exchange/include/award_information.cfm?id=#awards.id#" target="_blank" rel="noopener" rel="noreferrer">
				<cfif #awards.orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#awards.orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>

				 </td>
				 <td class="feed_sub_header" width=500><b><a href="/exchange/include/award_information.cfm?id=#awards.id#" target="_blank" rel="noopener" rel="noreferrer">

				 <cfif len(awards.award_description) GT 300>
				 #left(awards.award_description,300)#...
				 <cfelse>
				 #awards.award_description#
				 </cfif>

				 </a></b><span class="link_small_gray"><br>#awards.product_or_service_code_description#</span></td>
				 <td width=30>&nbsp;</td>
				 <td class="feed_sub_header" style="font-weight: normal;" valign=middle>#awards.awarding_sub_agency_name#<span class="link_small_gray"><br>#awards.awarding_agency_name#<br>#awards.awarding_office_name#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/include/federal_profile.cfm?duns=#awards.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#awards.recipient_name#</a></td>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(awards.current_total_value_of_award,'$999,999,999')#</td>

                 <cfset days = datediff("d",now(),awards.period_of_performance_current_end_date)>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>

				 <cfif days is 0>
				 Today
				 <cfelseif days is 1>
				 Tomorrow
				 <cfelse>
				 #days# Days
				 </cfif>

				 </td>

               </cfoutput>

                 <td align=right width=40>
					<div class="dropdown">
					  <img src="/images/icon_pin.png" style="cursor: pointer;" width=30>

					  <div class="dropdown-content" style="width: 275px; top: 6; text-align: left; right: 10;">
						<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pin to Board</b><br><br>
						<cfoutput query="boards">
							<a href="pintoboard.cfm?id=#awards.id#&board=#boards.pingroup_id#&t=contract&l=summary&i=#i#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;&nbsp;#boards.pingroup_name#</a>
						</cfoutput>
					  </div>
					</div>
			      </td>

			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfloop>

             </cfif>

             <tr><td height=10></td></tr>
             <tr><td colspan=7><hr></td></tr>

             </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

