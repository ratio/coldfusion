<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfquery name="snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<body class="body">

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from lab_tech
 where contains((*),'#trim(snapshot.snapshot_keyword)#')

<cfif sv is 1>
<cfelseif sv is 10>
</cfif>

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">Lab Innovations ( #trim(numberformat(agencies.recordcount,'99,999'))# )</td>
				 <td class="feed_sub_header" align=right>
				 <a href="cview_07.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="cview_07.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
				 &nbsp;|&nbsp;

				 <a href="/exchange/" addtoken="no">Return</a>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

					<cfif agencies.recordcount GT #perpage#>

				    <td class="feed_sub_header">

				    Page -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&i=#i#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&i=#i#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				</td>
				</cfif>

                <cfset keyword = #replace(snapshot.snapshot_keyword,'"','',"all")#>

				</tr>

			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td class="feed_sub_header"><b>Innovation Description</b></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <cfset bgcolor="ffffff">
         <cfelse>
          <cfset bgcolor="e0e0e0">
         </cfif>

         <tr bgcolor="#bgcolor#">
            <td class="feed_sub_header"><a href="/exchange/include/labtech_information.cfm?id=#id#" target="_blank">#availabletechnology#</td>
            </tr>
         <tr bgcolor="#bgcolor#">
            <td class="feed_sub_header" style="font-weight: normal;">#shortdescription#</b></td></tr>
         </tr>

         <tr bgcolor="#bgcolor#">
            <td class="feed_option" style="padding-bottom: 10px;"><b>#department# - #federallab#</b></td>
         </tr>

         <tr bgcolor="#bgcolor#" height=10></td></tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>