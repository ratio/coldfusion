<cfquery name="snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub# and
        snapshot_type_id = 1
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header" valign=middle>Topics</td>
     <td class="feed_sub_header" class="feed_sub_header" valign=middle align=right>

     <a href="/exchange/components/home_page/snapshot_company/add.cfm"><img src="/images/plus3.png" width=15 alt="Configure" title="Configure" border=0 align=absmiddle hspace=5></a>

     <cfif snapshot.recordcount LT 5>
      <a href="/exchange/components/home_page/snapshot_company/add.cfm">Add Topic</a>
     </cfif>

     </td></tr>
</table>

    <table cellspacing=0 cellpadding=0 border=0 width=100%>

    <cfif snapshot.recordcount is 0>
     <tr><td class="feed_sub_header" style="font-weight: normal;">No topics were found. <a href="/exchange/components/home_page/snapshot_company/add.cfm">Click here</a> to add a Topic.</td></tr>
    <cfelse>

    <tr>
	  <td class="feed_sub_header">&nbsp;Maxiumum of 5 Topics</td>
	  <td class="feed_sub_header" align=center>All<br>Companies</td>
	  <td class="feed_sub_header" align=center>Won<br>Contracts</td>
	  <td class="feed_sub_header" align=center>Won<br>Grants</td>
	  <td class="feed_sub_header" align=center>Won<br>SBIR/STTRs</td>
	  <td class="feed_sub_header" align=center>Awarded<br>Patents</td>
	  <td class="feed_sub_header" align=center>University<br>Innovations</td>
	  <td class="feed_sub_header" align=center>Lab<br>Innovations</td>
      <td></td>
    </tr>

    <tr><td colspan=9><hr></td></tr>

     <cfloop query="snapshot">

	  <cfquery name="comp_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(company_id) as total from company
	   where contains((*),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="university_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(id) as total from autm
	   where contains((*),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="lab_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(id) as total from lab_tech
	   where contains((*),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="awards_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(recipient_duns)) as total from award_data
	   where contains((awarding_agency_name, award_description),'#trim(snapshot.snapshot_keyword)#')
      </cfquery>

	  <cfquery name="grants_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	    select count(distinct(recipient_duns)) as total from grants
        where contains((cfda_title, awarding_agency_name, awarding_sub_agency_name, award_description),'#trim(snapshot.snapshot_keyword)#') and
        recipient_duns is not null
	  </cfquery>

	  <cfquery name="sbir_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select count(id) as total from sbir
          where contains((award_title, department, research_keywords, abstract),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="patent_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
         select count(patent_id) as total from patent
		 where contains((patent.patent_id, title, abstract),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	<cfoutput>

		<tr bgcolor="FFFFFF">
	    <td class="feed_sub_header">&nbsp;<a href="/exchange/components/home_page/snapshot_company/edit.cfm?i=#snapshot.snapshot_id#">#snapshot.snapshot_name#</a><span class="link_small_gray" style="font-weight: normal;"><br>&nbsp;<i>#replace(snapshot.snapshot_keyword,'"','',"all")#</i></span></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif comp_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(comp_total.total,'99,999')#</a></cfif></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif awards_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_02.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(awards_total.total,'99,999')#</a></cfif></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif grants_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_03.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(grants_total.total,'99,999')#</a></cfif></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif sbir_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_04.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(sbir_total.total,'99,999')#</a></cfif></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif patent_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_05.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(patent_total.total,'99,999')#</a></cfif></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif university_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_06.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(university_total.total,'99,999')#</a></cfif></td>
		<td class="big_number" align=center style="background-color: 0C314D; font-size: 20px; color: FFFFFF; width: 11%;"><cfif lab_total.total is 0>0<cfelse><a href="/exchange/components/home_page/snapshot_company/cview_07.cfm?i=#snapshot.snapshot_id#" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(lab_total.total,'99,999')#</a></cfif></td>
        <td align=center width=30>

					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=6 hspace=10>
					  <div class="dropdown-content" style="width: 200px; text-align: left; padding-left: 0px;">
						<a href="/exchange/components/home_page/snapshot_company/edit.cfm?i=#snapshot.snapshot_id#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Update Topic</a>
						<a href="/exchange/components/home_page/snapshot_company/delete.cfm?i=#snapshot.snapshot_id#" onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">&nbsp;<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;Delete Topic</a>
					  </div>
					</div>

    </tr>

	</cfoutput>

    </cfloop>

   </cfif>

</table>

