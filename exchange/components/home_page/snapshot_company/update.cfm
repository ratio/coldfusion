<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Refresh">

	<cfset search_string = #replace(new_string,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,')','',"all")#>
	<cfset search_string = #replace(search_string,'(','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update snapshot
	 set snapshot_keyword = '#search_string#'
	 where snapshot_id = #i# and
	       snapshot_hub_id = #session.hub# and
	       snapshot_usr_id = #session.usr_id#
	</cfquery>

<cfif location is "cview_05_details">
	<cflocation URL="#location#.cfm?sv=#sv#&i=#i#&name=#name#" addtoken="no">
<cfelse>
	<cflocation URL="#location#.cfm?sv=#sv#&i=#i#" addtoken="no">
</cfif>

</cfif>