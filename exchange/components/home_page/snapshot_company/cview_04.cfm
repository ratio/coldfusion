<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfquery name="snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 40>
</cfif>

<body class="body">

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sbir.duns, company_name, cast(company_about as varchar(max)) as about_short, cast(company_long_desc as varchar(max)) as about_long, company_keywords, company.company_website, company_city, company_state, company_id, company_logo from sbir
  join company on company_duns = sbir.duns
  where (sbir.duns <> '' or company_duns <> '') and
  company.company_duns is not null
  and contains((award_title, department, research_keywords, abstract),'#trim(snapshot.snapshot_keyword)#')
  group by sbir.duns, company_name, cast(company_about as varchar(max)), cast(company_long_desc as varchar(max)), company_keywords, company.company_website, company_city, company_state, company_id, company_logo

 <cfif sv is 1>
  order by company_name ASC
 <cfelseif sv is 10>
  order by company_name DESC
 <cfelseif sv is 2>
  order by company_city ASC
 <cfelseif sv is 20>
  order by company_city DESC
 <cfelseif sv is 3>
  order by company_state ASC
 <cfelseif sv is 30>
  order by company_state DESC
 <cfelseif sv is 4>
  order by total asc
 <cfelseif sv is 40>
  order by total DESC
 <cfelseif sv is 5>
  order by fbo_opp_name ASC
 <cfelseif sv is 50>
  order by fbo_opp_name DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 80>
  order by fbo_naics DESC
 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC
 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC
 </cfif>

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">COMPANIES FOUND ( #trim(numberformat(agencies.recordcount,'99,999'))# ) - AWARDED SBIR/STTRs</td>
				 <td class="feed_sub_header" align=right>
				 <a href="cview_03.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" hspace=10 title="Export to Excel"></a>
				 <a href="cview_03.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
				 &nbsp;|&nbsp;

				 <a href="/exchange/" addtoken="no">Return</a>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

					<cfif agencies.recordcount GT #perpage#>

				    <td class="feed_sub_header">

				    Result Set -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&i=#i#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&i=#i#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				</td>
				</cfif>

                <cfset keyword = #replace(snapshot.snapshot_keyword,'"','',"all")#>

				<td class="feed_header" align=right>
				<form action="update.cfm" method="post">
				    <td class="feed_sub_header" style="font-weight: normal;" align=right><b>Update Keyword</b>&nbsp;&nbsp;
				    <input class="input_text" type="text" style="width: 300px;" name="new_string" required value='#keyword#'>&nbsp;&nbsp;
				    <input class="button_blue" type="submit" name="button" value="Refresh">
				    <input type="hidden" name="location" value="cview_04">
				    <input type="hidden" name="sv" value=#sv#>
				    <input type="hidden" name="i" value=#i#>
                    </td>
                </form>
                </td>

				</tr>

				<tr><td height=20></td></tr>
			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="cview_04.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>NAME</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" style="padding-left: 10px; padding-right: 10px;"><b>OVERVIEW</b></td>
            <td class="feed_sub_header"><b>WEBSITE</b></td>
            <td class="feed_sub_header"><a href="cview_04.cfm?i=#i#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CITY</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="cview_04.cfm?i=#i#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>STATE</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="cview_04.cfm?i=#i#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>SBIR/STTRs</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

             <td width=60>

                    <a href="/exchange/include/profile.cfm?id=#company_id#&l=14&s=t" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=40 border=0>
					</cfif>
					</a>

			 </td>

             <td class="feed_sub_header" width=350><b><a href="/exchange/include/profile.cfm?id=#company_id#&l=14&s=t" target="_blank" rel="noopener" rel="noreferrer"><cfif len(company_name) GT 200>#ucase(left(company_name,200))#...<cfelse>#ucase(company_name)#</cfif></b></td>
             <cfif about_short is not "">
	             <td class="feed_option" width=800 style="padding-left: 10px; padding-right: 10px;">#ucase(about_short)#</td>
             <cfelse>
	             <td class="feed_option" width=800 style="padding-left: 10px; padding-right: 10px;">#ucase(about_long)#</td>
             </cfif>
             <td class="feed_option"><a href="#company_website#" target="_blank" rel="noopener" rel="noreferrer"><u>#ucase(company_website)#</u></a></td>
             <td class="feed_option" width=100>#ucase(company_city)#</td>
             <td class="feed_option" align=center>#ucase(company_state)#</td>
             <td class="feed_option" align=center style="padding-left: 5px; padding-right: 5px; font-size: 18px;"><b>#total#</b></td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>