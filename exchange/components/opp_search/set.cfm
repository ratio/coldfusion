<cfset search_string = #replace(keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>

<cfif timeframe is 1>
 <cfset start = #dateformat(now(),'mm/dd/yyyy')#>
 <cfset end = #dateformat(now(),'mm/dd/yyyy')#>
<cfelseif timeframe is 2>
 <cfset start = #dateadd("d",-1,now())#>
 <cfset end = #dateformat(now(),'mm/dd/yyyy')#>
<cfelseif timeframe is 3>
 <cfset start = #dateadd("d",-7,now())#>
 <cfset end = #dateformat(now(),'mm/dd/yyyy')#>
<cfelseif timeframe is 4>
 <cfset start = #dateadd("d",-30,now())#>
 <cfset end = #dateformat(now(),'mm/dd/yyyy')#>
<cfelseif timeframe is 5>
 <cfset start = #dateadd("d",-365,now())#>
 <cfset end = #dateformat(now(),'mm/dd/yyyy')#>
</cfif>

<cfif isdefined("search_contracts")>
 <cfset session.opp_contracts = 1>
<cfelse>
 <cfset session.opp_contracts = 0>
</cfif>

<cfif isdefined("search_grants")>
 <cfset session.opp_grants = 1>
<cfelse>
 <cfset session.opp_grants = 0>
</cfif>

<cfif isdefined("search_challenges")>
 <cfset session.opp_challenges = 1>
<cfelse>
 <cfset session.opp_challenges = 0>
</cfif>

<cfif isdefined("search_sbirs")>
 <cfset session.opp_sbirs = 1>
<cfelse>
 <cfset session.opp_sbirs = 0>
</cfif>

<cfif isdefined("search_needs")>
 <cfset session.opp_needs = 1>
<cfelse>
 <cfset session.opp_needs = 0>
</cfif>

<cfset session.opp_search_start = '#dateformat(start,'mm/dd/yyyy')#'>
<cfset session.opp_search_end = '#dateformat(end,'mm/dd/yyyy')#'>
<cfset session.opp_timeframe = #timeframe#>
<cfset session.raw_search_string = '#keyword#'>
<cfset session.opp_search_string = '#search_string#'>
<cflocation URL="results.cfm" addtoken="no">