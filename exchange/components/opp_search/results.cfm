<cfinclude template="/exchange/security/check.cfm">

<cfset fbo_count = 0>
<cfset need_count = 0>
<cfset sbir_count = 0>
<cfset award_count = 0>
<cfset grant_count = 0>
<cfset challenge_count = 0>
<cfset need_count = 0>

<cfif session.opp_needs is 1>

	<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from need
	 join hub on hub_id = need_hub_id
	 where need_status = 'Open' and
		   need_hub_id = #session.hub# and
		   need_public > 0 and
	       contains((need_name, need_desc, need_desc_full, need_keywords),'#trim(session.opp_search_string)#')
	</cfquery>

	<cfset need_count = needs.recordcount>
</cfif>

<cfif session.opp_grants is 1>
	<cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select opp_grant_id, opportunitytitle, department, agencyname, opportunitynumber, fundinginstrumenttype, awardfloor, awardceiling, closedate, orgname_logo from opp_grant
	 left join orgname on orgname_name = department
	 where posteddate between '#session.opp_search_start#' and '#session.opp_search_end#' and
	       closedate >= #now()# and
	       contains((opportunitytitle, department, description, agencyname),'#trim(session.opp_search_string)#')
     order by closedate
	</cfquery>
	<cfset grant_count = grants.recordcount>

</cfif>

<cfif session.opp_challenges is 1>

	<cfquery name="set" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select hub_parent_hub_id from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfif set.hub_parent_hub_id is "">
	 <cfset hub_list = 0>
	<cfelse>

	 <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select hub_id from hub
	  where hub_id = #set.hub_parent_hub_id#
	 </cfquery>

	 <cfif list.recordcount is 0>
	  <cfset hub_list = 0>
	 <cfelse>
	  <cfset hub_list = valuelist(list.hub_id)>
	 </cfif>

	</cfif>

	<cfquery name="cinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select challenge_source, challenge_url, challenge_keywords, challenge_end, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
	 join hub on hub_id = challenge_hub_id
	 where (challenge_hub_id = #session.hub# or challenge_hub_id in (#hub_list#)) and
			challenge_public = 1

	 and contains((challenge_name, challenge_keywords, challenge_organization, challenge_desc),'#trim(session.opp_search_string)#')

	 union
	 select challenge_source, challenge_url, challenge_keywords, challenge_end, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
	 join hub on hub_id = challenge_hub_id
	 where challenge_public = 2
	 and contains((challenge_name, challenge_keywords, challenge_organization, challenge_desc),'#trim(session.opp_search_string)#')

     union

	 select challenge_source, challenge_url, challenge_keywords, challenge_end, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
	 left join hub on hub_id = challenge_hub_id
	 where challenge_type_id = 1

	 and contains((challenge_name, challenge_keywords, challenge_organization, challenge_desc),'#trim(session.opp_search_string)#')

	 order by challenge_total_cash DESC
	</cfquery>

	<cfset challenge_count = cinfo.recordcount>

</cfif>

<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

<cfif session.opp_contracts is 1>

	<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from fbo
	 left join class_code on class_code_code = fbo_class_code
	 left join naics on naics_code = fbo_naics_code
	 left join orgname on orgname_name = fbo_agency
	 where (contains((fbo_opp_name, fbo_desc),'#trim(session.opp_search_string)#') )
	 and fbo_import_date between '#session.opp_search_start#' and '#session.opp_search_end#'
	 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
	</cfquery>

	<cfset fbo_count = fbo.recordcount>

</cfif>

<cfif session.opp_sbirs is 1>

	<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from opp_sbir
	 left join orgname on orgname_name = agency
	 where posteddate between '#session.opp_search_start#' and '#session.opp_search_end#' and
	       closedate >= #now()# and
	       contains((solicitationnumber,title,objective, description, phasei, phaseii, phaseiii, keywords ),'#trim(session.opp_search_string)#')
	 order by closedate
	</cfquery>

	<cfset sbir_count = sbir.recordcount>

</cfif>

<cfset total_count = sbir_count + grant_count + award_count + challenge_count + need_count + fbo_count>
<cfset keyword = #replace(session.opp_search_string,'"','',"all")#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  <cfinclude template="/exchange/components/recent_boards/index.cfm">

      </td><td valign=top>

      <div class="main_box">
      <cfinclude template="index.cfm">

			  <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td class="feed_header">Search Results</td>
			       <td align=right class="feed_sub_header" style="font-weight: normal;">

					<div class="dropdown">
					<img src="/images/plus3.png" width=15 hspace=10><span class="feed_sub_header">Create Notification</span>
					  <div class="dropdown-content" style="height: 425px; width: 375px; top: 14; text-align: left; right: 10;">
						  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                          <form action="save_notification.cfm" method="post">
                           <tr><td class="feed_sub_header" colspan=2>Create Notification</td></tr>
                           <tr><td colspan=2><hr></td></tr>
                           <tr><td class="feed_sub_header">Name</td>
                               <td><input type="text" class="input_text" placeholder="Notification name" required name="usr_notification_name" style="width: 225px;"></td>
                           </tr>

                           <tr><td class="feed_sub_header">Keywords</td>
                               <td><input type="text" class="input_text" placeholder="search keywords" required name="usr_notification_keywords" value="#session.raw_search_string#" style="width: 225px;"></td>
                           </tr>

                           <tr><td class="feed_sub_header" valign=top>Sources</td>
                               <td>

								  <table cellspacing=0 cellpadding=0 border=0 width=100%>
								   <tr>
								      <td><input type="checkbox" name="usr_notification_contracts" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_contracts")><cfif session.opp_contracts is 1>checked</cfif><cfelse>checked</cfif>></td>
								      <td class="feed_sub_header" style="font-weight: normal;">Contracts</td>
								      <td><input type="checkbox" name="usr_notification_grants" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_grants")><cfif session.opp_grants is 1>checked</cfif><cfelse>checked</cfif>></td>
								      <td class="feed_sub_header" style="font-weight: normal;">Grants</td>
								   </tr>

								   <tr>
								      <td><input type="checkbox" name="usr_notification_sbirs" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_sbirs")><cfif session.opp_sbirs is 1>checked</cfif><cfelse>checked</cfif>></td>
								      <td class="feed_sub_header" style="font-weight: normal;">SBIR/STTRs</td>
								      <td><input type="checkbox" name="usr_notification_challenges" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_challenges")><cfif session.opp_challenges is 1>checked</cfif><cfelse>checked</cfif>></td>
								      <td class="feed_sub_header" style="font-weight: normal;">Challenges</td>
								   </tr>

								   <tr>
								      <td><input type="checkbox" name="usr_notification_needs" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_needs")><cfif session.opp_needs is 1>checked</cfif><cfelse>checked</cfif>></td>
								      <td class="feed_sub_header" style="font-weight: normal;">Needs</td>
								      <td></td>
								      <td></td>
								   </tr>

								  </table>

                               </td>
                           </tr>

                           <tr><td colspan=2><hr></td></tr>
                           <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>Schedule</b>&nbsp;
                               <select name="usr_notification_frequency" class="input_select" style="width: 160px;">
                                <option value="Daily">Daily
                                <option value="Weekly">Weekly
                                <option value="Monthly">Monthly
                               </select>
                               </td></tr>

                           <input type="hidden" name="usr_notification_type" value="Open Opportunities">
                           <input type="hidden" name="return_page" value="/exchange/components/opp_search/results.cfm">

                           <tr><td colspan=2><hr></td></tr>
                           <tr><td height=10></td></tr>
                           <tr><td></td><td><input type="submit" name="button" class="button_blue" value="Save Notification"></td></tr>
						   </form>
						  </table>

					  </div>
					</div>

			      </td></tr>
         <tr><td height=10></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">The selected Opportunity has been pinned to your board.</td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_sub_header" style="color: green;">Opportunity notification has been successfully created.</td></tr>
          </cfif>
         </cfif>

			   <tr>
			      <td class="feed_sub_header" style="font-weight: normal;">

			      <cfif total_count is 0>
			       Unfortunately, we did not find any Opportunities matching "<b><i>#keyword#</i></b>".<br>Please try to refine your search criteria.
			      <cfelseif total_count is 1>
			       We found <b>1</b> Opportunity matching "<b><i>#keyword#</i></b>".<br>To fine more, please refine your search criteria above.
			      <cfelse>
			      We found <b>#trim(numberformat(total_count,'99,999'))#</b> Opportunities matching "<b><i>#keyword#</i></b>".
			      </cfif>
			      </td>

			      <td class="feed_sub_header" style="text-decoration: none;" align=right>
			         <cfif total_count GT 0>

			          <cfif session.opp_contracts is 1>
                       <a href="##contracts" style="text-decoration: none;">
			           <span style="background-color: green; padding-top: 8px; color: ffffff; border-radius: 150px; margin-left: 5px; padding-bottom: 8px; padding-left: 15px; padding-right: 15px;"><cfif fbo_count is 0>No Contracts<cfelseif fbo_count is 1>1 Contract<cfelse>#fbo_count# Contracts</cfif></span>
			           </a>
			          </cfif>

			          <cfif session.opp_grants is 1>
                       <a href="##grants" style="text-decoration: none;">
		               <span style="background-color: green; padding-top: 8px; color: ffffff; border-radius: 150px; margin-left: 5px; padding-bottom: 8px; padding-left: 15px; padding-right: 15px;"><cfif grant_count is 0>No Grants<cfelseif grant_count is 1>1 Grant<cfelse>#grant_count# Grants</cfif></span>
		               </a>
			          </cfif>

			          <cfif session.opp_sbirs is 1>
                       <a href="##sbirs" style="text-decoration: none;">
			           <span style="background-color: green; padding-top: 8px; color: ffffff; border-radius: 150px; margin-left: 5px; padding-bottom: 8px; padding-left: 15px; padding-right: 15px;"><cfif sbir_count is 0>No SBIR/STTRs<cfelseif sbir_count is 1>1 SBIR/STTR<cfelse>#sbir_count# SBIR/STTRs</cfif></span>
                       </a>
			          </cfif>

			          <cfif session.opp_challenges is 1>
                       <a href="##challenges" style="text-decoration: none;">
			           <span style="background-color: green; padding-top: 8px; color: ffffff; border-radius: 150px; margin-left: 5px; padding-bottom: 8px; padding-left: 15px; padding-right: 15px;"><cfif challenge_count is 0>No Challenges<cfelseif challenge_count is 1>1 Challenge<cfelse>#challenge_count# Challenges</cfif></span>
			           </a>
			          </cfif>

			          <cfif session.opp_needs is 1>
                       <a href="##needs" style="text-decoration: none;">
			           <span style="background-color: green; padding-top: 8px; color: ffffff; border-radius: 150px; margin-left: 5px; padding-bottom: 8px; padding-left: 15px; padding-right: 15px;"><cfif need_count is 0>No Needs<cfelseif need_count is 1>1 Need<cfelse>#need_count# Needs</cfif></span>
			           </a>
			          </cfif>

			         </cfif>
			      </td>

			   </tr>

			   <tr><td height=10></td></tr>

			  </table>
			  </cfoutput>

       </div>

       <!--- Contracts --->

       <cfif session.opp_contracts is 1>

     <a name="contracts">

       <div class="main_box">

             <!--- Fbo --->

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_header" style="padding-bottom: 0px;">Contracts</td>
			      <td align=right><img src="/images/betasamgov.png" height=25></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			 </table>

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif fbo.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No Contracts were found.</td></tr>
             <cfelse>

             <cfset counter = 0>

		     <tr>
                <td></td>
                <td class="feed_sub_header">Opportunity Name</td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header">Type</td>
                <td class="feed_sub_header">Small Business</td>
                <td class="feed_sub_header" align=right>Date Posted</td>
                <td class="feed_sub_header" align=right>Date Due</td>
                <td></td>
             </tr>

			 <cfloop query="fbo">

			 <cfoutput>

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=40>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=40>
			 </cfif>

				 <td width=70 class="table_row" valign=middle>

				 <a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">
				<cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#fbo.orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>

				 </td>
				 <td class="feed_sub_header"><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo.fbo_opp_name#</a></b><span class="link_small_gray"><br>#class_code_name#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;" width=300>#fbo.fbo_agency#<span class="link_small_gray"><br>#fbo.fbo_dept#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;">#fbo.fbo_notice_type#</td>
				 <td class="feed_sub_header" style="font-weight: normal;" width=125><cfif fbo.fbo_setaside_original is "">Not specified<cfelse>#fbo.fbo_setaside_original#</cfif></td>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>

                 <cfif fbo.fbo_pub_date_updated is not "">
                  #dateformat(fbo.fbo_pub_date_updated,'mmm dd, yyyy')#
                 <cfelse>
                  #dateformat(fbo.fbo_pub_date_original,'mmm dd, yyyy')#
                 </cfif>
				 </td>

				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>


                 <cfif fbo.fbo_response_date_updated is not "">
                  #dateformat(fbo.fbo_response_date_updated,'mmm dd, yyyy')#
                 <cfelse>
                  <cfif #fbo.fbo_response_date_original# is "">Not Specified<cfelse>#dateformat(fbo.fbo_response_date_original,'mmm dd, yyyy')#</cfif>
                 </cfif>

                 </td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#fbo.fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>

                 </cfoutput>

			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

              </cfloop>

             </cfif>

             <tr><td height=20></td></tr>

             </table>

             </div>

             </a>

       </cfif>

       <!--- Grants --->

       <cfif session.opp_grants is 1>

       <a name="grants">

       <div class="main_box">

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_header" style="padding-bottom: 0px;">Grants</td>
			      <td align=right><img src="/images/grants_gov.png" height=30></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			 </table>

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif grants.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No Grants were found.</td></tr>
             <cfelse>

             <cfset counter = 0>

		     <tr>
                <td></td>
                <td class="feed_sub_header">Grant Name</td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header">Opportunity #</td>
                <td class="feed_sub_header" align=center>Funding</td>
                <td class="feed_sub_header" align=right>Award Ceiling</td>
                <td class="feed_sub_header" align=right>Closes In</td>
                <td></td>
             </tr>

        <cfloop query="grants">

         <cfoutput>
         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

			<td width=70 valign=top>
			<a href="/exchange/include/grant_new_information.cfm?id=#grants.opp_grant_id#" vspace=15 target="_blank" rel="noopener" rel="noreferrer">
            <cfif #grants.orgname_logo# is "">
              <img src="#image_virtual#/icon_usa.png" width=40 border=0>
            <cfelse>
              <img src="#image_virtual#/#grants.orgname_logo#" width=40 vspace=15 border=0>
            </cfif>
            </a>
            </td>

             <td class="feed_sub_header" width=450><b><a href="/exchange/include/grant_new_information.cfm?id=#grants.opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer">#grants.opportunitytitle#</a></b></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=400>#ucase(grants.agencyname)#<span class="link_small_gray"><br>#grants.agencyname#</span></td>
             <td class="feed_sub_header" style="font-weight: normal;" width=120>#grants.opportunitynumber#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=center width=75>#grants.fundinginstrumenttype#</td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#numberformat(grants.awardceiling,'$999,999,999')#</td>

                 <cfset days = datediff("d",now(),grants.closedate)>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#days# Days</td>
                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#grants.opp_grant_id#&t=grant','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>
		   </cfoutput>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

            </cfif>

             <tr><td height=20></td></tr>

             </table>

          </div>

          </a>

       </cfif>

     <cfif session.opp_sbirs is 1>

     <a name="sbirs">

     <div class="main_box">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_header" style="padding-bottom: 0px;" colspan=2>SBIR/STTRs</td></tr>
			  <tr><td colspan=10><hr></td></tr>

             <cfif sbir.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No SBIR/STTRs were found.</td></tr>
             <cfelse>

          <tr>
             <td></td>
             <td class="feed_sub_header">Opportunity Name</td>
             <td class="feed_sub_header">Organization</td>
             <td class="feed_sub_header" align=right>Closes In</td>
          </tr>

          <cfset counter = 0>

          <cfloop query="sbir">

 			 <cfoutput>

             <cfif sbir.source is "HHS">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=40>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=40>
			 </cfif>

              <td width=70>

 			 	<cfif sbir.orgname_logo is "">
 			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
 			 	<cfelse>
 			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#sbir.orgname_logo#" align=top width=40 border=0></a>
 			 	</cfif>

                </td>


 				 <td class="feed_sub_header" style="font-weight: normal;" width=800>

                  <a href="/exchange/include/sbir_hhs.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer">
                  <cfif sbir.objective is not "">
 					 <cfif len(sbir.objective) GT 1000>
 					  #left(sbir.objective,1000)#...
 					 <cfelse>
 					  #sbir.objective#
 					 </cfif>
 				 <cfelse>
 				  Objective not provided.
 				 </cfif>
 				 </a>

 				 </td>


 				<td class="feed_sub_header" style="font-weight: normal;">

 				<cfif sbir.agency is "" or sbir.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(sbir.agency)#</cfif>
 				<span class="link_small_gray"><br>#ucase(sbir.department)#</span></td>

              <cfelseif sbir.source is "DOD">

              <tr><td width=70>

 				 <cfif sbir.orgname_logo is "">
 				  <a href="/exchange/include/sbir_dod.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
 				 <cfelse>
 				  <a href="/exchange/include/sbir_dod.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#sbir.orgname_logo#" align=top width=40 border=0></a>
 				 </cfif>

                  </td>


 				 <td class="feed_sub_header" style="font-weight: normal;">

                  <a href="/exchange/include/sbir_dod.cfm?id=#sbir.id#" target="_blank" rel="noopener" rel="noreferrer">
                  <cfif sbir.objective is not "">
 					 <cfif len(sbir.objective) GT 600>
 					  #left(sbir.objective,600)#...
 					 <cfelse>
 					  #sbir.objective#
 					 </cfif>
 				  <cfelse>
 				   Description not provided.
 				  </cfif>
 				  </a>

 			      <cfif sbir.keywords is not ""><br>
                   <br><span class="link_small_gray">KEYWORDS: </b>#ucase(sbir.keywords)#</span>
                  </cfif>

 				 </td>

 				<td class="feed_sub_header" style="font-weight: normal;">

 				<cfif sbir.agency is "" or sbir.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(sbir.agency)#</cfif>
 				<span class="link_small_gray"><br>#ucase(sbir.department)#</span></td>

              </cfif>

                 <cfset days = datediff("d",now(), sbir.closedate)>

				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#days# Days</td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#sbir.id#&t=sbir','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>
			  </cfoutput>

 				</tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

             </cfloop>

             </cfif>

             <tr><td height=20></td></tr>

            </table>

         </div>

         </a>

     </cfif>

     <cfif session.opp_challenges is 1>

     <a name="challenges">

     <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header" colspan=3>Challenges</a></td></tr>
          <tr><td colspan=3><hr></td></tr>


          <cfif cinfo.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Challenges were found.</td></tr>
          <cfelse>

          <cfset count = 1>

           <cfloop query="cinfo">

           <cfoutput>

               <tr>

				<td valign=top width=125>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td align=center>

				    <cfif cinfo.challenge_type_id is 1>

						<cfif cinfo.challenge_image is "">
						  <a href="#cinfo.challenge_url#" target="_blank" rel="noopener"><img src="#image_virtual#/stock_challenge.png" vspace=10 width=100 border=0></a>
						<cfelse>
						  <a href="#cinfo.challenge_url#" target="_blank" rel="noopener"><img src="#cinfo.challenge_image#" onerror="this.onerror=null; this.src='#image_virtual#/stock_challenge.png'" style="margin-top: 10px;" vspace=10 width=100></a>
						</cfif>

					<cfelse>

						<cfif cinfo.challenge_image is "">
						  <a href="/exchange/opps/challenge_detail.cfm?i=#encrypt(cinfo.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/stock_challenge.png" vspace=10 width=100 border=0></a>
						<cfelse>
						  <a href="/exchange/opps/challenge_detail.cfm?i=#encrypt(cinfo.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#cinfo.challenge_image#" vspace=10 width=100 border=0></a>
						</cfif>

					</cfif>

			    </td></tr>

			    </table>

			    </td>

			    <td width=20></td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr>
                    <td class="feed_sub_header">

					<cfif cinfo.challenge_source is "Innocentive">
					  <img src="/images/logo_innocentive.png" height=30 style="margin-right: 10px;">
					<cfelseif cinfo.challenge_source is "Challenge.Gov">
					  <img src="/images/logo_challenge.png" height=30 style="margin-right: 10px;">
					<cfelseif cinfo.challenge_source is "IdeaConnection">
					  <img src="/images/logo_ideaconnection.png" height=30 style="margin-right: 10px;">
					<cfelseif cinfo.challenge_source is "Kaggle">
					  <img src="/images/logo_kaggle.png" height=30 style="margin-right: 10px;">
					</cfif>

                    <cfif cinfo.challenge_type_id is 1>
                     <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer">#cinfo.challenge_name#</a>
                    <cfelse>
                     <a href="/exchange/opps/challenge_detail.cfm?i=#encrypt(cinfo.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#cinfo.challenge_name#</a>
                    </cfif>

                    </td>
                    <td align=right width=50 valign=top>
					<img vspace=10 src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#cinfo.challenge_id#&t=challenge','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			        </td>
                 </tr>

                 <tr>
                    <td class="feed_sub_header" style="font-weight: normal;" colspan=2>#cinfo.challenge_desc#</td>
                 </tr>


                <cfif cinfo.challenge_keywords is not "">
                 <tr><td class="link_small_gray" style="font-weight: normal;" colspan=3><b><i>#cinfo.challenge_keywords#</i></b></td></tr>
                </cfif>

                <tr>

                   <td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;">

				   <b>Ends - </b>

					<cfif #cinfo.challenge_end# is "">
					 TBD
					<cfelse>
					 #dateformat(cinfo.challenge_end,'mmm d, yyyy')#
					</cfif>


                  </td>

                <td class="feed_sub_header" style="font-weight: normal;" align=right>

                    <b>
                    Prize -
                    <cfif #cinfo.challenge_total_cash# is not "">

						<cfif cinfo.challenge_currency is "$">$
						<cfelseif cinfo.challenge_currency is "I">INR
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelse>
						</cfif>

                      #numberformat(cinfo.challenge_total_cash,'$999,999,999')#
                    <cfelse>
                     Not Specified
                    </cfif>
                    </b>


                </td>

		        </tr>

                </table>

                </td></tr>

			   <cfif count LT cinfo.recordcount>

				 <tr><td height=10></td></tr>
				 <tr><td colspan=3><hr></td></tr>
				 <tr><td height=10></td></tr>
			   </cfif>

           <cfset count = count + 1>


                </cfoutput>

              </cfloop>

           </td></tr>

          </cfif>

		  </table>

		  </div>

		  </a>

     </cfif>

       <!--- Needs --->

       <cfif session.opp_needs is 1>

       <a name="needs">

       <div class="main_box">

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_header" style="padding-bottom: 0px;" colspan=2>Needs</td></tr>
			  <tr><td colspan=10><hr></td></tr>

             <cfif needs.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No Needs were found.</td></tr>
             <cfelse>

             <cfset counter = 0>

        <cfloop query="needs">

         <cfoutput>
         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

			<td width=150 valign=top>

			<cfif needs.need_image is "">
			  <a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#/stock_need.png" style="margin-top: 15px;" width=125 border=0></a>
			<cfelse>
			  <a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#needs.need_image#" style="margin-top: 15px;" width=125></a>
			</cfif>
            </td>

            <td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>

             <td class="feed_sub_header" width=450><b><a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#needs.need_name#</a></b></td>
                 <td align=right width=40>
			     </td>
			</tr>

			</table>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#needs.need_desc#</td></tr>

			<tr><td class="feed_sub_header" colspan=2>Posted From: <u>#needs.hub_name#</u></td></tr>

			<tr><td class="link_small_gray"><b>Keywords - </b><i>#needs.need_keywords#</i></b></td>
			    <td class="feed_sub_header" align=right>Response Due -

			    <cfif #needs.need_date_response# is "">
			     Not provided
			    <cfelse>
			     #dateformat(needs.need_date_response,'mmm dd, yyyy')#
			    </cfif>


			    </td></tr>
			</table>

			</td>

		   </cfoutput>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

            </cfif>

             <tr><td height=20></td></tr>

             </table>

          </div>

          </a>

       </cfif>

      </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

