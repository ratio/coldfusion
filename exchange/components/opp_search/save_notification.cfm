<cfinclude template="/exchange/security/check.cfm">

<cfif usr_notification_type is "Open Opportunities">

<cfquery name="getemail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_email from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfset search_string = #replace(usr_notification_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>


<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into usr_notification
 (
  usr_notification_name,
  usr_notification_email_list,
  usr_notification_keywords,
  usr_notification_type,
  usr_notification_hub_id,
  usr_notification_usr_id,
  usr_notification_contracts,
  usr_notification_sbirs,
  usr_notification_grants,
  usr_notification_awards,
  usr_notification_challenges,
  usr_notification_needs,
  usr_notification_frequency,
  usr_notification_active
 )
 values
 (
  '#usr_notification_name#',
  '#tostring(tobinary(getemail.usr_email))#',
  '#search_string#',
  '#usr_notification_type#',
   #session.hub#,
   #session.usr_id#,
   <cfif isdefined("usr_notification_contracts")>1<cfelse>null</cfif>,
   <cfif isdefined("usr_notification_sbirs")>1<cfelse>null</cfif>,
   <cfif isdefined("usr_notification_grants")>1<cfelse>null</cfif>,
   <cfif isdefined("usr_notification_awards")>1<cfelse>null</cfif>,
   <cfif isdefined("usr_notification_challenges")>1<cfelse>null</cfif>,
   <cfif isdefined("usr_notification_needs")>1<cfelse>null</cfif>,
  '#usr_notification_frequency#',
  1
  )
</cfquery>

<cflocation URL="#return_page#?u=2" addtoken="no">

</cfif>