
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=10></td></tr>
 <tr><td valign=top width=50%>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>
 <form action="/exchange/components/opp_search/set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

 <tr><td class="feed_header">Search for Open Opportunities</td></tr>
 <tr><td height=10></td></tr>
 <tr><td class="feed_sub_header" style="font-weight: normal;">

 <input type="text" class="input_text" placeholder="Type in a keyword..." name="keyword" style="width: 200px; height: 30px;" <cfif isdefined("session.raw_search_string")>value="<cfoutput>#session.raw_search_string#</cfoutput>"</cfif> required>

 <cfoutput>
<b>Posted</b>&nbsp;&nbsp;<select name="timeframe" class="input_select">
  <option value=1 <cfif isdefined("session.opp_timeframe") and session.opp_timeframe is 1>selected</cfif>>Today
  <option value=2 <cfif isdefined("session.opp_timeframe") and session.opp_timeframe is 2>selected</cfif>>Yesterday
  <option value=3 <cfif isdefined("session.opp_timeframe") and session.opp_timeframe is 3>selected</cfif>>This Week
  <option value=4 <cfif isdefined("session.opp_timeframe") and session.opp_timeframe is 4>selected</cfif>>This Month
  <option value=5 <cfif isdefined("session.opp_timeframe") and session.opp_timeframe is 5>selected</cfif>>This Year
  </select>
 </cfoutput>

 &nbsp;<input type="submit" name="button" value="Search" class="button_blue">

 </td></tr>
 </table>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=5></td></tr>
 <tr>
   <td width=30><input type="checkbox" name="search_contracts" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_contracts")><cfif session.opp_contracts is 1>checked</cfif><cfelse>checked</cfif>></td>
   <td class="feed_sub_header">Contracts</td>
   <td width=30><input type="checkbox" name="search_sbirs" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_sbirs")><cfif session.opp_sbirs is 1>checked</cfif><cfelse>checked</cfif>></td>
   <td class="feed_sub_header">SBIR/STTRs</td>
   <td width=30><input type="checkbox" name="search_grants" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_grants")><cfif session.opp_grants is 1>checked</cfif><cfelse>checked</cfif>></td>
   <td class="feed_sub_header">Grants</td>
   <td width=30><input type="checkbox" name="search_challenges" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_challenges")><cfif session.opp_challenges is 1>checked</cfif><cfelse>checked</cfif>></td>
   <td class="feed_sub_header">Challenges</td>
   <td width=30><input type="checkbox" name="search_needs" style="width: 20px; height: 20px; margin-right: 10px;" <cfif isdefined("session.opp_needs")><cfif session.opp_needs is 1>checked</cfif><cfelse>checked</cfif>></td>
   <td class="feed_sub_header">Needs</td>
 </tr>
 </form>
 </table>

</td><td width=20>&nbsp;</td><td valign=top>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr><td class="feed_header">Search Tips</td></tr>
<tr>
	<td class="feed_sub_header" style="font-weight: normal;">To search for opportunities please use a combination of keywords such as "machine learning and health" or
	"health or clinical and Army".  The more specific your search criteria you use, the better your search results will be.
	</td>
</tr>
</table>

</td></tr>
</table>