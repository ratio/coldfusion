<div class="left_box">

<cfquery name="pipe_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_pipeline_id from sharing
 where sharing_hub_id = #session.hub# and
	   sharing_pipeline_id is not null and
	   (sharing_to_usr_id = #session.usr_id#)
</cfquery>

<cfif pipe_access.recordcount is 0>
 <cfset pipe_list = 0>
<cfelse>
 <cfset pipe_list = valuelist(pipe_access.sharing_pipeline_id)>
</cfif>

<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select pipeline_name, pipeline_image, pipeline_id, usr_first_name, usr_last_name, pipeline_updated from pipeline
  left join usr on usr_id = pipeline_usr_id
  where pipeline_hub_id = #session.hub# and
		(pipeline_id in (#pipe_list#) or pipeline_usr_id = #session.usr_id#)
  order by pipeline_name DESC
</cfquery>

	<center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><a href="/exchange/pipeline/"><b>Opportunity Boards</b></a></td>
		      <td align=right valign=top></td></tr>
		  <tr><td><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfset count = 1>

          <cfif pipeline.recordcount is 0>

           <tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunity Boards have been created.</td></tr>

         <cfelse>

		  <cfloop query="pipeline">

			  <cfquery name="item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select count(deal_id) as total from deal
			   where deal_pipeline_id = #pipeline.pipeline_id#
			  </cfquery>

			  <cfoutput>

				   <tr>
					   <td width=30><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

					   <cfif #pipeline.pipeline_image# is "">
						 <img src="#image_virtual#icon_portfolio_company.png" width=18 border=0>
					   <cfelse>
						 <img src="#media_virtual#/#pipeline.pipeline_image#" width=18>
					   </cfif>
					   </a></td>

					   <td class="link_med_blue"><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(pipeline.pipeline_name) GT 20>#left(pipeline.pipeline_name,'20')#...<cfelse>#pipeline.pipeline_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
					</tr>

			  <cfif count is not pipeline.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

			  </cfoutput>

			  </td></tr>

			  </cfloop>

          </cfif>

	</center>

	</table>

</div>