<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i#
</cfquery>

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfif usr.hub_xref_role_id is 1>
	      <cfinclude template="/exchange/portfolio/recent.cfm">
      <cfelse>
	      <cfinclude template="/exchange/profile_company.cfm">
      </cfif>

      </td><td valign=top>

	  <div class="main_box">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">UPDATE TOPIC</td><td align=right class="feed_option"><a href="/exchange/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			  </table>

		   <form action="/exchange/components/home_page/snapshot_company/save.cfm" method="post">

           <cfoutput>

           <cfset keyword = #replace(edit.snapshot_keyword,'"','',"all")#>

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td height=10></td></tr>
              <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Topics are keywords that allow you to filter results.</td></tr>
			  <tr><td height=10></td></tr>

			  <tr><td class="feed_sub_header" width=150>Topic Name</td>
			      <td><input name="snapshot_name" type="text" class="input_text" style="width: 250px;" maxlength="100" value='#edit.snapshot_name#'></td></tr>

			  <tr><td class="feed_sub_header">Keywords</td>
			      <td><input name="snapshot_keyword" type="text" class="input_text" style="width: 500px;" maxlength="100" value='#keyword#' placeholder="i.e., Machine Learning and Healthcare or Clinical"></td></tr>

             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr><td height=10></td></tr>

			 <tr><td></td><td><input type="submit" name="button" class="button_blue_large" value="Update"</td></tr>

             </table>

             <input type="hidden" name="i" value=#i#>

          </cfoutput>

          </form>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

