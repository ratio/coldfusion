<cfinclude template="/exchange/security/check.cfm">

<cfif t is "Procurement">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select pin_id from pin
	 where pin_group_id = #board# and
		   pin_procurement_id = #id# and
		   pin_usr_id = #session.usr_id# and
		   pin_hub_id = #session.hub#
	</cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select fbo_opp_name as name from fbo
	 where fbo_id = #id#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pin
	 (
	  pin_procurement_id,
	  pin_group_id,
	  pin_usr_id,
	  pin_hub_id,
	  pin_date_added,
	  pin_opp_name,
	  pin_source
	 )
	 values
	 (
	  #id#,
	  #board#,
	  #session.usr_id#,
	  #session.hub#,
	  #now()#,
	  '#opp_name.name#',
	  'Procurement'
	  )
	</cfquery>

	</cfif>

	<cflocation URL="index.cfm?u=7" addtoken="no">

<cfelseif t is "grant">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select pin_id from pin
	 where pin_group_id = #board# and
		   pin_grant_id = #id# and
		   pin_usr_id = #session.usr_id# and
		   pin_hub_id = #session.hub#
	</cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select opportunitytitle as name from opp_grant
	 where opp_grant_id = #id#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pin
	 (
	  pin_grant_id,
	  pin_group_id,
	  pin_usr_id,
	  pin_hub_id,
	  pin_date_added,
	  pin_opp_name,
	  pin_source
	 )
	 values
	 (
	  #id#,
	  #board#,
	  #session.usr_id#,
	  #session.hub#,
	  #now()#,
	  '#opp_name.name#',
	  'Grant'
	  )
	</cfquery>

	</cfif>

	<cflocation URL="grants.cfm?u=7" addtoken="no">

<cfelseif t is "challenge">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select pin_id from pin
	 where pin_group_id = #board# and
		   pin_challenge_id = #id# and
		   pin_usr_id = #session.usr_id# and
		   pin_hub_id = #session.hub#
	</cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select challenge_name as name from challenge
	 where challenge_id = #id#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pin
	 (
	  pin_challenge_id,
	  pin_group_id,
	  pin_usr_id,
	  pin_hub_id,
	  pin_date_added,
	  pin_opp_name,
	  pin_source
	 )
	 values
	 (
	  #id#,
	  #board#,
	  #session.usr_id#,
	  #session.hub#,
	  #now()#,
	  '#opp_name.name#',
	  'Challenge'
	  )
	</cfquery>

	</cfif>

	<cflocation URL="challenges.cfm?u=7" addtoken="no">

<cfelseif t is "contract">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select pin_id from pin
	  where pin_group_id = #board# and
	 	    pin_contract_id = #id# and
		    pin_usr_id = #session.usr_id# and
		    pin_hub_id = #session.hub#
	</cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select recipient_name as name from award_data
	 where id = #id#
	</cfquery>

	<cfset contract_name = #opp_name.name# & " Contract">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pin
	 (
	  pin_contract_id,
	  pin_group_id,
	  pin_usr_id,
	  pin_hub_id,
	  pin_date_added,
	  pin_opp_name,
	  pin_source
	 )
	 values
	 (
	  #id#,
	  #board#,
	  #session.usr_id#,
	  #session.hub#,
	  #now()#,
	  '#contract_name#',
	  'Contract'
	  )
	</cfquery>

	</cfif>

	<cflocation URL="/exchange/include/award_information.cfm?id=#id#&u=7" addtoken="no">

</cfif>

