<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 4>
</cfif>

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfquery name="customers" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select distinct(awarding_sub_agency_code), awarding_sub_agency_name, count(distinct(parent_award_id)) as contracts, count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total, orgname_logo from award_data
left join orgname on orgname_name = awarding_sub_agency_name
where contains((award_description),'#trim(opp_snapshot.snapshot_keyword)#') and
	  period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
	  federal_action_obligation > 0
group by awarding_sub_agency_code, awarding_sub_agency_name, orgname_logo

 <cfif sv is 1>
  order by awarding_sub_agency_name ASC
 <cfelseif sv is 10>
  order by awarding_sub_agency_name DESC

 <cfelseif sv is 2>
  order by contracts DESC
 <cfelseif sv is 20>
  order by contracts ASC

 <cfelseif sv is 3>
  order by awards DESC
 <cfelseif sv is 30>
  order by awards ASC

 <cfelseif sv is 4>
  order by total DESC
 <cfelseif sv is 40>
  order by total ASC

 </cfif>


</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">CUSTOMERS (#trim(numberformat(customers.recordcount,'999,999'))#)</td>
               <td class="feed_sub_header" align=right>

                 <a href="/exchange/">Return</a>

               </td></tr>

           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Customers are organizations that have purchased products, services or technologies for the capability selected and/or are paying to implement them over the next 2 years.</td></tr>

           <tr><td colspan=2><hr></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif customers.recordcount is 0>
           <tr><td class="feed_sub_header">No Customers were found.</td></tr>
          <cfelse>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>


			  <tr height=40>
			     <td></td>
				 <td class="feed_sub_header"><a href="view35.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>CUSTOMER</b></a></td>
				 <td class="feed_sub_header" align=center><a href="view35.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CONTRACTS</b></a></td>
				 <td class="feed_sub_header" align=center><a href="view35.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AWARDS</b></a></td>
				 <td class="feed_sub_header" align=right><a href="view35.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AMOUNT</b></a></td>
		      </tr>


          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="CUSTOMERS">

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

			   <cfoutput>


					 <td width=60 class="table_row" valign=middle>

					<cfif #orgname_logo# is "">
					  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
					<cfelse>
					  <img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
					</cfif>

					 </td>

				   <td class="feed_sub_header"><b>#customers.awarding_sub_agency_name#</b></a></td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#customers.contracts#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#customers.awards#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(customers.total,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

          </cfloop>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>