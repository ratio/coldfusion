<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfquery name="snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pingroup
 where pingroup_usr_id = #session.usr_id# and
       pingroup_hub_id = #session.hub#
 order by pingroup_name
</cfquery>

<body class="body">

<cfset opp_count = 1>
<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

<cfquery name="fbo_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(fbo_solicitation_number)) as total from fbo
 where
 (
  contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_agency, fbo_naics_code, fbo_contract_award_name),'#trim(snapshot.snapshot_keyword)#')
 )

 and fbo_inactive_date_updated > '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics_code
 left join orgname on orgname_name = fbo_agency
 where (contains((fbo_opp_name, fbo_desc),'#trim(snapshot.snapshot_keyword)#') )
 and fbo_inactive_date_updated > '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')

<cfif isdefined("sv")>

 <cfif sv is 1>
  order by fbo_solicitation_number ASC
 <cfelseif sv is 10>
  order by fbo_solicitation_number DESC
 <cfelseif sv is 2>
  order by fbo_agency ASC
 <cfelseif sv is 20>
  order by fbo_agency DESC
 <cfelseif sv is 3>
  order by fbo_office ASC
 <cfelseif sv is 30>
  order by fbo_office DESC
 <cfelseif sv is 4>
  order by fbo_location ASC
 <cfelseif sv is 40>
  order by fbo_location DESC
 <cfelseif sv is 5>
  order by fbo_opp_name ASC
 <cfelseif sv is 50>
  order by fbo_opp_name DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics_code ASC
 <cfelseif sv is 80>
  order by fbo_naics_code DESC
 <cfelseif sv is 9>
  order by fbo_setaside_original ASC
 <cfelseif sv is 90>
  order by fbo_setaside_original DESC
 <cfelseif sv is 10>
  order by fbo_pub_date_updated ASC
 <cfelseif sv is 100>
  order by fbo_pub_date_updated DESC
 </cfif>
<cfelse>
order by fbo_pub_date_updated DESC
</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">OPPORTUNITIES (#ltrim(numberformat(fbo_count.total,'999,999'))# Unique)</td>
				 <td class="feed_sub_header" align=right>
				 <a href="view10.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 hspace=10 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="view10.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>

				 <cfif isdefined("l")>
				  <cfif l is 2>
				   &nbsp;|&nbsp;<a href="/exchange/">Return</a>
				  </cfif>
				 </cfif>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAME / TITLE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>SOLICIATION ##</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DEPARTMENT</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AGENCY</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" width=50><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>TYPE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>SET ASIDE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 9><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 90><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" width=110 align=right><a href="view10.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>POSTED</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 10><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 100><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfloop query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=40>
         <cfelse>
          <tr bgcolor="e0e0e0" height=40>
         </cfif>

             <cfoutput>

			 <td width=70 class="table_row" valign=middle>

             <a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">
            <cfif #orgname_logo# is "">
              <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
            <cfelse>
              <img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
            </cfif>
			 </a>

			 </td>
             <td class="feed_sub_header"><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo_opp_name#</a></b></td>
             <td class="feed_sub_header" style="font-weight: normal;">#class_code_name#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=middle width=200><cfif len(fbo_solicitation_number) GT 50>#left(fbo_solicitation_number,'50')#...<cfelse>#fbo_solicitation_number#</cfif></td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=middle width=100>#fbo_dept#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=middle width=100>#fbo_agency#</td>
             <td class="feed_sub_header" style="font-weight: normal;">#fbo_notice_type#</td>
             <td class="feed_sub_header" style="font-weight: normal;" width=75><cfif fbo_setaside_original is "">Not specified<cfelse>#fbo_setaside_original#</cfif></td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right width=75>#dateformat(fbo_pub_date_updated,'mm/dd/yyyy')#</td>

             </cfoutput>

				 <td align=right width=50>

					<div class="dropdown">
					  <img src="/images/icon_pin.png" style="cursor: pointer;" width=30>

					  <div class="dropdown-content" style="width: 275px; text-align: left; right: 20;">
						<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pin to Board</b><br><br>
						<cfoutput query="boards">
							<a href="pintoboard.cfm?id=#agencies.fbo_id#&board=#boards.pingroup_id#&t=procurement&l=dashboard"><i class="fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;&nbsp;#boards.pingroup_name#</a>
						</cfoutput>
					  </div>
					</div>

				 </td>
         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray">Opportunities include sources sought, pre-solicitations and solicitations that have not been awarded and the archive date has not expired.</td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>