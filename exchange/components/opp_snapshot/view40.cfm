<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="needs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from need
 where need_public = 1
 and contains((*),'"#trim(ky)#"')
</cfquery>

<cfquery name="cinfo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from challenge
 where challenge_public = 1
 and challenge_end >= '#dateformat(now(),'mm/dd/yyyy')#'
 and contains((*),'"#trim(ky)#"')
</cfquery>

<cfset needs_total = 0>
<cfset challenges_total = 0>

<cfset demand_total = evaluate(needs.recordcount + cinfo.recordcount)>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">#ucase(ky)# DEMAND (#trim(numberformat(demand_total,'999,999'))#)</td>
               <td class="feed_option" align=right><img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
           <tr><td colspan=2><hr></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=10></td></tr>
          <tr><td class="feed_header" colspan=3>CHALLENGES</a></td></tr>
          <tr><td height=10></td></tr>

          <cfif cinfo.recordcount is 0>
           <tr><td class="feed_sub_header">No Challenges were found.</td></tr>
          <cfelse>

           <cfloop query="cinfo">

			   <cfoutput>

               <tr>

				<td valign=top width=170>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td align=center>

				<cfif cinfo.challenge_type_id is 1>

					<cfif cinfo.challenge_image is "">
					  <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/stock_challenge.png" vspace=10 width=125 border=0></a>
					<cfelse>

					 <cfif left(cinfo.challenge_image,4) is "http">

					  <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#cinfo.challenge_image#" vspace=10 width=150 border=0></a>

					 <cfelse>

					  <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="http://www.challenge.gov#cinfo.challenge_image#" vspace=10 width=150 border=0></a>

					 </cfif>

					</cfif>

				<cfelse>

					<cfif cinfo.challenge_image is "">
					  <a href="/exchange/opps/challenge_detail.cfm?challenge_id=#cinfo.challenge_id#"><img src="/images/stock_challenge.png" vspace=10 width=125 border=0></a>
					<cfelse>
					  <a href="/exchange/opps/challenge_detail.cfm?challenge_id=#cinfo.challenge_id#"><img src="#media_virtual#/#cinfo.challenge_image#" vspace=10 width=125 border=0></a>
					</cfif>

				</cfif>

			    </td></tr>

			    <tr><td class="feed_sub_header" align=center>
			     <cfif #cinfo.challenge_end# is "">
			      Open
			     <cfelse>
					 <cfif #cinfo.challenge_end# GTE #dateformat(now(),'mm/dd/yyyy')#>
					  Open
					 <cfelse>
					  Closed
					 </cfif>
			     </cfif>
			     </td></tr>

			    </table>

			    </td>

			    <td width=30>&nbsp;</td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_header">

                <cfif cinfo.challenge_type_id is 1>
	                <a href="#challenge_url#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(cinfo.challenge_name)#</b></a>
                <cfelse>
	                <a href="/exchange/opps/challenge_detail.cfm?challenge_id=#cinfo.challenge_id#"><b>#ucase(cinfo.challenge_name)#</b></a>
                </cfif>

                  <td class="feed_sub_header" align=right width=150>

                    Prize:
                    <cfif #cinfo.challenge_total_cash# is not "">

						<cfif cinfo.challenge_currency is "$">$
						<cfelseif cinfo.challenge_currency is "I">INR
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelse>
						</cfif>

                      #numberformat(cinfo.challenge_total_cash,'999,999,999')#
                    <cfelse>
                      Not Provided
                    </cfif>

                  </td></tr>


                <cfif cinfo.challenge_organization is not "">
                  <tr><td class="feed_sub_header" colspan=2><b><cfif #cinfo.challenge_annoymous# is 1>SPONSOR: PRIVATE ORGANIZATION	<cfelse>SPONSOR: #ucase(cinfo.challenge_organization)#</cfif></b></td></tr>
                </cfif>

                </td>

                <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#cinfo.challenge_desc#</td></tr>

                <cfif cinfo.challenge_reward is not "">
	                <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>Reward - </b>#cinfo.challenge_reward#</td></tr>
                </cfif>

                <tr><td class="link_small_gray"><b>Keywords&nbsp;: </b><cfif cinfo.challenge_keywords is "">Not Defined<cfelse>#lcase(cinfo.challenge_keywords)#</cfif>&nbsp;&nbsp;|&nbsp;&nbsp;<b>Close Date&nbsp;:&nbsp;</b><cfif cinfo.challenge_end is "">Unknown<cfelse>#dateformat(cinfo.challenge_end,'mmm dd, yyyy')#</cfif></td></tr>
		        </table>

		        </td></tr>

             <tr><td height=10></td></tr>
             <tr><td colspan=7><hr></td></tr>
             <tr><td height=10></td></tr>

		   </cfoutput>

          </cfloop>

          </cfif>

		  </table>

	  </div>


<!--- Needs --->

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=10></td></tr>
          <tr><td class="feed_header" colspan=3>NEEDS</a></td></tr>
          <tr><td height=10></td></tr>

          <cfif needs.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Future Needs were found.</td></tr>
          <cfelse>

           <cfloop query="needs">

           <cfoutput>

           <tr><td width=200><a href="/exchange/opps/need_detail.cfm?need_id=#needs.need_id#&l=1" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/stock_need.png" width=100 border=0></a></td>
               <td>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
			    <tr><td class="feed_sub_header"><a href="/exchange/opps/need_detail.cfm?need_id=#needs.need_id#&l=1" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(needs.need_name)#</b></a></td></tr>
			    <tr><td class="feed_sub_header" style="font-weight: normal;">#needs.need_desc#</b></td></tr>
			   </table>

			   <td></tr>

			   <cfset needs_total = needs_total+1>

			   <cfif needs.recordcount GT needs_total>
				   <tr><td></td><td colspan=2><hr></td></tr>
			   </cfif>

  			</cfoutput>

           </cfloop>

          </cfif>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>