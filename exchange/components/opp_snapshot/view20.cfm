<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfset value_count = 1>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select
 distinct(award_id_piid), recipient_name, recipient_duns, max(period_of_performance_current_end_date) as max_date, count(modification_number) as mods, max(period_of_performance_current_end_date) as date, awarding_agency_name, awarding_sub_agency_name, max(potential_total_value_of_award) as potential_value, sum(base_and_all_options_value) as all_options, max(current_total_value_of_award) as current_value, sum(federal_action_obligation) as obligated, sum(base_and_exercised_options_value) as options, orgname_logo from award_data
 left join orgname on orgname_name = awarding_sub_agency_name
 where contains((award_description),'#trim(opp_snapshot.snapshot_keyword)#') and

 <cfif type is 1>
  action_date between '#dateformat(evaluate(now()-180),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#'
 <cfelseif type is 2>
  period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#'
 <cfelseif type is 3>
  period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#'
 <cfelseif type is 4>
  period_of_performance_current_end_date between '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#'
 <cfelseif type is 5>
  period_of_performance_current_end_date between '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#'
 </cfif>

 group by award_id_piid, recipient_name, recipient_duns, period_of_performance_current_end_date, awarding_agency_name, awarding_sub_agency_name, orgname_logo

 <cfif sv is 1>
  order by max_date ASC
 <cfelseif sv is 10>
  order by max_date DESC

 <cfelseif sv is 2>
  order by award_id_piid ASC
 <cfelseif sv is 20>
  order by award_id_piid DESC

 <cfelseif sv is 3>
  order by mods ASC
 <cfelseif sv is 30>
  order by mods DESC

 <cfelseif sv is 4>
  order by awarding_sub_agency_name ASC
 <cfelseif sv is 40>
  order by awarding_sub_agency_name DESC

 <cfelseif sv is 5>
  order by obligated DESC
 <cfelseif sv is 50>
  order by obligated ASC

 <cfelseif sv is 6>
  order by options DESC
 <cfelseif sv is 60>
  order by options ASC

 <cfelseif sv is 7>
  order by all_options DESC
 <cfelseif sv is 70>
  order by all_options ASC

 <cfelseif sv is 8>
  order by current_value DESC
 <cfelseif sv is 80>
  order by current_value ASC

 <cfelseif sv is 9>
  order by potential_value DESC
 <cfelseif sv is 90>
  order by potential_value ASC

 <cfelseif sv is 11>
  order by recipient_name ASC
 <cfelseif sv is 110>
  order by recipient_name DESC


</cfif>


</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="URL.PageId" default="0">
<cfset RecordsPerPage = 250>
<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
<cfset EndRow = StartRow+RecordsPerPage-1>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">EXPIRING CONTRACTS - (

           <cfif type is 1>
             Last 180 Days
           <cfelseif type is 2>
             Next 90 Days
           <cfelseif type is 3>
             3-	12 Months
           <cfelseif type is 4>
             6-12 Months
           <cfelseif type is 5>
             1-2 Years
           </cfif>
           )

           </td>
               <td class="feed_sub_header" align=right>

               <a href="view20.cfm?i=#i#&type=#type#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" title="Export to Excel" hspace=10></a><a href="view20.cfm?i=#i#&type=#type#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
               &nbsp;|&nbsp;
               <a href="/exchange/" addtoken="no">Return</a>

               </td></tr>

           <tr><td colspan=2><hr></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" colspan=16>No opportunities were found.</td></tr>
          <cfelse>

          <tr>
              <td class="feed_sub_header">

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
              </td>
              <td class="feed_sub_header" align=right>


              </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
			     <td></td>
				 <td class="feed_option"><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Department / Agency</b></a></td>
				 <td class="feed_option"><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Incumbent</b></a></td>
				 <td class="feed_option"><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Award ##</b></a></td>
				 <td class="feed_option" align=center><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Actions</b></a></td>
				 <td class="feed_option"><b>Award Description</b></td>
				 <td class="feed_option" align=right><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Obligated</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>With Options</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>All Options</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Current Value</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Potential Value</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>End Date</b></a></td>

			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

            <cfquery name="award" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
             select top(1) award_description from award_data
             where award_id_piid = '#agencies.award_id_piid#'
             order by action_date ASC
            </cfquery>

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=50>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=50>
			   </cfif>

			   <cfoutput>



				 <td width=70 class="table_row" valign=top>

                <a href="/exchange/include/find_award.cfm?award_id=#agencies.award_id_piid#" target="_blank" rel="noopener" rel="noreferrer">
                <cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>

				 </td>



				   <td class="feed_option" style="font-weight: normal;" width=350 valign=top>#agencies.awarding_agency_name#<br>#agencies.awarding_sub_agency_name#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#recipient_name#</b></a></td>

				   <td class="feed_option" valign=top><a href="/exchange/include/find_award.cfm?award_id=#agencies.award_id_piid#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.award_id_piid#</b></a></td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=center width=75>#mods#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;">#award.award_description#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(options,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(all_options,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(current_value,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(potential_value,'$999,999,999')#</td>
				   <td class="feed_option" valign=top width=75 align=right>#dateformat(date,'mm/dd/yyyy')#</td>

			   </cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

			  </cfif>

			  <cfif CurrentRow eq EndRow>
			   <cfbreak>
			  </cfif>

          </cfloop>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>