<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="opp_snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #i# and
        snapshot_usr_id = #session.usr_id# and
        snapshot_hub_id = #session.hub#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 8>
</cfif>

<cfquery name="partners" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select distinct(recipient_duns), recipient_state_code, recipient_city_name, recipient_name, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(parent_award_id)) as contracts, count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total, sum(potential_total_value_of_award) as potential, company_website, company_logo from award_data
left join company on company_duns = recipient_duns
where contains((award_description),'#trim(opp_snapshot.snapshot_keyword)#') and
	  period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
	  federal_action_obligation > 0
group by recipient_duns, recipient_name, recipient_state_code, recipient_city_name, company_website, company_logo

 <cfif sv is 1>
  order by recipient_name ASC
 <cfelseif sv is 10>
  order by recipient_name DESC

 <cfelseif sv is 2>
  order by recipient_duns ASC
 <cfelseif sv is 20>
  order by recipient_duns DESC

 <cfelseif sv is 3>
  order by recipient_city_name ASC
 <cfelseif sv is 30>
  order by recipient_city_name DESC

 <cfelseif sv is 4>
  order by recipient_state_code ASC, recipient_city_name ASC
 <cfelseif sv is 40>
  order by recipient_state_code DESC, recipient_city_name ASC

 <cfelseif sv is 5>
  order by agencies DESC
 <cfelseif sv is 50>
  order by agencies ASC

 <cfelseif sv is 6>
  order by contracts DESC
 <cfelseif sv is 60>
  order by contracts ASC

 <cfelseif sv is 7>
  order by awards DESC
 <cfelseif sv is 70>
  order by awards ASC

 <cfelseif sv is 8>
  order by total DESC
 <cfelseif sv is 80>
  order by total ASC

 <cfelseif sv is 9>
  order by potential DESC
 <cfelseif sv is 90>
  order by potential ASC
 </cfif>


</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">POTENTIAL PARTNERS</td>
               <td class="feed_sub_header" align=right>

               <a href="/exchange/">Return</a>


               </td></tr>

           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Partners are companies that have won contracts or task orders and are currently delivering this capability to customers.  Partners are also companies who are seeking new and innovative products, services, technologies and approaches to complement their work and differentiate themselves on future procurements.</td></tr>

           <tr><td colspan=2><hr></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif partners.recordcount is 0>
           <tr><td class="feed_sub_header">No partners were found.</td></tr>
          <cfelse>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
			     <td></td>
				 <td class="feed_sub_header"><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>PARTNER</b></a></td>
				 <td class="feed_sub_header"><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DUNS</b></a></td>
				 <td class="feed_sub_header"><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>CITY</b></a></td>
				 <td class="feed_sub_header" align=center><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>STATE</b></a></td>
				 <td class="feed_sub_header" align=center><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AGENCIES</b></a></td>
				 <td class="feed_sub_header" align=center><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>CONTRACTS</b></a></td>
				 <td class="feed_sub_header" align=center><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>AWARDS</b></a></td>
				 <td class="feed_sub_header" align=right><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>OBLIGATED</b></a></td>
				 <td class="feed_sub_header" align=right><a href="view30.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>POTENTIAL VALUE</b></a></td>
		      </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="partners">

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=40>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=40>
			   </cfif>

			   <cfoutput>

                    <td width=50>
                    <a href="set.cfm?i=#i#&duns=#partners.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif partners.company_logo is "">
					  <img src="//logo.clearbit.com/#partners.company_website#" width=30 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
					  <img src="#media_virtual#/#partners.company_logo#" width=30 border=0>
					</cfif>
					</a>
					</td>




				   <td class="feed_sub_header"><a href="set.cfm?i=#i#&duns=#partners.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#partners.recipient_name#</b></a></td>
				   <td class="feed_sub_header" style="font-weight: normal;">#partners.recipient_duns#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#partners.recipient_city_name#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#partners.recipient_state_code#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#partners.agencies#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#partners.contracts#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=center>#partners.awards#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(partners.total,'$999,999,999')#</td>
				   <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(partners.potential,'$999,999,999')#</td>
				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

          </cfloop>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>