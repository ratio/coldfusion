<cfinclude template="/exchange/security/check.cfm">

<cfquery name="res" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from resource
 where resource_cat_id = #resource_cat_id#
 order by resource_order
</cfquery>

<cfquery name="cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from resource_category
 where resource_category_id = #resource_cat_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		   <tr><td class="feed_header">Resources</td></tr>
 		  </table>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td valign=top width="200">

		   <cfinclude template="resource_menu.cfm">

  		   </td><td valign=top width=50>&nbsp;</td><td valign=top>

 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		     <tr><td>&nbsp;</td></tr>
 		     <cfoutput>
				 <tr><td class="feed_header"><b>#cat.resource_category_name#</b></td></tr>
				 <cfif #cat.resource_category_desc# is not "">
					 <tr><td class="feed_option">#cat.resource_category_desc#</td></tr>
				 </cfif>
             </cfoutput>
             <tr><td height=10></td></tr>

             <cfif #res.recordcount# is 0>

             <tr><td class="feed_option">No resources exist for this category.</td></tr>

             <cfelse>

				 <cfoutput query="res">
				   <tr><td class="feed_sub_header">#res.resource_name#</td></tr>
				   <tr><td class="feed_option">#replace(res.resource_desc,"#chr(10)#","<br>","all")#</td></tr>
				   <cfif #res.resource_url# is not "">
				   <tr><td class="feed_option"><font size=2><a href="#res.resource_url#" target="_blank" rel="noopener" rel="noreferrer">#res.resource_url#</a></font></td></tr>
				   </cfif>
				   <cfif #res.resource_file# is not "">
					 <tr><td class="feed_option"><font size=2><a href="#media_virtual#/#resource_file#" target="_blank" rel="noopener" rel="noreferrer">Download Attachement</a></font></td></tr>
				   </cfif>
				   <tr><td><hr></td></tr>
				 </cfoutput>

             </cfif>

            </table>

  		   </td></tr>

 		  </table>

 		  <!--- End of the Workbench --->

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

