<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.hub")>

	<cfquery name="res" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select top(10) * from resource
	 where resource_hub_id = #session.hub#
	 order by resource_created DESC
	</cfquery>

<cfelse>

	<cfquery name="res" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select top(10) * from resource
	 where resource_hub_id is null
	 order by resource_created DESC
	</cfquery>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		   <tr><td class="feed_header">Resources</td></tr>
 		  </table>

 		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td valign=top width="200">

		   <cfinclude template="resource_menu.cfm">

  		   </td><td valign=top width=50>&nbsp;</td><td valign=top>

 		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		     <tr><td>&nbsp;</td></tr>
             <tr><td class="feed_header"><b>Latest Resources Posted</b></td></tr>
             <tr><td height=10></td></tr>

             <cfif #res.recordcount# is 0>

	             <tr><td class="feed_option">No resources found.</td></tr>

             <cfelse>

             <tr><td height=10></td></tr>

				 <cfoutput query="res">
				   <tr><td class="feed_sub_header"><b>#res.resource_name#</b></td></tr>
				   <tr><td class="feed_option">#res.resource_desc#</td></tr>
				   <cfif #res.resource_url# is not "">
				   <tr><td class="feed_option"><a href="#res.resource_url#" target="_blank" rel="noopener" rel="noreferrer">#res.resource_url#</a></td></tr>
				   </cfif>
				   <cfif #res.resource_file# is not "">
					 <tr><td class="feed_option"><a href="#media_virtual#/#resource_file#" target="_blank" rel="noopener" rel="noreferrer">Download Attachement</a></td></tr>
				   </cfif>
				   <tr><td><hr></td></tr>
				 </cfoutput>

             </cfif>

            </table>

  		   </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

