<cfif isdefined("session.hub")>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from resource_category
	 where resource_category_hub_id = #session.hub#
	 order by resource_category_order
	</cfquery>

<cfelse>

	<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from resource_category
	 where resource_category_hub_id is null
	 order by resource_category_order
	</cfquery>

</cfif>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td>&nbsp;</td></tr>
 <tr><td class="feed_header" colspan=2><b>Categories</b></td></tr>
 <tr><td height=10></td></tr>

     <tr><td width=50><a href="/exchange/resources/"><img src="/images/resource_home.png" width=30 border=0></a></td>
         <td class="feed_option"><b><a href="index.cfm">Home</a></b></td></tr>

	 <cfloop query="cats">

		<cfquery name="resources" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(resource_id) as total from resource
		 where resource_cat_id = #cats.resource_category_id#
		</cfquery>

	  <cfoutput>

	  <tr><td class="feed_option">
	  <cfif #cats.resource_category_icon# is "">
	   <a href="/exchange/resources/resources.cfm?resource_cat_id=#cats.resource_category_id#"><img src="/images/resource_folder.png" width=30 border=0></a>
	  <cfelse>
	   <a href="/exchange/resources/resources.cfm?resource_cat_id=#cats.resource_category_id#"><img src="/images/#cats.resource_category_icon#" width=30 border=0></a>
	  </cfif>
	  </td>

	  <td class="feed_option">

	  <a href="resources.cfm?resource_cat_id=#cats.resource_category_id#"><b>#cats.resource_category_name#&nbsp;(#resources.total#)</b></a>

	  </td></tr>

	 </cfoutput>

 </cfloop>

</table>