<cfinclude template="/exchange/security/check.cfm">


<cfif button is "Associate With A Different Company">

    <cflocation URL="/exchange/company/setup.cfm" addtoken="no">

<cfelseif button is "Remove Company Affiliation">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select company_id from company
	 where company_admin_id = #session.usr_id# and
	       company_id = #session.company_id#
	</cfquery>

	<cfif #check.recordcount# is 1>

		<cfquery name="remove1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update company
		 set company_admin_id = null
		 where company_id = #session.company_id#
		</cfquery>

    </cfif>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update usr
	 set usr_company_id = null
	 where usr_id = #session.usr_id#
	</cfquery>

	<cfset #session.company_id# = 0>

</cfif>

<cflocation URL="/exchange/profile/index.cfm?u=9" addtoken="no">



