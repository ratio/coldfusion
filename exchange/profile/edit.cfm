<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="state" datasource="#lake_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

  <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from market
   where market_hub_id = #session.hub#
  </cfquery>

  <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from sector
   where sector_hub_id = #session.hub#
   order by sector_name
  </cfquery>

  <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from topic
   where topic_hub_id = #session.hub#
   order by topic_name
  </cfquery>

	  <cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where align_usr_id = #session.usr_id# and
	         align_hub_id = #session.hub# and
			 align_type_id = 2
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where align_usr_id = #session.usr_id# and
	         align_hub_id = #session.hub# and
	         align_type_id = 3
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where align_usr_id = #session.usr_id# and
	         align_hub_id = #session.hub# and
	         align_type_id = 4
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

<style>
.filecontainer {
    overflow: hidden;
    position: relative;
}
.filecontainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    padding-bottom: 20px;
    vertical-align: text-bottom;
    opacity: 0;
    position: absolute;
    right: 0;
    cursor: pointer;
    text-align: right;
    top: 0;
}
</style>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Update Profile</td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

			   </table>

               <form action="save.cfm" method="post" enctype="multipart/form-data" >

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td class="feed_sub_header">First Name</td>
				       <td><input type="text" name="usr_first_name" class="input_text" size=25 value="#profile.usr_first_name#" maxlength="50" required></td></tr>
			       <tr><td class="feed_sub_header">Last Name</td>
                       <td><input type="text" name="usr_last_name" class="input_text" size=35 value="#profile.usr_last_name#" maxlength="50" required></td></tr>
				   <tr><td class="feed_sub_header" valign=top>Profile Picture</td>

					<cfif #profile.usr_photo# is "">
					  <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="usr_photo" accept="image/*"></td></tr>
					<cfelse>
					  <td class="feed_sub_header" style="font-weight: normal;" valign=middle><img src="#media_virtual#/#profile.usr_photo#" width=100><br><br>

					      Change Photo<br><br>
						  <input type="file" name="usr_photo" accept="image/*">
						  <br><br>

					      <input type="checkbox" name="remove_photo" style="width: 20px; height: 20px;" valign=top>&nbsp;&nbsp;or, check to remove photo</td></tr>
					 </cfif>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				   <tr><td class="feed_sub_header">Company Name</td>
                       <td class="feed_option"><input type="text" name="usr_company_name" class="input_text" style="width: 300px;" value="#profile.usr_company_name#" maxlength="50"></td></tr>
				   <tr><td class="feed_sub_header">Email</td>
                       <td class="feed_option"><input type="email" name="usr_email" class="input_text" style="width: 300px;" value="#tostring(tobinary(profile.usr_email))#" maxlength="50" required></td></tr>
				   <tr><td class="feed_sub_header">Title</td>
                       <td class="feed_option"><input type="text" name="usr_title" class="input_text" style="width: 300px;" value="#profile.usr_title#" maxlength="50"></td></tr>
  		           <tr><td class="feed_sub_header">Address</td>
                       <td class="feed_option"><input type="text" name="usr_address_line_1" class="input_text" style="width: 400px;" value="#profile.usr_address_line_1#" maxlength="100"></td></tr>
                   <tr><td class="feed_sub_header"></td>
                       <td class="feed_option"><input type="text" name="usr_address_line_2" class="input_text" style="width: 400px;" value="#profile.usr_address_line_2#" maxlength="100"><br>
                   <tr><td class="feed_sub_header"></td>
                       <td class="feed_option"><input type="text" name="usr_city"  class="input_text" style="width: 220px;" value="#profile.usr_city#" maxlength="50">&nbsp;&nbsp;


                   </cfoutput>

                   <select name="usr_state" class="input_select">
                   <option value=0>State
                   <cfoutput query="state">
                   <option value="#state_abbr#" <cfif #profile.usr_state# is #state_abbr#>selected</cfif>>#state_name#
                   </cfoutput>
                   </select>
                   &nbsp;&nbsp;

                   <cfoutput>

                   <input type="text" name="usr_zip" class="input_text" size=10 value="#profile.usr_zip#" maxlength="10">

                   </td></tr>

			       <tr><td class="feed_sub_header">Work Phone</td>
                       <td><input type="tel" class="input_text" name="usr_phone" size=20 value="#profile.usr_phone#" maxlength="20"></td></tr>

			       <tr><td class="feed_sub_header">Cell Phone</td>
                       <td><input type="tel" class="input_text" name="usr_cell_phone" size=20 value="#profile.usr_cell_phone#" maxlength="20"></td></tr>

		        <tr><td class="feed_sub_header">LinkedIn Profile (URL)</td>
                    <td><input type="url" class="input_text" name="usr_linkedin" size=70 value="#profile.usr_linkedin#" maxlength="200"></td></tr>

		        <tr><td class="feed_sub_header">Facebook Profile (URL)</td>
                    <td><input type="url" class="input_text" name="usr_facebook" size=70 value="#profile.usr_facebook#" maxlength="200"></td></tr>

		        <tr><td class="feed_sub_header">Twitter Profile (URL)</td>
                    <td><input type="url" class="input_text" name="usr_twitter" size=70 value="#profile.usr_twitter#" maxlength="200"></td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                </cfoutput>

                <tr><td class="feed_sub_header" valign=top>Interests</td>
                    <td valign=top>

			<table cellspacing=0 cellpadding=0 border=0>

				<tr><td class="feed_sub_header"><b>Markets</b></td>
					<td class="feed_sub_header"><b>Accounts / Sectors</b></td>
					<td class="feed_sub_header"><b>Capabilities and Services</b></td>
				</tr>

			<tr>

				 <td width=200>
					  <select class="input_select" name="market_id" style="width: 150px; height: 200px;" multiple>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=400>
					  <select class="input_select" style="width: 350px; height: 200px;" name="sector_id" multiple>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select class="input_select" style="width: 350px; height: 200px;" name="topic_id" multiple>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
                <tr><td class="link_small_gray" colspan=2>To select multiple items please use the SHIFT or CTRL key.</td></tr>

			  </table>

                    </td></tr>

                <cfoutput>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>


			    <tr><td class="feed_sub_header">My Keywords</td>
				    <td><input type="text" name="usr_keywords" class="input_text" size=70 value="#profile.usr_keywords#" maxlength="300"></td></tr>
                <tr><td></td><td class="link_small_gray">Please seperate keywords with a comma (i.e., Machine Learning, Clinical, etc.)</td></tr>

                <tr><td height=10></td></tr>

			    <tr><td class="feed_sub_header" valign=top>About Me</td>
                    <td><textarea name="usr_about" class="input_textarea" rows="6" cols="120">#profile.usr_about#</textarea></td></tr>

			    <tr><td class="feed_sub_header" valign=top>Experience</td>
                    <td><textarea name="usr_experience" class="input_textarea" rows="6" cols="120">#profile.usr_experience#</textarea></td></tr>

			    <tr><td class="feed_sub_header" valign=top>Education</td>
                    <td><textarea name="usr_education" class="input_textarea" rows="6" cols="120">#profile.usr_education#</textarea></td></tr>

			    <tr><td class="feed_sub_header" valign=top>Hobbies</td>
                    <td><textarea name="usr_hobbies" class="input_textarea" rows="6" cols="120">#profile.usr_hobbies#</textarea></td></tr>


                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>


			       <tr><td class="feed_sub_header" valign=top><b>Profile Privacy</b></td>
                       <td class="feed_option"><select name="usr_profile_display" class="input_select">
                    <option value=1 <cfif #profile.usr_profile_display# is 1>selected</cfif>>Public
                    <option value=2 <cfif #profile.usr_profile_display# is 2>selected</cfif>>Private
                    </select><br><br><b>Private</b> - Profile information is not displayed to users<br><b>Public</b> - Profile information is displayed to users.

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>


			       <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update Profile" vspace=10></td></tr>

			   </table>

               <cfif isdefined("l")>
                <input type="hidden" name="l" value=1>
               </cfif>

			   </form>

			   </table>

              </td></tr>

           </table>

 		  </div>

         </cfoutput>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>