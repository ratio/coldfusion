<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update Profile">

        <cfif isdefined("remove_photo")>

			<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select usr_photo from usr
			  where usr_id = #session.usr_id#
			</cfquery>

            <cfif FileExists("#media_path#\#remove.usr_photo#")>
        	 <cffile action = "delete" file = "#media_path#\#remove.usr_photo#">
        	</cfif>

        </cfif>

		<cfif #usr_photo# is not "">

			<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select usr_photo from usr
			  where usr_id = #session.usr_id#
			</cfquery>

			<cfif #getfile.usr_photo# is not "">
             <cfif FileExists("#media_path#\#getfile.usr_photo#")>
 			  <cffile action = "delete" file = "#media_path#\#getfile.usr_photo#">
 			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "usr_photo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

    <cfset usr_full_name = #usr_first_name# & " " & #usr_last_name#>

    <cftransaction>

	<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update usr

	  set usr_first_name = '#usr_first_name#',
	      usr_company_name = '#usr_company_name#',
	      usr_full_name = '#usr_full_name#',
	      usr_last_name = '#usr_last_name#',
	      usr_keywords = '#usr_keywords#',
	      usr_about = '#usr_about#',
	      usr_experience = '#usr_experience#',
	      usr_hobbies = '#usr_hobbies#',
	      usr_education = '#usr_education#',
	      usr_profile_display = #usr_profile_display#,
	      usr_title = '#usr_title#',
		  <cfif #usr_photo# is not "">
		   usr_photo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_photo")>
		   usr_photo = null,
		  </cfif>
	      usr_address_line_1 = '#usr_address_line_1#',
	      usr_address_line_2 = '#usr_address_line_2#',
	      usr_city = '#usr_city#',
	      usr_zip = '#usr_zip#',
	      usr_cell_phone = '#usr_cell_phone#',
	      usr_phone = '#usr_phone#',
	      usr_linkedin = '#usr_linkedin#',
	      usr_facebook = '#usr_facebook#',
	      usr_twitter = '#usr_twitter#',
	      usr_state = <cfif #usr_state# is 0>null<cfelse>'#usr_state#'</cfif>,
	      usr_email = '#tobase64(lcase(usr_email))#'
	  where usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (2,3,4)) and
	       (align_usr_id = #session.usr_id#)
	       <cfif isdefined("session.hub")>
		       and (align_hub_id = #session.hub#)
	       <cfelse>
	           and (align_hub_id is null)
	       </cfif>
	</cfquery>

	<cfif isdefined("market_id")>

		<cfloop index="m_element" list=#market_id#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 2, #m_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sector_id")>

		<cfloop index="s_element" list=#sector_id#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 3, #s_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topic_id")>

		<cfloop index="t_element" list=#topic_id#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 4, #t_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	</cftransaction>

</cfif>

<cflocation URL="/exchange/profile/index.cfm?u=6" addtoken="no">