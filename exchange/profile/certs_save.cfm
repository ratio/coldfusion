<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cftransaction>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete usr_cert
		 where usr_cert_hub_id = #session.hub# and
		       usr_cert_usr_id = #session.usr_id#
		</cfquery>

		<cfif isdefined("cert_id")>

         <cfloop index="c" list="#cert_id#">

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into usr_cert
			 (usr_cert_cert_id, usr_cert_usr_id, usr_cert_hub_id)
			 values
			 (#c#,#session.usr_id#,#session.hub#)
			</cfquery>

         </cfloop>

		</cfif>

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update usr
		 set usr_updated = #now()#
		 where usr_id = #session.usr_id#
		</cfquery>

	</cftransaction>

	<cflocation URL="certs.cfm?l=9&u=1" addtoken="no">

</cfif>

