<cfinclude template="/exchange/security/check.cfm">

<cfquery name="plan" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">


<style>
.app_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 174px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header" style="font-size: 30;">My Subscription - Plan Details</td>
			       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=20></td></tr>
           </table>

           <cfoutput query="plan">

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

	         <tr><td height=10></td></tr>

	         <tr><td valign=top width=125>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                  <td>
                   <cfif #hub_role_image# is "">
                   <img src="#image_virtual#/icon_product_stock.png" style="width: 125px; height: 125x;" border=0>
                   <cfelse>
                   <img src="#media_virtual#/#hub_role_image#" style="width: 125px; height: 125px;" border=0>
                   </cfif>
                  </td>
              </tr>

              <tr><td height=20></td></tr>

              </table>

              </td><td width=20>&nbsp;</td><td valign=top>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                 <td class="feed_header">#hub_role_name#</td>
                 <td align=right></td>
              </tr>

              <tr><td height=10></td></tr>

              <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" colspan=2>#hub_role_desc#</td>
              </tr>

              </table>

              </td></tr>

              <tr><td colspan=3><hr></td></tr>

              </table>

	   </cfoutput>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Apps Included in this Plan</td></tr>
      <tr><td height=20></td></tr>

	 </table>

		<cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select menu_app_id from menu
		  where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				menu_hub_id = #session.hub#
		</cfquery>

	   <cfif apps.recordcount is 0>
	    <cfset app_list = 0>
	   <cfelse>
	    <cfset app_list = valuelist(apps.menu_app_id)>
	   </cfif>

	  <cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from app
	   join app_category on app_category.app_category_id = app.app_category_id
	   where app_active = 1 and
	   app_id in (#app_list#)
	   order by app_name
	  </cfquery>

	  <cfif apps.recordcount is 0>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">No Apps have been included in this Plan.</td></tr>
	  </table>

	  <cfelse>

      <cfloop query="apps">

	  <div class="app_badge">
	   <cfinclude template="app_badge.cfm">
	  </div>

	  </cfloop>

	  </cfif>

	  </div>

           </td></tr>

           </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>