<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  join hub_xref on hub_xref_role_id = hub_role_id
  where hub_role_hub_id = #session.hub# and
        hub_xref_usr_id = #session.usr_id#
</cfquery>

<cfquery name="profile_apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(distinct(menu_app_id)) as total from menu
  where menu_role_id = #profiles.hub_role_id# and
		menu_hub_id = #session.hub#
</cfquery>

<cfquery name="others" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub# and
        hub_role_public = 1 and
        hub_role_id <> #profiles.hub_role_id#
  order by hub_role_order
</cfquery>

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(portfolio_id) as total from portfolio
  where portfolio_usr_id = #session.usr_id# and
        portfolio_hub_id = #session.hub#
</cfquery>

<cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(deal_id) as total from deal
  where deal_owner_id = #session.usr_id# and
        deal_hub_id = #session.hub#
</cfquery>

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(pipeline_id) as total from pipeline
  where pipeline_usr_id = #session.usr_id# and
        pipeline_hub_id = #session.hub#
</cfquery>

<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(market_report_id) as total from market_report
  where market_report_created_by_usr_id = #session.usr_id# and
        market_report_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<style>
.badge {
    width: 95%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: auto;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 4px;
    background-color: #ffffff;
}
.bar {
    width: 100px;
    background-color: red;
    color: red;
    height: 20px;
    text-align: left;
    border-radius: 1px;
    background-color: #ffffff;
}

</style>

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header" style="font-size: 30;">My Subscription</td></tr>
			   <tr><td><hr></td></tr>
			   <tr><td height=20></td></tr>
           </table>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_header"><img src="/images/icon_subscribe.png" width=30>&nbsp;&nbsp;&nbsp;You are currently subscribed to the below plan.</td></tr>
		    <tr><td height=20></td></tr>
		   </table>

           <cfoutput query="profiles">

			<div class="badge">

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

	         <tr><td height=10></td></tr>

	         <tr><td valign=top width=100>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                  <td>
                  <a href="manage.cfm?i=#encrypt(hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
                   <cfif #hub_role_image# is "">
                   <img src="#image_virtual#/icon_product_stock.png" style="width: 100px; height: 100px;" border=0>
                   <cfelse>
                   <img src="#media_virtual#/#hub_role_image#" style="width: 100px; height: 100px;" border=0>
                   </cfif>
                  </a>
                  </td>
              </tr>

              <tr><td height=30></td></tr>

              <tr>
                 <td class="feed_header" align=center>Apps<br>Included</td>
              </tr>

              <tr>
                 <td class="feed_header" align=center style="font-weight: normal; padding-bottom: 0px; font-size: 30;"><b>#profile_apps.total#</b></td>
              </tr>

              </table>

              </td><td width=20>&nbsp;</td><td valign=top>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                 <td class="feed_header"><a href="manage.cfm?i=#encrypt(hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#hub_role_name#</a></td>
                 <td align=right><a href="manage.cfm?i=#encrypt(hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/icon_subscription_manage.png" height=35 border=0 alt="Manage Plan" title="Manage Plan"></a></td>
              </tr>

              <tr><td height=10></td></tr>

              <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" colspan=2>#hub_role_desc#</td>
              </tr>

              <tr><td colspan=2><hr></td></tr>

              <tr><td colspan=2>

		         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		          <tr>
		             <td class="feed_sub_header" align=center>Market Reports</td>
		             <td class="feed_sub_header" align=center>Portfolios</td>
		             <td class="feed_sub_header" align=center>Opp Boards</td>
		             <td class="feed_sub_header" align=center>Deals</td>
		             <td class="feed_sub_header" align=center>Start</td>
		             <td class="feed_sub_header" align=center>End</td>
		             <td class="feed_sub_header" align=center>Monthly</td>
		             <td class="feed_sub_header" align=center>Annual</td>
		          </tr>

		          <tr>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_reports is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(reports.total/hub_role_limit_reports*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_portfolios is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(portfolios.total/hub_role_limit_portfolios*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_boards is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(boards.total/hub_role_limit_boards*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_deals is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(deals.total/hub_role_limit_deals*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">12/12/2019</td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">12/12/2020</td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">#numberformat(hub_role_monthly,'$999,999')#</td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">#numberformat(hub_role_annually,'$999,999')#</td>

		          </tr>

		          <tr>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_reports is "">Unlimited<cfelse>#reports.total# of #hub_role_limit_reports# Used</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_portfolios is "">Unlimited<cfelse>#portfolios.total# of #hub_role_limit_portfolios# Used</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_boards is "">Unlimited<cfelse>#boards.total# of #hub_role_limit_boards# Used</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_deals is "">Unlimited<cfelse>#deals.total# of #hub_role_limit_deals# Used</cfif></td>
		             <td class="feed_sub_header" width=12% align=center colspan=2 align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;">22 Days Remaining</td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;"></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;"></td>

		          </tr>

		         </table>

              </td></tr>

              </table>

              </td></tr>

              </table>

			</div>

		   </cfoutput>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td><hr></td></tr>
			<tr><td height=20></td></tr>

			<tr><td class="feed_header"><img src="/images/icon_other.png" width=30>&nbsp;&nbsp;&nbsp;Other subscription plans that you may be interested in.</td></tr>
		    <tr><td height=20></td></tr>
		   </table>

           <cfloop query="others">

			<cfquery name="others_apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select count(distinct(menu_app_id)) as total from menu
			  where menu_role_id = #others.hub_role_id# and
					menu_hub_id = #session.hub#
			</cfquery>

			<cfoutput>

			<div class="badge">

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

	         <tr><td height=10></td></tr>

	         <tr><td valign=top width=100>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                  <td>
                  <a href="plan.cfm?i=#encrypt(others.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
                   <cfif #others.hub_role_image# is "">
                   <img src="#image_virtual#/icon_product_stock.png" style="width: 100px; height: 100px;" border=0>
                   <cfelse>
                   <img src="#media_virtual#/#others.hub_role_image#" style="width: 100px; height: 100px;" border=0>
                   </cfif>
                  </a>
                  </td>
              </tr>

              <tr><td height=20></td></tr>

              <tr>
                 <td class="feed_header" align=center>Apps Included</td>
              </tr>

              <tr>
                 <td class="feed_header" align=center style="font-weight: normal; padding-bottom: 0px; font-size: 30;"><b>#others_apps.total#</b></td>
              </tr>


              </table>

              </td><td width=20>&nbsp;</td><td valign=top>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                 <td class="feed_header"><a href="plan.cfm?i=#encrypt(others.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#others.hub_role_name#</a></td>
                 <td align=right><a href="plan.cfm?i=#encrypt(others.hub_role_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/icon_subscription_more.png" height=35 alt="Learn More" title="Learn More" border=0></a></td>
              </tr>

              <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" colspan=2>#others.hub_role_desc#</td>
              </tr>

              <tr><td colspan=2><hr></td></tr>

              <tr><td colspan=2>

		         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		          <tr>
		             <td class="feed_sub_header" align=center>Market Reports</td>
		             <td class="feed_sub_header" align=center>Portfolios</td>
		             <td class="feed_sub_header" align=center>Opp Boards</td>
		             <td class="feed_sub_header" align=center>Deals</td>
		             <td class="feed_sub_header" align=center>&nbsp;</td>
		             <td class="feed_sub_header" align=center>&nbsp;</td>
		             <td class="feed_sub_header" align=center>Monthly</td>
		             <td class="feed_sub_header" align=center>Annual</td>
		          </tr>

		          <tr>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;"><cfif others.hub_role_limit_reports is "">Unlimited<cfelse>





		             #others.hub_role_limit_reports#</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;"><cfif others.hub_role_limit_portfolios is "">Unlimited<cfelse>#others.hub_role_limit_portfolios#</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;"><cfif others.hub_role_limit_boards is "">Unlimited<cfelse>#others.hub_role_limit_boards#</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;"><cfif others.hub_role_limit_deals is "">Unlimited<cfelse>#others.hub_role_limit_deals#</cfif></td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">&nbsp;</td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">&nbsp;</td>

		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">#numberformat(others.hub_role_monthly,'$999,999')#</td>
		             <td class="feed_sub_header" width=12% align=center style="padding-top: 0px; font-weight: normal;">#numberformat(others.hub_role_annually,'$999,999')#</td>

		          </tr>

		         </table>

		         </td></tr>

              </table>

              </td></tr>

              </table>

			</div>

			</cfoutput>

		   </cfloop>

           </td></tr>

          </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>