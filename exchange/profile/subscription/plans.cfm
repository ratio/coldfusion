<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profiles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_hub_id = #session.hub# and
        hub_role_public = 1
  order by hub_role_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<style>
.badge {
    width: 95%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: auto;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 4px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Subscription</td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td height=5></td></tr>
                 <tr><td class="feed_sub_header"><font color="green"><b>Password has been successfully changed.</b></font></td></tr>
                </cfif>
               </cfif>

             <tr><td height=20></td></tr>

           </table>

           <cfoutput query="profiles">

			<div class="badge">

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

	         <tr><td height=10></td></tr>

	         <tr><td valign=top width=200>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                  <td>
                   <cfif #hub_role_image# is "">
                   <img src="#image_virtual#/icon_product_stock.png" style="width: 175px; height: 175px;" border=0>
                   <cfelse>
                   <img src="#media_virtual#/#hub_role_image#" style="width: 175px; height: 175px;" border=0>
                   </cfif>
                  </td>
              </tr>

              </table>

              </td><td valign=top>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr><td height=10></td></tr>

              <tr>
                 <td class="feed_header">#hub_role_name#</td>
                 <td class="feed_header" align=center>Monthly</td>
                 <td class="feed_header" align=center>Annually</td>
              </tr>

              <tr>
                 <td class="feed_sub_header" style="font-weight: normal;">#hub_role_desc#</td>
                 <td class="feed_header" align=center style="font-weight: normal;" width=100>#numberformat(hub_role_monthly,'$999,999')#</td>
                 <td class="feed_header" align=center style="font-weight: normal;" width=100>#numberformat(hub_role_annually,'$999,999')#</td>
              </tr>

              </table>

              </td></tr>

              </table>

			</div>

		   </cfoutput>

           </td></tr>

          </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>