<cfinclude template="/exchange/security/check.cfm">

<cfquery name="plan" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from hub_role
  where hub_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(portfolio_id) as total from portfolio
  where portfolio_usr_id = #session.usr_id# and
        portfolio_hub_id = #session.hub#
</cfquery>

<cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(deal_id) as total from deal
  where deal_owner_id = #session.usr_id# and
        deal_hub_id = #session.hub#
</cfquery>

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(pipeline_id) as total from pipeline
  where pipeline_usr_id = #session.usr_id# and
        pipeline_hub_id = #session.hub#
</cfquery>

<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(market_report_id) as total from market_report
  where market_report_created_by_usr_id = #session.usr_id# and
        market_report_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">


<style>
.app_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 174px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header" style="font-size: 30;">My Subscription</td>
			       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=20></td></tr>
			   <tr><td class="feed_header">My Plan</td></tr>
			   <tr><td height=10></td></tr>
           </table>

           <cfoutput query="plan">

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

	         <tr><td height=10></td></tr>

	         <tr><td valign=top width=150>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                  <td>
                   <cfif #hub_role_image# is "">
                   <img src="#image_virtual#/icon_product_stock.png" style="width: 150px; height: 150x;" border=0>
                   <cfelse>
                   <img src="#media_virtual#/#hub_role_image#" style="width: 150px; height: 150px;" border=0>
                   </cfif>
                  </td>
              </tr>

              <tr><td height=20></td></tr>

              </table>

              </td><td width=20>&nbsp;</td><td valign=top>

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr>
                 <td class="feed_header">#hub_role_name#</td>
                 <td align=right></td>
              </tr>

              <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" colspan=2>#hub_role_desc#</td>
              </tr>

              </table>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr>
                   <td class="feed_header" style="padding-bottom: 0px;">Plan Duration</td>
                   <td class="feed_header">&nbsp;</td>
                   <td class="feed_header" style="padding-bottom: 0px;" align=right>Monthly</td>
                   <td class="feed_header" style="padding-bottom: 0px;" align=right>Annually</td>
                </tr>

                <tr>
                   <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">12/12/2000 - 12/12/2001 (<b>43 days remaining</b>)</td>
                   <td class="feed_sub_header" width=40%>&nbsp;</td>
                   <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" align=right width=100>$30</td>
                   <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" align=right width=100>$400</td>
                </tr>

              </table>

              </td></tr>

              <tr><td colspan=3><hr></td></tr>

              </table>

		         <table cellspacing=0 cellpadding=0 border=0 width=100%>
                 <tr><td height=20></td></tr>
                 <tr><td width=165 valign=top>

		         <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td class="feed_header">Usage Summary</td></tr>
                  <tr><td class="link_small_gray">Includes base plan and additional purchases.</td></tr>
                 </table>

                 </td><td valign=top>

		         <table cellspacing=0 cellpadding=0 border=0 width=100%>
		          <tr>
		             <td class="feed_header" align=center>Market Reports</td>
		             <td class="feed_header" align=center>Portfolios</td>
		             <td class="feed_header" align=center>Opp Boards</td>
		             <td class="feed_header" align=center>Deals</td>
		          </tr>

		          <tr>
		             <td class="feed_sub_header" align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_reports is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(reports.total/hub_role_limit_reports*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_portfolios is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(portfolios.total/hub_role_limit_portfolios*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_boards is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(boards.total/hub_role_limit_boards*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>
		             <td class="feed_sub_header" align=center style="padding-top: 0px; font-weight: normal;">

                     <cfif hub_role_limit_deals is "">
                      <cfset progress_width = 100>
                     <cfelse>
                     <cfset calc_width = #evaluate(deals.total/hub_role_limit_deals*100)#>
                       <cfif calc_width GTE 100>
                       <cfset progress_width = 100>
                      <cfelse>
                       <cfset progress_width = calc_width>
                      </cfif>
                     </cfif>

					 <div class="w3-border" style="text-align: left; width: 100px;">
					   <div class="w3-green" style="height:20px; width: #progress_width#%;"></div>
					 </div>

		             </td>

		          </tr>

		          <tr>
		             <td class="feed_sub_header" width=150 align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_reports is "">Unlimited<cfelse>#reports.total# of #hub_role_limit_reports# Used</cfif></td>
		             <td class="feed_sub_header" width=150 align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_portfolios is "">Unlimited<cfelse>#portfolios.total# of #hub_role_limit_portfolios# Used</cfif></td>
		             <td class="feed_sub_header" width=150 align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_boards is "">Unlimited<cfelse>#boards.total# of #hub_role_limit_boards# Used</cfif></td>
		             <td class="feed_sub_header" width=150 align=center style="padding-top: 0px; font-weight: bold; font-size: 16px;"><cfif hub_role_limit_deals is "">Unlimited<cfelse>#deals.total# of #hub_role_limit_deals# Used</cfif></td>
		          </tr>


		         </table>

		         </td><td width=250 valign=top>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_sub_header" style="font-weight: normal;">

				       Do you need more of these?  If so, click <b>Add More</b> to see what options are available.

				       </td></tr>
				 </table>


		         </td><td>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td align=right><a href="upgrade.cfm"><img src="/images/icon_subscription_add_more.png" height=35 alt="Add More" title="Add More"></a></td></tr>
				 </table>

		         </td></tr>

                 <tr><td height=10></td></tr>
                 <tr><td colspan=4><hr></td></tr>
                 <tr><td height=10></td></tr>

		         </table>

		         </cfoutput>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">Apps Included in this Plan</td>
	       <td align=right><a href="upgrade.cfm"><img src="/images/icon_subscription_add_apps.png" height=35 alt="Upgrade" title="Upgrade"></a></td></tr>
      <tr><td height=20></td></tr>

	 </table>

		<cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select menu_app_id from menu
		  where menu_role_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
				menu_hub_id = #session.hub#
		</cfquery>

	   <cfif apps.recordcount is 0>
	    <cfset app_list = 0>
	   <cfelse>
	    <cfset app_list = valuelist(apps.menu_app_id)>
	   </cfif>

	  <cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from app
	   join app_category on app_category.app_category_id = app.app_category_id
	   where app_active = 1 and
	   app_id in (#app_list#)
	   order by app_name
	  </cfquery>

	  <cfif apps.recordcount is 0>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">No Apps have been included in this Plan.</td></tr>
	  </table>

	  <cfelse>

      <cfloop query="apps">

	  <div class="app_badge">
	   <cfinclude template="app_badge.cfm">
	  </div>

	  </cfloop>

	  </cfif>

	  </div>

           </td></tr>

           </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>