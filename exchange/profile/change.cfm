<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Change Password</td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td height=5></td></tr>
                 <tr><td class="feed_sub_header"><font color="green"><b>Password has been successfully changed.</b></font></td></tr>
                </cfif>
               </cfif>

               <form action="update.cfm" method="post">

                   <cfif isdefined("e")>
                    <cfif e is 1>
				     <tr><td class="feed_sub_header"><font color="red"><b>Current password is invalid. Please try again.</b></td></tr>
                    <cfelse>
  			         <tr><td class="feed_sub_header"><font color="red"><b>New password does not match confirm password.  Please try again.</b></td></tr>
                    </cfif>
                   </cfif>

				   <tr><td class="feed_sub_header"><b>Current Password</b></td></tr>
                   <tr><td><input type="password" class="input_text" name="current_password" size=35 maxlength="50" required></td></tr>
                   <tr><td height=10></td></tr>
				   <tr><td class="feed_sub_header"><b>New Password</b></td></tr>
                   <tr><td><input type="password" class="input_text" name="new_password" size=35 maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,}" title="Passwords must contain at least one number, one uppercase and lowercase letter, a special character, and at least 8 characters" required></td></tr>
                   <tr><td height=10></td></tr>
				   <tr><td class="feed_sub_header"><b>Confirm New Password</b></td></tr>
                   <tr><td><input type="password" class="input_text" name="new_password_confirm" size=35 maxlength="50" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,}" title="Passwords must contain at least one number, one uppercase and lowercase letter, a special character, and at least 8 characters" required></td></tr>
                   <tr><td height=10></td></tr>

	               <tr><td class="link_small_gray">Passwords must contain at least one number, one uppercase and lowercase letter, a special character, and at least 8 characters.</td></tr>
                   <tr><td height=10></td></tr>
	               <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>



 			       <tr><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>

			   </form>

              </td></tr>

           </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>