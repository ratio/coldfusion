<cfinclude template="/exchange/security/check.cfm">

    <cfif #new_password# is not #new_password_confirm#>
     <cflocation URL="/exchange/profile/change.cfm?e=2" addtoken="no">
    </cfif>

	<cfquery name="find" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 join hub_xref on hub_xref_usr_id = usr_id
	 where hub_xref_hub_id = #session.hub# and
	       hub_xref_usr_id = #session.usr_id# and
		   hub_xref_active = 1
	</cfquery>

	<cfquery name="hinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfif #tostring(tobinary(find.usr_password))# is #current_password#>

	<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update usr
	  set usr_password = '#tobase64(new_password)#',
	      usr_password_date = #now()#
	  where usr_id = #session.usr_id#
	</cfquery>

	<cfmail from="#hinfo.hub_name# <noreply@ratio.exchange>"
			  to="#tostring(tobinary(find.usr_email))#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="Password Changed">

	<html>
	<head>
	<title>#hinfo.hub_name#</title>
	</head><div class="center">
	<body class="body">

    <cfoutput>
	<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
	 <tr><td class="feed_sub_header">Your #hinfo.hub_name# password has been changed.</td></tr>
	 <tr><td class="feed_sub_header" style="font-weight: normal;">If you did not change your password please contract us immediately.</td></tr>
	 <tr><td>&nbsp;</td></tr>

	 <tr><td class="feed_sub_header" style="font-weight: normal;">
	 <b>#hinfo.hub_support_name#</b><br>
	    #hinfo.hub_support_phone#<br>
	    #hinfo.hub_support_email#
	  </td></tr>

	</table>
	</cfoutput>

	</body>
	</html>

	</cfmail>

	<cfelse>

	  <cflocation URL="/exchange/profile/change.cfm?e=1" addtoken="no">

    </cfif>

<cflocation URL="/exchange/profile/change.cfm?l=2&u=1" addtoken="no">