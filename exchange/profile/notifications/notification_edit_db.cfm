<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update usr_notification
	 set usr_notification_name = '#usr_notification_name#',
		 usr_notification_keywords ='#usr_notification_keywords#',
		 usr_notification_contracts = <cfif isdefined("usr_notification_contracts")>1<cfelse>null</cfif>,
		 usr_notification_sbirs = <cfif isdefined("usr_notification_sbirs")>1<cfelse>null</cfif>,
		 usr_notification_grants = <cfif isdefined("usr_notification_grants")>1<cfelse>null</cfif>,
		 usr_notification_awards = <cfif isdefined("usr_notification_awards")>1<cfelse>null</cfif>,
		 usr_notification_challenges = <cfif isdefined("usr_notification_challenges")>1<cfelse>null</cfif>,
		 usr_notification_frequency = '#usr_notification_frequency#',
		 usr_notification_email_list = '#usr_notification_email_list#'
	 where usr_notification_id = #i# and
		   usr_notification_usr_id = #session.usr_id# and
		   usr_notification_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="notifications.cfm?l=3&u=1" addtoken="no">

<cfelse>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete usr_notification
	 where usr_notification_id = #i# and
		   usr_notification_usr_id = #session.usr_id# and
		   usr_notification_hub_id = #session.hub#
	</cfquery>

	<cflocation URL="notifications.cfm?l=3&u=3" addtoken="no">

</cfif>
