<cfinclude template="/exchange/security/check.cfm">

<cfquery name="notifications" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_notification
 where usr_notification_usr_id = #session.usr_id# and
       usr_notification_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header" style="font-size: 30;">Notifications</td>
				       <td class="feed_sub_header" align=right>

				       <a href="add.cfm?l=3"><img src="/images/plus3.png" width=15 hspace=10 border=0></a>
				       <a href="add.cfm?l=3">Add Notification</a>

				       </td></tr>
				   <tr><td colspan=2><hr></td></tr>
			   </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=6>Notifications are emails that you can setup to automatically inform you about information that is posted to the Exchange.</td></tr>

               <cfif notifications.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">You have not setup any Notifications.</td></tr>

               <cfelse>

				   <cfif isdefined("u")>
					<cfif u is 1>
					 <tr><td class="feed_sub_header" colspan=3><font color="green"><b>Notification settings have been updated.</b></font></td></tr>
					<cfelseif u is 3>
					 <tr><td class="feed_sub_header" colspan=3><font color="red"><b>Notification has been deleted.</b></font></td></tr>
					<cfelseif u is 4>
					 <tr><td class="feed_sub_header" colspan=3><font color="green"><b>Notification has been updated.</b></font></td></tr>
					<cfelseif u is 5>
					 <tr><td class="feed_sub_header" colspan=3><font color="green"><b>Notification has been successfully ran.</b></font></td></tr>
					</cfif>
					 <tr><td height=10></td></tr>

				   </cfif>

                <tr>
                   <td class="feed_sub_header">Notification Name</td>
                   <td class="feed_sub_header">Type</td>
                   <td class="feed_sub_header">Keywords</td>
                   <td class="feed_sub_header">Email List</td>
                   <td class="feed_sub_header">Frequency</td>
                   <td class="feed_sub_header" align=center>Status</td>
                   <td></td>
                </tr>

               <cfset counter = 0>

               <cfloop query="notifications">

               <cfoutput>

                 <cfif counter is 0>
                  <tr bgcolor="ffffff">
                 <cfelse>
                  <tr bgcolor="e0e0e0">
                 </cfif>

                     <td class="feed_sub_header" width=400>

                     <cfif usr_notification_type is "Open Opportunities" or usr_notification_type is "Opportunity Search">
              	       <a href="edit_new_opportunities.cfm?i=#usr_notification_id#&l=3&t=#notifications.usr_notification_type#">#notifications.usr_notification_name#</a>
              	     <cfelseif usr_notification_type is "New Awards">
                     	<a href="edit_award_notification.cfm?i=#usr_notification_id#&l=3&t=#notifications.usr_notification_type#">#notifications.usr_notification_name#</a>
                     </cfif>

                     </td>
                     <td class="feed_sub_header" style="font-weight: normal;" width=175>#notifications.usr_notification_type#</td>
                     <td class="feed_sub_header" style="font-weight: normal;">#replaceNoCase(notifications.usr_notification_keywords,'"','',"all")#</td>
                     <td class="feed_sub_header" style="font-weight: normal; padding-right: 10px;" width=200>

                     <cfif listlen(notifications.usr_notification_email_list) is 1>
                      #notifications.usr_notification_email_list#
                     <cfelse>
                      #listlen(notifications.usr_notification_email_list)# Recipients <img src="/images/icon_email.png" hspace=10 style="cursor: pointer;" width=20 alt="#notifications.usr_notification_email_list#" title="#notifications.usr_notification_email_list#">
                     </cfif>

                     </td>
                     <td class="feed_sub_header" style="font-weight: normal;">#notifications.usr_notification_frequency#</td>

              <td class="feed_sub_header" style="font-weight: normal;" align=center width=75>

			  <cfif notifications.usr_notification_active is 1>
				  <a href="notification_update.cfm?i=#notifications.usr_notification_id#&u=0"><img src="/images/icon_slider_on.png" width=55 border=0 hspace=5 title="Disable" alt="Disable"></a>
			  <cfelse>
				  <a href="notification_update.cfm?i=#notifications.usr_notification_id#&u=1"><img src="/images/icon_slider_off.png" width=55 border=0 hspace=5 title="Activate" alt="Activate"></a>
			  </cfif>

              </td>

              <td align=center width=50>

              <cfif usr_notification_type is "Open Opportunities" or usr_notification_type is "Opportunity Search">
                <a href="edit_new_opportunities.cfm?i=#usr_notification_id#&l=3&t=#notifications.usr_notification_type#"><img src="/images/icon_edit.png" width=25 alt="Edit Notification" title="Edit Notification" hspace=5></a>
              <cfelseif usr_notification_type is "New Awards">
                <a href="edit_award_notification.cfm?i=#usr_notification_id#&l=3&t=#notifications.usr_notification_type#"><img src="/images/icon_edit.png" width=25 alt="Edit Notification" title="Edit Notification" hspace=5></a>
              </cfif>
              </td>

              <td align=center width=40>

              <cfif notifications.usr_notification_type is "Opportunity Search" or notifications.usr_notification_type is "Open Opportunities">
                <a href="run_new_opportunities.cfm?i=#usr_notification_id#" onclick="return confirm('Run Notification?\r\nAre you sure you want to run this notification?');"><img src="/images/icon_play.png" width=28 alt="Run Notification" title="Run Notification"></a>
              <cfelseif usr_notification_type is "New Awards">
                <a href="run_awards.cfm?i=#usr_notification_id#" onclick="return confirm('Run Notification?\r\nAre you sure you want to run this notification?');"><img src="/images/icon_play.png" width=28 alt="Run Notification" title="Run Notification"></a>
              </cfif>

              </td>


                 </tr>

                 </cfoutput>

                 <cfif counter is 0>
                  <cfset counter = 1>
                 <cfelse>
                  <cfset counter = 0>
                 </cfif>

   		       </cfloop>

   		       </cfif>

   		  </table>

              </td></tr>

           </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>