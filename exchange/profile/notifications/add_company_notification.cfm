<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=20></td></tr>
		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header" style="font-size: 30;">Add Notification</td>
				       <td class="feed_sub_header" align=right><a href="add.cfm?l=3">Return</a></td></tr>
				   <tr><td colspan=2><hr></td></tr>
		   </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td height=10></td></tr>

            <form action="award_db.cfm" method="post">

            <tr>
               <td class="feed_sub_header" width=200>Notification Type</td>
               <td class="feed_sub_header">Company Portfolio Updates</td>
            </tr>

            <tr>
               <td class="feed_sub_header" width=200>Notification Name</td>
               <td><input type="text" name="usr_notification_name" class="input_text" style="width: 500px;" required></td>
            </tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200 valign=top>Description</td>
               <td><textarea name="usr_notification_desc" class="input_textarea" style="width: 500px; height: 100px;"></textarea></td>
            </tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Search Keywords</td>
               <td><input type="text" name="usr_notification_keywords" class="input_text" style="width: 800px;"></td>
            </tr>

            <tr><td></td>
                <td class="link_small_gray">Enter keywords that can be used to filter notification results (i.e., machine learning or natural language processing or NLP, etc.).  Leave blank for all.</td></tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Select Company Portfolio</td>
               <td><input type="text" name="usr_notification_naics" class="input_text" style="width: 800px;"></td>
            </tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Notification Email List</td>
               <td><input type="text" name="usr_notification_email_list" class="input_text" style="width: 800px;" required></td>
            </tr>

            <tr><td></td>
                <td class="link_small_gray">Enter the email addresses (seperated by commas) that will receive this notification.</td></tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Notification Schedule</td>
               <td><select name="usr_notification_frequency" class="input_select" style="width: 150px;">
                    <option value="Daily">Daily
                    <option value="Weekly">Weekly
                    <option value="Monthly">Monthly
                   </select>

                   <span class="link_small_gray">Daily - 7am (EDT) each day, Weekly - 7am (EDT) every Monday, Monthly - 7am (EDT) on the 1st of each month.</span>

               </td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Status</td>
               <td><select name="usr_notification_active" class="input_select" style="width: 150px;">
                    <option value=0>Off (Paused)
                    <option value=1 selected>On (Active)
                   </select>

               </td></tr>

            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>

            <tr><td></td><td>

            <input type="submit" name="button" value="Save Notification" class="button_blue_large">

            </td></tr>

            </form>

   		   </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>