<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(usr_notification_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>

<cfif button is "Save Notification">

	<cfquery name="add" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert usr_notification
	 (
	 usr_notification_name,
	 usr_notification_email_list,
	 usr_notification_keywords,
	 usr_notification_desc,
	 usr_notification_duns,
	 usr_notification_naics,
	 usr_notification_type,
	 usr_notification_hub_id,
	 usr_notification_usr_id,
	 usr_notification_active,
	 usr_notification_frequency
	 )
	 values
	 (
	 '#usr_notification_name#',
	 '#usr_notification_email_list#',
	 '#search_string#',
	 '#usr_notification_desc#',
	 '#usr_notification_duns#',
	 '#usr_notification_naics#',
	 'New Awards',
	  #session.hub#,
	  #session.usr_id#,
	  #usr_notification_active#,
	 '#usr_notification_frequency#'
	 )
	</cfquery>

	<cflocation URL="index.cfm?l=3&u=1" addtoken="no">

<cfelseif button is "Update Notification">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update usr_notification
	 set
	 usr_notification_name = '#usr_notification_name#',
	 usr_notification_email_list = '#usr_notification_email_list#',
	 usr_notification_keywords = '#search_string#',
	 usr_notification_desc = '#usr_notification_desc#',
	 usr_notification_duns = '#usr_notification_duns#',
	 usr_notification_naics = '#usr_notification_naics#',
	 usr_notification_active = #usr_notification_active#,
	 usr_notification_frequency = '#usr_notification_frequency#'
	 where usr_notification_id = #i#
	</cfquery>

	<cflocation URL="index.cfm?l=3&u=1" addtoken="no">

<cfelseif button is "Delete Notification">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete usr_notification
	 where usr_notification_id = #i#
	</cfquery>

	<cflocation URL="index.cfm?l=3&u=3" addtoken="no">

</cfif>

