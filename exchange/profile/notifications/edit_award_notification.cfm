<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_notification
 where usr_notification_usr_id = #session.usr_id# and
       usr_notification_hub_id = #session.hub# and
       usr_notification_id = #i#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=20></td></tr>
		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header" style="font-size: 30;">Edit Notification</td>
				       <td class="feed_sub_header" align=right><a href="index.cfm?l=3">Cancel</a></td></tr>
				   <tr><td colspan=2><hr></td></tr>
		   </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td height=10></td></tr>

            <form action="award_db.cfm" method="post">

            <cfoutput>

            <tr>
               <td class="feed_sub_header" width=200>Notification Name</td>
               <td><input type="text" name="usr_notification_name" class="input_text" value="#edit.usr_notification_name#" style="width: 500px;" required></td>
            </tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200 valign=top>Description</td>
               <td><textarea name="usr_notification_desc" class="input_textarea" style="width: 500px; height: 100px;">#edit.usr_notification_desc#</textarea></td>
            </tr>

            <tr><td height=10></td></tr>
            <tr>
               <td class="feed_sub_header" width=200>Search Keywords</td>
               <td><input type="text" name="usr_notification_keywords" class="input_text" style="width: 800px;" value='#replaceNoCase(edit.usr_notification_keywords,'"','',"all")#'></td>
            </tr>

            <tr><td></td>
                <td class="link_small_gray">Enter the keywords that will be used to find opportunities (i.e., machine learning or natural language processing or NLP, etc.)</td></tr>

            <tr>
               <td class="feed_sub_header" width=200>DUNS Number(s)</td>
               <td><input type="text" name="usr_notification_duns" value="#edit.usr_notification_duns#" class="input_text" style="width: 800px;"></td>
            </tr>

            <tr><td></td>
                <td class="link_small_gray">Enter the Vendors DUNS number(s) that you would like to get notifications about (i.e., 92831231,39403921 etc.)  Leave blank for all.</td></tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>NAICS Code(s)</td>
               <td><input type="text" name="usr_notification_naics" value="#edit.usr_notification_naics#" class="input_text" style="width: 800px;"></td>
            </tr>

            <tr><td></td>
                <td class="link_small_gray">Enter the Federal NAICS Code(s) that you would like to get notifications about (i.e., 511213,421001,513541 etc.) Leave blank for all.</td></tr>





            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Notification Email List</td>
               <td><input type="text" name="usr_notification_email_list" class="input_text" style="width: 800px;" value="#edit.usr_notification_email_list#" required></td>
            </tr>

            <tr><td></td>
                <td class="link_small_gray">Enter the email addresses (seperated by commas) that will receive this notification.</td></tr>

            <tr><td height=10></td></tr>

            <tr>
               <td class="feed_sub_header" width=200>Notification Schedule</td>
               <td><select name="usr_notification_frequency" class="input_select" style="width: 150px;">
                    <option value="Daily" <cfif edit.usr_notification_frequency is "Daily">selected</cfif>>Daily
                    <option value="Weekly" <cfif edit.usr_notification_frequency is "Weekly">selected</cfif>>Weekly
                    <option value="Monthly" <cfif edit.usr_notification_frequency is "Monthly">selected</cfif>>Monthly
                   </select>

                   <span class="link_small_gray">Daily - 7am (EDT) each day, Weekly - 7am (EDT) every Monday, Monthly - 7am (EDT) on the 1st of each month.</span>

               </td></tr>


            <tr>
               <td class="feed_sub_header" width=200>Status</td>
               <td><select name="usr_notification_active" class="input_select" style="width: 150px;">
                    <option value=0 <cfif edit.usr_notification_active is 0>selected</cfif>>Off (Paused)
                    <option value=1 <cfif edit.usr_notification_active is 1>selected</cfif>>On (Active)
                   </select>

               </td></tr>

            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>

            <tr><td></td><td>

            <input type="submit" name="button" value="Update Notification" class="button_blue_large">
            &nbsp;&nbsp;
            <input class="button_blue_large" type="submit" name="button" value="Delete Notification" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">
            <input type="hidden" name="i" value=#i#>

            </td></tr>

            </cfoutput>

            </form>

   		   </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>