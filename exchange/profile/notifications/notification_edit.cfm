<cfinclude template="/exchange/security/check.cfm">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_notification
 where usr_notification_id = #i# and
       usr_notification_usr_id = #session.usr_id# and
       usr_notification_hub_id = #session.hub#
</cfquery>

<cfquery name="distribution" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(usr_id), usr_first_name, usr_last_name, usr_photo from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where hub_xref_active = 1 and
       hub_xref_hub_id = #session.hub#
 order by usr_last_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Edit Notification</td></tr>
				   <tr><td colspan=2><hr></td></tr>
			   </table>

			  <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <form action="notification_edit_db.cfm" method="post">

				<tr><td class="feed_sub_header" width=150>Type</td>
                    <td class="feed_sub_header" width=150>#edit.usr_notification_type#</td>
				</tr>

				<tr><td class="feed_sub_header" width=150>Name</td>
					<td><input type="text" class="input_text" placeholder="notification name" name="usr_notification_name" value="#edit.usr_notification_name#" style="width: 325px;"></td>
				</tr>

				<tr><td class="feed_sub_header">Keywords</td>
					<td><input type="text" class="input_text" placeholder="search keywords" name="usr_notification_keywords" value="#edit.usr_notification_keywords#" style="width: 525px;"></td>
				</tr>

				<tr><td class="feed_sub_header" valign=top>Sources</td>
					<td>

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr>
						  <td width=30><input type="checkbox" name="usr_notification_contracts" style="width: 20px; height: 20px; margin-right: 10px;" <cfif #edit.usr_notification_contracts# is 1>checked</cfif>></td>
						  <td width=150 class="feed_sub_header" style="font-weight: normal;">Contracts</td>
						  <td width=30><input type="checkbox" name="usr_notification_grants" style="width: 20px; height: 20px; margin-right: 10px;" <cfif #edit.usr_notification_grants# is 1>checked</cfif>></td>
						  <td class="feed_sub_header" style="font-weight: normal;">Grants</td>
					   </tr>

					   <tr>
						  <td width=30><input type="checkbox" name="usr_notification_sbirs" style="width: 20px; height: 20px; margin-right: 10px;" <cfif #edit.usr_notification_sbirs# is 1>checked</cfif>></td>
						  <td class="feed_sub_header" style="font-weight: normal;">SBIR/STTRs</td>
						  <td width=30><input type="checkbox" name="usr_notification_challenges" style="width: 20px; height: 20px; margin-right: 10px;" <cfif #edit.usr_notification_challenges# is 1>checked</cfif>></td>
						  <td class="feed_sub_header" style="font-weight: normal;">Challenges</td>
					   </tr>

					  </table>

					</td>
				</tr>

				<tr><td class="feed_sub_header">Frequency</td>
					<td>
					<select name="usr_notification_frequency" class="input_select">
					 <option value="Daily">Daily
					 <option value="Weekly">Weekly
					</select>
					</td></tr>

				<input type="hidden" name="i" value="#i#">

			    </cfoutput>

			    <tr><td height=10></td></tr>

				<tr><td class="feed_sub_header" valign=top>Distribution<br>List</td>
					<td>
					<select name="usr_notification_email_list" class="input_select" required multiple style="width: 200px; height: 200px;">
                     <cfoutput query="distribution">
					 <option value=#usr_id# <cfif listfind(edit.usr_notification_email_list, usr_id)>selected</cfif>>#usr_last_name#, #usr_first_name#
					 </cfoutput>
					</select>
					</td></tr>

			    <tr><td height=10></td></tr>

				<tr><td colspan=3><hr></td></tr>
				<tr><td height=10></td></tr>
				<tr><td></td><td>
				<input type="submit" name="button" class="button_blue_large" value="Update">
                &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">
				</td></tr>
			   </form>
			  </table>

              </td></tr>

           </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>