<cfinclude template="/exchange/security/check.cfm">

<cfquery name="notifications" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_notification
 where usr_notification_usr_id = #session.usr_id# and
       usr_notification_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=20></td></tr>
		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_header" style="font-size: 30;">Add Notification</td>
			    <td class="feed_sub_header" align=right><a href="index.cfm?l=3">Return</a></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Please select the type of notification you would like to create.</td></tr>
			<tr><td height=10></td></tr>
		   </table>

            <form action="select.cfm" method="post">

	           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr>
				    <td class="feed_sub_header" width=40 style="padding-bottom: 0px; margin-bottom: 0px;"><input type="radio" checked name="usr_notification_type" value="Open Opportunities" style="width: 22px; height: 22px;"></td>
				    <td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">Open Opportunities</td>
				 </tr>

				 <tr>
				    <td class="feed_sub_header" style="padding-top: 5px; margin-top: 5px;"></td>
				    <td class="feed_sub_header" style="padding-top: 5px; margin-top: 5px; font-weight: normal;">Notifies you when new Opportunities are posted to the Exchange.  This option also allows you to select the type of Opportunities you would like to get notified about and how often.</td>
				 </tr>

				 <tr>
				    <td class="feed_sub_header" width=40 style="padding-bottom: 0px; margin-bottom: 0px;"><input type="radio" name="usr_notification_type" value="Award Notification" style="width: 22px; height: 22px;"></td>
				    <td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">New Awards</td>
				 </tr>

				 <tr>
				    <td class="feed_sub_header" style="padding-top: 5px; margin-top: 5px;"></td>
				    <td class="feed_sub_header" style="padding-top: 5px; margin-top: 5px; font-weight: normal;">Notifies you when new contracts are awarded to vendors.</td>
				 </tr>

				 <tr>
				    <td class="feed_sub_header" width=40 style="padding-bottom: 0px; margin-bottom: 0px;"><input type="radio" name="usr_notification_type" value="Company" style="width: 22px; height: 22px;"></td>
				    <td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">Company Portfolio Updates</td>
				 </tr>

				 <tr>
				    <td class="feed_sub_header" style="padding-top: 5px; margin-top: 5px;"></td>
				    <td class="feed_sub_header" style="padding-top: 5px; margin-top: 5px; font-weight: normal;">Notifies you about new information that is posted to the Exchange for Companies in your Portfolio.</td>
				 </tr>

			<tr><td height=10></td></tr>
			<tr><td colspan=2><hr></td></tr>
			<tr><td height=10></td></tr>

            <tr><td></td>
                <td><input type="submit" name="button" class="button_blue_large" value="Continue >>"></td>
            </tr>

            </form>

   		   </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>