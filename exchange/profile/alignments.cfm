<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 left join company on company_id = usr_company_id
 where usr_id = #session.usr_id#
</cfquery>


  <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from market
   <cfif isdefined("session.hub")>
	   where market_hub_id = #session.hub#
   <cfelse>
       where market_hub_id is null
   </cfif>
  </cfquery>

  <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from sector
   <cfif isdefined("session.hub")>
	   where sector_hub_id = #session.hub#
   <cfelse>
       where sector_hub_id is null
   </cfif>
   order by sector_name
  </cfquery>

  <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from topic
   <cfif isdefined("session.hub")>
	   where topic_hub_id = #session.hub#
   <cfelse>
       where topic_hub_id is null
   </cfif>
   order by topic_name
  </cfquery>

	  <cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          (align_hub_id = #session.hub#) and
	         </cfif>
			 (align_type_id = 2)
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          (align_hub_id = #session.hub#) and
	         </cfif>
			 (align_type_id = 3)
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          (align_hub_id = #session.hub#) and
	         </cfif>
			 (align_type_id = 4)
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Alignments</td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">


			   Alignments allow you to associate with Markets, Accounts, or Capabilities & Services.


			   </td></tr>

			   <tr><td height=10></td></tr>

				   <cfif isdefined("u")>
					<cfif u is 1>
					 <tr><td class="feed_sub_header"><font color="green"><b>Alignments have been updated.</b></font></td></tr>
					 <tr><td height=10></td></tr>
					</cfif>
				   </cfif>

			   </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="alignments_db.cfm" method="post">

        <tr><td colspan=2>

			<table cellspacing=0 cellpadding=0 border=0>

				<tr><td class="feed_sub_header"><b>Market Interests</b></td>
					<td class="feed_sub_header"><b>Accounts / Sector Interests</b></td>
					<td class="feed_sub_header"><b>Capability and Service Interests</b></td>
				</tr>

			<tr>

				 <td width=200>
					  <select class="input_select" name="market_id" style="width: 150px; height: 200px;" multiple required>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=400>
					  <select class="input_select" style="width: 350px; height: 200px;" name="sector_id" multiple required>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select class="input_select" style="width: 350px; height: 200px;" name="topic_id" multiple required>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
                <tr><td class="link_small_gray" colspan=2>To select multiple items please use the SHIFT or CTRL key.</td></tr>

				<tr><td>&nbsp;</td></tr>
				<tr><td colspan=3><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>

              </form>

			  </table>

		   </td></tr>

 		  </table>

              </td></tr>

           </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>