<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 left join company on company_id = usr_company_id
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="page" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from page
 where page_homepage = 1
 order by page_homepage_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Default Home Page Settings</td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Please choose the page that you will default to when you signin.</td></tr>

				   <cfif isdefined("u")>
					<cfif u is 1>
					 <tr><td class="feed_sub_header"><font color="green"><b>Default home page has been updated.</b></font></td></tr>
					 <tr><td height=10></td></tr>
					</cfif>
				   </cfif>

			   </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <form action="homepage_db.cfm" method="post">

               <cfoutput query="page">

                   <cfif mid(page_homepage_order,3,1) is 0>

				   <tr><td width=40><input type="radio" name="usr_default_home_page" style="width: 20px; height: 20px;" value=#page_id# <cfif #profile.usr_default_home_page# is #page_id#>checked</cfif>></td>
				       <td class="feed_sub_header" colspan=2>#page_name#</td></tr>

				   <cfelse>

				   <tr><td width=40></td>
				       <td width=40><input type="radio" name="usr_default_home_page" style="width: 20px; height: 20px;" value=#page_id# <cfif #profile.usr_default_home_page# is #page_id#>checked</cfif>></td>
				       <td class="feed_sub_header">#page_name#</td></tr>

				   </cfif>

   		       </cfoutput>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2>
                   <input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>
                   </td></tr>


               </form>


   		  </table>

              </td></tr>

           </table>

           </td></tr>

        </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>