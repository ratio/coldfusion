<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Cancel">
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfif isdefined("align_type_value")>

<cftransaction>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete align
		  where (align_type_id = 1) and
		        (align_usr_id = #session.usr_id#)
		        <cfif isdefined("session.hub")>
		         and (align_hub_id = #session.hub#)
		        <cfelse>
		         and (align_hub_id is null)
		        </cfif>

		</cfquery>

		<cfloop index="element" list=#align_type_value#>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.usr_id#, 1, #element#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

</cftransaction>

</cfif>

<cflocation URL="index.cfm?u=5" addtoken="no">


