<cfinclude template="/exchange/security/check.cfm">

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select company_name, company_id from usr
 join company on company_id = usr_company_id
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 left join company on company_id = usr_company_id
 where usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <form action="/exchange/profile/change_company_db.cfm" method="post">

				   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Change Company</td>
					   <td align=right class="feed_sub_header"></td></tr>
				   </tr>
				   <tr><td colspan=3><hr></td></tr>

				   <tr><td height=10></td></tr>

				   <tr><td class="feed_sub_header" width=200>Current Company</td></tr>
				   <tr><td class="feed_sub_header" style="font-weight: normal;">#comp.company_name#</td></tr>

				   <tr><td height=10></td></tr>

				   <tr><td colspan=3><hr></td></tr>

				   <tr><td height=20></td></tr>

				   <tr><td><input type="submit" class="button_blue_large" name="button" value="Remove Company Affiliation" onclick="return confirm('Remove Company Affiliation?\r\nBy doing so you will no longer be associated with this company.  Are you sure you want to proceed?');">
				   &nbsp;&nbsp;&nbsp;<input type="submit" class="button_blue_large" name="button" value="Associate With A Different Company"></td></tr>

				   <tr><td height=20></td></tr>

               </form>

			   </table>

              </td></tr>

           </table>

 		  </div>

         </cfoutput>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>