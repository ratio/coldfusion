<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="lenses" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from lens
 <cfif isdefined("session.hub")>
  where lens_hub_id = #session.hub#
 <cfelse>
  where lens_hub_id is null
 </cfif>
 order by lens_order
</cfquery>

<cfquery name="align" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select align_type_value from align
 where (align_usr_id = #session.usr_id#) and
		 <cfif isdefined("session.hub")>
          (align_hub_id = #session.hub#) and
		 <cfelse>
		  (align_hub_id is null) and
		 </cfif>
       (align_type_id = 1)
</cfquery>

<cfset #lens_list# = #valuelist(align.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=200>

		   <cfinclude template="left_menu.cfm">

           </td><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header"><b>Update Feed Lenses</b></td><td align=right class="feed_option"><a href="index.cfm">Return</a></td></tr>
			   <tr><td height=5></td></tr>

           </table>

	      <form action="feeds_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_option" colspan=3>Please select the Feed Lenses that you want to subscribe to.</td></tr>

           <tr><td class="feed_option" width=90><b>Subscribe</b></td>
               <td class="feed_option"><b>Feed Lens</b></td>
               <td class="feed_option"><b>Description</b></td>
           </tr>

           <cfset #bgcolor# = "ffffff">
           <cfset #counter# = 1>
           <cfoutput query="lenses">
            <cfif #counter# is 1>
             <cfset #bgcolor# = "ffffff">
            <cfelse>
             <cfset #bgcolor# = "e0e0e0">
            </cfif>

            <tr bgcolor=#bgcolor#>
             <td align=center><input type="checkbox" name="align_type_value" value=#lenses.lens_id# <cfif listfind(lens_list,lenses.lens_id)>Checked</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td width=300 class="feed_option">#lenses.lens_name#</td>
             <td class="feed_option">#lenses.lens_desc#</td>
            </tr>

            <cfif #counter# is 1>
             <cfset #counter# = 0>
            <cfelse>
             <cfset #counter# = 1>
            </cfif>

           </cfoutput>

           <tr><td>&nbsp;</td></tr>

           <tr><td colspan=3>
           <input class="button_blue" type="submit" name="button" value="Update">
           </td></tr>

 		  </table>

 		  </form>
















		   </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

