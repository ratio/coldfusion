<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update hub_xref
 set hub_xref_default = null
 where hub_xref_usr_id = #session.usr_id#
</cfquery>

<cfif #hub_default# is 0>

<cfelse>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update hub_xref
	 set hub_xref_default = 1
	 where hub_xref_usr_id = #session.usr_id# and
	       hub_xref_hub_id = #hub_default#
	</cfquery>

</cfif>

</cftransaction>

<cflocation URL="hubs.cfm?u=1" addtoken="no">


