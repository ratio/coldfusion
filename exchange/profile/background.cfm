<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Change Banner</td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td height=10></td></tr>

			   </table>

               <form action="/exchange/profile/save_background.cfm" method="post" enctype="multipart/form-data" >

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td class="feed_sub_header" valign=top>Banner</td></tr>

				   <tr>

					<cfif #profile.usr_background# is "">
					  <td class="feed_sub_header" style="font-weight: normal;">

					  <input type="file" id="image" onchange="validate_img()" name="usr_background">

					  </td></tr>
					<cfelse>
					  <td class="feed_sub_header" style="font-weight: normal;"><img src="#media_virtual#/#profile.usr_background#" width=300><br><br>
					      <b>Change Banner</b><br><br>
					      <input type="file" id="image" onchange="validate_img()" name="usr_background"><br><br>
					      <input type="checkbox" name="remove_photo" style="width: 20px; height: 20px;">&nbsp;or, check to remove banner</td></tr>
					 </cfif>

                <tr><td height=10></td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">For best results, banner image should be 1,500 pixels wide and 300 pixels tall.</td></tr>
                <tr><td height=20></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

		        <tr><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>

			   </table>

               <cfif isdefined("l")>
                <input type="hidden" name="l" value=1>
               </cfif>

			   </form>

			   </table>












              </td></tr>

           </table>

 		  </div>

         </cfoutput>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>