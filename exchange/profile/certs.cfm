<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from cert_cat
  where cert_cat_hub_id = #session.hub#
  order by cert_cat_order
</cfquery>

<cfquery name="certs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select usr_cert_cert_id from usr_cert
  where usr_cert_hub_id = #session.hub# and
        usr_cert_usr_id = #session.usr_id#
</cfquery>

<cfif certs.recordcount is 0>
 <cfset clist = 0>
<cfelse>
 <cfset clist = valuelist(certs.usr_cert_cert_id)>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>My Certifications</td></tr>
			   <tr><td><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <tr><td class="feed_sub_header" style="color: green;" colspan=3>Certifications have been successfully updated.</td></tr>
                <tr><td height=20></td></tr>
               <cfelse>
               			   <tr><td height=10></td></tr>

               </cfif>

               <form action="certs_save.cfm" method="post">

               <cfloop query="cats">

				 <cfquery name="certs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				   select * from certification
				   where certification_cat_id = #cats.cert_cat_id#
				   order by certification_name
				 </cfquery>

               <cfoutput>

                <tr><td class="feed_header" colspan=3>#cats.cert_cat_name#</td></tr>
                <tr><td height=10></td></tr>

               </cfoutput>

                <cfoutput query="certs">
                 <tr>
                    <td width=30>&nbsp;</td>
                    <td width=50><input type="checkbox" name="cert_id" value=#certification_id# style="width: 22px; height: 22px;" <cfif listfind(clist,certification_id)>checked</cfif>></td>
                    <td class="feed_sub_header" style="font-weight: normal;">#certification_name#</td></tr>
                </cfoutput>

               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

               </cfloop>

               <cfif cats.recordcount GT 0>
	               <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Save"></td></tr>
               <cfelse>
                   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Certifications Categories have been created.  Please contact your System Administrator.</td></tr>
               </cfif>

               </form>

			   </table>


              </td></tr>

           </table>

 		  </div>


	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>