<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 left join company on company_id = usr_company_id
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="userhubs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 left join hub_xref on hub_xref_hub_id = hub_id
 where hub_status = 1 and
       hub_xref_usr_id = #session.usr_id# and
       hub_xref_active = 1
 order by hub_name
</cfquery>

<cfquery name="exchange" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(hub_xref_id) as total from hub_xref
 where hub_xref_default = 1 and
       hub_xref_usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Default Community</td></tr>
				   <tr><td colspan=2><hr></td></tr>

			       <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Communities are groups that focus on specific topics or interests.  Your default Hub or Node is where you will login to.</td></tr>

				   <cfif isdefined("u")>
					<cfif u is 1>
					 <tr><td height=10></td></tr>
					 <tr><td class="feed_sub_option"><font color="green"><b>Default Community.</b></font></td></tr>
					</cfif>
				   </cfif>

               </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif #userhubs.recordcount# is 0>

           <tr><td class="feed_sub_header">You have not joined any Communities.</td></tr>

           <cfelse>

            <tr><td>&nbsp;</td></tr>

            <tr><td width=100 class="feed_sub_header" align=center><b>My Default</b></td></tr>
            <tr><td height=5></td></tr>

            <form action="hub_set.cfm" method="post">

			<tr><td align=center><input type="radio" name="hub_default" style="width: 23px; height: 23px;" value=0 <cfif #exchange.total# is 0>checked</cfif>></td>
			    <td valign=middle width=150 align=center>

					<img src="/images/exchange_logo_black.png" width=75 border=0>

					</td>

					<td width=40>&nbsp;</td>

					<td valign=top>

					<table cellspacing=0 cellpadding=0 border=0 width=100%>
					 <tr><td class="feed_header" valign=top>RATIO EXCHANGE</td>
					 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>

                      The Exchange is an online marketplace that simplifies and accelerates how companies, startups, entrepreneurs and organizations capitalize on $1 billion of government spending each year to increase revenue and growth. The Exchange connects buyers, suppliers, regional hubs, brokers, investors and partners to:
                      <ul>
                       <li><b>SOURCE</b> � discover edge, new, emerging, and mainstream companies and capabilities who are innovating and delivering their products and services every day.
                       <li><b>SELL</b> � broadcast your products and services to the market, get discovered by hundreds of thousands of buyers and partners who need to solve a problem or competitively differentiate.
                       <li><b>SOLVE</b> � discover new opportunities to align and position your products and services with buyers and partners who are trying to solve today�s and tomorrow�s challenges.
                       <li><b>INVEST</b> � find the needle in the haystack and invest in their success.
                      </ul>

					 </td><tr>
					</table>

					</td></tr>
					 <tr><td height=5></td></tr>
                     <tr><td colspan=4><hr></td></tr>
                     <tr><td height=5></td></tr>

			<cfoutput query="userhubs">

			<tr><td align=center><input type="radio" style="width: 23px; height: 23px;" name="hub_default" value=#userhubs.hub_xref_hub_id# <cfif #userhubs.hub_xref_default# is 1>checked</cfif>></td><td valign=middle align=center>

					<cfif #userhubs.hub_logo# is "">
							<img style="border-radius: 8px;" src="/images/hub_stock_logo.png" width=100 border=0>
					<cfelse>
							<img style="border-radius: 8px;" src="#media_virtual#/#userhubs.hub_logo#" width=100 border=0>
					</cfif>

					</td>

					<td width=20>&nbsp;</td>

					<td valign=top>

					<table cellspacing=0 cellpadding=0 border=0 width=100%>
					 <tr><td class="feed_header" valign=top>#userhubs.hub_name#</a></td>
					     <td class="feed_header" valign=top align=right>#userhubs.hub_city#, #userhubs.hub_state#</td></tr>
					 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#userhubs.hub_desc#</td><tr>
                     <cfif #userhubs.hub_tags# is not "">
	                     <tr><td class="feed_option" colspan=2><i>#userhubs.hub_tags#</i></td></tr>
                     </cfif>
					</table>

					</td></tr>
					<tr><td height=10></td></tr>
                    <tr><td colspan=4><hr></td></tr>
                    <tr><td height=10></td></tr>

				  </cfoutput>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Set As Default" vspace=10></td></tr>

             </form>

   		  </table>

         </cfif>

           </td></tr>

        </table>

          </td></tr>

  </table>

 		  </div>

	  </td></tr>

  </table>



<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>