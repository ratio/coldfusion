<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

  <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from market
   <cfif isdefined("session.hub")>
	   where market_hub_id = #session.hub#
   <cfelse>
       where market_hub_id is null
   </cfif>
  </cfquery>

  <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from sector
   <cfif isdefined("session.hub")>
	   where sector_hub_id = #session.hub#
   <cfelse>
       where sector_hub_id is null
   </cfif>
   order by sector_name
  </cfquery>

  <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from topic
   <cfif isdefined("session.hub")>
	   where topic_hub_id = #session.hub#
   <cfelse>
       where topic_hub_id is null
   </cfif>
   order by topic_name
  </cfquery>

	  <cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          (align_hub_id = #session.hub#) and
	         </cfif>
			 (align_type_id = 2)
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          (align_hub_id = #session.hub#) and
	         </cfif>
			 (align_type_id = 3)
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          (align_hub_id = #session.hub#) and
	         </cfif>
			 (align_type_id = 4)
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=200>

		   <cfinclude template="left_menu.cfm">

           </td><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header"><b>Update Alignments</b></td><td align=right class="feed_option"><a href="index.cfm">Return</a></td></tr>
			   <tr><td class="feed_option" colspan=2>Please select the markets, sectors, and capabilities and services that you are interested in.</td></tr>
			   <tr><td>&nbsp;</td></tr>

           </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="alignments_db.cfm" method="post">

        <tr><td colspan=2>

			<table cellspacing=0 cellpadding=0 border=0>

			<tr><td><b>Market Interest</b></td>
				<td><b>Sectors Focus</b></td>
				<td><b>Capabilities & Services</b></td>
				</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>

				 <td width=180>
					  <select style="font-size: 12px; width: 160px; height: 260px; padding-left: 10px; padding-top: 5px;" name="market_id" multiple required>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=220>
					  <select style="font-size: 12px; width: 200px; height: 260px; padding-left: 10px; padding-top: 5px;" name="sector_id" multiple required>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select style="font-size: 12px; width: 275px; height: 260px; padding-left: 10px; padding-top: 5px;" name="topic_id" multiple required>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
                <tr><td class="text_xsmall" colspan=2><b><i>To select multiple items please use the SHIFT or CTRL key.</i></b></td></tr>

				<tr><td>&nbsp;</td></tr>
				<tr><td colspan=3><input class="button_blue" type="submit" name="button" value="Update" vspace=10></td></tr>

              </form>

			  </table>

















		   </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

