<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("remove_photo")>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select usr_background from usr
	  where usr_id = #session.usr_id#
	</cfquery>

	<cfif FileExists("#media_path#\#remove.usr_background#")>
	 <cffile action = "delete" file = "#media_path#\#remove.usr_background#">
	</cfif>

</cfif>

<cfif #usr_background# is not "">

	<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select usr_background from usr
	  where usr_id = #session.usr_id#
	</cfquery>

	<cfif #getfile.usr_background# is not "">
	 <cfif FileExists("#media_path#\#getfile.usr_background#")>
	  <cffile action = "delete" file = "#media_path#\#getfile.usr_background#">
	 </cfif>
	</cfif>

	<cffile action = "upload"
	 fileField = "usr_background"
	 destination = "#media_path#"
	 nameConflict = "MakeUnique">

</cfif>

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
update usr

  <cfif isdefined("remove_photo")>
   set usr_background = null
  <cfelse>
	  <cfif #usr_background# is not "">
	   set usr_background = '#cffile.serverfile#'
	  <cfelse>
	   set usr_background = null
	  </cfif>
  </cfif>

where usr_id = #session.usr_id#
</cfquery>

<cflocation URL="/exchange/profile/index.cfm?u=5" addtoken="no">