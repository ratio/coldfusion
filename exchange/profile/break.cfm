<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update usr
 set usr_company_id = null,
     usr_company_name = null
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="check_company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_id from company
 where company_id = #session.company_id# and
       company_admin_id = #session.usr_id#
</cfquery>

<cfif check_company.recordcount is 1>

  <cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   update company
   set company_admin_id = null,
       company_registered = null
   where company_id = #session.company_id# and
         company_admin_id = #session.usr_id#
  </cfquery>

</cfif>

<cfset session.company_id = 0>

<cflocation URL="index.cfm?u=10" addtoken="no">