<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<!--- Certifications --->

<cfquery name="certs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select certification_name from usr_cert
  join certification on certification_id = usr_cert_cert_id
  where usr_cert_hub_id = #session.hub# and
        usr_cert_usr_id = #session.usr_id#
        order by certification_name
</cfquery>

<!--- Alignments --->

<cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select market_name from align
join market on market_id = align_type_value
where (align_usr_id = #session.usr_id#) and
	 <cfif isdefined("session.hub")>
	  (align_hub_id = #session.hub#) and
	 </cfif>
	 (align_type_id = 2)
</cfquery>

<cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select sector_name from align
join sector on sector_id = align_type_value
where (align_usr_id = #session.usr_id#) and
	 <cfif isdefined("session.hub")>
	  (align_hub_id = #session.hub#) and
	 </cfif>
	 (align_type_id = 3)
</cfquery>

<cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select topic_name from align
join topic on topic_id = align_type_value
where (align_usr_id = #session.usr_id#) and
	 <cfif isdefined("session.hub")>
	  (align_hub_id = #session.hub#) and
	 </cfif>
	 (align_type_id = 4)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/profile/profile_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=200>

		   <cfinclude template="/exchange/profile/profile_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>My Profile</td>
                   <td align=right class="feed_sub_header" width=400><a href="/exchange/profile/edit.cfm"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Profile" title="Edit Profile"></a>&nbsp;&nbsp;<a href="/exchange/profile/edit.cfm">Update Profile</a></td></tr>
			   </tr>
			   <tr><td colspan=3><hr></td></tr>

               </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Password successfully changed.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Profile successfully updated.</b></font></td></tr>
                <cfelseif u is 3>
                  <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Notification settings successfully updated.</b></font></td></tr>
                <cfelseif u is 4>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Market, Sector, and Capability & Services alignments have been successfully updated.</b></font></td></tr>
                <cfelseif u is 5>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Profile background has been saved.</b></font></td></tr>
                <cfelseif u is 6>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Profile has been successfully updated.</b></font></td></tr>
                <cfelseif u is 9>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Your company association has been removed.</b></font></td></tr>
                <cfelseif u is 9>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>You have removed your associated with your Company.</b></font></td></tr>
                <cfelseif u is 11>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Certifications have been successfully updated.</b></font></td></tr>
                 </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" width=200>Name</td>
			       <td class="feed_sub_header" style="font-weight: normal;">#profile.usr_first_name# #profile.usr_last_name#</td></tr>

               <cfif profile.usr_company_id is "">

			   <tr><td class="feed_sub_header" width=200>Company</td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_company_name# is "">Not Provided<cfelse>#profile.usr_company_name#</cfif>

			       &nbsp;&nbsp;<a href="/exchange/company/setup/">Associate with Company</a>


			       </td></tr>

               <cfelse>

				<cfquery name="c_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  select company_name from company
				  where company_id = #profile.usr_company_id#
				</cfquery>

			   <tr><td class="feed_sub_header" width=200>Company</td>
			       <td class="feed_sub_header" style="font-weight: normal;">#c_info.company_name#

			       &nbsp;&nbsp;<a href="/exchange/company/setup/"><img src="/images/icon_edit.png" width=15 hspace=10>Change Company</a>
			       &nbsp;|&nbsp;<a href="break.cfm" onclick="return confirm('No Company?\r\nAre you sure you want remove your Company association?');"><img src="/images/delete.png" width=15 hspace=10>Remove Association</a>


			       </td></tr>

               </cfif>

			   <tr><td class="feed_sub_header">Title</td>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_title# is "">Not Provided<cfelse>#profile.usr_title#</cfif></td></tr>


			   <tr><td class="feed_sub_header" valign=top>Interests</td>

               </cfoutput>

			   <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <tr><td valign=top width=20%>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                        <tr><td class="feed_sub_header">Markets</td></tr>

                         <cfif market_alignment.recordcount is 0>
	                         <tr><td class="feed_sub_header" style="font-weight: normal;">No Alignments Selected
	                     <cfelse>

							 <tr><td class="feed_sub_header" style="font-weight: normal;">
							 <cfoutput query="market_alignment">
							  #market_name#<br>
							 </cfoutput>
							 </td></tr>

                         </cfif>

					   </table>

			        </td><td width=50>&nbsp;</td><td valign=top width=20%>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                        <tr><td class="feed_sub_header">Accounts / Sectors</td></tr>


                         <cfif sector_alignment.recordcount is 0>

	                         <tr><td class="feed_sub_header" style="font-weight: normal;">No Alignments Selected

	                     <cfelse>

							 <tr><td class="feed_sub_header" style="font-weight: normal;">
							 <cfoutput query="sector_alignment">
							  #sector_name#<br>
							 </cfoutput>
							 </td></tr>

                         </cfif>


					   </table>

			        </td><td width=50>&nbsp;</td><td valign=top width=20%>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                        <tr><td class="feed_sub_header">Capabilites and Services</td></tr>

                         <cfif topic_alignment.recordcount is 0>
	                         <tr><td class="feed_sub_header" style="font-weight: normal;">No Alignments Selected
	                     <cfelse>

                         <tr><td class="feed_sub_header" style="font-weight: normal;">
                         <cfoutput query="topic_alignment">
                          #topic_name#<br>
                         </cfoutput>
                         </td></tr>

                         </cfif>

					   </table>
                    </td><td width=35%>&nbsp;</td></tr>

			   </table>

			   </td></tr>

			   <cfoutput>
               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" valign=top>Contact Information</td>
			       <td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=25 valign=middle>&nbsp;&nbsp;#tostring(tobinary(profile.usr_email))#</td></tr>
			   <tr><td></td><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon-phone_2.png" alt="Work Phone" title="Work Phone" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.usr_phone# is "">Not Provided<cfelse>#profile.usr_phone#</cfif></td></tr>
			   <tr><td></td><td class="feed_sub_header" style="font-weight: normal;"><img src="/images/icon_cell_phone.png" alt="Cell Phone" title="Cell Phone" width=25 valign=middle>&nbsp;&nbsp;<cfif #profile.usr_cell_phone# is "">Not Provided<cfelse>#profile.usr_cell_phone#</cfif></td></tr>
			   <tr><td class="feed_sub_header" valign=top><b>Address</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;" valign=top>
			       <cfif #trim(profile.usr_address_line_1)# is not ''>
			        #trim(profile.usr_address_line_1)#
			       </cfif>
			       <cfif #trim(profile.usr_address_line_2)# is not ''>
			       #trim(profile.usr_address_line_2)#
			       </cfif>
			       <br>#profile.usr_city# #profile.usr_state# #profile.usr_zip#</td></tr>
			   <tr><td class="feed_sub_header"><img src="/images/icon_linkedin.png" width=20 align=absmiddle>&nbsp;&nbsp;<b>LinkedIn</b></td>
		       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_linkedin# is not ""><a href="#profile.usr_linkedin#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer">#profile.usr_linkedin#</a><cfelse>Not Provided</cfif></td></tr>
			   <tr><td class="feed_sub_header"><img src="/images/icon_facebook.png" width=20 align=absmiddle>&nbsp;&nbsp;<b>Facebook</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_facebook# is not ""><a href="#profile.usr_facebook#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer">#profile.usr_facebook#</a><cfelse>Not Provided</cfif></td></tr>
			   <tr><td class="feed_sub_header"><img src="/images/icon_twitter.png" width=20 align=absmiddle>&nbsp;&nbsp;<b>Twitter</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_twitter# is not ""><a href="#profile.usr_twitter#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer">#profile.usr_twitter#</a><cfelse>Not Provided</cfif></td></tr>
               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

			   <tr><td class="feed_sub_header" valign=top><b>My Keywords</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_keywords# is not "">#profile.usr_keywords#<cfelse>Not provided</cfif></td></tr>

			   <tr><td class="feed_sub_header" valign=top><b>About Me</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_about# is not "">#replace(profile.usr_about,"#chr(10)#","<br>","all")#<cfelse>Not provided</cfif></td></tr>

               </cfoutput>

			   <tr><td class="feed_sub_header" valign=top><b>Certifications</b></td>

					   <td class="feed_sub_header" style="font-weight: normal;" valign=top>

						   <cfif certs.recordcount is 0>
							No Certifications Listed
						   <cfelse>

							 <cfoutput query="certs">
							  <li>#certification_name#</li>
							 </cfoutput>

						   </cfif></td></tr>

               <cfoutput>

			   <tr><td class="feed_sub_header" valign=top><b>Experience</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_experience# is not "">#replace(profile.usr_experience,"#chr(10)#","<br>","all")#<cfelse>Not provided</cfif></td></tr>
			   <tr><td class="feed_sub_header" valign=top><b>Education</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_education# is not "">#replace(profile.usr_education,"#chr(10)#","<br>","all")#<cfelse>Not provided</cfif></td></tr>
				   <tr><td class="feed_sub_header" valign=top><b>Hobbies</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;"><cfif #profile.usr_hobbies# is not "">#replace(profile.usr_hobbies,"#chr(10)#","<br>","all")#<cfelse>Not provided</cfif></td></tr>


	              <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>
			   <tr><td class="feed_sub_header" valign=top><b>Profile Privacy</b></td>
			       <td class="feed_sub_header" style="font-weight: normal;">

			       <cfif #profile.usr_profile_display# is 1>
			       Public (profile information is displayed)
			       <cfelse>
			       Private (profile information is not displayed)
			       </cfif>

			       </td></tr>

               <tr><td height=20></td></tr>

			   </table>

              </td></tr>

           </table>

 		  </div>

         </cfoutput>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>