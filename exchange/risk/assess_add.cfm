<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<!--- Lookups --->

<cfquery name="impact" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from impact
  order by impact_order
</cfquery>

<cfquery name="impact_like" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from impact_like
  order by impact_like_order
</cfquery>

<cfquery name="area" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from impact_area
  order by impact_area_name
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=top height=24>CREATE RISK ASSESSMENT</td>
		      <td align=right><a href="/exchange/sourcing/risks/risk_assessment.cfm"><img src="/images/close.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>

         <form action="assess_db.cfm" method="post">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr>
			 <td width=200 class="feed_sub_header" valign=middle>Assessment Name</td>
			 <td><input type="text" name="impact_assessment_name" class="input_text" style="width: 700px;"></td>
		  </tr>

		  <tr>
		     <td class="feed_sub_header" valign=top width=200>Comments</td>
		     <td colspan=3><textarea name="impact_assessment_comments" class="input_text" style="width: 700px; height: 100px;"></textarea></td>
		  </tr>

		  <tr><td height=10></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr><td height=10></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfloop query="area">

			  <cfoutput>

				  <tr>
					  <td class="feed_sub_header" width=200>#area.impact_area_name#</td>
					  <td class="feed_sub_header">Impact</td>
					  <td class="feed_sub_header">Likelihood</td>
				  </tr>

			  </cfoutput>

				  <tr>
				     <td></td>
				     <td width=250>

					 <select name="<cfoutput>#area.impact_area_id#</cfoutput>impact_assessment_score_impact" class="input_select" style="width: 200px;">
					  <cfoutput query="impact">
					   <option value=#impact_id#>#impact_name#
					  </cfoutput>
					 <select>

				     </td>

				     <td>

					 <select name="<cfoutput>#area.impact_area_id#</cfoutput>impact_assessment_score_like" class="input_select" style="width: 200px;">
					  <cfoutput query="impact_like">
					   <option value=#impact_like_id#>#impact_like_name#
					  </cfoutput>
					 <select>

				     </td>
				  </tr>

              <tr><td height=20></td></tr>
			  <tr><td colspan=3><hr></td></tr>

          </cfloop>

		  <tr><td height=20></td></tr>
		  <tr><td></td><td><input type="submit" class="button_blue_large" name="button" value="Create Assessment"></td></tr>

		 </table>

		 </form>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>