<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<!--- Lookups --->

<cfquery name="risk" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk
  left join risk_status on risk_status.risk_status_id = risk.risk_status_id
  left join usr on usr_id = risk_created_by
  left join risk_level on risk_level.risk_level_id = risk.risk_level_id
  left join risk_type on risk_type.risk_type_id = risk.risk_type_id
  where risk_id = #session.risk_id#
</cfquery>

<cfquery name="cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_mitigate_cat
  order by risk_mitigate_cat_name
</cfquery>

<cfquery name="level" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_level
  order by risk_level_order
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

      <cfoutput>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header">#ucase(risk.risk_name)#</td>
		      <td align=right class="feed_sub_header">

		      <cfif isdefined("l")>
		        <a href="risk_open.cfm">Return</a>
		      <cfelse>
		        <a href="risk_mitigate.cfm">Return</a>
		      </cfif>

		      </td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>

       </cfoutput>

         <form action="risk_mitigate_db.cfm" method="post">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header" valign=middle>Strategy Name</td>
				 <td><input type="text" name="risk_mitigate_name" class="input_text" style="width: 700px;" placeholder="Please provide a high level name for this risk mitigation strategy."></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Describe<br>Strategy</td>
				 <td><textarea name="risk_mitigate_desc" class="input_text" style="width: 1100px; height: 150px;" placeholder="Please provide a full description of the risk mitigation strategy."></textarea></td>
			  </tr>

			  <tr><td class="feed_sub_header" width=200>Mitigate Through</td>
                 <td>
				 <select name="risk_mitigate_cat_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="cat">
				   <option value=#cat.risk_mitigate_cat_id#>#risk_mitigate_cat_name#
				  </cfoutput>
				 <select>
				 </td></tr>

			  <tr><td class="feed_sub_header" width=200>Level of Risk</td>
                 <td>
				 <select name="risk_mitigate_level" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="level">
				   <option value=#risk_level_id#>#risk_level_name#
				  </cfoutput>
				 <select>
				 </td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Planned Return / Value</td>
				 <td><textarea name="risk_mitigate_return" class="input_text" style="width: 1100px; height: 80px;" placeholder="Please provide a description of the estimated return for this mitigation strategy."></textarea></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Assumptions</td>
				 <td><textarea name="risk_mitigate_assumptions" class="input_text" style="width: 1100px; height: 80px;" placeholder="Please provide a list of all assumptions for this risk mitigation strategy."></textarea></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Estimated Cost</td>
				 <td><input type="number" class="input_date" name="risk_mitigate_cost" step="1"></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Start Date</td>
				 <td><input type="date" class="input_date" name="risk_mitigate_start_date"></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>End Date</td>
				 <td><input type="date" class="input_date" name="risk_mitigate_end_date"></td>
			  </tr>

			  <tr><td height=10></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td height=10></td></tr>

			  <tr><td></td><td><input type="submit" class="button_blue_large" name="button" value="Add Strategy"></td></tr>

		 </table>

		 </form>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>