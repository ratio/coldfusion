<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=2" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfquery name="division" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from division
  order by division_name
</cfquery>

<cfquery name="risk" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk
  where risk_id = #session.risk_id#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header"><cfoutput>#ucase(risk.risk_name)#</cfoutput>:  ADD RISK TO OUTCOME(S)</td>
		      <td class="feed_sub_header" align=right></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr><td height=10></td></tr>
		 </table>

		<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from outcome
		  left join division on division_id = outcome_division_id
		  left join resource on resource_id = outcome_manager_id
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif info.recordcount is 0>
	         <tr><td class="feed_sub_header">No Outcomes have been created.</td></tr>
         <cfelse>

         <tr><td class="feed_sub_header" colspan=2>FILTER OPTIONS</td>
             <td align=right colspan=7>

				 <select name="risk_division" class="input_select" style="width: 200px;">
				  <option value=0>ALL BUSINESS LINES
				  <cfoutput query="division">
				   <option value=#division_id#>#ucase(division_name)#
				  </cfoutput>
				  <select>

				  &nbsp;&nbsp;<input type="submit" name="button" class="button_blue" value="Filter">

             </td></tr>

         <tr><td height=20></td></tr>

         <form action="add_to_outcome_db.cfm" method="post">

         <tr>
            <td class="feed_sub_header" width=70>SELECT</td>
            <td class="feed_sub_header" width=150><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=111<cfelse><cfif #sv# is 111>sv=110<cfelse>sv=111</cfif></cfif>"><b>OUTCOME ID</b></a><cfif isdefined("sv") and sv is 11>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" width=200><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>OUTCOME</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header">DESCRIPTION</td>
            <td class="feed_sub_header"><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>BUSINESS LINE</b></a><cfif isdefined("sv") and sv is 2>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header"><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>MANAGER</b></a><cfif isdefined("sv") and sv is 2>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=right><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>START DATE</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=right><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>ACHIEVE BY</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfloop query="info">

         <cfif counter is 0>
         <tr bgcolor="FFFFFF">
         <cfelse>
         <tr bgcolor="E0E0E0">
         </cfif>

         <cfoutput>

            <td class="data_row" valign=top>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" style="width: 22px; height: 22px;" name="outcome_id" value=#info.outcome_id#></td>
            <td class="data_row" valign=top width=100><a class="data_row" href="set.cfm?outcome_id=#info.outcome_id#"><b>#info.outcome_number#</b></a></td>
            <td class="data_row" valign=top width=200><a class="data_row" href="set.cfm?outcome_id=#info.outcome_id#"><b>#info.outcome_name#</b></a></td>
            <td class="data_row" valign=top width=500>#info.outcome_desc#</td>
            <td class="data_row" valign=top width=150>#info.division_name#</td>
            <td class="data_row" valign=top width=150>#info.resource_name#</td>
            <td class="data_row" valign=top width=120 align=right>#dateformat(info.outcome_start_date,'mm/dd/yyyy')#</td>
            <td class="data_row" valign=top align=right width=120>#dateformat(info.outcome_end_date,'mm/dd/yyyy')#</td>

         </cfoutput>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

         </cfif>

	     <tr><td colspan=5><input type="submit" name="button" class="button_blue_large" value="Add Risk to Outcome(s)">

         </form>

       </table>



      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>