<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add Strategy">

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert risk_mitigate
		  (
		  risk_mitigate_name,
		  risk_mitigate_cost,
		  risk_mitigate_cat_id,
		  risk_mitigate_return,
		  risk_mitigate_level,
		  risk_mitigate_desc,
		  risk_mitigate_assumptions,
		  risk_mitigate_start_date,
		  risk_mitigate_end_date,
		  risk_mitigate_created,
		  risk_mitigate_updated,
		  risk_mitigate_selected,
		  risk_mitigate_risk_id,
		  risk_mitigate_created_by
		  )
		  values
		  (
		  '#risk_mitigate_name#',
		  <cfif risk_mitigate_cost is "">null<cfelse>#risk_mitigate_cost#</cfif>,
		   #risk_mitigate_cat_id#,
		  '#risk_mitigate_return#',
		   #risk_mitigate_level#,
		  '#risk_mitigate_desc#',
		  '#risk_mitigate_assumptions#',
		  <cfif #risk_mitigate_start_date# is "">null<cfelse>'#risk_mitigate_start_date#'</cfif>,
		  <cfif #risk_mitigate_end_date# is "">null<cfelse>'#risk_mitigate_end_date#'</cfif>,
		  #now()#,
		  #now()#,
		  0,
		  #session.risk_id#,
		  #session.usr_id#
		  )
		</cfquery>

	<cflocation URL="risk_mitigate.cfm?u=1" addtoken="no">

<cfelseif button is "Update Strategy">

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update risk_mitigate
		  set risk_mitigate_name = '#risk_mitigate_name#',
		      risk_mitigate_return = '#risk_mitigate_return#',
		      risk_mitigate_cost = <cfif #risk_mitigate_cost# is "">null<cfelse>#risk_mitigate_cost#</cfif>,
		      risk_mitigate_cat_id = #risk_mitigate_cat_id#,
		      risk_mitigate_level = #risk_mitigate_level#,
		      risk_mitigate_desc = '#risk_mitigate_desc#',
		      risk_mitigate_assumptions = '#risk_mitigate_assumptions#',
		      risk_mitigate_start_date = <cfif #risk_mitigate_start_date# is "">null<cfelse>'#risk_mitigate_start_date#'</cfif>,
		      risk_mitigate_end_date = <cfif #risk_mitigate_end_date# is "">null<cfelse>'#risk_mitigate_end_date#'</cfif>,
		      risk_mitigate_updated = #now()#
          where risk_mitigate_id = #risk_mitigate_id#
		</cfquery>

	<cflocation URL="risk_mitigate.cfm?u=2" addtoken="no">

<cfelseif button is "Delete Strategy">

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete risk_mitigate
		  where risk_mitigate_id = #risk_mitigate_id#
		</cfquery>

    <cflocation URL="risk_mitigate.cfm?u=3" addtoken="no">

</cfif>