<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from impact_assessment
  where impact_assessment_risk_id = #session.risk_id#
  order by impact_assessment_date DESC
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
          </td><td valign=top>

      <div class="main_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=top height=24>RISK ASSESSMENTS</td>
		      <td align=right><a href="/exchange/sourcing/risks/risk_open.cfm"><img src="/images/close.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfif info.recordcount is 0>
           <tr><td class="feed_sub_header">No impact assessments have been recorded.</td>
               <td class="feed_option" align=right><b><a href="assess_add.cfm">CREATE ASSESSMENT</a></b></td></tr>
         <cfelse>

			<cfquery name="area" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from impact_area
			  order by impact_area_name
			</cfquery>

         <tr><td class="feed_option" colspan=8 align=right><b><a href="assess_add.cfm">CREATE ASSESSMENT</a></b></td></tr>
         <tr><td height=10></td></tr>

         <tr>
            <td class="feed_sub_header" width=200><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>RISK</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header">DESCRIPTION</td>

            <cfloop query="area">
             <cfoutput>
              <td class="feed_sub_header" align=center>#ucase(impact_area_name)#</td>
             </cfoutput>
            </cfloop>

            <td class="feed_sub_header" align=center>TOTAL RISK</td>

            <td class="feed_sub_header" align=right><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>DATE</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfloop query="info">

         <cfif counter is 0>
         <tr bgcolor="FFFFFF">
         <cfelse>
         <tr bgcolor="E0E0E0">
         </cfif>

         <cfoutput>

            <td class="data_row"><a class="data_row" href="set.cfm"><b>#info.impact_assessment_name#</b></a></td>
            <td class="data_row">#info.impact_assessment_comments#</td>

         </cfoutput>

	     <cfset total_risk = 0>

         <cfloop query="area">

			<cfquery name="score" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from impact_assessment_score
			  join impact on impact_id = impact_assessment_score_impact
			  join impact_like on impact_like_id = impact_assessment_score_like
			  where impact_assessment_score_risk_id = #session.risk_id# and
			        impact_assessment_score_assessment_id = #info.impact_assessment_id# and
			        impact_assessment_score_area_id = #area.impact_area_id#
			</cfquery>

			<cfoutput query="score">

			  <cfset risk_score = #evaluate(impact_value * impact_like_value)#>
			  <cfset total_risk = total_risk + risk_score>
			  <td class="data_row" align=center>

			  <cfif risk_score GT 0 and risk_score LTE 5>
			   <img src="/images/risk_1.png" height=15>
			  <cfelseif risk_score GT 5 and risk_score LTE 10>
			   <img src="/images/risk_2.png" height=15>
			  <cfelseif risk_score GT 10 and risk_score LTE 15>
			   <img src="/images/risk_3.png" height=15>
			  <cfelseif risk_score GT 15 and risk_score LTE 20>
			   <img src="/images/risk_4.png" height=15>
			  <cfelse>
			   <img src="/images/risk_5.png" height=15>
			  </cfif>

			  </td>
			</cfoutput>

         </cfloop>

         <cfoutput>
         <td class="data_row" align=center>

			  <cfif total_risk GT 0 and total_risk LTE 20>
			   <img src="/images/risk_1.png" height=20>
			  <cfelseif total_risk GT 20 and total_risk LTE 40>
			   <img src="/images/risk_2.png" height=20>
			  <cfelseif total_risk GT 40 and total_risk LTE 60>
			   <img src="/images/risk_3.png" height=20>
			  <cfelseif total_risk GT 60 and total_risk LTE 80>
			   <img src="/images/risk_4.png" height=20>
			  <cfelse>
			   <img src="/images/risk_5.png" height=20>
			  </cfif>


         </td>
         <td class="data_row" align=right>#dateformat(info.impact_assessment_date,'mm/dd/yyyy')#</td>
         </cfoutput>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

         </cfif>

		 </table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>