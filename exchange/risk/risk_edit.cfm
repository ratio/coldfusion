<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Lookups --->

<cfquery name="impact" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_impact
  order by risk_impact_order
</cfquery>

<cfquery name="like" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_like
  order by risk_like_order
</cfquery>

<cfquery name="mag" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_mag
  order by risk_mag_order
</cfquery>

<cfquery name="stakeholders" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from stakeholder
  order by stakeholder_name
</cfquery>

<cfquery name="strategy" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_strategy
  order by risk_strategy_name
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk
  where risk_id = #session.risk_id#
</cfquery>

<cfquery name="status" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_status
  order by risk_status_order
</cfquery>

<cfquery name="type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_type
  order by risk_type_order
</cfquery>

<cfquery name="level" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_level
  order by risk_level_order
</cfquery>

<cfquery name="timing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_timing
  order by risk_timing_order
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

         <cfoutput>
		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=top height=24>Edit Risk</td>
		      <td align=right class="feed_sub_header"><a href="risk_open.cfm">Return</a></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>


         <form action="risk_db.cfm" method="post">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td width=200 class="feed_sub_header" valign=middle>Risk Name</td>
				 <td><input type="text" name="risk_name" class="input_text" style="width: 700px;" value="#info.risk_name#" placeholder="Please provide a high level name for this risk."></td>

			  </tr>

		  <tr>
		     <td class="feed_sub_header" valign=top width=200>Description</td>
		     <td colspan=3><textarea name="risk_desc" class="input_text" style="width: 700px; height: 100px;" placeholder="Please provide a brief description of the risk.">#info.risk_desc#</textarea></td>
		  </tr>

		  <tr>
		     <td class="feed_sub_header" valign=top>Background</td>
		     <td colspan=3><textarea name="risk_background" class="input_text" style="width: 700px; height: 100px;" placeholder="Please provide a brief description of the risk.">#info.risk_background#</textarea></td>
		  </tr>

		  </cfoutput>

		  <tr><td height=10></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr><td height=10></td></tr>

		  </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		      <tr><td height=10></td></tr>

			  <tr>


				 <td class="feed_sub_header" width=200>Risk Timing</td>
                 <td width=300>
				 <select name="risk_timing_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="timing">
				   <option value=#risk_timing_id# <cfif #info.risk_timing_id# is #risk_timing_id#>selected</cfif>>#risk_timing_name#
				  </cfoutput>
				  <select>
				 </td>


				 </td>

				 <td class="feed_sub_header" width=200></td>
                 <td>

				 </td></tr>

			  <tr>

				 <td class="feed_sub_header" width=200>Risk Status</td>
                  <td>
				 <select name="risk_status_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="status">
				   <option value=#risk_status_id# <cfif #info.risk_status_id# is #risk_status_id#>selected</cfif>>#risk_status_name#
				  </cfoutput>
				 <select>

				 </td>
			  </tr>

			  <tr><td class="feed_sub_header" width=200>Risk Strategy</td>
                 <td>
				 <select name="risk_strategy_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="strategy">
				   <option value=#risk_strategy_id# <cfif #info.risk_strategy_id# is #risk_strategy_id#>selected</cfif>>#risk_strategy_name#
				  </cfoutput>
				 <select>
				 </td>

				 <td class="feed_sub_header" width=200>Risk Magnitude</td>
                  <td>

				 <select name="risk_mag_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="mag">
				   <option value=#risk_mag_id# <cfif #info.risk_mag_id# is #risk_mag_id#>selected</cfif>>#risk_mag_name#
				  </cfoutput>
				 <select>

				 </td>
			  </tr>

			  <tr><td class="feed_sub_header" width=200>Current Risk Level</td>
                 <td>
				 <select name="risk_level_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="level">
				   <option value=#risk_level_id# <cfif #info.risk_level_id# is #risk_level_id#>selected</cfif>>#risk_level_name#
				  </cfoutput>
				 <select>
				 </td>

				 <td class="feed_sub_header" width=200>Risk Exist Today?</td>
                  <td>
                 <span class="feed_sub_header">
                 <input type="radio" name="risk_current" style="width: 16px; height: 16px;" value=0 <cfif #info.risk_current# is 0 or #info.risk_current# is "">checked</cfif>>&nbsp;&nbsp;No&nbsp;&nbsp;
                 <input type="radio" name="risk_current" style="width: 16px; height: 16px;" value=1 <cfif #info.risk_current# is 1>checked</cfif>>&nbsp;&nbsp;Yes&nbsp;&nbsp;
                 </span>

				 </td>
			  </tr>

         </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>
		  <tr><td colspan=4><hr></td></tr>
		  <tr><td height=10></td></tr>

			  <tr><td class="feed_sub_header" width=200>Risk Impact</td>
                 <td>
				 <select name="risk_impact_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="impact">
				   <option value=#risk_impact_id# <cfif risk_impact_id is info.risk_impact_id>selected</cfif>>#risk_impact_name#
				  </cfoutput>
				 <select>
				 </td>

				 <td class="feed_sub_header" width=200>Risk Likelihood</td>
                  <td>

				 <select name="risk_like_id" class="input_select" style="width: 200px;">
				  <option value=0>Select
				  <cfoutput query="like">
				   <option value=#risk_like_id# <cfif risk_like_id is info.risk_impact_id>selected</cfif>>#risk_like_name#
				  </cfoutput>
				 <select>

				 </td>
			  </tr>

         <cfoutput>

		  <tr>
		     <td class="feed_sub_header" valign=top width=200>Risk Identified</td>
		     <td width=300><input type="date" class="input_date" name="risk_start_date" value="#info.risk_start_date#"></td>

		     <td class="feed_sub_header" valign=top width=200>Estimated Arrival Date</td>
		     <td><input type="date" class="input_date" name="risk_arrival_date" value="#info.risk_arrival_date#"></td></tr>
		  </tr>

		  <tr><td class="feed_sub_header" valign=top>Planned Closed Date</td>
		     <td><input type="date" class="input_date" name="risk_close_date" value="#info.risk_close_date#"></td>

		     <td class="feed_sub_header" valign=top>Mitigate By</td>
		     <td><input type="date" class="input_date" name="risk_mitigation_date" value="#info.risk_mitigation_date#"></td></tr>

		  <tr><td class="feed_sub_header" valign=top>Completion Status</td>
		     <td><input type="number" class="input_text" name="risk_status_close" step="5" style="width: 75px;" value=#info.risk_status_close#><span class="feed_sub_header">&nbsp;%</span></td></tr>

         </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr><td height=10></td></tr>

          </cfoutput>

          <cfoutput>

		  <tr>
		     <td class="feed_sub_header" valign=top>Risk Impact</td>
		     <td colspan=3><textarea name="risk_impact" class="input_text" style="width: 700px; height: 100px;">#info.risk_impact#</textarea></td>
		  </tr>

		  <tr>		 <td class="feed_sub_header" width=200>Is the Risk<br>Observable?</td>
                  <td>
                 <span class="feed_sub_header">
                 <input type="radio" name="risk_observe" style="width: 16px; height: 16px;" value=0 <cfif #info.risk_observe# is 0 or #info.risk_observe# is "">checked</cfif>>&nbsp;&nbsp;No&nbsp;&nbsp;
                 <input type="radio" name="risk_observe" style="width: 16px; height: 16px;" value=1 <cfif #info.risk_observe# is 1>checked</cfif>>&nbsp;&nbsp;Yes&nbsp;&nbsp;
                 </span>

				 </td></tr>

		  <tr>
		     <td class="feed_sub_header" valign=top>Risk Indicators</td>
		     <td><textarea name="risk_indicators" class="input_text" style="width: 700px; height: 100px;" placeholder="Please describe any indicators, measures or metrics that we should we be aware of to protect against or forecast the arrival of this risk.  Examples include customer satisfaction ratings, speed, help desk tickets, etc.">#info.risk_indicators#</textarea></td>
		  </tr>

		  </cfoutput>

		  <tr><td height=10></td></tr>

		  <tr><td class="feed_sub_header" width=200 valign=top>Stakeholders<br>Effected?</td>
			 <td>
			 <select name="risk_stakeholders" class="input_select" multiple style="width: 250px; height: 120px;">
			  <cfoutput query="stakeholders">
			   <option value=#stakeholder_id# <cfif listfind(info.risk_stakeholders,stakeholder_id)>selected</cfif>>#stakeholder_name#
			  </cfoutput>
			 <select>
			 </td></tr>

          <tr><td height=10></td></tr>

          <cfoutput>

		  <tr><td height=10></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr><td height=10></td></tr>

		  <tr>
		     <td class="feed_sub_header" valign=top>Comments</td>
		     <td colspan=3><textarea name="risk_comments" class="input_text" style="width: 700px; height: 200px;">#info.risk_comments#</textarea></td>
		  </tr>

		  <tr><td height=10></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  <tr><td height=10></td></tr>

		  <tr><td></td><td>

		  <input type="submit" class="button_blue_large" name="button" value="Update Risk">&nbsp;&nbsp;
		  <input type="submit" class="button_blue_large" name="button" value="Delete Risk" onclick="return confirm('Delete Risk?\r\nAre you sure you want to delete this risk?');">

          <cfif isdefined("l")>
           <input type="hidden" name="l" value=#l#>
          </cfif>

		  </td></tr>

		 </table>

		 </form>

		 </cfoutput>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>