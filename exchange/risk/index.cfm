<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td width=185 valign=top>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

      <div class="main_box">

		<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from risk
		  left join risk_status on risk_status.risk_status_id = risk.risk_status_id
		  left join risk_level on risk_level.risk_level_id = risk.risk_level_id
		  left join risk_type on risk_type.risk_type_id = risk.risk_type_id

		  where risk_id > 0

		  <cfif isdefined("session.hub")>
		   and risk_hub_id = #session.hub#
		  <cfelse>
		   and risk_hub_id is null
		  </cfif>

		  <cfif #session.company_id# is 0>
		   and (risk_created_by = #session.usr_id#)
		  <cfelse>
		   and (risk_created_by = #session.usr_id# or risk_company_id = #session.company_id#)
		  </cfif>

          <cfif isdefined("session.risk_keyword")>
            and (risk.risk_name like '%#session.risk_keyword#%' or risk.risk_desc like '%#session.risk_keyword#%')
          </cfif>

        <cfif sv is 1>
         order by risk_name ASC
        <cfelseif sv is 10>
         order by risk_name DESC
        <cfelseif sv is 2>
         order by division_name ASC
        <cfelseif sv is 20>
         order by division_name DESC
        <cfelseif sv is 3>
        <cfelseif sv is 30>
        <cfelseif sv is 4>
         order by risk_mitigation_date ASC
        <cfelseif sv is 40>
         order by risk_mitigation_date DESC
        <cfelseif sv is 5>
         order by resource_name ASC
        <cfelseif sv is 50>
         order by resource_name DESC
        <cfelseif sv is 6>
         order by risk_status_name ASC
        <cfelseif sv is 60>
         order by risk_status_name DESC
        <cfelseif sv is 7>
         order by risk_level_value ASC
        <cfelseif sv is 70>
         order by risk_level_value DESC
        </cfif>

		</cfquery>


		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header">Risks (<cfoutput><cfif #info.recordcount# is 0>No Risks<cfelse>#info.recordcount#</cfif></cfoutput>)</td>
		      <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=13>&nbsp;&nbsp;<a href="risk_add.cfm">Add Risk</a></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" valign=absmiddle>Filter Risks</td>
             <td align=right valign=absmiddle>

             <form action="filter_refresh.cfm" method="post">

                 <input type="text" name="risk_keyword" <cfif isdefined("session.risk_keyword")>value=<cfoutput>#session.risk_keyword#</cfoutput></cfif> class="input_text" size=20 placeholder="keyword">&nbsp;&nbsp;

				  <input type="submit" name="button" class="button_blue" value="Filter">&nbsp;<input type="submit" name="button" class="button_blue" value="Clear">

               </form>

             </td></tr>

         </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif info.recordcount is 0>
             <cfif isdefined("session.risk_keyword")>
	         <tr><td class="feed_sub_header" style="font-weight: normal;">No risks were found matching your filter criteria.</td></tr>
             <cfelse>
	         <tr><td class="feed_sub_header" style="font-weight: normal;">No risks have been created.</td></tr>
	         </cfif>
         <cfelse>

		  <cfif isdefined("u")>
		   <cfif u is "r3">
		    <tr><td class="feed_sub_header" style="color: green;">Risk has been successfully deleted.</td></tr>
		   </cfif>
		  </cfif>


         <tr><td height=10></td></tr>

         <tr>
            <td class="feed_sub_header"><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Risk Name</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header">DESCRIPTION</td>
            <td class="feed_sub_header" align=right><a href="index.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Mitigate By</b></a><cfif isdefined("sv") and sv is 4>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=center><a href="index.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Status</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=right><a href="index.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Level</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfloop query="info">

         <cfif counter is 0>
         <tr bgcolor="FFFFFF">
         <cfelse>
         <tr bgcolor="E0E0E0">
         </cfif>

         <cfoutput>

            <td class="data_row" valign=top width=300><a class="data_row" href="set.cfm?i=#encrypt(info.risk_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>

            <cfif isdefined("session.filter_keyword")>
            #ucase(replaceNoCase(info.risk_name,session.filter_keyword,"<span style='background:yellow'>#ucase(session.filter_keyword)#</span>","all"))#
            <cfelse>
            <cfif #info.risk_name# is "">NOT PROVIDED<cfelse>#ucase(info.risk_name)#</cfif>
            </cfif>

            </b></a></td>
            <td class="data_row" valign=top width=400>

            <cfif isdefined("session.filter_keyword")>
            #replaceNoCase(info.risk_desc,session.filter_keyword,"<span style='background:yellow'>#session.filter_keyword#</span>","all")#
            <cfelse>
            <cfif #info.risk_desc# is "">Not provided<cfelse>#info.risk_desc#</cfif>
            </cfif>

            </td>
            <td class="data_row" valign=top align=center width=120><cfif #info.risk_mitigation_date# is "">TBD<cfelse>#dateformat(info.risk_mitigation_date,'mm/dd/yyyy')#</cfif></td>
            <td class="data_row" valign=top align=center><cfif #info.risk_status_name# is "">TBD<cfelse>#info.risk_status_name#</cfif></td>
            <td class="data_row" valign=top align=right width=80>

               <cfif info.risk_level_value is 0 or info.risk_level_value is "">
                <img src="/images/risk_1.png" height=16 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 1>
                <img src="/images/risk_1.png" height=16 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 2>
                <img src="/images/risk_2.png" height=16 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 3>
                <img src="/images/risk_3.png" height=16 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 4>
                <img src="/images/risk_4.png" height=16 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 5>
                <img src="/images/risk_5.png" height=16 width=40 valign=absmiddle>
               </cfif>

            </td>

         </cfoutput>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfloop>

         </cfif>

       </table>

      </div>

      </td></tr>

   </table>

  <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>