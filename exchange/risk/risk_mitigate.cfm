<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="risk" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk
  left join risk_status on risk_status.risk_status_id = risk.risk_status_id
  left join usr on usr_id = risk_created_by
  left join risk_level on risk_level.risk_level_id = risk.risk_level_id
  left join risk_type on risk_type.risk_type_id = risk.risk_type_id
  left join risk_mitigate on risk_mitigate_risk_id = risk_id
  where risk_id = #session.risk_id#
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_mitigate
  left join risk_level on risk_level_id = risk_mitigate_level
  where risk_mitigate_risk_id = #session.risk_id#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

      <cfoutput>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header">#ucase(risk.risk_name)#</td>
		      <td align=right class="feed_sub_header">
		      	<a href="risk_mitigate_add.cfm">Add Mitigation Strategy</a>&nbsp;|&nbsp;
		      	<a href="risk_open.cfm">Return</a>

		      </td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>

      </cfoutput>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfif info.recordcount is 0>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">You have not created or selected a risk mitigation strategy.</td></tr>
		 <cfelse>

           <tr><td colspan=2 class="feed_sub_header">RISK MITIGATION STRATEGIES</td></tr>
           <tr><td height=10></td></tr>

          <form action="risk_mitigate_select.cfm" method="post">

          <cfoutput query="info">

              <tr>
                 <td width=50><input type="radio" name="risk_mitigate_id" value=#risk_mitigate_id# <cfif risk_mitigate_selected is 1>checked</cfif> style="width: 20px; height: 20px;" required></td>
                 <td class="feed_sub_header"><a href="risk_mitigate_edit.cfm?risk_mitigate_id=#info.risk_mitigate_id#">#ucase(info.risk_mitigate_name)#</a></td>
                 <td align=right>
                  <span class="feed_sub_header">Dates:&nbsp;&nbsp;</span>
                  <span class="feed_sub_header" style="font-weight: normal;">

                  <cfif info.risk_mitigate_start_date is "">
                   Not Provided -
                  <cfelse>
                   #dateformat(info.risk_mitigate_start_date,'mm/dd/yyyy')# -
                  </cfif>

                  <cfif info.risk_mitigate_end_date is "">
                   Not Provided
                  <cfelse>
                   #dateformat(info.risk_mitigate_end_date,'mm/dd/yyyy')#
                  </cfif>

                  </span>

                  </td>

                  <td align=right class="feed_sub_header" width=200>Risk Level&nbsp;&nbsp;

               <cfif info.risk_level_value is 0 or info.risk_level_value is "">
                <img src="/images/risk_1.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 1>
                <img src="/images/risk_1.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 2>
                <img src="/images/risk_2.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 3>
                <img src="/images/risk_3.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 4>
                <img src="/images/risk_4.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 5>
                <img src="/images/risk_5.png" height=20 width=40 valign=absmiddle>
               </cfif>

                 </td></tr>
              <tr>
                 <td></td>
                 <td colspan=3 class="feed_sub_header" style="font-weight: normal;"><cfif #info.risk_mitigate_desc# is "">Not provided<cfelse>#info.risk_mitigate_desc#</cfif></td>
              </tr>

              <tr>
                 <td></td>
                 <td class="feed_sub_header">Assumptions</td>
              </tr>

              <tr>
                 <td></td>
                 <td colspan=3 class="feed_sub_header" style="font-weight: normal;"><cfif #info.risk_mitigate_assumptions# is "">Not provided<cfelse>#info.risk_mitigate_assumptions#</cfif></td>
              </tr>

              <tr><td colspan=4><hr></td></tr>

            </cfoutput>

            <tr><td height=10></td></tr>
            <tr><td colspan=3><input type="submit" name="button" class="button_blue_large" value="Select Strategy"></td></tr>

            </form>




         </cfif>

		 </table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>