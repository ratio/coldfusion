<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_person_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,#session.usr_id#, #session.company_id#,#session.hub#,#now()#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="recent.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
           <td class="feed_header">Profile</td>
           <td class="feed_sub_header" align=right><a href="results.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=20></td></tr>

		   <tr><td valign=top width=20%>

		   <cfinclude template="left.cfm">

           </td><td width=40>&nbsp;</td><td valign=top width=100%>

           <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			    <tr><td class="feed_header" style="font-size: 40px; padding-bottom: 0px;">#profile.person_first_name# #profile.person_last_name#</td></tr>
              </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_sub_header" colspan=2 style="font-size: 24px;">

				<cfif profile.person_source is "Crunchbase">
				<a href="get_comp.cfm?id=#profile.person_cb_company_id#" style="font-size: 24px;" target="_blank">#profile.person_company#</a>
				<cfelseif profile.person_source is "SAMS">
				<a href="/exchange/include/federal_profile.cfm?duns=#profile.person_sams_duns#" style="font-size: 24px;" target="_blank">#profile.person_company#</a>
				</cfif>

				</td></tr>
				<tr><td class="feed_sub_header">#profile.person_title#</td></tr>
				<tr><td height=10></td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><td height=10></td></tr>
				<tr><td class="feed_header">Snapshot</td></tr>
				<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#profile.person_desc#</td></tr>
			  </table>

		   </cfoutput>

			  <cfquery name="jobs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select * from cb_jobs
			   left join cb_organizations on cb_organizations.uuid = org_uuid
			   where person_uuid = '#profile.person_cb_id#'
			   order by started_on DESC
			  </cfquery>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td height=10></td></tr>
				<tr><td class="feed_header">History</td></tr>
				<tr><td height=10></td></tr>

				<cfif jobs.recordcount is 0>
					<tr><td class="feed_sub_header" style="font-weight: normal;">No history found.</td></tr>
			    <cfelse>

			      <cfset count = 1>

                  <cfoutput query="jobs">
                   <tr>
                   <td width=70 valign=top>

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_sub_header"><a href="get_comp.cfm?id=#jobs.org_uuid#"><img src="#logo_url#" width=50 onerror="this.onerror=null; this.src='/images/stock_company.png'"></a></td></tr>
				      </table>

                   </td><td valign=top>

                   <!---

                   <cfif jobs.ended_on is "">
                   <cfset diff = #datediff('m',dateformat(jobs.started_on,'mm/dd/yyyy'),dateformat(now(),'mm/dd/yyyy'))#>
                   <cfelse>
                   <cfset diff = #datediff('m',dateformat(jobs.started_on,'mm/dd/yyyy'),dateformat(jobs.ended_on,'mm/dd/yyyy'))#>
                   </cfif> --->

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_sub_header" style="padding-bottom: 5px;"><a href="get_comp.cfm?id=#jobs.org_uuid#" style="font-size: 22px;" target="_blank">#jobs.org_name#</a> <cfif jobs.is_current is 'true'>( Current )</cfif></td>
                            <td class="feed_sub_header" style="padding-bottom: 5px;" align=right>#dateformat(jobs.started_on,'mmm dd, yyyy')# - <cfif jobs.ended_on is "">Current<cfelse>#dateformat(jobs.ended_on,'mmm dd, yyyy')#</cfif></td></tr>
                        <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#jobs.title#</td>
                            <td class="link_small_gray" style="font-weight: normal; padding-top: 0px;" align=right>

  <!---                          <cfif diff is 12>
                            1 Year
                            <cfelseif diff LT 12>
                             #diff# Months
                            <cfelse>
                             #numberformat(evaluate(diff/12),'99.9')# Years
                            </cfif> --->

                            </td></tr>

                        <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;">#jobs.short_description#</td></tr>
                        <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;">#jobs.category_groups_list#</td></tr>
                        <tr><td height=10></td></tr>


                        </tr>
                      </table>

                      </td></tr>

                  <cfif count LT jobs.recordcount>
                  <tr><td colspan=2><hr></td></tr>
                  </cfif>

                  <cfset count = count + 1>

                  </cfoutput>

			    </cfif>
			  </table>

            </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

