<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="status" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from person_status
 where person_status_person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_person_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,#session.usr_id#, #session.company_id#,#session.hub#,#now()#)
</cfquery>

<cfquery name="rtypes" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from person_type
 order by person_type_name
</cfquery>

<cfquery name="rel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from seedspot.dbo.person_rel
 join exchange.dbo.person on exchange.dbo.person.person_id = seedspot.dbo.person_rel.person_rel_person_id
 join seedspot.dbo.person_type on seedspot.dbo.person_type.person_type_id = seedspot.dbo.person_rel.person_rel_type_id
 join seedspot.dbo.usr on seedspot.dbo.usr.usr_id = seedspot.dbo.person_rel.person_rel_usr_id
 where seedspot.dbo.person_rel.person_rel_person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#


</cfquery>

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -6px;
    margin-top: 20px;
    margin-left: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    padding-right: 20px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -4px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}
.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/pportfolio/recent.cfm">

      </td><td valign=top>


          <cfoutput>


          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header" valign=absmiddle><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<a href="profile.cfm?i=#i#">Profile</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<a href="relationships.cfm?i=#i#">Relationships</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><i class="fa fa-commenting" aria-hidden="true"></i></i>&nbsp;&nbsp;&nbsp;<a href="comments.cfm?i=#i#">Intel</a></span>
          </div>

          </cfoutput>

	  <div class="main_box_2">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=20></td></tr>

		   <tr><td valign=top width=20%>

		   <cfinclude template="left.cfm">

           </td><td width=40>&nbsp;</td><td valign=top width=100%>

           <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td class="feed_header" style="font-size: 40px; padding-bottom: 0px;">#profile.person_first_name# #profile.person_last_name#</td>
			      <td class="feed_header" align=right>

			<td width=50 align=right>
			<div class="dropdown">
			  <img src="/images/3dots2.png" style="cursor: pointer;" height=8>
			  <div class="dropdown-content" style="width: 250px; text-align: left;">
				<a href="/exchange/include/save_person.cfm" onclick="window.open('/exchange/include/save_person.cfm?i=#encrypt(profile.person_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=200, scrollbars=yes,resizable=yes,width=900,height=375'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
			  </div>
			</div>
			</td>
			      </td></tr>

           </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_sub_header" colspan=2 style="font-size: 24px;"><a href="get_comp.cfm?id=#profile.person_cb_company_id#" style="font-size: 24px;" target="_blank">#profile.person_company#</a></td></tr>
				<tr><td class="feed_sub_header">#profile.person_title#</td>
			    <td align=right>

              </cfoutput>

					<div class="dropdown">
					<img src="/images/plus3.png" width=15 hspace=10 style="cursor: pointer;"><span class="feed_sub_header">Add Relationship</span>
					  <div class="dropdown-content" style="height: auto; width: 550px; top: 14; text-align: left; right: 10;">
						  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                          <form action="add_rel.cfm" method="post">

                           <tr><td class="feed_sub_header" colspan=2>Relationship Details</td></tr>
                           <tr><td colspan=2><hr></td></tr>

								   <tr>
								      <td class="feed_sub_header">Select the type of your relationship</td>
								      <td>

								      <select name="person_rel_type_id" class="input_select" style="width: 225px;">
								      <cfoutput query="rtypes">
								       <option value=#person_type_id#>#person_type_name#
								      </cfoutput>
								      </select>

								      </td>
								   </tr>

								   <tr>
								      <td colspan=2><input type="text" class="input_text" name="person_rel_org" style="width: 500px;" placeholder="At what company / organization?"></td>
								   </tr>

								   <tr>
								      <td colspan=2><textarea name="person_rel_comments" class="input_textarea" style="width: 500px; height: 75px;" placeholder="What is the context of this relationship?"></textarea></td>
								   </tr>

								   <tr>
								      <td colspan=2 class="feed_sub_header" style="font-weight: normal;"><b>From</b>&nbsp;

								      <input type="date" class="input_text" name="person_rel_started">
								      <b>To</b>&nbsp;
								      <input type="date" class="input_text" name="person_rel_ended">

								      </td>
								   </tr>

								   <tr><td colspan=2 class="link_small_gray">Leave the end date blank if you still have the relationship.</td></tr>
                                   <tr><td height=10></td></tr>

								   <tr>
								      <td colspan=2 class="feed_sub_header" style="font-weight: normal;"><b>Relationship Strength</b>&nbsp;

								      <select name="person_rel_strength" class="input_select">
								       <option value=1>Weak
								       <option value=1>Moderate
								       <option value=1>Good
								       <option value=1>Excellent
								       <option value=1>Very Strong
                                      </select>

								      </td>
								   </tr>

								   <cfoutput>
								   <input type="hidden" name="i" value=#i#>
								   </cfoutput>

                           <tr><td colspan=2><hr></td></tr>
                           <tr><td height=10></td></tr>
                           <tr><td colspan=2><input type="submit" name="button" class="button_blue" value="Add Relationship"></td></tr>
                           <tr><td height=10></td></tr>
						   </form>
						  </table>

					  </div>
					</div>

				    </td>


				</tr>
				<tr><td height=10></td></tr>
				<tr><td colspan=2><hr></td></tr>
			  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td height=10></td></tr>

				<cfif isdefined("u")>
				 <cfif u is 1>
				  <tr><td class="feed_sub_header" style="color: green;">Relationship has been successfully added.</td></tr>
				  <tr><td height=10></td></tr>
				 </cfif>
				</cfif>

				<tr><td class="feed_header">Relationships</td>
				    <td class="feed_sub_header" align=right>


				    </td></tr>
              </table>


			        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			         <cfif rel.recordcount is 0>
			          <tr><td class="feed_sub_header" style="font-weight: normal;">No relationships were found for this person.</td></tr>
			         <cfelse>

			         <tr><td height=10></td></tr>

			          <tr>
			             <td></td>
			             <td class="feed_sub_header">Name</td>
			             <td class="feed_sub_header">Relationship Context</td>
			             <td class="feed_sub_header" align=right>Type</td>
			             <td class="feed_sub_header" align=right>Strength</td>
			             <td></td>
                      </tr>
                      <cfset count = 1>
                      <cfoutput query="rel">

			          <tr>


						 <cfif #usr_photo# is "">
						  <td width=65 valign=top><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="margin-top: 10px;" src="/images/headshot.png" height=40 width=40 border=0></td>
						 <cfelse>
						  <td width=65 valign=top><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px; margin-top: 10px;" src="#media_virtual#/#usr_photo#" height=40 width=40 border=0></td>
						 </cfif>

			             <td valign=top width=225 class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#usr_full_name#</b></a><br><span class="link_small_gray">#usr_title#</span></td>
			             <td valign=top class="feed_sub_header" style="font-weight: normal;"><b>#person_rel_org#</b><br><span class="link_small_gray">#person_rel_comments#</span></td>
			             <td valign=top width=200 align=right class="feed_sub_header" style="font-weight: normal;"><b>#person_type_name#</b><br>
			             <span class="link_small_gray">

			             <cfif person_rel_started is "" and person_rel_ended is "">
			             <cfelse>
			             #dateformat(person_rel_started,'mmm dd, yyyy')# - <cfif person_rel_ended is "">Current<cfelse>#dateformat(person_rel_ended,'mmm dd, yyyy')#</cfif>
			             </cfif>

			             </span></td>

                         <td width=100 align=right><progress id="file" value="#person_rel_strength#" max="5" style="color: green; width: 50px; height: 40px;"></progress></td>

                         <td valign=top width=50 align=right>

                          <img src="/images/icon_edit.png" style="margin-top: 20px;" width=20 alt="Update Relationship" title="Update Relationship">

                         </td>


                      </tr>

                      <cfif count LT rel.recordcount>
                       <tr><td colspan=6><hr></td></tr>
                      </cfif>

                      <cfset count = count + 1>


                      </cfoutput>


			         </cfif>

			        </table>







            </td></tr>

 		  </table>



	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

