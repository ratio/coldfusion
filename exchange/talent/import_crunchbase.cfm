<cfsetting RequestTimeout = "9000000">

<cfquery name="cb" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select
 featured_job_organization_name,
 featured_job_organization_uuid,
 first_name,
 last_name,
 cb_people.name,
 featured_job_title,
 cb_people.twitter_url,
 cb_people.facebook_url,
 cb_people.linkedin_url,
 cb_people.logo_url,
 description,
 gender,
 cb_people.country_code,
 cb_people.state_code,
 cb_people.city,
 cb_people.uuid,
 category_groups_list,
 category_list
 from cb_people

 left join cb_people_descriptions on cb_people_descriptions.uuid = cb_people.uuid
 left join cb_organizations on cb_organizations.uuid = cb_people.featured_job_organization_uuid
 where cb_people.featured_job_organization_uuid is not null

</cfquery>

<cfset count = 0>

<cfloop query="cb">

    <cfset #company_keywords# = #cb.category_groups_list# & " " & #cb.category_list#>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into person
	 (
	 person_company,
	 person_company_keywords,
	 person_cb_company_id,
	 person_first_name,
	 person_last_name,
	 person_full_name,
	 person_title,
	 person_email,
	 person_phone,
	 person_cell,
	 person_twitter,
	 person_facebook,
	 person_linkedin,
	 person_photo,
	 person_desc,
	 person_gender,
	 person_country,
	 person_state,
	 person_city,
	 person_cb_id,
	 person_sams_duns,
	 person_source,
	 person_created,
	 person_updated,
	 person_hub_id
	 )
	 values
	 (
     '#featured_job_organization_name#',
     '#company_keywords#',
     '#featured_job_organization_uuid#',
     '#first_name#',
     '#last_name#',
     '#name#',
     '#featured_job_title#',
     null,
     null,
     null,
     '#twitter_url#',
     '#facebook_url#',
     '#linkedin_url#',
     '#logo_url#',
     '#description#',
     '#gender#',
     '#country_code#',
     '#state_code#',
     '#city#',
     '#uuid#',
     null,
     'Crunchbase',
     #now()#,
     #now()#,
     1
     )
	</cfquery>

	<cfset count = count + 1>

</cfloop>

<cfoutput>
	users = #count#
</cfoutput>