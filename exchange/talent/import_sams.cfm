<cfsetting RequestTimeout = "9000000">

<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns like '9%'
</cfquery>

<cfset count = 0>

<cfloop query="sams">

    <cfif #sams.naisc_code_string# is not "">

    	<cfset naics_list = #replace(sams.naisc_code_string,'~',',','all')#>

     	<cfset new_list = "">

     	<cfloop index="i" list="#naics_list#">

			<cfquery name="desc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select naics_code_description from naics
			 where naics_code = '#left(i,6)#'
			</cfquery>

			<cfset new_list = listappend(new_list,'#trim(desc.naics_code_description)#')>

  	    </cfloop>

    </cfif>

    <cfset full_name = '#sams.poc_fnme#' & " " & '#sams.poc_lname#'>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     set ansi_defaults off
	 insert into person
	 (
	 person_first_name,
	 person_company_keywords,
	 person_company,
	 person_last_name,
	 person_full_name,
	 person_title,
	 person_email,
	 person_phone,
	 person_cell,
	 person_twitter,
	 person_facebook,
	 person_linkedin,
	 person_photo,
	 person_desc,
	 person_gender,
	 person_country,
	 person_state,
	 person_city,
	 person_cb_id,
	 person_sams_duns,
	 person_source,
	 person_created,
	 person_updated,
	 person_hub_id
	 )
	 values
	 (
     '#sams.poc_fnme#',
     <cfif isdefined("new_list")>'#new_list#'<cfelse>null</cfif>,
     '#sams.legal_business_name#',
     '#sams.poc_lname#',
     '#full_name#',
     '#sams.poc_title#',
     '#sams.poc_email#',
     '#sams.poc_us_phone#',
     null,
     null,
     null,
     null,
     null,
     null,
     null,
     '#sams.country_2#',
     '#sams.state_2#',
     '#sams.city_2#',
     null,
     '#sams.duns#',
     'SAMS',
     #now()#,
     #now()#,
     1
     )
	</cfquery>

	<cfset count = count + 1>

</cfloop>

<cfoutput>
	users = #count#
</cfoutput>