<cfif not isdefined("session.people_search_type")>
 <cfset session.people_search_type = 1>
</cfif>

<cfif not isdefined("session.people_type")>
 <cfset session.people_type = 0>
</cfif>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <form action="people_set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

<tr>
	<td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-bottom: 0px;">

	   <b>Find People&nbsp;&nbsp;</b>

	   <select name="people_search_type" class="input_select" style="width: 250px;">
		 <option value=1 <cfif session.people_search_type is 1>selected</cfif>>By Name
		 <option value=2 <cfif session.people_search_type is 2>selected</cfif>>By Title
		 <option value=3 <cfif session.people_search_type is 3>selected</cfif>>By Market / Capability
		 <option value=4 <cfif session.people_search_type is 4>selected</cfif>>By Company
	   </select>
		&nbsp;

		<input class="input_text" type="text" style="width: 275px;" placeholder = "keyword" name="people_search_keyword" <cfif isdefined("session.people_search_keyword")>value="<cfoutput>#replace(session.people_search_keyword,'"','','all')#</cfoutput>"</cfif> required onfocus="clearThis(this)">
		&nbsp;
		<input class="button_blue" type="submit" name="button" value="Search">

	</td>
</tr>
</form>

</table>

