<div class="left_box">

  <cfquery name="rtypes" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from person_type
   order by person_type_name
  </cfquery>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header" valign=bottom><b>Filter Results</b></td>
	   <td align=right valign=top></td></tr>
   <tr><td><hr></td></tr>
  </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <form action="filter_set.cfm" method="post">

      <tr>
	     <td><input type="checkbox" name="alumni" <cfif isdefined("session.alumni")>checked</cfif> style="width: 22px; height: 22px;" onchange="form.submit();"></td>
	     <td class="feed_sub_header" style="padding-top: 5px;">Current Alumni</td>
      </tr>

      <tr><td colspan=2><hr></td></tr>

	  <cfoutput query="rtypes">

	  <tr>
	     <td><input type="checkbox" name="rtype_id"

         <cfif isdefined("session.filter_list") and listfind(session.filter_list,#person_type_id#)>checked</cfif>

	     value=#person_type_id# style="width: 22px; height: 22px;" onchange="form.submit();"></td>
	     <td class="feed_sub_header" style="padding-top: 5px;">#person_type_name#</td>
	  </tr>

     </cfoutput>

     <tr><td colspan=2><hr></td></tr>
     <tr><td height=10></td></tr>
     <tr><td colspan=2><input type="submit" name="button" value="Clear Filters" class="button_blue"></td></tr>


   </form>
  </table>


</div>