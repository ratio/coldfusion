<cfinclude template="/exchange/security/check.cfm">

<cfset location = 1>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="10">
 select distinct(recent_person_id) from recent
 where (recent_usr_id = #session.usr_id# and
	    recent_hub_id = #session.hub#) and
	    recent_person_id is not null
</cfquery>

<cfif recent.recordcount is 0>
 <cfset recent_list = 0>
<cfelse>
 <cfset recent_list = valuelist(recent.recent_person_id)>
</cfif>

<cfquery name="list" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="5">
 select * from person
 where person_id in (#recent_list#)
</cfquery>

<style>
.people_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 225px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/company_profile/index.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Talent Finder</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right>
	              </td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <cfinclude template="people_search_start.cfm">

          </div>

          <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Recently Viewed People</td></tr>
            <tr><td><hr></td></tr>
            <tr><td height=20></td></tr>

            <tr><td>

          <cfoutput query="list">

			<div class="people_badge">
			 <cfinclude template="people_badge.cfm">
			</div>

	   	   </cfoutput>

           </td></tr>
           </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>