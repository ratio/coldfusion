<cfinclude template="/exchange/security/check.cfm">

<cfif trim(people_search_name) is "" and trim(people_search_title) is "" and trim(people_search_company) is "" and trim(people_search_capability) is "">
 <cfif session.people_location is 1>
  <cflocation URL="index.cfm?e=1" addtoken="no">
 <cfelse>
  <cflocation URL="results.cfm?e=1" addtoken="no">
 </cfif>
</cfif>

<cfset StructDelete(Session,"people_search_name")>
<cfset StructDelete(Session,"people_search_title")>
<cfset StructDelete(Session,"people_search_company")>
<cfset StructDelete(Session,"people_search_capability")>

<cfif trim(people_search_name) is not "">
	<cfset search_string = #replace(people_search_name,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
	<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	<cfset search_string = #replace(search_string,'"(','("',"all")#>
	<cfset search_string = #replace(search_string,')"','")',"all")#>
	<cfset session.people_search_name = #search_string#>
</cfif>

<cfif trim(people_search_title) is not "">
	<cfset search_string = #replace(people_search_title,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
	<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	<cfset search_string = #replace(search_string,'"(','("',"all")#>
	<cfset search_string = #replace(search_string,')"','")',"all")#>
	<cfset session.people_search_title = #search_string#>
</cfif>

<cfif trim(people_search_company) is not "">
	<cfset search_string = #replace(people_search_company,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
	<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	<cfset search_string = #replace(search_string,'"(','("',"all")#>
	<cfset search_string = #replace(search_string,')"','")',"all")#>
	<cfset session.people_search_company = #search_string#>
</cfif>

<cfif trim(people_search_capability) is not "">
	<cfset search_string = #replace(people_search_capability,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
	<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	<cfset search_string = #replace(search_string,'"(','("',"all")#>
	<cfset search_string = #replace(search_string,')"','")',"all")#>
	<cfset session.people_search_capability = #search_string#>
</cfif>

<cflocation URL="results.cfm" addtoken="no">


