<cfset StructDelete(Session,"news_time")>
<cfset StructDelete(Session,"news_keyword")>
<cfset StructDelete(Session,"news_source")>

<cfif button is "Filter">

	<cfif isdefined("news_time")>
	 <cfset session.news_time = #news_time#>
	</cfif>

	<cfset session.news_source = #news_source#>

	<cfif isdefined("news_keyword")>

	<cfif news_keyword is not "">

		<cfset search_string = #replace(news_keyword,chr(34),'',"all")#>
		<cfset search_string = #replace(search_string,'''','',"all")#>
		<cfset search_string = #replace(search_string,')','',"all")#>
		<cfset search_string = #replace(search_string,'(','',"all")#>
		<cfset search_string = #replace(search_string,',','',"all")#>
		<cfset search_string = #replace(search_string,':','',"all")#>
		<cfset search_string = '"' & #search_string#>
		<cfset search_string = #search_string# & '"'>
		<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
		<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
		<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
	    <cfset session.news_keyword = #search_string#>

    </cfif>

	</cfif>

</cfif>

<cflocation URL="/exchange/news.cfm" addtoken="no">