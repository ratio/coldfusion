<cfif isdefined("session.hub")>

  <cfquery name="hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from hub
   where hub_id = #session.hub#
  </cfquery>

  <cfif hub.hub_logout_page is "">
   <cfset logout_page = "/">
  <cfelse>
   <cfset logout_page = "#hub.hub_logout_page#">
  </cfif>

<cfelse>
  <cfset logout_page = "/">
</cfif>

<cfset StructDelete(Session,"usr_id")>
<cfset StructDelete(Session,"company_id")>
<cfset StructDelete(Session,"hub")>
<cfset StructDelete(Session,"key")>
<cfset StructDelete(Session,"add_filter")>
<cfset StructDelete(Session,"keyword")>
<cfset StructDelete(Session,"portfolio_id")>
<cfset StructClear(Session)>

<cflocation URL="#logout_page#" addtoken="no">