<cfinclude template="/exchange/security/check.cfm">

<cfquery name="hub_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

 <cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(hub_xref_id) as total from hub_xref
  where hub_xref_hub_id = #session.hub# and
		hub_xref_active = 1 and
		hub_xref_usr_id <> #session.usr_id#
 </cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_header"><a href="/exchange/hubs/members/">MY NETWORK</a></td></tr>
            <tr><td colspan=3><hr></td></tr>
            <cfoutput>
            <tr><td class="feed_option" colspan=3><b>

				 <cfif #members.total# is 0>
				  	No Connections
				 <cfelseif #members.total# is 1>
				  	1 Connection
				 <cfelseif #members.total# GT 1>
				    <cfoutput>
				 	#members.total# Connections
				 	</cfoutput>
				 </cfif>

				 </b></td></tr>

			</cfoutput>

			<tr><td height=10></td></tr>

			 <cfquery name="pics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select top(9) * from hub_xref
			  left join usr on usr_id = hub_xref_usr_id
			  where hub_xref_hub_id = #session.hub# and
			        hub_xref_active = 1 and
			        hub_xref_usr_id <> #session.usr_id#
			  order by hub_xref_joined DESC
			 </cfquery>

			 <tr><td colspan=3>

			 <cfoutput query="pics">

			     <cfif usr_profile_display is 2>
					  <img style="border-radius: 150px;" src="/images/headshot.png" width=37 height=37 border=0 alt="Private Profile" vspace=5  title="Private Profile">
			     <cfelse>
					 <cfif #usr_photo# is "">
					  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(hub_xref_usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 150px;" vspace=5 src="/images/headshot.png" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 <cfelse>
					  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(hub_xref_usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 150px;" vspace=5  src="#media_virtual#/#usr_photo#" width=37 height=37 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
					 </cfif>
				 </cfif>
			 </cfoutput>
                  <cfif members.total GT 1>
	                  <a href="/exchange/hubs/members/"><img style="border-radius: 150px;" src="/images/all.png" width=35 height=35 vspace=5  border=0 alt="All Members" title="All Members"></a>
                  </cfif>
             </td></tr>

		</table>

    </td></tr>

</table>