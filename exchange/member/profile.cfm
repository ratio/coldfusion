<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("vi")>

	<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from cview
	   left join usr on usr_id = cview_by_usr_id
	   left join company on company_id = usr_company_id
	   left join size on size_id = company_size_id
	   left join entity on entity_id = company_entity_id
	  where cview_id = #vi#
</cfquery>

</cfif>

<cfif isdefined("h")>

	<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from usr
	   left join hub on hub_owner_id = usr_id
	   left join company on company_id = usr_company_id
	   left join size on size_id = company_size_id
	   left join entity on entity_id = company_entity_id
	  where hub_id = #h#
	</cfquery>

</cfif>

<cfif isdefined("i")>

	<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from follow
	   left join usr on usr_id = follow_by_usr_id
	   left join company on company_id = usr_company_id
	   left join size on size_id = company_size_id
	   left join entity on entity_id = company_entity_id
	  where follow_id = #i#
	</cfquery>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top>

 	       <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td valign=top>

           <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">#profile.usr_first_name# #profile.usr_last_name#</td>
				       <td align=right><a href="##" onclick="windowClose();"><img src="/images/delete.png" width=20 border=0></a></td></tr>
				   <tr><td colspan=2><hr></td></tr>
				   <tr><td height=20></td></tr>
			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

		       <tr><td valign=top width=150>

					<cfif #profile.usr_photo# is "">
					 <img src="/images/headshot.png" height=160 border=0>
					<cfelse>
					 <img style="border-radius: 0px;" src="#media_virtual#/#profile.usr_photo#" height=160 border=0>
					</cfif>

					<br><br>
					<center>
					 <a href="#profile.usr_linkedin#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/linkedin.jpg" width=75 border=0></a>
					</center>

					</td>

					<td width=20>&nbsp;</td><td valign=top>

		            <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td class="feed_title"><cfif #profile.company_name# is not "">#profile.company_name#, </cfif><cfif #profile.usr_title# is "">Not provided<cfelse>#profile.usr_title#</cfif></td></tr>
				   <tr><td class="feed_option">#profile.usr_email#</td></tr>
				   <cfif #profile.usr_phone# is not "">
				   	<tr><td class="feed_option">#profile.usr_phone#</td></tr>
				   </cfif>
				   <tr><td class="feed_option"><b>Address</b></td></tr>
				   <tr><td class="feed_option">
				   <cfif #profile.usr_address_line_1# is "" and #profile.usr_address_line_2# is "">
				   <cfelse>
				   	#profile.usr_address_line_1#<br><cfif #profile.usr_address_line_2# is not "">#profile.usr_address_line_2#<br>
				   </cfif>
				   </cfif>#profile.usr_city# #profile.usr_state#.  #profile.usr_zip#</td></tr>
				   </table>

				   </td></tr>

                   <cfif profile.usr_about is not "">
					   <tr><td height=20></td></tr>
					   <tr><td class="feed_sub_header" colspan=3><b>About Me</b></td></tr>
					   <tr><td class="feed_option" colspan=3>#replace(profile.usr_about,"#chr(10)#","<br>","all")#</td></tr>
				   </cfif>

                   <cfif profile.usr_experience is not "">
					   <tr><td height=20></td></tr>
					   <tr><td class="feed_sub_header" colspan=3><b>Experience</b></td></tr>
					   <tr><td class="feed_option" colspan=3>#replace(profile.usr_experience,"#chr(10)#","<br>","all")#</td></tr>
				   </cfif>

                   <cfif profile.usr_education is not "">
					   <tr><td height=20></td></tr>
					   <tr><td class="feed_sub_header" colspan=3><b>Education</b></td></tr>
					   <tr><td class="feed_option" colspan=3>#replace(profile.usr_education,"#chr(10)#","<br>","all")#</td></tr>
				   </cfif>

			  </table>

              <cfif profile.company_name is not "">

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>

				   <tr><td valign=top width=150>

					  <cfif #profile.company_logo# is "">
					   <a href="/exchange/include/company_profile.cfm?id=#profile.company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/company_logo.png" width=100 border=0></a>
					  <cfelse>
					   <a href="/exchange/include/company_profile.cfm?id=#profile.company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#profile.company_logo#" width=100 border=0></a>
					  </cfif>

					  </td><td width=20>&nbsp;</td><td valign=top>

			          <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td class="feed_title"><a href="/exchange/include/company_profile.cfm?id=#profile.company_id#" target="_blank" rel="noopener" rel="noreferrer">#profile.company_name#</a></td></tr>

				   <tr><td class="feed_option">#profile.company_address_l1#</td></tr>
				   <tr><td class="feed_option">#profile.company_city#, #profile.company_state#  #profile.company_zip#</td></tr>
				   <cfif #profile.company_website# is not "">
				   <tr><td class="feed_option"><a href="#profile.company_website#" target="_blank" rel="noopener" rel="noreferrer">#profile.company_website#</a></td></tr>
				   </cfif>

				   <tr><td class="feed_option"><b>Founded: </b>#profile.company_founded#</td></tr>
				   <tr><td class="feed_option"><b>Size: </b><cfif #profile.size_name# is "">Unknown<cfelse>#profile.size_name#</cfif><br>
				   <tr><td class="feed_option"><b>Type: </b><cfif #profile.entity_name# is "">Unknown<cfelse>#profile.entity_name#</cfif><br>

               </table>

               </td></tr>

                   <cfif #profile.company_about# is not "">
                   <tr><td height=20></td></tr>
				   <tr><td class="feed_sub_header" colspan=3><b>Company Information</b></td></tr>
				    <tr><td class="feed_option" colspan=3>#profile.company_about#</td></tr>
				    <tr><td height=5></td></tr>
                   </cfif>

               </table>

             </cfif>

           </td></tr>

 		  </table>

		   </cfoutput>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

