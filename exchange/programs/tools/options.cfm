<cfquery name="program_cat" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from program_cat
  where program_cat_hub_id = #session.hub#
  order by program_cat_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header">Events Configuration</td>
      <td class="feed_sub_header" align=right><a href="/exchange/admin/apps/myapps.cfm">My Apps</a></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td height=10></td></tr>
</table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

   <tr><td class="feed_sub_header"><a href="cat_add.cfm">Add Event Category</a>&nbsp;|&nbsp;<a href="add.cfm">Add Event</a></td></tr>
   <tr><td>&nbsp;</td></tr>

	<tr><td>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfif program_cat.recordcount is 0>

	 <tr><td class="feed_sub_header" style="font-weight: normal;">No Program Categories exist.  Please add a Program Category before adding a Program.</td></tr>

	<cfelse>

		<tr>
			<td class="feed_sub_header" width=200>Date</td>
			<td class="feed_sub_header">Program</td>
		</tr>

		<tr><td height=10></td></tr>

	<cfloop query="program_cat">

	 <cfoutput>
	 <tr><td colspan=2 class="feed_sub_header" bgcolor="e0e0e0"><a href="cat_edit.cfm?program_cat_id=#program_cat_id#"><b>#program_cat_name#</b></a></td></tr>
	 </cfoutput>

	 <cfquery name="program" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from program
	   where program_cat_id = #program_cat.program_cat_id#
	   order by program_date ASC
	 </cfquery>

	 <cfset counter = 0>

	 <cfif program.recordcount is 0>
	  <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;">No Programs have been added to this Category.</td></tr>
	 <cfelse>

	 <cfoutput query="program">

	  <cfif #counter# is 0>
	   <tr bgcolor="ffffff">
	  <cfelse>
	   <tr bgcolor="f0f0f0">
	  </cfif>

		  <td class="feed_sub_header" width=10% valign=top><a href="edit.cfm?program_id=#program_id#">#dateformat(program_date,'mm/dd/yyyy')#</a></td>
		  <td class="feed_sub_header" valign=top width=300><a href="edit.cfm?program_id=#program_id#">#program_name#</a></td>

	  </tr>

	  <cfif counter is 0>
	   <cfset counter = 1>
	  <cfelse>
	   <cfset counter = 0>
	  </cfif>

	  </cfoutput>

	  </cfif>

	 <tr><td>&nbsp;</td></tr>

	</cfloop>

	</cfif>

	</table>

</td></tr>

</table>




