<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from program_cat
 where program_cat_hub_id = #session.hub#
 order by program_cat_order
</cfquery>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from program
 where program_id = #program_id# and
       program_hub_id = #session.hub#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

      <form action="db.cfm" method="post" enctype="multipart/form-data" >

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Event</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfoutput>

		   <tr><td class="feed_sub_header">Event Name</td>
		       <td><input class="input_text" style="width: 600px;" type="text" value="#edit.program_name#" required name="program_name"></td></tr>
		   <tr><td valign=top class="feed_sub_header">Description</td>
		       <td><textarea class="input_textarea" style="width: 800px; height: 100px;" name="program_desc">#edit.program_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header">Link / URL</td>
		       <td><input class="input_text" style="width: 800px;" type="url" value="#edit.program_url#" name="program_url"></td></tr>

		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" style="width: 800px;" type="text" value="#edit.program_keywords#" name="program_keywords"></td></tr>

		   <tr><td class="feed_sub_header">Location</td>
		       <td><input class="input_text" style="width: 300px;" type="text" value="#edit.program_location#" name="program_location"></td></tr>

		   <tr><td class="feed_sub_header">Date</td>
		       <td><input class="input_date" style="width: 175px;" type="date" value="#edit.program_date#" name="program_date"></td></tr>

		   <tr><td class="feed_sub_header">Time</td>
		       <td><input class="input_text" style="width: 175px;" type="text" value="#edit.program_time#" name="program_time"></td></tr>

		   </cfoutput>

		   <tr><td class="feed_sub_header">Category</td>
		   <td>
		   <select name="program_cat_id" class="input_select" style="width: 175px;">
		   <cfoutput query="cats">
		    <option value=#program_cat_id# <cfif #edit.program_cat_id# is #program_cat_id#>selected</cfif>>#program_cat_name#
		   </cfoutput>
		   </select></td></tr>

           <cfoutput>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td></td><td>
		   <input class="button_blue_large" type="submit" name="button" value="Update">&nbsp;&nbsp;
		   <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>
		     <input type="hidden" name="program_id" value=#program_id#>
		  </table>

		  </cfoutput>

      </td></tr>
     </table>

	  </div>

      </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>


