<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfif #program_cat_image# is not "">
		<cffile action = "upload"
		 fileField = "program_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into program_cat (program_cat_image, program_cat_hub_id, program_cat_name, program_cat_desc,program_cat_order,program_cat_updated)
	 values (<cfif #program_cat_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,#session.hub#,'#program_cat_name#','#program_cat_desc#',<cfif #program_cat_order# is "">null<cfelse>#program_cat_order#</cfif>,#now()#)
	</cfquery>

  <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete program_cat
	 where program_cat_id = #program_cat_id#
	</cfquery>

	<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete program
	 where program_cat_id = #program_cat_id#
	</cfquery>

</cftransaction>

<cflocation URL="index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select program_cat_image from program_cat
		  where program_cat_id = #program_cat_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.program_cat_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.program_cat_image#">
		</cfif>

	</cfif>

	<cfif #program_cat_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select program_cat_image from program_cat
		  where program_cat_id = #program_cat_id#
		</cfquery>

		<cfif #getfile.program_cat_image# is not "">
         <cfif fileexists("#media_path#\#getfile.program_cat_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.program_cat_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "program_cat_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update program_cat
	 set program_cat_name = '#program_cat_name#',
	     program_cat_desc = '#program_cat_desc#',

		  <cfif #program_cat_image# is not "">
		   program_cat_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   program_cat_image = null,
		  </cfif>

	     program_cat_updated = #now()#,
	     program_cat_order = <cfif #program_cat_order# is "">null<cfelse>#program_cat_order#</cfif>
	 where program_cat_id = #program_cat_id# and
	       program_cat_hub_id = #session.hub#
	</cfquery>

<cflocation URL="index.cfm?u=2" addtoken="no">

</cfif>

