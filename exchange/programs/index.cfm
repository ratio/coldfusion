<cfinclude template="/exchange/security/check.cfm">

<style>
.program_badge {
    width: 22%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 275px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfquery name="cats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from program_cat
 where program_cat_hub_id = #session.hub#
 order by program_cat_order
</cfquery>

<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select top(4) * from program
 join program_cat on program_cat.program_cat_id = program.program_cat_id
 where program_hub_id = #session.hub#
 order by program_date ASC
</cfquery>

<cfquery name="all" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from program
 join program_cat on program_cat.program_cat_id = program.program_cat_id
 where program_hub_id = #session.hub#
 order by program_date DESC
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Programs and Events</td>
          <tr><td class="feed_sub_header" style="font-weight: normal;">Programs and Events are tools and events that help you grow and accelerate sustainable impact in the market.</td></tr>
        </table>
        </td><td align=right>

        <cfoutput query="cats">
         &nbsp;&nbsp;<img src="#media_virtual#/#program_cat_image#" style="width: 60px; height: 60px;">
        </cfoutput>

        </td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header">Upcoming Programs</td>
              <td align=right class="feed_sub_header" style="font-weight: normal;">

            <b>Find Program</b>
		    &nbsp;&nbsp;
			<input type="text" class="input_text" placeholder="Type in a keyword..." style="width: 250px; height: 30px;">
			&nbsp;&nbsp;
			<b>related to</b>
			&nbsp;&nbsp;
			<select name="related_to" class="input_select" style="width: 250px;">
			<option value=0>All Categories
			<cfoutput query="cats">
			 <option value=#program_cat_id#>#program_cat_name#
			</cfoutput>
			&nbsp;&nbsp;<input type="submit" name="button" value="Search" class="button_blue">

              </td> </tr>
          <tr><td height=20></td></tr>
        </table>

<cfoutput query="programs">

<div class="program_badge">

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=5></td></tr>
 <tr><td width=90 valign=top><img src="#media_virtual#/#programs.program_cat_image#" width=75></td>

     <td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header" style="padding-top: 0px;">#programs.program_name#</td></tr>
          <tr><td class="link_small_gray" valign=top><img src="/images/icon_date.png" width=18>&nbsp;&nbsp;#dateformat(programs.program_date,'mmm d, yyyy')#, #programs.program_time#</td></tr>
          <tr><td class="link_small_gray" valign=top><img src="/images/icon_location.png" width=18>&nbsp;&nbsp;<cfif #programs.program_location# is "">Not specified<cfelse>#programs.program_location#</cfif></td></tr>

		</table>

     </td></tr>
 <tr><td height=5></td></tr>
 <tr><td colspan=2 class="feed_option">
      <cfif len(programs.program_desc GT 220)>
       #left(programs.program_desc,220)#...
      <cfelse>
       #programs.program_desc#
      </cfif>
     </td></tr>
 <tr><td height=5></td></tr>
 <tr><td colspan=2><hr></td></tr>

 <tr><td align=center colspan=2>
     <input type="submit" name="button" class="button_blue" style="background-color: green;" value="Details">&nbsp;
     <input type="submit" name="button" class="button_blue" style="background-color: green;" value="RSVP"></td></tr>


</table>

</div>

</cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header">More Programs</td></tr>
          <tr><td><hr></td></tr>
          <tr><td height=20></td></tr>
        </table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>


        <cfoutput query="all">


 <tr><td height=5></td></tr>
 <tr><td width=90 valign=top><img src="#media_virtual#/#all.program_cat_image#" width=75></td>

     <td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header" style="padding-top: 0px;">#all.program_name#</td></tr>
          <tr><td class="link_small_gray" valign=top><img src="/images/icon_date.png" width=18>&nbsp;&nbsp;#dateformat(all.program_date,'mmm d, yyyy')#, #all.program_time#</td></tr>
          <tr><td class="link_small_gray" valign=top><img src="/images/icon_location.png" width=18>&nbsp;&nbsp;<cfif #all.program_location# is "">Not specified<cfelse>#all.program_location#</cfif></td></tr>

		</table>

     </td>

     <td align=right>

     <input type="submit" name="button" class="button_blue" value="Details" style="background-color: green;">

     </td>

     </tr>
 <tr><td height=5></td></tr>
 <tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;">
       #all.program_desc#

     </td></tr>
 <tr><td height=5></td></tr>
 <tr><td colspan=3><hr></td></tr>
 <tr><td height=10></td></tr>


        </cfoutput>


</table>

 	  </div>

	  </td></tr>

   </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>