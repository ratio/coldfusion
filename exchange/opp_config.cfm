<cfinclude template="/exchange/security/check.cfm">

<cfquery name="opp_data" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <div class="main_box">

          <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">CONFIGURE DASHBOARD</td><td align=right class="feed_option"><a href="/exchange/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">Please select the areas you would like to include on your Dashboard and the keywords used for the summaries.</td></tr>
			   <tr><td height=10></td></tr>
			  </table>

		   <form action="opp_config_save.cfm" method="post">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td valign=top width=40><input name="usr_dashboard_opp" type="checkbox" checked required style="width: 20px; height: 20px; margin-top: 10px;" <cfif opp_data.usr_dashboard_opp is 1>checked></cfif></td>

			  <td valign=top>

 			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_sub_header">OPPORTUNITY DASHBOARD</td></tr>
				<tr><td class="feed_sub_header" style="font-weight: normal;"><b>Search Keywords</b></td></tr>
				<tr><td><input name="keywords" type="text" class="input_text" size=100 maxlength="150" value="#opp_data.usr_keywords#"></td></tr>
				<tr><td class="link_small_gray" colspan=2>Opportunity keywords allow you to filter Opportunities on your dashboard to matching keywords (i.e., Machine Learning, Clinical, etc.).</td></tr>
				<tr><td height=20></td></tr>
               </table>

             </td></tr>

             <!---

             <tr><td colspan=2><hr></td></tr>
             <tr><td height=10></td></tr>

			 <tr><td valign=top><input name="usr_dashboard_comp" type="checkbox" style="width: 20px; height: 20px; margin-top: 10px;" <cfif opp_data.usr_dashboard_comp is 1>checked</cfif>></td>

			 <td valign=top>

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header">COMPANY SUMMARY</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Keywords</b></td></tr>
			  <tr><td><input name="usr_dashboard_comp_keywords" type="text" class="input_text" size=100 maxlength="150" value="#opp_data.usr_dashboard_comp_keywords#"></td></tr>
			  <tr><td class="link_small_gray" colspan=2>Company keywords allow you to filter Companies based on matching name, description or capability keywords (i.e., Machine Learning, Clinical, etc.).</td></tr>
             </table>

             </td></tr>

             <tr><td height=20></td></tr> --->

             <tr><td colspan=2><hr></td></tr>
             <tr><td height=10></td></tr>
			 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Update"</td></tr>

           </table>

          </cfoutput>

          </form>

	  </div>

	  </td>

      <td valign=top width=185>

	  <cfinclude template="/exchange/network.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td>


	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

