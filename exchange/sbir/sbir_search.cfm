 <cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total from sbir
 </cfquery>

 <cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  order by state_name
 </cfquery>

 <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(award_amount) as value, department from sbir
  where department is not null
  group by department
  order by department
 </cfquery>

 <cfquery name="year" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(award_amount) as value, award_year from sbir
  where department is not null
  group by award_year
  order by award_year
 </cfquery>

 <cfquery name="state_awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select state,count(id) as total, sum(award_amount) as value from sbir
  where state is not null
  group by state
  order by state
 </cfquery>

<cfif not isdefined("session.grant_from")>
	<cfset session.grant_from = dateadd('yyyy',-2,now())>
</cfif>

<cfif not isdefined("session.grant_to")>
	<cfset session.grant_to = now()>
</cfif>

 <cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header"><img src="/images/icon_light.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;<a href="index.cfm">Search Federal SBIR/STTR Awards ( #trim(numberformat(sbir.total,'999,999'))# )</a></td>
		  <td align=right></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	 </table>
 </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <form action="/exchange/sbir/set_sbir.cfm" method="post">

         <tr>
             <td class="feed_option"><b>Keyword</b></td>
             <td class="feed_option"><b>Department</b></td>
             <td class="feed_option"><b>State</b></td>
             <td class="feed_option"><b>Type</b></td>
             <td class="feed_option"><b>Phase</b></td>
             <td class="feed_option"><b>Award Year</b></td>
        </tr>

        <tr>
            <td width=200><input type="text" class="input_text" name="keyword" style="width: 300px;" placeholder="keyword" <cfif isdefined("session.sbir_search_keyword")>value="<cfoutput>#session.sbir_search_keyword#</cfoutput>"</cfif>></td>

            <td width=320>
             <select name="department" class="input_select" style="width:300px;">
             <option value=0>All
             <cfoutput query="dept">
             <option value="#department#" <cfif isdefined("session.sbir_search_department") and session.sbir_search_department is #department#>selected</cfif>>#department#
             </cfoutput>
             </select></td>

            <td width=180>
             <select name="state" class="input_select">
             <option value=0>All
             <cfoutput query="state">
             <option value="#state_abbr#"<cfif isdefined("session.sbir_search_state") and session.sbir_search_state is #state_abbr#>selected</cfif>>#state_name#
             </cfoutput>
             </select></td>

            <td width=80>
             <select name="program" class="input_select">
             <option value=0>All
             <option value="SBIR" <cfif isdefined("session.sbir_search_program") and session.sbir_search_program is "SBIR">selected</cfif>>SBIR
             <option value="STTR" <cfif isdefined("session.sbir_search_program") and session.sbir_search_program is "STTR">selected</cfif>>STTR
             </select></td>

            <td width=115>
             <select name="phase" class="input_select">
             <option value=0>All
             <option value="Phase I" <cfif isdefined("session.sbir_search_phase") and session.sbir_search_phase is "Phase I">selected</cfif>>Phase I
             <option value="Phase II" <cfif isdefined("session.sbir_search_phase") and session.sbir_search_phase is "Phase II">selected</cfif>>Phase II
             </select>&nbsp;&nbsp;</td>

            <td width=100>
             <select name="award_year" class="input_select">
             <option value=0>All
             <cfoutput query="year">
             <option value=#award_year# <cfif isdefined("session.sbir_search_award_year") and session.sbir_search_award_year is #award_year#>selected</cfif>>#award_year#
             </cfoutput>
             </select>

            </td>

            <td><input class="button_blue" type="submit" name="button" value="Search"></td>

            </tr>

         </form>

         </table>
