<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">SBIR/STTR AWARDS</td>
         <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td class="feed_sub_header"><b>STATE - <cfoutput>#state#</cfoutput></b></td></tr>
        </table>

		 <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select * from sbir
		  where state = '#state#'
		 </cfquery>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr height=40>
              <td class="text_xsmall"><b>Award Title</b></td>
              <td class="text_xsmall" width=125><b>Contract</b></td>
              <td class="text_xsmall"><b>Awardee</b></td>
              <td class="text_xsmall"><b>City</b></td>
              <td class="text_xsmall" align=center width=50><b>State</b></td>
              <td class="text_xsmall"><b>Program</b></td>
              <td class="text_xsmall" width=50><b>Phase</b></td>
              <td class="text_xsmall" width=30><b>Year</b></td>
              <td class="text_xsmall" align=right width=75><b>Amount</b></td>
          </tr>

          <cfset counter = 0>

          <cfoutput query="awards">

		 <cfif counter is 0>
		  <tr bgcolor="ffffff" height=30>
		 <cfelse>
		  <tr bgcolor="e0e0e0" height=30>
		 </cfif>

               <td class="text_xsmall"><a href="detail.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_title#</b></a></td>
			   <td class="text_xsmall">#contract#</a></td>
			   <td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(company)#</b></a></td>
               <td class="text_xsmall">#city#</td>
               <td class="text_xsmall" align=center>#state#</td>
               <td class="text_xsmall">#program#</td>
               <td class="text_xsmall">#phase#</td>
               <td class="text_xsmall">#award_year#</td>
               <td class="text_xsmall" align=right>#numberformat(award_amount,'$999,999,999')#</td>

           </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>


          </cfoutput>



         </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>