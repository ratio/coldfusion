<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

	  <div class="main_box">
	   <cfinclude template="/exchange/sbir/sbir_search.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">SBIR / STTR Programs </td>
             <td class="feed_header" align=right></td></tr>
         <tr><td height=5></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>
         The Small Business Innovation Research (SBIR) program is a United States Government program, coordinated by the Small Business
         Administration, that provides funding to help small businesses conduct research and development (R&D).
         The Small Business Technology Transfer (STTR) is another program that expands funding opportunities in the
         federal innovation research and development (R&D) arena.  To find out more information, please visit <a href="https://www.sbir.gov/" target="_blank" rel="noopener" rel="noreferrer">https://www.sbir.gov/</a>.
         </td></tr>
        </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td colspan=8><hr></td></tr>

         <tr><td class="feed_sub_header" colspan=8><b>Quick Views</b></td></tr>
         <tr><td height=5></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=40>
				<td class="feed_option" width=70><b>State</b></td>
				<td class="feed_option" align=center><b>Awards</b></td>
				<td class="feed_option" align=right><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="state_awards">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="detail_state.cfm?state=#state#"><b>#ucase(state)#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td valign=top width=30>&nbsp;</td>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=40>
				<td class="feed_option"><b>Department</b></td>
				<td class="feed_option" align=center><b>Awards</b></td>
				<td class="feed_option" align=right><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="dept">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="detail_dept.cfm?dept=#department#"><b>#ucase(department)#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td valign=top width=30>&nbsp;</td>

         <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=40>
				<td class="feed_option"><b>Year</b></td>
				<td class="feed_option" align=center><b>Awards</b></td>
				<td class="feed_option" align=right><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="year">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="detail_year.cfm?year=#award_year#"><b>#ucase(award_year)#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td width=30>&nbsp;</td>

         <td valign=top>

			 <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select count(id) as total, sum(award_amount) as value, phase from sbir
			  where department is not null
			  group by phase
			  order by phase
			 </cfquery>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=40>
				<td class="feed_option"><b>Phase</b></td>
				<td class="feed_option" align=center><b>Awards</b></td>
				<td class="feed_option" align=right><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="dept">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>
				  <td class="feed_option"><a href="detail_phase.cfm?phase=#phase#"><b>#ucase(phase)#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>
			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td></tr>

        </table>

        </td></tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>