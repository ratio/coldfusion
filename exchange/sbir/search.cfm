<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 11>
</cfif>

 <cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  insert into keyword_search
  (
   keyword_search_usr_id,
   keyword_search_company_id,
   keyword_search_hub_id,
   keyword_search_keyword,
   keyword_search_date,
   keyword_search_area
   )
   values
   (
   #session.usr_id#,
   #session.company_id#,
   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  '#session.sbir_search_keyword#',
   #now()#,
   3
   )
 </cfquery>

 <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from sbir
  left join orgname on orgname_name = department
  where contains((abstract,agency,award_title,company,department,program,research_keywords,RI_name,solicitation_number),'#trim(session.sbir_search_keyword)#')

  <cfif #session.sbir_search_department# is not 0>
   and department = '#session.sbir_search_department#'
  </cfif>

  <cfif #session.sbir_search_state# is not 0>
   and state = '#session.sbir_search_state#'
  </cfif>

  <cfif #session.sbir_search_program# is not 0>
   and program = '#session.sbir_search_program#'
  </cfif>

  <cfif #session.sbir_search_phase# is not 0>
   and phase = '#session.sbir_search_phase#'
  </cfif>

  <cfif #session.sbir_search_award_year# is not 0>
   and award_year = #session.sbir_search_award_year#
  </cfif>

	<cfif sv is 1>
     order by department ASC
	<cfelseif sv is 10>
     order by department DESC
	<cfelseif sv is 2>
     order by contract ASC
	<cfelseif sv is 20>
     order by contract DESC
	<cfelseif sv is 3>
     order by company ASC
	<cfelseif sv is 30>
     order by company DESC
	<cfelseif sv is 4>
     order by city ASC
	<cfelseif sv is 40>
     order by city DESC
	<cfelseif sv is 5>
     order by state ASC
	<cfelseif sv is 50>
     order by state DESC
	<cfelseif sv is 6>
     order by program ASC
	<cfelseif sv is 60>
     order by program DESC
	<cfelseif sv is 7>
     order by phase ASC
	<cfelseif sv is 70>
     order by phase DESC
	<cfelseif sv is 8>
     order by award_year ASC
	<cfelseif sv is 80>
     order by award_year DESC
	<cfelseif sv is 9>
     order by award_amount DESC
	<cfelseif sv is 90>
     order by award_amount ASC
	<cfelseif sv is 11>
     order by award_start_date DESC
	<cfelseif sv is 110>
     order by award_start_date ASC
	</cfif>

 </cfquery>

 <cfset perpage = 200>

 <cfparam name="url.start" default="1">
 <cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
 </cfif>

 <cfset totalPages = ceiling(agencies.recordCount / perpage)>
 <cfset thisPage = ceiling(url.start / perpage)>

 <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td>

	  <div class="main_box">
	   <cfinclude template="/exchange/sbir/sbir_search.cfm">
	  </div>

      <div class="main_box">

        <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=top>Search Results (#trim(numberformat(agencies.recordcount,'99,999'))#)</td>
             <td align=right valign=top class="feed_sub_header">


					<cfif agencies.recordcount GT #perpage#>
						Page <b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif></td>
			 </tr>
         <tr><td colspan=3><hr></td></tr>

         <tr><td class="feed_sub_header" colspan=5>You searched for "#replace(session.sbir_search_keyword,'"','','all')#"</td></tr>

        </table>

        </cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr height=40>
              <td></td>

             <td class="feed_option"><b>Description</b></td>
             <td class="feed_option"><a href="search.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Department</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option"><a href="search.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Awardee</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option"><a href="search.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option" align=center><a href="search.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option" align=center><a href="search.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Program</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option"><a href="search.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Phase</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option"><a href="search.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Year</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option" align=right><a href="search.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Amount</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 9><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 90><img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_option" align=right><a href="search.cfm?<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Date</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 11><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 110><img src="/images/icon_sort_down.png" width=15></cfif></td>

</tr>

         <cfset counter = 0>

          <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

		 <cfif counter is 0>
		  <tr bgcolor="ffffff" height=40>
		 <cfelse>
		  <tr bgcolor="e0e0e0" height=40>
		 </cfif>

               <td width=60>
               <cfif orgname_logo is not "">
               <img src="#image_virtual#/#agencies.orgname_logo#" vspace=5 width=40 border=0>
               <cfelse>
               <img src="#image_virtual#/icon_usa.png" vspace=5 width=40 border=0>

               </cfif></td>

               <td class="text_xsmall">

               <a href="detail.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>

               <cfif trim(session.sbir_search_keyword) is not "">
                #replaceNoCase(award_title,session.sbir_search_keyword,"<span style='background:yellow'>#session.sbir_search_keyword#</span>","all")#
               <cfelse>
                #award_title#
               </cfif>

               </b></a>

               </td>

               <td class="text_xsmall" width=200>#ucase(department)#</td>
			   <td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(company)#</b></a></td>
               <td class="text_xsmall">#city#</td>
               <td class="text_xsmall" align=center width=60>#state#</td>
               <td class="text_xsmall" align=center width=60>#program#</td>
               <td class="text_xsmall">#phase#</td>
               <td class="text_xsmall" align=center>#award_year#</td>
               <td class="text_xsmall" align=right>#numberformat(award_amount,'$999,999,999')#</td>
               <td class="text_xsmall" align=right width=100>#dateformat(award_start_date,'mm/dd/yyyy')#</td>

               </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

          </cfoutput>

         </table>

	  </div>

</td></tr>

</table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>