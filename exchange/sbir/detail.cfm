<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

  <cfquery name="detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from sbir
   where id = #id#
  </cfquery>

<cfquery name="insert_recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 insert recent(recent_sbir_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">SBIR/STTR Award Information</td>
             <td class="feed_sub_header" align=right>
             <img src="/images/delete.png" border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

        <cfoutput query="detail">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>

               <td valign=top>


                    <table cellspacing=0 cellpadding=0 border=0 width=100%>

 						<tr>
 						   <td class="feed_sub_header" valign=top><b>Awardee</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer"><u><b>#ucase(company)#</b></u></a><br>Location - #city#, #state#  #zip#<br>Employees - <cfif #number_employees# is "">Unknown<cfelse>#number_employees#</cfif></td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header" valign=top><b>Awardee DUNS</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#duns#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header" valign=top><b>Business Contact</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">
 						   #contact_name#<br>
 						   #contact_title#<br>
 						   #contact_phone#<br>
 						   #contact_email#</td>
 						</tr>

                    </table>

               </td><td valign=top>

                    <table cellspacing=0 cellpadding=0 border=0 width=100%>

 						<tr>
 						   <td class="feed_sub_header"><b>Department</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#department#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Contract Number</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#contract#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Solicitation Number</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#solicitation_number#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Amount</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#numberformat(award_amount,'$999,999,999.99')#</td>
 						</tr>

                    </table>

               </td><td valign=top>

                    <table cellspacing=0 cellpadding=0 border=0 width=100%>

 						<tr>
 						   <td class="feed_sub_header"><b>Program / Type</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#program# / #phase#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Soliciation Year</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#Solicitation_Year#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Award Year</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;">#award_year#</td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Start Date</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;"><cfif #award_start_date# is "">Not Provided<cfelse>#dateformat(award_start_date,'mm/dd/yyyy')#</cfif></td>
 						</tr>

 						<tr>
 						   <td class="feed_sub_header"><b>Close Date</b></td>
 						   <td class="feed_sub_header" style="font-weight: normal;"><cfif award_close_date is "">Not Provided<cfelse>#dateformat(award_close_date,'mm/dd/yyyy')#</cfif></td>
 						</tr>

                    </table>

               </td>

           </tr>

           <tr><td colspan=4><hr></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
              <td class="feed_sub_header" width=100>Hub Zone</td>
              <td width=40>

              <cfif hubzone_owned is "Y">
               <img src="/images/box_checked.png" width=20>
              <cfelse>
               <img src="/images/box_unchecked.png" width=20>
              </cfif>

              </td>
              <td class="feed_sub_header" width=350>Socially and Economically Disadvantaged</td>

              <td width=40>

              <cfif socially_and_economically_disadvantaged is "Y">
               <img src="/images/box_checked.png" width=20>
              <cfelse>
               <img src="/images/box_unchecked.png" width=20>
              </cfif>

              </td>

              <td class="feed_sub_header" width=150>Woman Owned</td>

              <td>

              <cfif woman_owned is "Y">
               <img src="/images/box_checked.png" width=20>
              <cfelse>
               <img src="/images/box_unchecked.png" width=20>
              </cfif>

              </td></tr>

        </table>


         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td><hr></td></tr>


           <tr><td class="feed_sub_header" valign=top><b>Award Title</b></td></tr>
           <tr><td class="feed_sub_header" style="font-weight: normal;">

               <cfif isdefined("session.sbir_search_keyword")>
                #replaceNoCase(award_title,session.sbir_search_keyword,"<span style='background:yellow'>#session.sbir_search_keyword#</span>","all")#
               <cfelse>
                #award_title#
               </cfif>

               </td></tr>

           <tr><td class="feed_sub_header" valign=top><b>Abstract</b></td></tr>
           <tr><td class="feed_sub_header" style="font-weight: normal;">


               <cfif isdefined("session.sbir_search_keyword")>
                #replaceNoCase(detail.abstract,session.sbir_search_keyword,"<span style='background:yellow'>#session.sbir_search_keyword#</span>","all")#
               <cfelse>
                #detail.abstract#
               </cfif>
               </td></tr>


           <tr><td class="feed_sub_header" valign=top><b>Research Keywords</b></td></tr>
           <tr><td class="feed_sub_header" style="font-weight: normal;">

               <cfif research_keywords is "">
                No keywords provided.
               <cfelse>
               <cfif isdefined("session.sbir_search_keyword")>
                #replaceNoCase(research_keywords,session.sbir_search_keyword,"<span style='background:yellow'>#session.sbir_search_keyword#</span>","all")#
               <cfelse>
                <cfif #research_keywords# is "">Not provided<cfelse>#research_keywords#</cfif>
               </cfif>
               </cfif>

           </td></tr>

         </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td><hr></td></tr>

			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">
			   <b>Research Institute</b><br><br>
			   <cfif ri_name is "">Not Provided<cfelse>#ri_name#</cfif><br>
			   <cfif ri_poc_phone is not "">
			   <cfelseif ri_poc_phone is "() -">
			   <cfelseif trim(ri_poc_phone) is "">
			   <cfelseif ri_phone_phone is " ">
			   <cfelse>
			   Phone - #ri_poc_phone#
			   </cfif>

            </td>
            </tr>

			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">
			   <b>Principal Investigator</b><br><br>
			   <cfif #pi_name# is "">Not Provided<cfelse>#pi_name#</cfif><br>
			   <cfif pi_phone is not "">Phone - #pi_phone#<br></cfif>
			   <cfif pi_email is not "">Email - #pi_email#</cfif>

			</td>
			</tr>


		</table>

      </cfoutput>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>