<cfinclude template="/exchange/security/check.cfm">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="lenses" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from lens
 order by lens_order
</cfquery>

<cfquery name="align" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select align_type_value from align
 where (align_usr_id = #session.usr_id#) and
       (align_type_id = 1)
</cfquery>

<cfset #lens_list# = #valuelist(align.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	      <form action="lens_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td colspan=3 class="feed_header">Acceleration Lenses</td></tr>
           <tr><td>&nbsp;</td></tr>

           <tr><td class="feed_option" colspan=3>Acceleration lenses are categories the Exchange uses to send you targeted opportunities for growth.</td></tr>
           <tr><td>&nbsp;</td></tr>

           <cfif isdefined("u")>
           	<tr><td colspan=4><font color="green"><b>Lens Subscription Updated</b></font></td></tr>
           	<tr><td>&nbsp;</td></tr>
           </cfif>

           <tr><td class="feed_option" width=90><b>Subscribe</b></td>
               <td class="feed_option"><b>Acceleration Lens</b></td>
               <td class="feed_option"><b>Value to Your Company</b></td>
           </tr>

           <cfset #bgcolor# = "ffffff">
           <cfset #counter# = 1>
           <cfoutput query="lenses">
            <cfif #counter# is 1>
             <cfset #bgcolor# = "ffffff">
            <cfelse>
             <cfset #bgcolor# = "e0e0e0">
            </cfif>

            <tr bgcolor=#bgcolor#>
             <td align=center><input type="checkbox" name="align_type_value" value=#lenses.lens_id# <cfif listfind(lens_list,lenses.lens_id)>Checked</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td width=300 class="feed_option">#lenses.lens_name#</td>
             <td class="feed_option">#lenses.lens_desc#</td>
            </tr>

            <cfif #counter# is 1>
             <cfset #counter# = 0>
            <cfelse>
             <cfset #counter# = 1>
            </cfif>

           </cfoutput>

           <tr><td>&nbsp;</td></tr>
           <tr><td colspan=3>
           <input class="button_blue" type="submit" name="button" value="Update">&nbsp;
           <input class="button_blue" type="submit" name="button" value="Cancel">

           </td></tr>

 		  </table>

 		  </form>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

