<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Cancel">
 <cflocation URL="/exchange/" addtoken="no">
</cfif>

<cfif isdefined("align_type_value")>

<cftransaction>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete align
		  where (align_type_id = 1) and
		        (align_usr_id = #session.usr_id#)
		</cfquery>

		<cfloop index="element" list=#align_type_value#>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#session.usr_id#, 1, #element#)
			</cfquery>

		</cfloop>

</cftransaction>

</cfif>

<cflocation URL="/exchange/index.cfm?u=1" addtoken="no">


