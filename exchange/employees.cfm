<cfinclude template="/exchange/security/check.cfm">

<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join hub_xref on hub_xref_usr_id = usr_id
 where hub_xref_role_id = 1 and
       hub_xref_active = 1 and
       hub_xref_hub_id = #session.hub#

 <cfif isdefined("session.people_keyword")>
   and contains((usr_full_name, usr_first_name, usr_last_name, usr_keywords),'"#trim(session.people_keyword)#"')
 </cfif>

 order by hub_xref_joined
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 185px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
<td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/profile_company.cfm">

      </td><td valign=top>

	  <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr>
				       <form action="employees_search.cfm" method="post">
				       <td class="feed_header"><a href="employees.cfm">BOOZ ALLEN EMPLOYEES</a></td>

				       <td align=right>

				       <span class="feed_sub_header">Find Employee</span>&nbsp;&nbsp;

                       <cfoutput>
				       <input type="text" class="input_text" name="people_keyword" style="width: 200px;" placeholder="keyword" <cfif isdefined("session.people_keyword")>value="#session.people_keyword#"</cfif>>
				       &nbsp;
				       <input type="submit" name="button" class="button_blue" value="Search">
				       <input type="submit" name="button" class="button_blue" value="Clear">
				       </cfoutput>

				       </td>

				       </form>

				       </tr>

				       <tr><td colspan=2><hr></td></tr>
				       <tr><td height=10>&nbsp;</td></tr>

       <cfif isdefined("emp_keyword")>
        <tr><td class="feed_sub_header">SEARCH RESULTS</td></tr>
       </cfif>

       <cfif members.recordcount is 0>
        <tr><td class="feed_sub_header" style="font-weight: normal;">No Employees were found.  <cfif isdefined("emp_keyword")><a href="employees.cfm"><b>Show All</b></a> employees.</cfif></td></tr>
       </cfif>

	   </table>

       <center>

	   <cfoutput query="members">

           <cfif members.usr_profile_display is 2>

			<div class="member_badge">

	         <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td height=30></td></tr>
	          <tr><td align=center><img src="/images/headshot.png" height=120></td></tr>
	          <tr><td class="feed_sub_header" align=center>Private Profile</td></tr>
	         </table>

            </div>

           <cfelse>

			<div class="profile_badge">

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <tr><td valign=top width=140>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif #members.usr_photo# is "">
					  <tr><td align=center valign=top width=130><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=125 width=125 border=0 vspace=15></td><td width=20>&nbsp;</td></tr>
					 <cfelse>
					  <tr><td align=center valign=top width=130><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 2px;" vspace=15 src="#media_virtual#/#members.usr_photo#" height=125 width=125 border=0><td width=20>&nbsp;</td></tr>
					 </cfif>

					 </table>

				 <td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=15></td></tr>
				 <tr><td class="feed_sub_header" style="padding-top: 0px;"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#members.usr_first_name# #members.usr_last_name#</a></td></tr>
				 <tr><td class="feed_option" style="padding-top: 0px; padding-bottom: 0px;"><b>#ucase(members.usr_company_name)#</b></a></td></tr>
				 <tr><td class="feed_option" style="padding-top: 5px; padding-bottom: 8px;">#members.usr_title#</td></tr>
			     <tr><td class="feed_option" style="font-weight: normal;"><img src="/images/icon-email_2.png" alt="Email Address" title="Email Address" width=20 valign=middle>&nbsp;&nbsp;#decrypt(members.usr_email,session.key, "AES/CBC/PKCS5Padding", "HEX")#</td></tr>

					<tr><td height=10></td></tr>
				<tr><td colspan=3 class="text_xsmall"><font color="gray"><i>Last updated on #dateformat(members.usr_updated,'mmm dd, yyyy')#</i></font></td></tr>
				</table>

				</td></tr>

				</table>

			</div>

			</cfif>

	   </cfoutput>

	   </center>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

