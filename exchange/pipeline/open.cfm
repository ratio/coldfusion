<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="check_access.cfm">

<cfquery name="pipe_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pipeline
 where pipeline_id = #session.pipeline_id#
</cfquery>

<cfquery name="pipe_summary" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(deal_id) as count, sum(deal_value_total) as total, max(deal_value_total) as largest, min(deal_value_total) as smallest, avg(deal_value_total) as average from deal
 where deal_pipeline_id = #session.pipeline_id#
</cfquery>

<cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select deal_id, deal_name, deal_type, deal_customer_name, deal_close_date, deal_pop_city as deal_city, deal_pop_state as deal_stage, deal_naics as naics, deal_psc as psc, deal_past_sol as past_solicitation, deal_current_sol as deal_current_solicitation, usr_first_name, usr_last_name, deal_stage_name, deal_stage_value, deal_value_total, deal_updated, usr_photo from deal
  left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
  left join usr on deal_owner_id = usr_id
  where deal_pipeline_id = #session.pipeline_id#
  order by deal_updated DESC
</cfquery>

<cfif isdefined("export")>
  <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="pipeline.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <cfoutput>

		   <div class="main_box">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_header">


			 <cfif #pipeline.pipeline_image# is "">
			    <img src="/images/stock_pipeline.png" width=40 style="margin-right: 5px;">
			 <cfelse>
			    <img src="#media_virtual#/#pipeline.pipeline_image#" style="margin-right: 5px;" width=40>
			 </cfif>


			    #pipe_info.pipeline_name#


			    </td>

				<td align=right class="feed_sub_header">

			   <cfif session.pipeline_access_level GTE 2>
				 <a href="sharing.cfm"><img src="/images/icon_sharing.png" width=20 alt="Share Board" title="Share Board" hspace=10></a><a href="/exchange/pipeline/sharing.cfm">Share Board</a>&nbsp;|&nbsp;
                 <a href="edit_pipeline.cfm"><img src="/images/icon_edit.png" width=20 hspace=10></a><a href="edit_pipeline.cfm">Edit Board</a>&nbsp;|&nbsp;
			   </cfif>

				<a href="index.cfm">All Boards</a></td></tr>
		    <tr><td colspan=2><hr></td></tr>
		   </table>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <cfif isdefined("u")>
			<cfif u is 1>
			 <tr><td colspan=2 class="feed_sub_header" style="color: green;">Opportunity has been successfully created.</td></tr>
			</cfif>
		   </cfif>

		   <tr>
		       <td class="feed_sub_header">Description</td>
		       <td class="feed_sub_header" align=center>## of Opps</td>
		       <td class="feed_sub_header" align=center>Total Value</td>
		       <td class="feed_sub_header" align=center>Largest Value</td>
		       <td class="feed_sub_header" align=center>Average Value</td>
		       <td class="feed_sub_header" align=center>Smallest Value</td>
		       <td class="feed_sub_header" align=right>Last Updated</td>


		       </tr>
		   <tr>
		       <td class="feed_sub_header" style="font-weight: normal;">
		       <cfif pipe_info.pipeline_desc is "">No Description Provided<cfelse>#pipe_info.pipeline_desc#</cfif>
		       </td>

		       <td class="feed_sub_header" style="font-weight: normal;" align=center>
		       #pipe_summary.count#
		       </td>

		       <td class="feed_sub_header" style="font-weight: normal;" align=center>
		       #numberformat(pipe_summary.total,'$999,999,999')#
		       </td>

		       <td class="feed_sub_header" style="font-weight: normal;" align=center>
		       #numberformat(pipe_summary.largest,'$999,999,999')#
		       </td>

		       <td class="feed_sub_header" style="font-weight: normal;" align=center>
		       #numberformat(pipe_summary.average,'$999,999,999')#
		       </td>

		       <td class="feed_sub_header" style="font-weight: normal;" align=center>
		       #numberformat(pipe_summary.smallest,'$999,999,999')#
		       </td>

		       <td class="feed_sub_header" style="font-weight: normal;" align=right>
		       #dateformat(pipe_info.pipeline_updated,'mmm dd, yyyy')#
		       </td>

		       </tr>

		   </table>

        </cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr><td colspan=2><hr></td></tr>
             <tr><td height=10></td></tr>
             <tr><td class="feed_header">Opportunities</td>
                 <td class="feed_sub_header" align=right>

                 <cfif session.pipeline_access_level GTE 2>
				    <img src="/images/plus3.png" width=15 hspace=10><a href="/exchange/pipeline/deals/add.cfm?l=1">Add Opportunity</a>
                 </cfif>

			     <cfif agencies.recordcount GT 0>
			     	<a href="open.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 hspace=10 border=0>
			     	<a href="open.cfm?export=1">Export to Excel</a>
			     </cfif>

                 </td></tr>

           </table>

           <cfif agencies.recordcount is 0>

           <cfoutput>
		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunities have been created for this Board.  To add an Opportunity to this board, <a href="/exchange/pipeline/deals/add.cfm?i=#encrypt(pipe_info.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&l=1"><b>click here</b></a></td></tr>
           </table>
           </cfoutput>

           <cfelse>

           <cfset counter = 0>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=10></td></tr>

             <tr>
                 <td class="feed_sub_header">Opportunity Name</td>
                 <td class="feed_sub_header">Customer</td>
                 <td class="feed_sub_header">Type</td>
                 <td class="feed_sub_header" align=center>Est Close Date</td>
                 <td class="feed_sub_header" align=center>Owner</td>
                 <td class="feed_sub_header">Stage</td>
                 <td class="feed_sub_header" align=right>Potential Value</td>
                 <td class="feed_sub_header" align=right>Last Updated</td>
             </tr>

             <cfoutput query="agencies">

             <cfif counter is 0>
              <tr bgcolor="ffffff">
             <cfelse>
              <tr bgcolor="e0e0e0">
             </cfif>

              <td class="feed_sub_header" style="font-weight: normal; padding-right: 20px;" width=600>
              <a href="/exchange/pipeline/deals/deal_open.cfm?i=#encrypt(deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&l=1">

              <cfif len(deal_name) GT 100>
               <b>#left(deal_name,100)#</b> ...
              <cfelse>
              <b>#deal_name#</b>
              </cfif>

              </a></td>
              <td class="feed_sub_header" style="font-weight: normal;" width=300>#deal_customer_name#</td>
              <td class="feed_sub_header" style="font-weight: normal; padding-right: 30px;">#deal_type#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center width=150>

              <cfif deal_close_date is "">
               TBD
              <cfelse>
              #dateformat(deal_close_date,'mmm dd, yyyy')#
              </cfif>
              </td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center width=100>

				 <cfif #usr_photo# is "">
				  <img src="#image_virtual#headshot.png" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#">
				 <cfelse>
				  <img src="#media_virtual#/#usr_photo#" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#">
				 </cfif>

              </td>
              <td class="feed_sub_header" style="font-weight: normal;" width=100>

              <cfif #deal_stage_name# is "">TBD<cfelse>
			  #deal_stage_name#
			 </cfif>

              </td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>

              <cfif deal_value_total is "">
               TBD
              <cfelse>
              #numberformat(deal_value_total,'$999,999,999')#
              </cfif>

              </td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>#dateformat(deal_updated,'mmm dd, yyyy')#</td>

             </tr>

             <cfif counter is 0>
              <cfset counter = 1>
             <cfelse>
              <cfset counter = 0>
             </cfif>

             </cfoutput>

           </table>

           </cfif>

		   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>