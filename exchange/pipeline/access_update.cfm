<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  update sharing
  set sharing_access = #a#
  where sharing_hub_id = #session.hub# and
        sharing_pipeline_id = #session.pipeline_id# and
        sharing_to_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
 </cfquery>

<cflocation URL="sharing.cfm?u=3" addtoken="no">