<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save Board">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select pipeline_image from pipeline
		  where pipeline_id = #session.pipeline_id# and
		        pipeline_usr_id = #session.usr_id#
		</cfquery>

		<cfif fileexists("#media_path#\#remove.pipeline_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.pipeline_image#">
		</cfif>

	</cfif>

	<cfif #pipeline_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select pipeline_image from pipeline
		  where pipeline_id = #session.pipeline_id#
		</cfquery>

		<cfif #getfile.pipeline_image# is not "">

		 <cfif fileexists("#media_path#\#getfile.pipeline_image#")>
		   <cffile action = "delete" file = "#media_path#\#getfile.pipeline_image#">
		 </cfif>

		</cfif>

		<cffile action = "upload"
		 fileField = "pipeline_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update pipeline
      set pipeline_name = '#pipeline_name#',
		  pipeline_desc = '#pipeline_desc#',

		  <cfif #pipeline_image# is not "">
		   pipeline_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   pipeline_image = null,
		  </cfif>

		  pipeline_updated = #now()#
      where pipeline_id = #session.pipeline_id#
	</cfquery>

	<cflocation URL="open.cfm?u=2" addtoken="no">

<cfelseif #button# is "Add Board">

	<cfif #pipeline_image# is not "">
		<cffile action = "upload"
		 fileField = "pipeline_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pipeline
	 (
      pipeline_name,
      pipeline_desc,
      pipeline_image,
      pipeline_usr_id,
      pipeline_company_id,
      pipeline_hub_id,
      pipeline_updated
      )
      values
      (
      '#pipeline_name#',
      '#pipeline_desc#',
       <cfif #pipeline_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
       #session.usr_id#,
       <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
       <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
       #now()#
      )
	</cfquery>

	<cflocation URL="/exchange/pipeline/index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete Board">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select pipeline_image from pipeline
	  where pipeline_id = #session.pipeline_id#
	</cfquery>

    <cfif remove.pipeline_image is not "">

     <cfif fileexists("#media_path#\#remove.pipeline_image#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.pipeline_image#">
	 </cfif>

	</cfif>

	<cftransaction>

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete pipeline
		 where pipeline_id = #session.pipeline_id#
		</cfquery>

	</cftransaction>

	<cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>
