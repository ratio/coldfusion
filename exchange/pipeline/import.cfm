<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<cfif session.pipeline_access_level GTE 2>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="pipe_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pipeline
 where pipeline_id = #session.pipeline_id#
</cfquery>

<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group
 where sharing_group_usr_id = #session.usr_id# and
       sharing_group_hub_id = #session.hub#
 order by sharing_group_name
</cfquery>

<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_id from sharing
 join usr on usr_id = sharing_to_usr_id
 where sharing_pipeline_id = #session.pipeline_id# and
       sharing_hub_id = #session.hub#
</cfquery>

<cfif users.recordcount is 0>
 <cfset user_list = 0>
<cfelse>
 <cfset user_list = valuelist(users.usr_id)>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
            <td class="feed_header">
             <cfoutput>
               Share Board - #pipe_info.pipeline_name#
             </cfoutput>

             </td>
            <td align=right class="feed_sub_header"><a href="/exchange/pipeline/open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
        <tr><td class="feed_header">Import Users</td></tr>
        <tr><td class="feed_sub_header" style="font-weight: normal;">Please select one or more users from your Groups below to grant them access to your Opportunity Board.</td></tr>
       </table>

          <form action="import_users.cfm" method="post">

          <cfloop query="groups">

			<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from sharing_group_usr
			 join usr on usr_id = sharing_group_usr_usr_id
			 where sharing_group_usr_group_id = #groups.sharing_group_id#
			 order by usr_last_name, usr_first_name
			</cfquery>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <cfoutput>
	          <tr><td height=10></td></tr>
              <tr><td class="feed_sub_header" bgcolor="e0e0e0" width=200 colspan=3>
              <a href="##" onclick="toggle_visibility('#groups.sharing_group_id#');"><img src="/images/plus3.png" width=15 hspace=5 border=0 alt="Expand" title="Expand"></a>
              &nbsp;
              <a href="##" onclick="toggle_visibility('#groups.sharing_group_id#');">#ucase(groups.sharing_group_name)#</a>

              </td>
              <td class="feed_sub_header" style="font-weight: normal;" bgcolor="e0e0e0">#groups.sharing_group_desc#</td></tr>
	          <tr><td height=10></td></tr>
            </cfoutput>
          </table>

          <cfoutput><div id="#groups.sharing_group_id#" style="display:none;"></cfoutput>

          <cfif members.recordcount is 0>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td colspan=5 class="feed_sub_header" style="font-weight: normal;">No users have been added to this Group.</td></tr>
			</table>
          <cfelse>

          <cfset counter = 1>
          <cfoutput query="members">

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td width=100 class="feed_sub_header">

                 <cfif listfind(user_list,members.usr_id)>
                 <span style="color: green;">Granted</span>
                 <cfelse>
                 	&nbsp;&nbsp;&nbsp;<input type="checkbox" class="input_text" name="share_id" value=#encrypt(members.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;">
                 </cfif>

                 </td>

                 <td width=80 class="feed_sub_header">

				 <cfif #members.usr_photo# is "">
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(members.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 border=0>
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(members.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 0px;" src="#media_virtual#/#members.usr_photo#" height=40 width=40 border=0>
				 </cfif>

				</td>

			    <td width=200 class="feed_sub_header"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(members.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#ucase(members.usr_first_name)# #ucase(members.usr_last_name)#</a></td>
			    <td width=200 class="feed_sub_header" style="font-weight: normal;">#ucase(members.usr_title)#</td>
			    <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(members.usr_email))#</td>
			 </tr>

			 <cfif counter LT members.recordcount>
			  <tr><td colspan=7><hr></td></tr>
			 </cfif>

			 <cfset counter = counter + 1>

			</table>

			</cfoutput>

            </cfif>

			</div>

			</cfloop>

 	        <table cellspacing=0 cellpadding=0 border=0 width=100%>
              <tr><td colspan=5><hr></td></tr>
         	  <tr><td height=5></td></tr>
              <tr><td colspan=5 class="link_small_gray">Selected users will be granted View Access by default.</td></tr>
              <tr><td height=10></td></tr>
              <tr><td colspan=5><input type="submit" name="button" class="button_blue_large" value="Import Users" onclick="return confirm('Import Users?\r\nAre you sure you want to share your Portfolio with the selected user(s)?);"></td></tr>
			</table>

			</form>

          </td></tr>


         </table>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>