<!--- Display Deals --->

<cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from deal
  left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
  join usr on usr_id = deal_owner_id
  left join deal_priority on deal_priority.deal_priority_id = deal.deal_priority_id
  where deal_pipeline_id = #pipeline.pipeline_id# and
		deal_comm_id is null
  order by deal_updated DESC
</cfquery>

<cfquery name="total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select sum(deal_value_total) as value from deal
where deal_pipeline_id = #pipeline.pipeline_id# and
	 deal_comm_id is null
</cfquery>

<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=middle width=50>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr>
			<cfif #pipeline.pipeline_image# is "">
			  <td>
			    <a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/stock_pipeline.png" width=50 border=0 alt="Open" title="Open"></a>
			  </td>
			<cfelse>
			  <td><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#pipeline.pipeline_image#" width=50 border=0 alt="Open" title="Open"></a></td>
			</cfif>

			</tr>
		 </table>

     <td width=20>&nbsp;</td><td valign=middle>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_title" valign=top><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(pipeline.pipeline_name) GT 40>#left(pipeline.pipeline_name,'40')#...<cfelse>#pipeline.pipeline_name#</cfif></a></td>
		       <td class="feed_title" align=right>

		       Total Value: <cfif #total.value# is "">$0<cfelse>#numberformat(total.value,'$999,999,999,999')#</cfif>

		       </td>
		   </tr>
		   <tr><td class="feed_option" colspan=2><b>Board Owner</b> - #pipeline.usr_first_name# #pipeline.usr_last_name#

		   </td></tr>

		 </table>

     </td></tr>

     <tr><td colspan=3><hr></td></tr>

     <tr><td colspan=3>

         <div class="scroll_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif deals.recordcount is 0>
			   <tr><td height=5></td></tr>
		       <tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunities have been created or added to this Board.<br><br><a href="/exchange/pipeline/deals/add.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&l=2"><b>Add Opportunity</b></a>.</td></tr>
			 <cfelse>
			   <tr><td height=5></td></tr>
			   <tr>
				   <td class="feed_option"><b>Opportunity</b></td>
				   <td class="feed_option"><b>Customer</b></td>
				   <td class="feed_option"><b>Type</b></td>
				   <td class="feed_option"><b>Owner</b></td>
				   <td class="feed_option" align=center><b>Priority</b></td>
				   <td class="feed_option" align=center><b>Stage</b></td>
				   <td class="feed_option" align=center><b>Est Close Date</b></td>
				   <td class="feed_option" style="padding-right: 10px;" align=right><b>Potential Value</b></td>
			   </tr>

			   <tr><td height=5></td></tr>

			   <cfset counter = 0>

			   <cfloop query="deals">

				   <cfoutput>

					   <cfif counter is 0>
						<tr bgcolor="ffffff" height=22>
					   <cfelse>
						<tr bgcolor="e0e0e0" height=22>
					   </cfif>

							 <td class="feed_option" style="padding-right: 20px;" valign=top width=500><a href="set.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&d=#encrypt(deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>

							 <cfif len(deals.deal_name) GT 200>
							  #left(deals.deal_name,200)#...
							 <cfelse>
							  #deals.deal_name#
							 </cfif>

							 </b></a></td>
							 <td class="feed_option" style="padding-right: 20px;" width=300 valign=top><cfif #deals.deal_customer_name# is "">TBD<cfelse>#deals.deal_customer_name#</cfif></td>
							 <td class="feed_option" style="padding-right: 20px;" valign=top>#deal_type#</td>

							 <td class="feed_option" style="padding-right: 20px;" valign=top width=100>#usr_first_name# #usr_last_name#</td>

							 <td class="feed_option" align=center valign=top width=100><cfif #deals.deal_priority_name# is "">TBD<cfelse>

							 #deals.deal_priority_name#

							 </cfif></td>


							 <td class="feed_option" align=center valign=top width=100><cfif #deals.deal_stage_name# is "">TBD<cfelse>

							 #deals.deal_stage_name#

							 </cfif></td>

							 <td class="feed_option" width=125 valign=top style="padding-right: 10px;" align=center>
							 <cfif #deals.deal_close_date# is "">TBD<cfelse>#dateformat(deals.deal_close_date,'mmm dd, yyyy')#</cfif>

							 </td>

							 <td class="feed_option" align=right valign=top width=125 style="padding-right: 10px;"><cfif #deals.deal_value_total# is "">TBD<cfelse>#numberformat(deals.deal_value_total,'$999,999,999,999')#</cfif></td>

					   </tr>

					   <cfif counter is 0>
						<cfset counter = 1>
					   <cfelse>
						<cfset counter = 0>
					   </cfif>

				   </cfoutput>

			   </cfloop>

			 </cfif>
 	     </table>

        </div>

       </td></tr>

    </table>

</cfoutput>