<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=middle width=70>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<cfif #pipeline.pipeline_image# is "">
			  <tr><td>
			    <a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/icon_pipeline_opp.png" width=70 border=0 alt="Open" title="Open"></a>
			  </td></tr>
			<cfelse>
			  <tr><td><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#pipeline.pipeline_image#" width=70 border=0 alt="Open" title="Open"></a></td></tr>
			</cfif>
		 </table>

     <td width=20>&nbsp;</td><td valign=middle>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_title" valign=top><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(pipeline.pipeline_name) GT 40>#left(pipeline.pipeline_name,'40')#...<cfelse>#pipeline.pipeline_name#</cfif></a></td>
		       <td class="feed_title" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="/exchange/pipeline/deals/add.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Add Deal</a></td>
		   </tr>
		   <tr><td class="feed_option" colspan=2><b>Manager</b> - #pipeline.usr_first_name# #pipeline.usr_last_name#

		   </td></tr>

		 </table>

     </td></tr>

     <tr><td colspan=3><hr></td></tr>

     <tr><td colspan=3>

          <!--- Display Deals --->

		  <cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from deal
			  left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
			  where deal_pipeline_id = #pipeline.pipeline_id#
		  </cfquery>

         <div class="scroll_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif deals.recordcount is 0>
			   <tr><td height=5></td></tr>
		       <tr><td class="feed_sub_header" style="font-weight: normal;">No Deals have been added to this Pipeline.<br><br>To add Deals, <a href="/exchange/pipeline/deals/add.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>Click here</b></a>.</td></tr>
			 <cfelse>
			   <tr><td height=5></td></tr>
			   <tr>
				   <td class="feed_option"><b>Deal</b></td>
				   <td class="feed_option"><b>Stage</b></td>
				   <td class="feed_option" align=center><b>Close Date</b></td>
				   <td class="feed_option" align=right><b>Deal</b></td>
			   </tr>

			   <tr><td height=5></td></tr>

			   <cfset counter = 0>

			   <cfloop query="deals">

				   <cfoutput>

					   <cfif counter is 0>
						<tr bgcolor="ffffff" height=22>
					   <cfelse>
						<tr bgcolor="e0e0e0" height=22>
					   </cfif>

							 <td class="feed_option" width=300><a href="deal_open.cfm?id=#encrypt(deals.deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#deals.deal_name#</b></a></td>
							 <td class="feed_option"><cfif #deals.deal_stage_name# is "">TBD<cfelse>#deals.deal_stage_name#</cfif></td>
							 <td class="feed_option" align=center width=75><cfif #deals.deal_close_date# is "">TBD<cfelse>#dateformat(deals.deal_close_date,'mm/dd/yyyy')#</cfif></td>
							 <td class="feed_option" align=right width=100><cfif #deals.deal_value_total# is "">TBD<cfelse>#numberformat(deals.deal_value_total,'$999,999,999,999')#</cfif></td>

					   </tr>

					   <cfif counter is 0>
						<cfset counter = 1>
					   <cfelse>
						<cfset counter = 0>
					   </cfif>

				   </cfoutput>

			   </cfloop>

			 </cfif>

 	     </table>

        </div>

       </td></tr>

    </table>

</cfoutput>