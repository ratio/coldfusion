<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.pipeline_view")>
 <cfset #session.pipeline_view# = 1>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.pipeline_badge_block {
    width: 98%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 335px;
    padding-top: 20px;
    padding-bottom: 30px;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

  <cfinclude template="/exchange/include/header.cfm">

        <cfif session.pipeline_view is 1>

          <cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from pipeline
			join usr on usr_id = pipeline_usr_id
			where pipeline_usr_id = #session.usr_id# and
			      pipeline_hub_id = #session.hub#
            order by pipeline_updated DESC
          </cfquery>

       <cfelseif session.pipeline_view is 2>

			<cfquery name="pipe_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select sharing_pipeline_id from sharing
			 where sharing_hub_id = #session.hub# and
			       sharing_pipeline_id is not null and
				   (sharing_to_usr_id = #session.usr_id#)
			</cfquery>

			<cfif pipe_access.recordcount is 0>
			 <cfset pipe_list = 0>
			<cfelse>
			 <cfset pipe_list = valuelist(pipe_access.sharing_pipeline_id)>
			</cfif>

            <cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select pipeline_name, pipeline_image, pipeline_id, usr_first_name, usr_last_name, pipeline_updated from pipeline
			  left join usr on usr_id = pipeline_usr_id
			  where pipeline_hub_id = #session.hub# and
					(pipeline_id in (#pipe_list#) or pipeline_usr_id = #session.usr_id#)
			  order by pipeline_updated DESC
			</cfquery>

       </cfif>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="pipeline.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="/exchange/pipeline/refresh_pipelines.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>Opportunity Boards</td>
        </cfoutput>
            <td align=right><span class="feed_sub_header">Filter Boards</span>&nbsp;&nbsp;

            <select name="pipeline_view" class="input_select" onchange="form.submit()">

              <option value=1 <cfif #session.pipeline_view# is 1>selected</cfif>>My Opportunity Boards
              <option value=2 <cfif #session.pipeline_view# is 2>selected</cfif>>All Opportunity Boards

            </select>

            </td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Opportunity Boards are groupings of Opportunities that you are interested in or have been shared with you.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>


       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Opportunity has been successfully created.</td></tr>
        <cfelseif u is 3>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Opportunity has been successfully deleted.</td></tr>
        <cfelseif u is 4>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Opportunity has been successfully added.</td></tr>
        <cfelseif u is 5>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Opportunity has been successfully deleted.</td></tr>
        </cfif>
       </cfif>

       </table>

       <center>

       <cfif #pipeline.recordcount# is 0>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunity Boards have been created.  To create a new Opportunity Board, <a href="/exchange/pipeline/create_pipeline.cfm"><b>click here</b></a>.</td></tr>
		   </table>

       <cfelse>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td height=20></td></tr>
		   </table>

			<cfloop query="pipeline">

				<div class="pipeline_badge_block">
					<cfinclude template="pipeline_badge.cfm">
				</div>

			</cfloop>


	    </cfif>

	   </center>

      </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>