<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("share_id")>

 <cftransaction>

 <cfloop index="d" list="#share_id#">

  <cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select sharing_id from sharing
   where sharing_hub_id = #session.hub# and
		 sharing_pipeline_id = #session.pipeline_id# and
		 sharing_to_usr_id = #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfif check.recordcount is 0>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into sharing
	 (
	  sharing_hub_id,
	  sharing_pipeline_id,
	  sharing_from_usr_id,
	  sharing_to_usr_id,
	  sharing_access
	 )
	 values
	 (
	 #session.hub#,
	 #session.pipeline_id#,
	 #session.usr_id#,
	 #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	 1
	 )
	</cfquery>

	</cfif>

  </cfloop>

 </cftransaction>

 	<cflocation URL="sharing.cfm?u=4" addtoken="no">

<cfelse>

	<cflocation URL="sharing.cfm?u=5" addtoken="no">

</cfif>

