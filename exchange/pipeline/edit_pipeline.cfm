<cfinclude template="/exchange/security/check.cfm">

<cfif session.pipeline_access_level GTE 2>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="pipe_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pipeline
 where pipeline_id = #session.pipeline_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

		   <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

       <div class="main_box">

       <form action="pipeline_save.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Opportunity Board</td>
            <td align=right class="feed_sub_header"><a href="/exchange/pipeline/open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

       <cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=175><b>Board Name</b></td>
             <td class="feed_option"><input type="text" class="input_text"  style="width: 534px;" name="pipeline_name" value="#pipe_info.pipeline_name#" style="width:300px;" placeholder="Please give this Opportunity Board a name." required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="pipeline_desc" class="input_textarea" cols=90 rows=5 placeholder="Please provide a high level description of this Opportunity Board.">#pipe_info.pipeline_name#</textarea></td>
             </td></tr>

		 <tr><td class="feed_sub_header" valign=top>Image</td>
		 	 <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #pipe_info.pipeline_image# is "">
					  <input type="file" name="pipeline_image">
					<cfelse>
					  <img src="#media_virtual#/#pipe_info.pipeline_image#" width=150><br><br>
					  <input type="file" name="pipeline_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
					 </cfif>

			  </td></tr>

          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td></td><td>

          <input class="button_blue_large" type="submit" name="button" value="Save Board">&nbsp;&nbsp;
          <input class="button_blue_large" type="submit" name="button" value="Delete Board" onclick="return confirm('Are you sure you want to delete this record?');">&nbsp;&nbsp;

          </td></tr>

          </cfoutput>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>