<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("share_id")>

 <cftransaction>

	 <cfloop index="d" list="#share_id#">

	 <cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete sharing
	  where sharing_hub_id = #session.hub# and
			sharing_pipeline_id = #session.pipeline_id# and
			sharing_to_usr_id = #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	 </cfquery>

	 </cfloop>

 </cftransaction>

</cfif>

<cflocation URL="sharing.cfm?u=2" addtoken="no">