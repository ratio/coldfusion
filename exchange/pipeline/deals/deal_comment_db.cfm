<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Comment">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal_comment
	 (
	 deal_comment_text,
	 deal_comment_deal_id,
	 deal_comment_hub_id,
	 deal_comment_usr_id,
	 deal_comment_date
	 )
	 values
	 (
	 '#deal_comment_text#',
	  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#
	  )
	</cfquery>

    <cfif isdefined("l")>
     <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=7" addtoken="no">
    </cfif>

    <cflocation URL="deal_open.cfm?i=#i#&u=31" addtoken="no">

<cfelseif button is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal_comment
	 set deal_comment_text = '#deal_comment_text#',
	     deal_comment_date = #now()#
	 where deal_comment_hub_id = #session.hub# and
	       deal_comment_id = #deal_comment_id#
	</cfquery>

    <cfif isdefined("l")>
     <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=8" addtoken="no">
    </cfif>


    <cflocation URL="deal_open.cfm?i=#i#&u=32" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_comment
	 where deal_comment_hub_id = #session.hub# and
	       deal_comment_id = #deal_comment_id#
    </cfquery>

    <cfif isdefined("l")>
     <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=9" addtoken="no">
    </cfif>

    <cflocation URL="deal_open.cfm?i=#i#&u=33" addtoken="no">

</cfif>
