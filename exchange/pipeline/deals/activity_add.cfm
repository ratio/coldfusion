<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">


<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">Add Activity</td>
                 <td class="feed_sub_header" align=right>

                 <cfif isdefined("l")>
                 <a href="/exchange/marketplace/communities/opp_open.cfm?i=#i#">Return</a>

                 <cfelse>
                 <a href="deal_open.cfm?i=#i#">Return</a>

                 </cfif>
                 </td></tr>
		     <tr><td colspan=2><hr></td></tr>
		  </table>

      </cfoutput>

        <form action="activity_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=95%>

        <cfif isdefined("u")>
         <tr><td colspan=2 class="feed_sub_header"><font color="red"><b>Error.  The start date cannot be greater or equal to the end date</b></font></td></tr>
        </cfif>

           <tr><td class="feed_sub_header"><b>Activity Name</b></td></tr>
           <tr><td class="feed_option"><input type="text" class="input_text" name="deal_activity_name" style="width: 400px;" maxlength="200" required></td></tr>
           <tr><td class="feed_sub_header"><b>Description</b></td></tr>
           <tr><td class="feed_option"><textarea class="input_textarea" name="deal_activity_desc" style="width: 800px; height: 100px;"></textarea></td></tr>
           <tr><td class="feed_sub_header"><b>Start Date</b></td></tr>
           <tr><td class="feed_option"><input class="input_date" type="date" min="2000-01-01" max="2100-12-31" name="deal_activity_start_date" required></td></tr>
           <tr><td class="feed_sub_header"><b>End Date</b></td></tr>
           <tr><td class="feed_option"><input class="input_date" type="date" min="2000-01-01" max="2100-12-31" name="deal_activity_end_date" required></td></tr>
           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>
           <tr><td><input type="submit" name="button" class="button_blue_large" value="Save Activity">

           <cfoutput>
           <input type="hidden" name="i" value=#i#>
           <cfif isdefined("l")>
            <input type="hidden" name="l" value=#l#>
           </cfif>
           </cfoutput>

		</table>

		</form>

      </td></tr>

   </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>