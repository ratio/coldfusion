<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 11>
</cfif>

<cfquery name="get_keyword" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_snapshot
 where partner_snapshot_id = #i#
</cfquery>

<cfset ky = #get_keyword.partner_snapshot_keyword#>
<cfset session.search_filter = #ky#>

<body class="body">

 <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

	select lab_tech.id, lab_tech.department, lab_tech.federallab, lab_tech.availabletechnology, lab_tech.shortdescription, lab_tech.fulldescription, labs.imageurl from lab_tech
	join labs on labs.federallab = lab_tech.federallab
    where lab_tech.id > 0
    and contains((lab_tech.availabletechnology, lab_tech.shortdescription, lab_tech.fulldescription),'#trim(session.search_filter)#')

    <cfif sv is 1>
     order by lab_tech.department ASC, lab_tech.federallab ASC
    <cfelseif sv is 10>
     order by lab_tech.department DESC, lab_tech.federallab ASC
    <cfelseif sv is 2>
    <cfelseif sv is 20>
    <cfelseif sv is 3>
     order by availabletechnology ASC
    <cfelseif sv is 30>
     order by availabletechnology DESC
    <cfelseif sv is 4>
    <cfelseif sv is 40>
    <cfelseif sv is 5>
    <cfelseif sv is 50>
    <cfelseif sv is 6>
    <cfelseif sv is 60>

    </cfif>

 </cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">

			   LAB TECH - "<i>#replaceNoCase(session.search_filter,'"','',"all")#</i>"</td>
				 <td class="feed_sub_header" align=right>
				 <a href="view_labs.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 hspace=10 alt="Export to Excel" title="Export to Excel"></a>
                 <a href="view_labs.cfm?i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
				 &nbsp;&nbsp;|&nbsp;&nbsp;

					 <a href="market.cfm?i=#session.pid#">Return</a>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

				    <td class="feed_sub_header">



				    </td>

				    <td class="feed_sub_header" align=right>

					<cfif agencies.recordcount GT #perpage#>

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#&i=#i#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#&i=#i#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				    </cfif>

				</td>

				</tr>

				<tr><td height=20></td></tr>
			</table>
        </cfoutput>



 			<table cellspacing=0 cellpadding=0 border=0 width=100%>


          <cfoutput>
          <tr>
             <td></td>
             <td class="feed_sub_header"><a href="view_labs.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>DEPARTMENT</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
             <td class="feed_sub_header"><a href="view_labs.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TECHNOLOGY</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
          </tr>
          </cfoutput>

          <cfset counter = 0>

		  <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

          <cfif counter is 0>
           <tr bgcolor="ffffff" height=90>
          <cfelse>
           <tr bgcolor="e0e0e0" height=90>
          </cfif>

              <td width=100>

                <cfif #imageurl# is "">
                <img src="/images/app_innovation.png" width=70>
                <cfelse>
                <img src="#imageurl#" width=70 onerror="this.src='/images/app_innovation.png'">
                </cfif>

              </td>
              <td class="feed_option" style="font-weight: normal;" width=300><b>#ucase(department)#</b><br>#federallab#</td>
              <td class="feed_option" style="font-weight: normal;">

              <a href="/exchange/include/labtech_information.cfm?id=#id#" target="_blank" rel="noopener"><b>#ucase(replaceNoCase(availabletechnology,session.search_filter,"<span style='background:yellow'>#ucase(session.search_filter)#</span>","all"))#</b></a>
              <br>
              #replaceNoCase(shortdescription,session.search_filter,"<span style='background:yellow'>#ucase(session.search_filter)#</span>","all")#

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>


          </table>

 	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>