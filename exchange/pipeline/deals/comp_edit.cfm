<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="snap" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comp_snapshot
 where comp_snapshot_id = #id#
</cfquery>

<cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal
 where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 order by department_name
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 order by agency_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <div class="main_box">

       <form action="comp_save.cfm" method="post" enctype="multipart/form-data" >

       <cfoutput>
       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Competition Snapshot</td>
            <td align=right class="feed_sub_header"><a href="companies.cfm?i=#i#&id=#id#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>
       </cfoutput>

       <cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=150><b>Name</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 534px;" value="#snap.comp_snapshot_name#" name="comp_snapshot_name" placeholder="Please give this snapshot name." required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="comp_snapshot_desc" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  cols=70 rows=5 placeholder="Please provide a high level description of this snapshot.">#snap.comp_snapshot_desc#</textarea></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Capabilities</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 534px;"
             value='#replaceNoCase(snap.comp_snapshot_keywords,'"','',"all")#'
             name="comp_snapshot_keywords" maxlength=300 placeholder="(i.e., Machine Learning and Navy)" required></td>
             </td></tr>

         <tr><td colspan=2><hr></td></tr>

         </cfoutput>

          <tr><td class="feed_sub_header" valign=top width=175>Department</td>
              <td class="feed_option">

              <select name="comp_snapshot_dept" class="input_select" style="width: 410px;">

              <option value=0>Select Department

              <cfoutput query="dept">
               <option value="#department_code#" <cfif #department_code# is #snap.comp_snapshot_dept#>selected</cfif>>#department_name#
              </cfoutput>

              </select>

              </td></tr>

          <tr><td class="feed_sub_header" valign=top width=175>Agency</td>
              <td class="feed_option">

              <select name="comp_snapshot_agency" class="input_select" style="width: 410px;">

              <option value=0>Select Agency

              <cfoutput query="agency">
               <option value="#agency_code#" <cfif #agency_code# is #snap.comp_snapshot_agency#>selected</cfif>>#agency_name#
              </cfoutput>

              </select>

              </td></tr>

         <cfoutput>

         <tr><td class="feed_sub_header" width=150><b>Contract Number</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 300px;" value="#snap.comp_snapshot_contract_id#" name="comp_snapshot_contract_id" placeholder=""></td>
             </td></tr>

          <tr><td class="feed_sub_header" width=150>NAICS Code(s)</td>
              <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 300px;" maxlength="100" value="#snap.comp_snapshot_naics#" name="comp_snapshot_naics" placeholder="Buying NAICS Code(s)"></td>
              </tr>

          <tr><td class="feed_sub_header" width=150>PSC Code(s)</td>
              <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 300px;" maxlength="100" value="#snap.comp_snapshot_psc#" name="comp_snapshot_psc" placeholder="Buying PSC Code(s)"></td>
              </tr>

          <tr><td class="feed_sub_header" valign=top width=175>Place of Performance</td>

              <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 200px;" value="#snap.comp_snapshot_city#" name="comp_snapshot_city" placeholder="City Name">

          </cfoutput>

              <select name="comp_snapshot_state" class="input_select" style="width: 200px;">

              <option value=0>Select State

              <cfoutput query="state">
               <option value=#state_abbr# <cfif #state_abbr# is #snap.comp_snapshot_state#>selected</cfif>>#state_name#
              </cfoutput>

              </select>

              </td></tr>


	      <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td></td><td>

                    <input class="button_blue_large" type="submit" name="button" value="Save Snapshot" onclick="javascript:document.getElementById('page-loader').style.display='block';">&nbsp;&nbsp;&nbsp;
		            <input class="button_blue_large" type="submit" name="button" value="Delete Snapshot" onclick="return confirm('Delete Snapshot?\r\nAre you sure you want to delete this Snapshot?');">&nbsp;

          </td></tr>

        <cfoutput>
         <input type="hidden" name="id" value=#id#>
         <input type="hidden" name="i" value="#i#">
        </cfoutput>

       </table>


       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>