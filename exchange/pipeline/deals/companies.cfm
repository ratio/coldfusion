<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">

<cfif not isdefined("sv")>
 <cfset sv = 40>
</cfif>

<cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from deal
  left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
  where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="snap" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comp_snapshot
 where comp_snapshot_id = #id#
</cfquery>

<body class="body">

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

   select recipient_duns, cast(company_about as varchar(max)) as about_short, cast(company_long_desc as varchar(max)) as about_long, company_name, company_id, company_website, company_logo,
		  company_keywords, company_city, company_state,
		  (select count(id) as awards from award_data where recipient_duns = company_duns


   <cfif snap.comp_snapshot_dept is not 0>
	and awarding_agency_code = '#snap.comp_snapshot_dept#'
   </cfif>

   <cfif snap.comp_snapshot_agency is not 0>
	and awarding_sub_agency_code = '#snap.comp_snapshot_agency#'
   </cfif>

   <cfif snap.comp_snapshot_city is not "">
	and primary_place_of_performance_city_name = '#snap.comp_snapshot_city#'
   </cfif>

   <cfif snap.comp_snapshot_state is not 0>
	and primary_place_of_performance_state_code = '#snap.comp_snapshot_state#'
   </cfif>

   and contains ((award_description, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, funding_office_name),'#trim(snap.comp_snapshot_keywords)#')














		  ) as awards,
		  cast(company_long_desc as varchar(max)) as company_long_desc from company
   join award_data on recipient_duns = company_duns
   where id > 0

   <cfif snap.comp_snapshot_dept is not 0>
	and awarding_agency_code = '#snap.comp_snapshot_dept#'
   </cfif>

   <cfif snap.comp_snapshot_agency is not 0>
	and awarding_sub_agency_code = '#snap.comp_snapshot_agency#'
   </cfif>

   <cfif snap.comp_snapshot_city is not "">
	and primary_place_of_performance_city_name = '#snap.comp_snapshot_city#'
   </cfif>

   <cfif snap.comp_snapshot_state is not 0>
	and primary_place_of_performance_state_code = '#snap.comp_snapshot_state#'
   </cfif>

   and contains ((award_description, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, funding_office_name),'#trim(snap.comp_snapshot_keywords)#')
   group by recipient_duns, cast(company_about as varchar(max)), cast(company_long_desc as varchar(max)), company_name, company_id, company_duns, company_website, cast(company_long_desc as varchar(max)), company_logo, company_keywords, company_city, company_state

 <cfif sv is 1>
  order by company_name ASC
 <cfelseif sv is 10>
  order by company_name DESC
 <cfelseif sv is 2>
  order by company_city ASC
 <cfelseif sv is 20>
  order by company_city DESC
 <cfelseif sv is 3>
  order by company_state ASC
 <cfelseif sv is 30>
  order by company_state DESC
 <cfelseif sv is 4>
  order by awards asc, company_name ASC
 <cfelseif sv is 40>
  order by awards DESC, company_name ASC
 <cfelseif sv is 5>
  order by deal_comp_competitor_id DESC, company_name ASC
 <cfelseif sv is 50>
  order by deal_comp_competitor_id ASC, awards DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 80>
  order by fbo_naics DESC
 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC
 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC
 </cfif> --->

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">#ucase(snap.comp_snapshot_name)# - #numberformat(agencies.recordcount,'99,999')#</td>
				 <td class="feed_sub_header" align=right>

				 <a href="comp_edit.cfm?i=#i#&id=#id#"><img src="/images/icon_edit.png" hspace=10 width=20 alt="Edit Snapshot" title="Edit Snapshot"></a>
				 <a href="comp_edit.cfm?i=#i#&id=#id#">Edit Snapshot</a>
				 &nbsp;&nbsp;|&nbsp;&nbsp;

				 <a href="companies.cfm?i=#i#&id=#id#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="companies.cfm?i=#i#&id=#id#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
				 &nbsp;&nbsp;|&nbsp;&nbsp;

					 <a href="competition.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Return</a>

				 </td></tr>
				</cfoutput>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

         </table>

        <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td colspan=2 class="feed_sub_header">Search String -

				"<i>#replaceNoCase(snap.comp_snapshot_keywords,'"','',"all")#</i>"

				</td></tr>

				<tr>

					<cfif agencies.recordcount GT #perpage#>

				    <td class="feed_sub_header">

				    Result Set -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&i=#i#&id=#id#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&i=#i#&id=#id#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>

						</cfif>

				      </cfif>

				</td>

				<td align=right class="feed_sub_header">

                </td>

				</tr>

				    <cfif isdefined("u")>
				    <tr><td class="feed_sub_header">

				     <cfif u is 1>
				      <span style="color: green;">Competitor has been successfully added to the deal.</span>
				     <cfelseif u is 2>
				      <span style="color: green;">Snapshot successfully updated.</span>
				     </cfif>
				    </td></tr>
				    </cfif>

			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td colspan=2 class="feed_sub_header"><a href="companies.cfm?i=#i#&id=#id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>COMPANY</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" style="padding-left: 10px; padding-right: 10px;"><b>DESCRIPTION</b></td>
            <td class="feed_sub_header"><b>WEBSITE</b></td>
            <td class="feed_sub_header"><a href="companies.cfm?i=#i#&id=#id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CITY</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=center><a href="companies.cfm?i=#i#&id=#id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>STATE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=center><a href="companies.cfm?i=#i#&id=#id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>CONTRACTS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

             <td width=60>

                    <a href="/exchange/include/profile.cfm?id=#company_id#&l=11&s=t" target="_blank" rel="noopener">
                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=40 border=0>
					</cfif>
					</a>

			 </td>

             <td class="feed_sub_header" width=300><b><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank"><cfif len(company_name) GT 200>#ucase(left(company_name,200))#...<cfelse>#ucase(company_name)#</cfif></b></td>
             <cfif about_short is not "">
	             <td class="feed_option" width=600 style="padding-left: 10px; padding-right: 10px;">#ucase(about_short)#</td>
             <cfelse>
	             <td class="feed_option" width=600 style="padding-left: 10px; padding-right: 10px;">#ucase(about_long)#</td>
             </cfif>
             <td class="feed_option"><a href="#company_website#" target="_blank" rel="noopener"><u><cfif len(company_website) GT 30>#left(company_website,30)#...<cfelse>#ucase(company_website)#</cfif></u></a></td>
             <td class="feed_option" width=100>#ucase(company_city)#</td>
             <td class="feed_option" align=center>#ucase(company_state)#</td>
             <td class="feed_option" align=center style="padding-left: 5px; padding-right: 5px; font-size: 18px;"><b>#awards#</b></td>
             <td align=right>

             <a href="save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><img src="/images/plus3.png" width=15 hspace=5 border=0 alt="Add to Opportunity" title="Add to Opportunity"></a>

             </td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>