<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(comp_snapshot_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfif #button# is "Save Snapshot">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update comp_snapshot
      set
      comp_snapshot_name = '#comp_snapshot_name#',
      comp_snapshot_desc = '#comp_snapshot_desc#',
      comp_snapshot_contract_id = '#comp_snapshot_contract_id#',
      comp_snapshot_keywords = '#search_string#',
      comp_snapshot_dept = '#comp_snapshot_dept#',
      comp_snapshot_agency = '#comp_snapshot_agency#',
      comp_snapshot_city = '#comp_snapshot_city#',
      comp_snapshot_state = '#comp_snapshot_state#',
      comp_snapshot_naics = '#comp_snapshot_naics#',
      comp_snapshot_psc = '#comp_snapshot_psc#',
      comp_snapshot_updated = #now()#
      where comp_snapshot_id = #id#
	</cfquery>

	<cflocation URL="competition.cfm?i=#i#&u=2" addtoken="no">

<cfelseif #button# is "Create Snapshot">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into comp_snapshot
	 (
      comp_snapshot_name,
      comp_snapshot_contract_id,
      comp_snapshot_desc,
      comp_snapshot_keywords,
      comp_snapshot_dept,
      comp_snapshot_agency,
      comp_snapshot_city,
      comp_snapshot_state,
      comp_snapshot_naics,
      comp_snapshot_psc,
      comp_snapshot_deal_id,
      comp_snapshot_created,
      comp_snapshot_updated

      )
      values
      (
      '#comp_snapshot_name#',
      '#comp_snapshot_contract_id#',
      '#comp_snapshot_desc#',
      '#search_string#',
      '#comp_snapshot_dept#',
      '#comp_snapshot_agency#',
      '#comp_snapshot_city#',
      '#comp_snapshot_state#',
      '#comp_snapshot_naics#',
      '#comp_snapshot_psc#',
       #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
       #now()#,
       #now()#
       )
	</cfquery>

	<cflocation URL="competition.cfm?i=#i#&u=4" addtoken="no">

<cfelseif #button# is "Delete Snapshot">

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete comp_snapshot
		 where comp_snapshot_id = #id#
		</cfquery>

	<cflocation URL="competition.cfm?i=#i#&id=#id#&u=5" addtoken="no">

</cfif>
