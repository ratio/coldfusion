<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
.comp_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 260px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 3px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

     <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from deal
       left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
       where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
      </cfquery>

     <cfquery name="snap" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      select * from comp_snapshot
      where comp_snapshot_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
     </cfquery>


      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="deal_open.cfm?i=#i#">Opportunity Summary</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="market.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Partners</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="competition.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Competition</a></span>
          </div>

       </cfoutput>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="/exchange/pipeline/refresh_pipelines.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>#ucase(deal.deal_name)#</td>
            <td align=right class="feed_sub_header">

			 <cfif session.pipeline_access_level GTE 2>

			 <a href="comp_add.cfm?i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a>
			 <a href="comp_add.cfm?i=#i#">Add Snapshot</a>&nbsp;|&nbsp;

             </cfif>

			 <a href="deal_open.cfm?i=#i#">Return</a>


            </td></tr>
        </cfoutput>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>

       <cfif isdefined("u")>
        <cfif u is 2>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Snapshot has been successfully updated.</td></tr>
        <cfelseif u is 4>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Snapshot has been successfully created.</td></tr>
        <cfelseif u is 5>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Snapshot has been successfully deleted.</td></tr>
        </cfif>
       </cfif>

       </table>

       <cfoutput>
		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td height=10></td></tr>
			<tr><td class="feed_header">Competition Snapshots</td></tr>
			<tr><td height=20></td></tr>
		   </table>
       </cfoutput>


         <cfif snap.recordcount is 0>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <cfoutput>
				<tr><td class="feed_sub_header" style="font-weight: normal;">No Competition Snapshots exists.  <cfif session.pipeline_access_level GTE 2>Add one by <a href="comp_add.cfm?i=#i#"><b>clicking here</b></a>.</cfif></td></tr>
			  </cfoutput>
			  </table>
         <cfelse>

         <cfloop query="snap">

			  <cfquery name="competitors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(recipient_duns)) as total from award_data
               where (contains((award_description, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, funding_office_name),'#trim(snap.comp_snapshot_keywords)#'))

			   <cfif snap.comp_snapshot_dept is not 0>
			    and awarding_agency_code = '#snap.comp_snapshot_dept#'
			   </cfif>

			   <cfif snap.comp_snapshot_agency is not 0>
			    and awarding_sub_agency_code = '#snap.comp_snapshot_agency#'
			   </cfif>

			   <cfif snap.comp_snapshot_city is not "">
			    and primary_place_of_performance_city_name = '#snap.comp_snapshot_city#'
			   </cfif>

			   <cfif snap.comp_snapshot_state is not 0>
			    and primary_place_of_performance_state_code = '#snap.comp_snapshot_state#'
			   </cfif>

			  </cfquery>

			 <cfoutput>

             <div class="comp_badge">

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" align=center><a href="companies.cfm?i=#i#&id=#snap.comp_snapshot_id#" onclick="javascript:document.getElementById('page-loader').style.display='block';">#snap.comp_snapshot_name#</a></td></tr>
					<tr><td class="feed_sub_header" align=center valign=top style="font-weight: normal;"><i>#replaceNoCase(snap.comp_snapshot_keywords,'"','',"all")#</i></td></tr>
				 </table>

				 <center>

				 <table cellspacing=0 cellpadding=0 border=0 width=50%>
                    <tr><td height=10></td></tr>
                    <tr><td class="feed_header" style="font-size: 50px; background-color: 435177; color: ffffff;" align=center height=100><a href="companies.cfm?i=#i#&id=#snap.comp_snapshot_id#" onclick="javascript:document.getElementById('page-loader').style.display='block';" class="feed_header" style="font-size: 50px; background-color: 435177; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">#numberformat(competitors.total,'99,999')#</a></td></tr>
					<tr><td class="feed_sub_header" align=center>Contractors</td></tr>

                 </table>

                 </center>

		     </div>

			 </cfoutput>

         </cfloop>

         </cfif>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>