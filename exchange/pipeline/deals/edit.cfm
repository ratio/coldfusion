<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="stage" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal_stage
 where deal_stage_hub_id = #session.hub#
 order by deal_stage_order
</cfquery>

<cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal
 where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal_type
 where deal_type_hub_id = #session.hub#
 order by deal_type_order
</cfquery>

<cfquery name="priority" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal_priority
 where deal_priority_hub_id = #session.hub#
 order by deal_priority_order
</cfquery>

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 order by department_name
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 order by agency_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

		   <cfinclude template="/exchange/components/my_profile/profile.cfm">
		   <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <div class="main_box">

       <form action="save.cfm" method="post" enctype="multipart/form-data" >

       <cfoutput>
       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Opportunity</td>
            <td align=right class="feed_sub_header"><a href="deal_open.cfm?i=#i#">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>
       </cfoutput>

       <cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=150><b>Opportunity Name</b></td>
             <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_name#" style="width: 800px;" name="deal_name" placeholder="Please give this Opportunity a name." required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="deal_desc" class="input_textarea" style="width: 800px; height: 200px;" placeholder="Please provide a high level description of this Opportunity.">#deal.deal_desc#</textarea></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Customer Name</b></td>
             <td class="feed_option"><input type="text" class="input_text" style="width: 534px;" value="#deal.deal_customer_name#" name="deal_customer_name" placeholder="Please provide the Customer's name" required></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Capabilities Needed</b></td>
             <td class="feed_option"><input type="text" class="input_text" style="width: 534px;" value="#deal.deal_keywords#" name="deal_keywords" maxlength=300 placeholder="(i.e., Machine Learning, Clinical, etc.)"></td>
             </td></tr>

         <tr><td></td>
             <td class="link_small_gray">For multiple capabilites, seperate capabilites with a comma (i.e., machine learning, clinical, etc.)</td></tr>

         <tr><td class="feed_sub_header" width=200><b>Estimated Release Date</b></td>
             <td class="feed_option"><input type="date" class="input_date" style="width: 175px;" name="deal_release_date" value="#deal.deal_release_date#"></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Estimated Close Date</b></td>
             <td class="feed_option"><input type="date" class="input_date" style="width: 175px;" value="#deal.deal_close_date#" name="deal_close_date"></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Potential Value</b></td>
             <td class="feed_option"><input type="number" class="input_text" style="width: 175px;" value="#deal.deal_value_total#" name="deal_value_total"></td>
             </td></tr>

         <tr><td colspan=2><hr></td></tr>

        </cfoutput>

          <tr><td class="feed_sub_header" valign=top width=175>Department</td>
              <td class="feed_option">

              <select name="deal_dept_code" class="input_select" style="width: 410px;">

              <option value=0>Select Department

              <cfoutput query="dept">
               <option value='#department_code#' <cfif #deal.deal_dept_code# is #department_code#>selected</cfif>>#department_name#
              </cfoutput>

              </select>

              </td></tr>

          <tr><td class="feed_sub_header" valign=top width=175>Agency</td>
              <td class="feed_option">

              <select name="deal_agency_code" class="input_select" style="width: 410px;">

              <option value=0>Select Agency

              <cfoutput query="agency">
               <option value='#agency_code#' <cfif #deal.deal_agency_code# is #agency_code#>selected</cfif>>#agency_name#
              </cfoutput>

              </select>

              </td></tr>

         <cfoutput>

         <tr><td class="feed_sub_header" width=150><b>Existing Contract ##</b></td>
             <td class="feed_option"><input type="text" class="input_text" style="width: 300px;" value="#deal.deal_contract_number#" name="deal_contract_number" placeholder="If yes, enter Contract Number"></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Current Solicitation ##</b></td>
             <td class="feed_option"><input type="text" class="input_text" style="width: 300px;" value="#deal.deal_current_sol#" name="deal_current_sol"></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Past Solicitation ##</b></td>
             <td class="feed_option"><input type="text" class="input_text" style="width: 300px;" value="#deal.deal_past_sol#" name="deal_past_sol"></td>
             </td></tr>

          <tr><td class="feed_sub_header" width=150>NAICS Code(s)</td>
              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_naics#" style="width: 300px;" maxlength="100" name="deal_naics" placeholder="Buying NAICS Code(s)">

              &nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup" style="cursor: pointer;" onclick="window.open('/exchange/opps/naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">

              </td>
              </tr>

          <tr><td class="feed_sub_header" width=150>PSC Code(s)</td>
              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_psc#" style="width: 300px;" maxlength="100" name="deal_psc" placeholder="Buying PSC Code(s)">
&nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="PSC Lookup" title="PSC Lookup" style="cursor: pointer;" onclick="window.open('/exchange/opps/psc_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">

              </td>
              </tr>

          <tr><td class="feed_sub_header" valign=top width=175>Place of Performance</td>

              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_pop_city#" style="width: 200px;" name="deal_pop_city" placeholder="City Name">

          </cfoutput>

              <select name="deal_pop_state" class="input_select" style="width: 200px;">

              <option value=0>Select State

              <cfoutput query="state">
               <option value=#state_abbr# <cfif #deal.deal_pop_state# is #state_abbr#>selected</cfif>>#state_name#
              </cfoutput>

              </select>

              </td></tr>

         <cfoutput>

         <tr><td class="feed_sub_header" width=150><b>Reference URL</b></td>
             <td class="feed_option"><input type="url" class="input_text" style="width: 800px;" value="#deal.deal_url#" name="deal_url"></td>
             </td></tr>
         </cfoutput>

        <tr><td colspan=2><hr></td></tr>

        <tr><td class="feed_sub_header" width=150><b>Type</b></td>
             <td class="feed_option">

             <select name="deal_type_id" class="input_select" style="width: 200px;">
             <option value=0>Select Type
             <cfoutput query="type">
              <option value=#deal_type_id# <cfif #deal.deal_type_id# is #deal_type_id#>selected</cfif>>#deal_type_name#
             </cfoutput>

             </select>


             </td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Priority</b></td>
             <td class="feed_option">

             <select name="deal_priority_id" class="input_select" style="width: 200px;">
             <option value=0>Select Priority
             <cfoutput query="priority">
              <option value=#deal_priority_id# <cfif #deal.deal_priority_id# is #deal_priority_id#>selected</cfif>>#deal_priority_name#
             </cfoutput>

             </select>

             </td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Stage</b></td>
             <td class="feed_option">

             <select name="deal_stage_id" class="input_select" style="width: 200px;">
             <option value=0>Select Stage
             <cfoutput query="stage">
              <option value=#deal_stage_id# <cfif #deal.deal_stage_id# is #deal_stage_id#>selected</cfif>>#deal_stage_name#
             </cfoutput>

             </select>


             </td>
             </td></tr>

         <cfoutput>

		 <tr><td class="feed_sub_header" valign=top>Image</td>
			 <td class="feed_sub_header" style="font-weight: normal;">

			 <cfif #deal.deal_image# is "">
			  <input type="file" name="deal_image">
			 <cfelse>
			  <img src="#media_virtual#/#deal.deal_image#" width=150><br><br>
			  <input type="file" name="deal_image"><br><br>
			  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
			 </cfif>

			 </td></tr>

	      <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td></td><td>

          <input class="button_blue_large" type="submit" name="button" value="Save">&nbsp;&nbsp;&nbsp;
          <input class="button_blue_large" type="submit" name="button" value="Delete" onclick="return confirm('Delete Model?\r\nAre you sure you want to delete this Model?');">&nbsp;

          </td></tr>

       <input type="hidden" name="i" value="#i#">

        </cfoutput>

       </table>


       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>