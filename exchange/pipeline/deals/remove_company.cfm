<cfinclude template="/exchange/security/check.cfm">

<cfif #type# is "p">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_comp
	 where deal_comp_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		   deal_comp_partner_id = #id#
	</cfquery>

	<cflocation URL="deal_open.cfm?i=#i#&u=5" addtoken="no">

<cfelseif #type# is "c">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_comp
	 where deal_comp_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		   deal_comp_competitor_id = #id#
	</cfquery>

	<cflocation URL="deal_open.cfm?i=#i#&u=6" addtoken="no">

</cfif>