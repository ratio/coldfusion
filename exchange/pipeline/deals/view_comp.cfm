<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<body class="body">

<cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_comp_company_id from hub_comp
 where hub_comp_hub_id = #session.hub#
</cfquery>

<cfquery name="get_keyword" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner_snapshot
 where partner_snapshot_id = #i#
</cfquery>

<cfset ky = #get_keyword.partner_snapshot_keyword#>
<cfset session.search_filter = #ky#>

<cfif v is 1>

	<cfif list.recordcount is 0>
	 <cfset comp_list = 0>
	<cfelse>
	 <cfset comp_list = valuelist(list.hub_comp_company_id)>
	</cfif>

</cfif>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select company_name, company_about, company_long_desc, company_keywords, company_website, company_city, company_state, company_id, company_logo from company

  <cfif v is 1>

  where company_id in (#comp_list#)
  and contains((company_about, company_keywords, company_long_desc),'#trim(session.search_filter)#')

  <cfelseif v is 2>

  where contains((company_about, company_keywords, company_long_desc),'#trim(session.search_filter)#')

  </cfif>

 <cfif sv is 1>
  order by company_name ASC
 <cfelseif sv is 10>
  order by company_name DESC
 <cfelseif sv is 2>
  order by company_city ASC
 <cfelseif sv is 20>
  order by company_city DESC
 <cfelseif sv is 3>
  order by company_state ASC
 <cfelseif sv is 30>
  order by company_state DESC
 <cfelseif sv is 4>
  order by fbo_location ASC
 <cfelseif sv is 40>
  order by fbo_location DESC
 <cfelseif sv is 5>
  order by fbo_opp_name ASC
 <cfelseif sv is 50>
  order by fbo_opp_name DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 80>
  order by fbo_naics DESC
 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC
 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC
 </cfif>

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">

			 <cfif v is 1>
			   In Network Companies
			 <cfelseif v is 2>
			   Out of Network Companies
			 <cfelseif v is 3>
			   Subcontractors
			 </cfif> - "<i>#replaceNoCase(session.search_filter,'"','',"all")#</i>"</td>
				 <td class="feed_sub_header" align=right>
				 <a href="view_comp.cfm?v=#v#&i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 hspace=10 alt="Export to Excel" title="Export to Excel"></a>
                 <a href="view_comp.cfm?v=#v#&i=#i#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
				 &nbsp;&nbsp;|&nbsp;&nbsp;

					 <a href="market.cfm?i=#session.pid#">Return</a>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

				    <td class="feed_sub_header"></td>

				    <td class="feed_sub_header" align=right>

					<cfif agencies.recordcount GT #perpage#>

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&v=#v#&i=#i#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&v=#v#&i=#i#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				    </cfif>

				</td>

				</tr>

				<tr><td height=20></td></tr>
			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td></td>
            <td class="feed_sub_header"><a href="view_comp.cfm?v=#v#&i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" style="padding-left: 10px; padding-right: 10px;"><b>Description</b></td>
            <td class="feed_sub_header" style="padding-left: 10px; padding-right: 10px;"><b>Keywords</b></td>
            <td class="feed_sub_header"><b>Website</b></td>
            <td class="feed_sub_header"><a href="view_comp.cfm?v=#v#&i=#i#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=center><a href="view_comp.cfm?v=#v#&i=#i#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

             <td width=70>
                    <a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener">
                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=40 border=0>
					</cfif>
					</a>
			 </td>

             <td class="feed_sub_header" width=250><b><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener"><cfif len(company_name) GT 200>#ucase(left(company_name,200))#...<cfelse>#ucase(company_name)#</cfif></b></td>
             <td class="feed_option" style="padding-left: 10px; padding-right: 10px;" width=400>#ucase(company_about)#</td>
             <td class="feed_option" style="padding-left: 10px; padding-right: 10px;">#ucase(company_keywords)#</td>
             <td class="feed_option"><a href="#company_website#" target="_blank" rel="noopener"><u>#ucase(company_website)#</u></a></td>
             <td class="feed_option" width=100>#ucase(company_city)#</td>
             <td class="feed_option" width=50 align=center>#ucase(company_state)#</td>
             <td align=right>

             <a href="save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><img src="/images/plus3.png" width=15 hspace=5 border=0 alt="Add to Opportunity" title="Add to Opportunity"></a>

             </td>


         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>