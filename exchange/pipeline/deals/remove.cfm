<cfinclude template="/exchange/security/check.cfm">

<cfif listlen(session.add_filter) GT 1>
 <cfset filter_position = listfind(session.add_filter,'#i#')>
 <cfset session.add_filter = listdeleteat(session.add_filter,'#filter_position#')>
<cfelse>
 <cfset StructDelete(Session,"add_filter")>
</cfif>

 <cfif location is "view_sub">
  <cflocation URL="view_sub.cfm?i=#i#&ky=#ky#&sv=#sv#" addtoken="no">
 <cfelseif location is "companies">
  <cflocation URL="companies.cfm?i=#i#&sv=#sv#&id=#id#" addtoken="no">
 </cfif>

