<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
.comp_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 310px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 3px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfset session.pid = #i#>

     <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from deal
       left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
       where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
      </cfquery>

     <cfquery name="snap" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      select * from partner_snapshot
      where partner_snapshot_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
     </cfquery>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="deal_open.cfm?i=#i#">Opportunity Summary</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="market.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Partners</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="competition.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Competition</a></span>
          </div>

       </cfoutput>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="/exchange/pipeline/refresh_pipelines.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>#ucase(deal.deal_name)#</td>
            <td align=right class="feed_sub_header">

            <cfif session.pipeline_access_level GTE 2>
			 <a href="partner_add.cfm?i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a>
			 <a href="partner_add.cfm?i=#i#">Add Snapshot</a>&nbsp;|&nbsp;
            </cfif>

			 <a href="deal_open.cfm?i=#i#">Return</a>

            </td></tr>
        </cfoutput>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>

       <cfif isdefined("u")>
        <cfif u is 2>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Snapshot has been successfully updated.</td></tr>
        <cfelseif u is 4>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Snapshot has been successfully created.</td></tr>
        <cfelseif u is 5>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Snapshot has been successfully deleted.</td></tr>
        </cfif>
       </cfif>

       </table>

       <cfoutput>
		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td height=10></td></tr>
			<tr><td class="feed_header">PARTNER SNAPSHOTS</td></tr>
			<tr><td class="feed_sub_header" style="font-weight: normal;">

            Partner Snapshots are summary counts of Companies that match keywords that are related to this Model.   They are meant to provide you with a starting point for understanding �The Market�.  To analyze these Snapshots click on a specific area / number and start fine tuning your search.  Or, add a new Snapshot above.
			</td></tr>

			<tr><td height=10></td></tr>

		   </table>
       </cfoutput>

         <cfif snap.recordcount is 0>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <cfoutput>
				<tr><td class="feed_sub_header" style="font-weight: normal;">No Competition Snapshots exists.  Add one by <a href="partner_add.cfm?i=#i#"><b>clicking here</b></a>.</td></tr>
			  </cfoutput>
			  </table>
         <cfelse>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <tr>
 	  <td></td>
 	  <td class="feed_sub_header" align=center>In Network</td>
 	  <td class="feed_sub_header" align=center>Out of Network</td>
 	  <td class="feed_sub_header" align=center>Contractors</td>
 	  <td class="feed_sub_header" align=center>Subcontractors</td>
 	  <td class="feed_sub_header" align=center>Won Grants</td>
 	  <td class="feed_sub_header" align=center>Won SBIR/STTRs</td>
 	  <td class="feed_sub_header" align=center>Awarded Patents</td>
 	  <td class="feed_sub_header" align=center>Lab Tech</td>
     </tr>

     <tr>
 	  <td></td>
 	  <td class="feed_option" align=center>Companies</td>
 	  <td class="feed_option" align=center>The Entire Market</td>
 	  <td class="feed_option" align=center>Prime Contractors</td>
 	  <td class="feed_option" align=center>Supporting Primes</td>
 	  <td class="feed_option" align=center>Leverage Research</td>
 	  <td class="feed_option" align=center>New Technologies</td>
 	  <td class="feed_option" align=center>Ideas and Inventions</td>
 	  <td class="feed_option" align=center>Government Tech</td>
     </tr>

     <tr><td colspan=9><hr></td></tr>

      <cfset count = 1>

      <cfloop query="snap">

      <cfset k = #snap.partner_snapshot_keyword#>

	  <cfquery name="hcomp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select hub_comp_company_id from hub_comp
	   where hub_comp_hub_id = #session.hub#
	  </cfquery>

	  <cfif hcomp.recordcount is 0>
	   <cfset in_list = 0>
	  <cfelse>
	   <cfset in_list = valuelist(hcomp.hub_comp_company_id)>
	  </cfif>

	  <cfquery name="in_network" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(company_id) as total from company
	   where company_id in (#in_list#) and
       contains((company_long_desc, company_keywords, company_about),'#trim(k)#')
	  </cfquery>

 	  <cfquery name="comp_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 	   select count(company_id) as total from company
 	   where contains((*),'#trim(k)#')
 	  </cfquery>

 	  <cfquery name="lab_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 	   select count(id) as total from lab_tech
 	   where contains((*),'#trim(k)#')
 	  </cfquery>

 	  <cfquery name="awards_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 	   select count(distinct(recipient_duns)) as total from award_data
 	   where contains((award_description),'#trim(k)#')
       </cfquery>

 	  <cfquery name="sub_awards_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 	   select count(distinct(subawardee_duns)) as total from award_data_sub
 	   where contains((subaward_description),'#trim(k)#')
       </cfquery>

 	  <cfquery name="grants_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 	    select count(distinct(recipient_duns)) as total from grants
         where contains((cfda_title, awarding_agency_name, awarding_sub_agency_name, award_description),'#trim(k)#')
 	  </cfquery>

 	  <cfquery name="sbir_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 		  select count(distinct(duns)) as total from sbir
          where contains((award_title, department, research_keywords, abstract),'#trim(k)#')
 	  </cfquery>

 	  <cfquery name="patent_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
          select count(distinct(organization)) as total from patent
 		 join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
 		 where contains((patent.patent_id, title, abstract),'#k#')
 	  </cfquery>

 	<cfoutput>

 	 <tr>
 	    <td class="feed_sub_header" width=15%><a href="partner_edit.cfm?i=#i#&id=#snap.partner_snapshot_id#">#ucase(replaceNoCase(k,'"','',"all"))#</a></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif in_network.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_comp.cfm?v=1&id=#i#&i=#snap.partner_snapshot_id#" style="font-size: 30px; color: ffffff;">#numberformat(in_network.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif comp_total.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_comp.cfm?v=2&id=#i#&i=#snap.partner_snapshot_id#" style="font-size: 30px; color: ffffff;">#numberformat(comp_total.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif awards_total.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_awards.cfm?i=#snap.partner_snapshot_id#&v=1" style="font-size: 30px; color: ffffff;">#numberformat(awards_total.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif sub_awards_total.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_awards.cfm?i=#snap.partner_snapshot_id#&v=2" style="font-size: 30px; color: ffffff;">#numberformat(sub_awards_total.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif grants_total.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_awards.cfm?i=#snap.partner_snapshot_id#&v=3" style="font-size: 30px; color: ffffff;">#numberformat(grants_total.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif sbir_total.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_awards.cfm?i=#snap.partner_snapshot_id#&v=4" style="font-size: 30px; color: ffffff;">#numberformat(sbir_total.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif patent_total.total is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_awards.cfm?i=#snap.partner_snapshot_id#&v=5" style="font-size: 30px; color: ffffff;">#numberformat(patent_total.total,'99,999')#</a></cfif></td>
 		<td class="big_number" align=center style="background-color: 435177;"><cfif lab_total.recordcount is 0><span style="font-size: 30px; color: ffffff;">0</span><cfelse><a href="view_labs.cfm?i=#snap.partner_snapshot_id#" style="font-size: 30px; color: ffffff;">#numberformat(lab_total.total,'99,999')#</a></cfif></td>
     </tr>

     <cfif count LT snap.recordcount>
       <tr><td colspan=9><hr></td></tr>
     </cfif>

     <cfset count = count + 1>

 	</cfoutput>

    </cfloop>

    </table>

    </cfif>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>