<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

      <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from deal
       where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
      </cfquery>

      <cfquery name="state" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from state
       order by state_name
      </cfquery>

      <cfquery name="dept" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select distinct(awarding_agency_code), awarding_agency_name from award_data
       order by awarding_agency_name
      </cfquery>

      <cfquery name="agency" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select distinct(awarding_sub_agency_code), awarding_sub_agency_name from award_data
       order by awarding_sub_agency_name
      </cfquery>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       <div class="left_box">

       </div>

       </td><td valign=top>

       <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="deal_open.cfm?i=#i#">DEAL DASHBOARD</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="customer.cfm?i=#i#">CUSTOMER PROFILE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="market.cfm?i=#i#">MARKETPLACE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="competition.cfm?i=#i#">COMPETITION</a></span>
          </div>

       </cfoutput>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>#ucase(deal.deal_name)#</td>
            <td align=right class="feed_sub_header">
            <img src="/images/icon_edit.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="customer.cfm?i=#i#">Return</a>

            </td></tr>
        </cfoutput>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>

        <form action="customer_save.cfm" method="post">

        <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header" width=150>Customer Name</td>
              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_customer_name#" style="width: 534px;" name="deal_customer_name" placeholder="Customer Name" required></td>
              </tr>

          <tr><td class="feed_sub_header" valign=top width=175>Place of Performance</td>

              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_pop_city#" style="width: 200px;" name="deal_pop_city" placeholder="City Name">

        </cfoutput>

              <select name="deal_type_id" class="input_select" style="width: 200px;">

              <option value=0>Select State

              <cfoutput query="state">
               <option value=#state_abbr# <cfif #deal.deal_pop_state# is #state_abbr#>selected</cfif>>#state_name#
              </cfoutput>

              </select>

              </td></tr>

          <tr><td class="feed_sub_header" valign=top width=175>Department</td>
              <td class="feed_option">

              <select name="deal_dept_code" class="input_select" style="width: 410px;">

              <option value=0>Select Department

              <cfoutput query="dept">
               <option value='#awarding_agency_code#' <cfif #deal.deal_dept_code# is #awarding_agency_code#>selected</cfif>>#awarding_agency_name#
              </cfoutput>

              </select>

              </td></tr>

          <tr><td class="feed_sub_header" valign=top width=175>Agency</td>
              <td class="feed_option">

              <select name="deal_agency_code" class="input_select" style="width: 410px;">

              <option value=0>Select Agency

              <cfoutput query="agency">
               <option value='#awarding_sub_agency_code#' <cfif #deal.deal_agency_code# is #awarding_sub_agency_code#>selected</cfif>>#awarding_sub_agency_name#
              </cfoutput>

              </select>

              </td></tr>

          <cfoutput>

          <tr><td class="feed_sub_header" width=150>NAICS Code(s)</td>
              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_naics#" style="width: 300px;" maxlength="100" name="deal_naics" placeholder="Buying NAICS Code(s)" required></td>
              </tr>

          <tr><td class="feed_sub_header" width=150>PSC Code(s)</td>
              <td class="feed_option"><input type="text" class="input_text" value="#deal.deal_psc#" style="width: 300px;" maxlength="100" name="deal_psc" placeholder="Buying PSC Code(s)" required></td>
              </tr>

           </cfoutput>

           <tr><td height=15></td></tr>
           <tr><td></td><td>

           <cfoutput>
             <input class="button_blue_large" type="submit" name="button" value="Save Profile"></td></tr>
             <input type="hidden" name="i" value="#i#">
           </cfoutput>

        </table>


        </form>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>