<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal_comment
 where deal_comment_id = #deal_comment_id# and
       deal_comment_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
       deal_comment_hub_id = #session.hub#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">Edit Comment</td>
                 <td class="feed_sub_header" align=right>

			 <cfif isdefined("l")>
			  <a href="/exchange/marketplace/communities/opp_open.cfm?i=#i#">Return</a>
			 <cfelse>
			  <a href="deal_open.cfm?i=#i#">Return</a>
			 </cfif>

                 </td></tr>
		     <tr><td colspan=2><hr></td></tr>
		  </table>

      </cfoutput>

      <cfoutput>

        <form action="deal_comment_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header" valign=top width=100>Comments</td>
               <td class="feed_option"><textarea class="input_textarea" name="deal_comment_text" style="width: 900px; height: 200px;">#edit.deal_comment_text#</textarea></td></tr>
           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>

           <tr><td></td><td><input type="submit" name="button" class="button_blue_large" value="Update">
           &nbsp;&nbsp;
           <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">
           </td></tr>

             <cfif isdefined("l")>
              <input type="hidden" name="l" value=#l#>
             </cfif>

 		   <input type="hidden" name="i" value=#i#>
 		   <input type="hidden" name="deal_comment_id" value=#deal_comment_id#>

		</table>

		</form>

		</cfoutput>

      </td></tr>

   </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>