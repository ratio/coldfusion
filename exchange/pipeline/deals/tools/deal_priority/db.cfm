<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal_priority (deal_priority_name, deal_priority_hub_id, deal_priority_order)
	 values ('#deal_priority_name#',#session.hub#,#deal_priority_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal_priority
	 set deal_priority_name = '#deal_priority_name#',
	     deal_priority_order = #deal_priority_order#
	 where deal_priority_id = #deal_priority_id# and
	       deal_priority_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_priority
	 where deal_priority_id = #deal_priority_id# and
	       deal_priority_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

