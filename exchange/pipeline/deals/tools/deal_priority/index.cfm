<cfinclude template="/exchange/security/check.cfm">

<cfquery name="deal_priority" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from deal_priority
  where deal_priority_hub_id = #session.hub#
  order by deal_priority_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Deal Priorities</td>
	       <td class="feed_sub_header" align=right><a href="/exchange/pipeline/deals/tools/">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Deal Priority has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Deal Priority has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Deal Priority has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Deal Priority</a></td></tr>

      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #deal_priority.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Deal Priorities exist.</td></tr>

        <cfelse>
        <tr>
				<td class="feed_sub_header">Order</b></td>
				<td class="feed_sub_header" width=300>Name</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="deal_priority">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=middle width=5%><a href="edit.cfm?deal_priority_id=#deal_priority_id#">#deal_priority_order#</a></td>
              <td class="feed_sub_header" valign=middle><a href="edit.cfm?deal_priority_id=#deal_priority_id#">#deal_priority_name#</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

