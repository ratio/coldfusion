<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal_type (deal_type_name, deal_type_hub_id, deal_type_order)
	 values ('#deal_type_name#',#session.hub#,#deal_type_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal_type
	 set deal_type_name = '#deal_type_name#',
	     deal_type_order = #deal_type_order#
	 where deal_type_id = #deal_type_id# and
	       deal_type_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_type
	 where deal_type_id = #deal_type_id# and
	       deal_type_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

