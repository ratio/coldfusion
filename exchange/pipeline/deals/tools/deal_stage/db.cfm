<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal_stage (deal_stage_name, deal_stage_hub_id, deal_stage_order)
	 values ('#deal_stage_name#',#session.hub#,#deal_stage_order#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal_stage
	 set deal_stage_name = '#deal_stage_name#',
	     deal_stage_order = #deal_stage_order#
	 where deal_stage_id = #deal_stage_id# and
	       deal_stage_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_stage
	 where deal_stage_id = #deal_stage_id# and
	       deal_stage_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

