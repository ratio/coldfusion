<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Attachment">

	<cfif #attachment_file# is not "">
		<cffile action = "upload"
		 fileField = "attachment_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 insert attachment
	 (
	  attachment_name,
	  attachment_desc,
	  attachment_created_by,
      attachment_deal_id,
	  attachment_file,
	  attachment_updated
	  )
	 values (
	 '#attachment_name#',
	 '#attachment_desc#',
	  #session.usr_id#,
	  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
      <cfif #attachment_file# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	  #now()#
	  )
	</cfquery>

	<cfif isdefined("l")>
	 <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=4" addtoken="no">
	</cfif>

	<cflocation URL="deal_open.cfm?i=#i#&u=10" addtoken="no">

<cfelseif button is "Save">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select attachment_file from attachment
		  where attachment_id = #attachment_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.attachment_file#")>
			<cffile action = "delete" file = "#media_path#\#remove.attachment_file#">
	    </cfif>

	</cfif>

	<cfif #attachment_file# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select attachment_file from attachment
		  where attachment_id = #attachment_id#
		</cfquery>

		<cfif #getfile.attachment_file# is not "">
          <cfif fileexists("#media_path#\#getfile.attachment_file#")>
		   <cffile action = "delete" file = "#media_path#\#getfile.attachment_file#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "attachment_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 update attachment
	 set attachment_name = '#attachment_name#',
	     attachment_desc = '#attachment_desc#',

		  <cfif #attachment_file# is not "">
		   attachment_file = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   attachment_file = null,
		  </cfif>

	  attachment_updated = #now()#
	  where attachment_id = #attachment_id#
	</cfquery>

	<cfif isdefined("l")>
	 <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=5" addtoken="no">
	</cfif>

	<cflocation URL="deal_open.cfm?i=#i#&u=20" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select attachment_file from attachment
	  where attachment_id = #attachment_id#
	</cfquery>

	<cfif remove.attachment_file is not "">
     <cfif fileexists("#media_path#\#remove.attachment_file#")>
		 <cffile action = "delete" file = "#media_path#\#remove.attachment_file#">
     </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 delete attachment
	 where attachment_id = #attachment_id#
	</cfquery>

	<cfif isdefined("l")>
	 <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=6" addtoken="no">
	</cfif>

	<cflocation URL="deal_open.cfm?i=#i#&u=30" addtoken="no">

</cfif>
