<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from deal
 where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

		   <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <div class="main_box">

       <form action="partner_save.cfm" method="post" enctype="multipart/form-data" >

       <cfoutput>
       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Add Partner Snapshot</td>
            <td align=right class="feed_sub_header"><a href="market.cfm?i=#i#">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>
       </cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" width=150><b>Keyword</b></td>
             <td class="feed_option"><input type="text" class="input_text" style="width: 534px;" name="partner_snapshot_keyword" placeholder="i.e., Machine learning and clinical." required></td>
             </td></tr>

	      <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td></td><td>

          <input class="button_blue_large" type="submit" name="button" value="Create Snapshot">

          </td></tr>

        <cfoutput>
         <input type="hidden" name="i" value="#i#">
        </cfoutput>

       </table>


       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>