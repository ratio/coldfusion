<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

<cfset session.deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 20px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	display: inline-block;
	margin-left: -4px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	padding-right: 20px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}

.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<cfinclude template="/exchange/pipeline/check_access.cfm">

      <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from deal
       left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
       join usr on usr_id = deal_owner_id
       left join deal_type on deal_type.deal_type_id = deal.deal_type_id
       left join deal_priority on deal_priority.deal_priority_id = deal.deal_priority_id
       where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
      </cfquery>

	  <cfquery name="activity" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from deal_activity
	   where deal_activity_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	  </cfquery>

	  <cfquery name="attachments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from attachment
	   left join usr on usr_id = attachment_created_by
	   where attachment_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	  </cfquery>

	  <cfquery name="comments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from deal_comment
	   join usr on usr_id = deal_comment_usr_id
	   where deal_comment_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	   order by deal_comment_date DESC
	  </cfquery>


      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/components/recent_boards/index.cfm">

       </td><td valign=top>

       <cfoutput>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_home2.png" width=20 align=absmiddle>&nbsp;&nbsp;<a href="deal_open.cfm?i=#i#">Opportunity Summary</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="market.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Partners</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="competition.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Competition</a></span>
          </div>

       </cfoutput>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="/exchange/pipeline/refresh_pipelines.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>

			 <cfif len(deal.deal_name) GT 100>
			  #left(deal.deal_name,100)#...
			 <cfelse>
			  #deal.deal_name#
	         </cfif>

             </td>

				<td align=right class="feed_sub_header">

                 <cfif session.pipeline_access_level GTE 2>

						<img src="/images/icon_edit.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="edit.cfm?i=#i#">Edit Opportunity</a>
						&nbsp;|&nbsp;

				 </cfif>

				<cfif isdefined("l")>
				 <cfif l is "h">
				  <a href="/exchange/pipeline/">Return</a>
				 <cfelseif l is 1>
					<a href="/exchange/pipeline/open.cfm">Return</a>
				 </cfif>
				<cfelse>
					<a href="/exchange/pipeline/open.cfm">Return</a>
			    </cfif>


            </td></tr>
        </cfoutput>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>

       <cfif isdefined("u")>
        <cfif u is 2>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Opportunity has been successfully updated.</td></tr>
        <cfelseif u is 4>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;"></td></tr>
        <cfelseif u is 5>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Company has been removed as a Deal Partner</td></tr>
        <cfelseif u is 6>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Company has been removed as a Deal Competitor</td></tr>
        <cfelseif u is 10>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Attachment has been successfully saved.</td></tr>
        <cfelseif u is 20>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Attachment has been successfully updated.</td></tr>
        <cfelseif u is 30>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Attachment has been successfully deleted.</td></tr>
        <cfelseif u is 21>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Activity has been successfully added.</td></tr>
        <cfelseif u is 22>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Activity has been successfully updated.</td></tr>
        <cfelseif u is 23>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Activity has been successfully deleted.</td></tr>

        <cfelseif u is 31>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Comment has been successfully added.</td></tr>
        <cfelseif u is 32>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Comment has been successfully updated.</td></tr>
        <cfelseif u is 33>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Comment has been successfully deleted.</td></tr>


        </cfif>
       </cfif>

       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
		    <td class="feed_sub_header">Customer</td>
		    <td class="feed_sub_header">Owner</td>
		    <td class="feed_sub_header" align=center>Stage</td>
		    <td class="feed_sub_header" align=center>Type</td>
		    <td class="feed_sub_header" align=center>Priority</td>
		    <td class="feed_sub_header" align=center>Est Close Date</td>
		    <td class="feed_sub_header" align=right>Potential Value</td>
         </tr>

        <cfoutput>

         <tr>
            <td class="feed_sub_header" style="font-weight: normal;" width=500>#deal.deal_customer_name#</td>
            <td class="feed_sub_header" style="font-weight: normal;">#deal.usr_first_name# #deal.usr_last_name#</td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #deal.deal_stage_name# is "">TBD<cfelse>#deal.deal_stage_name#</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #deal.deal_type_name# is "">TBD<cfelse>#deal.deal_type_name#</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #deal.deal_priority_name# is "">TBD<cfelse>#deal.deal_priority_name#</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center width=75><cfif #deal.deal_close_date# is "">TBD<cfelse>#dateformat(deal.deal_close_date,'mm/dd/yyyy')#</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=right width=100><cfif #deal.deal_value_total# is "">TBD<cfelse>#numberformat(deal.deal_value_total,'$999,999,999,999')#</cfif></td>
         </tr>

        <tr><td colspan=6 class="feed_sub_header">Description</td></tr>

        <cfif trim(deal.deal_desc) is "">
         <tr><td colspan=6 class="feed_sub_header" style="font-weight: normal;">No description provided.</td></tr>
        <cfelse>
         <tr><td colspan=6 class="feed_sub_header" style="font-weight: normal;">#deal.deal_desc#</td></tr>
        </cfif>

        <tr><td colspan=6 class="feed_sub_header">Reference URL</td></tr>
        <tr><td colspan=6 class="feed_sub_header" style="font-weight: normal;">

        <cfif trim(deal.deal_url) is "">
         Not provided.
        <cfelse>

         <cfif deal.deal_award_id is not "">
          <a href="/exchange/include/award_information.cfm?id=#deal.deal_award_id#" target="_blank" style="font-weight: normal;"><u>#session.site#/exchange/include/award_information.cfm?id=#deal.deal_award_id#</u></a>
         <cfelseif deal.deal_grant_id is not "">
          <a href="/exchange/include/grant_new_information.cfm?id=#deal.deal_grant_id#" style="font-weight: normal;"><u>#session.site#/exchange/include/grant_new_information.cfm?id=#deal.deal_grant_id#</u></a>
         <cfelse>
          <a href="#deal.deal_url#" target="_blank" style="font-weight: normal;"><u>#wrap(deal.deal_url,50)#</u></a>

         </cfif>

        </cfif>
        </td></tr>

        </cfoutput>

	   </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td colspan=3><hr></td></tr>
          <tr><td valign=top class="feed_sub_header">Schedule</td>
              <td class="feed_sub_header" align=right>

              <cfoutput>

                 <cfif session.pipeline_access_level GTE 2>
					<a href="activity_add.cfm?i=#i#"><img src="/images/plus3.png" width=15 hspace=10 border=0 alt="Add Activity" title="Add Activity"></a>
					<a href="activity_add.cfm?i=#i#">Add Activity</a>
				  </cfif>

			  </cfoutput>

              </td></tr>

        <cfif activity.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No activities have been created.</td></tr>
        <cfelse>

          <tr><td colspan=2>

			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript">
			  google.charts.load('current', {'packages':['timeline']});
			  google.charts.setOnLoadCallback(drawChart);
			  function drawChart() {
				var container = document.getElementById('timeline');
				var chart = new google.visualization.Timeline(container);
				var dataTable = new google.visualization.DataTable();

				dataTable.addColumn({ type: 'string', id: 'Name' });
				dataTable.addColumn({ type: 'string', id: 'Activity' });
				dataTable.addColumn({ type: 'date', id: 'Start' });
				dataTable.addColumn({ type: 'date', id: 'End' });
				dataTable.addColumn({ type: 'string', role: 'tooltip', id: 'link', 'p': {'html': true} });
				dataTable.addRows([

				<cfoutput query="activity">
	            <cfif session.pipeline_access_level is 3>
				  [ 'Activities', '#deal_activity_name#', new Date(#year(deal_activity_start_date)#, #evaluate(month(deal_activity_start_date)-1)#, #day(deal_activity_start_date)#), new Date(#year(deal_activity_end_date)#, #evaluate(month(deal_activity_end_date)-1)#, #day(deal_activity_end_date)#),'activity_edit.cfm?i=#i#&id=#deal_activity_id#'],
				<cfelse>
				  [ 'Activities', '#deal_activity_name#', new Date(#year(deal_activity_start_date)#, #evaluate(month(deal_activity_start_date)-1)#, #day(deal_activity_start_date)#), new Date(#year(deal_activity_end_date)#, #evaluate(month(deal_activity_end_date)-1)#, #day(deal_activity_end_date)#),''],
				</cfif>
				</cfoutput>

				  ]);

			var options = {
			  timeline: { showRowLabels: false, colorByRowLabel: true }
			};

		 google.visualization.events.addListener(chart, 'select', function () {
			var selection = chart.getSelection();
			if (selection.length > 0) {
			  window.open(dataTable.getValue(selection[0].row, 4), '_self');
			  console.log(dataTable.getValue(selection[0].row, 4));
			}
		  });

		  function drawChart1() {
			chart.draw(dataTable, options);
		  }
		  drawChart1();

			  }
			</script>

			<div id="timeline" style="width: 100%; height: 150;"></div>

        </td></tr>

        </cfif>

        </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=3><hr></td></tr>
          <tr><td valign=top width=48%>

			<cfquery name="partner_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select deal_comp_partner_id from deal_comp
			 where deal_comp_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       deal_comp_partner_id is not null
            </cfquery>

            <cfif partner_list.recordcount is 0>
             <cfset deal_list = 0>
            <cfelse>
             <cfset deal_list = valuelist(partner_list.deal_comp_partner_id)>
            </cfif>

			<cfquery name="deal_partner" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from company
			 where company_id in (#deal_list#)
			 order by company_name
 		    </cfquery>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr><td height=10></td></tr>

		    <cfoutput>
		    <tr>
		        <td class="feed_sub_header" colspan=2><a href="market.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Potential Partners</a><a href="market.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/icon_search.png" width=18 alt="Find Partners" title="Find Partners" border=0 hspace=10></a></td>
		        <cfif deal_partner.recordcount GT 0>
		        	<td class="feed_header" align=center>STRENGTH</td>
		        </cfif>
             </tr>
             </cfoutput>

 		    <cfif deal_partner.recordcount is 0>
		     <cfoutput>
		     <tr><td class="feed_sub_header" style="font-weight: normal;">No Partners have been identified.

		     <cfif session.pipeline_access_level is 3>
			     <a href="/exchange/pipeline/deals/market.cfm?i=#i#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><b>Click here</b></a> to source potential Partners.
			 </cfif>

			 </td></tr>
		     </cfoutput>
            <cfelse>

             <tr><td height=10></td></tr>


            <cfoutput query="deal_partner">

            <tr>

             <td width=50>

                    <a href="company_details.cfm?i=#i#&id=#company_id#&type=p">
                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=30 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=30 border=0>
					</cfif>
					</a>

			 </td>

             <td class="feed_sub_header"><b><a href="company_details.cfm?i=#i#&id=#company_id#&type=p"><cfif len(company_name) GT 200>#ucase(left(company_name,200))#...<cfelse>#ucase(company_name)#</cfif></b></td>
             <td class="feed_sub_header" width=2% align=center style="background-color: 435177; color: ffffff; border-style: solid; border-width: 3px; border-color: ffffff;"><b>TBD</b></td>

             </tr>

             </cfoutput>

             </cfif>

            </table>

          </td><td width=75>&nbsp;</td><td valign=top width=48%>


			<cfquery name="competitor_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select deal_comp_competitor_id from deal_comp
			 where deal_comp_deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			       deal_comp_competitor_id is not null
            </cfquery>

            <cfif competitor_list.recordcount is 0>
             <cfset competitor_list = 0>
            <cfelse>
             <cfset competitor_list = valuelist(competitor_list.deal_comp_competitor_id)>
            </cfif>

			<cfquery name="deal_competitor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from company
			 where company_id in (#competitor_list#)
			 order by company_name
 		    </cfquery>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
		     <cfoutput>
		     <tr>
		         <td class="feed_sub_header" colspan=2><a href="competition.cfm?i=#i#">Potential Competitors</a><a href="competition.cfm?i=#i#"><img src="/images/icon_search.png" width=18 alt="Find Competitors" title="Find Competitors" border=0 hspace=10></a></td>

		         <cfif deal_competitor.recordcount GT 0>
		         	<td class="feed_header" align=center>STRENGTH</td>
		         </cfif>

		         </tr>

             <tr><td height=10></td></tr>

		     </cfoutput>

 		    <cfif deal_competitor.recordcount is 0>
		     <cfoutput>
		     <tr>
		         <td class="feed_sub_header" style="font-weight: normal;" colspan=3>No Competitors have been identified.

                 <cfif session.pipeline_access_level is 3>
		         	<a href="/exchange/pipeline/deals/competition.cfm?i=#i#"><b>Click here</b></a> to identify Competitors.
		         </cfif>
		         </td>
		         </tr>
		     </cfoutput>
            <cfelse>

            <cfoutput query="deal_competitor">

            <tr>

             <td width=50>

                    <a href="company_details.cfm?i=#i#&id=#company_id#&type=c">
                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=30 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=30 border=0>
					</cfif>
					</a>

			 </td>

             <td class="feed_sub_header"><b><a href="company_details.cfm?i=#i#&id=#company_id#&type=c"><cfif len(company_name) GT 200>#ucase(left(company_name,200))#...<cfelse>#ucase(company_name)#</cfif></b></td>
             <td width=2% class="feed_sub_header" align=center style="background-color: 435177; color: ffffff; border-style: solid; border-width: 3px; border-color: ffffff;"><b>TBD</b></td>


             </tr>

             </cfoutput>

             </cfif>

            </table>

          </td></tr>

        </table>

       <!--- Attachments --->

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=2><hr></td></tr>
         <tr><td class="feed_sub_header">Opportunity Attachments</td>
             <td class="feed_sub_header" align=right>

            <cfif session.pipeline_access_level GTE 2>
             <cfoutput>
             	<a href="attachment_add.cfm?i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a><a href="attachment_add.cfm?i=#i#">Add Attachment</a>
             </cfoutput>
            </cfif>

            </td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif attachments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No attachments have been added.</td></tr>
             <cfelse>

              <tr>
                  <td></td>
                  <td class="feed_sub_header">Name</td>
                  <td class="feed_sub_header">Description</td>
                  <td class="feed_sub_header">File</td>
                  <td class="feed_sub_header" align=right>Uploaded</td>
              </tr>

              <cfset counter = 0>

              <cfoutput query="attachments">

              <cfif counter is 0>
               <tr bgcolor="FFFFFF">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>

                 <td width=60>

				 <cfif #usr_photo# is "">
				  <img src="/images/headshot.png" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#">
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#usr_photo#" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
				 </cfif>

				 </td>

                  <td class="feed_sub_header" style="font-weight: normal;" width=200>

                  <b>

	            <cfif session.pipeline_access_level GTE 2>
                  <a href="attachment_edit.cfm?i=#i#&attachment_id=#attachment_id#">#attachment_name#</a>
                 <cfelse>
                  #attachment_name#
                 </cfif>

                  </b></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_desc# is "">Not Provided<cfelse>#attachment_desc#</cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_file# is "">No attachment<cfelse><a href="#media_virtual#/#attachment_file#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;"><u>#attachment_file#</u></a></cfif></td>
                  <td class="feed_sub_header" align=right style="font-weight: normal;" width=120>#dateformat(attachment_updated,'mm/dd/yyyy')#</td>

                  </tr>

                  <cfif counter is 0>
                   <cfset counter = 1>
                  <cfelse>
                   <cfset counter = 0>
                  </cfif>

              </cfoutput>

             </cfif>
          </table>


       <!--- Comments --->

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td class="feed_sub_header">Opportunity Comments & Notes</td>
             <td class="feed_sub_header" align=right>

            <cfif session.pipeline_access_level GTE 2>
             <cfoutput>
             	<a href="comment_add.cfm?i=#i#"><img src="/images/plus3.png" width=15 hspace=10></a><a href="deal_comment_add.cfm?i=#i#">Add Comment</a>
             </cfoutput>
            </cfif>

            </td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif comments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No comments or notes have been added.</td></tr>
             <cfelse>

              <tr>
                  <td></td>
                  <td class="feed_sub_header">Comment / Note</td>
                  <td class="feed_sub_header" align=right>Created</td>
              </tr>

              <cfset counter = 0>

              <cfoutput query="comments">

              <cfif counter is 0>
               <tr bgcolor="FFFFFF">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>

                 <td width=60>

				 <cfif #usr_photo# is "">
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#usr_photo#" height=40 width=40 vspace=5 border=0 alt="#usr_first_name# #usr_last_name#" title="#usr_first_name# #usr_last_name#"></a>
				 </cfif>

				 </td>

                  <td class="feed_sub_header" style="font-weight: normal;">#deal_comment_text#</td>

                  <td class="feed_sub_header" align=right style="font-weight: normal;" width=120>#dateformat(deal_comment_date,'mm/dd/yyyy')#</td>

                  <td align=right width=40>
		            <cfif session.pipeline_access_level GTE 2>
	                  <a href="deal_comment_edit.cfm?i=#i#&deal_comment_id=#deal_comment_id#"><img src="/images/icon_edit.png" width=20 alt="Edit" title="Edit"></a>
                    </cfif>
                    </td>

                  </tr>

                  <cfif counter is 0>
                   <cfset counter = 1>
                  <cfelse>
                   <cfset counter = 0>
                  </cfif>

              </cfoutput>

             </cfif>
          </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>