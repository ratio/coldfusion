<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(partner_snapshot_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfif #button# is "Save Snapshot">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update partner_snapshot
      set
      partner_snapshot_keyword = '#search_string#'
      where partner_snapshot_id = #id#
	</cfquery>

	<cflocation URL="market.cfm?i=#i#&u=2" addtoken="no">

<cfelseif #button# is "Create Snapshot">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into partner_snapshot
	 (
      partner_snapshot_keyword,
      partner_snapshot_deal_id
      )
      values
      (
      '#search_string#',
       #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
       )
	</cfquery>

	<cflocation URL="market.cfm?i=#i#&u=4" addtoken="no">

<cfelseif #button# is "Delete Snapshot">

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete partner_snapshot
		 where partner_snapshot_id = #id#
		</cfquery>

	<cflocation URL="market.cfm?i=#i#&id=#id#&u=5" addtoken="no">

</cfif>
