<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 40>
</cfif>

<body class="body">

<cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(id) as total, subawardee_duns, deal_comp_partner_id, company_name, cast(company_about as varchar(max)) as about_short, cast(company_long_desc as varchar(max)) as about_long, company_keywords, company_website, company_city, company_state, company_id, company_logo from award_data_sub
  join company on company_duns = award_data_sub.subawardee_duns
  left join deal_comp on deal_comp_partner_id = company_id
  where contains ((award_data_sub.subaward_description),'"#trim(ky)#"')

	<cfif isdefined("session.add_filter")>
	 <cfif listlen(session.add_filter) is 1>
	  and contains((subaward_description),'"#trim(session.add_filter)#"')
	 <cfelse>
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((subaward_description),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	 </cfif>
	</cfif>

  group by subawardee_duns, company_name, deal_comp_partner_id, cast(company_about as varchar(max)), cast(company_long_desc as varchar(max)), company_keywords, company_website, company_city, company_state, company_id, company_logo

 <cfif sv is 1>
  order by company_name ASC
 <cfelseif sv is 10>
  order by company_name DESC
 <cfelseif sv is 2>
  order by company_city ASC
 <cfelseif sv is 20>
  order by company_city DESC
 <cfelseif sv is 3>
  order by company_state ASC
 <cfelseif sv is 30>
  order by company_state DESC
 <cfelseif sv is 4>
  order by total asc, company_name ASC
 <cfelseif sv is 40>
  order by total DESC, company_name ASC
 <cfelseif sv is 5>
  order by deal_comp_partner_id DESC, company_name ASC
 <cfelseif sv is 50>
  order by deal_comp_partner_id ASC, total DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 80>
  order by fbo_naics DESC
 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC
 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC
 </cfif>

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/pipeline/recent.cfm">

       </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">SUBCONTRACTORS FOUND - #numberformat(agencies.recordcount,'99,999')#</td>
				 <td class="feed_option" align=right>
				 <a href="/exchange/opps/forecast/views/cview_02.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
				 &nbsp;&nbsp;&nbsp;&nbsp;

					 <img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">

				 </td></tr>
				</cfoutput>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

         </table>

         <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td class="feed_sub_header">You searched for "<i>#ky#</i>"</td>

              <td align=right>

				<form action="set_filter.cfm" method="post">
				    <td class="feed_sub_header" style="font-weight: normal;" align=right><b>Add Filter</b>&nbsp;&nbsp;
				    <input class="input_text" type="text" size=30 name="add_filter" required placeholder="and 'new keyword'">&nbsp;&nbsp;
				    <input class="button_blue" type="submit" name="button" value="Apply">
				    <input type="hidden" name="location" value="view_sub">
				    <input type="hidden" name="ky" value="#ky#">
				    <input type="hidden" name="i" value=#i#>
				    <input type="hidden" name="sv" value=#sv#>
                    </td>
                </form>

              </td>

          </tr>

         </cfoutput>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

					<cfif agencies.recordcount GT #perpage#>

				    <td class="feed_sub_header">

				    Result Set -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>

						</cfif>

				      </cfif>

				</td>

				<td align=right class="feed_sub_header">

					<cfif isdefined("session.add_filter")>

                      Filters:&nbsp;&nbsp;

						 <cfloop index="k" list="#session.add_filter#">
							<cfset size = evaluate(10*len(k))>
							 <input class="button_blue" style="font-size: 11px; height: 25px; width: auto; margin-left: 10px; margin-right: 0px; margin-top: 5px; margin-bottom: 5px;" type="submit" name="button" value="X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#ucase(k)#&nbsp;&nbsp;" onclick="location.href = 'remove.cfm?i=#i#&ky=#ky#&location=view_sub&sv=#sv#'">
						 </cfloop>

					</cfif>

                </td>

				</tr>

				<tr><td colspan=3><hr></td></tr>
				<tr><td height=10></td></tr>

				    <cfif isdefined("u")>
				    <tr><td class="feed_sub_header">

				     <cfif u is 1>
				      <span style="color: green;">Partner has been successfully added to the deal.</span>
				     <cfelseif u is 2>
				      <span style="color: red;">Partner has been removed from the deal.</span>
				     </cfif>
				    </td></tr>
				    </cfif>

			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td class="feed_sub_header"><a href="view_sub.cfm?i=#i#&ky=#ky#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>SELECT</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td colspan=2 class="feed_sub_header"><a href="view_sub.cfm?i=#i#&ky=#ky#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>COMPANY</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" style="padding-left: 10px; padding-right: 10px;"><b>OVERVIEW</b></td>
            <td class="feed_sub_header"><b>WEBSITE</b></td>
            <td class="feed_sub_header"><a href="view_sub.cfm?i=#i#&ky=#ky#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CITY</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=center><a href="view_sub.cfm?i=#i#&ky=#ky#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>STATE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=center><a href="view_sub.cfm?i=#i#&ky=#ky#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>SUBCONTRACTS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

             <td width=50>

             <cfif #deal_comp_partner_id# is not "">
	             <a href="select.cfm?i=#i#&ky=#ky#&sv=#sv#&deal_comp_partner_id=#company_id#&a=0"><img src="/images/box_checked.png" style="padding-left: 10px;" width=30 border=0 alt="Unselect" title="Unselect"></a>
             <cfelse>
	             <a href="select.cfm?i=#i#&ky=#ky#&sv=#sv#&deal_comp_partner_id=#company_id#&a=1"><img src="/images/box_unchecked.png" style="padding-left: 10px;" width=30 border=0 alt="Select" title="Select"></a>
             </cfif>
             <!---

             <input type="checkbox" name="deal_comp_partner_id" style="width: 22px; height: 22px;" <cfif #deal_comp_partner_id# is not "">checked</cfif> value=#company_id# onChange="this.form.submit()">

             --->

             </td>

             <td width=60>

                    <a href="/exchange/include/profile.cfm?id=#company_id#&l=11&s=t" target="_blank" rel="noopener">
                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=40 border=0>
					</cfif>
					</a>

			 </td>

             <td class="feed_sub_header" width=500><b><a href="/exchange/include/profile.cfm?id=#company_id#&l=11&s=t" target="_blank" rel="noopener"><cfif len(company_name) GT 200>#ucase(left(company_name,200))#...<cfelse>#ucase(company_name)#</cfif></b></td>
             <cfif about_short is not "">
	             <td class="feed_option" width=300 style="padding-left: 10px; padding-right: 10px;">#ucase(about_short)#</td>
             <cfelse>
	             <td class="feed_option" width=300 style="padding-left: 10px; padding-right: 10px;">#ucase(about_long)#</td>
             </cfif>
             <td class="feed_option"><a href="#company_website#" target="_blank" rel="noopener"><u><cfif len(company_website) GT 30>#left(company_website,30)#...<cfelse>#ucase(company_website)#</cfif></u></a></td>
             <td class="feed_option" width=100>#ucase(company_city)#</td>
             <td class="feed_option" align=center>#ucase(company_state)#</td>
             <td class="feed_option" align=center style="padding-left: 5px; padding-right: 5px; font-size: 18px;"><b>#total#</b></td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>