<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

	<cfquery name="update_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal
	 set deal_updated = #now()#
	 where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="update_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal_activity
	 set deal_activity_name = '#deal_activity_name#',
	     deal_activity_desc = '#deal_activity_desc#',
	     deal_activity_start_date = '#deal_activity_start_date#',
	     deal_activity_end_date = '#deal_activity_end_date#',
	     deal_activity_updated = #now()#
	 where deal_activity_id = #id#
	</cfquery>

	<cfif isdefined("l")>
	 <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=2" addtoken="no">
	</cfif>

    <cflocation URL="deal_open.cfm?i=#i#&u=22" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete deal_activity
	 where deal_activity_id = #id#
	</cfquery>

	<cfif isdefined("l")>
	 <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=3" addtoken="no">
	</cfif>

    <cflocation URL="deal_open.cfm?i=#i#&u=23" addtoken="no">

<cfelseif button is "Save Activity">

<cftransaction>

	<cfquery name="update_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update deal
	 set deal_updated = #now()#
	 where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cfquery name="insert_activity" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal_activity
	 (
	 deal_activity_deal_id,
	 deal_activity_name,
	 deal_activity_desc,
	 deal_activity_usr_id,
	 deal_activity_created,
	 deal_activity_updated,
	 deal_activity_hub_id,
	 deal_activity_start_date,
	 deal_activity_end_date
	 )
	 values
	 (
	  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	 '#deal_activity_name#',
	 '#deal_activity_desc#',
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	  #session.hub#,
	 '#deal_activity_start_date#',
	 '#deal_activity_end_date#'
	  )
	</cfquery>

</cftransaction>

	<cfif isdefined("l")>
	 <cflocation URL="/exchange/marketplace/communities/opp_open.cfm?i=#i#&u=2" addtoken="no">
	</cfif>

    <cflocation URL="deal_open.cfm?i=#i#&u=21" addtoken="no">

</cfif>
