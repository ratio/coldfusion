<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

     <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from deal
       left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
       where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
      </cfquery>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       <div class="left_box">

       </div>

       </td><td valign=top>

       <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="deal_open.cfm?i=#i#">DEAL DASHBOARD</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="customer.cfm?i=#i#">CUSTOMER PROFILE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="market.cfm?i=#i#">MARKETPLACE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="competition.cfm?i=#i#">COMPETITION</a></span>
          </div>

       </cfoutput>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="/exchange/pipeline/refresh_pipelines.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>#ucase(deal.deal_name)#</td>
            <td align=right class="feed_sub_header">
            <img src="/images/icon_edit.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="edit_customer.cfm?i=#i#">EDIT CUSTOMER PROFILE</a>

            </td></tr>
        </cfoutput>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Deal has been successfully updated.</td></tr>
        <cfelseif u is 4>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;"></td></tr>
        </cfif>
       </cfif>

       </table>

       <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td class="feed_sub_header">CUSTOMER NAME</td>
            <td class="feed_sub_header" align=right>DEAL PLACE OF PERFORMANCE</td>
         </tr>

         <tr>
            <td class="feed_sub_header" style="font-weight: normal;">#deal.deal_customer_name#</td>
            <td class="feed_sub_header" style="font-weight: normal;" align=right><cfif #deal.deal_pop_city# is "" and #deal.deal_pop_state# is "">TBD</cfif></td>
         </tr>

         <tr><td colspan=2><hr></td></tr>

       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td class="feed_sub_header">FEDERAL ALIGNMENT</td>
            <td class="feed_sub_header" align=right>
              <img src="/images/icon_edit.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="edit_customer.cfm?i=#i#">UPDATE ALIGNMENTS</a>
            </td>

         </tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>


         <tr>
            <td class="feed_sub_header">BUYING DEPARTMENT</td>
		    <td class="feed_sub_header" align=center>BUYING AGENCY</td>
		    <td class="feed_sub_header" align=center>BUYING OFFICE</td>
		    <td class="feed_sub_header" align=center>DEAL NAICS CODE(S)</td>
		    <td class="feed_sub_header" align=right>DEAL PSC CODE(S)</td>
         </tr>

         <tr>
		    <td class="feed_sub_header" style="font-weight: normal;"><cfif #deal.deal_dept_code# is "">TBD<cfelse>&nbsp;</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #deal.deal_agency_code# is "">TBD<cfelse>&nbsp;</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #deal.deal_office_code# is "">TBD<cfelse>&nbsp;</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #deal.deal_naics# is "">TBD<cfelse>&nbsp;</cfif></td>
		    <td class="feed_sub_header" style="font-weight: normal;" align=right><cfif #deal.deal_psc# is "">TBD<cfelse>&nbsp;</cfif></td>
         </tr>

	   </table>

        </cfoutput>










	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>