<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

       <div class="main_box">

       <cfoutput>
       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>Add Comment</td>
            <td valign=top align=right class="feed_sub_header">

			 <cfif isdefined("l")>
			  <a href="/exchange/marketplace/communities/opp_open.cfm?i=#i#">Return</a>
			 <cfelse>
			  <a href="deal_open.cfm?i=#i#">Return</a>
			 </cfif>

             </td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>
       </cfoutput>

            <form action="deal_comment_db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header" valign=top width=100>Comment</td>
				 <td><textarea class="input_textarea" name="deal_comment_text" style="width: 900px; height: 200px;" placeholder="Please enter your comments here."></textarea></td>
		      </tr>

               <tr><td colspan=2><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
              <input type="submit" class="button_blue_large" name="button" value="Add Comment">&nbsp;&nbsp;

		      </td></tr>

             </table>

             <cfoutput>

             <cfif isdefined("l")>
              <input type="hidden" name="l" value=#l#>
             </cfif>

             <input type="hidden" name="i" value=#i#>
             </cfoutput>

             </form>

   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>