<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template="/exchange/include/header.cfm">

<style>
.comp_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 310px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 3px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

     <cfquery name="deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       select * from deal
       left join deal_stage on deal_stage.deal_stage_id = deal.deal_stage_id
       where deal_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
      </cfquery>

     <cfquery name="snap" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      select * from comp_snapshot
      where comp_snapshot_id = #id#
     </cfquery>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

      <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="/exchange/pipeline/refresh_pipelines.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>#ucase(deal.deal_name)#</td>
            <td align=right class="feed_sub_header"><a href="competition.cfm?i=#i#&id=#id#">Return</a></td></tr>
        </cfoutput>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>

       <cfif isdefined("u")>
        <cfif u is 2>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Snapshot has been successfully updated.</td></tr>
        <cfelseif u is 4>
         <tr><td colspan=2 class="feed_sub_header" style="color: green;">Snapshot has been successfully created.</td></tr>
        <cfelseif u is 5>
         <tr><td colspan=2 class="feed_sub_header" style="color: red;">Snapshot has been successfully deleted.</td></tr>
        </cfif>
       </cfif>

       </table>

       <cfoutput>
		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header">#ucase(snap.comp_snapshot_name)#</td></tr>
			<tr><td class="feed_sub_header" style="font-weight: normal;">#snap.comp_snapshot_desc#</td></tr>
			<tr><td height=10></td></tr>
		   </table>
       </cfoutput>


		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>
				<td class="feed_sub_header">Competitor?</td>
				<td></td>
				<td class="feed_sub_header">Company Name</td>
				<td class="feed_sub_header">Description</td>
				<td class="feed_sub_header">Website</td>
				<td class="feed_sub_header">City</td>
				<td class="feed_sub_header" align=center>State</td>
				<td class="feed_sub_header" align=center>Awards</td>
			</tr>

			<tr><td height=10></td></tr>

          <cfset row_counter = 0>

          <form action="remove_selected.cfm" method="post">

			  <cfquery name="company_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select recipient_duns, company_name, company_id, company_website, company_logo,
			          company_keywords, company_city, company_state,
                      (select count(distinct(parent_award_id)) as awards from award_data where recipient_duns = company_duns) as awards,
			          cast(company_long_desc as varchar(max)) as company_long_desc from company
			   join award_data on recipient_duns = company_duns

               where (contains((award_description),'"#trim(snap.comp_snapshot_keywords)#"'))

			   <cfif snap.comp_snapshot_dept is not 0>
			    and awarding_agency_code = '#snap.comp_snapshot_dept#'
			   </cfif>

			   <cfif snap.comp_snapshot_agency is not 0>
			    and awarding_sub_agency_code = '#snap.comp_snapshot_agency#'
			   </cfif>

			   <cfif snap.comp_snapshot_city is not "">
			    and primary_place_of_performance_city_name = '#snap.comp_snapshot_city#'
			   </cfif>

			   <cfif snap.comp_snapshot_state is not 0>
			    and primary_place_of_performance_state_code = '#snap.comp_snapshot_state#'
			   </cfif>

			   group by recipient_duns, company_name, company_id, company_duns, company_website, cast(company_long_desc as varchar(max)), company_logo, company_keywords, company_city, company_state
               order by awards DESC


			  </cfquery>

          <cfloop query="company_info">

                <tr

                <cfoutput>

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>

					<td width=2%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="selected_company" style="width: 22px; height: 22px;" value=#company_info.company_id#></td>

					<td width=100 class="feed_option" align=center>

                    <a href="/exchange/include/company_profile.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif company_info.company_logo is "">
					  <img src="//logo.clearbit.com/#company_info.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_info.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header" width=225 style="padding-right: 20px;"><a href="/exchange/include/company_profile.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(company_info.company_name)#</a></td>

                    <td class="feed_option">#company_info.company_long_desc#</td>

					<td class="feed_option" width=250>
					<a href="#company_info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(company_info.company_website) GT 40>
					<u>#ucase(left(company_info.company_website,40))#...</u>
					<cfelse>
					<u>#ucase(company_info.company_website)#</u>
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(company_info.company_city)#</a></td>
					<td class="feed_option" width=75 align=center>#ucase(company_info.company_state)#</a></td>
					<td class="feed_sub_header" align=center>#company_info.awards#</a></td>

				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				 <tr><td colspan=10><hr></td></tr>

				</cfoutput>

			</cfloop>

		  <tr><td height=10></td></tr>
          <tr><td colspan=3><input type="submit" name="button" value="Update" class="button_blue_large"  onclick="return confirm('Update Competitors?\r\nAre you sure you want to update your list of Competitors?');"></td></tr>

          </form>

          </table>
















	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>