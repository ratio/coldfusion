<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from comments_rating
 where comments_rating_hub_id = #session.hub#
 order by comments_rating_value DESC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

       <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>ADD COMMENTS</td>
             <td align=right class="feed_sub_header" valign=absmiddle><a href="company_details.cfm?i=#i#&id=#id#&type=#type#">Return</a></td>
		 <tr><td colspan=10><hr></td></tr>

	   </table>
	   </cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=125>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option">

                    <a href="open.cfm?id=#info.company_id#">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=100 border=0>
					</cfif>
					</a>

					</td>
			    </tr>

			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(info.company_name)#</td>
	              <td class="feed_sub_header" align=right></td></tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #info.company_about# is "">
					No Description Provided
					<cfelse>
					#info.company_about#
					</cfif>

					</td>

			    </tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #info.company_keywords# is "">
					No Keywords Provided
					<cfelse>
					#info.company_keywords#
					</cfif>

					</td>
			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

       <!--- Company Feedback --->

       <form action="comments_db.cfm" method="post" enctype="multipart/form-data">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td class="feed_sub_header" width=200><b>Context</b></td>
			<td><input name="company_intel_context" class="input_text" type="text" size=100 maxlength="1000" required placeholder="What's the context of writing these comments?"></td>
		</tr>

		<tr><td class="feed_sub_header" valign=top><b>Description</b></td>
			<td><textarea name="company_intel_comments" class="input_textarea" rows=6 cols=101 placeholder="Please provide any comments related to this intel."></textarea></td>
		</tr>

		<tr><td class="feed_sub_header"><b>Reference URL</b></td>
			<td><input name="company_intel_url" class="input_text" type="url" size=100 maxlength="1000"></td>
		</tr>

		<tr><td height=10></td></tr>

		<tr><td class="feed_sub_header" valign=top>Attachment</td>
			<td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="company_intel_attachment"></td></tr>

		<tr>

		<td class="feed_sub_header">Rating</td>
		<td><select name="company_intel_rating" class="input_select">
		<cfoutput query="rating">
		 <option value=#comments_rating_value#>#comments_rating_name#
		</cfoutput>
		</select>

		</td></tr>

		<tr>
		<td class="feed_sub_header">Sharing</td>
		<td><select name="company_intel_sharing" class="input_select">
		 <option value=1>Yes, anyone in Company can see
		 <option value=0 selected>No, display only to me
		</select>

		</td></tr>

		<tr><td height=10></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>

		<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Comment"></td></tr>

	   </table>

	   <cfoutput>

	    <input type="hidden" name="type" value="#type#">

	    <input type="hidden" name="id" value=#id#>
	    <input type="hidden" name="i" value=#i#>

	    <cfif isdefined("l")>
	     <input type="hidden" name="l" value=#l#>
	    <cfelse>
	     <input type="hidden" name="l" value=0>
	    </cfif>

	   </cfoutput>

	   </form>


        </table>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>