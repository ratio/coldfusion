<cfset pipeline_access = 0>
<cfset session.pipeline_access_level = 0>

<!--- Check for Admin access --->

<cfquery name="admin_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select pipeline_id from pipeline
 where pipeline_usr_id = #session.usr_id# and
       pipeline_id = #session.pipeline_id#
</cfquery>

<cfif admin_access.recordcount is 1>

	 <cfset pipeline_access = 1>
	 <cfset session.pipeline_access_level = 3>

<cfelse>

	 <!--- Check for user Access --->

	<cfquery name="user_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_access from sharing
	 where  sharing_pipeline_id = #session.pipeline_id# and
			sharing_hub_id = #session.hub# and
			sharing_to_usr_id = #session.usr_id#
	</cfquery>

	<cfif user_access.recordcount is 1>

	 	<cfset pipeline_access = 1>
	 	<cfset session.pipeline_access_level = #user_access.sharing_access#>

    </cfif>

</cfif>

<cfif pipeline_access is 0>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

