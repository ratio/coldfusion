<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<style>
.profile_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

	  <center>
	  <div class="main_box">

		  <cfquery name="people" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from follow
		   join usr on usr_id = follow_by_usr_id
		   where follow_company_id = #session.company_id# and
		         follow_hub_id = #session.hub#
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Followers <cfif people.recordcount GT 0>(<cfoutput>#people.recordcount#</cfoutput>)</cfif></td><td align=right class="feed_option"></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

          <cfif people.recordcount is 0>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">You have no followers yet.</td></tr>
			</table>

          <cfelse>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td height=20></td></tr>
			</table>

            <cfoutput query="followers">

			<div class="profile_badge">
			 <cfinclude template="/exchange/marketplace/people/badge.cfm">
			</div>

	   </cfoutput>

		  </cfif>

	  </div>
	  </center>

      </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

