
<!--- Auto Logout --->

<cfif isdefined("session.hub")>

	 <cfquery name="logout" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select hub_logout_page from hub
	  where hub_id = #session.hub#
	 </cfquery>

	 <cfif logout.hub_logout_page is "">

		<script language="javascript" type="text/javascript">
		 setTimeout('window.location.href=\"/index.cfm"',3600000);
		</script>

	 <cfelse>

		 <script language="javascript" type="text/javascript">
		  setTimeout('window.location.href=\"<cfoutput>#logout.hub_logout_page#</cfoutput>"',3600000);
		 </script>

	 </cfif>

<cfelse>

	<script language="javascript" type="text/javascript">
	 setTimeout('window.location.href=\"/index.cfm"',3600000);
	</script>

</cfif>

<cfinclude template="ip_check.cfm">

<cfif isdefined("app_id")>
  <cfinclude template="app_check.cfm">
</cfif>

<cfif not isdefined("session.usr_id")>
 <cflocation URL="/" addtoken="no">
<cfelse>

 	 <cfif left(cgi.script_name,40) is "/exchange/marketplace/communities/admin/">

      <cfif not isdefined("session.community_id")>

       <cflocation URL="/exchange/" addtoken="no">

      <cfelse>

		 <cfquery name="community_admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from comm_xref
		   where comm_xref_usr_id = #session.usr_id# and
				 comm_xref_comm_id = #session.community_id# and
				 comm_xref_usr_role = 1
		  </cfquery>

		  <cfif community_admin.recordcount is 0>
		   <cflocation URL="/exchange/" addtoken="no">
		  </cfif>

      </cfif>

 	 <cfelseif left(cgi.script_name,16) is "/exchange/admin/">

		<cfquery name="admin" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from hub_xref
		 join usr on usr_id = hub_xref_usr_id
		 where hub_xref_usr_id = #session.usr_id# and
			   hub_xref_hub_id = #session.hub#
		</cfquery>

		<cfif admin.hub_xref_usr_role is 1>
		 <cfset sadmin = 1>
		<cfelse>
		 <cfset sadmin = 0>
		</cfif>

		 <cfif sadmin is 1>

		  <cfelse>

			 <cfquery name="log_insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into log
			  (log_hub_id, log_usr_id, log_company_id, log_date,log_page,log_ip)
			  values(null,#session.usr_id#,<cfif isdefined("session.company_id")>#session.company_id#<cfelse>null</cfif>,#now()#,'Unauthorized Access to Hub Admin','#cgi.remote_addr#')
			 </cfquery>

			 <cflocation URL="/exchange/" addtoken="no">

		  </cfif>

	 </cfif>

</cfif>

