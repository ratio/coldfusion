<cfif not isdefined("form_token")>
 <cfinclude template="/exchange/error_msg.cfm">
 <cfabort>
</cfif>

<cfif #csrfverifytoken(form_token)# is "Yes">
<cfelse>
 <cfinclude template="/exchange/error_msg.cfm">
 <cfabort>
</cfif>

