<!--- Get App ID --->

<cfquery name="get_app" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select app_id from app
  where app_security_id = '#app_id#'
</cfquery>

<!--- Check to see if the App is installed --->

<cfquery name="hub_app" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select hub_app_id, hub_app_status_id, hub_app_app_id from hub_app
  where hub_app_app_id = #get_app.app_id# and
        hub_app_hub_id = #session.hub#
</cfquery>

<cfif hub_app.recordcount is 1>

 <cfif hub_app.hub_app_status_id is 1>

    <!--- Go, run app.  --->

 <cfelse>

 	<cflocation URL="/exchange/security/app.cfm?i=#encrypt(get_app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&s=s" addtoken="no">

 </cfif>

<cfelse>

    <cflocation URL="/exchange/security/app.cfm?i=#encrypt(get_app.app_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&s=n" addtoken="no">

</cfif>



