<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="app_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from app
 join app_category on app_category.app_category_id = app.app_category_id
 where app_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

	      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">App Access Problem</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right>
	              </td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif s is "s">

             <!--- Stopped App --->

             <tr><td class="feed_sub_header" style="color: red;">This App has been stopped by the Administrator of this Exchange.   To turn this App back on please contact the Exchange Administrator.</td></tr>

             <cfelseif s is "n">

             <!--- App Not Installed --->

             <tr><td class="feed_sub_header" style="color: red;">This App has not been installed in this Exchange.  To install this App please contact your Exchange Administator.</td></tr>

             </cfif>

             <tr><td height=10></td></tr>

             <tr><td><hr></td></tr>
             <tr><td height=20></td></tr>

          </table>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=140>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr><td align=center>

      			 <cfif #app_info.app_image# is "">
	  			  <img src="#image_virtual#/app.png" height=110 width=110 border=0>
	  			 <cfelse>
	  			  <img style="border-radius: 2px;" vspace=15 src="#image_virtual#/#app_info.app_image#" height=110 width=110 border=0>
			     </cfif>

			     </td></tr>

			  </table>

           </td><td width=30>&nbsp;</td><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
				  <tr><td class="feed_header">#app_info.app_name#</td>

				  <td align=right>

				  <cfif app_info.app_new is 1>
				    <img src="#image_virtual#/app_new.png" height=30>
			      </cfif>

			      <cfif app_info.app_status is 1>
				    <img src="#image_virtual#/app_planned.png" hspace=10 height=30>
				  <cfelseif app_info.app_status is 2>
				    <img src="#image_virtual#/app_alpha.png" hspace=10 height=30>
				  <cfelseif app_info.app_status is 3>
				    <img src="#image_virtual#/app_beta.png" hspace=10 height=30>
				  <cfelseif app_info.app_status is 4>
				    <img src="#image_virtual#/app_released.png" hspace=10 height=30>
                  </cfif>

			     </td></tr>
			  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				  <tr><td class="feed_sub_header" style="color: gray;" colspan=2>#app_info.app_category_name#</td></tr>
				  <cfif trim(app_info.app_desc_long) is "">
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">No Description Provided</td></tr>
                  <cfelse>
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">#replace(app_info.app_desc_long,"#chr(10)#","<br>","all")#</td></tr>
				  </cfif>
                  <tr><td><hr></td></tr>

				  <tr><td class="feed_sub_header">App Features & Capabilites</td></tr>

				  <cfif trim(app_info.app_desc_details) is "">
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">No Features or Capabilites Provided</td></tr>
                  <cfelse>
					<tr><td class="feed_sub_header" style="font-weight: normal;">#replace(app_info.app_desc_details,"#chr(10)#","<br>","all")#</td></tr>
				  </cfif>

                  <tr><td><hr></td></tr>

				  <tr><td class="feed_sub_header">App Pricing</td></tr>

				  <cfif trim(app_info.app_pricing) is "">
				  	<tr><td class="feed_sub_header" style="font-weight: normal;">No Pricing Information Provided</td></tr>
                  <cfelse>
				    <tr><td class="feed_sub_header" style="font-weight: normal;">#replace(app_info.app_pricing,"#chr(10)#","<br>","all")#</td></tr>
				  </cfif>

			  </table>

			  </td></tr>

		  </table>

          </cfoutput>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>