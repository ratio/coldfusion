<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfif t is "c">

<cfquery name="cinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

</cfif>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">Ask Question</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<cfoutput>
	<table cellspacing=0 cellpadding=0 border=0 width=100%>

     <tr><td height=10></td></tr>
	 <cfif t is "c">
       <tr><td class="feed_header" style="padding-bottom: 0px;">Challenge</td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">#cinfo.challenge_name#</td></tr>

	 </cfif>

	</table>
</cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="save_question.cfm" method="post">

 <cfoutput>

	 <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">From</td></tr>
	 <tr><td class="feed_sub_header" style="font-weight: normal;">#user.usr_first_name# #user.usr_last_name# (#tostring(tobinary(user.usr_email))#)</td></tr>

	 <tr><td class="feed_sub_header" valign=top>Subject</td></tr>
	 <tr><td><input type="text" name="question_subject" style="width: 900px;" class="input_text" maxlength="1000"></td></tr>

	 <tr><td class="feed_sub_header" valign=top>Question</td></tr>
	 <tr><td class="feed_option"><textarea name="question_text" style="width: 900px; height: 200px;" class="input_textarea"></textarea></td></tr>
     <tr><td height=10></td></tr>
	 <tr><td><input type="submit" name="button" class="button_blue_large" value="Submit Question"></td></tr>

     <input type="hidden" name="i" value=#i#>
     <input type="hidden" name="t" value="#t#">

 </cfoutput>

</table>

</center>

</body>
</html>

