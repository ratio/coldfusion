<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.opp_recent")>
 <cfset #session.opp_recent# = 1>
</cfif>

<cfif not isdefined("session.opp_advanced")>
 <cfset #session.opp_advanced# = 1>
</cfif>

<cfset session.view = 1>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<cfset from = dateadd('d',-2,now())>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="saved_search" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_search
 where opp_search_id = #opp_search_id# and
       opp_search_usr_id = #session.usr_id#
</cfquery>

<cfquery name="opp_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select count(distinct(fbo_solicitation_number)) as total from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics_code
   where fbo_id > 0

   <cfif #saved_search.opp_search_keyword# is not "">
	and contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_agency, fbo_naics_code, fbo_contract_award_name),'"#saved_search.opp_search_keyword#"')
   </cfif>

   <cfif saved_search.opp_search_posted_from is "" and saved_search.opp_search_posted_to is "">
   <cfelse>
    <cfif saved_search.opp_search_posted_from is "" and saved_search.opp_search_posted_to is not "">
     and fbo_pub_date_updated <= '#saved_search.opp_search_posted_to#'
    <cfelseif saved_search.opp_search_posted_from is not "" and saved_search.opp_search_posted_to is "">
     and fbo_pub_date_updated >= '#saved_search.opp_search_posted_from#'
    <cfelse>
     and (fbo_pub_date_updated >= '#saved_search.opp_search_posted_from#' and fbo_pub_date_updated <= '#saved_search.opp_search_posted_to#')
    </cfif>
   </cfif>

   <cfif saved_search.opp_search_type is not 0>
    and fbo_notice_type = '#saved_search.opp_search_type#'
   </cfif>

   <cfif saved_search.opp_search_status is 2>
	and fbo_inactive_date_updated > #now()#
   <cfelseif saved_search.opp_search_status is 3>
	and fbo_inactive_date_updated < #now()#
   </cfif>

   <cfif saved_search.opp_search_dept is not 0>
    and fbo_agency = '#saved_search.opp_search_dept#'
   </cfif>

   <cfif saved_search.opp_search_setaside is not 0>
    and fbo_setaside_original = '#saved_search.opp_search_setaside#'
   </cfif>

   <cfif #listlen(saved_search.opp_search_naics)# GT 0>
		<cfif #listlen(saved_search.opp_search_naics)# GT 1>
		<cfset ncounter = 1>
		and (
         <cfloop index="nc" list="#saved_search.opp_search_naics#">
           (fbo_naics_code = '#nc#')
           <cfif ncounter LT #listlen(saved_search.opp_search_naics)#> or</cfif>
           <cfset ncounter = ncounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_naics_code = '#saved_search.opp_search_naics#'
		</cfif>
    </cfif>

   <cfif #listlen(saved_search.opp_search_psc)# GT 0>
		<cfif #listlen(saved_search.opp_search_psc)# GT 1>
		<cfset pcounter = 1>
		and (
         <cfloop index="pc" list="#saved_search.opp_search_psc#">
           (fbo_class_code = '#pc#')
           <cfif pcounter LT #listlen(saved_search.opp_search_psc)#> or</cfif>
           <cfset pcounter = pcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_class_code = '#saved_search.opp_search_psc#'
		</cfif>
    </cfif>

  </cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select fbo_id, fbo_solicitation_number, orgname_logo, fbo_pub_date, fbo_dept, fbo_agency, fbo_office, fbo_class_code, class_code_name, fbo_notice_type, fbo_naics_code, fbo_opp_name, fbo_setaside_original from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics_code
   left join orgname on orgname_name = fbo_agency
   where fbo_id > 0

   <cfif #saved_search.opp_search_keyword# is not "">
	and contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_agency, fbo_naics_code, fbo_contract_award_name),'"#saved_search.opp_search_keyword#"')
   </cfif>

   <cfif saved_search.opp_search_posted_from is "" and saved_search.opp_search_posted_to is "">
   <cfelse>
    <cfif saved_search.opp_search_posted_from is "" and saved_search.opp_search_posted_to is not "">
     and fbo_pub_date <= '#saved_search.opp_search_posted_to#'
    <cfelseif saved_search.opp_search_posted_from is not "" and saved_search.opp_search_posted_to is "">
     and fbo_pub_date >= '#saved_search.opp_search_posted_from#'
    <cfelse>
     and (fbo_pub_date >= '#saved_search.opp_search_posted_from#' and fbo_pub_date <= '#saved_search.opp_search_posted_to#')
    </cfif>
   </cfif>

   <cfif saved_search.opp_search_type is not 0>
    and fbo_notice_type = '#saved_search.opp_search_type#'
   </cfif>

   <cfif saved_search.opp_search_status is 2>
	and fbo_inactive_date_updated > #now()#
   <cfelseif saved_search.opp_search_status is 3>
	and fbo_inactive_date_updated < #now()#
   </cfif>

   <cfif saved_search.opp_search_dept is not 0>
    and fbo_agency = '#saved_search.opp_search_dept#'
   </cfif>

   <cfif saved_search.opp_search_setaside is not 0>
    and fbo_setaside_original = '#saved_search.opp_search_setaside#'
   </cfif>

   <cfif #listlen(saved_search.opp_search_naics)# GT 0>
		<cfif #listlen(saved_search.opp_search_naics)# GT 1>
		<cfset ncounter = 1>
		and (
         <cfloop index="nc" list="#saved_search.opp_search_naics#">
           (fbo_naics_code = '#nc#')
           <cfif ncounter LT #listlen(saved_search.opp_search_naics)#> or</cfif>
           <cfset ncounter = ncounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_naics_code = '#saved_search.opp_search_naics#'
		</cfif>
    </cfif>

   <cfif #listlen(saved_search.opp_search_psc)# GT 0>
		<cfif #listlen(saved_search.opp_search_psc)# GT 1>
		<cfset pcounter = 1>
		and (
         <cfloop index="pc" list="#saved_search.opp_search_psc#">
           (fbo_class_code = '#pc#')
           <cfif pcounter LT #listlen(saved_search.opp_search_psc)#> or</cfif>
           <cfset pcounter = pcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_class_code = '#saved_search.opp_search_psc#'
		</cfif>
    </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by fbo_agency ASC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 10>
		     order by fbo_agency DESC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 2>
		     order by fbo_opp_name ASC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 20>
		     order by fbo_opp_name DESC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 3>
		     order by class_code_name ASC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 30>
		     order by class_code_name DESC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 4>
		     order by fbo_notice_type ASC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 40>
		     order by fbo_notice_type DESC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 5>
		     order by fbo_naics_code ASC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 50>
		     order by fbo_naics_code DESC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 6>
		     order by fbo_setaside_original ASC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 60>
		     order by fbo_setaside_original DESC, fbo_pub_date_updated DESC
		    <cfelseif #sv# is 7>
		     order by fbo_pub_date ASC
		    <cfelseif #sv# is 70>
		     order by fbo_solicitation_number DESC
		    <cfelseif #sv# is 8>
		     order by fbo_solicitation_number ASC
		    <cfelseif #sv# is 80>
		     order by fbo_pub_date DESC
		    </cfif>
		   <cfelse>
            order by fbo_pub_date DESC
		   </cfif>

  </cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="/exchange/opps/saved_searches.cfm">
      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
			 <tr><td class="feed_header">Saved Search:  #saved_search.opp_search_name#</td>
				 <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>


			 <cfif saved_search.opp_search_desc is not "">
			 	<tr><td class="feed_sub_header" style="font-weight: normal;">#saved_search.opp_search_desc#</td></tr>
			 </cfif>

           <tr><td colspan=2><hr></td></tr>

		   <tr><td class="feed_sub_header">

			<cfoutput>
				<cfif agencies.recordcount GT #perpage#>
					<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
						<a href="#link#&opp_search_id=#opp_search_id#&sv=#sv#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=20></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt agencies.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
						<a href="#link#&opp_search_id=#opp_search_id#&sv=#sv#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=20></a>
					<cfelse>
					</cfif>
				</cfif>
			</cfoutput>

		   </td>

				 <td class="feed_sub_header" align=right>

				 <a href="/exchange/opps/go_expanded.cfm?opp_search_id=#opp_search_id#"><img src="/images/icon_expand.png" hspace=5 width=20 alt="Expanded View" title="Expanded View"></a>
				 <a href="/exchange/opps/go_expanded.cfm?opp_search_id=#opp_search_id#">Expand Results</a>
				 &nbsp;&nbsp;
				 <a href="/exchange/opps/edit_search.cfm?opp_search_id=#opp_search_id#&l=1"><img src="/images/icon_edit.png" width=20 hspace=5 alt="Configure Saved Search" title="Configure Saved Search"></a>
				 <a href="/exchange/opps/edit_search.cfm?opp_search_id=#opp_search_id#&l=1">Edit Search</a>

                 <cfif agencies.recordcount GT 0>

				 &nbsp;&nbsp;
				 <a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&export=1"><img src="/images/icon_export_excel.png" hspace=5 width=20 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&export=1">Export to Excel</a>

				 </cfif>

				</td>

		   </tr>

				</cfoutput>
         <tr><td height=20></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" colspan=3><font color="green"><b>Search has been successfully created.</b></font></td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_sub_header" colspan=3><font color="green"><b>Search has been successfully updated.</b></font></td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

       </table>

      <cfif agencies.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header">No opportunities were found.</td></tr>
        </table>

      <cfelse>

      <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td></td>
            <td class="feed_option" width=200><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>ORGANIZATION</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>

            <td class="feed_option"><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>SOLICITATION ##</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>


            <td class="feed_option"><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>NAME / TITLE</b></a><cfif isdefined("sv") and sv is 2>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option" width=50><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>TYPE</b></a><cfif isdefined("sv") and sv is 4>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option" align=center><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>SET ASIDE</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option" width=110 align=right><a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>DATE POSTED</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td>&nbsp;</td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

		     <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #orgname_logo# is ""><img src="#image_virtual#icon_usa.png" valign=top align=top width=40 border=0 vspace=10><cfelse><img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10></cfif></a></td>

             <td class="text_xsmall" valign=middle width=300><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(fbo_dept)#</b></a><br>#fbo_agency#<br>#fbo_office#</td>
             <td class="text_xsmall" width=100><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#fbo_solicitation_number#</a></td>
             <td class="text_xsmall"><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#fbo_opp_name#</a></td>
             <td class="text_xsmall" width=200>#class_code_name#&nbsp;</td>
             <td class="text_xsmall" width=75>#fbo_notice_type#</td>
             <td class="text_xsmall" align=center width=75>#fbo_naics_code#</td>
             <td class="text_xsmall" width=120><cfif #fbo_setaside_original# is "">No<cfelse>#fbo_setaside_original#</cfif></td>
             <td class="text_xsmall" align=right width=120>#dateformat(fbo_pub_date,'mm/dd/yyyy')#</td>

             <td align=right width=40>
			  <img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			 </td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>
        </table>

        </cfif>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>