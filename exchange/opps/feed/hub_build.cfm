<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif isdefined("session.company_id")>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><cfoutput>#hub.hub_name#</cfoutput> FEED Builder</td><td align=right class="feed_option"><a href="/exchange/">Return</a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td class="feed_option">The FEED Builder allows you to create and configure the types of Feeds you will receive.</td></tr>
           <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td colspan=3 class="feed_header">Federal Business Opportunities (FBO.Gov)</td></tr>
            <tr><td colspan=3 class="feed_option">FBO.Gov is a Federal Government portal that posts and maintains solicitations that are issued to industry greater than $25,000.  The EXCHANGE pulls this information daily.</td></tr>
            <tr><td height=20></td></tr>

			<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 1 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_name
			</cfquery>

			<tr><td valign=top width=33%>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td width=30><a href="fbo_add.cfm"><img src="/images/plus3.png" width=15 alt="Create Opportunity Feed" title="Create Opportunity Feed" border=0></a></td><td class="feed_sub_header">Opportunity Feeds</td></tr>

            <cfif fbo.recordcount is 0>
              <tr><td></td><td class="feed_option" colspan=3>You have no opportunity feeds created.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <tr><td height=10></td></tr>
              <cfoutput query="fbo">
                <tr><td></td><td class="feed_option"><a href="fbo_edit.cfm?usr_feed_id=#usr_feed_id#"><b>#usr_feed_name#</b></a></td></tr>
                <tr><td></td><td class="feed_option">#usr_feed_desc#</td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td><td valign=top width=33%>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 2 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_name
			</cfquery>

            <tr><td width=30><a href="award_add.cfm"><img src="/images/plus3.png" width=15 alt="Create Award Notification Feed" title="Create Award Notification Feed" border=0></a></td><td class="feed_sub_header">Award Notification Feeds</td></tr>

            <cfif awards.recordcount is 0>
              <tr><td></td><td class="feed_option" colspan=3>You have no award feeds created.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <tr><td height=10></td></tr>
              <cfoutput query="awards">
                <tr><td></td><td class="feed_option"><a href="award_edit.cfm?usr_feed_id=#usr_feed_id#"><b>#usr_feed_name#</b></a></td></tr>
                <tr><td></td><td class="feed_option">#usr_feed_desc#</td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td><td valign=top>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="sol" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 3 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_solicitation_number
			</cfquery>

            <tr><td width=30><a href="sol_add.cfm"><img src="/images/plus3.png" width=15 alt="Add Solicitation Number" title="Add Solicitation Number" border=0></a></td><td class="feed_sub_header">Solicitation Tracker</td></tr>

            <cfif sol.recordcount is 0>
              <tr><td></td><td class="feed_option" colspan=3>You are not tracking any solicitations.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <tr><td height=10></td></tr>
              <cfoutput query="sol">
                <tr><td></td><td class="feed_option"><a href="sol_edit.cfm?usr_feed_id=#usr_feed_id#"><b>#usr_feed_solicitation_number#</b></a></td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td></tr>

          <tr><td colspan=3><hr></td></tr>

			<cfquery name="market" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select market_name from market, align
			  where (market.market_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 2)
					order by market.market_name
			</cfquery>

			<cfset market_list = #valuelist(market.market_name)#>

			<cfquery name="sector" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select sector_name from sector, align
			  where (sector.sector_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 3)
					order by sector.sector_name
			</cfquery>

			<cfset sector_list = #valuelist(sector.sector_name)#>

			<cfquery name="topic" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select topic_name from topic, align
			  where (topic.topic_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 4)
					order by topic.topic_name
			</cfquery>

			<cfset topic_list = #valuelist(topic.topic_name)#>

            <tr><td height=10></td></tr>
            <tr><td colspan=3 class="feed_header">Opportunity Alignments</td></tr>
            <tr><td colspan=3 class="feed_option">For opportunities that are not aligned to Federal standards, the EXCHANGE posts opportunities based on your Market, Sector, and Capability & Service alignments.</td></tr>
            <tr><td height=20></td></tr>


                    <tr>
                        <td class="feed_sub_header"><a href="alignments.cfm"><img src="/images/pencil.png" alt="Edit Alignments" title="Edit Alignments" border=0 width=15></a>&nbsp;&nbsp;<b>Markets</b></td>
                        <td class="feed_sub_header"><a href="alignments.cfm"><img src="/images/pencil.png" alt="Edit Alignments" title="Edit Alignments" border=0 width=15></a>&nbsp;&nbsp;<b>Sectors</b></td>
                        <td class="feed_sub_header"><a href="alignments.cfm"><img src="/images/pencil.png" alt="Edit Alignments" title="Edit Alignments" border=0 width=15></a>&nbsp;&nbsp;<b>Capabilites & Services</b></td>

                    <tr>

                    <td class="feed_option" valign=top>

                    <cfloop index="melement" list="#market_list#">
                     <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#melement#<br></cfoutput>
                    </cfloop>

                    </td>

                    <td class="feed_option" valign=top>

                    <cfloop index="selement" list="#sector_list#">
                     <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#selement#<br></cfoutput>
                    </cfloop>

                    </td>

                    <td class="feed_option" valign=top>

                    <cfloop index="telement" list="#topic_list#">
                     <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#telement#<br></cfoutput>
                    </cfloop>

                    </td></tr>

                    <tr><td colspan=3><hr></td></tr>

					<tr><td height=10></td></tr>
					<tr><td colspan=3 class="feed_header">Opportunity Categories</td></tr>
					<tr><td colspan=3 class="feed_option">Opportunity categories allow you to filter your feeds to the types of opportunities you want to receive.</td></tr>

					<cfquery name="lens" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					  select lens_name, lens_desc from lens, align
					  where (lens.lens_id = align.align_type_value) and
							(align.align_usr_id = #session.usr_id#) and
							(align.align_type_id = 1)
							order by lens.lens_name
					</cfquery>

					<tr><td colspan=3>

		   		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   		   <tr><td>&nbsp;</td></tr>
		   		   <tr><td width=30><a href="categories.cfm"><img src="/images/pencil.png" width=15 border=0 title="Update Categories" alt="Update Categories"></td>
		   		       <td class="feed_sub_header">Category</td>
		   		       <td class="feed_sub_header">Description and Value to You</td></tr>
		   		   <tr><td height=5></td></tr>
		   		   <cfset counter = 0>
                   <cfoutput query="lens">
                     <tr><td></td>
                         <td <cfif counter is 0>bgcolor="ffffff"<cfelse>bgcolor="e0e0e0"</cfif> class="feed_option" valign=top>#lens_name#</td>
                         <td <cfif counter is 0>bgcolor="ffffff"<cfelse>bgcolor="e0e0e0"</cfif> class="feed_option" valign=top>#lens_desc#</td>
                     </tr>
                   <cfif counter is 0>
                    <cfset counter = 1>
                   <cfelse>
                    <cfset counter = 0>
                   </cfif>

                   </cfoutput>

                   </table>

                   </td></tr>

</table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

