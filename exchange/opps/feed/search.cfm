<cfif not isdefined("button")>
 <cfset session.feed_time = #time#>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfif #button# is "Search">

 <cfset session.search_type = #search_type#>

 <cfif search_type is 1>
  <cfset session.feed_keyword = #keyword#>
  <cflocation URL="index.cfm" addtoken="no">
 <cfelse>
  <cfset #session.opp_keyword# = #keyword#>
  <cfset #session.location# = "Feeds">
  <cflocation URL="/exchange/opps/finder/results.cfm" addtoken="no">
 </cfif>

<cfelseif #button# is "Reset">
 <cfset StructDelete(Session,"feed_time")>
 <cfset StructDelete(Session,"feed_lens")>
 <cfset StructDelete(Session,"feed_keyword")>
</cfif>

<cflocation URL="index.cfm" addtoken="no">