<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif isdefined("session.company_id")>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="share_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Share Feed</td><td class="feed_option" align=right>

           <cfif isdefined("l")>
            <a href="/exchange/opps/saved/">Return</a>
           <cfelse>
            <a href="/exchange/">Return</a>
           </cfif>
           </td></tr>
           <tr><td height=10></td></tr>
		  </table>

		  <form action="share_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_option" width=100><b>Email Address</b></td>
                 <td class="feed_option"><input type="email" name="email" size=30 required></td>
             </tr>

             <tr>
                 <td class="feed_option"><b>Subject</b></td>
                 <td class="feed_option"><input type="text" name="subject" size=50 required></td>
             </tr>

             <tr>
                 <td class="feed_option" valign=top><b>Message</b></td>
                 <td class="feed_option"><textarea name="message" cols=100 rows=8></textarea></td>
             </tr>

			 <tr><td></td><td><input class="button_blue" type="submit" name="button" value="Share" vspace=10></td></tr>

             <tr><td height=10></td></tr>

		     <cfoutput>
		     <input type="hidden" name="feed_id" value=#feed_id#>
		     <cfif isdefined("l")>
		      <input type="hidden" name="l" value=1>
		     </cfif>
		     </cfoutput>

            </form>

          </table>

		  <cfquery name="feed" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select * from feed
		   where feed_id = #feed_id#
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Feed Details</td></tr>
           <tr><td height=10></td></tr>

		   <cfoutput>

           <cfif feed.feed_organization is not "">
		   <tr><td class="feed_option"><b>#feed.feed_organization#</b></td></tr>
		   <tr><td height=20></td></tr>
		   </cfif>

		   <tr><td class="feed_option"><b>#feed.feed_title#</b></td></tr>

		   <tr><td class="feed_option">#feed.feed_desc#</td></tr>

           <cfif #feed.feed_url# is not "">
           <tr><td height=20></td></tr>
		   <tr><td class="feed_option"><b>URL Reference</b>: <a href="#feed.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#feed.feed_url#</a></td></tr>
		   </cfif>

		   <tr><td height=5></td></tr>
		   <tr><td class="feed_option">#feed.feed_desc_long#</td></tr>

		   </cfoutput>

		  </table>


          </form>

          </td></tr>

        </table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

