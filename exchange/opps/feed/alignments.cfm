<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif isdefined("session.company_id")>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Update Feed Alignments</td><td align=right class="feed_option"><a href="/exchange/feed/build.cfm">Return</a></td></tr>
           <tr><td class="feed_option">Alignments allow you to filter opportunities in the Feed to your market, sector, and capabilities & services preferences.</td></tr>
           <tr><td height=20></td></tr>
		  </table>

	 <cfquery name="market" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select * from market
	   <cfif isdefined("session.hub")>
	    where market_hub_id = #session.hub#
	   <cfelse>
	    where market_hub_id is null
	   </cfif>
	   order by market_name
	  </cfquery>

	  <cfquery name="sector" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select * from sector
	   <cfif isdefined("session.hub")>
	    where sector_hub_id = #session.hub#
	   <cfelse>
	    where sector_hub_id is null
	   </cfif>
	   order by sector_name
	  </cfquery>

	  <cfquery name="topic" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select * from topic
	   <cfif isdefined("session.hub")>
	    where topic_hub_id = #session.hub#
	   <cfelse>
	    where topic_hub_id is null
	   </cfif>
	   order by topic_name
	  </cfquery>

	  <cfquery name="market_alignment" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          align_hub_id = #session.hub# and
	         </cfif>
			 (align_type_id = 2)
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          align_hub_id = #session.hub# and
	         </cfif>
			 (align_type_id = 3)
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
	         <cfif isdefined("session.hub")>
	          align_hub_id = #session.hub# and
	         </cfif>
			 (align_type_id = 4)
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td colspan=2>

			<table cellspacing=0 cellpadding=0 border=0>

			<tr><td><b>Markets</b></td>
				<td><b>Sectors</b></td>
				<td><b>Capabilities & Services</b></td>
				</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>

				 <td width=180>
					  <select style="font-size: 12px; width: 160px; height: 260px; padding-left: 10px; padding-top: 5px;" name="market_id" multiple required>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=220>
					  <select style="font-size: 12px; width: 200px; height: 260px; padding-left: 10px; padding-top: 5px;" name="sector_id" multiple required>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select style="font-size: 12px; width: 275px; height: 260px; padding-left: 10px; padding-top: 5px;" name="topic_id" multiple required>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
                <tr><td class="feed_option" colspan=2>To select multiple items please use the SHIFT or CTRL key.</td></tr>

				<tr><td height=15></td></tr>
				<tr><td><input class="button_blue" style="font-size: 11px; height: 23px; width: 125px;" type="submit" name="button" value="Update Alignments"></td></tr>

 			  </table>

 		  </form>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

