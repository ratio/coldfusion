<cfinclude template="/exchange/security/check.cfm">

<cfquery name="feed" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from feed
 where feed_id = #feed_id#
</cfquery>

<cfquery name="usr" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfif isdefined("session.hub")>
	<cfquery name="hub_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>
</cfif>

<cfmail from="#usr.usr_first_name# #usr.usr_last_name# <noreply@ratio.exchange>"
		  to="#email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="#subject#">
<html>
<head>
<title>Opportunity</title>
</head><div class="center">
<body class="body">
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">

 <cfoutput>

   <tr><td class="feed_option"><b>#usr.usr_first_name# #usr.usr_last_name# has shared an opportunity with you on <cfif isdefined("session.hub")>#hub_info.hub_name#<cfelse>The EXCHANGE</cfif>.</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td class="feed_option">

        #message#

       </td></tr>

   <tr><td>&nbsp;</td></tr>
   <tr><td><hr></td></tr>

   <cfif feed.feed_organization is not "">
   <tr><td class="feed_option"><b>#feed.feed_organization#</b></td></tr>
   <tr><td height=20></td></tr>
   </cfif>

   <tr><td class="feed_option"><b>#feed.feed_title#</b></td></tr>

   <tr><td class="feed_option">#feed.feed_desc#</td></tr>

   <cfif #feed.feed_url# is not "">
   <tr><td height=20></td></tr>
   <tr><td class="feed_option"><b>URL Reference</b>: <a href="#feed.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#feed.feed_url#</a></td></tr>
   </cfif>

   <tr><td height=5></td></tr>
   <tr><td class="feed_option">#feed.feed_desc_long#</td></tr>

   <tr><td>&nbsp;</td></tr>
   <tr><td><hr></td></tr>

   <tr><td class="feed_option">This share was enabled through the <a href="https://www.ratio.exchange">RATIO EXCHANGE</a> which is dedicated to helping companies and organization acceleate growth.</td></tr>
   <tr><td class="feed_option">To find out more information about the EXCHANGE, visit https://www.ratio.exchange today.</td></tr>

 </cfoutput>
</table>

</body>
</html>
</cfmail>

<cfif isdefined("l")>
	<cflocation URL="/exchange/opps/saved/index.cfm?u=2" addtoken="no">
<cfelse>
	<cflocation URL="/exchange/index.cfm?u=3" addtoken="no">
</cfif>