<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif session.company_id is not 0>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Setup Feeds</td><td align=right class="feed_option"><a href="/exchange/">Return</a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td class="feed_option">Create the Feeds that you want the EXCHANGE to deliver to you.</td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header"><a href="build.cfm">General Alignments</a>&nbsp;|&nbsp;<a href="fbo_builder.cfm">FBO.Gov Feeds</a></td></tr>
           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="market" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select market_name from market, align
			  where (market.market_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 2)
					<cfif isdefined("session.hub")>
					 and (align.align_hub_id = #session.hub#)
					<cfelse>
					 and (align.align_hub_id is null)
					</cfif>
					order by market.market_name
			</cfquery>

			<cfset market_list = #valuelist(market.market_name)#>

			<cfquery name="sector" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select sector_name from sector, align
			  where (sector.sector_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 3)
					<cfif isdefined("session.hub")>
					 and (align.align_hub_id = #session.hub#)
					<cfelse>
					 and (align.align_hub_id is null)
					</cfif>
					order by sector.sector_name
			</cfquery>

			<cfset sector_list = #valuelist(sector.sector_name)#>

			<cfquery name="topic" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select topic_name from topic, align
			  where (topic.topic_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 4)
					<cfif isdefined("session.hub")>
					 and (align.align_hub_id = #session.hub#)
					<cfelse>
					 and (align.align_hub_id is null)
					</cfif>
					order by topic.topic_name
			</cfquery>

			<cfset topic_list = #valuelist(topic.topic_name)#>

            <tr><td height=10></td></tr>
            <tr><td colspan=3 class="feed_header">Feed Alignments</td></tr>
            <tr><td colspan=3 class="feed_option">Please select the Markets, Sectors, and Capability & Services that are best aligned to your company.</td></tr>
            <tr><td height=20></td></tr>

                    <tr>
                        <td class="feed_sub_header"><a href="alignments.cfm"><img src="/images/pencil.png" alt="Edit Alignments" title="Edit Alignments" border=0 width=15></a>&nbsp;&nbsp;<b>Markets</b></td>
                        <td class="feed_sub_header"><a href="alignments.cfm"><img src="/images/pencil.png" alt="Edit Alignments" title="Edit Alignments" border=0 width=15></a>&nbsp;&nbsp;<b>Sectors</b></td>
                        <td class="feed_sub_header"><a href="alignments.cfm"><img src="/images/pencil.png" alt="Edit Alignments" title="Edit Alignments" border=0 width=15></a>&nbsp;&nbsp;<b>Capabilites & Services</b></td>
                    <tr>

                    <td class="feed_option" valign=top>

                    <cfloop index="melement" list="#market_list#">
                     <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#melement#<br></cfoutput>
                    </cfloop>

                    </td>

                    <td class="feed_option" valign=top>

                    <cfloop index="selement" list="#sector_list#">
                     <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#selement#<br></cfoutput>
                    </cfloop>

                    </td>

                    <td class="feed_option" valign=top>

                    <cfloop index="telement" list="#topic_list#">
                     <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#telement#<br></cfoutput>
                    </cfloop>

                    </td></tr>

                    <tr><td colspan=3><hr></td></tr>

					<tr><td height=10></td></tr>
					<tr><td colspan=3 class="feed_header">Feed Categories</td></tr>
					<tr><td colspan=3 class="feed_option">Feed categories allow you to specify the type of opportunities and information that will be sent to your Feed.</td></tr>

					<cfquery name="lens" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					  select lens_name, lens_desc from lens, align
					  where (lens.lens_id = align.align_type_value) and
							(align.align_usr_id = #session.usr_id#) and
							(align.align_type_id = 1)
			  			     order by lens.lens_name
					</cfquery>

					<tr><td colspan=3>

		   		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   		   <tr><td>&nbsp;</td></tr>
		   		   <tr><td width=25><a href="categories.cfm"><img src="/images/pencil.png" width=15 border=0 title="Update Categories" alt="Update Categories"></td>
		   		       <td class="feed_sub_header">Category</td>
		   		       <td class="feed_sub_header">Description</td></tr>
		   		   <tr><td height=5></td></tr>
		   		   <cfset counter = 0>
                   <cfoutput query="lens">
                     <tr><td></td>
                         <td width=200 <cfif counter is 0>bgcolor="ffffff"<cfelse>bgcolor="e0e0e0"</cfif> class="feed_option" valign=top>#lens_name#</td>
                         <td <cfif counter is 0>bgcolor="ffffff"<cfelse>bgcolor="e0e0e0"</cfif> class="feed_option" valign=top>#lens_desc#</td>
                     </tr>
                   <cfif counter is 0>
                    <cfset counter = 1>
                   <cfelse>
                    <cfset counter = 0>
                   </cfif>

                   </cfoutput>

                   </table>

                   </td></tr>

</table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

