<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Move to Portfolio">

	<cfquery name="interest" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert feed_interest
	 (feed_interest_feed_id, feed_interest_usr_id, feed_interest_decision, feed_interest_decision_date)
	 values
	 (#feed_id#, #session.usr_id#, 1, #now()#)
	</cfquery>

</cfif>

<cflocation URL="/exchange/" addtoken="no">
