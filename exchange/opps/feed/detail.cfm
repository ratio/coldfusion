<cfinclude template="/exchange/security/check.cfm">

<cfquery name="detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from feed
 where feed_id = #feed_id#
</cfquery>

<!-- Get Lookups -->

  <cfif #detail.feed_market_id# is not 0 and #detail.feed_market_id# is not "">
   <cfquery name="market" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select market_name from market
    where (market_id in (#detail.feed_market_id#))
   </cfquery>
   <cfset #market_list# = #valuelist(market.market_name)#>

  </cfif>

  <cfif #detail.feed_sector_id# is not 0 and #detail.feed_sector_id# is not "">

   <cfquery name="sector" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select sector_name from sector
    where (sector_id in (#detail.feed_sector_id#))
   </cfquery>
   <cfset #sector_list# = #valuelist(sector.sector_name)#>

  </cfif>

  <cfif #detail.feed_topic_id# is not 0 and #detail.feed_topic_id# is not "">

   <cfquery name="topic" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select topic_name from topic
    where (topic_id in (#detail.feed_topic_id#))
   </cfquery>
   <cfset #topic_list# = #valuelist(topic.topic_name)#>

  </cfif>

  <cfif #detail.feed_lens_id# is not 0 and #detail.feed_lens_id# is not "">

	   <cfquery name="lens" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select lens_name from lens
		where (lens_id in (#detail.feed_lens_id#))
	   </cfquery>

	   <cfset #lens_list# = #valuelist(lens.lens_name)#>

  </cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td colspan=4 class="feed_header">Feed Details</td>
                        <form action="move.cfm" method="post">
			            <td align=right><input class="button_blue" style="width: 125px; height: 25px" type="submit" name="button" value="Return to Feed">&nbsp;<input class="button_blue" style="width: 125px; height: 25px" type="submit" name="button" value="Move to Portfolio"></td>
                        <input type="hidden" name="feed_id" value=<cfoutput>#feed_id#</cfoutput>>
                        </form>

           </tr>
           <tr><td>&nbsp;<td></tr>
          </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td>&nbsp;</td></tr>

		<cfoutput>

        <tr><td><b>Title</b></td></tr>
        <tr><td class="feed_option">#detail.feed_title#</td></tr>
        <tr><td height=10>&nbsp;</td></tr>
        <tr><td><b>Organization</b></td></tr>
        <tr><td class="feed_option">#detail.feed_organization#</td></tr>
        <tr><td height=10>&nbsp;</td></tr>
        <tr><td colspan=5><b>Description</b></td></tr>
        <tr><td colspan=5 class="feed_option">#replace(detail.feed_short_desc,"#chr(10)#","<br>","all")#</td></tr>
        <tr><td>&nbsp;</td></tr>
    	<tr><td colspan=5><b>More Detail</b></td></tr>
       	<tr><td colspan=5 class="feed_option"><cfif #detail.feed_long_desc# is "">Not provided.<cfelse>#replace(detail.feed_long_desc,"#chr(10)#","<br>","all")#</cfif></td></tr>
        <tr><td>&nbsp;</td></tr>
        <cfif #detail.feed_recommendation# is not "">
			<tr><td colspan=5><b>Recommendation</b></td></tr>
			<tr><td colspan=5 class="feed_option"><cfif #detail.feed_recommendation# is "">Not provided.<cfelse>#replace(detail.feed_recommendation,"#chr(10)#","<br>","all")#</cfif></td></tr>
			<tr><td>&nbsp;</td></tr>
        </cfif>

        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td>

            <cfif #detail.feed_lens_id# is not "">

 			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td><b>Exchange Alignments Used</b></td></tr>
            <tr><td height=10>&nbsp;</td></tr>
  			<tr>
 			    <td width=20%><b>Accelerator(s)</b></td>
 			    <td width=20%><b>Markets</b></td>
 				<td width=20%><b>Sectors</b></td>
 				<td width=20%><b>Capabilities</b></td>
 				</tr>
 			<tr>

 				 <td class="feed_option" valign=top>

 				 <cfif #detail.feed_lens_id# is not 0>
 					 <cfloop index="lelement" list="#lens_list#">
 					   <cfoutput>#lelement#</cfoutput><br>
 					 </cfloop>
 				 <cfelse>
 					  All
 				 </cfif>
 				 </td>

 				 <td class="feed_option" valign=top>

 				 <cfif #detail.feed_market_id# is not 0>
 					 <cfloop index="melement" list="#market_list#">
 					   <cfoutput>#melement#</cfoutput><br>
 					 </cfloop>
 				 <cfelse>
 					  All
 				 </cfif>
 				 </td>

 				 <td class="feed_option" valign=top>

 				 <cfif #detail.feed_sector_id# is not 0>
 					 <cfloop index="selement" list="#sector_list#">
 					   <cfoutput>#selement#</cfoutput><br>
 					 </cfloop>
 				 <cfelse>
 					  All
 				 </cfif>
 				 </td>

 				 <td class="feed_option" valign=top>

 				 <cfif #detail.feed_topic_id# is not 0>
 					 <cfloop index="telement" list="#topic_list#">
 					   <cfoutput>#telement#</cfoutput><br>
 					 </cfloop>
 				 <cfelse>
 					  All
 				 </cfif>
 				 </td>
 			</tr>

 			</cfif>

			</table>

	    <tr><td>&nbsp;</td></tr>

        <cfif #detail.feed_naics# is not "">
        <tr><td><b>NAICS Code(s)</b></td></tr>
       	<tr><td class="feed_option"><cfif #detail.feed_naics# is not "">#detail.feed_naics#<cfelse>Not provided.</cfif></td></tr>
     	<tr><td height=10>&nbsp;</td></tr>
     	</cfif>

        <tr><td><b>Reference</b></td></tr>
       	<tr><td class="feed_option"><cfif #detail.feed_url# is not ""><a href="#detail.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#detail.feed_url#</a><cfelse>Not provided.</cfif></td></tr>
     	<tr><td height=10>&nbsp;</td></tr>

        <cfif #detail.feed_attachment# is not "">
			<tr><td><b>Attachment</b></td></tr>
			<tr><td class="feed_title"><font size=2><a href="#media_virtual#/#detail.feed_attachment#" target="_blank" rel="noopener" rel="noreferrer"><u>See Attachment</u></a></font></td></tr>
			<tr><td>&nbsp;</td></tr>
	    </cfif>

 		<tr><td><b>Date / Time Sent</b></td></tr>
    	<tr><td class="feed_option">#dateformat(detail.feed_updated,'mm/dd/yy')#, #timeformat(detail.feed_updated)#</td></tr>
    	<tr><td height=10>&nbsp;</td></tr>

        </cfoutput>


           </table>

     	   </td></tr>
	  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

