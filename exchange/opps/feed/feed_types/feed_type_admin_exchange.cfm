  <cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td height=10></td></tr>
	 <tr><td class="feed_option" width=80 valign=top>

	  <img style="border-radius: 150px;" src="/images/exchange_logo_black.png" width=65 border=0>

	 </td>
	 <td class="feed_sub_header" valign=top><font size=4>EXCHANGE</b></font><br><span class="text_xsmall">#feed.usr_first_name# #feed.usr_last_name#, #feed.usr_title#</span><br>

     <font size=2 color="gray">

			   <cfif #mdiff# LT 1>
				 < minute ago
			   <cfelseif #mdiff# GTE 1 and #mdiff# LT 2>
				#mdiff# minute ago
			   <cfelseif #mdiff# GTE 2 and #mdiff# LT 60>
				#mdiff# minutes ago
			   <cfelseif #mdiff# GT 60>
				<cfif #mdiff# LT 1440>
				 #round(evaluate(mdiff/60))# hours ago
				<cfelse>
				 <cfif #round(evaluate(mdiff/1440))# GT 1>
				   #round(evaluate(mdiff/1440))# days ago
				 <cfelse>
				   #round(evaluate(mdiff/1440))# days ago
				 </cfif>
				</cfif>
			   </cfif>
	 </b>
	 </font>

	 </td><td align=right valign=top width=80>

	   <a href="/exchange/feed/share.cfm?feed_id=#feed.feed_id#"><img src="/images/icon_share.png" height=15 valign=middle alt="Share with Others" title="Share with Others" hspace=2></a>
	   <a href="/exchange/feed/save.cfm?feed_id=#feed.feed_id#"><img src="/images/icon_star.png" height=15 valign=middle alt="Send to Portfolio" title="Send to Portfolio" border=0 hspace=2></a>
	   <a href="/exchange/feed/remove.cfm?feed_id=#feed.feed_id#" onclick="return confirm('Remove Feed?\r\nAre you sure you want to remove this opportunity from your Feed?');"><img src="/images/icon_trash.png" height=20 valign=middle alt="Not Interested" title="Not Interested"></a>

	 </td></tr>

	 <tr><td height=15></td></tr>
	 <tr><td colspan=3 class="feed_title"><a href="/exchange/hubs/feed_detail.cfm?feed_id=#feed.feed_id#&type=#feed.feed_type_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(feed.feed_title)#</b></font></a></td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=3 class="feed_option">#(replace(feed.feed_desc,"#chr(10)#","<br>","all"))#</td></tr>

	 <cfif #feed.feed_url# is not "">
	  <tr><td height=10></td></tr>
	  <tr><td colspan=3 class="text_xsmall"><a href="#feed.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#feed.feed_url#</a></td></tr>
	  <tr><td height=5></td></tr>
	 </cfif>

	 <cfif #feed.feed_attachment# is not "">

     <cfset MyFile="#media_path#\#feed.feed_attachment#">
     <cfset FileInfo=GetFileInfo(MyFile)>

	  <tr><td height=10></td></tr>
	  <tr><td colspan=3 class="text_xsmall"><a href="#media_virtual#/#feed.feed_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;-&nbsp;(#ucase(ListLast(feed.feed_attachment, "."))#, #round(evaluate(fileinfo.size/1000))#k)</td></tr>
	  <tr><td height=5></td></tr>
	 </cfif>

  </table>

 </cfoutput>
