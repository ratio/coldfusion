  <cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td height=10></td></tr>
	 <tr><td class="feed_option" width=80 valign=top>

	 <cfoutput>

		 <cfif feed.feed_image_url is "">
		  <a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/fbo.png" valign=top align=top height=50 border=0></a>
		 <cfelse>
		  <a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#feed.feed_image_url#" align=top height=50 border=0></a>
		 </cfif>

     </cfoutput>

     </td><td valign=top class="feed_sub_header"><font size=4>Federal Business Opportunity</font><br>
     <font size=2>Posted by the EXCHANGE</font><br>

     <font size=2 color="gray">

			   <cfif #mdiff# LT 1>
				 < minute ago
			   <cfelseif #mdiff# GTE 1 and #mdiff# LT 2>
				#mdiff# minute ago
			   <cfelseif #mdiff# GTE 2 and #mdiff# LT 60>
				#mdiff# minutes ago
			   <cfelseif #mdiff# GT 60>
				<cfif #mdiff# LT 1440>
				 #round(evaluate(mdiff/60))# hours ago
				<cfelse>
				 <cfif #round(evaluate(mdiff/1440))# GT 1>
				   #round(evaluate(mdiff/1440))# days ago
				 <cfelse>
				   #round(evaluate(mdiff/1440))# days ago
				 </cfif>
				</cfif>
			   </cfif>
	 </b>
	 </font>

	 </td><td align=right valign=top width=80>

	   <a href="/exchange/feed/share.cfm?feed_id=#feed.feed_id#"><img src="/images/icon_share.png" height=15 valign=middle alt="Share with Others" title="Share with Others" hspace=2></a>
	   <a href="/exchange/feed/save.cfm?feed_id=#feed.feed_id#"><img src="/images/icon_star.png" height=15 valign=middle alt="Send to Portfolio" title="Send to Portfolio" border=0 hspace=2></a>
	   <a href="/exchange/feed/remove.cfm?feed_id=#feed.feed_id#" onclick="return confirm('Remove Feed?\r\nAre you sure you want to remove this opportunity from your Feed?');"><img src="/images/icon_trash.png" height=20 valign=middle alt="Not Interested" title="Not Interested"></a>

	 </td></tr>

	 <tr><td height=15></td></tr>
	 <tr><td colspan=3 class="feed_title"><a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(feed.feed_title)#</b></font></a></td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=3 class="feed_option"><b>#ucase(feed.feed_organization)#</b></td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=3 class="feed_option"><b>#ucase(feed.feed_value_1)#</b><br>
	                                           Solicitation ## - <cfif #feed.feed_value_2# is "">Not specified<cfelse>#ucase(feed.feed_value_2)#</cfif><br>
	                                           #feed.feed_value_3#</td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=3 class="feed_option">
	 <cfif #len(feed.feed_desc)# GT 200>
	  #left((replace(feed.feed_desc,"#chr(10)#","<br>","all")),'200')#... <a href="/exchange/opps/opp_detail.cfm?fbo_id=#feed.feed_fbo_id#" target="_blank" rel="noopener" rel="noreferrer">read more >></a>
	 <cfelse>
	  #replace(feed.feed_desc,"#chr(10)#","<br>","all")#
	 </cfif></td></tr>

	 <cfif #feed.feed_url# is not "">
	  <tr><td height=10></td></tr>
	  <tr><td colspan=3 class="text_xsmall"><a href="#feed.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#feed.feed_url#</a></td></tr>
	  <tr><td height=5></td></tr>
	 </cfif>

  </table>

 </cfoutput>
