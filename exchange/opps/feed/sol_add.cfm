<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif session.company_id is not 0>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Add Solicitation Tracker Feed</td><td align=right class="feed_option"><a href="/exchange/feed/fbo_builder.cfm">Return</a></td></tr>
           <tr><td class="feed_option">This feed will notify you when any updates are made to a solicitation your are tracking.   Updates include modifications, documents, special notices, awards, and other material after a solicitation has been released.</td></tr>
           <tr><td height=10></td></tr>
		  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td class="feed_option" width=150><b>Solicitation Number</b></td>
              <td class="feed_option"><input type="text" name="usr_feed_solicitation_number" style="width: 250px;" maxlength="100" required></td>
          </tr>

           <tr><td height=10></td></tr>
           <tr><td></td><td><input class="button_blue" style="font-size: 11px; height: 23px; width: 125px;" type="submit" name="button" value="Add Solicitation"></td></tr>

           </table>

 		  </form>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

