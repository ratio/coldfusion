<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <cfif isdefined("session.hub")>
	 <cfinclude template="/exchange/include/hub_tabs.cfm">
  </cfif>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/include/profile.cfm">
	  </div>

	  <cfif session.company_id is not 0>

		  <div class="left_box">
			  <cfinclude template="/exchange/include/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/include/advisor.cfm">
	  </div>

      </td><td valign=top>

		  <div class="main_box">

		   <cfinclude template="feed.cfm">

		  </div>

	  </div>

	  </td>

	  <cfif isdefined("session.hub")>

	      <cfif hub.hub_home_about is 1 or hub.hub_home_sponsors is 1 or hub.hub_home_child_hubs is 1>

			  <td valign=top width=200>

			  <cfif hub.hub_home_about is 1>

				  <div class="right_box">
					  <cfinclude template="/exchange/hubs/about_hub.cfm">
				  </div>

			  </cfif>

			  <cfif hub.hub_home_sponsors is 1>

				  <div class="right_box">
					  <cfinclude template="/exchange/hubs/hub_sponsors.cfm">
				  </div>

			  </cfif>

			  <cfif hub.hub_home_child_hubs is 1>

				  <div class="right_box">
					  <cfinclude template="/exchange/hubs/sponsored_hubs.cfm">
				  </div>

			  </cfif>

			  </td>

		  </cfif>

	  </cfif>

	  </tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>