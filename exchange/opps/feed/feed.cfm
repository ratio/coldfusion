<cfif not isdefined("session.feed_time")>
 <cfset #session.feed_time# = 0>
</cfif>

<cfif not isdefined("session.feed_lens")>
 <cfset #session.feed_lens# = 0>
</cfif>

<cfif not isdefined("session.hub_filter")>
 <cfset #session.hub_filter# = 1>
</cfif>

<cfquery name="feed" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from feed
 join feed_usr on feed_usr_feed_id = feed.feed_id
 left join usr on usr.usr_id = feed.feed_posted_by_usr_id
 left join company on company.company_id = feed.feed_posted_by_company_id
 left join hub on hub_id = feed.feed_posted_by_hub_id
 where feed_usr.feed_usr_usr_id = #session.usr_id#
 and feed.feed_status = 1

 <cfif isdefined("session.hub")>
  and (feed.feed_posted_by_hub_id = #session.hub# or feed.feed_posted_by_hub_id is null)
 <cfelse>
  and (feed.feed_posted_by_hub_id is null)
 </cfif>

 <cfif isdefined("session.feed_keyword")>
  and (feed.feed_title like '%#session.feed_keyword#%' or
       feed.feed_organization like '%#session.feed_keyword#%' or
       feed.feed_desc like '%#session.feed_keyword#%')
 </cfif>

 <cfif isdefined("session.feed_lens")>
  <cfif session.feed_lens is 0>
  <cfelse>
  	and (feed.feed_lens_id like '%#session.feed_lens#%')
  </cfif>
 </cfif>

 <cfif #session.feed_time# is not 0>

	   <cfif #session.feed_time# is 1>

	   <cfset start_date = dateformat(now(),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
	   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

	  <cfelseif #session.feed_time# is 2>

		   <cfset start_date = dateformat(dateadd('d','-7',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

		  <cfelseif #session.feed_time# is 3>
		   <cfset start_date = dateformat(dateadd('d','-30',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

		  <cfelseif #session.feed_time# is 4>
		   <cfset start_date = dateformat(dateadd('d','-90',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

	 </cfif>

	  and (feed.feed_created >= '#start_date#' and feed.feed_created <= '#end_date#')

 </cfif>

 order by feed.feed_updated DESC

</cfquery>

  <form name="search" action="search.cfm" method="post">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header"><cfoutput>Opportunity Feed (#feed.recordcount#)</cfoutput></td>
       <td class="feed_option" align=right>

	   <b>Search&nbsp;&nbsp;</b>

			<select name="search_type" class="input_white" style="width:175px" >
				<option value=1 <cfif isdefined("session.search_type") and session.search_type is 1>selected</cfif>>Opportunity Feed
				<option value=2 <cfif isdefined("session.search_type") and session.search_type is 2>selected</cfif>>Contract Opportunities
			</select>

            <input style="width: 200px;" class="input_white" type="text" name="keyword">&nbsp;&nbsp;
			<input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Search">
			<input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Reset">

       </td></tr>
   <tr><td height=10></td></tr>
  </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_option" valign=bottom><a href="/exchange/post.cfm">Post</a>&nbsp;|&nbsp;
	                                             <a href="/exchange/feed/build.cfm">Setup Feeds</a>&nbsp;|&nbsp;
	                                             <a href="/exchange/feed/clear.cfm" onclick="return confirm('Clear Feeds?\r\nBy clicking OK, you will remove all of your feeds.  Are you sure you want to clear your feeds?');">Clear Feeds</a>
	   </td>
           <td class="feed_option" align=right><b>Display</b>&nbsp;

			<select name="time" class="input_white" style="width:125px" onchange='this.form.submit()'>
				<option value=0 <cfif session.feed_time is 0>selected</cfif>>All Feeds
				<option value=1 <cfif session.feed_time is 1>selected</cfif>>Today
				<option value=2 <cfif session.feed_time is 2>selected</cfif>>This Week
				<option value=3 <cfif session.feed_time is 3>selected</cfif>>This Month
				<option value=4 <cfif session.feed_time is 4>selected</cfif>>This Quarter
			</select>

            </td></tr>

       </table>

   </form>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfif isdefined("u")>
 <cfif u is 1>
   <tr><td class="feed_option"><b><font color="green">Feed has been successfully removed.</font></b></td></tr>
 <cfelseif u is 2>
   <tr><td class="feed_option"><b><font color="green">Feed has been successfully saved to your Portfolio.</font></b></td></tr>
 <cfelseif u is 3>
   <tr><td class="feed_option"><b><font color="green">Feed has been successfully shared.</font></b></td></tr>
 <cfelseif u is 4>
   <tr><td class="feed_option"><b><font color="green">Post has been added.</font></b></td></tr>
 <cfelseif u is 9>
   <tr><td class="feed_option"><b><font color="green">All of your feeds have been cleared.</font></b></td></tr>
 </cfif>
 <tr><td height=10></td></tr>
</cfif>

<tr><td>

<cfif feed.recordcount is 0>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_sub_header">No opportunity feeds have been posted.</td></tr>
   <tr><td height=10></td></tr>
   <tr><td class="feed_option">To start receiving opportunity feeds, please <a href="/exchange/feed/build.cfm"><b>setup your feeds</b></a> and we will start sending you opportunities when they are identified or posted.</td></tr>
  </table>

<cfelse>

<cfloop query="feed">

  <cfset mdiff = #datediff("n",feed.feed_created,now())#>

	   <div class="feed_box">

		   <cfif feed.feed_type_id is 1>
		   	<cfinclude template = "/exchange/feed/feed_types/feed_type_post.cfm">
		   <cfelseif feed.feed_type_id is 2>
		   	<cfinclude template = "/exchange/feed/feed_types/feed_type_admin_hub.cfm">
		   <cfelseif feed.feed_type_id is 3>
		   	<cfinclude template = "/exchange/feed/feed_types/feed_type_fbo.cfm">
		   <cfelseif feed.feed_type_id is 4>
		   	<cfinclude template = "/exchange/feed/feed_types/feed_type_admin_exchange.cfm">
           </cfif>

	   </div>

   </div>

</cfloop>

</cfif>

</td></tr>

</table>