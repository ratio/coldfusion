<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif session.company_id is not 0>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

			<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select distinct(fbo_agency) from fbo
			 order by fbo_agency
			</cfquery>

			<cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select distinct(fbo_type) from fbo
			 order by fbo_type
			</cfquery>

			<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select distinct(fbo_setaside) from fbo
			 order by fbo_setaside
			</cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Create Award Notification Feed</td><td align=right class="feed_option"><a href="/exchange/feed/fbo_builder.cfm">Return</a></td></tr>
           <tr><td class="feed_option">This feed will notify you when an award is made that meets your criteria below.</td></tr>
           <tr><td height=10></td></tr>
		  </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td class="feed_option"><b>Feed Name</b></td>
              <td class="feed_option"><input type="text" name="usr_feed_name" style="width: 250px;" maxlength="100" required></td>
          </tr>

          <tr>
              <td class="feed_option" valign=top><b>Description</b></td>
              <td class="feed_option"><textarea name="usr_feed_desc" cols=50 rows=5></textarea></td>
          </tr>

          <tr>
              <td class="feed_option"><b>Agency</b></td>
              <td class="feed_option">
              <select name="usr_feed_dept" style="width:250px;">
               <option value=0>All
               <cfoutput query="dept">
                <option value="#fbo_agency#">#fbo_agency#
               </cfoutput>
              </select>
              </td></tr>

          <tr>
              <td class="feed_option"><b>Company DUNS Number *</b></td>
              <td class="feed_option"><input type="text" name="usr_feed_duns" style="width:250px;" maxlength="299">&nbsp;&nbsp;<a href="/exchange/marketplace/" target="_blank" rel="noopener" rel="noreferrer">Marketplace</a></td>
          </tr>

          <tr>
              <td class="feed_option" width=200><b>NAICS Code(s) *</b></td>
              <td class="feed_option"><input type="text" name="usr_feed_naics" style="width:250px;" maxlength="299">&nbsp;&nbsp;<a href="https://www.census.gov/cgi-bin/sssd/naics/naicsrch?chart=2017" target="_blank" rel="noopener" rel="noreferrer">NAICS Code list</a></td>
          </tr>

           <tr>
               <td class="feed_option"><b>Product or Service *</b></td>
               <td class="feed_option"><input type="text" name="usr_feed_fbo_psc" style="width: 250px;" maxlength="299">&nbsp;&nbsp;<a href="https://www.fbo.gov/index?s=getstart&mode=list&tab=list&tabmode=list&static=faqs#q4" target="_blank" rel="noopener" rel="noreferrer">Product or Service Code list</a>.</td>
           </tr>

          <tr>
              <td class="feed_option"><b>Keyword</b></td>
              <td class="feed_option"><input type="text" name="usr_feed_keyword" style="width:250px;" maxlength="100" ></td>
          </tr>

           <tr>
               <td class="feed_option"><b>Small Business</b></td>
               <td class="feed_option">
				<select name="usr_feed_fbo_sb" style="width:150px;">
				 <option value=0>No Preference
				 <cfoutput query="setaside">
				 <option value="#fbo_setaside#">#fbo_setaside#
				 </cfoutput>
                </select></td>

           </tr>

           <tr><td height=10></td></tr>
           <tr><td></td><td><input class="button_blue" style="font-size: 11px; height: 23px; width: 125px;" type="submit" name="button" value="Add Award Feed"></td></tr>
           <tr><td height=10></td></tr>
           <tr><td></td><td class="text_xsmall">* - Entering nothing will return all.  To select multiple values, seperate codes with a commma and no spaces.</td></tr>
           <tr><td height=10></td></tr>

           </table>

 		  </form>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

