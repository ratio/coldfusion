<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif session.company_id is not 0>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

	      <form action="build_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">FBO.Gov Feeds</td><td align=right class="feed_option"><a href="/exchange/">Return</a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td class="feed_option">We pull information from the Federal Business Opportunities website (http://www.fbo.gov) daily and deliver them to you through your subscriptions below.</td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header"><a href="build.cfm">General Alignments</a>&nbsp;|&nbsp;<a href="fbo_builder.cfm">FBO.Gov Feeds</a></td></tr>
           <tr><td height=10></td></tr>
           <tr><td colspan=2><hr></td></tr>

		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
            <tr><td colspan=3 class="feed_header">Federal Business Opportunities (FBO.Gov)</td></tr>
            <tr><td colspan=3 class="feed_option">FBO.Gov is a Federal Government procurement portal that posts and maintains solicitations that are issued to industry greater than $25,000.  The EXCHANGE pulls this information daily.</td></tr>
            <tr><td height=20></td></tr>

			<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 1 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_name
			</cfquery>

			<tr><td valign=top width=33%>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td width=30><a href="fbo_add.cfm"><img src="/images/plus3.png" width=15 alt="Create Opportunity Feed" title="Create Opportunity Feed" border=0></a></td><td class="feed_sub_header">Opportunity Tracker</td></tr>

            <cfif fbo.recordcount is 0>
              <tr><td></td><td class="feed_option" colspan=3>You have no opportunity feeds created.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <tr><td height=10></td></tr>
              <cfoutput query="fbo">
                <tr><td></td><td class="feed_option"><a href="fbo_edit.cfm?usr_feed_id=#usr_feed_id#"><b>#usr_feed_name#</b></a></td></tr>
                <tr><td></td><td class="feed_option">#usr_feed_desc#</td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td><td valign=top width=33%>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 2 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_name
			</cfquery>

            <tr><td width=30><a href="award_add.cfm"><img src="/images/plus3.png" width=15 alt="Create Award Notification Feed" title="Create Award Notification Feed" border=0></a></td><td class="feed_sub_header">Award Notification Tracker</td></tr>

            <cfif awards.recordcount is 0>
              <tr><td></td><td class="feed_option" colspan=3>You have no award feeds created.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <tr><td height=10></td></tr>
              <cfoutput query="awards">
                <tr><td></td><td class="feed_option"><a href="award_edit.cfm?usr_feed_id=#usr_feed_id#"><b>#usr_feed_name#</b></a></td></tr>
                <tr><td></td><td class="feed_option">#usr_feed_desc#</td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td><td valign=top>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="sol" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from usr_feed
			 where usr_feed_type_id = 3 and
			       usr_feed_usr_id = #session.usr_id#
			 order by usr_feed_solicitation_number
			</cfquery>

            <tr><td width=30><a href="sol_add.cfm"><img src="/images/plus3.png" width=15 alt="Add Solicitation Number" title="Add Solicitation Number" border=0></a></td><td class="feed_sub_header">Solicitation Tracker</td></tr>

            <cfif sol.recordcount is 0>
              <tr><td></td><td class="feed_option" colspan=3>You are not tracking any solicitations.</td></tr>
              <tr><td height=10></td></tr>
            <cfelse>
              <tr><td height=10></td></tr>
              <cfoutput query="sol">
                <tr><td></td><td class="feed_option"><a href="sol_edit.cfm?usr_feed_id=#usr_feed_id#"><b>#usr_feed_solicitation_number#</b></a></td></tr>
              </cfoutput>

            </cfif>

	        <tr><td height=10></td></tr>

          </table>

          </td></tr>

                   </table>

                   </td></tr>

</table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

