<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	  <div class="left_box">
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  </div>

	  <cfif isdefined("session.company_id")>

		  <div class="left_box">
			  <cfinclude template="/exchange/stats.cfm">
		  </div>

	  </cfif>

	  <div class="left_box">
	      <cfinclude template="/exchange/advisor.cfm">
	  </div>

      </td><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Update Feed Categories</td><td align=right class="feed_option"><a href="/exchange/feed/build.cfm">Return</a></td></tr>
           <tr><td height=10></td></tr>
		  </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfquery name="profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from usr
		 where usr_id = #session.usr_id#
		</cfquery>

		<cfquery name="lenses" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from lens
		 order by lens_name
		</cfquery>

		<cfquery name="align" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select align_type_value from align
		 where (align_usr_id = #session.usr_id#) and
			   (align_type_id = 1)
		</cfquery>

		<cfset #lens_list# = #valuelist(align.align_type_value)#>

	      <form action="category_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_option" colspan=3>Please select the Feed categories you want to subscribe to.</td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_option" width=90><b>Subscribe</b></td>
               <td class="feed_option"><b>Category</b></td>
               <td class="feed_option"><b>Description</b></td>
           </tr>

           <cfset #bgcolor# = "ffffff">
           <cfset #counter# = 1>
           <cfoutput query="lenses">
            <cfif #counter# is 1>
             <cfset #bgcolor# = "ffffff">
            <cfelse>
             <cfset #bgcolor# = "e0e0e0">
            </cfif>

            <tr bgcolor=#bgcolor#>
             <td align=center><input type="checkbox" name="align_type_value" value=#lenses.lens_id# <cfif listfind(lens_list,lenses.lens_id)>Checked</cfif>>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td width=200 class="feed_option">#lenses.lens_name#</td>
             <td class="feed_option">#lenses.lens_desc#</td>
            </tr>

            <cfif #counter# is 1>
             <cfset #counter# = 0>
            <cfelse>
             <cfset #counter# = 1>
            </cfif>

           </cfoutput>

           <tr><td>&nbsp;</td></tr>

           <tr><td colspan=3>
           <input class="button_blue" type="submit" name="button" value="Update">
           </td></tr>

 		  </table>

 		  </form>

      </table>

	  </div>

	  </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

