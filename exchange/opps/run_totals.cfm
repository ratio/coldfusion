<!--- In Ecosystem --->

<cfquery name="set" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_parent_hub_id from hub
 where hub_id = #session.hub#
</cfquery>

<cfif set.hub_parent_hub_id is "">
 <cfset hub_list = 0>
<cfelse>

 <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select hub_id from hub
  where hub_id = #set.hub_parent_hub_id#
 </cfquery>

 <cfif list.recordcount is 0>
  <cfset hub_list = 0>
 <cfelse>
  <cfset hub_list = valuelist(list.hub_id)>
 </cfif>

</cfif>

<cfquery name="in" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
 join hub on hub_id = challenge_hub_id
 where (challenge_hub_id = #session.hub# or challenge_hub_id in (#hub_list#)) and
        challenge_public = 1
 union
 select hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
 join hub on hub_id = challenge_hub_id
 where challenge_public = 2
 order by challenge_total_cash DESC
</cfquery>

<cfquery name="out" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(challenge_id) as total from challenge
 where challenge_type_id = 1
</cfquery>