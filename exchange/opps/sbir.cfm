<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfif not isdefined("session.sbir_recent")>
 <cfset #session.sbir_recent# = 1>
</cfif>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<body class="body">

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<!--- Lookups --->

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="/exchange/opps/sbir_sites.cfm">
      </td><td valign=top>

      <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_light.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
          </div>

      </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_opportunities_sbir.cfm">
	  </div>

       <div class="main_box">

		<cfquery name="recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from opp_sbir
		 left join orgname on orgname_name = agency

		 <cfif session.sbir_recent is 1>
		  where posteddate between '#dateformat(dateadd('d',-30,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
		 <cfelseif session.sbir_recent is 2>
		  where posteddate between '#dateformat(dateadd('d',-90,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
		 <cfelseif session.sbir_recent is 3>
		  where posteddate between '#dateformat(dateadd('d',-180,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
		 </cfif>

			<cfif #sv# is 1>
			 order by agencyname ASC
			<cfelseif #sv# is 10>
			 order by agencyname DESC
			<cfelseif #sv# is 2>
			 order by opportunitynumber ASC
			<cfelseif #sv# is 20>
			 order by opportunitynumber DESC
			<cfelseif #sv# is 3>
			 order by opportunitytitle ASC
			<cfelseif #sv# is 30>
			 order by opportunitytitle DESC
			<cfelseif #sv# is 4>
			 order by fundinginstrumenttype ASC
			<cfelseif #sv# is 40>
			 order by fundinginstrumenttype DESC
			<cfelseif #sv# is 5>
			 order by awardfloor ASC
			<cfelseif #sv# is 50>
			 order by awardfloor DESC
			<cfelseif #sv# is 6>
			 order by awardceiling ASC
			<cfelseif #sv# is 60>
			 order by awardceiling DESC
			<cfelseif #sv# is 7>
			 order by updated ASC
			<cfelseif #sv# is 70>
			 order by updated DESC
			</cfif>

		</cfquery>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <form action="sbir_refresh.cfm" method="post">
          <cfoutput>
 			 <tr><td class="feed_header">Recently Posted <cfif recent.recordcount is 0><cfelse>(#ltrim(numberformat(recent.recordcount,'99,999'))#)</cfif></td>
 				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=300><b>Posted</b>&nbsp;&nbsp;
 				 <select name="sbir_recent" class="input_select" style="width:125px" onchange="form.submit()">
 					<option value=1 <cfif #session.sbir_recent# is 1>selected</cfif>>Last 30 Days
 					<option value=2 <cfif #session.sbir_recent# is 2>selected</cfif>>Last 90 Days
 					<option value=3 <cfif #session.sbir_recent# is 3>selected</cfif>>Last 180 Days
 				 </select>

 				</td></tr>
 				</cfoutput>
             </form>

          <tr><td height=5></td></tr>
          <tr><td colspan=3><hr></td></tr>
          <tr><td height=5></td></tr>

        </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif recent.recordcount is 0>
            <tr><td class="feed_sub_header" style="font-weight: normal;">There are no recently posted SBIR/STTRs.</td></tr>
          <cfelse>

             <tr>
                <td></td>
                <td class="feed_sub_header">Opportunity Name</td>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header" align=right>Closes On</td>
             </tr>

             <cfset counter = 0>

             <cfloop query="recent">

    			 <cfoutput>

                <cfif recent.source is "HHS">

   			 <cfif counter is 0>
   			  <tr bgcolor="ffffff" height=40>
   			 <cfelse>
   			  <tr bgcolor="e0e0e0" height=40>
   			 </cfif>

                 <td width=70>

    			 	<cfif recent.orgname_logo is "">
    			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#recent.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
    			 	<cfelse>
    			 	 <a href="/exchange/include/sbir_hhs.cfm?id=#recent.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#recent.orgname_logo#" align=top width=40 border=0></a>
    			 	</cfif>

                   </td>


    				 <td class="feed_sub_header" style="font-weight: normal;" width=800>

                     <a href="/exchange/include/sbir_hhs.cfm?id=#recent.id#" target="_blank" rel="noopener" rel="noreferrer">
                     <cfif recent.objective is not "">
    					 <cfif len(recent.objective) GT 1000>
    					  #left(recent.objective,1000)#...
    					 <cfelse>
    					  #recent.objective#
    					 </cfif>
    				 <cfelse>
    				  Objective not provided.
    				 </cfif>
    				 </a>

    				 </td>


    				<td class="feed_sub_header" style="font-weight: normal;">

    				<cfif recent.agency is "" or recent.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(recent.agency)#</cfif>
    				<span class="link_small_gray"><br>#ucase(recent.department)#</span></td>

                 <cfelseif recent.source is "DOD">

   			 <cfif counter is 0>
   			  <tr bgcolor="ffffff" height=40>
   			 <cfelse>
   			  <tr bgcolor="e0e0e0" height=40>
   			 </cfif>



                 <td width=70 valign=top>

    				 <cfif recent.orgname_logo is "">
    				  <a href="/exchange/include/sbir_dod.cfm?id=#recent.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 vspace=10 border=0></a>
    				 <cfelse>
    				  <a href="/exchange/include/sbir_dod.cfm?id=#recent.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#recent.orgname_logo#" align=top width=40 vspace=10 border=0></a>
    				 </cfif>

                     </td>

    				 <td class="feed_sub_header" style="font-weight: normal;">

                     <a href="/exchange/include/sbir_dod.cfm?id=#recent.id#" target="_blank" rel="noopener" rel="noreferrer">
                     <cfif recent.objective is not "">
    					 <cfif len(recent.objective) GT 400>
    					  #left(recent.objective,400)#...
    					 <cfelse>
    					  #recent.objective#
    					 </cfif>
    				  <cfelse>
    				   Description not provided.
    				  </cfif>
    				  </a>

    			      <cfif recent.keywords is not ""><br>
                      <br><span class="link_small_gray">Keywords: </b>#ucase(recent.keywords)#</span>
                     </cfif>

    				 </td>

    				<td class="feed_sub_header" style="font-weight: normal;" valign=top>

    				<cfif recent.agency is "" or recent.agency is "N/A">Not Specified<cfelse>#ucase(recent.agency)#</cfif>
    				<span class="link_small_gray"><br>#ucase(recent.department)#</span></td>

                 </cfif>

   				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125 valign=top>

   				 <cfif recent.closedate is "">
   				  Not Specified
   				 <cfelseif recent.closedate is "1900-01-01">
   				  Not Specified
   				 <cfelse>
   				 #dateformat(recent.closedate,'mm/dd/yyyy')#
   				 </cfif>

   				 </td>

                    <td align=right width=40 valign=top>
 					<img src="/images/icon_pin.png" style="cursor: pointer;" vspace=10 width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#recent.id#&t=sbir','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
   			     </td>

    				</tr>

   			 <cfif counter is 0>
   			  <cfset counter = 1>
   			 <cfelse>
   			  <cfset counter = 0>
   			 </cfif>

          </cfoutput>

                </cfloop>

                </cfif>

                <tr><td height=20></td></tr>


         </table>

       </div>


      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>