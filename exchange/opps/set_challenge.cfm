  <cfif button is "Clear">
   <cfset StructDelete(Session,"challenge_status")>
   <cfset StructDelete(Session,"challenge_type")>
   <cfset StructDelete(Session,"challenge_keyword")>
   <cflocation URL="challenges.cfm" addtoken="no">
  </cfif>

  <cfset search_area = 12>

  <!--- Remove Invalid Characters --->

  <cfif trim(challenge_keyword) is "">
      <cfset StructDelete(Session,"challenge_keyword")>
  <cfelse>
    <cfset search_string = #replace(challenge_keyword,chr(34),'',"all")#>
    <cfset search_string = #replace(search_string,'''','',"all")#>
    <cfset search_string = #replace(search_string,')','',"all")#>
    <cfset search_string = #replace(search_string,'(','',"all")#>
    <cfset search_string = #replace(search_string,',','',"all")#>
    <cfset search_string = #replace(search_string,':','',"all")#>
    <cfset search_string = '"' & #search_string#>
    <cfset search_string = #search_string# & '"'>
    <cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
    <cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
    <cfset #session.challenge_keyword# = #search_string#>
  </cfif>

  <cfset #session.challenge_status# = #challenge_status#>
  <cfset #session.challenge_type# = #challenge_type#>

  <cfif isdefined("session.challenge_keyword")>

	  <cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  insert into keyword_search
	  (
	   keyword_search_usr_id,
	   keyword_search_company_id,
	   keyword_search_hub_id,
	   keyword_search_keyword,
	   keyword_search_date,
	   keyword_search_area
	   )
	   values
	   (
	   #session.usr_id#,
	   #session.company_id#,
	   #session.hub#,
	  '#session.challenge_keyword#',
	   #now()#,
	   #search_area#
	   )
	 </cfquery>

 </cfif>

<cflocation URL="/exchange/opps/challenges_results.cfm" addtoken="no">
