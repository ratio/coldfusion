<cfinclude template="/exchange/security/check.cfm">

	<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from usr
	  where usr_id = #session.usr_id#
	</cfquery>

	<cfif t is "n">

		<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
          select * from need
          join usr on usr_id = need_owner_id
          where need_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
        </cfquery>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into question
		 (
		  question_usr_id,
		  question_subject,
		  question_text,
		  question_date_submitted,
		  question_need_id,
		  question_public
		  )
		  values
		  (
		  #session.usr_id#,
		 '#question_subject#',
		 '#question_text#',
		  #now()#,
		  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		  0
		  )
		 </cfquery>

     <!--- Send Emails --->

		<cfmail from="#session.network_name# <noreply@ratio.exchange>"
				  to="#tostring(tobinary(need.usr_email))#"
		  username="noreply@ratio.exchange"
		  password="Gofus107!"
			  port="25"
			useSSL="false"
			type="html"
			server="mail.ratio.exchange"
		   subject="Need Question">
		<html>
		<head>
		<title><cfoutput>#session.network_name#</cfoutput></title>
		</head><div class="center">
		<body class="body">

		<cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">

			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">A question has been submitted regarding a Need posted on the Exchange.</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Need</b> : #need.need_name#</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>From: </b> #user.usr_first_name# #user.usr_last_name# (#tostring(tobinary(user.usr_email))#)</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#question_subject#</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(question_text,"#chr(10)#","<br>","all")#</td></tr>
			 <tr><td>&nbsp;</td></tr>
			</table>

		</cfoutput>

		</body>
		</html>

		</cfmail>

		<cfmail from="#session.network_name# <noreply@ratio.exchange>"
				  to="#tostring(tobinary(user.usr_email))#"
		  username="noreply@ratio.exchange"
		  password="Gofus107!"
			  port="25"
			useSSL="false"
			type="html"
			server="mail.ratio.exchange"
		   subject="Need Question Received">
		<html>
		<head>
		<title><cfoutput>#session.network_name#</cfoutput></title>
		</head><div class="center">
		<body class="body">

		<cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">

			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Thank you for submitting your question.  The point of contact for the Need has been notified and will respond as soon as possible.</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Need</b> : #need.need_name#</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#question_subject#</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(question_text,"#chr(10)#","<br>","all")#</td></tr>
			 <tr><td>&nbsp;</td></tr>

		</cfoutput>

		</body>
		</html>

		</cfmail>

	 </cfif>

	<cfif t is "c">

		<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
          select * from challenge
          where challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
        </cfquery>

		<cfquery name="hinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
          select * from hub
          where hub_id = #info.challenge_hub_id#
        </cfquery>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into question
		 (
		  question_usr_id,
		  question_subject,
		  question_text,
		  question_date_submitted,
		  question_challenge_id,
		  question_public
		  )
		  values
		  (
		  #session.usr_id#,
		 '#question_subject#',
		 '#question_text#',
		  #now()#,
		  #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		  0
		  )
		 </cfquery>

     <!--- Send Emails --->

		<cfmail from="#hinfo.hub_name# <noreply@ratio.exchange>"
				  to="#info.challenge_poc_email#"
		  username="noreply@ratio.exchange"
		  password="Gofus107!"
			  port="25"
			useSSL="false"
			type="html"
			server="mail.ratio.exchange"
		   subject="Challenge Question">
		<html>
		<head>
		<title><cfoutput>#session.network_name#</cfoutput></title>
		</head><div class="center">
		<body class="body">

		<cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">

			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">A question has been submitted regarding a Challenge posted on #hinfo.hub_name#.</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Challenge</b> : #info.challenge_name#</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>From: </b> #user.usr_first_name# #user.usr_last_name# (#tostring(tobinary(user.usr_email))#)</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#question_subject#</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(question_text,"#chr(10)#","<br>","all")#</td></tr>
			 <tr><td>&nbsp;</td></tr>
			</table>

		</cfoutput>

		</body>
		</html>

		</cfmail>

		<cfmail from="#hinfo.hub_name# <noreply@ratio.exchange>"
				  to="#user.usr_email#"
		  username="noreply@ratio.exchange"
		  password="Gofus107!"
			  port="25"
			useSSL="false"
			type="html"
			server="mail.ratio.exchange"
		   subject="Challenge Question  Sent">
		<html>
		<head>
		<title><cfoutput>#hinfo.hub_name#</cfoutput></title>
		</head><div class="center">
		<body class="body">

		<cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Thank you for submitting your question for the Challenge - #info.challenge_name#.</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">The point of contact for the Challenge has been notified and will respond as soon as possible.</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>#hinfo.hub_support_name#</b></td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hinfo.hub_support_phone#</td></tr>
			 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hinfo.hub_support_email#</td></tr>
			 <tr><td>&nbsp;</td></tr>
		</cfoutput>

		</body>
		</html>

		</cfmail>

	 </cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>

         <tr><td class="feed_header">Thank you for submitting your question.  We will answer your question as soon as possible.</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>