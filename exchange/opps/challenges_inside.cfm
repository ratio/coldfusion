<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">


<cfif not isdefined("session.challenge_status")>
 <cfset session.challenge_status = 1>
</cfif>

<cfif not isdefined("session.exchange_id")>
 <cfset session.exchange_id = 0>
</cfif>

<cfset session.challenge_location = 2>

<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>


<style>
.sub_tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.sub_tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.
.sub_main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>


<cfinclude template = "/exchange/include/header.cfm">

<cfinclude template="run_totals.cfm">

<cfquery name="set" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_parent_hub_id from hub
 where hub_id = #session.hub#
</cfquery>

<cfif set.hub_parent_hub_id is "">
 <cfset hub_list = 0>
<cfelse>

 <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select hub_id from hub
  where hub_id = #set.hub_parent_hub_id#
 </cfquery>

 <cfif list.recordcount is 0>
  <cfset hub_list = 0>
 <cfelse>
  <cfset hub_list = valuelist(list.hub_id)>
 </cfif>

</cfif>

<cfquery name="cinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select challenge_end, hub_id, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
 join hub on hub_id = challenge_hub_id
 where (challenge_hub_id = #session.hub# or challenge_hub_id in (#hub_list#)) and
        challenge_public = 1
        <cfif session.challenge_status is not 0>
        	and challenge_status = #session.challenge_status#
        </cfif>
        <cfif session.exchange_id is not 0>
        	and challenge_hub_id = #session.exchange_id#
        </cfif>
 union
 select challenge_end, hub_id, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
 join hub on hub_id = challenge_hub_id
 where challenge_public = 2
        <cfif session.challenge_status is not 0>
        	and challenge_status = #session.challenge_status#
        </cfif>
        <cfif session.exchange_id is not 0>
        	and challenge_hub_id = #session.exchange_id#
        </cfif>
 order by challenge_total_cash DESC
</cfquery>

<cfquery name="exchanges" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_id, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
 join hub on hub_id = challenge_hub_id
 where (challenge_hub_id = #session.hub# or challenge_hub_id in (#hub_list#)) and
        challenge_public = 1
 union
 select hub_id, hub_name, challenge_id, challenge_name, challenge_total_cash, challenge_type_id, challenge_status, challenge_annoymous, challenge_organization, challenge_currency, challenge_desc, challenge_image from challenge
 join hub on hub_id = challenge_hub_id
 where challenge_public = 2
 order by challenge_total_cash DESC
</cfquery>

<cfif exchanges.recordcount is 0>
 <cfset exchange_list = 0>
<cfelse>
 <cfset exchange_list = valuelist(exchanges.hub_id)>
</cfif>

<cfquery name="networks" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_id, hub_name from hub
 where hub_id in (#exchange_list#)
 order by hub_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="recent_challenges.cfm">

<!---          <cfinclude template="/exchange/opps/follow_challenge.cfm"> --->
      </td><td valign=top>

      <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
          </div>

      </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_challenges.cfm">
	  </div>

      <cfoutput>

		  <div class="sub_tab_active" style="margin-left: 20px;">
		   <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="challenges_inside.cfm">Exchange Challenges<cfif #in.recordcount# GT 0> (#in.recordcount#)</cfif></a></span>

		   	 <div class="tooltip" align=right><img src="/images/help_black.png" width=25>
		   	   <span class="tooltiptext">Challenges that are posted, managed and maintained on the Exchange.</span>
			 </div>

		  </div>

		  <div class="sub_tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="challenges_outside.cfm">Other Challenges</a></span>
		  </div>

	  </cfoutput>

      <div class="main_box_2">

        <form action="set_filter.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">Filter Challenges</td>
		     <td align=right class="feed_sub_header" style="font-weight: normal;">

		     <b>Exchange / Network</b>&nbsp;

			  <select name="exchange_id" class="input_select" onchange="form.submit()">
			   <option value=0>All
               <cfoutput query="networks">
                <option value=#networks.hub_id# <cfif session.exchange_id is #hub_id#>selected</cfif>>#hub_name#
               </cfoutput>
			  <select>

		     <b>Status</b>&nbsp;

			  <select name="challenge_status" class="input_select" onchange="form.submit()">
			   <option value=0 <cfif session.challenge_status is 0>selected</cfif>>All Challenges
			   <option value=1 <cfif session.challenge_status is 1>selected</cfif>>Open
			   <option value=2 <cfif session.challenge_status is 2>selected</cfif>>Closed
			   <option value=3 <cfif session.challenge_status is 3>selected</cfif>>Pending
			  <select>

	          <b>Sort By</b>&nbsp;

			  <select name="challenge_sort" class="input_select" onchange="form.submit()">
			   <option value=1>Prize (low to high)
			   <option value=2>Price (high to low)
			   <option value=3>Close Date (earliest)
			   <option value=4>Close Date (latest)
			  <select>


		     </td></tr>

		 <tr><td colspan=2><hr></td></tr>

         <cfif isdefined("u")>
          <cfif u is 7>
           <tr><td colspan=3 class="feed_sub_header" style="color: green;">Opportunity has successfully been pinned to your board.</td></tr>
          </cfif>
         </cfif>
		</table>

		</form>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif cinfo.recordcount is 0>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">No Challenges have been posted.</td></tr>
		<cfelse>

		   <cfset count = 1>

           <cfloop query="cinfo">

			   <cfoutput>

               <tr>

				<td valign=top width=125>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td align=center>

					<cfif cinfo.challenge_image is "">
					  <a href="/exchange/opps/challenge_detail.cfm?i=#encrypt(cinfo.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/stock_challenge.png" vspace=10 width=100 border=0></a>
					<cfelse>
					  <a href="/exchange/opps/challenge_detail.cfm?i=#encrypt(cinfo.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#cinfo.challenge_image#" vspace=10 width=100 border=0></a>
					</cfif>

			    </td></tr>

			    </table>

			    </td>

			    <td width=20></td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_header" style="padding-bottom: 0px; margin-bottom: 0px;"><a href="/exchange/opps/challenge_detail.cfm?i=#encrypt(cinfo.challenge_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#cinfo.challenge_name#</b></a>

			    <td class="feed_sub_header" align=right>
			     <cfif #cinfo.challenge_status# is 1>
			      <img src="/images/icon_open.png" width=100>
			     <cfelseif #cinfo.challenge_status# is 2>
			      <img src="/images/icon_closed.png" width=100>
			     <cfelseif #cinfo.challenge_status# is 3>
			      <img src="/images/icon_pending.png" width=100>
			     </cfif></td>

                 <td align=right width=50 valign=top>
					<img vspace=10 src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#cinfo.challenge_id#&t=challenge','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			     </td>

			     </tr>

                  <tr><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px;"><b><cfif #cinfo.challenge_annoymous# is 1>Private Organization<cfelse>#cinfo.challenge_organization#</cfif></b></td>

                   <td class="feed_sub_header" align=right style="padding-top: 0px; padding-bottom: 0px;">

                    <cfif #cinfo.challenge_total_cash# is not "">
                    Prize:
						<cfif cinfo.challenge_currency is "$">$
						<cfelseif cinfo.challenge_currency is "I">INR
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelseif cinfo.challenge_currency is "�">�
						<cfelse>
						</cfif>

                      #numberformat(cinfo.challenge_total_cash,'$999,999,999')#
                    </cfif>

                  </td></tr>

                <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#cinfo.challenge_desc#</td>


                </tr>

                <tr><td class="link_small_gray">
				#cinfo.hub_name#
		        </td>
                <td class="feed_sub_header" style="font-weight: normal;" align=right><b>Ends</b>

                <cfif #cinfo.challenge_end# is "">
                 TBD
                <cfelse>
                 on #dateformat(cinfo.challenge_end,'mmm d, yyyy')#
                </cfif>

                </td>
		        </tr>

              </table>

		   </cfoutput>

           <cfif count LT cinfo.recordcount>

             <tr><td height=10></td></tr>
             <tr><td colspan=7><hr></td></tr>
             <tr><td height=10></td></tr>
           </cfif>

           <cfset count = count + 1>

          </cfloop>

          <tr><td height=10></td></tr>

		</cfif>

		</table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>