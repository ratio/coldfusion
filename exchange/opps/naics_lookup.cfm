<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">NAICS CODE LOOKUP</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>


 <tr>
    <td class="feed_sub_header" style="font-weight: normal;">
     The North American Industry Classification System (NAICS) is used by the United States, Canada, and Mexico to classify businesses by industry. Each business is classified into a six-digit NAICS code number based on the majority of activity at the business.
    </td></tr>

</table>

<cfoutput>
	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <form action="naics_lookup.cfm" method="post">
	 <tr>
		 <td class="feed_sub_header" width=225>Search for code or keyword</td>
		 <td><input type="text" name="naics" class="input_text" style="width: 250px;" required maxlength="299" <cfif isdefined("naics")>value="#naics#"</cfif>>
			 <input class="button_blue" type="submit" name="button" value="Search"></td>
	 </tr>
	 </form>
	 </table>
 </cfoutput>

<cfif isdefined("naics")>

 <cfquery name="lookup" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select naics_code, naics_code_description from naics
  where naics_code like '%#trim(naics)#%' or naics_code_description like '%#trim(naics)#%'
  order by naics_code_description
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfif lookup.recordcount is 0>

	 <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>

	<cfelse>

	 <tr>
	    <td class="feed_sub_header" width=100>Code</td>
	    <td class="feed_sub_header">Name / Description</td></tr>
	 </tr>

	 <cfset counter = 0>

	 <cfoutput query="lookup">

	 <cfif counter is 0>
	  <tr bgcolor="ffffff">
	 <cfelse>
	  <tr bgcolor="e0e0e0">
	 </cfif>

	 <td class="feed_sub_header">#naics_code#</td>
	 <td class="feed_sub_header">

     #replaceNoCase(ucase(naics_code_description), naics,"<span style='background:yellow'>#ucase(naics)#</span>","all")#

	 </td>

	 </tr>

	 <cfif counter is 0>
	  <cfset counter = 1>
	 <cfelse>
	  <cfset counter = 0>
	 </cfif>

	</cfoutput>

	</cfif>

 </table>


</cfif>

</center>

</body>
</html>

