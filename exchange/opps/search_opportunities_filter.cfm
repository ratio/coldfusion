<cfset from = dateadd('d',-30,now())>

 <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_agency) from fbo
  where fbo_agency is not null or fbo_agency <> ' '
  order by fbo_agency
 </cfquery>

 <cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_notice_type) from fbo
  order by fbo_notice_type
 </cfquery>

 <cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_setaside) from fbo
  order by fbo_setaside
 </cfquery>

 <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(fbo_id) as total from fbo
 </cfquery>

 <cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header" valign=top height=24>FILTER RESULTS (#ltrim(numberformat(total.total,'999,999'))#)</td></tr>
	  <tr><td height=10></td></tr>
	 </table>
 </cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <form action="set2.cfm" method="post">

          <tr>
            <td class="feed_option"><b>Department</b></td>
            <td class="feed_option">

                  <select name="opp_dept" class="input_select">
                   <option value=0 <cfif #session.opp_dept# is 0>selected</cfif>>All
                   <cfoutput query="dept">
                   <option value="#fbo_agency#" <cfif #session.opp_dept# is '#fbo_agency#'>selected</cfif>><cfif #fbo_agency# is "">Not Specified<cfelse>#fbo_agency#</cfif>
                   </cfoutput>
                  </select>

              </td>

<cfoutput>

              <td>
                  <td class="feed_option"><b>Keyword</b></td>
                  <td class="feed_option"><input type="text" name="opp_keyword" class="input_text" style="width: 200px;" maxlength="100" size="40" placeholder="Keyword" <cfif isdefined("session.opp_keyword")>value="#session.opp_keyword#"</cfif>></td>
              </td>

              <td>
                  <td class="feed_option"><b>NAICS Code *</b></td>
                  <td class="feed_option">
                  <input type="text" name="opp_naics" class="input_text" maxlength="299" <cfif isdefined("session.opp_naics")>value="#session.opp_naics#"</cfif>>
                  &nbsp;<a href="/exchange/opps/lookup_naics.cfm"><img src="/images/icon_search.png" height=15 alt="NAICS Code Lookup" title="NAICS Code Lookup"></a>
                  </td>

              <td>
                  <td class="feed_option"><b>From</b></td>
                  <td class="feed_option">
                  <input type="date" class="input_text" name="opp_posted_from" style="width: 135px;" value="#dateformat(session.opp_posted_from,'yyyy-mm-dd')#">

                  </td>

              <td>
                  <td class="feed_option"><b>Status</b></td>
                  <td class="feed_option" align=right>

			<select name="opp_status" class="input_select" style="width:85px">
				<option value=1 <cfif #session.opp_status# is 1>selected</cfif>>All
				<option value=2 <cfif #session.opp_status# is 2>selected</cfif>>Active
				<option value=3 <cfif #session.opp_status# is 3>selected</cfif>>Archived
			</select>

</cfoutput>

               &nbsp;&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Search">

               </td>

            </tr>

          <tr>
              <td class="feed_option"><b>Type</b></td>
              <td class="feed_option">
                  <select name="opp_type" class="input_select">
                   <option value=0 <cfif #session.opp_type# is 0>selected</cfif>>All
                   <cfoutput query="type">
                    <option value="#fbo_notice_type#" <cfif #session.opp_type# is #fbo_notice_type#>selected</cfif>>#fbo_notice_type#
                   </cfoutput>
                  </select>
              </td>


              <td>
                  <td class="feed_option"><b>Set Aside</b></td>
                  <td class="feed_option">
                                    <select name="opp_setaside" class="input_select">
				                     <option value=0>No Preference
				                     <cfoutput query="setaside">
				                     <option value="#fbo_setaside#" <cfif #session.opp_setaside# is #fbo_setaside#>selected</cfif>>#fbo_setaside#
				                     </cfoutput>
                  </select>

              </td>
<cfoutput>
              <td>
                  <td class="feed_option"><b>PSC Code *</b></td>
                  <td class="feed_option"><input type="text" name="opp_psc" class="input_text" maxlength="299" <cfif isdefined("session.opp_psc")>value="#session.opp_psc#"</cfif>>
                  &nbsp;<a href="/exchange/opps/lookup_psc.cfm"><img src="/images/icon_search.png" height=15 alt="PSC Code Lookup" title="PSC Code Lookup"></a>
              </td>
              <td>
                  <td class="feed_option"><b>To</b></td>
                  <td class="feed_option"><input type="date" name="opp_posted_to" class="input_text" style="width: 135px;" value="#dateformat(session.opp_posted_to,'yyyy-mm-dd')#"></td>
              </td>
</cfoutput>

                <td align=right colspan=3 class="feed_option"><a href="/exchange/opps/advanced.cfm?i=1" style="font-size: 12"><b>Basic Search</b></a></td></tr>

            </tr>
            <tr><td height=10></td></tr>
            <tr><td class="text_xsmall" colspan=10>* to search for multiple codes, seperate values with commas (i.e., 43412,33928, etc.)</td></tr>

          </form>

         </table>

