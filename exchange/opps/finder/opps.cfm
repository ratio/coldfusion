<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics
   where contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#session.opp_keyword#"')
   order by fbo_date_posted DESC
  </cfquery>


  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Active Opportunities</td><td class="feed_option" align=right><a href="results.cfm">Return</a></td></tr>
	     <tr><td height=5></td></tr>
	     <tr><td class="feed_option"><b>Displayed below are the active procurement opportunity for <cfoutput>"<i>#session.opp_keyword#</i>".</cfoutput></b></td></tr>
        </table>

		<cfparam name="URL.PageId" default="0">
		<cfset RecordsPerPage = 100>
		<cfset TotalPages = (fbo.Recordcount/RecordsPerPage)>
		<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		<cfset EndRow = StartRow+RecordsPerPage-1>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=5></td></tr>

         <tr><td colspan=2 align=right class="feed_option">

				  <cfif fbo.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>

             </td>
         </tr>

         <tr><td colspan=2><hr></td></tr>
         <tr><td height=20></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td></td>
            <td class="text_xsmall" align=center><b>Date Posted</b></td>
            <td class="text_xsmall"><b>Soliciation Number</b></td>
            <td class="text_xsmall"><b>Opportunity Name</b></td>
            <td class="text_xsmall"><b>Agency</b></td>
            <td class="text_xsmall"><b>Office</b></td>
            <td class="text_xsmall"><b>Location</b></td>
            <td class="text_xsmall"><b>Type</b></td>
            <td class="text_xsmall" width=100 align=center><b>NAICS</b></td>
            <td class="text_xsmall" width="75"><b>PSC Code</b></td>
            <td class="text_xsmall"><b>SB Set-Aside</b></td>
         </tr>

         <cfset counter = 0>

         <cfloop query="fbo">

		 <cfif CurrentRow gte StartRow >

			 <cfoutput>

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>
				 <td class="text_xsmall" valign=top align=center width=70><cfif #fbo.fbo_image_url# is "">&nbsp;<cfelse><a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#fbo.fbo_image_url#" width=50 vspace=10 border=0 alt="#fbo.fbo_agency#" title="#fbo.fbo_agency#"></a></cfif></td>
				 <td class="text_xsmall" valign=top width="75" align=center>#dateformat(fbo.fbo_date_posted,'mm/dd/yyyy')#</td>
				 <td class="text_xsmall" valign=top><a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #fbo.fbo_solicitation_number# is "">Not identified<cfelse>#fbo.fbo_solicitation_number#</cfif></a></td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_opp_name#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_agency#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_office#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_location#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_type#</td>
				 <td class="text_xsmall" valign=top align=center>#fbo.fbo_naics#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_class_code# - #fbo.class_code_name#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_setaside#</td>

			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			  <cfif CurrentRow eq EndRow>
			   <cfbreak>
			  </cfif>

         </cfif>

         </cfloop>
        </table>



























 	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>