<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfif session.view is 1>

	  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select id,action_date,award_id_piid,recipient_name,awarding_agency_name,awarding_sub_agency_name,awarding_office_name,
				  award_description,naics_code,recipient_duns,product_or_service_code,primary_place_of_performance_city_name,primary_place_of_performance_state_code,
				  type_of_set_aside,type_of_contract_pricing,period_of_performance_start_date,period_of_performance_current_end_date,
                  federal_action_obligation from award_data
	   where contains((award_description),'"#session.opp_keyword#"') and
			 action_date between '#dateformat(evaluate(now()-365),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#' and
			 federal_action_obligation > 0

    <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by primary_place_of_performance_city_name ASC
	<cfelseif #sv# is 50>
	 order by primary_place_of_performance_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 16>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 160>
	 order by awarding_agency_name DESC
	</cfif>

   <cfelse>
	 order by period_of_performance_current_end_date ASC
   </cfif>

   </cfquery>

  <cfelseif session.view is 2>

	  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select id,action_date,award_id_piid,recipient_name,awarding_agency_name,awarding_sub_agency_name,awarding_office_name,
				  award_description,naics_code,recipient_duns,product_or_service_code,primary_place_of_performance_city_name,primary_place_of_performance_state_code,
				  type_of_set_aside,type_of_contract_pricing,period_of_performance_start_date,period_of_performance_current_end_date,
                  federal_action_obligation from award_data
	   where contains((award_description),'"#session.opp_keyword#"') and
			 action_date between '#dateformat(evaluate(now()-90),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#' and
			 federal_action_obligation > 0 and
			 (modification_number = '0' or modification_number is null)

    <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by primary_place_of_performance_city_name ASC
	<cfelseif #sv# is 50>
	 order by primary_place_of_performance_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 16>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 160>
	 order by awarding_agency_name DESC
	</cfif>

   <cfelse>
	 order by period_of_performance_current_end_date ASC
   </cfif>

 	 </cfquery>

  <cfelseif session.view is 3>

	  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select id,action_date,award_id_piid,recipient_name,awarding_agency_name,awarding_sub_agency_name,awarding_office_name,
				  award_description,naics_code,recipient_duns,product_or_service_code,primary_place_of_performance_city_name,primary_place_of_performance_state_code,
				  type_of_set_aside,type_of_contract_pricing,period_of_performance_start_date,period_of_performance_current_end_date,
                  federal_action_obligation from award_data
	   where contains((award_description),'"#session.opp_keyword#"') and
			 period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and
			 federal_action_obligation > 0 and
			 (modification_number = '0' or modification_number is null)
    <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by primary_place_of_performance_city_name ASC
	<cfelseif #sv# is 50>
	 order by primary_place_of_performance_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 16>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 160>
	 order by awarding_agency_name DESC
	</cfif>

   <cfelse>
	 order by period_of_performance_current_end_date ASC
   </cfif>

	 </cfquery>

  <cfelseif session.view is 4>

	  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select id,action_date,award_id_piid,recipient_name,awarding_agency_name,awarding_sub_agency_name,awarding_office_name,
				  award_description,naics_code,recipient_duns,product_or_service_code,primary_place_of_performance_city_name,primary_place_of_performance_state_code,
				  type_of_set_aside,type_of_contract_pricing,period_of_performance_start_date,period_of_performance_current_end_date,
                  federal_action_obligation from award_data
	   where contains((award_description),'"#session.opp_keyword#"') and
			 period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and
			 federal_action_obligation > 0 and
			 (modification_number = '0' or modification_number is null)
    <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by primary_place_of_performance_city_name ASC
	<cfelseif #sv# is 50>
	 order by primary_place_of_performance_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 16>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 160>
	 order by awarding_agency_name DESC
	</cfif>

   <cfelse>
	 order by period_of_performance_current_end_date ASC
   </cfif>

	 </cfquery>

  <cfelseif session.view is 5>

	  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select id,action_date,award_id_piid,recipient_name,awarding_agency_name,awarding_sub_agency_name,awarding_office_name,
				  award_description,naics_code,recipient_duns,product_or_service_code,primary_place_of_performance_city_name,primary_place_of_performance_state_code,
				  type_of_set_aside,type_of_contract_pricing,period_of_performance_start_date,period_of_performance_current_end_date,
                  federal_action_obligation from award_data
	   where contains((award_description),'"#session.opp_keyword#"') and
			 period_of_performance_current_end_date between '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and
			 federal_action_obligation > 0 and
			 (modification_number = '0' or modification_number is null)
    <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by primary_place_of_performance_city_name ASC
	<cfelseif #sv# is 50>
	 order by primary_place_of_performance_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 16>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 160>
	 order by awarding_agency_name DESC
	</cfif>

   <cfelse>
	 order by period_of_performance_current_end_date ASC
   </cfif>

	 </cfquery>

  </cfif>


  <cfinclude template="/exchange/include/header.cfm">

   <cfif isdefined("export")>

	 <cfinclude template="/exchange/include/export_to_excel.cfm">

	</cfif>

<cfparam name="URL.PageId" default="0">
<cfset RecordsPerPage = 250>
<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
<cfset EndRow = StartRow+RecordsPerPage-1>

      <div class="main_box">

      <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Opportunity Detail</td><td class="feed_option" align=right><a href="results.cfm">Return</a></td></tr>
	     <tr><td height=5></td></tr>
	     <tr><td class="feed_option">Displayed below are the opportunity details for the area you selected.  To find out more information about the opportunity or contractor click on the contract number or the incumbent.</td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

      </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_option" colspan=16>No awards were found with the information you entered.</td></tr>
          <cfelse>

           <tr>
              <td class="feed_option" colspan=10>
              <cfoutput>

              <cfif session.view is 1>
              <b>Awarded contracts in the last year for "<i>#session.opp_keyword#</i></b>"

              <cfelseif session.view is 2>
              <b>Awarded contracts in the last 90 days for "<i>#session.opp_keyword#</i></b>"

              <cfelseif session.view is 3>
              <b>Upcoming opportunities in the next 90 days for "<i>#session.opp_keyword#</i></b>"

              <cfelseif session.view is 4>
              <b>Upcoming opportunities in the next 3-6 months for "<i>#session.opp_keyword#</i></b>"

              <cfelseif session.view is 5>
              <b>Upcoming opportunities in the next 6-12 months for "<i>#session.opp_keyword#</i></b>"


              </cfif>










              </cfoutput>
              </td>
              <td class="feed_option" align=right colspan=11><b><a href="detail.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a></b></td>
           </tr>

           <tr>

           <td><td class="feed_option" colspan=16 align=right>


				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
           </td></tr>

          <tr><td height=20></td></tr>

          <cfoutput>

			  <tr>
				 <td class="text_xsmall" width=75><a href="detail.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract ##</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Vendor</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=15<cfelse><cfif #sv# is 15>sv=150<cfelse>sv=15</cfif></cfif>"><b>Department</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=15<cfelse><cfif #sv# is 15>sv=150<cfelse>sv=15</cfif></cfif>"><b>Agency</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Office</b></a></td>

				 <td class="text_xsmall"><b>Award Description</b></td>


				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PSC</b></a></td>

				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>City</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>State</b></a></td>


				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Set Aside</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Pricing</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP Start</b></a></td>
				 <td class="text_xsmall"><a href="detail.cfm?<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PoP End</b></a></td>
				 <td class="text_xsmall" align=right><a href="detail.cfm?<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Obligation</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff">
			   <cfelse>
				<tr bgcolor="e0e0e0">

			   </cfif>

			   <cfoutput>

				   <td class="text_xsmall" valign=top width=120>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">#agencies.award_id_piid#</a></td>
				   <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#agencies.recipient_name#</a></td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_agency_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_sub_agency_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_office_name#</td>
				   <td class="text_xsmall" valign=top width=300>#replaceNoCase(agencies.award_description,session.opp_keyword,"<span style='background:yellow'>#ucase(session.opp_keyword)#</span>","all")#</td>
				   <td class="text_xsmall" valign=top width=75>#agencies.naics_code#</td>
				   <td class="text_xsmall" valign=top width=50>#agencies.product_or_service_code#</td>
				   <td class="text_xsmall" valign=top>#agencies.primary_place_of_performance_city_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.primary_place_of_performance_state_code#</td>
				   <td class="text_xsmall" valign=top width=50>#agencies.type_of_set_aside#</td>
				   <td class="text_xsmall" valign=top width=100>#agencies.type_of_contract_pricing#</td>
				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.period_of_performance_start_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top align=right width=75>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

		  <cfset tot = tot + #federal_action_obligation#>

 </cfif>

								  <cfif CurrentRow eq EndRow>
								   <cfbreak>
								  </cfif>

          </cfloop>

          <tr><td colspan=17><hr></td></tr>
          <tr>
             <td class="feed_option"><b>Total</b></td>
             <td class="feed_option" colspan=16 align=right><b><cfoutput>#numberformat(tot,'$999,999,999')#</cfoutput></b></td>
          </tr>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

 	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html> --->