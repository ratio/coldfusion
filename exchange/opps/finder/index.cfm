<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Opportunity Finder</td><td class="feed_option" align=right></td></tr>
	     <tr><td height=5></td></tr>
	     <tr><td class="feed_option">The Opportunity Finder allows you to enter keywords about your company, your interests, and focus areas to find opportunities to accelerate revenue and growth.</td></tr>
         <tr><td height=10></td></tr>
        </table>

         <form action="set.cfm" method="post">
			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_option"><b>Search for Opportunity</b>&nbsp;&nbsp;
				 <input type="text" name="keyword" size=30 required>&nbsp;&nbsp;
				 <input class="button_blue" style="font-size: 11px; height: 22px; width: 65px;" type="submit" name="button" value="Search">
			 </td></tr>
             </table>
         </form>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>