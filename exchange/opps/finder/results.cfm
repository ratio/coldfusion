<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- FBO Results --->

  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics
   where fbo_id > 0
   and contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#session.opp_keyword#"')
   and (fbo_notice_type = 'Presolicitation' or fbo_notice_type = 'Solicitation' or fbo_notice_type = 'Combined Synopsis/Solicitation')
   order by fbo_date_posted DESC
  </cfquery>

  <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select count(id) as awards, sum(federal_action_obligation) as total from award_data
   where contains((award_description),'"#session.opp_keyword#"') and
         action_date between '#dateformat(evaluate(now()-365),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#' and
         federal_action_obligation > 0
  </cfquery>

  <cfquery name="awards_last90" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select count(id) as awards, sum(federal_action_obligation) as total from award_data
   where contains((award_description),'"#session.opp_keyword#"') and
         action_date between '#dateformat(evaluate(now()-90),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#' and
         federal_action_obligation > 0 and
         (modification_number = '0' or modification_number is null)
  </cfquery>

  <cfquery name="awards_next90" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select count(id) as awards, sum(federal_action_obligation) as total from award_data
   where contains((award_description),'"#session.opp_keyword#"') and
         period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and
         federal_action_obligation > 0 and
         (modification_number = '0' or modification_number is null)
  </cfquery>

  <cfquery name="awards_next180" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select count(id) as awards, sum(federal_action_obligation) as total from award_data
   where contains((award_description),'"#session.opp_keyword#"') and
         period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and
         federal_action_obligation > 0 and
         (modification_number = '0' or modification_number is null)
  </cfquery>

  <cfquery name="awards_next365" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select count(id) as awards, sum(federal_action_obligation) as total from award_data
   where contains((award_description),'"#session.opp_keyword#"') and
         period_of_performance_current_end_date between '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and
         federal_action_obligation > 0 and
         (modification_number = '0' or modification_number is null)
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

      <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Opportunity Results - "<i>#session.opp_keyword#</i>"</td>
	     <td class="feed_option" align=right>
	     <cfif isdefined("session.location")>
	      <a href="/exchange/">
	     <cfelse>
	     <a href="index.cfm">
	     </cfif>Return</a></td></tr>
	     <tr><td height=5></td></tr>
	     <tr><td class="feed_option">Displayed below are the opportunities we found.</td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td>&nbsp;</td></tr>
        </table>

      </cfoutput>

      <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>
			   <td></td>
			   <td class="feed_header" colspan=2 align=center>Past Awarded Contracts</td>
			   <td class="feed_header" colspan=3 align=center>Upcoming Contracts</td>
			   <td></td>
		    </tr>

		   <tr><td>&nbsp;</td></tr>


			   <tr>
				  <td class="feed_header" align=center>Active Opportunities</td>
				  <td class="feed_header" align=center>Last Year</td>
				  <td class="feed_header" align=center>Last 90 Days</td>
				  <td class="feed_header" align=center>Next 90 Days</td>
				  <td class="feed_header" align=center>3 - 6 Months</td>
				  <td class="feed_header" align=center>6 - 12 Months</td>
			   </tr>

			   <tr><td height=5></td></tr>

			   <tr>
				  <td class="big_number" align=center><a href="opps.cfm">#fbo.recordcount#</a></td>
				  <td class="big_number" align=center><a href="set_detail.cfm?view=1">#awards.awards#</a></td>
				  <td class="big_number" align=center><a href="set_detail.cfm?view=2">#awards_last90.awards#</a></td>
				  <td class="big_number" align=center><a href="set_detail.cfm?view=3">#awards_next90.awards#</a></td>
				  <td class="big_number" align=center><a href="set_detail.cfm?view=4">#awards_next180.awards#</a></td>
				  <td class="big_number" align=center><a href="set_detail.cfm?view=5">#awards_next365.awards#</a></td>
			   </tr>

			   <tr><td height=10></td></tr>

			   <tr>
				  <td class="feed_header" align=center></td>
				  <td class="feed_header" align=center>#numberformat(awards.total,'$999,999,999')#</td>
				  <td class="feed_header" align=center>#numberformat(awards_last90.total,'$999,999,999')#</td>
				  <td class="feed_header" align=center>#numberformat(awards_next90.total,'$999,999,999')#</td>
				  <td class="feed_header" align=center>#numberformat(awards_next180.total,'$999,999,999')#</td>
				  <td class="feed_header" align=center>#numberformat(awards_next365.total,'$999,999,999')#</td>

			   </tr>

			   <tr><td>&nbsp;</td></tr>

			</table>

        </cfoutput>

  	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>