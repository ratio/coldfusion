<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.grant_recent")>
 <cfset #session.grant_recent# = 1>
</cfif>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<cfif not isdefined("session.grant_status")>
 <cfset session.grant_status = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>


<style>
/* Style The Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  right: 0;
  font-size: 13px;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
</style>

<cfset from = dateadd('d',-2,now())>

<cfquery name="recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_grant
 left join orgname on orgname_name = department

 <cfif session.grant_recent is 1>
  where posteddate between '#dateformat(dateadd('d',-7,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
 <cfelseif session.grant_recent is 3>
  where posteddate between '#dateformat(dateadd('d',-30,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
 <cfelseif session.grant_recent is 4>
  where posteddate between '#dateformat(dateadd('d',-90,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
 </cfif>

	<cfif #sv# is 1>
	 order by department ASC, agencyname ASC
	<cfelseif #sv# is 10>
	 order by department DESC, agencyname ASC
	<cfelseif #sv# is 2>
	 order by opportunitynumber ASC
	<cfelseif #sv# is 20>
     order by opportunitynumber DESC
	<cfelseif #sv# is 3>
	 order by opportunitytitle ASC
	<cfelseif #sv# is 30>
	 order by opportunitytitle DESC
	<cfelseif #sv# is 4>
	 order by fundinginstrumenttype ASC
	<cfelseif #sv# is 40>
	 order by fundinginstrumenttype DESC
	<cfelseif #sv# is 5>
	 order by awardfloor ASC
	<cfelseif #sv# is 50>
	 order by awardfloor DESC
	<cfelseif #sv# is 6>
	 order by awardceiling ASC
	<cfelseif #sv# is 60>
	 order by awardceiling DESC
	<cfelseif #sv# is 7>
	 order by posteddate ASC
	<cfelseif #sv# is 70>
	 order by posteddate DESC
	</cfif>

</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      </td><td valign=top>

      <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_grants3.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
          </div>

	  <div class="tab_not_active">
	   <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></a></span>
	  </div>

	  <div class="tab_not_active">
	   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.grants_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
	  </div>

	  </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_grants.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="grant_refresh.cfm" method="post">
         <cfoutput>
			 <tr><td class="feed_header">Recently Posted <cfif recent.recordcount is 0><cfelse>(#ltrim(numberformat(recent.recordcount,'99,999'))#)</cfif></td>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=300><b>Posted</b>&nbsp;&nbsp;
				 <select name="grant_recent" class="input_select" style="width:125px" onchange="form.submit()">
					<option value=1 <cfif #session.grant_recent# is 1>selected</cfif>>This Week
					<option value=2 <cfif #session.grant_recent# is 2>selected</cfif>>This Month
					<option value=3 <cfif #session.grant_recent# is 3>selected</cfif>>This Quarter
				 </select>

				</td></tr>
				</cfoutput>
            </form>

         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

         <cfif isdefined("u")>
          <cfif u is 7>
           <tr><td colspan=3 class="feed_sub_header" style="color: green;">Opportunity has successfully been pinned to your board.</td></tr>
          </cfif>
         </cfif>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif recent.recordcount is 0>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No recently posted Grants were found.</td></tr>

        <cfelse>

			 <tr height=50>

				<td></td>
				<td class="feed_option"><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>ORGANIZATION</b></a><cfif isdefined("sv") and sv is 1>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option"><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TITLE</b></a><cfif isdefined("sv") and sv is 3>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option"><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>OPPORTUNITY #</b></a><cfif isdefined("sv") and sv is 2>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" align=center><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>FUNDING</b></a><cfif isdefined("sv") and sv is 4>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" align=right><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>FLOOR</b></a><cfif isdefined("sv") and sv is 5>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" align=right><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>CEILING</b></a><cfif isdefined("sv") and sv is 6>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" align=right><a href="/exchange/opps/grants.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>POSTED</b></a><cfif isdefined("sv") and sv is 7>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
			    <td></td>
			 </tr>

			 <cfset counter = 0>

			 <cfloop query="recent">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=70>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=70>
			 </cfif>

			 <cfoutput>

		        <td width=70 class="table_row" valign=middle><a href="/exchange/include/grant_new_information.cfm?id=#recent.opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #orgname_logo# is ""><img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10><cfelse><img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10></cfif></a></td>
                <td class="feed_option" valign=middle width=300><a href="/exchange/include/grant_new_information.cfm?id=#recent.opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(department)#</b></a><br><a href="/exchange/include/grant_new_information.cfm?id=#recent.opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencyname#</b></a></td>
				 <td class="feed_option" valign=middle width=450><b><a href="/exchange/include/grant_new_information.cfm?id=#recent.opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(recent.opportunitytitle)#</a></td>
				 <td class="feed_option" valign=middle width=120>#recent.opportunitynumber#</td>
				 <td class="feed_option" valign=middle align=center width=75>#recent.fundinginstrumenttype#</td>
				 <td class="feed_option" valign=middle align=right width=125>#numberformat(recent.awardfloor,'$999,999,999')#</td>
				 <td class="feed_option" valign=middle align=right width=125>#numberformat(recent.awardceiling,'$999,999,999')#</td>
				 <td class="feed_option" valign=middle align=right width=120>#dateformat(recent.posteddate,'mm/dd/yyyy')#</td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#recent.opp_grant_id#&t=grant','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			     </td>

              </cfoutput>


			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfloop>

        </cfif>

        </table>

      </div>

      </td>

      </tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>