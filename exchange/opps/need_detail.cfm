<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfquery name="follow" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select follow_id from follow
 where follow_need_id = #need_id# and
       follow_by_usr_id = #session.usr_id#
</cfquery>

<cfquery name="who" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(follow_id) as total from follow
 where follow_need_id = #need_id#
</cfquery>

<cfquery name="recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 insert into recent
 (recent_usr_id, recent_need_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#session.usr_id#,#need_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<cfquery name="need" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_created_by
 where need_id = #need_id#
</cfquery>

<body class="body">

<!--- Lookups --->

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><cfoutput>#ucase(need.need_name)#</cfoutput></td>
		     <td class="feed_sub_header" align=right>

		     <cfoutput>

                 <a href="##" onclick="window.open('question.cfm?need_id=#need_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;">Ask Question</a>
                 &nbsp;|&nbsp;

				 <cfif follow.recordcount is 0>
				  <a href="/exchange/opps/need_follow.cfm?need_id=#need_id#&f=y">Follow</a>&nbsp;|&nbsp;
				 <cfelse>
				  <a href="/exchange/opps/need_follow.cfm?need_id=#need_id#&f=n">Unfollow</a>&nbsp;|&nbsp;
				 </cfif>

		     </cfoutput>

		     <a href="/exchange/opps/needs.cfm">Return</a>


		     </td></tr>
		 <tr><td colspan=2><hr></td></tr>

	     <cfif isdefined("u")>
	      <cfif u is 1>
	       <tr><td class="feed_sub_header" style="color: green;" colspan=2>You are now following this Need.</td></tr>
          <cfelseif u is 2>
	       <tr><td class="feed_sub_header" style="color: green;" colspan=2>You are no longer following this Need.</td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>


		</table>

      <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=middle width=100>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif need.need_image is "">
				  <img src="#image_virtual#/stock_need.png" width=75 vspace=10>
				<cfelse>
				  <img src="#media_virtual#/#need.need_image#" width=75>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                <td class="feed_sub_header">ID / ##</td>
                <td class="feed_sub_header">ORGANIZATION</td>
                <td class="feed_sub_header" align=center>FOLLOWERS</td>
                <td class="feed_sub_header" align=right>SOLVE BY</td>
             </tr>

             <tr>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" width=75><cfif #need.need_number# is "">TBD<cfelse>#ucase(need.need_number)#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #need.need_organization# is "">Not Identified<cfelse>#ucase(need.need_organization)#</cfif></td>
                <td class="feed_sub_header" valign=top align=center style="font-weight: normal;" width=100>#who.total#</td>
                <td class="feed_sub_header" valign=top align=right style="font-weight: normal;" width=100><cfif #need.need_solve_by# is "">TBD<cfelse>#dateformat(need.need_solve_by,'mm/dd/yyyy')#</cfif></td>
             </tr>

         </table>

         </td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=10><hr></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_sub_header">DESCRIPTION</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">TODAY'S CHALLANGES</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #need.need_desc# is "">Not Provided<cfelse>#need.need_desc#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #need.need_challenges# is "">Not Provided<cfelse>#need.need_challenges#</cfif></td>
             </tr>

             <tr><td class="feed_sub_header">USE CASE SCENARIO</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">FUTURE STATE SOLUTION</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #need.need_use_case# is "">Not Provided<cfelse>#need.need_use_case#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #need.need_future_state# is "">Not Provided<cfelse>#need.need_future_state#</cfif></td>
             </tr>

             <tr>
                 <td class="feed_sub_header">KEYWORDS</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">REFERENCE URL</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #need.need_keywords# is "">Not Provided<cfelse>#need.need_keywords#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #need.need_url# is "">Not Provided<cfelse><a href="#need.need_url#" target="_blank" rel="noopener" rel="noreferrer">#need.need_url#</a></cfif></td>
             </tr>

         </table>

         </cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td><hr></td></tr>
           <tr><td class="feed_sub_header">REQUIREMENTS</td></tr>
         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfquery name="req" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select * from req
		  where req_need_id = #need_id#
		  order by req_priority DESC
		 </cfquery>

			<cfif req.recordcount is 0>

			   <tr><td class="feed_sub_header" colspan=4 style="font-weight: normal;">No Requirements have been added.</td></tr>

		    <cfelse>

				<tr>
				 <td class="feed_sub_header">ID / #</td>
				 <td class="feed_sub_header">NAME</td>
				 <td class="feed_sub_header">DESCRIPTION</td>
				 <td class="feed_sub_header" align=right>PRIORITY</td>
				</tr>

				<cfset counter = 0>

                       <cfloop query="req">

                       <cfoutput>

						 <cfif counter is 0>
						 <tr bgcolor="FFFFFF">
						 <cfelse>
						 <tr bgcolor="E0E0E0">
						 </cfif>

							<td class="feed_sub_header" valign=top width=50 style="font-weight: normal;"><cfif #req.req_number# is "">TBD<cfelse>#req.req_number#</cfif></td>
							<td class="feed_sub_header" valign=top width=200 style="font-weight: normal;"><b><cfif req.req_name is "">NOT PROVIDED<cfelse>#ucase(req.req_name)#</cfif></b></td>
							<td class="feed_sub_header" valign=top width=500 style="font-weight: normal;"><cfif req.req_desc is "">Not Provided<cfelse>#replace(req.req_desc,"#chr(10)#","<br>","all")#</cfif><cfif req.req_keywords is not ""><br><i><b>Keywords - </b>#req.req_keywords#</i></cfif>

                            <cfif req.req_attachment is not "">
                             <br><b>Attachment - </b><a href="#media_virtual#/#req.req_attachment#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">Download Attachment</a>
                            </cfif>

                            <cfif req.req_link is not "">
                             <br><b>URL: </b> - <a href="#req.req_link#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#req.req_link#</a>
                            </cfif>

                            </td>

							<td class="feed_sub_header" valign=top width=100 style="font-weight: normal;" align=right>

							<cfif #req.req_priority# is 1>
							 Low
							<cfelseif #req.req_priority# is 2>
							 Medium
							<cfelseif #req.req_priority# is 3>
							 High
							<cfelse>
							 Critical
							</cfif>

							</td>

						 </tr>

						 <cfif counter is 0>
						  <cfset counter = 1>
						 <cfelse>
						  <cfset counter = 0>
						 </cfif>

                       </cfoutput>

                       </cfloop>

           <cfoutput>
           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>

             <tr>
                 <td class="feed_sub_header" width=200>POINT OF CONTACT</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #need.need_poc_name# is "">Not Provided<cfelse>#need.need_poc_name#</cfif></td></tr>
             </tr>

             <cfif #need.need_poc_title# is not "">
             <tr>
                 <td class="feed_sub_header" width=200>TITLE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top>#need.need_poc_title#</td></tr>
             </tr>
             </cfif>

             <tr>
                 <td class="feed_sub_header" width=200>EMAIL</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #need.need_poc_email# is "">Not Provided<cfelse>#need.need_poc_email#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>PHONE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #need.need_poc_phone# is "">Not Provided<cfelse>#need.need_poc_phone#</cfif></td></tr>
             </tr>

           </table>
           </cfoutput>

		    </cfif>

		<cfquery name="questions" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from question
		 where question_public = 1 and
		 question_need_id = #need_id#
		 order by question_date_submitted DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td colspan=2><hr></td></tr>
		 <tr>
			 <td class="feed_sub_header">QUESTIONS & ANSWERS</td>
			 <td class="feed_sub_header" align=right></td></tr>
	   </table>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfif questions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been submitted or posted for this Need.</td></tr>
         <cfelse>

         <cfoutput query="questions">

          <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><b>Q.  #ucase(questions.question_subject)#</b><br>#questions.question_text#</td>
              <td class="feed_sub_header" valign=top align=right>#dateformat(questions.question_updated,'mm/dd/yyyy')#, #timeformat(questions.question_updated)#</td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>A.  </b>#questions.question_answer#</td></tr></tr>
          <tr><td colspan=2><hr></td></tr>

         </cfoutput>

         </cfif>

           </table>












      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>