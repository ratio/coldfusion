<cfset from = dateadd('d',-30,now())>
 <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(agencyname) from opp_grant
  order by agencyname
 </cfquery>

 <cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fundinginstrumenttype) from opp_grant
  order by fundinginstrumenttype
 </cfquery>

 <cfquery name="updated" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select max(import_date) as updated from opp_grant
 </cfquery>

 <cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header" height=24>Search Grants</td>
	      <td class="feed_sub_header" align=right valign=absmiddle>Last Updated: #dateformat(updated.updated,'mmmm dd, yyyy')#</td></tr>
	  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Grants are opportunities to fund your research and development to assist the Federal Government in solving specific challenges or needs.</td></tr>
	  <tr><td colspan=2><hr></td></tr>
	 </table>
 </cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="/exchange/opps/set_grants.cfm" method="post">
          <tr>

             <td class="feed_option"><b>Keyword</b></td>
		      <cfoutput>
				  <td class="feed_option"><input type="text" name="grant_keyword" class="input_text" style="width: 200px;" maxlength="100" size="40" placeholder="i.e., machine learning"></td>
			  </cfoutput>

             <td class="feed_option"><b>Source</b></td>
             <td class="feed_option">
                  <select name="grant_agency" class="input_select" style="width: 250px;">
                   <option value=0>ALL
                   <cfoutput query="agency">
                   <option value="#agencyname#" <cfif isdefined("session.grant_agency") and #session.grant_agency# is '#agencyname#'>selected</cfif>><cfif #agencyname# is "">NOT SPECIFIED<cfelse>#ucase(agencyname)#</cfif>
                   </cfoutput>
                  </select>
              </td>
              <td class="feed_option"><b>Funding Type</b></td>
              <td class="feed_option">
                  <select name="grant_type" class="input_select" style="width: 150px;">
                   <option value=0>ALL
                   <cfoutput query="type">
                    <option value="#fundinginstrumenttype#" <cfif isdefined("session.grant_type") and #session.grant_type# is #fundinginstrumenttype#>selected</cfif>>#ucase(fundinginstrumenttype)#
                   </cfoutput>
                  </select>
              </td>
			  <td>
				  <td class="feed_option"><b>Status</b></td>
				  <td class="feed_option">
					<select name="grant_status" class="input_select" style="width:140px">
					<option value=1 <cfif isdefined("session.grant_status") and #session.grant_status# is 1>selected</cfif>>ALL
					<option value=2 <cfif isdefined("session.grant_status") and #session.grant_status# is 2>selected</cfif>>ACTIVE
					<option value=3 <cfif isdefined("session.grant_status") and #session.grant_status# is 3>selected</cfif>>ARCHIVED
				</select>
		      </td>

			  <td><input class="button_blue" type="submit" name="button" value="Search"></td>


            </tr>
          </form>
        </table>

