<cfinclude template="/exchange/security/check.cfm">

  <cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   insert into opp_search
   (
   opp_search_name,
   opp_search_posted_from,
   opp_search_posted_to,
   opp_search_dept,
   opp_search_naics,
   opp_search_psc,
   opp_search_setaside,
   opp_search_type,
   opp_search_keyword,
   opp_search_usr_id
   )
   values
   (
   '#search_name#',
   <cfif session.posted_from is "">null<cfelse>'#session.posted_from#'</cfif>,
   <cfif session.posted_to is "">null<cfelse>'#session.posted_to#'</cfif>,
   '#session.dept#',
   '#session.naics#',
   '#session.psc#',
   '#session.setaside#',
   '#session.type#',
   '#session.keyword#',
    #session.usr_id#
    )
 </cfquery>

 <cflocation URL="search.cfm?u=1" addtoken="no">