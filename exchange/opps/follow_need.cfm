<!--- Hub Profile --->

<cfquery name="follow" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from follow
 left join need on need_id = follow_need_id
 where follow_by_usr_id = #session.usr_id# and
       follow_need_id is not null
</cfquery>

<div class="left_box" style="margin-top: 25px;">

  <center>
  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header" colspan=2><b>FOLLOWING</a></b></td></tr>
  <tr><td colspan=2><hr></td></tr>

  <cfset count = 1>

  <cfif #follow.recordcount# is 0>

   <tr><td class="feed_sub_header" style="font-weight: normal;">You are not following any Needs.</td></tr>

  <cfelse>

  <cfoutput query="follow">

   <tr><td width=50 valign=top>

		<cfif #follow.need_image# is "">
				<a href="/exchange/opps/need_detail.cfm?need_id=#follow.need_id#"><img src="/images/stock_need.png" width=30 border=0></a>
		<cfelse>
				<a href="/exchange/opps/need_detail.cfm?need_id=#follow.need_id#"><img src="#media_virtual#/#follow.need_image#" width=30 border=0></a>
		</cfif>

   </td>
   <td class="link_med_blue" valign=top><a href="/exchange/opps/need_detail.cfm?need_id=#follow.need_id#">#follow.need_name#</a></td></tr>

   <cfif count is not #follow.recordcount#>
   <tr><td colspan=2><hr></td></tr>
   </cfif>
   <cfset count = count + 1>

  </cfoutput>

  </cfif>

  </table>
  </center>

</div>