<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_header">Today's Opportunities</td>
               <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>

           <tr>

           <tr><td height=10></td></tr>

             <td class="feed_header">

             <cfif t is 1>

			 <cfquery name="naics_code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from naics
			  where naics_code = '#naics#'
			 </cfquery>

             <b><cfoutput>#naics_code.naics_code# - #naics_code.naics_code_description#</cfoutput></b>
             <cfelseif t is 2>

			 <cfquery name="class_code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from class_code
			  where class_code_code = '#pcode#'
			 </cfquery>

             <b><cfoutput>#class_code.class_code_name#</cfoutput></b>
             <cfelseif t is 3>

			 <cfquery name="class_code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from class_code
			  where class_code_code = '#scode#'
			 </cfquery>

             <b><cfoutput>#class_code.class_code_name#</cfoutput></b>
             </cfif>

             </td><td align=right class="feed_option">

             <b>Filter By Keyword</b>&nbsp;&nbsp;
             <input type="text" name="opp_keyword" size=20 <cfif isdefined("session.opp_keyword")>value="<cfoutput>#session.opp_keyword#</cfoutput>"</cfif>>&nbsp;&nbsp;
             <input class="button_blue" type="submit" name="button" value="Filter" vspace=10 style="width: 50px; height: 22px;">

             </td></tr>


             <tr><td class="feed_option" colspan=2 align=right><a href="search.cfm">Advanced Search</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="archives.cfm">Archives</a></td></tr>

             <tr><td colspan=2><hr></td></tr>

         </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfquery name="opps" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select * from opp
		  where opp_code = '#pcode#' and
		        (opp_title = 'SOLICITATIONS' or opp_title = 'SOURCES SOUGHT')
		  order by opp_title
		 </cfquery>

		 <cfoutput query="opps">
		  <tr>
		     <td class="feed_option" width=250 valign=top>
             <b>#opp_title#</b><br>
                #opp_notice_type#
             <br>
             <br>
             <b>Solicitation Number</b><br>
		        #opps.opp_solicitation_number#<br>
		     <br><br>
		     <b>Small Business Set-Aside</b><br>#opp_setaside#
		     <br>
		     <br>
		     <b>Date Posted</b><br>
		     #dateformat(opp_notice_date,'mm/dd/yyyy')#<br><br>
		     <a href="#opps.opp_url#" target="_blank" rel="noopener" rel="noreferrer"><b>Full Solicitation</b></a>
		     </td>
		     <td class="feed_option" valign=top><b>#opps.opp_contracting_office#</b><br>#opp_name#

		     <br><br>
		     <b>Point of Contact</b><br>#opps.opp_poc# (#opp_poc_email#)<br>

		     </td>
		  </tr>

		  <tr><td colspan=2><hr></td></tr>

		 </cfoutput>

		 </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>