<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="edit" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select* from opp_search
 where opp_search_id = #opp_search_id# and
       opp_search_usr_id = #session.usr_id#
</cfquery>

<cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_notice_type) from fbo
 order by fbo_notice_type
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_agency) from fbo
 order by fbo_agency
</cfquery>

 <cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_setaside_original) from fbo
  where (fbo_setaside_original <> '' and fbo_setaside_original <> 'N/A')
  order by fbo_setaside_original
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">Edit Search</td><td class="feed_sub_header" align=right>
	     <cfif l is 1>
	     	<a href="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#">Return</a>
	     <cfelse>
	     	<a href="/exchange/opps/go_expanded.cfm?opp_search_id=#opp_search_id#">Return</a>
	     </cfif>
	     </td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">To change your saved search parameters, please update the fields below and click save.</td></tr>
         <tr><td height=5></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=5></td></tr>
        </table>
        </cfoutput>


         <form action="/exchange/opps/edit_db.cfm" method="post">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <cfoutput>


         <tr><td class="feed_sub_header"><b>Search Name</b></td>
             <td class="feed_option"><input type="text" class="input_text"  style="width: 400px;" name="opp_search_name" style="width:300px;" value="#edit.opp_search_name#" required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="opp_search_desc" class="input_textarea" cols=70 rows=5>#edit.opp_search_desc#</textarea></td>
             </td></tr>

      </cfoutput>

         <tr><td class="feed_sub_header"><b>Type</b></td>
             <td class="feed_option">
                <select name="opp_search_type" class="input_select" style="width: 400px;">
                 <option value=0>All
                   <cfoutput query="type">
                     <option value="#fbo_notice_type#" <cfif #fbo_notice_type# is #edit.opp_search_type#>selected</cfif>>#fbo_notice_type#
                   </cfoutput>
                 </select>
              </td></tr>

         <tr><td class="feed_sub_header"><b>Department</b></td>
             <td class="feed_option">
                <select name="opp_search_dept" class="input_select" style="width: 400px;">
                 <option value=0>All
                   <cfoutput query="agency">
                     <option value="#fbo_agency#" <cfif #edit.opp_search_dept# is #fbo_agency#>selected</cfif>>#fbo_agency#
                   </cfoutput>
                 </select>
              </td></tr>

          <cfoutput>

          <tr><td class="feed_sub_header">From</td>
              <td><input type="date" name="opp_search_posted_from" class="input_date" value="#edit.opp_search_posted_from#">

              <span class="feed_sub_header">&nbsp;&nbsp;&nbsp;&nbsp;<b>To</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><input type="date" name="opp_search_posted_to" class="input_date" value="#edit.opp_search_posted_to#"></td>
              </td></tr>

          <tr><td class="feed_sub_header"><b>NAICS Code(s) *</b></td>
              <td class="feed_option"><input type="text" name="opp_search_naics" class="input_text" style="width:400px;" maxlength="299" value="#edit.opp_search_naics#">
              &nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup" style="cursor: pointer;" onclick="window.open('naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">
              </td></tr>

          <tr><td class="feed_sub_header"><b>Product or Service Code(s) *</b></td>
              <td class="feed_option"><input type="text" name="opp_search_psc" class="input_text" style="width: 400px;" maxlength="299" value="#edit.opp_search_psc#">
               &nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="PSC Lookup" title="PSC Lookup" style="cursor: pointer;" onclick="window.open('psc_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">
              </td></tr>

          </cfoutput>

          <tr><td class="feed_sub_header"><b>Set Aside</b></td>
              <td class="feed_option">
				<select name="opp_search_setaside" class="input_select" style="width:400px;">
				 <option value=0>No Preference
				 <cfoutput query="setaside">
				 <option value="#fbo_setaside_original#" <cfif #fbo_setaside_original# is #edit.opp_search_setaside#>selected</cfif>>#fbo_setaside_original#
				 </cfoutput>
                </select></td></tr>

           <cfoutput>

           <tr><td class="feed_sub_header"><b>Keyword</b></td>
               <td class="feed_option"><input type="text" name="opp_search_keyword" class="input_text" style="width:400px;" value="#edit.opp_search_keyword#">
               </td></tr>

           <tr><td class="feed_sub_header"><b>Status</b></td>
               <td class="feed_option">

				<select name="opp_search_status" class="input_select" style="width:85px">
					<option value=1 <cfif #edit.opp_search_status# is 1>selected</cfif>>All
					<option value=2 <cfif #edit.opp_search_status# is 2>selected</cfif>>Active
					<option value=3 <cfif #edit.opp_search_status# is 3>selected</cfif>>Archived
				</select>

               </td></tr>

            <input type="hidden" name="opp_search_id" value=#opp_search_id#>
            <input type="hidden" name="l" value=#l#>

           </cfoutput>

          <tr><td height=5></td></tr>
          <tr><td></td><td class="link_small_gray">* - Entering nothing will return all.  To search multiple values, seperate codes with a commma and no spaces.  For a complete code list, click here - <a href="https://www.census.gov/cgi-bin/sssd/naics/naicsrch?chart=2017" target="_blank" rel="noopener" rel="noreferrer">NAICS Code list</a>, or <a href="https://www.PROCUREMENTS/index?s=getstart&mode=list&tab=list&tabmode=list&static=faqs##q4" target="_blank" rel="noopener" rel="noreferrer">Product or Service Code list</a>.</td></tr>
          <tr><td height=5></td></tr>

          <tr><td height=10></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr><td></td><td>

          <input class="button_blue_large" type="submit" name="button" value="Save">&nbsp;&nbsp;
          <input class="button_blue_large" type="submit" name="button" value="Delete" onclick="return confirm('Delete Saved Search?\r\nAre you sure you want to delete this saved search?');">

          </td></tr>

        </table>

        </form>

	  </div>

	  </td></tr>

    </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>