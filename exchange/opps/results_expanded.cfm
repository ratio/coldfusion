<cfinclude template="/exchange/security/check.cfm">

<cfset session.view = 2>

<cfif not isdefined("sv")>
 <cfset sv = 100>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<!--- Lookups --->

<cfset from = dateadd('d',-2,now())>

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_agency) from fbo
 where fbo_agency is not null or fbo_agency <> ' '
 order by fbo_agency
</cfquery>

<cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_type) from fbo
 order by fbo_type
</cfquery>

<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_setaside_original) from fbo
 order by fbo_setaside_original
</cfquery>

<cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(fbo_id) as total from fbo
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

   select fbo_id, fbo_solicitation_number, fbo_dept, fbo_agency, fbo_office, fbo_opp_name, orgname_logo, fbo_desc, class_code_name, fbo_notice_type, fbo_naics_code,  fbo_setaside_original, fbo_pub_date from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics_code
   left join orgname on orgname_name = fbo_agency
   where (fbo_id > 0

   <cfif session.opp_posted_from is "" and session.opp_posted_to is "">
   <cfelseif session.opp_posted_from is not "" and session.opp_posted_to is not "">
    and (fbo_pub_date >= '#session.opp_posted_from#' and fbo_pub_date <= '#session.opp_posted_to#')
   <cfelseif session.opp_posted_from is not "" and session.opp_posted_to is "">
    and (fbo_pub_date >= '#session.opp_posted_from#')
   <cfelseif session.opp_posted_from is "" and session.opp_posted_to is not "">
    and (fbo_pub_date <= '#session.opp_posted_to#')
   </cfif>

   <cfif session.opp_setaside is not 0>
    and fbo_setaside_original = '#session.opp_setaside#'
   </cfif>

   <cfif session.opp_dept is not 0>
    and fbo_agency = '#session.opp_dept#'
   </cfif>

   <cfif session.opp_type is not 0>
    and fbo_notice_type = '#session.opp_type#'
   </cfif>

   <cfif #listlen(session.opp_naics)# GT 0>
		<cfif #listlen(session.opp_naics)# GT 1>
		<cfset ncounter = 1>
		and (
         <cfloop index="nc" list="#session.opp_naics#">
           (fbo_naics_code = '#nc#')
           <cfif ncounter LT #listlen(session.opp_naics)#> or</cfif>
           <cfset ncounter = ncounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_naics_code = '#session.opp_naics#'
		</cfif>
    </cfif>

   <cfif #listlen(session.opp_psc)# GT 0>
		<cfif #listlen(session.opp_psc)# GT 1>
		<cfset pcounter = 1>
		and (
         <cfloop index="pc" list="#session.opp_psc#">
           (fbo_class_code = '#pc#')
           <cfif pcounter LT #listlen(session.opp_psc)#> or</cfif>
           <cfset pcounter = pcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_class_code = '#session.opp_psc#'
		</cfif>
    </cfif>

   )

   <cfif #session.opp_keyword# is not "">
	and contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_agency, fbo_naics_code, fbo_contract_award_name),'#session.opp_keyword#')
   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by fbo_solicitation_number ASC
		    <cfelseif #sv# is 10>
		     order by fbo_solicitation_number DESC
		    <cfelseif #sv# is 2>
		     order by fbo_agency ASC
		    <cfelseif #sv# is 20>
		     order by fbo_agency DESC
		    <cfelseif #sv# is 3>
		     order by fbo_office ASC
		    <cfelseif #sv# is 30>
		     order by fbo_office DESC
		    <cfelseif #sv# is 4>
		     order by fbo_location ASC
		    <cfelseif #sv# is 40>
		     order by fbo_location DESC
		    <cfelseif #sv# is 5>
		     order by fbo_opp_name ASC
		    <cfelseif #sv# is 50>
		     order by fbo_opp_name DESC
		    <cfelseif #sv# is 6>
		     order by class_code_name ASC
		    <cfelseif #sv# is 60>
		     order by class_code_name DESC
		    <cfelseif #sv# is 7>
		     order by fbo_notice_type ASC
		    <cfelseif #sv# is 70>
		     order by fbo_notice_type DESC
		    <cfelseif #sv# is 8>
		     order by fbo_naics_code ASC
		    <cfelseif #sv# is 80>
		     order by fbo_naics_code DESC
		    <cfelseif #sv# is 9>
		     order by fbo_setaside_original ASC
		    <cfelseif #sv# is 90>
		     order by fbo_setaside_original DESC
		    <cfelseif #sv# is 10>
		     order by fbo_pub_date_updated ASC
		    <cfelseif #sv# is 100>
		     order by fbo_pub_date_updated DESC
		    </cfif>
		   <cfelse>
            order by fbo_pub_date_updated DESC
		   </cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

      <cfif agencies.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		  <tr><td class="feed_header">Opportunities Found (0)</td></tr>
          <tr><td height=5></td></tr>
          <tr><td colspan=3><hr></td></tr>
          <tr><td height=5></td></tr>
          <tr><td class="feed_sub_header">No Opportunities were found.</td></tr>
        </table>

      <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="refresh.cfm" method="post">
         <cfoutput>
			 <tr><td class="feed_header">Opportunities Found (#ltrim(numberformat(agencies.recordcount,'999,999'))#)</td>

                <td class="feed_sub_header" align=right>

				<cfif agencies.recordcount GT #perpage#>

					Page&nbsp;

					<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt agencies.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>

				</td>

				 <td class="feed_sub_header" align=right>
				 <a href="/exchange/opps/results.cfm"><img src="/images/icon_list.png" width=20 hspace=10 alt="Summary View" title="Summary View"></a>
                 <a href="/exchange/opps/results.cfm">Summary View</a>
				 &nbsp;&nbsp;
				 <a href="/exchange/opps/save_search.cfm?l=2"><img src="/images/icon_save2.png" hspace=10 width=20 alt="Save Search" title="Save Search"></a>
				 <a href="/exchange/opps/save_search.cfm?l=2">Save Search</a>
				 &nbsp;&nbsp;
				 <a href="/exchange/opps/results.cfm?export=1"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="/exchange/opps/results.cfm?export=1">Export to Excel</a>
				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_option"><font color="green"><b>Quick search added.</b></font></td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_option"><font color="green"><b>Quick search has been updated.</b></font></td></tr>
          <cfelseif u is 3>
           <tr><td class="feed_option"><font color="red"><b>Quick search has been deleted.</b></font></td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td></td>
            <td class="feed_option"><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>ORGANIZATION</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>SOLICIATION #</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAME / TITLE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" width=50><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>TYPE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=center><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>SET ASIDE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 9><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 90><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" width=110 align=right colspan=2><a href="/exchange/opps/results_expanded.cfm?<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>DATE POSTED</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 10><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 100><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td>&nbsp;</td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=60>
         <cfelse>
          <tr bgcolor="e0e0e0" height=60>
         </cfif>

		     <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #orgname_logo# is ""><img src="#image_virtual#/usa_flag.png" valign=top align=top width=40 border=0 vspace=10><cfelse><img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10></cfif></a></td>
             <td class="text_xsmall" width=300><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo_dept#</a></b><cfif fbo_agency is not ""><br>#fbo_agency#</cfif><cfif #fbo_office# is not ""><br>#fbo_office#</cfif></td>
             <td class="text_xsmall" valign=middle width=150><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo_solicitation_number#</b></a></td>
             <td class="text_xsmall"><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#fbo_opp_name#</b></a></td>
             <td class="text_xsmall"><cfif #class_code_name# is "">Unknown<cfelse>#class_code_name#</cfif></td>
             <td class="text_xsmall">#fbo_notice_type#</td>
             <td class="text_xsmall" align=center width=75>#fbo_naics_code#</td>
             <td class="text_xsmall" width=120><cfif #fbo_setaside_original# is "">No<cfelse>#fbo_setaside_original#</cfif></td>
             <td class="text_xsmall" align=right width=100>#dateformat(fbo_pub_date,'mm/dd/yyyy')#</td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			     </td>
         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>
        </table>

       </cfif>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>