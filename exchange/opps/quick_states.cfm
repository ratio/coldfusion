<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select fbo_pop_state, state_name, count(distinct(fbo_solicitation_number)) as total from fbo
 left join state on state_abbr = fbo_pop_state
 where fbo_pop_country = 'USA'
 group by state_name, fbo_pop_state
 order by state_name ASC
</cfquery>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/opps/quick_links.cfm">
	      <cfinclude template="/exchange/opps/saved_searches.cfm">
      </td><td valign=top>

      <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

		  <div class="tab_active">
		   <span class="feed_header"><img src="/images/icon_fed.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
		  </div>

      </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_opportunities_advanced.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		  <tr><td class="feed_header">Contracts by State</td></tr>
          <tr><td height=5></td></tr>
          <tr><td colspan=3><hr></td></tr>
          <tr><td height=5></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td class="feed_sub_header"><a href="/exchange/opps/quick_results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>STATE / CITY</b></a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="/exchange/opps/quick_results.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>CONTRACT(S)</b></a>&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=10></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfloop query="agencies">

			<cfquery name="city" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select fbo_pop_city, count(distinct(fbo_solicitation_number)) as total from fbo
			 where fbo_pop_state = '#agencies.fbo_pop_state#'
			 group by fbo_pop_city
			 order by fbo_pop_city ASC
			</cfquery>

         <tr><td height=10></td></tr>

          <tr bgcolor="e0e0e0" height=50>

         <cfoutput>

		     <td class="feed_header" width=300 style="padding-left: 10px;"><a href="/exchange/opps/quick_states_detail.cfm?state=#agencies.fbo_pop_state#"><cfif agencies.state_name is "">Unknown<cfelse>#agencies.state_name#</cfif></a></td>
		     <td class="feed_header"><a href="/exchange/opps/quick_states_detail.cfm?state=#agencies.fbo_pop_state#">#agencies.total#</a></td>

         </cfoutput>

         </tr>

         <tr><td height=10></td></tr>

         <cfset counter = 0>

         <cfoutput query="city">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="f0f0f0">
         </cfif>

         <td class="feed_sub_header" style="padding-left: 15px;">&nbsp;&nbsp;-&nbsp;&nbsp;#city.fbo_pop_city#</td>
         <td class="feed_sub_header">#city.total#</td>

         </tr>


         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>





         </cfloop>
        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>