<cfinclude template="/exchange/security/check.cfm">

<cfset session.view = 1>
<cfset clear = 1>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<cfset perpage = 100>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<!--- Lookups --->

<cfset from = dateadd('d',-2,now())>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

   select fbo_id, fbo_solicitation_number, fbo_dept, fbo_agency, fbo_office, fbo_opp_name, fbo_desc, fbo_class_code, class_code_name, orgname_logo, fbo_notice_type, fbo_naics_code,  fbo_setaside_original, fbo_pub_date from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics_code
   left join orgname on orgname_name = fbo_agency
   where (fbo_id > 0

   <cfif session.opp_posted_from is "" and session.opp_posted_to is "">
   <cfelseif session.opp_posted_from is not "" and session.opp_posted_to is not "">
    and (fbo_pub_date between '#session.opp_posted_from#' and '#session.opp_posted_to#')
   <cfelseif session.opp_posted_from is not "" and session.opp_posted_to is "">
    and (fbo_pub_date >= '#session.opp_posted_from#')
   <cfelseif session.opp_posted_from is "" and session.opp_posted_to is not "">
    and (fbo_pub_date <= '#session.opp_posted_to#')
   </cfif>

   <cfif session.opp_setaside is not 0>
    and fbo_setaside_original = '#session.opp_setaside#'
   </cfif>

   <cfif session.opp_status is 2>
    and fbo_inactive_date_updated >= #now()#
   <cfelseif session.opp_status is 3>
    and fbo_inactive_date_updated <= #now()#
   </cfif>

   <cfif session.opp_dept is not 0>
    and fbo_agency = '#session.opp_dept#'
   </cfif>

   <cfif session.opp_type is not 0>
    and fbo_notice_type = '#session.opp_type#'
   </cfif>

   <cfif #listlen(session.opp_naics)# GT 0>
		<cfif #listlen(session.opp_naics)# GT 1>
		<cfset ncounter = 1>
		and (
         <cfloop index="nc" list="#session.opp_naics#">
           (fbo_naics_code = '#nc#')
           <cfif ncounter LT #listlen(session.opp_naics)#> or</cfif>
           <cfset ncounter = ncounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_naics_code = '#session.opp_naics#'
		</cfif>
    </cfif>

   <cfif #listlen(session.opp_psc)# GT 0>
		<cfif #listlen(session.opp_psc)# GT 1>
		<cfset pcounter = 1>
		and (
         <cfloop index="pc" list="#session.opp_psc#">
           (fbo_class_code = '#pc#')
           <cfif pcounter LT #listlen(session.opp_psc)#> or</cfif>
           <cfset pcounter = pcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_class_code = '#session.opp_psc#'
		</cfif>
    </cfif>

   )

   <cfif #trim(session.opp_keyword)# is not "">
	and contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_dept, fbo_agency, fbo_office, fbo_major_command, fbo_sub_command, fbo_naics_code, fbo_contract_award_name),'#trim(session.opp_keyword)#')
   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by fbo_agency ASC
		    <cfelseif #sv# is 10>
		     order by fbo_agency DESC
		    <cfelseif #sv# is 2>
		     order by fbo_opp_name ASC
		    <cfelseif #sv# is 20>
		     order by fbo_opp_name DESC
		    <cfelseif #sv# is 3>
		     order by class_code_name ASC
		    <cfelseif #sv# is 30>
		     order by class_code_name DESC
		    <cfelseif #sv# is 4>
		     order by fbo_notice_type ASC
		    <cfelseif #sv# is 40>
		     order by fbo_notice_type DESC
		    <cfelseif #sv# is 5>
		     order by fbo_naics_code ASC
		    <cfelseif #sv# is 50>
		     order by fbo_naics_code DESC
		    <cfelseif #sv# is 6>
		     order by fbo_setaside_original ASC
		    <cfelseif #sv# is 60>
		     order by fbo_setaside_original DESC
		    <cfelseif #sv# is 7>
		     order by fbo_pub_date ASC
		    <cfelseif #sv# is 70>
		     order by fbo_pub_date DESC
		    <cfelseif #sv# is 8>
		     order by fbo_solicitation_number ASC
		    <cfelseif #sv# is 80>
		     order by fbo_solicitation_number DESC
		    </cfif>
		   <cfelse>
            order by fbo_pub_date_updated DESC
		   </cfif>

</cfquery>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/opps/saved_searches.cfm">
      </td><td valign=top>

      <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

		  <div class="tab_active">
		   <span class="feed_header"><img src="/images/icon_fed.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
		  </div>

      </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_opportunities_advanced.cfm">
	  </div>

      <div class="main_box">

      <cfif agencies.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		  <tr><td class="feed_header">Search Results</td></tr>
          <tr><td height=5></td></tr>
          <tr><td colspan=3><hr></td></tr>
          <tr><td height=5></td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities were found.</td></tr>
        </table>

      <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
			 <tr>

			 <td class="feed_header">Search Results (#ltrim(numberformat(agencies.recordcount,'999,999'))#)</td>

                <td class="feed_sub_header" align=right>

				<cfif agencies.recordcount GT #perpage#>
					<b>Page&nbsp; #thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt agencies.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>

				</td>

				 <td class="feed_sub_header" align=right>
				 <a href="/exchange/opps/results_expanded.cfm"><img src="/images/icon_expand.png" width=20 hspace=10 alt="Expanded View" title="Expanded View"></a>
				 <a href="/exchange/opps/results_expanded.cfm">Expanded View</a>
				 &nbsp;&nbsp;
				 <a href="/exchange/opps/save_search.cfm?l=1"><img src="/images/icon_save2.png" width=20 hspace=10 alt="Save Search" title="Save Search"></a>
				 <a href="/exchange/opps/save_search.cfm?l=1">Save Search</a>
				 &nbsp;&nbsp;
				 <a href="/exchange/opps/results.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 hspace=10 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="/exchange/opps/results.cfm?export=1">Export to Excel</a>
				 </td></tr>
		 </cfoutput>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_option"><font color="green"><b>Quick search added.</b></font></td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_option"><font color="green"><b>Quick search has been updated.</b></font></td></tr>
          <cfelseif u is 3>
           <tr><td class="feed_option"><font color="red"><b>Quick search has been deleted.</b></font></td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td></td>
            <td class="feed_option"><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>ORGANIZATION</b></a>&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>SOLICITATION #</b></a>&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>NAME / TITLE</b></a>&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a>&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option" width=50><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>TYPE</b></a>&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option" align=center><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a>&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option"><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>SET ASIDE</b></a>&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_option" width=110 align=right><a href="/exchange/opps/results.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>POSTED</b></a><cfif isdefined("sv") and sv is 7>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td>&nbsp;</td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

		     <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #orgname_logo# is ""><img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10><cfelse><img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10></cfif></a></td>
             <td class="text_xsmall" width=400><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo_dept#</a></b><cfif fbo_agency is not ""><br>#fbo_agency#</cfif><cfif #fbo_office# is not ""><br>#fbo_office#</cfif></td>
             <td class="text_xsmall" valign=middle width=150><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo_solicitation_number#</b></a></td>

             <td class="text_xsmall" width=400>
                   <cfif #session.opp_keyword# is not "">
                   <b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#replaceNoCase(fbo_opp_name,session.opp_keyword,"<span style='background:yellow'>#ucase(session.opp_keyword)#</span>","all")#</a></b>
                   <cfelse>
				   #fbo_opp_name#
				   </cfif>
             </td>
             <td class="text_xsmall" width=250>

             <cfif class_code_name is "">
              <cfif #fbo_class_code# is "">Unknown<cfelse>#fbo_class_code#</cfif>
             <cfelse>
                   <cfif #session.opp_keyword# is not "">
                   #replaceNoCase(class_code_name,session.opp_keyword,"<span style='background:yellow'>#ucase(session.opp_keyword)#</span>","all")#
                   <cfelse>
				   #class_code_name#
				   </cfif>
		     </cfif>
             </td>

             <td class="text_xsmall" width=100>#fbo_notice_type#</td>
             <td class="text_xsmall" align=center width=75 align=center>#fbo_naics_code#</td>
             <td class="text_xsmall" width=100><cfif #fbo_setaside_original# is "">No<cfelse>#fbo_setaside_original#</cfif></td>
             <td class="text_xsmall" align=right width=75>#dateformat(fbo_pub_date,'mm/dd/yyyy')#</td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			     </td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>
        </table>

      </cfif>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>