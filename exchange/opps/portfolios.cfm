<div class="left_box">

<cfquery name="port" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select portfolio_image, portfolio_desc, portfolio_usr_id, portfolio_id, portfolio_name from portfolio
 where  portfolio_type_id = 3 and
       ((portfolio_usr_id = #session.usr_id#) or (portfolio_company_id = #session.company_id# and portfolio_access_id = 2))
 order by portfolio_name
</cfquery>

  <center>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header" valign=bottom><a href="/exchange/portfolio/">OPP PORTFOLIOS</a></td><td align=right width=20 align=right><a href="/exchange/portfolio/create_portfolio.cfm"><img src="/images/plus3.png" height=14 alt="Create Portfolio" title="Create Portfolio" border=0 valign=top></a></td></tr>
  <tr><td colspan=2><hr></td></tr>
  </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <cfif port.recordcount is 0>
	  <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any opportunity portfolios.</td></tr>
  <cfelse>

      <tr><td height=10></td></tr>

	  <cfset count = 1>

	  <cfoutput query="port">

	  <cfquery name="item" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(portfolio_item_id) as total from portfolio_item
	   where portfolio_item_portfolio_id = #port.portfolio_id#
	  </cfquery>

	  <tr><td width=30 valign=top><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#port.portfolio_id#">

	   <cfif #port.portfolio_image# is "">
		 <img src="/images/icon_portfolio_opp.png" width=18 border=0 alt="Company Portfolio" title="Opportunity Porfolio">
	   <cfelse>
		 <img src="#media_virtual#/#port.portfolio_image#" width=18 alt="Company Portfolio" title="Opportunity Porfolio">
	   </cfif>

	   </a></td>
		  <td class="link_med_blue" valign=top><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#port.portfolio_id#">#portfolio_name#</a></td>
		  <td class="link_med_blue" align=right valign=top>#item.total#</td></tr>
	  <tr><td></td><td class="link_small_gray" colspan=2 valign=top><cfif len(portfolio_desc) GT 80>#left(portfolio_desc,'80')#...<cfelse>#portfolio_desc#</cfif></td></tr>

	  <cfif count is not port.recordcount>
	   <tr><td colspan=3><hr></td></tr>
	  </cfif>

	  <cfset count = count + 1>

	  </cfoutput>

  </cfif>

 </table>
  </center>

</div>

