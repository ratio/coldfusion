<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>

             <td class="feed_header">

             <cfif t is 1>

				 <cfquery name="naics_code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  select * from naics
				  where naics_code = '#code#'
				 </cfquery>

				 <cfset cat_code = #naics_code.naics_code_description#>

             <cfelseif t is 2>

				 <cfquery name="class_code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  select * from class_code
				  where class_code_code = '#code#'
				 </cfquery>

				 <cfset cat_code = #class_code.class_code_name#>

             <cfelseif t is 3>

				 <cfquery name="class_code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  select * from class_code
				  where class_code_code = '#code#'
				 </cfquery>

				 <cfset cat_code = #class_code.class_code_name#>

             </cfif>

             <b><cfoutput>#ucase(code)# - #ucase(cat_code)#</cfoutput></b>

             </td><td align=right class="feed_option"><a href="opp_summary.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>

            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>

         </table>

         <cfif t is 1>

			 <cfquery name="opps" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from fbo
			  left join naics on naics_code = fbo_naics_code
			  left join class_code on class_code_code = fbo_class_code
			  where fbo_naics_code = '#code#' and
			        fbo_inactive_date_updated > #now()#
			  order by fbo_pub_date_updated DESC
			 </cfquery>

         <cfelse>

			 <cfquery name="opps" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from fbo
			  left join class_code on class_code_code = fbo_class_code
			  left join naics on naics_code = fbo_naics_code
			  where fbo_class_code = '#code#' and
			        fbo_inactive_date_updated > #now()#
			  order by fbo_pub_date_updated DESC
			 </cfquery>

		 </cfif>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr height=50>
            <td class="text_xsmall" width=75><b>Date Posted</b></td>
            <td class="text_xsmall" width=150><b>Soliciation #</b></td>
            <td class="text_xsmall" width=100><b>Type</b></td>
            <td class="text_xsmall"><b>Agency</b></td>
            <td class="text_xsmall"><b>Office</b></td>
            <td class="text_xsmall"><b>Opportunity Summary</b></td>
            <td class="text_xsmall" width=50><b>NAICS</b></td>
            <td class="text_xsmall"><b>Product or Service Requested</b></td>
            <td class="text_xsmall"><b>SB Set-Aside</b></td>
            <td>&nbsp;</td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="opps">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=30>
         <cfelse>
          <tr bgcolor="e0e0e0" height=30>
         </cfif>

             <td class="text_xsmall" valign=middle>#dateformat(fbo_pub_date_updated,'mm/dd/yyyy')#</td>
             <td class="text_xsmall" valign=middle><a href="opp_detail.cfm?fbo_id=#fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><b><cfif #fbo_solicitation_number# is "">Not Provided<cfelse>#fbo_solicitation_number#</cfif></b></a></td>
             <td class="text_xsmall" valign=middle>#fbo_type#</td>
             <td class="text_xsmall" valign=middle>#fbo_agency#</td>
             <td class="text_xsmall" valign=middle>#fbo_office#</td>
             <td class="text_xsmall" valign=middle width=450>#fbo_opp_name#</td>
             <td class="text_xsmall" valign=middle>#fbo_naics_code#</td>
             <td class="text_xsmall" valign=middle width=200>#fbo_class_code# - #class_code_name#</td>
             <td class="text_xsmall" valign=middle width=100>#fbo_setaside_original#</td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			     </td>
         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>
        </table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>