<cfinclude template="/exchange/security/check.cfm">

<cfif f is "y">

  <cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   insert follow(follow_by_usr_id, follow_challenge_id, follow_date)
   values(#session.usr_id#,#decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,#now()#)
  </cfquery>

  <cflocation URL="challenge_detail.cfm?u=1&i=#i#" addtoken="no">

<cfelse>

  <cfquery name="unfollow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   delete follow
   where follow_by_usr_id = #session.usr_id# and
         follow_challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cflocation URL="challenge_detail.cfm?u=2&i=#i#" addtoken="no">

</cfif>

