<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join class_code on class_code_code = fbo_class_code
   left join naics on naics_code = fbo_naics
   where (fbo_id > 0


   <cfif session.posted_from is "" and session.posted_to is "">
   <cfelseif session.posted_from is not "" and session.posted_to is not "">
    and (fbo_date_posted >= '#session.posted_from#' and fbo_date_posted <= '#session.posted_to#')
   <cfelseif session.posted_from is not "" and session.posted_to is "">
    and (fbo_date_posted >= '#session.posted_from#')
   <cfelseif session.posted_from is "" and session.posted_to is not "">
    and (fbo_date_posted <= '#session.posted_to#')
   </cfif>

   <cfif session.setaside is not 0>
    and fbo_setaside = '#session.setaside#'
   </cfif>

   <cfif session.dept is not 0>
    and fbo_agency = '#session.dept#'
   </cfif>

   <cfif session.type is not 0>
    and fbo_type = '#session.type#'
   </cfif>

   <cfif #listlen(session.naics)# GT 0>
		<cfif #listlen(session.naics)# GT 1>
		<cfset ncounter = 1>
		and (
         <cfloop index="nc" list="#session.naics#">
           (fbo_naics = '#nc#')
           <cfif ncounter LT #listlen(session.naics)#> or</cfif>
           <cfset ncounter = ncounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_naics = '#session.naics#'
		</cfif>
    </cfif>

   <cfif #listlen(session.psc)# GT 0>
		<cfif #listlen(session.psc)# GT 1>
		<cfset pcounter = 1>
		and (
         <cfloop index="pc" list="#session.psc#">
           (fbo_class_code = '#pc#')
           <cfif pcounter LT #listlen(session.psc)#> or</cfif>
           <cfset pcounter = pcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and fbo_class_code = '#session.psc#'
		</cfif>
    </cfif>


   )

   <cfif #session.keyword# is not "">
	and contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#session.keyword#"')
   </cfif>

   order by fbo_date_posted DESC
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		<cfparam name="URL.PageId" default="0">
		<cfset RecordsPerPage = 100>
		<cfset TotalPages = (fbo.Recordcount/RecordsPerPage)>
		<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		<cfset EndRow = StartRow+RecordsPerPage-1>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Search Results</td>
             <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>

         <form action="save_search.cfm" method="post">
         <tr><td class="feed_option">
         <cfoutput>
         <b>#numberformat(fbo.recordcount,'999,999')# Opportunities Found</b>
         </cfoutput></td><td align=right class="feed_option">
         <b>Save as Quick Search</b>&nbsp;&nbsp;<input type="text" name="search_name" size=30>
                       &nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Save"></td></tr>

         </form>

         <tr><td height=10></td></tr>

         <tr><td colspan=2 align=right class="feed_option">

				  <cfif fbo.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>

             </td>
         </tr>

         <cfif isdefined("u")>
          <tr><td colspan=2 class="feed_option"><font color="green"><b>Your search has been saved.</b></td></tr>
          <tr><td height=10></td></tr>
         </cfif>

         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td></td>
            <td class="text_xsmall" align=center><b>Date Posted</b></td>
            <td class="text_xsmall"><b>Soliciation Number</b></td>
            <td class="text_xsmall"><b>Opportunity Name</b></td>
            <td class="text_xsmall"><b>Agency</b></td>
            <td class="text_xsmall"><b>Office</b></td>
            <td class="text_xsmall"><b>Location</b></td>
            <td class="text_xsmall"><b>Type</b></td>
            <td class="text_xsmall" width=100 align=center><b>NAICS</b></td>
            <td class="text_xsmall" width="75"><b>PSC Code</b></td>
            <td class="text_xsmall"><b>SB Set-Aside</b></td>
         </tr>

         <cfset counter = 0>

         <cfloop query="fbo">

		 <cfif CurrentRow gte StartRow >

			 <cfoutput>

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>
				 <td class="text_xsmall" valign=top align=center width=70><cfif #fbo.fbo_image_url# is "">&nbsp;<cfelse><a href="opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#fbo.fbo_image_url#" width=50 vspace=10 border=0 alt="#fbo.fbo_agency#" title="#fbo.fbo_agency#"></a></cfif></td>
				 <td class="text_xsmall" valign=top width="75" align=center>#dateformat(fbo.fbo_date_posted,'mm/dd/yyyy')#</td>
				 <td class="text_xsmall" valign=top><a href="opp_detail.cfm?fbo_id=#fbo.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #fbo.fbo_solicitation_number# is "">Not identified<cfelse>#fbo.fbo_solicitation_number#</cfif></a></td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_opp_name#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_agency#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_office#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_location#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_type#</td>
				 <td class="text_xsmall" valign=top align=center>#fbo.fbo_naics#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_class_code# - #fbo.class_code_name#</td>
				 <td class="text_xsmall" valign=top>#fbo.fbo_setaside#</td>

			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			  <cfif CurrentRow eq EndRow>
			   <cfbreak>
			  </cfif>

         </cfif>

         </cfloop>
        </table>
	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>