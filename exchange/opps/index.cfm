<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.opp_recent")>
 <cfset #session.opp_recent# = 1>
</cfif>

<cfif not isdefined("session.opp_advanced")>
 <cfset #session.opp_advanced# = 1>
</cfif>

<cfset session.view = 1>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<cfif not isdefined("session.opp_status")>
 <cfset session.opp_status = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<style>
/* Style The Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  right: 0;
  font-size: 13px;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
</style>

<!--- Lookups --->

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pingroup
 where pingroup_usr_id = #session.usr_id# and
       pingroup_hub_id = #session.hub#
 order by pingroup_name
</cfquery>

<cfset from = dateadd('d',-2,now())>

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_agency) from fbo
 where fbo_agency is not null or fbo_agency <> ' '
 order by fbo_agency
</cfquery>

<cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_type) from fbo
 order by fbo_type
</cfquery>

<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_setaside_original) from fbo
 order by fbo_setaside_original
</cfquery>

<cfquery name="recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select fbo_id, fbo_solicitation_number, fbo_dept, fbo_agency, fbo_office, fbo_opp_name, class_code_name, fbo_type, orgname_logo, fbo_naics_code, fbo_setaside_original, fbo_pub_date_updated, fbo_pub_date from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics_code
 left join orgname on orgname_name = fbo_agency

 <cfif session.opp_recent is 1>
  where fbo_pub_date = '#dateformat(now(),'yyyy-mm-dd')#'
 <cfelseif session.opp_recent is 2>
  where fbo_pub_date = '#dateformat(dateadd('d',-1,now()),'yyyy-mm-dd')#'
 <cfelseif session.opp_recent is 3>
  where fbo_pub_date between '#dateformat(dateadd('d',-3,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
 <cfelseif session.opp_recent is 4>
  where fbo_pub_date between '#dateformat(dateadd('d',-7,now()),'yyyy-mm-dd')#' and '#dateformat(now(),'yyyy-mm-dd')#'
 </cfif>

<cfif isdefined("sv")>

<cfif #sv# is 1>
 order by fbo_dept ASC
<cfelseif #sv# is 10>
 order by fbo_dept DESC
<cfelseif #sv# is 2>
 order by fbo_opp_name ASC
<cfelseif #sv# is 20>
 order by fbo_opp_name DESC
<cfelseif #sv# is 3>
 order by class_code_name ASC
<cfelseif #sv# is 30>
 order by class_code_name DESC
<cfelseif #sv# is 4>
 order by fbo_notice_type ASC
<cfelseif #sv# is 40>
 order by fbo_notice_type DESC
<cfelseif #sv# is 5>
 order by fbo_naics_code ASC
<cfelseif #sv# is 50>
 order by fbo_naics_code DESC
<cfelseif #sv# is 6>
 order by fbo_setaside_original ASC
<cfelseif #sv# is 60>
 order by fbo_setaside_original DESC
<cfelseif #sv# is 7>
 order by fbo_pub_date ASC
<cfelseif #sv# is 70>
 order by fbo_pub_date DESC
<cfelseif #sv# is 8>
 order by fbo_solicitation_number ASC
<cfelseif #sv# is 80>
 order by fbo_solicitation_number DESC
</cfif>
<cfelse>
order by fbo_pub_date_updated DESC
</cfif>

</cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt recent.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(recent.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="quick_links.cfm">
          <cfinclude template="/exchange/opps/saved_searches.cfm">

      </td><td valign=top>

      <cfoutput>


          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=10 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

		  <div class="tab_active">
		   <span class="feed_header"><img src="/images/icon_fed.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
		  </div>

	  </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_opportunities_advanced.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="refresh.cfm" method="post">
         <cfoutput>
			 <tr><td class="feed_header">Recently Posted (#ltrim(numberformat(recent.recordcount,'99,999'))#)</td>

             <td class="feed_sub_header" align=right>
				<cfif recent.recordcount GT #perpage#>
					<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfif isdefined("sv")>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=" & #sv#>
					    <cfelse>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
					    </cfif>
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt recent.recordCount>
						<cfif isdefined("sv")>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=" & #sv#>
						<cfelse>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
						</cfif>
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25></a>
					<cfelse>
					</cfif>
				</cfif>
			  </td>

				 <td class="feed_sub_header" align=right width=225 style="font-weight: normal;"><b>Posted</b>&nbsp;&nbsp;
				 <select name="opp_recent" class="input_select" style="width:125px" onchange="form.submit()">
					<option value=1 <cfif #session.opp_recent# is 1>selected</cfif>>Today
					<option value=2 <cfif #session.opp_recent# is 2>selected</cfif>>Yesterday
					<option value=3 <cfif #session.opp_recent# is 3>selected</cfif>>Last 3 Days
					<option value=4 <cfif #session.opp_recent# is 4>selected</cfif>>Last 7 Days
				 </select>

				</td></tr>
				</cfoutput>
            </form>

         <cfif isdefined("u")>
          <cfif u is 7>
           <tr><td colspan=3 class="feed_sub_header" style="color: green;">Opportunity has successfully been pinned to your board.</td></tr>
          </cfif>
         </cfif>

         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

         <cfif isdefined("u")>
          <cfif u is 3>
           <tr><td class="feed_option"><font color="red"><b>Saved search has been deleted.</b></font></td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif recent.recordcount is 0>

 	         <tr><td class="feed_sub_header" style="font-weight: normal;">No opportunities were found.</td></tr>

         <cfelse>

			 <tr>
				<td></td>
				<td class="feed_option" width=200><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>ORGANIZATION</b></a><cfif isdefined("sv") and sv is 1>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option"><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>SOLICITATION #</b></a><cfif isdefined("sv") and sv is 8>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option"><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>NAME / TITLE</b></a><cfif isdefined("sv") and sv is 2>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option"><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a><cfif isdefined("sv") and sv is 3>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" width=50><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>TYPE</b></a><cfif isdefined("sv") and sv is 4>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" align=center><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a><cfif isdefined("sv") and sv is 5>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option"><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>SB SET-ASIDE</b></a><cfif isdefined("sv") and sv is 6>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td class="feed_option" width=110 align=right colspan=2><a href="/exchange/opps/index.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>DATE POSTED</b></a><cfif isdefined("sv") and sv is 7>&nbsp;<img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=10></cfif></td>
				<td>&nbsp;</td>
			 </tr>

			 <cfset counter = 0>

			<cfoutput query="recent" startrow="#url.start#" maxrows="#perpage#">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=70>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=70>
			 </cfif>

				 <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#recent.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif #orgname_logo# is ""><img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10><cfelse><img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10></cfif></a></td>
				 <td class="text_xsmall" valign=middle width=300><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#recent.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(fbo_dept)#</b></a><br>#fbo_agency#<br>#fbo_office#</td>
				 <td class="text_xsmall"><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#recent.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#recent.fbo_solicitation_number#</td>
				 <td class="text_xsmall">#recent.fbo_opp_name#</td>
				 <td class="text_xsmall" width=200><cfif #recent.class_code_name# is "">Not specified<cfelse>#recent.class_code_name#</cfif>&nbsp;</td>
				 <td class="text_xsmall">#recent.fbo_type#</td>
				 <td class="text_xsmall" align=center width=75><cfif recent.fbo_naics_code is "">Unknown<cfelse>#recent.fbo_naics_code#</cfif></td>
				 <td class="text_xsmall" width=120><cfif recent.fbo_setaside_original is "">No<cfelse>#recent.fbo_setaside_original#</cfif></td>
				 <td class="text_xsmall" align=right width=120>#dateformat(recent.fbo_pub_date,'mm/dd/yyyy')#</td>
                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#recent.fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			     </td>

			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

         </cfif>

        </table>

      </div>

      </td>
      </tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>