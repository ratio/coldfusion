<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join naics on naics_code = fbo_naics_code
   left join class_code on class_code_code = fbo_class_code
   where fbo_id = #fbo_id#
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">SHARE OPPORTUNITY</td>
             <td class="feed_option" align=right><a href="opp_detail.cfm?fbo_id=<cfoutput>#fbo_id#</cfoutput>"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>

         <cfif isdefined("u")>
          <tr><td height=5></td></tr>
          <tr><td class="feed_sub_header"><font color="green">The opportunity has been successfully shared.</font></td></tr>
          <tr><td height=5></td></tr>
         </cfif>

        </table>

		  <form action="share_send.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_sub_header" width=150><b>Email Address</td>
                 <td><input class="input_text" type="email" name="email" class="input_text" style="width: 350px;" required></td>
             </tr>

             <tr>
                 <td class="feed_sub_header">Subject</td>
                 <td><input type="text" class="input_text" name="subject" style="width: 700px;" required></td>
             </tr>

             <tr>
                 <td class="feed_sub_header" valign=top>Message</td>
                 <td><textarea class="input_textarea" class="input_textarea" style="width: 700px;" name="message" rows=8></textarea></td>
             </tr>

			 <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Share" vspace=10></td></tr>

             <tr><td height=10></td></tr>

		     <cfoutput>
		     <input type="hidden" name="fbo_id" value=#fbo_id#>
		     </cfoutput>

            </form>
         <tr><td colspan=2><hr></td></tr>

          </table>

       <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td height=10></td></tr>
			 <tr><td class="feed_header">#fbo.fbo_opp_name#</td></tr>
			 <tr><td height=10></td></tr>
            </table>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td>

			        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			         <tr>
			             <td class="feed_option"><b>Solicitation ##</td>
			             <td class="feed_option">#fbo.fbo_solicitation_number#</td>

			             <td class="feed_option"><b>Date Posted</td>
			             <td class="feed_option">#dateformat(fbo.fbo_pub_date,'mm/dd/yyyy')#</td>

			         </tr>

			         <tr>
			             <td class="feed_option"><b>Notice Type</td>
			             <td class="feed_option">#fbo.fbo_type#</td>

			             <td class="feed_option"><b>Response Date</td>
			             <td class="feed_option">
			             <cfif isdate(fbo.fbo_response_date_original)>
			              #dateformat(fbo.fbo_response_date_original,'mm/dd/yyyy')# at #timeformat(fbo.fbo_response_date_original)#
			             <cfelse>
			             Unknown or N/A
			             </cfif></td>

			         </tr>

			         <tr>
			             <td class="feed_option"><b>Department</td>
			             <td class="feed_option">#fbo.fbo_dept#</td>


			             <td class="feed_option"><b>Small Business</td>
			             <td class="feed_option"><cfif fbo.fbo_setaside_original is "">No<cfelse>#fbo.fbo_setaside_original#</cfif></td>


			         </tr>

			         <tr>
			             <td class="feed_option"><b>Agency</td>
			             <td class="feed_option">#fbo.fbo_agency#</td>

			             <td class="feed_option" valign=top><b>NAICS</td>
			             <td class="feed_option" valign=top>#fbo.fbo_naics_code# - #fbo.naics_code_description#</td>


			         </tr>

			         <tr>
			             <td class="feed_option"><b>Office</td>
			             <td class="feed_option">#fbo.fbo_office#</td>

			             <td class="feed_option"><b>Product or Service</td>
			             <td class="feed_option">#fbo.fbo_class_code# - #fbo.class_code_name#</td>

			         </tr>

			         <tr>
			             <td class="feed_option"><b>Contracting Office</td>
			             <td class="feed_option">#fbo.fbo_contracting_office#</td>

			             <td class="feed_option"><b>Place of Performance</td>
			             <td class="feed_option">#fbo.fbo_pop_city#  #fbo.fbo_pop_state#, #fbo.fbo_pop_zip#</td>

			         </tr>

			        <tr><td class="feed_option"><b>Opportunity URL</b></td>
			            <td colspan=3 class="feed_option"><a href="#fbo.fbo_url#" target="_blank" rel="noopener" rel="noreferrer">#fbo.fbo_url#</a></td></tr>


			        </table>

			     </td></tr>
			 <tr><td height=10></td></tr>

			 <cfif fbo.fbo_type is "Award">

				 <tr><td><hr></td></tr>
				 <tr><td class="feed_header"><b>Award Information</b></td></tr>
				 <tr><td height=10></td></tr>
				 <tr><td class="feed_option"><b>Award Date: </b>#dateformat(fbo.fbo_contract_award_date,'mm/dd/yyy')# - <a href="/exchange/include/company_profile.cfm?id=0&duns=#fbo.fbo_contract_award_duns#" target="_blank" rel="noopener" rel="noreferrer">Company Look-up</a></td></tr>
				 <tr><td class="feed_option"><b>Awardee Name: </b>#fbo.fbo_contract_award_name#</td></tr>
				 <tr><td class="feed_option"><b>Awardee DUNS: </b>#fbo.fbo_contract_award_duns#</td></tr>
				 <tr><td class="feed_option"><b>Award Amount: </b>#fbo.fbo_contract_award_amount#</td></tr>

			 </cfif>

			 <tr><td><hr></td></tr>
			 <tr><td class="feed_sub_header"><b>OPPORTUNITY DESCRIPTION</b></td></tr>
			 <tr><td height=10></td></tr>
			 <tr><td class="feed_option">
             #replace(fbo.fbo_desc,"#chr(46)##chr(32)#",".<br><br>","all")#

			</td></tr>

			</table>

        </cfoutput>
 	  </div>

   </td></tr>

</table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>