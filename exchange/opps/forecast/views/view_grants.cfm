<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.grant_recent")>
 <cfset #session.grant_recent# = 1>
</cfif>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<cfif not isdefined("session.grant_status")>
 <cfset session.grant_status = 2>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<!--- Lookups --->

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select opp_grant_id, opportunitytitle, department, agencyname, opportunitynumber, fundinginstrumenttype, awardfloor, awardceiling, closedate from opp_grant
 where closedate > #now()#
 and contains((*),'"#trim(ky)#"')

	<cfif #sv# is 1>
	 order by department ASC, agencyname ASC
	<cfelseif #sv# is 10>
	 order by department DESC, agencyname ASC
	<cfelseif #sv# is 2>
	 order by opportunitynumber ASC
	<cfelseif #sv# is 20>
     order by opportunitynumber DESC
	<cfelseif #sv# is 3>
	 order by opportunitytitle ASC
	<cfelseif #sv# is 30>
	 order by opportunitytitle DESC
	<cfelseif #sv# is 4>
	 order by fundinginstrumenttype ASC
	<cfelseif #sv# is 40>
	 order by fundinginstrumenttype DESC
	<cfelseif #sv# is 5>
	 order by awardfloor ASC
	<cfelseif #sv# is 50>
	 order by awardfloor DESC
	<cfelseif #sv# is 6>
	 order by awardceiling ASC
	<cfelseif #sv# is 60>
	 order by awardceiling DESC
	<cfelseif #sv# is 7>
	 order by closedate ASC
	<cfelseif #sv# is 70>
	 order by closedate DESC
	</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="grant_refresh.cfm" method="post">
         <cfoutput>
			 <tr><td class="feed_header">GRANT OPPORTUNITIES (#ltrim(numberformat(agencies.recordcount,'99,999'))#) - #ky#</td>

				 <td class="feed_sub_header" align=right width=300>
				 <a href="view_grants.cfm?export=1&sv=#sv#&ky=#ky#"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
				 &nbsp;|&nbsp;
				  <img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
				</td></tr>
				</cfoutput>
            </form>

         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr height=50>
            <td class="feed_option"><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TITLE</b></a><cfif isdefined("sv") and sv is 3>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>ORGANIZATION</b></a><cfif isdefined("sv") and sv is 1>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>OPPORTUNITY ##</b></a><cfif isdefined("sv") and sv is 2>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=center><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>FUNDING TYPE</b></a><cfif isdefined("sv") and sv is 4>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=right><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AWARD FLOOR</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=right><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>AWARD CELING</b></a><cfif isdefined("sv") and sv is 6>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=right><a href="view_grants.cfm?ky=#ky#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>CLOSE DATE</b></a><cfif isdefined("sv") and sv is 7>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70>&nbsp;&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td>&nbsp;</td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=70>
         <cfelse>
          <tr bgcolor="e0e0e0" height=70>
         </cfif>

             <td class="feed_option" valign=top width=450><b><a href="/exchange/include/grant_new_information.cfm?id=#opp_grant_id#" target="_blank" rel="noopener" rel="noreferrer">

             #replaceNoCase(opportunitytitle,ky,"<span style='background:yellow'>#ucase(ky)#</span>","all")#


             </a></b></td>
             <td class="feed_option" valign=top width=200>#ucase(department)#<br>#agencyname#</td>
             <td class="feed_option" valign=top width=120>#opportunitynumber#</td>
             <td class="feed_option" valign=top align=center width=75>#fundinginstrumenttype#</td>
             <td class="feed_option" valign=top align=right width=75>#numberformat(awardfloor,'$999,999,999')#</td>
             <td class="feed_option" valign=top align=right width=75>#numberformat(awardceiling,'$999,999,999')#</td>
             <td class="feed_option" valign=top align=right>#dateformat(closedate,'mm/dd/yyyy')#</td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

        </table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>