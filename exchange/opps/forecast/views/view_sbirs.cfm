<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<!--- Lookups --->

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select id, solicitationnumber, department, agency, title, keywords, technologyarea, objective, opendate, applicationduedate, closedate, url from opp_sbir
   where closedate > #now()#
   and contains((*),'"#trim(ky)#"')
</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

<div class="main_box">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
			 <tr><td class="feed_header">SBIR/STTR OPPORTUNITIES (#agencies.recordcount#) - #ky#</td>
				 <td class="feed_sub_header" align=right>
				 <a href="/exchange/opps/results_sbir.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
				 &nbsp;|&nbsp;
				  <img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">

				 </td></tr>
		 </cfoutput>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td class="feed_sub_header">TITLE / DESCRIPTION</td>
            <td class="feed_sub_header" align=right>CLOSE DATE</td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif agencies.department is "DOD">

			 <tr>
				 <td class="feed_sub_header" valign=top><a href="/exchange/include/sbir_dod.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(agencies.title)#</a></td>
				 <td class="feed_sub_header" align=right width=120 valign=top>#dateformat(agencies.closedate,'mm/dd/yyyy')#</td>
			 </tr>
			 <tr>
				 <td class="feed_sub_header" style="font-weight: normal;">#replaceNoCase(agencies.objective,ky,"<span style='background:yellow'>#ky#</span>","all")#</td>
			 </tr>

			 <tr>
				<td class="link_small_gray"><b>DEPARTMENT OF DEFENSE (DOD), #agencies.agency#</b></td>
			 </tr>

			 <tr><td height=5></td></tr>

			 <tr>
				<td class="link_small_gray"><b>KEYWORDS: </b>#ucase(replaceNoCase(agencies.keywords,ky,"<span style='background:yellow'>#ky#</span>","all"))#</td>
			 </tr>

			 <tr><td height=5></td></tr>

			 <tr>
				<td class="link_small_gray"><b>TECHNOLOGY AREA: </b>#ucase(replaceNoCase(agencies.technologyarea,ky,"<span style='background:yellow'>#ky#</span>","all"))#</td>
			 </tr>


         <cfelseif agencies.department is "Department of Health and Human Services">

			 <tr>
				 <td class="feed_sub_header" valign=top><a href="/exchange/include/sbir_hhs.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(agencies.title)#</a></td>
				 <td class="feed_sub_header" align=right width=120 valign=top>#dateformat(agencies.closedate,'mm/dd/yyyy')#</td>
			 </tr>

			 <tr>
				 <td class="feed_sub_header" style="font-weight: normal;">#objective#</a></td>
			 </tr>

			 <tr>
				<td class="link_small_gray"><b>HEALTH AND HUMAN SERVICES (HHS)</b></td>
			 </tr>


         </cfif>

         <tr><td colspan=2><hr></td></tr>

         </cfoutput>

       </table>

       </td></tr>

       </table>

	  </div>

 </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>