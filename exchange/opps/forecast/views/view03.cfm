<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="capabilites" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select usr_keywords from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfset len = listlen(capabilites.usr_keywords)>
<cfset cap_list = valuelist(capabilites.usr_keywords)>

<cfset buyers_count = 1>
<cfif not isdefined("sv")>
 <cfset sv = 50>
</cfif>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(awarding_sub_agency_code), count(distinct(recipient_duns)) as vendors, count(distinct(parent_award_id)) as contracts, count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total, awarding_sub_agency_name from award_data
 where
 (

  <cfloop index="c" list="#capabilites.usr_keywords#">
   contains((award_description),'"#c#"')
   <cfif buyers_count LT len>
   or
   </cfif>
   <cfset buyers_count = buyers_count + 1>
 </cfloop>

 )
 and period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#'
 and federal_action_obligation > 0

group by awarding_sub_agency_code, awarding_sub_agency_name

<cfif #sv# is 1>
 order by awarding_sub_agency_name ASC
<cfelseif #sv# is 10>
 order by awarding_sub_agency_name DESC
<cfelseif #sv# is 2>
 order by vendors DESC
<cfelseif #sv# is 20>
 order by vendors ASC
<cfelseif #sv# is 3>
 order by contracts DESC
<cfelseif #sv# is 30>
 order by contracts ASC
<cfelseif #sv# is 4>
 order by awards DESC
<cfelseif #sv# is 40>
 order by awards ASC
<cfelseif #sv# is 5>
 order by total ASC
<cfelseif #sv# is 50>
 order by total DESC
</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">FEDERAL BUYERS (#trim(numberformat(agencies.recordcount,'999,999'))#) *</td>
               <td class="feed_option" align=right><a href="/exchange/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>

           <tr><td class="feed_sub_header"><b>Keywords used "<i>#cap_list#</i>"</b></td>
               <td align=right class="feed_option">

			   <a href="/exchange/opps/forecast/views/view03.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>

               </td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header">No Federal buyers were found.</td></tr>
          <cfelse>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
				 <td class="feed_option"><a href="view03.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>FEDERAL BUYER</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view03.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>VENDORS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view03.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>CONTRACTS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view03.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AWARDS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=right><a href="view03.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AMOUNT</b></a><cfif isdefined("sv") and sv is 5>&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
		      </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

			   <cfoutput>
				   <td class="feed_option"><a href="view03_detail.cfm?code=#awarding_sub_agency_code#"><b>#agencies.awarding_sub_agency_name#</b></a></td>
				   <td class="feed_option" align=center>#agencies.vendors#</td>
				   <td class="feed_option" align=center>#agencies.contracts#</td>
				   <td class="feed_option" align=center>#agencies.awards#</td>
				   <td class="feed_option" align=right>#numberformat(agencies.total,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

          </cfloop>

          <tr><td height=20></td></tr>
          <tr><td colspan=5><hr></td></tr>
		  <tr><td colspan=5 class="feed_option"><b><i>* Represents the buyers of similar products or services within the past 2 years.</i></b></td></tr>
          <tr><td height=10></td></tr>


          </cfif>


		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>