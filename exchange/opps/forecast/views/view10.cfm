<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<body class="body">

<cfquery name="capabilites" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_keywords from company
 where company_id = #session.company_id#
</cfquery>

<cfset len = listlen(capabilites.company_keywords)>
<cfset opp_count = 1>
<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

<cfquery name="fbo_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(fbo_solicitation_number)) as total from fbo
 where
 (
  contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_agency, fbo_naics_code, fbo_contract_award_name),'"#ky#"')
 )

 and fbo_inactive_date_updated > '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics_code
 where (contains((fbo_opp_name, fbo_desc),'"#ky#"') )
 and fbo_inactive_date_updated > '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')

<cfif isdefined("sv")>

 <cfif sv is 1>
  order by fbo_solicitation_number ASC
 <cfelseif sv is 10>
  order by fbo_solicitation_number DESC
 <cfelseif sv is 2>
  order by fbo_agency ASC
 <cfelseif sv is 20>
  order by fbo_agency DESC
 <cfelseif sv is 3>
  order by fbo_office ASC
 <cfelseif sv is 30>
  order by fbo_office DESC
 <cfelseif sv is 4>
  order by fbo_location ASC
 <cfelseif sv is 40>
  order by fbo_location DESC
 <cfelseif sv is 5>
  order by fbo_opp_name ASC
 <cfelseif sv is 50>
  order by fbo_opp_name DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics_code ASC
 <cfelseif sv is 80>
  order by fbo_naics_code DESC
 <cfelseif sv is 9>
  order by fbo_setaside_original ASC
 <cfelseif sv is 90>
  order by fbo_setaside_original DESC
 <cfelseif sv is 10>
  order by fbo_pub_date_updated ASC
 <cfelseif sv is 100>
  order by fbo_pub_date_updated DESC
 </cfif>
<cfelse>
order by fbo_pub_date_updated DESC
</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">OPPORTUNITIES (#ltrim(numberformat(fbo_count.total,'999,999'))# Unique) - #agencies.recordcount# Postings and Modifications for <i>"#ky#"</i></td>
				 <td class="feed_option" align=right>
				 <a href="/exchange/opps/forecast/views/view10.cfm?ky=#ky#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
				 &nbsp;&nbsp;&nbsp;&nbsp;

                 <cfif isdefined("l")>
					 <img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
                 <cfelse>

					 <cfif isdefined("loc")>
					 <a href="/exchange/capability.cfm?c=#ky#">
					 <cfelse>
					  <cfif isdefined("cus")>
					   <a href="/exchange/opps/forecast/custom.cfm">
					  <cfelse>
					   <a href="/exchange/opps/forecast/">
					  </cfif>
					 </cfif>
					 <img src="/images/delete.png" width=20 alt="Close" title="Close"></a>

				 </cfif>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>

       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td></td>
            <td class="feed_option"><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>SOLICIATION ##</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DEPARTMENT</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAME / TITLE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" width=50><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>TYPE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=center><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>SET ASIDE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 9><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 90><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" width=110 align=right><a href="view10.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>DATE POSTED</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 10><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 100><img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=40>
         <cfelse>
          <tr bgcolor="e0e0e0" height=40>
         </cfif>

			 <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/fbo_logo.png" align=top width=30 border=0 vspace=10></a></td>
             <td class="text_xsmall" valign=middle width=200><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><cfif len(fbo_solicitation_number) GT 50>#left(fbo_solicitation_number,'50')#...<cfelse>#fbo_solicitation_number#</cfif></b></a></td>
             <td class="text_xsmall" valign=middle width=150>#fbo_agency#</td>
             <td class="text_xsmall">#fbo_opp_name#</td>
             <td class="text_xsmall">#class_code_name#</td>
             <td class="text_xsmall">#fbo_notice_type#</td>
             <td class="text_xsmall" align=center width=75>#fbo_naics_code#</td>
             <td class="text_xsmall" width=75>#fbo_setaside_original#</td>
             <td class="text_xsmall" align=right width=75>#dateformat(fbo_pub_date_updated,'mm/dd/yyyy')#</td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray">Opportunities include sources sought, pre-solicitations and solicitations that have not been awarded and the archive date has not expired.</td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>