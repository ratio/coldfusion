<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="capabilites" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select usr_keywords from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfset len = listlen(capabilites.usr_keywords)>
<cfset cap_list = valuelist(capabilites.usr_keywords)>

<cfset g2m_count = 1>

<cfif not isdefined("sv")>
 <cfset sv = 8>
</cfif>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(recipient_duns), recipient_state_code, recipient_city_name, recipient_name, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(parent_award_id)) as contracts, count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data

 where
 (

  <cfloop index="c" list="#capabilites.usr_keywords#">
   contains((award_description),'"#c#"')
   <cfif g2m_count LT len>
   or
   </cfif>
   <cfset g2m_count = g2m_count + 1>
 </cfloop>

 )

and period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#'
and federal_action_obligation > 0

group by recipient_duns, recipient_name, recipient_state_code, recipient_city_name

<cfif #sv# is 1>
 order by recipient_name ASC
<cfelseif #sv# is 10>
 order by recipient_name DESC
<cfelseif #sv# is 2>
 order by recipient_duns ASC
<cfelseif #sv# is 20>
 order by recipient_duns ASC
<cfelseif #sv# is 3>
 order by recipient_city_name ASC
<cfelseif #sv# is 30>
 order by recipient_city_name DESC
<cfelseif #sv# is 4>
 order by recipient_state_code ASC
<cfelseif #sv# is 40>
 order by recipient_state_code DESC
<cfelseif #sv# is 5>
 order by agencies DESC
<cfelseif #sv# is 50>
 order by agencies ASC
<cfelseif #sv# is 6>
 order by contracts DESC
<cfelseif #sv# is 60>
 order by contracts ASC
<cfelseif #sv# is 7>
 order by awards DESC
<cfelseif #sv# is 70>
 order by awards ASC
<cfelseif #sv# is 8>
 order by total DESC
<cfelseif #sv# is 80>
 order by total ASC

</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">POTENTIAL GO TO MARKET (G2M) PARTNERS (#trim(numberformat(agencies.recordcount,'999,999'))#) *</td>
               <td class="feed_option" align=right><a href="/exchange/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>

           <tr><td class="feed_sub_header"><b>Keywords used "<i>#cap_list#</i>"</b></td>
               <td align=right class="feed_option">

               <a href="/exchange/opps/forecast/views/view04.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>

               </td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header">No partners were found.</td></tr>
          <cfelse>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
				 <td class="feed_option"><a href="view04.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>VENDOR / PARTNER</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option"><a href="view04.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option"><a href="view04.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>CITY</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view04.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>STATE</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view04.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AGENCIES</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view04.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>CONTRACTS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=center><a href="view04.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>AWARDS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=15></cfif></td>
				 <td class="feed_option" align=right><a href="view04.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>AMOUNT</b></a><cfif isdefined("sv") and sv is 8>&nbsp;&nbsp;<img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80>&nbsp;&nbsp;<img src="/images/icon_sort_down.png" width=15></cfif></td>
		      </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

			   <cfoutput>
				   <td class="feed_option"><a href="/exchange/include/federal_profile.cfm?duns=#agencies.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.recipient_name#</b></a></td>
				   <td class="feed_option">#agencies.recipient_duns#</td>
				   <td class="feed_option">#agencies.recipient_city_name#</td>
				   <td class="feed_option" align=center>#agencies.recipient_state_code#</td>
				   <td class="feed_option" align=center>#agencies.agencies#</td>
				   <td class="feed_option" align=center>#agencies.contracts#</td>
				   <td class="feed_option" align=center>#agencies.awards#</td>
				   <td class="feed_option" align=right>#numberformat(agencies.total,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

          </cfloop>

          <tr><td height=20></td></tr>
          <tr><td colspan=8><hr></td></tr>
		  <tr><td colspan=5 class="feed_option"><b><i>* Represents the companys that are delivering similar products or services to the Federal government within the past 2 years.</i></b></td></tr>
          <tr><td height=10></td></tr>

          </cfif>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>