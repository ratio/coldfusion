<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 4>
</cfif>

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="customers" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select distinct(awarding_sub_agency_code), awarding_sub_agency_name, count(distinct(parent_award_id)) as contracts, count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data
			   where contains((award_description),'"#ky#"') and
					 period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0
group by awarding_sub_agency_code, awarding_sub_agency_name

 <cfif sv is 1>
  order by awarding_sub_agency_name ASC
 <cfelseif sv is 10>
  order by awarding_sub_agency_name DESC

 <cfelseif sv is 2>
  order by contracts DESC
 <cfelseif sv is 20>
  order by contracts ASC

 <cfelseif sv is 3>
  order by awards DESC
 <cfelseif sv is 30>
  order by awards ASC

 <cfelseif sv is 4>
  order by total DESC
 <cfelseif sv is 40>
  order by total ASC

 </cfif>


</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">#ucase(ky)# CUSTOMERS (#trim(numberformat(customers.recordcount,'999,999'))#)</td>
               <td class="feed_option" align=right>

               <cfif isdefined("l")>
					<img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
               <cfelse>
				   <cfif isdefined("cus")>
					 <a href="/exchange/opps/forecast/custom.cfm"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a>
				   <cfelse>
					 <a href="/exchange/opps/forecast/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a>
				   </cfif>
               </cfif>

               </td></tr>

           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Customers are organizations that have purchased products, services or technologies for the capability selected and/or are paying to implement them over the next 2 years.</td></tr>

           <tr><td colspan=2><hr></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif customers.recordcount is 0>
           <tr><td class="feed_sub_header">No Customers were found.</td></tr>
          <cfelse>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>


			  <tr height=40>
				 <td class="feed_option"><a href="view35.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>CUSTOMER</b></a></td>
				 <td class="feed_option" align=center><a href="view35.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CONTRACTS</b></a></td>
				 <td class="feed_option" align=center><a href="view35.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AWARDS</b></a></td>
				 <td class="feed_option" align=right><a href="view35.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AMOUNT</b></a></td>
		      </tr>


          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="CUSTOMERS">

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

			   <cfoutput>
				   <td class="feed_option"><b>#customers.awarding_sub_agency_name#</b></a></td>
				   <td class="feed_option" align=center>#customers.contracts#</td>
				   <td class="feed_option" align=center>#customers.awards#</td>
				   <td class="feed_option" align=right>#numberformat(customers.total,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

          </cfloop>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>