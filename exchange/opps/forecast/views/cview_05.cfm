<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 40>
</cfif>

<cfif isdefined("ky")>
 <cfset session.search_filter = #ky#>
</cfif>

<body class="body">

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(patent_rawassignee.patent_id)) as total, name_first, name_last, organization, assignee_id from patent
 join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
 where contains((patent.patent_id, title, abstract),'"#session.search_filter#"')
 group by organization, name_first, name_last, assignee_id

 <cfif sv is 1>
  order by organization ASC
 <cfelseif sv is 10>
  order by organization DESC
 <cfelseif sv is 2>
  order by name_last ASC, name_first ASC
 <cfelseif sv is 20>
  order by name_last DESC, name_first ASC
 <cfelseif sv is 4>
  order by total asc
 <cfelseif sv is 40>
  order by total DESC
 <cfelseif sv is 5>
  order by fbo_opp_name ASC
 <cfelseif sv is 50>
  order by fbo_opp_name DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 80>
  order by fbo_naics DESC
 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC
 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC
 </cfif>

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">#numberformat(agencies.recordcount,'99,999')# COMPANIES FOUND THAT WERE AWARDED PATENTS</td>
				 <td class="feed_sub_header" align=right>
				 <a href="/exchange/opps/forecast/views/cview_05.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="/exchange/opps/forecast/views/cview_05.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
				 &nbsp;|&nbsp;
					 <img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
				     <a href="##" style="cursor: pointer;" onclick="windowClose();">Close</a>

				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

					<cfif agencies.recordcount GT #perpage#>

				    <td class="feed_sub_header">

				    Result Set -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				</td>
				</cfif>

				<td class="feed_header" align=right>
				<form action="filter.cfm" method="post">
				    <td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter</b>&nbsp;&nbsp;
				    <input class="input_text" type="text" size=30 name="filter" required value="#session.search_filter#" placeholder="i.e., Machine Learning, Navy, etc.">&nbsp;&nbsp;
				    <input class="button_blue" type="submit" name="button" value="Filter">
				    <input type="hidden" name="location" value="cview_05">
				    <input type="hidden" name="sv" value=#sv#>
                    </td>
                </form>
                </td>

				</tr>

			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <cfoutput>
         <tr>
            <td class="feed_sub_header"><a href="cview_05.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>COMPANY NAME</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header" align=right><a href="cview_05.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>PATENTS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=50>
         <cfelse>
          <tr bgcolor="e0e0e0" height=50>
         </cfif>

             <td class="feed_sub_header" width=500>
              <cfif organization is not "">
             	<b><a href="cview_05_details.cfm?name=#organization#"><cfif len(organization) GT 200>#ucase(left(organization,200))#...<cfelse>#ucase(organization)#</cfif></b>
              <cfelse>
             	<b>N/A</b>
              </cfif>
             </td>

             <td class="feed_option" align=right style="padding-left: 5px; padding-right: 5px; font-size: 18px; padding-right: 50px;"><b>#total#</b></td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>