<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 9>
</cfif>

<cfquery name="capabilites" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_keywords from company
 where company_id = #session.company_id#
</cfquery>

<cfset len = listlen(capabilites.company_keywords)>
<cfset cap_list = valuelist(capabilites.company_keywords)>

<cfset value_count = 1>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select
 distinct(award_id_piid), recipient_name, recipient_duns, max(period_of_performance_current_end_date) as max_date, count(modification_number) as mods, max(period_of_performance_current_end_date) as date, awarding_agency_name, awarding_sub_agency_name, max(potential_total_value_of_award) as potential_value, sum(base_and_all_options_value) as all_options, max(current_total_value_of_award) as current_value, sum(federal_action_obligation) as obligated, sum(base_and_exercised_options_value) as options from award_data
 where contains((award_description),'"#ky#"') and

 <cfif type is 1>
  action_date between '#dateformat(evaluate(now()-180),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#'
 <cfelseif type is 2>
  period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#'
 <cfelseif type is 3>
  period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#'
 <cfelseif type is 4>
  period_of_performance_current_end_date between '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#'
 <cfelseif type is 5>
  period_of_performance_current_end_date between '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#'
 </cfif>

 group by award_id_piid, recipient_name, recipient_duns, period_of_performance_current_end_date, awarding_agency_name, awarding_sub_agency_name

 <cfif sv is 1>
  order by max_date ASC
 <cfelseif sv is 10>
  order by max_date DESC

 <cfelseif sv is 2>
  order by award_id_piid ASC
 <cfelseif sv is 20>
  order by award_id_piid DESC

 <cfelseif sv is 3>
  order by mods ASC
 <cfelseif sv is 30>
  order by mods DESC

 <cfelseif sv is 4>
  order by awarding_sub_agency_name ASC
 <cfelseif sv is 40>
  order by awarding_sub_agency_name DESC

 <cfelseif sv is 5>
  order by obligated DESC
 <cfelseif sv is 50>
  order by obligated ASC

 <cfelseif sv is 6>
  order by options DESC
 <cfelseif sv is 60>
  order by options ASC

 <cfelseif sv is 7>
  order by all_options DESC
 <cfelseif sv is 70>
  order by all_options ASC

 <cfelseif sv is 8>
  order by current_value DESC
 <cfelseif sv is 80>
  order by current_value ASC

 <cfelseif sv is 9>
  order by potential_value DESC
 <cfelseif sv is 90>
  order by potential_value ASC

 <cfelseif sv is 11>
  order by recipient_name ASC
 <cfelseif sv is 110>
  order by recipient_name DESC


</cfif>


</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="URL.PageId" default="0">
<cfset RecordsPerPage = 250>
<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
<cfset EndRow = StartRow+RecordsPerPage-1>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_header">EXPIRING CONTRACTS - "<i>#ucase(ky)#</i>" - (

           <cfif type is 1>
             Last 180 Days
           <cfelseif type is 2>
             Next 90 Days
           <cfelseif type is 3>
             3-	12 Months
           <cfelseif type is 4>
             6-12 Months
           <cfelseif type is 5>
             1-2 Years
           </cfif>
           )

           </td>
               <td class="feed_option" align=right>

               <cfif isdefined("l")>
					<img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
               <cfelse>
				   <cfif isdefined("cus")>
				   <a href="/exchange/opps/forecast/custom.cfm"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a>
				   <cfelse>
				   <a href="/exchange/opps/forecast/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a>
				   </cfif>
               </cfif>

               </td></tr>
           <tr><td colspan=2><hr></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" colspan=16>No opportunities were found.</td></tr>
          <cfelse>

          <tr>
              <td class="feed_sub_header">

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
              </td>
              <td class="feed_sub_header" align=right>

              <cfoutput>

              <a href="view20.cfm?ky=#ky#&type=#type#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" title="Export to Excel" hspace=10></a><a href="view20.cfm?ky=#ky#&type=#type#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Download to Excel</a>

              </cfoutput>

              </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
				 <td class="feed_option"><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>End Date</b></a></td>
				 <td class="feed_option"><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Award ##</b></a></td>
				 <td class="feed_option" align=center><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Actions</b></a></td>
				 <td class="feed_option"><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Incumbent</b></a></td>
				 <td class="feed_option"><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Department / Agency</b></a></td>
				 <td class="feed_option"><b>Award Description</b></td>
				 <td class="feed_option" align=right><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Obligated</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>With Options</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>All Options</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Current Value</b></a></td>
				 <td class="feed_option" align=right><a href="view20.cfm?ky=#ky#<cfif isdefined("cus")>&cus=1</cfif>&type=#type#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Potential Value</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

            <cfquery name="award" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
             select top(1) award_description from award_data
             where award_id_piid = '#agencies.award_id_piid#'
             order by action_date ASC
            </cfquery>

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=50>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=50>
			   </cfif>

			   <cfoutput>

				   <td class="feed_option" valign=top width=75>#dateformat(date,'mm/dd/yyyy')#</td>
				   <td class="feed_option" valign=top><a href="/exchange/include/find_award.cfm?award_id=#agencies.award_id_piid#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.award_id_piid#</b></a></td>
				   <td class="feed_option" style="font-weight: normal;" valign=top align=center width=75>#mods#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#recipient_name#</b></a></td>
				   <td class="feed_option" style="font-weight: normal;" valign=top width=300>#agencies.awarding_agency_name#<br>#agencies.awarding_sub_agency_name#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top>#award.award_description#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top align=right width=100>#numberformat(options,'$999,999,999')#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top align=right width=100>#numberformat(all_options,'$999,999,999')#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top align=right width=100>#numberformat(current_value,'$999,999,999')#</td>
				   <td class="feed_option" style="font-weight: normal;" valign=top align=right width=100>#numberformat(potential_value,'$999,999,999')#</td>
			   </cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

			  </cfif>

			  <cfif CurrentRow eq EndRow>
			   <cfbreak>
			  </cfif>

          </cfloop>

          <tr><td colspan=17><hr></td></tr>
          <tr>
             <td class="feed_option"><b>Total</b></td>
             <td class="feed_option" colspan=16 align=right><b><cfoutput>#numberformat(tot,'$999,999,999')#</cfoutput></b></td>
          </tr>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>