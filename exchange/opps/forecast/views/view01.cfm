<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="capabilites" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select usr_keywords from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>
<cfset len = listlen(capabilites.usr_keywords)>
<cfset cap_list = valuelist(capabilites.usr_keywords)>

<cfset opp_count = 1>
<cfset fbo_count = 1>

<cfquery name="fbo_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(fbo_solicitation_number)) as total from fbo
 where
 (
 <cfloop index="c" list="#capabilites.usr_keywords#">
  contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#c#"')
  <cfif fbo_count LT len>
  or
  </cfif>
  <cfset fbo_count = fbo_count + 1>
 </cfloop>
 )

 and fbo_archive_date > '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select fbo_solicitation_number, fbo_id, fbo_agency, fbo_office, fbo_location, fbo_opp_name, class_code_name, fbo_notice_type, fbo_naics, fbo_setaside, fbo_date_posted, fbo_image_url, fbo_synopsis from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics
 where

 (
 <cfloop index="c" list="#capabilites.usr_keywords#">
  contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#c#"')
  <cfif opp_count LT len>
  or
  </cfif>
  <cfset opp_count = opp_count + 1>
 </cfloop>
 )

 and fbo_archive_date > '#fb_date#'
 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')

<cfif isdefined("sv")>

 <cfif sv is 1>
  order by fbo_solicitation_number ASC
 <cfelseif sv is 10>
  order by fbo_solicitation_number DESC

 <cfelseif sv is 2>
  order by fbo_agency ASC
 <cfelseif sv is 20>
  order by fbo_agency DESC

 <cfelseif sv is 3>
  order by fbo_office ASC
 <cfelseif sv is 30>
  order by fbo_office DESC

 <cfelseif sv is 4>
  order by fbo_location ASC
 <cfelseif sv is 40>
  order by fbo_location DESC

 <cfelseif sv is 5>
  order by class_code_name ASC
 <cfelseif sv is 50>
  order by class_code_name DESC

 <cfelseif sv is 6>
  order by fbo_notice_type ASC
 <cfelseif sv is 60>
  order by fbo_notice_type DESC

 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 70>
  order by fbo_naics DESC

 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC

 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC

 </cfif>
<cfelse>
order by fbo_date_posted
</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
			 <tr><td class="feed_header">OPPORTUNITIES FOUND (#ltrim(numberformat(fbo_count.total,'999,999'))# Unique) - #agencies.recordcount# Postings and Modifications</td>
				 <td class="feed_option" align=right>
				 <a href="/exchange/opps/forecast/views/view01.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
				 &nbsp;&nbsp;&nbsp;
				 <a href="/exchange/"><img src="/images/delete.png" width=20 alt="Close" title="Close"></a>
				 </td></tr>

         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>

           <tr><td class="feed_sub_header"><b>Keywords used "<i>#cap_list#</i>"</b></td>
               <td align=right class="feed_option"></td></tr>


         <tr><td height=10></td></tr>
         </cfoutput>
       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
            <td></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>SOLICIATION #</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DEPARTMENT</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AGENCY / OFFICE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>LOCATION</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAME / TITLE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" width=50><a href="view01.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>TYPE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" align=center><a href="view01.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option"><a href="view01.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>SET ASIDE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 9><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 90><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_option" width=110 align=right><a href="view01.cfm?<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>DATE POSTED</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 10><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 100><img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>

         <cfset counter = 0>

         <cfoutput query="agencies">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=40>
         <cfelse>
          <tr bgcolor="e0e0e0" height=40>
         </cfif>

			 <cfif fbo_image_url is "">
			  <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/fbo.png" valign=top align=top width=30 border=0 vspace=10></a></td>
			 <cfelse>
			  <td width=70 class="table_row" valign=middle><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#fbo_image_url#" align=top width=30 border=0 vspace=10></a></td>
			 </cfif>

             <td class="text_xsmall" valign=middle width=150><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#fbo_solicitation_number#</b></a></td>
             <td class="text_xsmall" valign=middle>#fbo_agency#</td>
             <td class="text_xsmall" valign=middle>#fbo_office#</td>
             <td class="text_xsmall" valign=middle>#fbo_location#</td>

             <td class="text_xsmall">#fbo_opp_name#</td>
             <td class="text_xsmall">#class_code_name#</td>

             <td class="text_xsmall">#fbo_notice_type#</td>
             <td class="text_xsmall" align=center width=75>#fbo_naics#</td>
             <td class="text_xsmall" width=120>#fbo_setaside#</td>
             <td class="text_xsmall" align=right>#dateformat(fbo_date_posted,'mm/dd/yyyy')#</td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray">Opportunities include sources sought, pre-solicitations and solicitations that have not been awarded and the archive date has not expired.</td></tr>
         <tr><td height=10></td></tr>


        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>