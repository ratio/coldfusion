<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<body class="body">

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

 select organization, patent.patent_id, title, abstract as description, patent.type as patent_type, patent.date from patent
 join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
 where contains((patent.patent_id, title, abstract),'"#session.search_filter#"') and
 organization = '#name#'

 <cfif sv is 1>
  order by title ASC
 <cfelseif sv is 10>
  order by title DESC
 <cfelseif sv is 2>
  order by date DESC
 <cfelseif sv is 20>
  order by date ASC
 <cfelseif sv is 4>
  order by total asc
 <cfelseif sv is 40>
  order by total DESC
 <cfelseif sv is 5>
  order by fbo_opp_name ASC
 <cfelseif sv is 50>
  order by fbo_opp_name DESC
 <cfelseif sv is 6>
  order by class_code_name ASC
 <cfelseif sv is 60>
  order by class_code_name DESC
 <cfelseif sv is 7>
  order by fbo_notice_type ASC
 <cfelseif sv is 70>
  order by fbo_notice_type DESC
 <cfelseif sv is 8>
  order by fbo_naics ASC
 <cfelseif sv is 80>
  order by fbo_naics DESC
 <cfelseif sv is 9>
  order by fbo_setaside ASC
 <cfelseif sv is 90>
  order by fbo_setaside DESC
 <cfelseif sv is 10>
  order by fbo_date_posted ASC
 <cfelseif sv is 100>
  order by fbo_date_posted DESC
 </cfif>

</cfquery>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>

			 <tr><td class="feed_header">#name# (#numberformat(agencies.recordcount)# Patents Found)</td>
				 <td class="feed_option" align=right>
				 <a href="/exchange/opps/forecast/views/cview_05_details.cfm?name=#name#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
				 &nbsp;&nbsp;&nbsp;&nbsp;
					 <a href="cview_05.cfm"><img src="/images/delete.png" width=20 alt="Close" title="Close" border=0></a>
				 </td></tr>
				</cfoutput>
            </form>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>

       </table>

       <!--- Show Opportunities --->

	    <cfoutput>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr>

					<cfif agencies.recordcount GT #perpage#>

				    <td class="feed_sub_header">

				    Result Set -&nbsp;&nbsp;

						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				</td>
				</cfif>

				<td class="feed_header" align=right>
				<form action="filter.cfm" method="post">
				    <td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter</b>&nbsp;&nbsp;
				    <input class="input_text" type="text" size=30 name="filter" required value="#session.search_filter#" placeholder="i.e., Machine Learning, Navy, etc.">&nbsp;&nbsp;
				    <input class="button_blue" type="submit" name="button" value="Filter">
				    <input type="hidden" name="location" value="cview_05_details">
				    <input type="hidden" name="name" value="#name#">
				    <input type="hidden" name="sv" value=#sv#>
                    </td>
                </form>
                </td>

				</tr>

			</table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif agencies.recordcount is 0>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No matching Patents were found.</td></tr>

        <cfelse>


        <cfoutput>
         <tr>
            <td class="feed_sub_header"><a href="cview_05_details.cfm?name=#name#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>TITLE</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=15></cfif></td>
            <td class="feed_sub_header"><B>DESCRIPTION</b></td>
            <td class="feed_sub_header" align=center><B>TYPE</b></td>
            <td class="feed_sub_header" align=right><a href="cview_05_details.cfm?name=#name#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>GRANTED</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=15><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=15></cfif></td>
         </tr>
         </cfoutput>

         <cfset counter = 0>

         <cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=50>
         <cfelse>
          <tr bgcolor="e0e0e0" height=50>
         </cfif>

             <td class="feed_sub_header" width=400 valign=top style="padding-right: 10px;"><b><a href="/exchange/include/patent_information.cfm?patent_id=#patent_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#title#</b></a></td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top>#description#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top width=75 align=center>#ucase(patent_type)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top align=right width=100>#dateformat(date,'mm/dd/yyyy')#</td>

         </tr>

         <cfif counter is 0>
          <cfset counter = 1>
         <cfelse>
          <cfset counter = 0>
         </cfif>

         </cfoutput>

         <tr><td height=20></td></tr>
         <tr><td colspan=10 class="link_small_gray"></td></tr>
         <tr><td height=10></td></tr>

         </cfif>

        </table>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>