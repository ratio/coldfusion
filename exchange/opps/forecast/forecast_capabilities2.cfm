		  <cfquery name="capabilites" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		    select usr_keywords from usr
		    where usr_id = #session.usr_id#
		  </cfquery>

          <cfif capabilites.usr_keywords is "">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any keywords.  <a href="/exchange/profile/edit.cfm"><b>Click here</b></a> to add them to your profile.</td></tr>
          </table>

          <cfelse>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		    <tr><td colspan=5 class="feed_sub_header" style="font-weight: normal;">Opportunity snapshots are based on the <u>first five (5) keywords</u> listed in your <a href="/exchange/profile/"><u>Profile</u></a>.</td></tr>
 		    <tr><td height=20></td></tr>

			<tr>
			   <td></td>
			   <td class="feed_header"><img src="/images/icon_box_green.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;PROCUREMENTS</td>
			   <td class="feed_header" align=center><img src="/images/icon_box_red.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;PAST CONTRACTS</td>
			   <td class="feed_header" colspan=4 align=center><img src="/images/icon_box_blue.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;EXPIRING CONTRACTS</td>
			   <td class="feed_header" align=center><img src="/images/icon_box_gray.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;G2M Partners</td>
			   <td></td>
		    </tr>

		   <tr><td height=5></td></tr>

			   <tr>
                  <td></td>
				  <td class="feed_sub_header" align=center>Active Now</td>
				  <td class="feed_sub_header" align=center>Last 180 Days</td>
				  <td class="feed_sub_header" align=center>Next 90 Days</td>
				  <td class="feed_sub_header" align=center>3 - 6 Months</td>
				  <td class="feed_sub_header" align=center>6 - 12 Months</td>
				  <td class="feed_sub_header" align=center>1-2 Years</td>
				  <td class="feed_sub_header" align=center>Awardees</td>
			   </tr>

			   <tr><td height=5></td></tr>

	      <cfset counter = 0>

          <cfloop index="c" list="#capabilites.usr_keywords#">

          <cfif counter LTE 5>

          <cfset session.opp_keyword = #trim(c)#>

	          <cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

			  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(fbo_solicitation_number)) as total from fbo
			   where contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#session.opp_keyword#"')
			   and fbo_archive_date > '#fb_date#'
			   and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
			  </cfquery>

			  <cfquery name="awards_last180" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where contains((award_description),'"#session.opp_keyword#"') and
					 action_date between '#dateformat(evaluate(now()-180),'mm/dd/yyyy')#' and '#dateformat(now(),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)
			  </cfquery>

			  <cfquery name="awards_next90" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where contains((award_description),'"#session.opp_keyword#"') and
					 period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)
			  </cfquery>

			  <cfquery name="awards_next180" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where contains((award_description),'"#session.opp_keyword#"') and
					 period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)
			  </cfquery>

			  <cfquery name="awards_next365" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where contains((award_description),'"#session.opp_keyword#"') and
					 period_of_performance_current_end_date between '#dateformat(evaluate(now()+180),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)
			  </cfquery>

			  <cfquery name="awards_next2" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where contains((award_description),'"#session.opp_keyword#"') and
					 period_of_performance_current_end_date between '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)
			  </cfquery>


			  <cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(recipient_duns)) as total from award_data
			   where contains((award_description),'"#session.opp_keyword#"') and
					 period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0
			  </cfquery>

          <cfoutput>

			   <tr>
				  <td class="feed_sub_header" width=100>#session.opp_keyword#</td>
				  <td class="big_number" align=center style="background-color: AEDDAF;"><cfif #fbo.total# is 0>#fbo.total#<cfelse><a href="/exchange/opps/forecast/views/view10.cfm?ky=#session.opp_keyword#">#fbo.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: ECBDBC;"><cfif #awards_last180.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=1">#awards_last180.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next90.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=2">#awards_next90.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next180.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=3">#awards_next180.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next365.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=4">#awards_next365.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next2.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=5">#awards_next2.awards#</a></cfif></td>
				  <td class="big_number" align=center><cfif #vendors.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view30.cfm?ky=#session.opp_keyword#">#vendors.total#</a></cfif></td>
			   </tr>

			   <tr>
				  <td class="feed_sub_header" colspan=2 align=center></td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_last180.total,'$999,999,999'))#</td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next90.total,'$999,999,999'))#</td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next180.total,'$999,999,999'))#</td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next365.total,'$999,999,999'))#</td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next2.total,'$999,999,999'))#</td>
				  <td></td>
			   </tr>
	           <tr><td colspan=8><hr></td></tr>
               </cfoutput>

               <cfset counter = counter + 1>

               </cfif>

             </cfloop>

         <tr><td height=15></td></tr>
         <tr><td colspan=10 class="link_small_gray">Active procurements include sources sought, pre-solicitations and solicitations that have not been awarded and the archive date has not expired.</td></tr>
         <tr><td height=10></td></tr>

          </table>

        </cfif>