<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("from")>
 <cfset session.from = #from#>
 <cfset session.to = #to#>
 <cfset session.agency_code = #agency_code#>
 <cfset session.naics = #naics#>
 <cfset session.keyword = #keyword#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Potential Recompetes</td><td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
           <tr><td>&nbsp;</td></tr>
          </table>

     	  <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select id, award_id_piid, action_date, recipient_duns, recipient_name, awarding_sub_agency_name, award_description, type_of_set_aside, product_or_service_code_description,
				   type_of_contract_pricing, federal_action_obligation, base_and_exercised_options_value,base_and_all_options_value, period_of_performance_start_date,
				   period_of_performance_current_end_date,period_of_performance_potential_end_date from award_data
			where ( awarding_agency_code = '#session.agency_code#' and
			     (
			      (period_of_performance_current_end_date >= '#session.from#') and (period_of_performance_current_end_date <= '#session.to#') or
			      (period_of_performance_potential_end_date >= '#session.from#') and (period_of_performance_potential_end_date <= '#session.to#')
			      )

			 and federal_action_obligation > 0
			 <cfif #session.naics# is not "">
			  and naics_code = '#session.naics#'
			 </cfif>

			 )

		   <cfif isdefined("session.keyword") and #session.keyword# is not "">
		     and contains((award_description, awarding_office_name, funding_office_name, solicitation_identifier, award_id_piid, parent_award_id),'"#session.keyword#"')
		   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by action_date ASC
		    <cfelseif #sv# is 10>
             order by action_date DESC
		    <cfelseif #sv# is 2>
             order by recipient_name ASC
		    <cfelseif #sv# is 20>
             order by recipient_name DESC
		    <cfelseif #sv# is 3>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 4>
             order by type_of_set_aside ASC
		    <cfelseif #sv# is 40>
             order by type_of_set_aside DESC
		    <cfelseif #sv# is 5>
             order by product_or_service_code_description ASC
		    <cfelseif #sv# is 50>
             order by product_or_service_code_description DESC
		    <cfelseif #sv# is 6>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 60>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 7>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 70>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 8>
             order by base_and_exercised_options_value DESC
		    <cfelseif #sv# is 80>
             order by base_and_exercised_options_value ASC
		    <cfelseif #sv# is 9>
             order by base_and_all_options_value DESC
		    <cfelseif #sv# is 90>
             order by base_and_all_options_value ASC
		    <cfelseif #sv# is 10>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 100>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 11>
             order by period_of_performance_current_end_date DESC
		    <cfelseif #sv# is 110>
             order by period_of_performance_current_end_date ASC
		    <cfelseif #sv# is 12>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 120>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 13>
             order by award_id_piid ASC
		    <cfelseif #sv# is 130>
             order by award_id_piid DESC
		    </cfif>

		   <cfelse>
			order by period_of_performance_current_end_date DESC

		   </cfif>

		  </cfquery>

     	  <cfquery name="agency_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select top(1) awarding_agency_name from award_data
           where awarding_agency_code = '#session.agency_code#'
          </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>
           <tr><td class="feed_header">#agency_name.awarding_agency_name#</td>
               <td class="feed_option" align=right><b>NAICS: </b><cfif #session.naics# is not "">#session.naics#<cfelse>All</cfif>&nbsp;|&nbsp;<b>From:</b> #dateformat(session.from,'mm/dd/yyyy')# <b>To:</b> #dateformat(session.to,'mm/dd/yyyy')#</td></tr>
           <tr><td class="feed_option"><b>Total Opportunities: #numberformat(awards.recordcount,'999,999')#</b></td></tr>
           <tr><td colspan=12><hr></td></tr>
          </cfoutput>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td>&nbsp;</td></tr>

          <cfif awards.recordcount is 0>
           <tr><td class="feed_option">No potential recompetes found.  Please try to refine your search criteria.</td></tr>
          <cfelse>

          <tr>

              <td class="text_xsmall" width=75><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
              <td class="text_xsmall" width=100><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Award #</b></a></td>
              <td class="text_xsmall"><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Incumbent</b></a></td>
              <td class="text_xsmall"><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Agency</b></a></td>
              <td class="text_xsmall"><b>Award Description</b></td>
              <td class="text_xsmall"><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Set Aside</b></a></td>
              <td class="text_xsmall"><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Product or Service</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Pricing</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Base</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Options</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Total</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>POP Start</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>POP End</b></a></td>
              <td class="text_xsmall" align=right><a href="recompetes.cfm?<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Contract End</b></a></td>
          </tr>

          <cfset counter = 0>

          <cfoutput query="awards">

           <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">
           </cfif>

              <td class="text_xsmall" valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
              <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
              <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
              <td class="text_xsmall" valign=top width=300>#award_description#</td>
              <td class="text_xsmall" valign=top>#type_of_set_aside#</td>
              <td class="text_xsmall" valign=top>#product_or_service_code_description#</td>
              <td class="text_xsmall" valign=top align=right>

              <cfif #type_of_contract_pricing# is "Firm Fixed Price">
               FFP
              <cfelseif #type_of_contract_pricing# is "Time & Materials">
               T&M
              <cfelseif #type_of_contract_pricing# is "Cost Plus Fixed Fee">
               CPFF
              <cfelseif #type_of_contract_pricing# is "Labor Hours">
               LH
              <cfelse>
              #type_of_contract_pricing#
              </cfif></td>
              <td class="text_xsmall" valign=top width=75 align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#numberformat(base_and_exercised_options_value,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#numberformat(base_and_all_options_value,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
           </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <tr><td>&nbsp;</td></tr>

          <tr><td class="text_xsmall" colspan=8>
          <i>
           <b>PSC</b> - Products or Service Code<br>
           <b>POP</b> - Period of Performance
          </i>
          </td></tr>


          </cfif>

          </table>




	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>