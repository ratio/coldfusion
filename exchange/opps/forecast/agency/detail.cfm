<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Forecast</td>
               <td class="feed_option" align=right><a href="dhs.cfm">Return</a></td></tr>
           <tr><td>&nbsp;</td></tr>
          </table>

		  <cfquery name="detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select * from dhs
		   where apfs_number = '#id#'
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput query="detail">

           <tr>
           <td class="feed_option" valign=top width=175><b>Organization: </b></td>
           <td class="feed_option" valign=top>#component#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Description: </b></td>
           <td class="feed_option" valign=top>#description#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Estimated Value: </b></td>
           <td class="feed_option" valign=top>#dollar_range#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Estimated Release Date: </b></td>
           <td class="feed_option" valign=top>#dateformat(estimated_release,'mm/dd/yyyy')#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Small Business: </b></td>
           <td class="feed_option" valign=top>#small_business_program#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Estimated Award: </b></td>
           <td class="feed_option" valign=top>#anticipated_award_quarter#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Anticipated Vehicle: </b></td>
           <td class="feed_option" valign=top>#contract_vehicle#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Type: </b></td>
           <td class="feed_option" valign=top><cfif contract_status is "NEW">New<cfelse>Follow-On</cfif></td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Incumbent: </b></td>
           <td class="feed_option" valign=top>#contractor#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>NAICS Code: </b></td>
           <td class="feed_option" valign=top>#naics_code#</td>
           </tr>

           <tr>
           <td class="feed_option" valign=top width=100><b>Point of Contact: </b></td>
           <td class="feed_option" valign=top>#Poc_name#<br>Email - #poc_email#<br>Phone - #poc_phone#</td>
           </tr>



          </cfoutput>

          </table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>