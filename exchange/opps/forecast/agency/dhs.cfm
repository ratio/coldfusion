<cfinclude template="/exchange/security/check.cfm">
<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

       <div class="main_box">

		  <cfquery name="forecast" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select * from dhs

		   <cfif isdefined("nf") and listlen(naics_list) GT 0>
		    where naics_code in (#naics_list#)
		   </cfif>

		   order by estimated_release DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">DHS Forecast - Long Range Planning <cfoutput>(#forecast.recordcount# Opportunities)</cfoutput></td>
               <td class="feed_option" align=right><a href="/exchange/forecast/">Return</a></td></tr>
           <tr><td>&nbsp;</td></tr>

		   <cfif isdefined("nf") and listlen(naics_list) is 0>
            <tr><td><font size=1 color="red"><b>You have not added any NAICS codes to your company profile.</font></td></tr>
            <tr><td>&nbsp;</td></tr>
           </cfif>

          </table>

		  <cfset counter = 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr>
               <td class="feed_option"><b>Est Release</b></td>
               <td class="feed_option"><b>Est Award</b></td>
               <td class="feed_option"><b>Organization</b></td>
               <td class="feed_option"><b>Description</b></td>
               <td class="feed_option"><b>Status</b></td>
               <td class="feed_option"><b>Type</b></td>
               <td class="feed_option"><b>Value Range</b></td>
               <td class="feed_option"><b>NAICS</b></td></tr>

           <cfoutput query="forecast">
           <tr

                       <cfif counter is 0>
		                bgcolor="ffffff"
		               <cfelse>
		                bgcolor="e0e0e0"
            </cfif>

           >
              <td class="feed_option" width=100><a href="detail.cfm?id=#apfs_number#">#dateformat(estimated_release,'mm/dd/yyyy')#</a></td>
              <td class="feed_option">#anticipated_award_quarter#</td>
              <td class="feed_option" width=120>#component#</td>
              <td class="feed_option" width=400>#left(description,'50')#...</td>
              <td class="feed_option"><cfif #contract_status# is "NEW">New<cfelse>Follow-On</cfif></td>
              <td class="feed_option">#small_business_program#</td>
              <td class="feed_option" width=200>#dollar_range#</td>
              <td class="feed_option">#naics_code#</td>
           </tr>

           <cfif counter is 0>
            <cfset counter = 1>
           <cfelse>
            <cfset counter = 0>
           </cfif>

           </cfoutput>

          </table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>