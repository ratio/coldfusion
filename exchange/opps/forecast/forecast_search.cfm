<cfset from = dateadd('d',-30,now())>

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 order by department_name
</cfquery>

 <cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_notice_type) from fbo
  order by fbo_notice_type
 </cfquery>

 <cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from set_aside
  order by set_aside_name
 </cfquery>

 <cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  order by state_name
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header" valign=top height=24><img src="/images/icon_forecast2.png" width=20 align=absmiddle>&nbsp;&nbsp;&nbsp;OPPORTUNITY FORECAST</td></tr>
  <tr><td height=10></td></tr>
 </table>

 <cfif not isdefined("session.forecast_from")>
 	<cfset from = dateadd('d',90,now())>
 </cfif>

 <cfif not isdefined("session.forecast_to")>
 	<cfset to = dateadd('d',730,now())>
 </cfif>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="set.cfm" method="post">

  <tr>
	  <td class="feed_option"><b>Department</b></td>
	  <td class="feed_option">

		  <select name="forecast_dept" class="input_select" style="width: 150px;">
		   <option value=0>All
		   <cfoutput query="dept">
		   <option value="#department_code#" <cfif isdefined("session.forecast_dept")><cfif #session.forecast_dept# is #department_code#>selected</cfif></cfif>>#department_name#
		   </cfoutput>
		  </select>

	  </td>

      <cfoutput>

	  <td class="feed_option"><b>Keyword</b></td>
	  <td class="feed_option"><input type="text" name="forecast_keyword" class="input_text" style="width: 150px;" maxlength="100" size="40" placeholder="Keyword" <cfif isdefined("session.forecast_keyword")>value="#session.forecast_keyword#"</cfif>></td>

	  <td class="feed_option"><b>Vendor</b></td>
	  <td class="feed_option"><input type="text" name="forecast_duns" class="input_text" style="width: 120px;" maxlength="100" size="40" placeholder="DUNS Number" <cfif isdefined("session.forecast_duns")>value="#session.forecast_duns#"</cfif>>

      &nbsp;<a href="/exchange/marketplace/" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_search.png" height=18 alt="Vendor Lookup" title="Vendor Lookup"></a>

	  </td>

	  <td class="feed_option"><b>NAICS Code(s) *</b></td>
		  <td class="feed_option">
		  <input type="text" name="forecast_naics" class="input_text" style="width: 120px;" maxlength="299" <cfif isdefined("session.forecast_naics")>value="#session.forecast_naics#"</cfif>>
		  &nbsp;<a href="https://www.census.gov/eos/www/naics/" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup"></a>
		  </td>

	  <td class="feed_option"><b>From</b></td>
	  <td class="feed_option"><input type="date" class="input_date" name="forecast_from" required style="width: 165px;" <cfif isdefined("session.forecast_from")>value="#dateformat(session.forecast_from,'yyyy-mm-dd')#"<cfelse>value="#dateformat(from,'yyyy-mm-dd')#"</cfif>></td>
	  <td class="feed_option" align=right><input class="button_blue" type="submit" name="button" value="Search"></td>

	  </cfoutput>

	</tr>

  <tr>
	  <td class="feed_option"><b>Agency</b></td>
	  <td class="feed_option">
		  <select name="forecast_agency" class="input_select" style="width: 150px;">
		   <option value=0>All
		   <cfoutput query="type">
			<option value="#fbo_notice_type#" <cfif isdefined("session.forecast_agency") and #session.forecast_agency# is #fbo_notice_type#>selected</cfif>>#fbo_notice_type#
		   </cfoutput>
		  </select>
	  </td>

	  <td class="feed_option"><b>Set Aside</b></td>
	  <td class="feed_option">
							<select name="forecast_setaside" class="input_select" style="width: 150px;">
							 <option value=0>No Preference
							 <cfoutput query="setaside">
							   <option value="#set_aside_code#" <cfif isdefined("session.forecast_setaside") and #set_aside_code# is #session.forecast_setaside#>selected</cfif>>#set_aside_name#
							 </cfoutput>
		  </select>

	  </td>

	  <td class="feed_option"><b>State</b></td>
	  <td class="feed_option">

		  <select name="forecast_state" class="input_select" style="width: 120px;">
		   <option value=0>All
		   <cfoutput query="states">
		   <option value="#state_abbr#">#state_name#
		   </cfoutput>
		  </select>

	  </td>

	  <cfoutput>

		  <td class="feed_option"><b>PS Code(s) *</b></td>
		  <td class="feed_option"><input type="text" name="forecast_psc" class="input_text" style="width: 120px;" maxlength="299" <cfif isdefined("session.forecast_psc")>value="#session.forecast_psc#"</cfif>>
		  &nbsp;<a href="https://www.fbo.gov/index?s=getstart&mode=list&tab=list&tabmode=list&static=faqs##q4" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_search.png" height=18 alt="PSC Code Lookup" title="PSC Code Lookup"></a>

	  </td>

	  <td class="feed_option"><b>To</b></td>
	  <td class="feed_option"><input type="date" name="forecast_to" required class="input_date" style="width: 165px;" <cfif isdefined("session.forecast_to")>value="#dateformat(session.forecast_to,'yyyy-mm-dd')#"<cfelse>value="#dateformat(to,'yyyy-mm-dd')#"</cfif>></td>

      </cfoutput>

	</tr>
	<tr><td height=10></td></tr>
	<tr><td class="text_xsmall" colspan=10>* to search for multiple codes, seperate values with commas (i.e., 43412,33928, etc.)</td></tr>

  </form>

 </table>