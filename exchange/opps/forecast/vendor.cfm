<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

     	  <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data
			where (recipient_duns = '#vduns#'

			and (
			      (period_of_performance_current_end_date >= '#vfrom#') and (period_of_performance_current_end_date <= '#vto#') or
			      (period_of_performance_potential_end_date >= '#vfrom#') and (period_of_performance_potential_end_date <= '#vto#')
			     )

			<cfif vnaics is not "">
			 and naics_code = '#vnaics#'
			</cfif>

			<cfif vpsc is not "">
			 and product_or_service_code = '#vpsc#'
			</cfif>
		   )

		   <cfif isdefined("keyword") and #keyword# is not "">
		     and contains((award_description, awarding_office_name, funding_office_name, solicitation_identifier, award_id_piid, parent_award_id),'"#keyword#"')
		   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by award_id_piid ASC
		    <cfelseif #sv# is 10>
             order by award_id_piid DESC
		    <cfelseif #sv# is 2>
             order by action_date DESC
		    <cfelseif #sv# is 20>
             order by action_date ASC
		    <cfelseif #sv# is 3>
             order by awarding_agency_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_agency_name DESC
		    <cfelseif #sv# is 4>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 6>
             order by naics_code ASC
		    <cfelseif #sv# is 60>
             order by naics_code DESC
		    <cfelseif #sv# is 7>
             order by product_or_service_code ASC
		    <cfelseif #sv# is 70>
             order by product_or_service_code DESC
		    <cfelseif #sv# is 8>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 80>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 9>
             order by base_and_exercised_options_value DESC
		    <cfelseif #sv# is 90>
             order by base_and_exercised_options_value ASC
		    <cfelseif #sv# is 10>
             order by base_and_all_options_value DESC
		    <cfelseif #sv# is 100>
             order by base_and_all_options_value ASC
		    <cfelseif #sv# is 11>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 110>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 12>
             order by period_of_performance_current_end_date DESC
		    <cfelseif #sv# is 120>
             order by period_of_performance_current_end_date ASC
		    <cfelseif #sv# is 13>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 130>
             order by period_of_performance_potential_end_date ASC

		    </cfif>

		   <cfelse>
			order by period_of_performance_current_end_date DESC

		   </cfif>

		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Potential Recompetes (<cfoutput>#awards.recordcount#</cfoutput>)</td><td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
           <tr><td>&nbsp;</td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

     	  <cfquery name="vendor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from sams
			where duns = '#vduns#'
	      </cfquery>

          <cfoutput>
           <tr><td class="feed_option"><b>Vendor - </b>

           <cfif vendor.recordcount is 0>
            DUNS Number #vduns# not found.
           <cfelse>
            <a href="/exchange/include/federal_profile.cfm?duns=#vendor.duns#" target="_blank" rel="noopener" rel="noreferrer">#vendor.legal_business_name#</a>
           </cfif>

           </td><tr>
           <tr><td><hr></td></tr>
          </cfoutput>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif awards.recordcount is 0>
           <tr><td class="feed_option">No contracts or awards found.  Please try to refine your search criteria.</td></tr>
          <cfelse>

          <cfoutput>

          <tr>
              <td class="text_xsmall" width=75><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Contract ##</b></a></td>
              <td class="text_xsmall" width=75><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Award Date</b></a></td>
              <td class="text_xsmall"><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Department</b></a></td>
              <td class="text_xsmall"><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
              <td class="text_xsmall"><b>Award Description</b></td>
              <td class="text_xsmall"><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>NAICS</b></a></td>
              <td class="text_xsmall"><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>PSC</b></a></td>
              <td class="text_xsmall" align=right><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Base</b></a></td>
              <td class="text_xsmall" align=right><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Options</b></a></td>
              <td class="text_xsmall" align=right><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>Total</b></a></td>
              <td class="text_xsmall" align=right><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>POP Start</b></a></td>
              <td class="text_xsmall" align=right><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>POP End</b></a></td>
              <td class="text_xsmall" align=right><a href="vendor.cfm?vto=#vto#&vfrom=#vfrom#&vduns=#vduns#&vpsc=#vpsc#&vnaics=#vnaics#&<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Potential End</b></a></td>
          </tr>

          <tr><td height=5></td></tr>

          </cfoutput>

          <cfset counter = 0>

          <cfoutput query="awards">

           <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">
           </cfif>

              <td class="text_xsmall" valign=top width=120><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
              <td class="text_xsmall" valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=top>#awarding_agency_name#</td>
              <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
              <td class="text_xsmall" valign=top width=300>#award_description#</td>
              <td class="text_xsmall" valign=top width=75>#naics_code#</td>
              <td class="text_xsmall" valign=top>#product_or_service_code#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#numberformat(base_and_exercised_options_value,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#numberformat(base_and_all_options_value,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=top width=75 align=right>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
           </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <tr><td>&nbsp;</td></tr>

          </cfif>

          </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>