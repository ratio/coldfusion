  <cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

  <center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		    <cfoutput>
 		    <tr><td class="feed_sub_header" STYLE="font-weight: normal;">You searched for "<i><b>#session.forecast_keyword#</b></i>".</td>
 		        <td class="feed_sub_header" align=right><a href="index.cfm">Return</a>&nbsp;&nbsp;<a href="index.cfm"><img src="/images/delete.png" width=20 alt="Return" title="Return" border=0></a></td></tr>
 		    </cfoutput>
          </table>

          <cfset counter = 1>

          <cfset add_count = 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td height=10></td></tr>

			<tr>
			   <td class="feed_header" colspan=4 align=center width=45%><img src="/images/icon_box_green.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;ACTIVE NOW</td>
			   <td class="feed_header" colspan=3 align=center width=30%><img src="/images/icon_box_blue.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;EXPIRING CONTRACTS</td>
			   <td class="feed_header" colspan=2 align=center width=25%><img src="/images/icon_box_gray.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;POTENTIAL BUYERS</td>
		    </tr>

		   <tr><td height=5></td></tr>

			   <tr>

				  <td class="feed_sub_header" align=center>Procurements</td>
				  <td class="feed_sub_header" align=center>SBIR/STTRs</td>
				  <td class="feed_sub_header" align=center>Grants</td>
				  <td class="feed_sub_header" align=center>Needs & Challenges</td>
				  <td class="feed_sub_header" align=center>90 Days</td>
				  <td class="feed_sub_header" align=center>3 - 12 Months</td>
				  <td class="feed_sub_header" align=center>1-2 Years</td>
				  <td class="feed_sub_header" align=center>Customers</td>
				  <td class="feed_sub_header" align=center>Partners</td>
			   </tr>

			   <tr><td colspan=9><hr></td></tr>

          <cfset session.opp_keyword = #trim(session.forecast_keyword)#>

				<cfquery name="needs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
				 select count(need_id) as total from need
				 where need_public = 1

				 <cfif isdefined("session.hub")>
				  and need_hub_id = #session.hub#
				 <cfelse>
				  and need_hub_id is null
				 </cfif>

				 and contains((*),'"#trim(session.opp_keyword)#"')
				</cfquery>

				<cfquery name="challenges" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
				 select count(challenge_id) as total from challenge
				 where challenge_public = 1

				 <cfif isdefined("session.hub")>
				  and challenge_hub_id = #session.hub#
				 <cfelse>
				  and challenge_hub_id is null
				 </cfif>

				 and contains((*),'"#trim(session.opp_keyword)#"')
				</cfquery>

				<cfset demand_total = evaluate(needs.total + challenges.total)>

	          <cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

			  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(fbo_solicitation_number)) as total from fbo
			   where contains((fbo_opp_name, fbo_desc),'"#session.opp_keyword#"')
			   and fbo_inactive_date_updated > '#fb_date#'
			   and (fbo_type not like 'Award%' or fbo_type not like '%cancel%' or fbo_type is not null)
			  </cfquery>

			  <cfquery name="awards_next90" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)) and
                     contains((award_description),'"#session.opp_keyword#"')
			  </cfquery>

			  <cfquery name="awards_next9months" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
			   where (period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)) and
					 contains((award_description),'"#session.opp_keyword#"')
			  </cfquery>

			  <cfquery name="awards_next2" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(id) as awards, sum(federal_action_obligation) as total from award_data
               where (period_of_performance_current_end_date between '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
					 federal_action_obligation > 0 and
					 (modification_number = '0' or modification_number is null)) and
					 contains((award_description),'"#session.opp_keyword#"')
			  </cfquery>

			  <cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(recipient_duns)) as total from award_data
			   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#') and
                      contains((award_description),'"#session.opp_keyword#"')
			  </cfquery>

			  <cfquery name="buyers" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select count(distinct(awarding_sub_agency_code)) as total from award_data
			   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#') and
                      contains((award_description),'"#session.opp_keyword#"')
			  </cfquery>

			  <!--- SBIR --->

			 <cfquery name="sbir_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select count(id) as total from opp_sbir
			  where contains((*),'"#session.opp_keyword#"') and
			  closedate > #now()#
			 </cfquery>

			 <!--- Grants --->

			 <cfquery name="grants_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select count(opp_grant_id) as total from opp_grant
			  where contains((*),'"#session.opp_keyword#"') and
			  closedate > #now()#
			 </cfquery>

          <cfoutput>

               <tr>
				  <td colspan=9 class="feed_sub_header" style="padding-left: 7px; padding-top: 5px; padding-bottom: 5px; margin-bottom: 15px;">#ucase(session.opp_keyword)#</td>
		       </tr>

			   <tr>
				  <td class="big_number" align=center style="background-color: AEDDAF;"><cfif #fbo.total# is 0>#fbo.total#<cfelse><a href="/exchange/opps/forecast/views/view10.cfm?ky=#session.opp_keyword#&l=2" target="_blank" rel="noopener" rel="noreferrer">#fbo.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: AEDDAF;"><cfif #sbir_total.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view_sbirs.cfm?ky=#session.opp_keyword#&l=2" target="_blank" rel="noopener" rel="noreferrer">#sbir_total.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: AEDDAF;"><cfif #grants_total.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view_grants.cfm?ky=#session.opp_keyword#&l=2" target="_blank" rel="noopener" rel="noreferrer">#grants_total.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: AEDDAF;"><cfif #demand_total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view40.cfm?ky=#session.opp_keyword#&l=2" target="_blank" rel="noopener" rel="noreferrer">#demand_total#</a></cfif></td>
			      <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next90.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=2&l=2" target="_blank" rel="noopener" rel="noreferrer">#awards_next90.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next9months.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=3&l=2" target="_blank" rel="noopener" rel="noreferrer">#awards_next9months.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: B2C2EF;"><cfif #awards_next2.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#session.opp_keyword#&type=5&l=2" target="_blank" rel="noopener" rel="noreferrer">#awards_next2.awards#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: e0e0e0;"><cfif #buyers.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view35.cfm?ky=#session.opp_keyword#&l=2" target="_blank" rel="noopener" rel="noreferrer">#buyers.total#</a></cfif></td>
				  <td class="big_number" align=center style="background-color: e0e0e0;"><cfif #vendors.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view30.cfm?ky=#session.opp_keyword#&l=2" target="_blank" rel="noopener" rel="noreferrer">#vendors.total#</a></cfif></td>
			   </tr>

			   <tr>
				  <td class="feed_sub_header" colspan=4 align=center></td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next90.total,'$999,999,999'))#</td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next9months.total,'$999,999,999'))#</td>
				  <td class="feed_sub_header" align=center>#ltrim(numberformat(awards_next2.total,'$999,999,999'))#</td>
				  <td colspan=2></td>
			   </tr>

			   <tr><td colspan=9><hr></td></tr>
			   <tr><td height=10></td></tr>
               </cfoutput>

         <tr><td height=15></td></tr>
         <tr><td colspan=10 class="link_small_gray">Procurements include sources sought, pre-solicitations and solicitations that have not been awarded and the archive date has not expired.</td></tr>
         <tr><td height=10></td></tr>


</center>