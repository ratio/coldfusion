<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.to")>
 <cfset #session.to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.from# = #dateformat(dateadd("d",-364,now()),'yyyy-mm-dd')#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfif not isdefined("session.forecast_type")>
 <cfset session.forecast_type = 1>
</cfif>

<cfif not isdefined("session.to2")>
 <cfset #session.to2# = #dateformat(dateadd("yyyy",1,now()),'yyyy-mm-dd')#>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/opps/opp_menu.cfm">
          <cfinclude template="/exchange/opps/portfolios.cfm">
      </td><td valign=top>

	  <div class="main_box">
		<cfinclude template="/exchange/opps/forecast/forecast_search.cfm">
	  </div>

      <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <form action="custom_set.cfm" method="post">

		 <tr><td class="feed_header">OPPORTUNITY SNAPSHOTS</td>
		     <td align=right>
		     <span class="feed_sub_header">Custom Search</span>
		     &nbsp;<input type="text" name="forecast_keyword" class="input_text" placeholder="Capability or Service" style="width: 200px;">
		     &nbsp;<input class="button_blue" type="submit" name="button" value="Search"></td>
		 </tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=5></td></tr>
         </form>
        </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td valign=top>

          <cfif not isdefined("session.forecast_type")>
           <cfset session.forecast_type = 1>
          </cfif>

          <cfif session.forecast_type is 1>
            <cfinclude template="forecast_capabilites.cfm">
          </cfif>

		</table>
      </td></tr>
     </table>
  </div>
  </td></tr>
</table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>