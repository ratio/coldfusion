<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<cfset session.forecast_location = 1>
     	  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">

			select id, award_id_piid, action_date, recipient_duns, recipient_name, awarding_sub_agency_name, award_description, type_of_set_aside, product_or_service_code_description,
				   type_of_contract_pricing, federal_action_obligation, base_and_exercised_options_value,base_and_all_options_value, period_of_performance_start_date,
				   period_of_performance_current_end_date,period_of_performance_potential_end_date from award_data

			where ( federal_action_obligation > 0

			       <cfif #session.forecast_dept# is not 0>
			         and awarding_agency_code = '#session.forecast_dept#'
			       </cfif>

			     and
			     (
			      (period_of_performance_current_end_date >= '#session.forecast_from#') and (period_of_performance_current_end_date <= '#session.forecast_to#') or
			      (period_of_performance_potential_end_date >= '#session.forecast_from#') and (period_of_performance_potential_end_date <= '#session.forecast_to#')
			      )

			 <cfif #session.forecast_setaside# is not "" and #session.forecast_setaside# is not 0>
			  and type_of_set_aside_code = '#session.forecast_setaside#'
			 </cfif>

             <cfif #session.forecast_duns# is not "">
              and recipient_duns = '#session.forecast_duns#'
             </cfif>

			 <cfif #session.forecast_naics# is not "">
			  and naics_code = '#session.forecast_naics#'
			 </cfif>

			 )

		   <cfif isdefined("session.forecast_keyword") and #session.forecast_keyword# is not "">
		     and contains((award_description, awarding_office_name, funding_office_name, solicitation_identifier, award_id_piid, parent_award_id),'"#session.forecast_keyword#"')
		   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by action_date ASC
		    <cfelseif #sv# is 10>
             order by action_date DESC
		    <cfelseif #sv# is 2>
             order by recipient_name ASC
		    <cfelseif #sv# is 20>
             order by recipient_name DESC
		    <cfelseif #sv# is 3>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 4>
             order by type_of_set_aside ASC
		    <cfelseif #sv# is 40>
             order by type_of_set_aside DESC
		    <cfelseif #sv# is 5>
             order by product_or_service_code_description ASC
		    <cfelseif #sv# is 50>
             order by product_or_service_code_description DESC
		    <cfelseif #sv# is 6>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 60>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 7>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 70>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 8>
             order by base_and_exercised_options_value DESC
		    <cfelseif #sv# is 80>
             order by base_and_exercised_options_value ASC
		    <cfelseif #sv# is 9>
             order by base_and_all_options_value DESC
		    <cfelseif #sv# is 90>
             order by base_and_all_options_value ASC
		    <cfelseif #sv# is 10>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 100>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 11>
             order by period_of_performance_current_end_date DESC
		    <cfelseif #sv# is 110>
             order by period_of_performance_current_end_date ASC
		    <cfelseif #sv# is 12>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 120>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 13>
             order by award_id_piid ASC
		    <cfelseif #sv# is 130>
             order by award_id_piid DESC
		    </cfif>

		   <cfelse>
			order by award_id_piid ASC

		   </cfif>

		  </cfquery>

   <cfif isdefined("export")>
	 <cfinclude template="/exchange/include/export_to_excel.cfm">
	</cfif>

<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/opps/opp_menu.cfm">
      </td><td valign=top>

	  <div class="main_box">
		<cfinclude template="/exchange/opps/forecast/forecast_search.cfm">
	  </div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <cfoutput>
				 <tr><td class="feed_header">SEARCH RESULTS (#ltrim(numberformat(agencies.recordcount,'999,999'))#)</td>
					 <td class="feed_option" align=right>

				 <a href="/exchange/opps/forecast/results_expanded.cfm"><img src="/images/icon_expand.png" width=20 alt="Expanded View" title="Expanded View"></a>
				 &nbsp;&nbsp;&nbsp;&nbsp;

					 <a href="/exchange/opps/forecast/results.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel"></a>
					 </td></tr>
			  </cfoutput>
            <tr><td colspan=2><hr></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td>&nbsp;</td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_option">No potential results found.  Please try to refine your search criteria.</td></tr>
          <cfelse>

          <tr height=50>

              <td class="text_xsmall" width=75><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATE</b></a></td>
              <td class="text_xsmall" width=100><a href="results.cfm?<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>AWARD #</b></a></td>
              <td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>INCUMBENT</b></a></td>
              <td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AGENCY</b></a></td>
              <td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>SET ASIDE</b></a></td>
              <td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>PRICING</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>BASE</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>OPTIONS</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>TOTAL</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>POP START</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>POP END</b></a></td>
              <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>CONTRACT END</b></a></td>
          </tr>

          <cfset counter = 0>

          <cfoutput query="agencies">

           <cfif counter is 0>
            <tr bgcolor="ffffff" height=30>
           <cfelse>
            <tr bgcolor="e0e0e0" height=30>
           </cfif>

              <td class="text_xsmall" valign=middle>#dateformat(action_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=middle><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
              <td class="text_xsmall" valign=middle><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#recipient_name#</b></a></td>
              <td class="text_xsmall" valign=middle>#awarding_sub_agency_name#</td>
              <td class="text_xsmall" valign=middle>#type_of_set_aside#</td>
              <td class="text_xsmall" valign=middle>#product_or_service_code_description#</td>
              <td class="text_xsmall" valign=middle align=right>

              <cfif #type_of_contract_pricing# is "Firm Fixed Price">
               FFP
              <cfelseif #type_of_contract_pricing# is "Time & Materials" or #type_of_contract_pricing# is "Time and Materials">
               T&M
              <cfelseif #type_of_contract_pricing# is "Cost Plus Fixed Fee">
               CPFF
              <cfelseif #type_of_contract_pricing# is "Labor Hours">
               LH
              <cfelseif #type_of_contract_pricing# is "Fixed Price Incentive">
               FPI
              <cfelse>
              #type_of_contract_pricing#
              </cfif></td>
              <td class="text_xsmall" valign=middle width=75 align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=middle width=75 align=right>#numberformat(base_and_exercised_options_value,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=middle width=75 align=right>#numberformat(base_and_all_options_value,'$999,999,999,999')#</td>
              <td class="text_xsmall" valign=middle width=75 align=right>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=middle width=75 align=right>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
              <td class="text_xsmall" valign=middle width=100 align=right>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
           </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>

          </table>

	  </div>

	 </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>