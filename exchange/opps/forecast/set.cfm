<cfset session.forecast_dept = #forecast_dept#>
<cfset session.forecast_keyword = #forecast_keyword#>
<cfset session.forecast_duns = #forecast_duns#>
<cfset session.forecast_naics = #forecast_naics#>
<cfset session.forecast_from = #forecast_from#>
<cfset session.forecast_agency = #forecast_agency#>
<cfset session.forecast_setaside = #forecast_setaside#>
<cfset session.forecast_state = #forecast_state#>
<cfset session.forecast_psc = #forecast_psc#>
<cfset session.forecast_to = #forecast_to#>

<cfif not isdefined("session.forecast_location")>
 <cfset session.forecast_location = 1>
</cfif>

<cfif session.forecast_location is 1>
 <cflocation URL="/exchange/opps/forecast/results.cfm" addtoken="no">
<cfelse>
 <cflocation URL="/exchange/opps/forecast/results_expanded.cfm" addtoken="no">
</cfif>