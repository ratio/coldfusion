<!--- Portfolios --->

<div class="right_box">

	<center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><b>SBIR/STTR Sources</b></td>
		      <td align=right valign=top></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=10></td></tr>

           <tr>
              <td align=center><a href="https://www.sbir.gov/agencies/small-business-administration" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/sba.png" width=55 border=0 alt="Small Business Administration" title="Small Business Administration"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/department-of-agriculture" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/usda.png" width=55 border=0 alt="Department of Agriculture" title="Department of Agriculture"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/department-of-commerce" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/commerce.png" width=55 border=0 alt="Department of Commerce" title="Department of Commerce"></a></td>
           </tr>

           <tr><td colspan=3 height=20></td></tr>

           <tr>
              <td align=center><a href="https://www.sbir.gov/agencies/department-of-defense" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/dod.png" width=55 border=0 alt="Department of Defense" title="Department of Defense"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/department-of-energy" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/energy.png" width=55 border=0 alt="Department of Energy" title="Department of Energy"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/department-of-education" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/education.png" width=55 border=0 alt="Department of Education" title="Department of Education"></a></td>
           </tr>

           <tr><td colspan=3 height=20></td></tr>

           <tr>
              <td align=center><a href="https://sbir.nih.gov/" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/hhs.png" width=55 border=0 alt="Department of Health & Human Services" title="Department of Health & Human Services"></a></td>
              <td align=center><a href="https://sbir2.st.dhs.gov/portal/SBIR/" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/dhs.png" width=55 border=0 alt="Department of Homeland Security" title="Department of Homeland Security"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/department-of-transportation" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/dot.png" width=55 border=0 alt="Department of Transportation" title="Departmment of Transporation"></a></td>
           </tr>

           <tr><td colspan=3 height=20></td></tr>

           <tr>
              <td align=center><a href="https://www.sbir.gov/agencies/environmental-protection-agency" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/epa.png" width=55 border=0 alt="Environmental Protection Agency" title="Environmental Protection Agency"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/national-aeronautics-and-space-administration" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/nasa.png" width=55 border=0 alt="National Aeronautics and Space Administration " title="National Aeronautics and Space Administration"></a></td>
              <td align=center><a href="https://www.sbir.gov/agencies/national-science-foundation" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/logos/nsf.png" width=55 border=0 alt="National Science Foundation" title="National Science Foundation"></a></td>
           </tr>


          </table>

	</center>

</div>



