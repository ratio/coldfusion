<cfinclude template="/exchange/security/check.cfm">

  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join naics on naics_code = fbo_naics_code
   left join class_code on class_code_code = fbo_class_code
   where fbo_id = #fbo_id#
  </cfquery>

  <cfif isdefined("session.hub")>
	  <cfquery name="hub_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select * from hub
	   where hub_id = #session.hub#
	  </cfquery>
  </cfif>

  <cfquery name="user" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from usr
   where usr_id = #session.usr_id#
  </cfquery>

<cfmail from="#user.usr_first_name# #user.usr_last_name# <noreply@ratio.exchange>"
		  to="#email#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="#subject#">
<html>
<head>
<title>Opportunity</title>
</head><div class="center">
<body class="body">
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">

 <cfoutput>

   <tr><td class="feed_option"><b>#user.usr_first_name# #user.usr_last_name# has shared an opportunity with you from <cfif isdefined("session.hub")>#hub_info.hub_name#<cfelse>the EXCHANGE</cfif>.</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td class="feed_option">

        #message#

       </td></tr>

   <tr><td>&nbsp;</td></tr>

   <cfif isdefined("session.hub")>
     <tr><td class="feed_option">To find out more information about #hub_info.hub_name#, please <a href="#hub_info.hub_logout_page#">click here</a>.</td></tr>
   <cfelse>
     <tr><td class="feed_option">To find out more information about the EXCHANGE, please <a href="https://www.ratio.exchange">click here</a>.</td></tr>
   </cfif>

   <tr><td height=10></td></tr>

   <tr><td><hr></td></tr>

   <tr><td>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header">#fbo.fbo_opp_name#</td></tr>
			 <tr><td height=10></td></tr>
            </table>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td>

			        <table cellspacing=0 cellpadding=0 border=0 width=100%>
			         <tr>
			             <td class="feed_option"><b>Solicitation ##</td>
			             <td class="feed_option">#fbo.fbo_solicitation_number#</td>

			             <td class="feed_option"><b>Date Posted</td>
			             <td class="feed_option">#dateformat(fbo.fbo_pub_date,'mm/dd/yyyy')#</td>
			         </tr>

			         <tr>
			             <td class="feed_option"><b>Notice Type</td>
			             <td class="feed_option">#fbo.fbo_type#</td>

			             <td class="feed_option"><b>Response Date</td>
			             <td class="feed_option">
			             <cfif isdate(fbo.fbo_response_date_original)>
			              #dateformat(fbo.fbo_response_date_original,'mm/dd/yyyy')# at #timeformat(fbo.fbo_response_date_original)#
			             <cfelse>
			             Unknown or N/A
			             </cfif></td>

			         </tr>

			         <tr>
			             <td class="feed_option"><b>Department</td>
			             <td class="feed_option">#fbo.fbo_dept#</td>


			             <td class="feed_option"><b>Small Business</td>
			             <td class="feed_option"><cfif #fbo.fbo_setaside_original# is "">No<cfelse>#fbo.fbo_setaside_original#</cfif></td>


			         </tr>

			         <tr>
			             <td class="feed_option"><b>Agency</td>
			             <td class="feed_option">#fbo.fbo_agency#</td>

			             <td class="feed_option" valign=top><b>NAICS</td>
			             <td class="feed_option" valign=top>#fbo.fbo_naics_code# - #fbo.naics_code_description#</td>


			         </tr>

			         <tr>
			             <td class="feed_option"><b>Office</td>
			             <td class="feed_option">#fbo.fbo_office#</td>

			             <td class="feed_option"><b>Product or Service</td>
			             <td class="feed_option">#fbo.fbo_class_code# - #fbo.class_code_name#</td>

			         </tr>

			         <tr>
			             <td class="feed_option"><b>Contracting Office</td>
			             <td class="feed_option">#fbo.fbo_contracting_office#</td>

			             <td class="feed_option"><b>Place of Performance</td>
			             <td class="feed_option">#fbo.fbo_pop_city# #fbo.fbo_pop_state#, #fbo.fbo_pop_zip#</td>

			         </tr>

			        <tr><td class="feed_option"><b>Opportunity URL</b></td>
			            <td colspan=3 class="feed_option"><a href="#fbo.fbo_url#" target="_blank" rel="noopener" rel="noreferrer">#fbo.fbo_url#</a></td></tr>


			        </table>

			     </td></tr>
			 <tr><td height=10></td></tr>

			 <cfif fbo.fbo_type is "Award">

				 <tr><td><hr></td></tr>
				 <tr><td class="feed_header"><b>Award Information</b></td></tr>
				 <tr><td height=10></td></tr>
				 <tr><td class="feed_option"><b>Award Date: </b>#dateformat(fbo.fbo_contract_award_date,'mm/dd/yyy')# - <a href="/exchange/include/company_profile.cfm?id=0&duns=#fbo.fbo_contract_award_duns#" target="_blank" rel="noopener" rel="noreferrer">Company Look-up</a></td></tr>
				 <tr><td class="feed_option"><b>Awardee Name: </b>#fbo.fbo_contract_award_name#</td></tr>
				 <tr><td class="feed_option"><b>Awardee DUNS: </b>#fbo.fbo_contract_award_duns#</td></tr>
				 <tr><td class="feed_option"><b>Award Amount: </b>#fbo.fbo_contract_award_amount#</td></tr>

			 </cfif>

			 <tr><td><hr></td></tr>
			 <tr><td class="feed_header"><b>Opportunity Description</b></td></tr>
			 <tr><td height=10></td></tr>
			 <tr><td class="feed_option">
             #replace(fbo.fbo_desc,"#chr(46)##chr(32)#",".<br><br>","all")#

			</td></tr>

			</table>

   </td></tr>

 </cfoutput>

</table>

</body>
</html>
</cfmail>

<cflocation URL="share.cfm?fbo_id=#fbo_id#&u=1" addtoken="no">