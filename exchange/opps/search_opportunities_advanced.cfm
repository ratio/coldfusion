<cfset from = dateadd('d',-30,now())>

 <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_agency) from fbo
  where fbo_agency is not null or fbo_agency <> ' '
  order by fbo_agency
 </cfquery>

 <cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_notice_type) from fbo
  order by fbo_notice_type
 </cfquery>

 <cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select distinct(fbo_setaside_original) from fbo
  where (fbo_setaside_original <> '' and fbo_setaside_original <> 'N/A')
  order by fbo_setaside_original
 </cfquery>

 <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
 </cfquery>

 <cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header" height=24>Search Contracts</td>
	      <td class="feed_sub_header" align=right></td></tr>
	  <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Contracts are opportunities for you to win work with the Federal Government or team with companies or organizations to win work.</td></tr>
	  <tr><td colspan=2><hr></td></tr>
	 </table>
 </cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <form action="/exchange/opps/set2.cfm" method="post">

          <tr>
            <cfoutput>
            <td class="feed_option"><b>Keyword</b></td>
            <td class="feed_option"><input type="text" name="opp_keyword" class="input_text" <cfif isdefined("session.original_keyword")>value="#session.original_keyword#"</cfif> style="width: 200px;" placeholder="i.e., machine learning"></td>
            </cfoutput>
             <td class="feed_option"><b>SB Set-Aside</b></td>
             <td class="feed_option">
                                    <select name="opp_setaside" class="input_select" style="width: 150px;">
				                     <option value=0>NO PREFERENCE
				                     <cfoutput query="setaside">
				                     <option value="#fbo_setaside_original#" <cfif isdefined("session.opp_setaside") and #session.opp_setaside# is #fbo_setaside_original#>selected</cfif>>#ucase(fbo_setaside_original)#
				                     </cfoutput>
                  </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">These are programs the Federal Government uses to "set aside" funding for socio-economic classes of business.</span>
			 </div>

              <cfoutput>

            </td>
  		    <td class="feed_option"><b>NAICS *</b></td>
					  <td class="feed_option">
					  <input type="text" name="opp_naics" class="input_text" style="width: 100px;" maxlength="299" <cfif isdefined("session.opp_naics")>value="#session.opp_naics#"</cfif>>
					  &nbsp;<img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup" style="cursor: pointer;" onclick="window.open('naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">
					  </td>

					  <td class="feed_option"><b>From</b></td>
					  <td class="feed_option"><input type="date" class="input_date" name="opp_posted_from" style="width: 160px;" <cfif isdefined("session.opp_posted_from")>value="#dateformat(session.opp_posted_from,'yyyy-mm-dd')#"<cfelse>value="#dateformat(from,'yyyy-mm-dd')#"</cfif>></td>

				  <td class="feed_option"><b>Status</b></td>
				  <td class="feed_option">

						<select name="opp_status" class="input_select" style="width:100px">
							<option value=1 <cfif isdefined("session.opp_status") and #session.opp_status# is 1>selected</cfif>>ALL
							<option value=3 <cfif isdefined("session.opp_status") and #session.opp_status# is 3>selected</cfif>>ARCHIVED
						</select>

			      </td>


				</tr>

            </cfoutput>

          <tr>

            <td class="feed_option"><b>Source</b></td>

            <td class="feed_option">

                  <select name="opp_dept" class="input_select" style="width: 200px;">
                   <option value=0>ALL
                   <cfoutput query="dept">
                   <option value="#fbo_agency#" <cfif isdefined("session.opp_dept") and #session.opp_dept# is '#fbo_agency#'>selected</cfif>><cfif #fbo_agency# is "">NOT SPECIFIED<cfelse>#ucase(fbo_agency)#</cfif>
                   </cfoutput>
                  </select>

              </td>

              <td class="feed_option"><b>Type</b></td>
              <td class="feed_option">
                  <select name="opp_type" class="input_select" style="width: 150px;">
                   <option value=0>ALL
                   <cfoutput query="type">
                    <option value="#fbo_notice_type#" <cfif isdefined("session.opp_type") and #session.opp_type# is #fbo_notice_type#>selected</cfif>>#ucase(fbo_notice_type)#
                   </cfoutput>
                  </select>
              </td>

			  <cfoutput>


					  <td class="feed_option"><b>PSC *</b></td>
					  <td class="feed_option"><input type="text" name="opp_psc" class="input_text" style="width: 100px;" maxlength="299" <cfif isdefined("session.opp_psc")>value="#session.opp_psc#"</cfif>>
					  &nbsp;<img src="/images/icon_search.png" height=18 alt="PSC Lookup" title="PSC Lookup" style="cursor: pointer;" onclick="window.open('psc_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;"></a>
				  </td>

					  <td class="feed_option"><b>To</b></td>
					  <td class="feed_option"><input type="date" name="opp_posted_to" class="input_date" style="width: 160px;" <cfif isdefined("session.opp_posted_to")>value="#dateformat(session.opp_posted_to,'yyyy-mm-dd')#"<cfelse>value="#dateformat(now(),'yyyy-mm-dd')#"</cfif>></td>
				  </td>

				  <td colspan=2><input class="button_blue" type="submit" name="button" value="Search">
				      <cfif isdefined("clear")>
				       &nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Clear">
				      </cfif>

				  </td>

			  </cfoutput>

            </tr>
            <tr><td height=10></td></tr>
            <tr><td class="link_small_gray" colspan=10>* to search for multiple NAICS or PSC codes, seperate values with commas (i.e., 43412,33928, etc.)</td></tr>

          </form>

         </table>

