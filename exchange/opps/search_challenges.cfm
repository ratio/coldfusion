<cfif not isdefined("session.challenge_type")>
 <cfset session.challenge_type = 0>
</cfif>

<cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <form action="set_challenge.cfm" method="post">

	      	  <tr><td class="feed_header" colspan=2>Search Challenges</td></tr>
	          <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Challenges are opportunities to help customers solve problems in a competitive environment.</td></tr>
	      	  <tr><td colspan=2><hr></td></tr>

	  <tr><td height=5></td></tr>
	  <tr>
	      <td class="feed_sub_header" style="font-weight: normal;">
            <cfoutput>

		    <input type="text" name="challenge_keyword" <cfif isdefined("session.challenge_keyword")>value='#replaceNoCase(session.challenge_keyword,'"','',"all")#'</cfif> class="input_text" style="width: 350px;" maxlength="100" required placeholder="Search for a Challenge...">

              &nbsp;
			  <select name="challenge_type" class="input_select">
			   <option value=0 <cfif session.challenge_type is 0>selected</cfif>>All
			   <option value=1 <cfif session.challenge_type is 1>selected</cfif>>Exchange Challenges
			   <option value=2 <cfif session.challenge_type is 2>selected</cfif>>Other Challenges
			  <select>

			  <b>Status</b>&nbsp;

			  <select name="challenge_status" class="input_select">
			   <option value=0 <cfif isdefined("session.challenge_status") and session.challenge_status is 0>selected</cfif>>All
			   <option value=1 <cfif isdefined("session.challenge_status") and session.challenge_status is 1>selected</cfif>>Open
			   <option value=2 <cfif isdefined("session.challenge_status") and session.challenge_status is 2>selected</cfif>>Closed
			  <select>

		      <input class="button_blue" type="submit" name="button" value="Search">
              &nbsp;
		      <cfif #findnocase('challenges_results.cfm',cgi.script_name,1)#><input class="button_blue" type="submit" name="button" value="Clear"></cfif>

		      </td>
  	        </cfoutput>
	      </td>
	      </tr>
     </form>
	 </table>

 </cfoutput>


