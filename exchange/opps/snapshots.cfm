<style>
.scroll {
    height: 250px;
    background-color: ffffff;
    overflow:auto;
}
.opp_number {
    font-family: calibri, arial;
    font-size: 18px;
    font-weight: bold;
    padding-top: 10px;
    padding-bottom: 10px;
    color: 000000;
    border: 5px solid #ffffff;
    background-color: e0e0e0;
}
.opp_number a:link {
    color: #000000;
    font-weight: bold;
    text-decoration: none;
}
.opp_number a:visited {
    font-weight: bold;
    color: #000000;
}
.opp_number a:hover {
    color: #000000;
    font-weight: bold;
    text-decoration: underl	ne;
}
</style>

<cfquery name="snapshot" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_hub_id = #session.hub# and
        snapshot_usr_id = #session.usr_id#
  order by snapshot_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td colspan=2><hr></td></tr>
 <tr><td class="feed_header">Opportunty Snapshots</td>
	 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="snapshot_add.cfm">Add Snapshot</a></td>
	 </tr>
 <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=2>
 Snapshots provide a summary list of active opportunities that have been matched to your interests and snapshot profiles.
 </td></tr>
</table>

<cfset option_number = 0>

  <cfif snapshot.recordcount is 0>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any Opportunity Snapshots.  <a href="/exchange/profile/edit.cfm"><b>Click here</b></a> to add them to your profile.</td></tr>
  </table>

  <cfelse>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	<tr>
	   <td width=9%>&nbsp;</td>
	   <td width=45% class="feed_sub_header" colspan=5 align=center><img src="/images/icon_box_green.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;Active Now</td>
	   <td width=27% class="feed_sub_header" colspan=3 align=center><img src="/images/icon_box_blue.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;Expiring Contracts</td>
	   <td width=18% class="feed_sub_header" colspan=2 align=center><img src="/images/icon_box_gray.png" width=12 style="margin-bottom: 4px;">&nbsp;&nbsp;Buyers</td>
	</tr>

	   <tr>
		  <td width=9% style="padding-left: 7px;">&nbsp;</td>
		  <td width=9% class="feed_sub_header" align=center>Procurements</td>
		  <td width=9% class="feed_sub_header" align=center>Grants</td>
		  <td width=9% class="feed_sub_header" align=center>SBIR/STTRs</td>
		  <td width=9% class="feed_sub_header" align=center>Needs</td>
		  <td width=9% class="feed_sub_header" align=center>Challenges</td>
		  <td width=9% class="feed_sub_header" align=center>90 Days</td>
		  <td width=9% class="feed_sub_header" align=center>3-12 Months</td>
		  <td width=9% class="feed_sub_header" align=center>1-2 Years</td>
		  <td width=9% class="feed_sub_header" align=center>Customers</td>
		  <td width=9% class="feed_sub_header" align=center>Partners</td>

	   </tr>

	   <tr><td></td><td colspan=10><hr></td></tr>
	   <tr><td height=10></td></tr>

  </table>

  <div class="scroll">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <cfset counter = 1>
  <cfloop query="snapshot">

		<cfquery name="needs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
		 select count(need_id) as total from need
		 where need_public = 1

		 <cfif isdefined("session.hub")>
		  and need_hub_id = #session.hub#
		 <cfelse>
		  and need_hub_id is null
		 </cfif>

		 and contains((*),'#trim(snapshot.snapshot_keyword)#')

		</cfquery>

		<cfquery name="challenges" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
		 select count(challenge_id) as total from challenge
		 where challenge_public = 1
		 and challenge_end >= '#dateformat(now(),'mm/dd/yyyy')#'

		 <cfif isdefined("session.hub")>
		  and challenge_hub_id = #session.hub#
		 <cfelse>
		  and challenge_hub_id is null
		 </cfif>

		 and contains((*),'#trim(snapshot.snapshot_keyword)#')

		</cfquery>

		<cfset demand_total = evaluate(needs.total + challenges.total)>

	  <cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

	  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(fbo_solicitation_number)) as total from fbo
	   where contains((fbo_opp_name, fbo_desc),'#trim(snapshot.snapshot_keyword)#')
	   and fbo_inactive_date_original > '#fb_date#'
	   and (fbo_type not like '%Award%')
	  </cfquery>

	  <cfquery name="awards_next90" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data
	   where period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#'
			 and contains((award_description),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="awards_next9months" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data
	   where period_of_performance_current_end_date between '#dateformat(evaluate(now()+90),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and
			 contains((award_description),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="awards_next2" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(award_id_piid)) as awards, sum(federal_action_obligation) as total from award_data
	   where period_of_performance_current_end_date between '#dateformat(evaluate(now()+365),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#' and
			 contains((award_description),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(recipient_duns)) as total from award_data
	   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#') and
			 contains((award_description),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <cfquery name="buyers" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select count(distinct(awarding_sub_agency_code)) as total from award_data
	   where (period_of_performance_current_end_date between '#dateformat(now(),'mm/dd/yyyy')#' and '#dateformat(evaluate(now()+730),'mm/dd/yyyy')#') and
			 contains((award_description),'#trim(snapshot.snapshot_keyword)#')
	  </cfquery>

	  <!--- SBIR --->

	 <cfquery name="sbir_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  select count(id) as total from opp_sbir
	  where contains((*),'#trim(snapshot.snapshot_keyword)#') and
	  closedate > #now()#
	 </cfquery>

	 <!--- Grants --->

	 <cfquery name="grants_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  select count(opp_grant_id) as total from opp_grant
	  where contains((*),'#trim(snapshot.snapshot_keyword)#') and
	  closedate > #now()#
	 </cfquery>

  <cfoutput>

	   <tr>
		  <td width=9% class="feed_sub_header" style="padding-left: 7px; padding-top: 5px; padding-bottom: 5px; margin-bottom: 15px;"><a href="snapshot_edit.cfm?snapshot_id=#snapshot.snapshot_id#" alt="#replaceNoCase(snapshot.snapshot_keyword,'"','',"all")#" title="#replaceNoCase(snapshot.snapshot_keyword,'"','',"all")#">#ucase(snapshot.snapshot_name)#</a></td>
		  <td width=9% class="opp_number" align=center style="background-color: AEDDAF;"><cfif #fbo.total# is 0>#fbo.total#<cfelse><a href="/exchange/opps/forecast/views/view10.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#fbo.total#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: AEDDAF;"><cfif #grants_total.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view_grants.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#grants_total.total#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: AEDDAF;"><cfif #sbir_total.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view_sbirs.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#sbir_total.total#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: AEDDAF;"><cfif #needs.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view40.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#needs.total#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: AEDDAF;"><cfif #challenges.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view40.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#challenges.total#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: B2C2EF;"><cfif #awards_next90.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#snapshot.snapshot_id#&type=2&l=2" target="_blank" rel="noopener" rel="noreferrer">#awards_next90.awards#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: B2C2EF;"><cfif #awards_next9months.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#snapshot.snapshot_id#&type=3&l=2" target="_blank" rel="noopener" rel="noreferrer">#awards_next9months.awards#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: B2C2EF;"><cfif #awards_next2.awards# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view20.cfm?ky=#snapshot.snapshot_id#&type=5&l=2" target="_blank" rel="noopener" rel="noreferrer">#awards_next2.awards#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: e0e0e0;"><cfif #buyers.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view35.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#buyers.total#</a></cfif></td>
		  <td width=9% class="opp_number" align=center style="background-color: e0e0e0;"><cfif #vendors.total# is 0>0<cfelse><a href="/exchange/opps/forecast/views/view30.cfm?ky=#snapshot.snapshot_id#&l=2" target="_blank" rel="noopener" rel="noreferrer">#vendors.total#</a></cfif></td>
	   </tr>

	   <tr>
		  <td class="feed_sub_header" colspan=6 align=center></td>
		  <td class="feed_option" style="font-weight: bold;" align=center>#ltrim(numberformat(awards_next90.total,'$999,999,999'))#</td>
		  <td class="feed_option" style="font-weight: bold;" align=center>#ltrim(numberformat(awards_next9months.total,'$999,999,999'))#</td>
		  <td class="feed_option" style="font-weight: bold;" align=center>#ltrim(numberformat(awards_next2.total,'$999,999,999'))#</td>
		  <td colspan=2></td>
	   </tr>

	   <cfif counter LT snapshot.recordcount>
		<tr><td></td><td colspan=10><hr></td></tr>
	   </cfif>
	   <cfset counter = counter + 1>

	   </cfoutput>

	 </cfloop>

	</table>

	</div>

 </cfif>