<cfinclude template="/exchange/security/check.cfm">

<cfif f is "y">

  <cfquery name="follow" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   insert follow(follow_by_usr_id, follow_need_id, follow_date)
   values(#session.usr_id#,#need_id#,#now()#)
  </cfquery>
  
  <cflocation URL="need_detail.cfm?u=1&need_id=#need_id#" addtoken="no">

<cfelse>

  <cfquery name="unfollow" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   delete follow
   where follow_by_usr_id = #session.usr_id# and
         follow_need_id = #need_id#
  </cfquery>

  <cflocation URL="need_detail.cfm?u=2&need_id=#need_id#" addtoken="no">

</cfif>

