<!--- Hub Profile --->

<cfquery name="follow" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from follow
 left join challenge on challenge_id = follow_challenge_id
 where follow_by_usr_id = #session.usr_id# and
       follow_challenge_id is not null
</cfquery>

<div class="left_box" style="margin-top: 25px;">

  <center>
  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header" colspan=2><b>INTERESTED IN</a></b></td></tr>
  <tr><td colspan=2><hr></td></tr>

  <cfset count = 1>

  <cfif #follow.recordcount# is 0>

   <tr><td class="feed_option">You are not interested in any Challenges.</td></tr>

  <cfelse>

  <cfoutput query="follow">

   <tr><td width=60>

		<cfif #follow.challenge_image# is "">
				<a href="/exchange/opps/challenge_detail.cfm?challenge_id=#follow.challenge_id#"><img src="/images/stock_challenge.png" width=40 border=0></a>
		<cfelse>
				<a href="/exchange/opps/challenge_detail.cfm?challenge_id=#follow.challenge_id#"><img src="#media_virtual#/#follow.challenge_image#" width=40 border=0></a>
		</cfif>

   </td>
   <td class="link_med_blue"><a href="/exchange/opps/challenge_detail.cfm?challenge_id=#follow.challenge_id#">#follow.challenge_name#</a></td></tr>

   <cfif count is not #follow.recordcount#>
   <tr><td colspan=2><hr></td></tr>
   </cfif>
   <cfset count = count + 1>

  </cfoutput>

  </cfif>

  </table>
  </center>

</div>