<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_agency) from fbo
 order by fbo_agency
</cfquery>

<cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_notice_type) from fbo
 order by fbo_notice_type
</cfquery>

<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(fbo_setaside) from fbo
 order by fbo_setaside
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/opps/opp_menu.cfm">
	      <cfinclude template="/exchange/opps/saved_searches.cfm">
      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">CREATE SAVED SEARCH</td><td class="feed_option" align=right><a href="/exchange/opps/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
         <tr><td class="feed_option">Create your own opportunity searches.   To create your saved search, fill out the below fields and click Create.</td></tr>
         <tr><td height=5></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=5></td></tr>

        </table>

         <form action="edit_db.cfm" method="post">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_option"><b>Search Name</b></td>
             <td class="feed_option"><input type="text" class="input_text"  style="width: 400px;" name="opp_search_name" style="width:300px;" required></td>
             </td></tr>

         <tr><td class="feed_option" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="opp_search_desc" class="input_textarea" cols=70 rows=5></textarea></td>
             </td></tr>

         <tr><td class="feed_option"><b>Type</b></td>
             <td class="feed_option">
                <select name="opp_search_type" class="input_select" style="width: 400px;">
                 <option value=0>All
                   <cfoutput query="type">
                     <option value="#fbo_notice_type#">#fbo_notice_type#
                   </cfoutput>
                 </select>
              </td></tr>

         <tr><td class="feed_option"><b>Agency</b></td>
             <td class="feed_option">
                <select name="opp_search_dept" class="input_select" style="width: 400px;">
                 <option value=0>All
                   <cfoutput query="agency">
                     <option value="#fbo_agency#">#fbo_agency#
                   </cfoutput>
                 </select>
              </td></tr>

          <tr><td class="feed_option"><b>From</b></td>
              <td class="feed_option"><input type="date" name="opp_search_posted_from" class="input_text" required></td>
              </td></tr>

          <tr><td class="feed_option"><b>To</b></td>
              <td class="feed_option"><input type="date" name="opp_search_posted_to" class="input_text" required></td>
              </td></tr>

          <tr><td class="feed_option"><b>NAICS Code(s) *</b></td>
              <td class="feed_option"><input type="text" name="opp_search_naics" class="input_text" style="width:400px;" maxlength="299"></td>
              </td></tr>

          <tr><td class="feed_option"><b>Product or Service *</b></td>
              <td class="feed_option"><input type="text" name="opp_search_psc" class="input_text" style="width: 400px;" maxlength="299"></td>
              </td></tr>

          <tr><td class="feed_option"><b>Small Business</b></td>
              <td class="feed_option">
				<select name="opp_search_setaside" class="input_select" style="width:400px;">
				 <option value=0>No Preference
				 <cfoutput query="setaside">
				 <option value="#fbo_setaside#">#fbo_setaside#
				 </cfoutput>
                </select></td></tr>

           <tr><td class="feed_option"><b>Keyword</b></td>
               <td class="feed_option"><input type="text" name="opp_search_keyword" class="input_text" style="width:400px;">
               </td></tr>

          <tr><td height=5></td></tr>
          <tr><td></td><td class="text_xsmall">* - Entering nothing will return all.  To search multiple values, seperate codes with a commma and no spaces.  For a complete code list, click here - <a href="https://www.census.gov/cgi-bin/sssd/naics/naicsrch?chart=2017" target="_blank" rel="noopener" rel="noreferrer">NAICS Code list</a>, or <a href="https://www.PROCUREMENTS/index?s=getstart&mode=list&tab=list&tabmode=list&static=faqs##q4" target="_blank" rel="noopener" rel="noreferrer">Product or Service Code list</a>.</td></tr>
          <tr><td height=5></td></tr>

          <tr><td height=10></td></tr>
          <tr><td></td><td>

          <input class="button_blue" type="submit" name="button" value="Create">

          </td></tr>

        </table>

        </form>

	  </div>

	</td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>