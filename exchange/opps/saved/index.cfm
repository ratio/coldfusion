<cfinclude template="/exchange/security/check.cfm">

<cfquery name="priorities" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from feed
 left join feed_interest on feed.feed_id = feed_interest.feed_interest_feed_id
 where feed_interest.feed_interest_decision = 1 and
       feed_interest.feed_interest_usr_id = #session.usr_id#
       order by feed_updated DESC
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Saved Opportunities (<cfoutput>#priorities.recordcount#</cfoutput>)</td>
           <td class="feed_option" align=right><a href="/exchange/opps/">Return</a></td></tr>
           <tr><td>&nbsp;<td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>


           <cfif isdefined("u")>
            <cfif u is 1>
             <tr><td><font size=2 color="red"><b>The opportunity has been removed.</b></font></td></tr>
            <cfelseif u is 2>
             <tr><td><font size=2 color="green"><b>The opportunity has been shared.</b></font></td></tr>
            </cfif>
            <tr><td>&nbsp;<td></tr>
           </cfif>

           <cfif #priorities.recordcount# is 0>
	           <tr><td class="feed_option">No opportunities have been saved to your portfolio.</td></tr>
           </cfif>

          </table>

           <cfoutput query="priorities">

           <div class="box_row">

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_title"><b><a href="opp_detail.cfm?feed_id=#feed_id#">#feed_title#</a></b></td>
               <td align=right valign=top width=100>

               <a href="/exchange/feed/share.cfm?feed_id=#feed_id#&l=1"><img src="/images/icon_share.png" border=0 width=15 align=middle title="Share with others" alt="Share with others"></a>&nbsp;&nbsp;
               <a href="delete.cfm?feed_id=#feed_id#" onclick="return confirm('Are you sure you want to remove this from your portfolio?');"><img src="/images/icon_trash.png" border=0 width=22 align=middle title="Remove from Portfolio" alt="Remove from Portfolio"></a>

               </td></tr>
           <tr><td class="feed_option"><b>#feed_organization#</b></td></tr>
           <tr><td colspan=2 class="text_xsmall"><cfif #len(feed_desc)# GT 500>#left(replace(feed_desc,"#chr(10)#","<br>","all"),'500')#...<cfelse>#replace(feed_desc,"#chr(10)#","<br>","all")#</cfif></td></tr>
           <tr><td colspan=2 align=right><font size=1 color="gray"><b>Added on #dateformat(feed_interest_decision_date,'mm/dd/yy')# at #timeformat(feed_interest_decision_date)#</b></font></td></tr>
		  <cfif #feed_attachment# is not "">
			<tr><td class="feed_title"><font size=2><a href="#media_virtual#/#feed_attachment#" target="_blank" rel="noopener" rel="noreferrer"><u>See Attachment</u></a></font></td></tr>
			<tr><td>&nbsp;</td></tr>
		  </cfif>
           </table>

           </div>

           </cfoutput>

   		   </table>

		   </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

