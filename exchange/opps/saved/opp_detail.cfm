<cfinclude template="/exchange/security/check.cfm">

<cfquery name="detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from feed
 left join feed_interest on feed.feed_id = feed_interest.feed_interest_feed_id
 where (feed.feed_id = #feed_id#) and
       (feed_interest.feed_interest_usr_id = #session.usr_id#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td colspan=4 class="feed_header">Opportunity Detail</td>
			            <td class="feed_option" align=right><a href="/exchange/opps/saved/">Return</a></td>

           </tr>
           <tr><td>&nbsp;<td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		<tr><td>&nbsp;</td></tr>

		<cfoutput>

        <tr><td><b>Title</b></td></tr>
        <tr><td class="feed_option">#detail.feed_title#</td></tr>
        <tr><td height=10>&nbsp;</td></tr>
        <tr><td><b>Organization</b></td></tr>
        <tr><td class="feed_option">#detail.feed_organization#</td></tr>
        <tr><td height=10>&nbsp;</td></tr>
        <tr><td colspan=2><b>Description</b></td></tr>
        <tr><td colspan=2 class="feed_option">#replace(detail.feed_desc,"#chr(10)#","<br>","all")#</td></tr>
        <tr><td>&nbsp;</td></tr>
    	<tr><td colspan=2><b>Full Description</b></td></tr>
       	<tr><td colspan=2 class="feed_option"><cfif #detail.feed_desc_long# is "">Not provided<cfelse>#replace(detail.feed_desc_long,"#chr(10)#","<br>","all")#</cfif></td></tr>
	    <tr><td>&nbsp;</td></tr>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

 				<tr><td><b>Reference</b></td></tr>
       			<tr><td class="feed_option"><cfif #detail.feed_url# is "">Not provided.<cfelse><a href="#detail.feed_url#" target="_blank" rel="noopener" rel="noreferrer">#detail.feed_url#</a></cfif></td></tr>
     			<tr><td height=10>&nbsp;</td></tr>
 				<tr><td><b>Date / Time Sent</b></td></tr>
    			<tr><td class="feed_option">#dateformat(detail.feed_updated,'mm/dd/yy')#, #timeformat(detail.feed_updated)#</td></tr>
    			<tr><td height=10>&nbsp;</td></tr>

 		   		</cfoutput>

              <form action="update.cfm" method="post">
 			  <tr><td><b>Notes & Actions</b></td></tr>
 			  <cfoutput>
              <tr><td><textarea name="notes" style="font-family: arial; font-size: 12px; width: 1000px; height: 100px;">#detail.feed_interest_notes#</textarea></td></tr>
              <tr><td>&nbsp;</td></tr>
              <tr><td><input class="button_blue" style="width: 100px; height: 25px" type="submit" name="button" value="Update"></td></tr>

              <input type="hidden" name="feed_id" value=#feed_id#>
              </cfoutput>
              </form>

			  </table>

             </td></tr>

           </table>

     	   </td></tr>
	  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

