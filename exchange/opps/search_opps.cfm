
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=10></td></tr>
 <tr><td valign=top width=50%>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header">Search Opportuities</td></tr>
 <tr><td height=10></td></tr>
 <tr><td>
 <input type="text" class="input_text" placeholder="keywords" style="width: 375px; height: 30px;">
 &nbsp;<input type="submit" name="button" value="Search" class="button_blue"></td></tr>
 </table>

 <table cellspacing=0 cellpadding=0 border=0 width=95%>
 <tr><td height=10></td></tr>
 <tr>

   <td><input type="checkbox" name="contract" style="width: 20px; height: 20px; margin-right: 10px;" checked></td>
   <td class="feed_sub_header">Contracts</td>

   <td><input type="checkbox" name="grants" style="width: 20px; height: 20px; margin-right: 10px;" checked></td>
   <td class="feed_sub_header">Grants</td>

   <td><input type="checkbox" name="sbir" style="width: 20px; height: 20px; margin-right: 10px;" checked></td>
   <td class="feed_sub_header">SBIR/STTRs</td>

   <td><input type="checkbox" name="challenges" style="width: 20px; height: 20px; margin-right: 10px;" checked></td>
   <td class="feed_sub_header">Challenges</td>
 </tr>
 </table>

</td><td width=20>&nbsp;</td><td valign=top>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr><td class="feed_header">Search Tips</td></tr>
<tr>
	<td class="feed_sub_header" style="font-weight: normal;">To narrow opportuities use a combination of keywords such as "machine learning and health" or
	"health or clinical".  Or, click on one of the tabs above to find specific opportunities.
	</td>
</tr>
</table>

</td></tr>
</table>