<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="cinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 join hub on hub_id = challenge_hub_id
 where challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="who" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(follow_id) as total from follow
 where follow_challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_usr_id, recent_challenge_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#session.usr_id#,#decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr>

      <td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="interest.cfm">
	      <cfinclude template="recent_challenges.cfm">
      </td>

      <td valign=top width=100%>

       <cfif isdefined("challenge_code")>

       <div class="main_box">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header">CONGRATULATIONS!!!</td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;">You have been invited to participate in the below Challenge.  If you are interested
			 in this Challenge, please click the <b>Interested</b> link below and you will be added to the list of Challenge participants.</td></tr>
		   </table>

	   </div>

       </cfif>


       <div class="main_box">

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">#cinfo.challenge_name#</td>
            <td class="feed_sub_header" align=right>

            <img src="#image_virtual#/icon_list.png" style="margin-bottom: 5px;" width=17 hspace=5 valign=absmiddle>
            <cfif session.challenge_location is 1>
            	<a href="challenges.cfm">All Challenges</a>
            <cfelseif session.challenge_location is 2>
                <a href="challenges.cfm">All Challenges</a>
            </cfif>
            </td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	     <cfif isdefined("u")>
	      <cfif u is 1>
	       <tr><td class="feed_sub_header" style="color: green;" colspan=2>You have selected your interested in this Challenge.</td></tr>
          <cfelseif u is 2>
	       <tr><td class="feed_sub_header" style="color: green;" colspan=2>You have selected you're not interested in this Challenge.</td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

       <tr><td height=10></td></tr>

       <tr><td valign=middle width=125>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif cinfo.challenge_image is "">
				  <img src="/images/stock_challenge.png" width=100>
				<cfelse>
				  <img src="#media_virtual#/#cinfo.challenge_image#" width=100>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                <td class="feed_sub_header">Organization</td>
                <td class="feed_sub_header">Exchange / Network</td>
                <td class="feed_sub_header" align=center>Interested</td>
                <td class="feed_sub_header" align=center>Status</td>
                <td class="feed_sub_header" align=center>Start Date</td>
                <td class="feed_sub_header" align=center>End Date</td>
             </tr>

             <tr>
                <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #cinfo.challenge_annoymous# is 1>Private<cfelse><cfif #cinfo.challenge_organization# is "">Not Identified<cfelse>#cinfo.challenge_organization#</cfif></cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;">#cinfo.hub_name#</td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=100>

                <cfif who.total is 0>
                 No Interest
                <cfelseif who.total is 1>
                 1 Person
                <cfelse>
                 #who.total# People
                </cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120>

			     <cfif #cinfo.challenge_status# is 1>
			      <img src="/images/icon_open.png" width=80>
			     <cfelseif #cinfo.challenge_status# is 2>
			      <img src="/images/icon_closed.png" width=80>
			     <cfelseif #cinfo.challenge_status# is 3>
			      <img src="/images/icon_pending.png" width=80>
			     </cfif>

			    </td>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120><cfif cinfo.challenge_start is "">TBD<cfelse>#dateformat(cinfo.challenge_start,'mmm d, yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120><cfif cinfo.challenge_end is "">TBD<cfelse>#dateformat(cinfo.challenge_end,'mmm d, yyyy')#</cfif></td>
             </tr>

          </table>

         </td></tr>

       </table>

       </div>

       <div class="main_box">

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_header">Challenge Summary</td>

           <td align=right class="feed_sub_header">
		                    <a href="##" onclick="window.open('question.cfm?i=#i#&t=c','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><img src="#image_virtual#/icon_question3.png" width=20 hspace=5 border=0 style="margin-bottom: 5px;"></a>

		                    <a href="##" onclick="window.open('question.cfm?i=#i#&t=c','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;">Ask Question</a>

                 &nbsp;&nbsp;

           </td></tr>
           <tr><td colspan=3><hr></td></tr>
           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>


           <tr><td valign=top width=60% valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Description</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_desc# is "">Not Provided<cfelse>#cinfo.challenge_desc#</cfif></td>
				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Today's Challenges</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_challenges# is "">Not Provided<cfelse>#cinfo.challenge_challenges#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Use Case Scenario</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_use_case# is "">Not Provided<cfelse>#cinfo.challenge_use_case#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Future State Solution</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_future_state# is "">Not Provided<cfelse>#cinfo.challenge_future_state#</cfif></td></tr>

			   </table>

           </td><td width=30></td><td width=38% valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Keywords</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_keywords# is "">Not Provided<cfelse>#cinfo.challenge_keywords#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Reference Website</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_url# is "">Not Provided<cfelse><a href="#cinfo.challenge_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#cinfo.challenge_url#</a></cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Reward</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_reward# is "">Not Provided<cfelse>#cinfo.challenge_reward#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-bottom: 0px;">Instructions</td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #cinfo.challenge_instructions# is "">Not Provided<cfelse>#cinfo.challenge_instructions#</cfif></td></tr>

			   </table>

           </td></tr>

           </table>

           <cfif #cinfo.challenge_annoymous# is not 1>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <tr><td height=10></td></tr>
				 <tr><td colspan=2><hr></td></tr>

				 <tr>
					 <td class="feed_sub_header" width=200>Point of Contact</td>
					 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #cinfo.challenge_poc_name# is "">Not Provided<cfelse>#cinfo.challenge_poc_name#</cfif></td></tr>
				 </tr>

				 <tr>
					 <td class="feed_sub_header" width=200>Title</td>
					 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #cinfo.challenge_poc_email# is "">Not Provided<cfelse>#cinfo.challenge_poc_title#</cfif></td></tr>
				 </tr>

				 <tr>
					 <td class="feed_sub_header" width=200>Email</td>
					 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #cinfo.challenge_poc_email# is "">Not Provided<cfelse>#cinfo.challenge_poc_email#</cfif></td></tr>
				 </tr>

				 <tr>
					 <td class="feed_sub_header" width=200>Phone</td>
					 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #cinfo.challenge_poc_phone# is "">Not Provided<cfelse>#cinfo.challenge_poc_phone#</cfif></td></tr>
				 </tr>

			   </table>

           </cfif>

			<cfquery name="attachments" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 select * from attachment
			 join usr on usr_id = attachment_created_by
			 where attachment_challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">Challenge Attachments</td>
                 <td class="feed_sub_header" align=right></td></tr>
           </table>

       </cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif attachments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No attachments have been added for this Challenge.</td></tr>
             <cfelse>

              <tr>
                  <td class="feed_sub_header">Name</td>
                  <td class="feed_sub_header">Desciption</td>
                  <td class="feed_sub_header">Attachment</td>
                  <td class="feed_sub_header" align=center>Added</td>
                  <td class="feed_sub_header" align=right>By</td>
              </tr>

              <cfoutput query="attachments">

               <tr>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top width=200><b>#attachment_name#</b></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_desc# is "">Not Provided<cfelse>#attachment_desc#</cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_file# is "">No attachment<cfelse><a href="#media_virtual#/#attachment_file#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;"><u>#attachment_file#</u></a></cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=center width=120>#dateformat(attachment_updated,'mm/dd/yyyy')#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=right width=200>#usr_first_name# #usr_last_name#</td>
               </tr>

              </cfoutput>

             </cfif>

			<cfquery name="updates" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from up
			 left join usr on usr_id = up_usr_id
			 where up_challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
			 up_post = 1
			 order by up_date DESC
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">Challenge Updates</td>
                 <td class="feed_sub_header" align=right></td></tr>
           </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif updates.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No updates have been posted for this Challenge.</td></tr>
	    <cfelse>

	    <cfset count = 1>

	   <cfloop query="updates">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #up_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="update_edit.cfm?update_id=#up_id#"><b>#ucase(up_name)#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#ucase(up_name)#</td>
			</cfif>
			<td class="feed_sub_header" valign=top align=right width=75>
			<cfif up_post is 0>
			 NOT POSTED
			<cfelse>
			 POSTED
			</cfif>
			</td>

		 </tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfif #up_desc# is "">No update provided<cfelse>#up_desc#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #up_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#up_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #up_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#up_url#" target="_blank" rel="noopener" rel="noreferrer">#up_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#usr_first_name# #usr_last_name# | #dateformat(up_date,'mm/dd/yyyy')# at #timeformat(up_date)#</td>
		   </tr>

		   <cfif count LT updates.recordcount>
		    <tr><td colspan=2><hr></td></tr>
		   </cfif>

		   <cfset count = count + 1>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>

		<cfquery name="questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from question
		 where question_public = 1 and
		 question_challenge_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
         order by question_date_submitted DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td colspan=2><hr></td></tr>
		 <tr>
			 <td class="feed_sub_header">Challenge Questions & Answers</td>
			 <td class="feed_sub_header" align=right></td></tr>
	   </table>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfif questions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been submitted or posted for this Challenge.</td></tr>
         <cfelse>

         <cfoutput query="questions">

          <tr><td class="feed_sub_header" valign=top><b>Q.  #questions.question_subject#</td><td class="feed_sub_header" valign=top align=right>#dateformat(questions.question_updated,'mm/dd/yyyy')#, #timeformat(questions.question_updated)#</td></tr>
          <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">#questions.question_text#</td></t/r>

         <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>A.  </b>#questions.question_answer#</td></tr></tr>
          <tr><td colspan=2><hr></td></tr>

         </cfoutput>

         </cfif>

           </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>