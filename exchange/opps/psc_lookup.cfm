<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">PRODUCT OR SERVICE CODE LOOKUP</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<cfoutput>
	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <form action="psc_lookup.cfm" method="post">
	 <tr>
		 <td class="feed_sub_header" width=225>Search for code or keyword</td>
		 <td><input type="text" name="psc" class="input_text" style="width: 250px;" required maxlength="299" <cfif isdefined("psc")>value="#psc#"</cfif>>
			 <input class="button_blue" type="submit" name="button" value="Search"></td>
	 </tr>
	 </form>
	 </table>
 </cfoutput>

<cfif isdefined("psc")>

 <cfquery name="lookup" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select class_code_code, class_code_name from class_code
  where class_code_code like '%#trim(psc)#%' or class_code_name like '%#trim(psc)#%'
  order by class_code_name
 </cfquery>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfif lookup.recordcount is 0>

	 <tr><td class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>

	<cfelse>

	 <tr>
	    <td class="feed_sub_header" width=100>Code</td>
	    <td class="feed_sub_header">Name / Description</td></tr>
	 </tr>

	 <cfset counter = 0>

	 <cfoutput query="lookup">

	 <cfif counter is 0>
	  <tr bgcolor="ffffff">
	 <cfelse>
	  <tr bgcolor="e0e0e0">
	 </cfif>

	 <td class="feed_sub_header">#class_code_code#</td>
	 <td class="feed_sub_header">

     #replaceNoCase(class_code_name,psc,"<span style='background:yellow'>#ucase(psc)#</span>","all")#

	 </td>

	 </tr>

	 <cfif counter is 0>
	  <cfset counter = 1>
	 <cfelse>
	  <cfset counter = 0>
	 </cfif>

	</cfoutput>

	</cfif>

 </table>


</cfif>

</center>

</body>
</html>

