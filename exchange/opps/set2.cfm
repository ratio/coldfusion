<cfif button is "Clear">
 <cfset StructDelete(Session,"original_keyword")>
 <cfset StructDelete(Session,"opp_posted_from")>
 <cfset StructDelete(Session,"opp_posted_from_to")>
 <cfset StructDelete(Session,"opp_dept")>
 <cfset StructDelete(Session,"opp_naics")>
 <cfset StructDelete(Session,"opp_psc")>
 <cfset StructDelete(Session,"opp_setaside")>
 <cfset StructDelete(Session,"opp_type")>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfset #session.original_keyword# = #opp_keyword#>
<cfset #session.opp_posted_from# = #opp_posted_from#>
<cfset #session.opp_posted_to# = #opp_posted_to#>
<cfset #session.opp_dept# = #opp_dept#>
<cfset #session.opp_naics# = #opp_naics#>
<cfset #session.opp_psc# = #opp_psc#>
<cfset #session.opp_setaside# = #opp_setaside#>
<cfset #session.opp_type# = #opp_type#>

<!--- Remove Invalid Characters --->

<cfif trim(opp_keyword) is "">
	<cfset search_string = ''>
<cfelse>
	<cfset search_string = #replace(opp_keyword,chr(34),'',"all")#>
	<cfset search_string = #replace(search_string,'''','',"all")#>
	<cfset search_string = #replace(search_string,')','',"all")#>
	<cfset search_string = #replace(search_string,'(','',"all")#>
	<cfset search_string = #replace(search_string,',','',"all")#>
	<cfset search_string = #replace(search_string,':','',"all")#>
	<cfset search_string = '"' & #search_string#>
	<cfset search_string = #search_string# & '"'>
	<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
	<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
</cfif>

<cfset #session.opp_keyword# = #search_string#>
<cfset #session.opp_status# = #opp_status#>

 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into keyword_search
  (
   keyword_search_usr_id,
   keyword_search_company_id,
   keyword_search_hub_id,
   keyword_search_keyword,
   keyword_search_date,
   keyword_search_area
   )
   values
   (
   #session.usr_id#,
   #session.company_id#,
   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  '#opp_keyword#',
   #now()#,
   4
   )
 </cfquery>

<cfif session.view is 1>
<cflocation URL="/exchange/opps/results.cfm" addtoken="no">
<cfelse>
<cflocation URL="/exchange/opps/results_expanded.cfm" addtoken="no">
</cfif>