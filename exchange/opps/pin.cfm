<cfquery name="pipe_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_pipeline_id from sharing
 where sharing_hub_id = #session.hub# and
	   sharing_pipeline_id is not null and
	   (sharing_to_usr_id = #session.usr_id#)
</cfquery>

<cfif pipe_access.recordcount is 0>
 <cfset pipe_list = 0>
<cfelse>
 <cfset pipe_list = valuelist(pipe_access.sharing_pipeline_id)>
</cfif>

<cfquery name="boards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select pipeline_name, pipeline_image, pipeline_id, usr_first_name, usr_last_name, pipeline_updated from pipeline
  left join usr on usr_id = pipeline_usr_id
  where pipeline_hub_id = #session.hub# and
		(pipeline_id in (#pipe_list#) or pipeline_usr_id = #session.usr_id#)
  order by pipeline_updated DESC
</cfquery>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header"><a href="/exchange/pipeline/">Opportunity Boards</a></td>
	 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="/exchange/pipeline/create_pipeline.cfm?l=dashboard">Add Board</a></a></td></tr>
 <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=2>
 Contains opportunities that you've shown interest in and have pinned to a board.
 </td></tr>
 <tr><td height=10></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr>
	 <td class="board">

         <cfloop query="boards">

			<cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from deal
			  where deal_pipeline_id = #boards.pipeline_id# and
			        deal_hub_id = #session.hub# and
			        deal_comm_id is null
			</cfquery>

			<cfquery name="count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select count(deal_id) as total, sum(deal_value_total) as revenue from deal
			  where deal_pipeline_id = #boards.pipeline_id# and
			        deal_hub_id = #session.hub# and
			        deal_comm_id is null
			</cfquery>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

         	<cfoutput>
		 		<tr bgcolor="e0e0e0">
                    <td class="feed_sub_header"><a href="##" onclick="toggle_visibility('#boards.pipeline_id#');"><img src="/images/plus3.png" width=15 hspace=5 border=0 alt="Expand" title="Expand"></a>
 		 		    &nbsp;<a href="##" onclick="toggle_visibility('#boards.pipeline_id#');">#boards.pipeline_name#

 		 		    <cfif count.total GT 0>
 		 		     <cfif #count.total# is 1>
 		 		     ( 1 Opportunity )
 		 		     <cfelse>
 		 		     ( #count.total# Opportunities )
 		 		     </cfif>
 		 		    </cfif>



                    </a>

 		 		    </td>
                    <td class="feed_sub_header" align=right style="padding-right: 15px;">

                    <cfif count.revenue is 0>
                     Board Value - $0
                    <cfelse>
                     Board Value - #numberformat(count.revenue,'$999,999,999')#
                    </cfif>

                    </td>
		 		</tr>

		 		<tr><td height=10></td></tr>

		 	</cfoutput>
            </table>
          <cfoutput><div id="#boards.pipeline_id#" style="display:none;"></cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 	<cfif deals.recordcount is 0>
		 	 <tr><td class="feed_option" style="font-weight: normal;">No Opportunities have been pinned to this board.</td></tr>
		 	<cfelse>

		 	<tr>
		 	   <td class="feed_sub_header">Opportunity Name</td>
		 	   <td class="feed_sub_header">Customer</td>
		 	   <td class="feed_sub_header">Type</td>
		 	   <td class="feed_sub_header" align=right>Potential Value</td>
            </tr>

		 	<cfset counter = 0>

		 	<cfoutput query="deals">
              <cfif counter is 0>
              <tr bgcolor="ffffff">
              <cfelse>
              <tr bgcolor="f3f3f3">
              </cfif>

                 <td class="feed_sub_header" style="font-weight: normal; padding-top: 10px; padding-bottom: 10px;" valign=top>
                  <a href="/exchange/pipeline/set.cfm?i=#encrypt(boards.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&d=#encrypt(deals.deal_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" target="_blank" rel="noopener" rel="noreferrer"><b>#deals.deal_name#</b></a>
                 </td>

                 <td class="feed_sub_header" style="font-weight: normal; padding-top: 10px; padding-bottom: 10px;" valign=top>
                  #deals.deal_customer_name#
                 </td>

                 <td class="feed_sub_header" style="font-weight: normal; padding-top: 10px; padding-bottom: 10px;" valign=top>
                  #deals.deal_type#
                 </td>

                 <td class="feed_sub_header" style="font-weight: normal; padding-top: 10px; padding-bottom: 10px;" align=right width=150 valign=top>
                  <cfif deals.deal_value_total is "">
                   TBD
                  <cfelse>
                   #numberformat(deals.deal_value_total,'$999,999,999')#
                  </cfif>
                 </td>

              </tr>

              <cfif counter is 0>
               <cfset counter = 1>
              <cfelse>
               <cfset counter = 0>
              </cfif>

		 	</cfoutput>

		 	</cfif>
            <tr><td height=10></td></tr>

			</table>

			</div>

		 </cfloop>

	 </td>

  </tr>

</table>