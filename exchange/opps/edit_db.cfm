<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

	<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 update opp_search
	 set opp_search_name = '#opp_search_name#',
	     opp_search_status = #opp_search_status#,
	     opp_search_type = '#opp_search_type#',
	     opp_search_dept = '#opp_search_dept#',
	     opp_search_posted_from = <cfif opp_search_posted_from is "">null<cfelse>'#opp_search_posted_from#'</cfif>,
	     opp_search_posted_to = <cfif opp_search_posted_to is "">null<cfelse>'#opp_search_posted_to#'</cfif>,
	     opp_search_naics = '#opp_search_naics#',
	     opp_search_psc = '#opp_search_psc#',
	     opp_search_setaside = '#opp_search_setaside#',
	     opp_search_keyword = '#opp_search_keyword#',
	     opp_search_desc = '#opp_search_desc#'

	 where opp_search_id = #opp_search_id# and
	       opp_search_usr_id = #session.usr_id#
	</cfquery>

	<cfif l is 1>
	 <cflocation URL="/exchange/opps/go.cfm?opp_search_id=#opp_search_id#&u=2" addtoken="no">
	<cfelse>
	 <cflocation URL="/exchange/opps/go_expanded.cfm?opp_search_id=#opp_search_id#&u=2" addtoken="no">
	</cfif>

<cfelseif #button# is "Save Search">

  <cftransaction>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into opp_search
	 (
      opp_search_name,
      opp_search_type,
      opp_search_status,
      opp_search_dept,
      opp_search_posted_from,
      opp_search_posted_to,
      opp_search_naics,
      opp_search_psc,
      opp_search_setaside,
      opp_search_keyword,
      opp_search_desc,
      opp_search_usr_id
      )
      values
      (
      '#opp_search_name#',
      '#opp_search_type#',
       #opp_search_status#,
      '#opp_search_dept#',
      <cfif opp_search_posted_from is "">null<cfelse>'#opp_search_posted_from#'</cfif>,
      <cfif opp_search_posted_to is "">null<cfelse>'#opp_search_posted_to#'</cfif>,
      '#opp_search_naics#',
      '#opp_search_psc#',
      '#opp_search_setaside#',
      '#opp_search_keyword#',
      '#opp_search_desc#',
       #session.usr_id#
      )
	</cfquery>

    </cftransaction>

    <cflocation URL="results.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 delete opp_search
	 where opp_search_id = #opp_search_id# and
	       opp_search_usr_id = #session.usr_id#
	</cfquery>

	<cflocation URL="/exchange/opps/index.cfm?u=3" addtoken="no">

</cfif>
