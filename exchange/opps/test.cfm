<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<table>

<tr><td class="feed_header">
"<div class=""modal-body"">
              <p></p><p><a name=""_Toc258873267""></a><a name=""_Section_II._Award""></a>The principal aim of this SBIR Funding Opportunity Announcement (FOA) is to stimulate research focusing on the development of innovative technologies that would enhance the treatment and clinical management of Alzheimer's disease (AD), Alzheimer's disease-related dementias (ADRD), and their related comorbidities. </p>
<p>Alzheimer's disease (AD) is the leading cause of dementia individuals over the age of 65 years and as many as 5 million older Americans have AD and/or Alzheimer's-disease-related dementias (ADRD).  AD and ADRD present a progressive and prolonged course of cognitive decline that can influence the diagnosis and treatment of comorbid illness(es) and can result in complex clinical-management challenges. </p>
<p>Clinical care and management of AD and ADRD should incorporate the caregiving preferences and goals of patients and their caregivers, prognosis, and other geriatric problems and syndromes.  The progression of AD and ADRD often comes to dominate clinical care and management and patients need early consideration and documentation of their caregiving preferences and goals. Consequently, reminding patients and their caregivers of their caregiving preferences and goals is essential.</p>
<p>There are few clinical guidelines available to address the clinical care and management of AD, ADRD, and related comorbidities.  Notable among them are: avoiding the use of medications known to have adverse effects for individuals suffering from AD and ADRD; prescribing tube feeding for patients who cannot obtain nutrition by mouth or who are unable to swallow safely or who need nutritional supplementation; and providing end-of-life care when clinical management and other treatment options are no longer indicated or possible. </p>
<p>A primary goal of this FOA is to encourage research that translates existing clinical guidelines into new and effective tools for the clinical care and management of AD, ADRD, and comorbidities.  In addition, this FOA also seeks to encourage the development of more personalized approaches.  Finally, the FOA aims to stimulate the development of clinical-care and management tools suitable for use by caregivers.</p>
<p>Examples of research topics that might be supported by this FOA include but are not limited to:</p>
<ul>
<li>Devising tools to measure outcomes of AD, ADRD, and related comorbidities, including tools that measure functional and cognitive status;</li>
<li class=""P_DoubleIndent"">Such tools would assess functional and cognitive status over multiple occasions and yield measurement data in a format amenable to incorporation into electronic health records (EHRs), which would facilitate data-sharing and coordinated clinical care and management;</li>
<li>Identifying, recording, and tracking changes in personalized goals of care, care preferences, and the documented advance directives;</li>
<li>Enabling clinical management across practice settings in line with patient goals and advance directives, including decisions to terminate the prescription of undesirable drugs and to reduce the intensity of medical interventions/services;</li>
<li>Caregiver-focused tools that support caregivers and patients with AD/ADRD to manage their health conditions and to prolong in-home residency; and</li>
<li>Developing clinical care resources and tools addressing applicable to reducing problems related to caregiver health, or improving aspects of caregiver health.</li>
</ul>
<p>See <a href=""http://grants.nih.gov/grants/guide/pa-files/PAR-17-067.html#_Section_VIII._Other"" class=""ext"">Section VIII. Other Information<span class=""ext"" aria-label=""(link is external)""></span></a> for award authorities and regulations.</p>
<p> </p>
<div class=""heading2""><a name=""_Section_II._Award_1""></a></div><p></p>
            </div>"</td></tr>
</table>
