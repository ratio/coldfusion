<div class="left_box">

		  <cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="10">
		   select distinct(need_id), need_name, max(recent_date), need_desc, need_image from recent
		   join need on need_id = recent_need_id
		   where recent_usr_id = #session.usr_id# and
		   recent_need_id is not null
		   group by need_id, need_name, need_image, need_desc
		   order by max(recent_date) DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><a href="/exchange/portfolio"><b>Recently Viewed</b></a></td>
		      <td align=right valign=top></td></tr>
		  <tr><td><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif recent.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;">No Challengess have been viewed.</td></tr>
          <cfelse>

          <tr><td height=10></td></tr>

		  <cfoutput query="recent">


			   <tr>
				   <td width=32><a href="/exchange/needs/need_details.cfm?i=#encrypt(recent.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

				   <cfif #recent.need_image# is "">
					 <img src="#image_virtual#/stock_need.png" width=22 border=0>
				   <cfelse>
					 <img src="#media_virtual#/#recent.need_image#" width=22>
				   </cfif>
				   </a></td>

				   <td class="link_med_blue"><a href="/exchange/needs/need_details.cfm?i=#encrypt(recent.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(recent.need_name) GT 45>#left(recent.need_name,'45')#...<cfelse>#recent.need_name#</cfif></a></td>
				</tr>

			   <tr><td></td><td class="link_small_gray" style="padding-left: 0px;" colspan=2>

                  <cfif len(recent.need_desc) GT 50>
                   #left(recent.need_desc,50)#...
                  <cfelse>
                   #recent.need_desc#
                  </cfif>


			   </td></tr>

			   <tr><td height=10></td></tr>

			  </cfoutput>

          </cfif>

	</center>

	</table>

</div>