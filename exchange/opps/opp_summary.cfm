<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		<cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select count(distinct(fbo_solicitation_number)) as total, fbo_naics_code, naics_code_description from fbo
		 left join naics on naics_code = fbo_naics_code
         where (fbo_response_date_updated > #now()# or fbo_response_date_original > #now()#)

		 <cfif isdefined("session.opp_filter")>
		  and (naics_code like '%#session.opp_filter#%' or naics_code_description like '%#session.opp_filter#%')
		 </cfif>

         group by fbo_naics_code, naics_code_description
		 order by naics_code_description
		</cfquery>

           <form action="set_opp_filter.cfm" method="post">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_header">By Products & Services</td>
			    <td align=right class="feed_sub_header"><a href="/exchange/opps/">Return</a></td></tr>
			 <tr><td colspan=2><hr></td></tr>
             <tr><td class="feed_sub_header" style="font-weight: normal;">The following information summarizes Opportunities by NAICS Code, Product, or Services the Federal Government has requested.</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter By</b>
                 &nbsp;
                 <input type="text" class="input_text" style="width: 200px;" name="opp_filter" placeholder="enter keyword" <cfif isdefined("session.opp_filter")>value="<cfoutput>#session.opp_filter#</cfoutput>"</cfif>>
                 &nbsp;
                 <input type="submit" name="button" class="button_blue" value="Filter">
                 <input type="submit" name="button" class="button_blue" value="Clear">

                 </td>

             </tr>
			</table>
                 </form>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr><td valign=top width=48%>

                 <table cellspacing=0 cellpadding=0 border=0 width=100%>

                  <tr><td class="feed_sub_header" colspan=3>BY NAICS CODE</td></tr>

                  <tr height=40>
                      <td class="feed_option" style="padding-left: 10px;"><b>Code</b></td>
                      <td class="feed_option"><b>Description</b></td>
                      <td class="feed_option" align=right style="padding-right: 20px;"><b>Opps</b></td>
                  </tr>

                  <cfset counter = 0>
				  <cfoutput query="naics">
				   <cfif counter is 0>
				   <tr bgcolor="ffffff" height=30>
				   <cfelse>
				   <tr bgcolor="e0e0e0" height=30>
				   </cfif>
				       <td class="feed_option" valign=top width=100>&nbsp;&nbsp;<cfif #fbo_naics_code# is "">NOT SPECIFIED<cfelse>#ucase(fbo_naics_code)#</cfif></td>
				       <td class="feed_option" valign=top><a href="cat_opps.cfm?t=1&code=#fbo_naics_code#"><b><cfif #naics_code_description# is "">NOT SPECIFIED<cfelse>#ucase(naics_code_description)#</cfif></b></a></td>
					   <td class="feed_option" valign=top align=right style="padding-right: 20px;"><a href="cat_opps.cfm?t=1&code=#fbo_naics_code#">#trim(numberformat(total,'99,999'))#</a></td></tr>
				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				  </cfoutput>

                 </table>

                 </td><td width=50>&nbsp;</td><td valign=top width=48%>

                 <table cellspacing=0 cellpadding=0 border=0 width=100%>

                  <tr><td class="feed_sub_header" colspan=3>BY PRODUCT OR SERVICE CODE</td></tr>
                  <tr height=40>
                      <td class="feed_option" style="padding-left: 10px;"><b>Code</b></td>
                      <td class="feed_option"><b>Description</b></td>
                      <td class="feed_option" align=right style="padding-right: 20px;"><b>Opps</b></td>
                  </tr>
			      <cfquery name="product" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				   select count(distinct(fbo_solicitation_number)) as total, class_code_code, class_code_name from fbo
				   left join class_code on class_code_code = fbo_class_code
				   where (fbo_response_date_updated > #now()# or fbo_response_date_original > #now()#)

					 <cfif isdefined("session.opp_filter")>
					  and (class_code_code like '%#session.opp_filter#%' or class_code_name like '%#session.opp_filter#%')
					 </cfif>

                   group by class_code_code, class_code_name
				   order by class_code_name ASC
				  </cfquery>

                  <cfset counter = 0>
				  <cfoutput query="product">
				   <cfif counter is 0>
				   <tr bgcolor="ffffff" height=30>
				   <cfelse>
				   <tr bgcolor="e0e0e0" height=30>
				   </cfif>
				       <td class="feed_option" valign=top width=100 style="padding-left: 10px;"><a href="cat_opps.cfm?t=2&code=#class_code_code#"><cfif class_code_code is "">NOT SPECIFIED<cfelse>#ucase(class_code_code)#</cfif></a></td>
				       <td class="feed_option" valign=top><a href="cat_opps.cfm?t=2&code=#class_code_code#"><b><cfif class_code_name is "">NOT SPECIFIED<cfelse>#ucase(class_code_name)#</cfif></b></a></td>
					   <td class="feed_option" valign=top align=right style="padding-right: 20px;"><a href="cat_opps.cfm?t=2&code=#class_code_code#">#trim(numberformat(total,'99,999'))#</a></td></tr>
				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				  </cfoutput>

                 </table>

                 </td></tr>
            </table>
       </tr>
   </table>

        </form>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>