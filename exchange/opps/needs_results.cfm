<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<!--- Lookups --->

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
          <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="recent_needs.cfm">
      </td><td valign=top>

      <cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">Contracts <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">Grants <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">Needs</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">Challenges <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
          </div>

      </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_needs.cfm">
	  </div>

      <div class="main_box">

		<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select * from need
		 where need_public = 1

	     <cfif session.need_status is 1>
		  and need_status = 'Open'
	     <cfelseif session.need_status is 2>
		  and need_status = 'Closed'
	     </cfif>

         <cfif trim(session.need_keyword) is not "">
	      and contains((*),'"#trim(session.need_keyword)#"')
	     </cfif>
		</cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <cfoutput>
		 	<tr><td class="feed_header">Search Results <cfif needs.recordcount GT 0>(#needs.recordcount#)</cfif></td></tr>
		 </cfoutput>
		 <tr><td><hr></td></tr>
		</table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif needs.recordcount is 0>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">No needs were found.</td></tr>
		<cfelse>

        <cfloop query="needs">

         <cfoutput>

          <tr bgcolor="ffffff" height=70>


			<td width=150 valign=top>

			<cfif needs.need_image is "">
			  <a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#image_virtual#stock_need.png" style="margin-top: 15px;" width=125 border=0></a>
			<cfelse>
			  <a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#needs.need_image#" style="margin-top: 15px;" width=125></a>
			</cfif>
            </td>

            <td valign=top>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr>

             <td class="feed_sub_header" width=450><b><a href="/exchange/needs/need_details.cfm?i=#encrypt(needs.need_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#needs.need_name#</a></b></td>
                 <td align=right width=40>
			     </td>
			</tr>

			</table>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#needs.need_desc#</td></tr>
			<tr><td class="link_small_gray"><b>Keywords - </b><i>#needs.need_keywords#</i></b></td>
			    <td class="feed_sub_header" align=right>Response Due -

			    <cfif #needs.need_date_response# is "">
			     Not provided
			    <cfelse>
			     #dateformat(needs.need_date_response,'mmm dd, yyyy')#
			    </cfif>


			    </td></tr>
			</table>

			</td>

		   </cfoutput>

         </tr>

         </cfloop>


		</cfif>

		</table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>