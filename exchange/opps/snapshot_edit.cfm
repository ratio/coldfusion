<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 20px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    display: inline-block;
    margin-left: -4px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    padding-right: 20px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}

.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<style>
.board {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 300px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from snapshot
  where snapshot_id = #snapshot_id# and
		snapshot_hub_id = #session.hub# and
		snapshot_usr_id = #session.usr_id#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

      <cfoutput>

		  <div class="tab_active">
		   <span class="feed_header"><img src="/images/icon_blocks.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">DASHBOARD</a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">PROCUREMENTS <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">GRANTS <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_light.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_info.png" width=19 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">NEEDS</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
		  </div>

		  <div class="tab_not_active">
		   <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=19 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">CHALLENGES <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
		  </div>

	  </cfoutput>

      <div class="main_box_2">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td class="feed_header">Edit Snapshot</td>
             <td class="feed_sub_header" align=right><a href="dashboard.cfm">Return</a></td>
             </tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>
         Please provide a name and keywords for this Snapshot.
         </td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

        <cfoutput>

        <form action="snapshot_db.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header" width="150">Snapshot Name</td>
             <td><input type="text" name="snapshot_name" class="input_text" value="#edit.snapshot_name#" required style="width: 400px;"></td>
             </tr>

         <tr><td class="feed_sub_header">Keywords</td>
             <td><input type="text" name="snapshot_keyword" class="input_text" value='#replaceNoCase(edit.snapshot_keyword,'"','',"all")#' required style="width: 500px;"></td>
             </tr>

         <tr><td></td>
             <td class="link_small_gray">Keyword Example: machine learning and healthcare</td></tr>

         <tr><td height=5></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>

         <tr><td></td><td>

         <input class="button_blue_large" type="submit" name="button" value="Save">
		 &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');">

		 </td></tr>

		 <input type="hidden" name="snapshot_id" value=#edit.snapshot_id#>



         </td></tr>
        </table>

        </form>

        </cfoutput>

      </div>

      </td>
      </tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>