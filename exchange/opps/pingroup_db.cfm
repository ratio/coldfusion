<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into pingroup
	  (
	   pingroup_name,
	   pingroup_hub_id,
	   pingroup_usr_id
	  )
	  values
	  (
	  '#pingroup_name#',
	   #session.hub#,
	   #session.usr_id#
	  )
	</cfquery>

	<cflocation URL="dashboard.cfm?u=4" addtoken="no">

<cfelseif button is "Save">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update pingroup
	  set pingroup_name = '#pingroup_name#'
	  where pingroup_id = #pingroup_id# and
	        pingroup_hub_id = #session.hub# and
	        pingroup_usr_id = #session.usr_id#
	</cfquery>

	<cflocation URL="dashboard.cfm?u=5" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete pingroup
	  where pingroup_id = #pingroup_id# and
	        pingroup_hub_id = #session.hub# and
	        pingroup_usr_id = #session.usr_id#
    </cfquery>

	<cflocation URL="dashboard.cfm?u=6" addtoken="no">

</cfif>