 <cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <form action="/exchange/opps/set_need.cfm" method="post">

	      	  <tr><td class="feed_header" colspan=2>Search Needs</td></tr>
	          <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Needs are requests that customers post on the Exchange seeking insights, information or approaches to help them solve their challenges or problems.</td></tr>
	      	  <tr><td colspan=2><hr></td></tr>

              <tr>
                  <td class="feed_sub_header" style="font-weight: normal;"><b>Keyword</b>&nbsp;&nbsp;
				  <input type="text" name="need_keyword" class="input_text" style="width: 250px;" maxlength="100" placeholder="enter keyword">
				  &nbsp;&nbsp;<b>Status</b>&nbsp;&nbsp;

				  <select name="need_status" class="input_select">
				   <option value=0 <cfif session.need_status is 0>selected</cfif>>All
				   <option value=1 <cfif session.need_status is 1>selected</cfif>>Open
				   <option value=2 <cfif session.need_status is 2>selected</cfif>>Closed
				  </select>&nbsp;

			      &nbsp;<input class="button_blue" type="submit" name="button" value="Search"></td></tr>

          </form>

	      </tr>
	 </table>
 </cfoutput>


