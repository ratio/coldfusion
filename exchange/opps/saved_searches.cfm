<cfif isdefined("opp_search_id")>
 <cfset choice = #opp_search_id#>
<cfelse>
 <cfset choice = 0>
</cfif>

<div class="left_box">

	<cfquery name="saved" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from opp_search
	 where opp_search_usr_id = #session.usr_id#
	 order by opp_search_name
	</cfquery>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
    <tr><td class="feed_header" colspan=2>Saved Searches</td>
        <td align=right><a href="/exchange/opps/add_search.cfm"><img src="/images/plus3.png" width=15 border=0 alt="Add Search" title="Add Search"></a></td></tr>
    <tr><td colspan=3><hr></td></tr>
    <cfif saved.recordcount is 0>
	 <tr><td class="feed_sub_header" style="font-weight: normal;">No searches have been saved.</td></tr>
	<cfelse>
	  <cfloop query="saved">

		  <cfquery name="count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(distinct(fbo_solicitation_number)) as total from fbo
		   left join class_code on class_code_code = fbo_class_code
		   left join naics on naics_code = fbo_naics_code
		   where fbo_id > 0

		   <cfif #saved.opp_search_keyword# is not "">
			and contains((fbo_opp_name, fbo_solicitation_number, fbo_desc, fbo_agency, fbo_dept, fbo_naics_code, fbo_contract_award_name),'"#saved.opp_search_keyword#"')
		   </cfif>

		   <cfif saved.opp_search_posted_from is "" and saved.opp_search_posted_to is "">
		   <cfelse>
			<cfif saved.opp_search_posted_from is "" and saved.opp_search_posted_to is not "">
			 and fbo_pub_date_original <= '#saved.opp_search_posted_to#'
			<cfelseif saved.opp_search_posted_from is not "" and saved.opp_search_posted_to is "">
			 and fbo_pub_date_original >= '#saved.opp_search_posted_from#'
			<cfelse>
			 and (fbo_pub_date_original >= '#saved.opp_search_posted_from#' and fbo_pub_date_original <= '#saved.opp_search_posted_to#')
			</cfif>
		   </cfif>

		   <cfif saved.opp_search_status is 2>
			and fbo_inactive_date_original > #now()#
		   <cfelseif saved.opp_search_status is 3>
			and fbo_inactive_date_original < #now()#
		   </cfif>

		   <cfif saved.opp_search_type is not 0>
			and fbo_type = '#saved.opp_search_type#'
		   </cfif>

		   <cfif saved.opp_search_dept is not 0>
			and fbo_agency = '#saved.opp_search_dept#'
		   </cfif>

		   <cfif saved.opp_search_setaside is not 0>
			and fbo_setaside_original = '#saved.opp_search_setaside#'
		   </cfif>

		   <cfif #listlen(saved.opp_search_naics)# GT 0>
				<cfif #listlen(saved.opp_search_naics)# GT 1>
				<cfset ncounter = 1>
				and (
				 <cfloop index="nc" list="#saved.opp_search_naics#">
				   (fbo_naics_code = '#nc#')
				   <cfif ncounter LT #listlen(saved.opp_search_naics)#> or</cfif>
				   <cfset ncounter = ncounter + 1>
				 </cfloop>
				 )
				<cfelse>
				 and fbo_naics_code = '#saved.opp_search_naics#'
				</cfif>
			</cfif>

		   <cfif #listlen(saved.opp_search_psc)# GT 0>
				<cfif #listlen(saved.opp_search_psc)# GT 1>
				<cfset pcounter = 1>
				and (
				 <cfloop index="pc" list="#saved.opp_search_psc#">
				   (fbo_class_code = '#pc#')
				   <cfif pcounter LT #listlen(saved.opp_search_psc)#> or</cfif>
				   <cfset pcounter = pcounter + 1>
				 </cfloop>
				 )
				<cfelse>
				 and fbo_class_code = '#saved.opp_search_psc#'
				</cfif>
			</cfif>

		   order by total DESC
		  </cfquery>

	   <cfoutput>
	   <tr height=27 <cfif choice is #saved.opp_search_id#>bgcolor="e0e0e0"</cfif>><td valign=middle width=26><a href="/exchange/opps/go.cfm?opp_search_id=#saved.opp_search_id#"><img src="/images/icon_search_gray.png" width=15 border=0></a></td>
	       <td class="link_med_blue" valign=middle><a href="/exchange/opps/go.cfm?opp_search_id=#saved.opp_search_id#"><cfif len(saved.opp_search_name) GT 21>#left(saved.opp_search_name,'21')#...<cfelse>#saved.opp_search_name#</cfif></a></td>
	       <td class="link_med_blue" valign=middle align=right><a href="/exchange/opps/go.cfm?opp_search_id=#saved.opp_search_id#">#numberformat(count.total,'9,999')#</a></td>
	       </tr>
	  </cfoutput>
	  </cfloop>
	</cfif>

	</table>


</div>

