<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="insert_recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 insert recent(recent_opp_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#fbo_id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

  <!--- Opportunity Detail --->

  <cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join naics on naics_code = fbo_naics_code
   left join class_code on class_code_code = fbo_class_code
   left join orgname on orgname_name = fbo_agency
   where fbo_id = #fbo_id#
  </cfquery>

  <!--- Opportunity History --->

  <cfquery name="history" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select * from fbo
   left join naics on naics_code = fbo_naics_code
   left join class_code on class_code_code = fbo_class_code
   where fbo_solicitation_number = '#fbo.fbo_solicitation_number#'
   order by fbo_pub_date_updated DESC
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

      <div class="main_box">

        <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td height=10></td></tr>
                <tr><td valign=top width=150>

				    <table cellspacing=0 cellpadding=0 border=0 width=100%>
					  <tr><td>

		               <cfif #fbo.orgname_logo# is ""><img src="#image_virtual#/icon_usa.png" valign=top align=top width=125 border=0 vspace=10><cfelse><a href="#fbo.orgname_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#fbo.orgname_logo#" valign=top align=top width=125 border=0 vspace=10></a></cfif>

					  </td></tr>
				    </table>

                    </td><td valign=top>

			          <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<tr><td class="feed_header">#ucase(fbo.fbo_dept)#</td><td align=right>

						<div class="dropdown" style="cursor: pointer;">
						  <img src="/images/3dots2.png" style="cursor: pointer; padding-left: 10px;" height=10>
						  <div class="dropdown-content" style="top: 5; width: 250px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px; padding-top: 0px; margin-top: 2px;">
							<a href="##" onclick="window.open('/exchange/include/save_opp.cfm?id=#fbo.fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"><i class="fa fa-thumb-tack" aria-hidden="true" style="padding-right: 10px;"></i>Pin to Opportunity Board</a>
							<a href="##" onclick="windowClose();" alt="Close" title="Close" style="cursor: pointer;"><i class="fa fa-times" aria-hidden="true" style="padding-right: 10px;"></i>Close</a>
						  </div>
						</div>

						<tr><td class="feed_sub_header">#ucase(fbo.fbo_agency)#</td></tr>
						<tr><td class="feed_sub_header">#fbo.fbo_office#</td></tr>
						<tr><td height=5></td></tr>
						<tr><td class="feed_option"><b>CONTRACTING OFFICE</b> - <cfif #fbo.fbo_contracting_office# is "">NOT SPECIFIED<cfelse>#ucase(fbo.fbo_contracting_office)#</cfif></td></tr>
						<tr><td height=15></td></tr>
                      </table>

                    </td>
                </tr>

                <tr><td colspan=3><hr></td></tr>

              </table>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>

		         <tr><td valign=top>

		          <table cellspacing=0 cellpadding=0 border=0 width=100%>

                     <tr><td height=10></td></tr>
						<tr><td class="feed_header" colspan=4>#ucase(fbo.fbo_opp_name)#</td><td align=right></td></tr>
						<tr><td height=10></td></tr>

                        <tr><td valign=top width=35%>

		                  <table cellspacing=0 cellpadding=0 border=0 width=100%>

							 <tr><td class="feed_option" valign=top width=150><b>Solicitation ##</td>
								 <td class="feed_option" valign=top width=300>#fbo.fbo_solicitation_number#</td></tr>

							 <tr>
								 <td class="feed_option" valign=top><b>Notice Type</td>
								 <td class="feed_option" valign=top>#fbo.fbo_notice_type#</td>
							 </tr>

							 <tr>
								 <td class="feed_option" valign=top><b>Small Business</td>
								 <td class="feed_option" valign=top><cfif #fbo.fbo_setaside_original# is "">Not specified.<cfelse>#fbo.fbo_setaside_original#</cfif></td>
							 </tr>


							 <tr>
								 <td class="feed_option" valign=top><b>NAICS</td>
								 <td class="feed_option" valign=top>#fbo.fbo_naics_code# - #fbo.naics_code_description#</td>
							 </tr>

							 <tr>

								 <td class="feed_option" valign=top><b>Product or Service</td>
								 <td class="feed_option" valign=top>

								 <cfif #fbo.fbo_class_code# is "">
								  Not provided
								 <cfelse>
								 #fbo.fbo_class_code# - #fbo.class_code_name#
								 </cfif></td>

							 </tr>

						  </table>

			         </td><td valign=top width=30%>

		                  <table cellspacing=0 cellpadding=0 border=0 width=100%>

							 <tr>
								 <td class="feed_option" valign=top width=150><b>Command</td>
								 <td class="feed_option" valign=top><cfif #fbo.fbo_major_command# is "">Not provided<cfelse>#fbo.fbo_major_command#</cfif></td>
							 </tr>

							 <tr>
								 <td class="feed_option" valign=top width=150><b>Sub Command</td>
								 <td class="feed_option" valign=top><cfif #fbo.fbo_sub_command# is "">Not provided<cfelse>#fbo.fbo_sub_command#</cfif></td>
							 </tr>

							 <tr>
								 <td class="feed_option" valign=top width=150><b>Place of Performance</td>
								 <td class="feed_option" valign=top>

								 <cfif #fbo.fbo_pop_city# is "" and #fbo.fbo_pop_state# is "">
								  Not specififed
								 <cfelse>
								 #fbo.fbo_pop_city#, #fbo.fbo_pop_state#  #fbo.fbo_pop_zip#  #fbo.fbo_pop_country#
								 </cfif>
								 </td>
							 </tr>

							 <tr>
								 <td class="feed_option" valign=top><b>Primary POC</td>
								 <td class="feed_option" valign=top>#fbo.fbo_poc#<cfif #fbo.fbo_poc_email# is not ""><br>#fbo.fbo_poc_email#</cfif><cfif #fbo.fbo_poc_phone# is not ""><br>#fbo.fbo_poc_phone#</cfif></td>
							 </tr>

							 <cfif #fbo.fbo_poc_secondary_email# is not "" and #fbo.fbo_poc_secondary_phone# is not"">

							 <tr>
								 <td class="feed_option" valign=top><b>Secondary POC</td>
								 <td class="feed_option" valign=top>#fbo.fbo_poc_secondary#<cfif #fbo.fbo_poc_secondary_email# is not ""><br>#fbo.fbo_poc_secondary_email#</cfif><cfif #fbo.fbo_poc_secondary_phone# is not ""><br>#fbo.fbo_poc_secondary_phone#</cfif></td>
							 </tr>
							 </cfif>

	                     </table>

                     </td><td valign=top width=35%>

		                  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			             <tr>
			                 <td class="feed_option" valign=top><b>Posted</td>
			                 <td class="feed_option" valign=top>#dateformat(fbo.fbo_pub_date,'mm/dd/yyyy')# at #timeformat(fbo.fbo_pub_date)#</td>
			             </tr>

			             <tr>
			                 <td class="feed_option" valign=top><b>Last Updated</td>
			                 <td class="feed_option" valign=top>

			                 <cfif fbo.fbo_updated is not "">
			                 	#dateformat(fbo.fbo_updated,'mm/dd/yyyy')# at #timeformat(fbo.fbo_updated)#
			                 </cfif>

			                 </td>

			             </tr>

			             <tr>
			                 <td class="feed_option"><b>Archive Date</b></td>
			                 <td class="feed_option">#dateformat(fbo.fbo_inactive_date_original,'mm/dd/yyyy')# at #timeformat(fbo.fbo_inactive_date_original)#</td>
						 </tr>

 					    <tr>
			                <td class="feed_option" valign=top><b>Response Date</td>
			                <td class="feed_option" valign=top>

			                 <cfif fbo.fbo_response_date_updated is "">
			                  <cfset due_date = #fbo.fbo_response_date_original#>
			                 <cfelse>
			                  <cfset due_date = #fbo.fbo_response_date_updated#>
			                 </cfif>

			                 <cfif due_date is "">
			                  Not Specified
			                 <cfelse>
			                 #dateformat(due_date,'mm/dd/yyyy')#
			                 </cfif></td>
			            </tr>

			            <tr>
			                <td class="feed_option" valign=top><b>Opportunity URL</b></td>
			                <td colspan=4 class="feed_option"><a href="#fbo.fbo_url#" target="_blank" rel="noopener" rel="noreferrer"><u><b>Click Here</b></u></a></td>
			            </tr>

                   </table>

                   </td></tr>


                     </table>


			        <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<tr><td height=5></td></tr>
					<tr><td colspan=4><hr></td></tr>

					 <cfif findnocase("Award Notice",fbo.fbo_notice_type) is 1>

						 <tr><td height=10></td></tr>
						 <tr><td class="feed_header"><b>AWARD INFORMATION</b></td></tr>
						 <tr><td height=10></td></tr>
						 <tr><td>


			        <table cellspacing=0 cellpadding=0 border=0 width=100%>


						 <tr><td class="feed_option"><b>Award Date</b></td>
						     <td class="feed_option">#dateformat(fbo.fbo_contract_award_date,'mm/dd/yyy')#</td></tr>
						 <tr><td class="feed_option"><b>Awardee Name</b></td>
						     <td class="feed_option">#fbo.fbo_contract_award_name#</td></tr>
						 <tr><td class="feed_option"><b>Awardee DUNS</b></td>

						      <td class="feed_option">

						 <cfif #fbo.fbo_contract_award_duns# is "">Not specified<cfelse>

							 <cfif left(fbo.fbo_contract_award_duns,1) is 0>

							 <cfset length = len(fbo.fbo_contract_award_duns)>
							 <cfset new_length = #evaluate(length - 1)#>
							 <cfset new_duns = right(fbo.fbo_contract_award_duns,new_length)>

								<cfif left(new_duns,1) is 0>

								 <cfset length = len(new_duns)>
								 <cfset new_length = #evaluate(length - 1)#>
								 <cfset new_duns_2 = right(new_duns,new_length)>

							      #fbo.fbo_contract_award_duns# - <a href="/exchange/include/company_profile.cfm?duns=#new_duns_2#" target="_blank" rel="noopener" rel="noreferrer"><b><u>Company Lookup</u></b></a>

                                <cfelse>

							      #fbo.fbo_contract_award_duns# - <a href="/exchange/include/company_profile.cfm?duns=#new_duns#" target="_blank" rel="noopener" rel="noreferrer"><b><u>Company Lookup</u></b></a>

								</cfif>

							 <cfelse>

							 #fbo.fbo_contract_award_duns# - <a href="/exchange/include/company_profile.cfm?duns=#fbo.fbo_contract_award_duns#" target="_blank" rel="noopener" rel="noreferrer"><b><u>Company Lookup</u></b></a>

							 </cfif>

						 </cfif>

						 </td></tr>
						 <tr><td class="feed_option" width=150><b>Award Amount</b></td>
						     <td class="feed_option"><cfif isnumeric(fbo.fbo_contract_award_amount)>#numberformat(fbo.fbo_contract_award_amount,'$999,999,999.99')#<cfelse>#fbo.fbo_contract_award_amount#</cfif></td></tr>

                         </table>
                         </td></tr>



                         <tr><td height=10></td></tr>
                         <tr><td><hr></td></t/r>
                         <tr><td height=5></td></tr>



					 </cfif>

					<cfif fbo.fbo_desc is not "">
						<tr><td height=5></td></tr>
						<tr><td colspan=4 class="feed_header"><b>OPPORTUNITY DESCRIPTION</b></td></tr>
						<tr><td height=10></td></tr>
						<tr><td colspan=4 class="feed_option">#replace(fbo.fbo_desc,"#chr(46)##chr(32)#",".<br><br>","all")#</td></tr>
	                    <tr><td><hr></td></t/r>
					</cfif>

					<tr><td height=5></td></tr>
					<tr><td colspan=4 class="feed_header"><b>ADDITIONAL REPORTING INFORMATION</b></td></tr>
					<tr><td height=10></td></tr>
					<tr><td colspan=4 class="feed_option">
					<b>Timezone:</b> #fbo.fbo_timezone#<br><br>
					#fbo.fbo_additional_reporting#
					</td></tr>

			      </table>

                 </cfoutput>

                    </td><td width=50>&nbsp;</td><td valign=top width=350>

			        <table cellspacing=0 cellpadding=0 border=0 width=100%>

                     <tr><td height=10></td></tr>
						<tr><td class="feed_header" colspan=2>OPPORTUNITY HISTORY</td><td align=right></td></tr>
						<tr><td height=10></td></tr>

			         <cfoutput query="history">
			           <tr><td class="feed_option" valign=top width=80>#dateformat(fbo_pub_date,'mm/dd/yyyy')# - </td>
			               <td class="feed_option" valign=top><a href="/exchange/opps/opp_detail.cfm?fbo_id=#fbo_id#"><b>#fbo_notice_type#</b></a></td></tr>
			           <tr><td colspan=2><hr></td></tr>
			         </cfoutput>

			         </table>

			</td></tr>

			</table>


 	  </div>

 	  </td></tr>

 </table>


<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>