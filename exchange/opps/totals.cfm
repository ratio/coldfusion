<!--- FBO Count --->

<cfquery name="fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(distinct(fbo_solicitation_number)) as total from fbo
</cfquery>

<!--- Future Needs --->

<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(need_id) as total from need
 where need_public = 1
</cfquery>

<!--- Market Challengess --->

<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(challenge_id) as total from challenge
 where challenge_public = 1
</cfquery>

<!--- SBIR --->

<cfquery name="sbir" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(id) as total from opp_sbir
</cfquery>

<!--- Grants --->

<cfquery name="grants" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(opp_grant_id) as total from opp_grant
</cfquery>