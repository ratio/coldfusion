<cfinclude template="/exchange/security/check.cfm">


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<!--- Lookups --->

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(opp_dept) from opp
 order by opp_dept
</cfquery>

<cfquery name="saved" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_search
 where opp_search_usr_id = #session.usr_id#
 order by opp_search_name
</cfquery>

<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(opp_setaside) from opp
 order by opp_setaside
</cfquery>

<cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(opp_id) as total from opp
 where opp_title = 'SOURCES SOUGHT' or opp_title = 'SOLICITATIONS'
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Opportunity Summary</td>
         <tr><td height=10></td></tr>
        </table>

		<cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select count(opp_id) as total, opp_naics, naics_code_description from opp
		 left join naics on naics_code = opp_naics
		 where (opp_title = 'SOLICITATIONS' or opp_title = 'SOURCES SOUGHT')
         group by opp_naics, naics_code_description
		 order by naics_code_description
		</cfquery>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr><td colspan=3 class="feed_header">Opportunity Summary</td></tr>
             <tr><td height=10></td></tr>
             <tr><td class="feed_option" colspan=5>The following information summarizes Opportunities by NAICS Code, Product, or Services the Federal Government has requested.</td></tr>
             <tr><td height=20></td></tr>

             <tr><td valign=top>

                 <table cellspacing=0 cellpadding=0 border=0 width=100%>

                  <tr><td class="feed_header" colspan=3>By NAICS Code</td></tr>

                  <tr>
                      <td class="feed_option"><b>Code</b></td>
                      <td class="feed_option"><b>Description</b></td>
                      <td class="feed_option"><b>Opps</b></td>
                  </tr>

                  <cfset counter = 0>
				  <cfoutput query="naics">
				   <cfif counter is 0>
				   <tr bgcolor="ffffff">
				   <cfelse>
				   <tr bgcolor="e0e0e0">
				   </cfif>
				       <td class="feed_option" valign=top width=75><cfif #opp_naics# is "">Unknown<cfelse>#opp_naics#</cfif></td>
				       <td class="feed_option" valign=top width=400><a href="cat_opps.cfm?t=1&code=#opp_naics#"><cfif #naics_code_description# is "">Unknown<cfelse>#naics_code_description#</cfif></a></td>
					   <td class="feed_option" valign=top align=center><a href="todays_opps_detail.cfm?t=1&naics=#opp_naics#">#total#</a></td></tr>
				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				  </cfoutput>

                 </table>

                 </td><td width=50>&nbsp;</td><td valign=top>

                 <table cellspacing=0 cellpadding=0 border=0 width=100%>

                  <tr><td class="feed_header" colspan=3>By Product</td></tr>
                  <tr>
                      <td class="feed_option" width=50><b>Code</b></td>
                      <td class="feed_option"><b>Description</b></td>
                      <td class="feed_option"><b>Opps</b></td>
                  </tr>
			      <cfquery name="product" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				   select count(opp_id) as total, class_code_code, class_code_name from opp
				   left join class_code on class_code_code = opp_code
				   where class_code_category = 'Product Code' and
				         (opp_title = 'SOLICITATIONS' or opp_title = 'SOURCES SOUGHT')
                   group by opp_code, class_code_code, class_code_name
				   order by class_code_name
				  </cfquery>

                  <cfset counter = 0>
				  <cfoutput query="product">
				   <cfif counter is 0>
				   <tr bgcolor="ffffff">
				   <cfelse>
				   <tr bgcolor="e0e0e0">
				   </cfif>
				       <td class="feed_option" valign=top width=30>#class_code_code#</td>
				       <td class="feed_option" valign=top><a href="cat_opps.cfm?t=2&code=#class_code_code#">#class_code_name#</a></td>
					   <td class="feed_option" valign=top align=center><a href="todays_opps_detail.cfm?t=2&pcode=#class_code_code#">#total#</a></td></tr>
				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				  </cfoutput>

                 </table>

                 </td><td width=50>&nbsp;</td><td valign=top>

                 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				  <cfquery name="service" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				   select count(opp_id) as total, class_code_code, class_code_name from opp
				   left join class_code on class_code_code = opp_code
				   where class_code_category = 'Service Code' and
                         (opp_title = 'SOLICITATIONS' or opp_title = 'SOURCES SOUGHT')
				   group by opp_code, class_code_code, class_code_name
				   order by class_code_name
				  </cfquery>

                  <tr><td class="feed_header" colspan=3>By Service</td></tr>
                  <tr>
                      <td class="feed_option" width=50><b>Code</b></td>
                      <td class="feed_option"><b>Description</b></td>
                      <td class="feed_option"><b>Opps</b></td>
                  </tr>
                  <cfset counter = 0>
				  <cfoutput query="service">
				   <cfif counter is 0>
				   <tr bgcolor="ffffff">
				   <cfelse>
				   <tr bgcolor="e0e0e0">
				   </cfif>
				       <td class="feed_option" valign=top width=30>#class_code_code#</td>
				       <td class="feed_option" valign=top><a href="cat_opps.cfm?t=3&code=#class_code_code#">#class_code_name#</a></td>
					   <td class="feed_option" valign=top align=center><a href="todays_opps_detail.cfm?t=3&scode=#class_code_code#">#total#</a></td></tr>
				  <cfif counter is 0>
				   <cfset counter = 1>
				  <cfelse>
				   <cfset counter = 0>
				  </cfif>

				  </cfoutput>

                 </table>

                 </td></tr>
            </table>
       </tr>
   </table>






        </form>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>