<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(snapshot_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

<cfif button is "Add">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into snapshot
	  (
	   snapshot_name,
	   snapshot_keyword,
	   snapshot_hub_id,
	   snapshot_usr_id
	  )
	  values
	  (
	  '#snapshot_name#',
	  '#search_string#',
	   #session.hub#,
	   #session.usr_id#
	  )
	</cfquery>

	<cflocation URL="dashboard.cfm?u=1" addtoken="no">

<cfelseif button is "Save">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update snapshot
	  set snapshot_name = '#snapshot_name#',
	      snapshot_keyword = '#search_string#'
	  where snapshot_id = #snapshot_id# and
	        snapshot_hub_id = #session.hub# and
	        snapshot_usr_id = #session.usr_id#
	</cfquery>

	<cflocation URL="dashboard.cfm?u=2" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete snapshot
	  where snapshot_id = #snapshot_id# and
	        snapshot_hub_id = #session.hub# and
	        snapshot_usr_id = #session.usr_id#
    </cfquery>

	<cflocation URL="dashboard.cfm?u=3" addtoken="no">

</cfif>