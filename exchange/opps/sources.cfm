<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="sources" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp
 where opp_title = 'SOURCES SOUGHT'
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Opportunities</td>
             <td class="feed_header" align=right></td></tr>
         <tr><td class="feed_option" colspan=2>The Exchange contains over <b>23 million</b> Federal contract awards from <b>2013 - 2018</b>.  To view awards please create a custom view or select from one of our standard views or search options.</td></tr>
         <tr><td>&nbsp;</td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_header" colspan=10>Sources Sought</td></tr>
         <tr><td height=10></td></tr>

         <tr>
             <td class="feed_option">Description</td>
         </tr>


         <cfoutput query="sources">
          <tr><td class="feed_option">#opp_solicitation_number#</td></tr>
         </cfoutput>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>