<cfinclude template="/exchange/security/check.cfm">

<cfset session.view = 1>

<cfif not isdefined("sv")>
 <cfset sv = 70>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<!--- Lookups --->

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select id, solicitationnumber, department, agency, title, description, keywords, technologyarea, objective, opendate, applicationduedate, closedate, url, orgname_logo, source from opp_sbir
   left join orgname on orgname_name = agency
   where id > 0

   <cfif #session.sbir_status# is not 1>
    <cfif #session.sbir_status# is 2>
     and closedate >= #now()#
    <cfelseif #session.sbir_status# is 3>
     and closedate <= #now()#
    </cfif>
   </cfif>

   <cfif #session.sbir_agency# is not 0>
    and agency = '#session.sbir_agency#'
   </cfif>

   <cfif #session.sbir_keyword# is not "">
	  and contains((solicitationnumber,title,objective, description, phasei, phaseii, phaseiii, keywords ),'#trim(session.sbir_keyword)#')
   </cfif>

   order by closedate DESC

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template = "/exchange/include/header.cfm">


  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="/exchange/opps/sbir_sites.cfm">
      </td><td valign=top>

      <cfoutput>

           <div class="tab_not_active" style="margin-left: 20px;">
            <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/dashboard.cfm">Dashboard</a></span>
           </div>


          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/">PROCUREMENTS <cfif #session.fbo_total# GT 0>(#trim(numberformat(session.fbo_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/grants.cfm">GRANTS <cfif #session.grants_total# GT 0>(#trim(numberformat(session.grants_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_light.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/sbir.cfm">SBIR/STTRs <cfif #session.sbir_total# GT 0>(#trim(numberformat(session.sbir_total,'999,999'))#)</cfif></a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/needs.cfm">NEEDS</a> <cfif #session.needs_total# GT 0>(#trim(numberformat(session.needs_total,'999,999'))#)</cfif></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/challenges.cfm">CHALLENGES <cfif #session.challenges_total# GT 0>(#trim(numberformat(session.challenges_total,'999,999'))#)</cfif></a></span>
          </div>

      </cfoutput>

	  <div class="main_box_2">
		<cfinclude template="/exchange/opps/search_opportunities_sbir.cfm">
	  </div>

      <div class="main_box">

      <cfif agencies.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
 		  <tr><td class="feed_header">Search Results</td></tr>
          <tr><td height=5></td></tr>
          <tr><td colspan=3><hr></td></tr>
          <tr><td height=5></td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No SBIR/STTR opportunities were found.</td></tr>
        </table>

      <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
			 <tr><td class="feed_header">Search Results (#agencies.recordcount#)</td>
				 <td class="feed_sub_header" align=right>
				 <a href="/exchange/opps/results_sbir.cfm?export=1"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" title="Export to Excel"></a>
				 <a href="/exchange/opps/results_sbir.cfm?export=1">Export to Excel</a>
				 </td></tr>
		 </cfoutput>
         <tr><td height=5></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td height=5></td></tr>


       </table>

       <!--- Show Opportunities --->

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
             <td colspan=2></td>
             <td class="feed_sub_header">Title / Description</td>
             <td align=right class="feed_sub_header">Close Date</td>
             <td width=50>&nbsp;</td>
          </tr>

        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfoutput query="agencies">

             <cfif agencies.source is "HHS">

             <tr><td width=100>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

					 <cfif agencies.orgname_logo is "">
					  <tr><td><a href="/exchange/include/sbir_hhs.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top height=80 border=0 vspace=10></a></td></tr>
					 <cfelse>
					  <tr><td><a href="/exchange/include/sbir_hhs.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#agencies.orgname_logo#" align=top width=80 border=0 vspace=10></a></td></tr>
					 </cfif>

				 </table>

                 </td><td width=20>&nbsp;</td>

                 <td colspan=2>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr>
					 <td class="feed_sub_header" valign=top><b><a href="/exchange/include/sbir_hhs.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(agencies.department)# - <cfif agencies.agency is "" or agencies.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(agencies.agency)#</cfif></a></td>
					 <td class="feed_sub_header" align=right width=100>#dateformat(agencies.closedate,'mm/dd/yyyy')#</td>
 					 <td align=right width=50><img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.id#&t=sbir','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"></td>

				 </tr>

				 <tr>

				 <td class="feed_option" style="font-weight: normal;" colspan=2>

                 <cfif agencies.description is not "">
					 <cfif len(agencies.description) GT 1000>
					  #left(replaceNoCase(agencies.description,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all"),1000)#...
					 <cfelse>
					  #replaceNoCase(agencies.description,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
					 </cfif>
				 <cfelse>

				  <cfif agencies.title is not "">
					 #replaceNoCase(agencies.title,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
                  <cfelse>
				 	 Description not provided.
				  </cfif>
				 </cfif>

				 </td>
			 </tr>

			 <cfif agencies.keywords is not "">
				 <tr>
					<td class="link_small_gray" colspan=2><b>KEYWORDS: </b>

					#ucase(replaceNoCase(agencies.keywords,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all"))#

					</td>
				 </tr>
			 </cfif>

             </td></tr>

             </table>

			 <tr><td colspan=4><hr></td></tr>


             <cfelseif agencies.source is "DOD">

             <tr><td>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					 <cfif agencies.orgname_logo is "">
					  <tr><td><a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top height=80 border=0 vspace=10></a></td></tr>
					 <cfelse>
					  <tr><td><a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#image_virtual#/#agencies.orgname_logo#" align=top width=80 border=0 vspace=10></a></td></tr>
					 </cfif>
				 </table>

                 </td><td width=20>&nbsp;</td>

                 <td colspan=2>

				 <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr>
					 <td class="feed_sub_header" valign=top><b><a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">DEPARTMENT OF DEFENSE - <cfif agencies.agency is "" or agencies.agency is "N/A">NOT SPECIFIED<cfelse>#ucase(agencies.agency)#</cfif></a></td>
						 <td class="feed_sub_header" align=right width=100>#dateformat(agencies.closedate,'mm/dd/yyyy')#</td>
 					 <td align=right width=50><img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.id#&t=sbir','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"></td>

				 </tr>

				 <tr>

                 <tr>
                    <td class="feed_option"><b>
                     <a href="/exchange/include/sbir_dod.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(replaceNoCase(agencies.title,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all"))#</a>
                    </td>
                 </tr>


				 <td class="feed_option" style="font-weight: normal;" colspan=2>

                 <cfif agencies.description is not "">
					 <cfif len(agencies.description) GT 1000>
					  #left(replaceNoCase(agencies.description,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all"),1000)#...
					 <cfelse>
					  #replaceNoCase(agencies.description,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
					 </cfif>
				 <cfelse>
				  Description not provided.
				 </cfif>

				 </td>
			 </tr>

			 <cfif agencies.keywords is not "">
				 <tr>
					<td class="link_small_gray" colspan=2><b>KEYWORDS: </b>
					#ucase(replaceNoCase(agencies.keywords,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all"))#
					</td>
				 </tr>
			 </cfif>

             </td></tr>

             </table>

             <tr><td height=10></td></tr>
			 <tr><td colspan=4><hr></td></tr>
             <tr><td height=10></td></tr>

            </cfif>

            </cfoutput>

        </table>

      </cfif>

	  </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>