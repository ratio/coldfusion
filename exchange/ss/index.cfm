<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="ss" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from ss
 where ss_hub_id = #session.hub#
 order by ss_order
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td></tr>
       <tr><td><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

      <cfinclude template="menu.cfm">

      </td><td width=40>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=15></td></tr>
		   <tr><td class="feed_header">We're Here to Help</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">
		   Booz Allen Partner Support Teams are here to help you in your process of engaging Partners and understanding existing relationships.  Explore the options below to learn more.
           </td></tr>

		   <tr><td><hr></td></tr>

		   <tr><td height=10></td></tr>

		   <cfif isdefined("u")>
		    <tr><td class="feed_sub_header" style="color: green;">Your support request has been sent.</td></tr>
		    <tr><td height=10></td></tr>
		   </cfif>

		  </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfoutput query="ss">
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;">

				<cfif ss_email is not "">
				 <a href="request.cfm?i=#ss_id#">#ss_name#</a>
				<cfelse>
				 <a href="#ss_url#" target="_blank" rel="noopener">#ss_name#</a>
				</cfif>

				</td></tr>
			</cfoutput>

			<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;">
             <a href="request_awg.cfm">Alliance Working Group (AWG)</a></td></tr>

			<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;">
             <a href="request_explore.cfm">Explore Media Engagement (Press Releases, Bylines, Interview, etc.)</a></td></tr>


			<tr><td height=10></td></tr>

			</table>


      </td></tr>

      </table>

      </div>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>