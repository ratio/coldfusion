  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header">Request a Non-Disclosure Agreement (NDA)</td></tr>
   <tr><td class="feed_sub_header" style="font-weight: normal;">
	A non-disclosure agreement (NDA) is a legal contract between parties that outlines confidential material, knowledge, or information that the parties wish to share with one another for certain purposes, but wish to restrict access to by third parties.  It is a contract through which the parties agree not to disclose information covered by the agreement and creates a confidential relationship between the parties to protect any type of confidential and proprietary information or trade secrets.  An NDA serves to protect non-public business information.

   </td></tr>

   <tr><td><hr></td></tr>

   <tr><td height=10></td></tr>

   <form action="submit.cfm" method="post">

	<cfif isdefined("u")>
	 <tr><td class="feed_sub_header" style="color: green;">Your request has been successfully sent.</td></tr>
	</cfif>

	<tr><td class="feed_sub_header">Name</td></tr>
	<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_name" class="input_text" style="width: 300px;" required></td></tr>

	<tr><td class="feed_sub_header">Email Address</td></tr>
	<tr><td><input type="email" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_email" class="input_text" style="width: 300px;" required></td></tr>

	<tr><td class="feed_sub_header">Partner Name (if known)</td></tr>
	<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_partner" class="input_text" style="width: 700px;"></td></tr>

	<tr><td class="feed_sub_header">Client or Account</td></tr>
	<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_client" class="input_text" style="width: 700px;"></td></tr>

	<tr><td class="feed_sub_header">More Information</td></tr>
	<tr><td><textarea name="support_message" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  style="width: 700px; height: 100px;"></textarea></td></tr>

	<tr><td height=10></td></tr>
	<tr><td><input type="submit" name="button" class="button_blue_large" value="Submit"></td></tr>

	<input type="hidden" name="support_area" value="Request a Non-Disclosure Agreement">

   </form>

  </table>

