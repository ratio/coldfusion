<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="support" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from ss
 where ss_hub_id = #session.hub# and
       ss_id = #i#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td>
           <td class="feed_sub_header" align=right><a href="index.cfm">More Support Services</a></td></tr>
       <tr><td colspan=2><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

      <cfinclude template="menu.cfm">

      </td><td width=40>&nbsp;</td><td valign=top>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=15></td></tr>

           <cfoutput>

		   <tr><td class="feed_header">#support.ss_name#</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">#support.ss_desc#</td></tr>

           </cfoutput>

		   <tr><td><hr></td></tr>

		   <tr><td height=10></td></tr>

		  </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <form action="submit.cfm" method="post">

				<tr><td class="feed_sub_header">Name</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_name" class="input_text" style="width: 300px;" required></td></tr>

				<tr><td class="feed_sub_header">Email Address</td></tr>
				<tr><td><input type="email" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_email" class="input_text" style="width: 300px;" required></td></tr>

				<tr><td class="feed_sub_header">Partner Name</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_partner" class="input_text" style="width: 700px;"></td></tr>

				<tr><td class="feed_sub_header">Client or Account</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_client" class="input_text" style="width: 700px;"></td></tr>

				<tr><td class="feed_sub_header">More Information</td></tr>
				<tr><td><textarea name="support_message" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  style="width: 700px; height: 100px;"></textarea></td></tr>

				<tr><td height=10></td></tr>
				<tr><td><hr></td></tr>
				<tr><td height=15></td></tr>

				<tr><td><input type="submit" name="button" class="button_blue_large" value="Submit"></td></tr>

                <cfoutput>
					<input type="hidden" name="i" value=#i#>
				</cfoutput>

			   </form>

			</table>


      </td></tr>

      </table>

      </div>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>