  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header">Request a Reseller Agreement</td></tr>
   <tr><td class="feed_sub_header" style="font-weight: normal;">
	Have you developed a mission solution that can be applied in the commercial sector?  Requesting a resale will trigger a review process by which a Booz Allen team will consider your request to deploy and/or modify this solution in the enterprise market.
   </td></tr>

   <tr><td><hr></td></tr>

   <tr><td height=10></td></tr>

   <form action="submit.cfm" method="post">

	<cfif isdefined("u")>
	 <tr><td class="feed_sub_header" style="color: green;">Your request has been successfully sent.</td></tr>
	</cfif>

	<tr><td class="feed_sub_header">Name</td></tr>
	<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_name" class="input_text" style="width: 300px;" required></td></tr>

	<tr><td class="feed_sub_header">Email Address</td></tr>
	<tr><td><input type="email" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_email" class="input_text" style="width: 300px;" required></td></tr>

	<tr><td class="feed_sub_header">Partner Name (if known)</td></tr>
	<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_partner" class="input_text" style="width: 700px;"></td></tr>

	<tr><td class="feed_sub_header">Client or Account</td></tr>
	<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_client" class="input_text" style="width: 700px;"></td></tr>

	<tr><td class="feed_sub_header">More Information</td></tr>
	<tr><td><textarea name="support_message" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  style="width: 700px; height: 100px;"></textarea></td></tr>

	<tr><td height=10></td></tr>
	<tr><td><input type="submit" name="button" class="button_blue_large" value="Submit"></td></tr>

	<input type="hidden" name="support_area" value="Request Reseller Agreement">

   </form>

  </table>

