<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td>
           <td class="feed_sub_header" align=right><a href="index.cfm">More Support Services</a></td></tr>
       <tr><td colspan=2><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

      <cfinclude template="menu.cfm">

      </td><td width=40>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=15></td></tr>

           <cfoutput>

		   <tr><td class="feed_header">Explore Media Engagement</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">If you are interested in exploring how we can leverage an existing partner relationship for earned media engagement, please fill out the following form.  There are a variety of ways we can promote the firm�s partnership work which might include a press release, a piece of thought leadership content, or positioning executives for joint media interviews.  Tell us more about the nature of the partnership, its timeliness, and its impact.</td></tr>
		   <tr><td height=10></td></tr>

           </cfoutput>

		  </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <form action="submit_explore.cfm" method="post">

				<tr><td class="feed_sub_header">Name</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_name" class="input_text" style="width: 300px;" required></td></tr>

				<tr><td class="feed_sub_header">Email Address</td></tr>
				<tr><td><input type="email" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_email" class="input_text" style="width: 300px;" required></td></tr>

				<tr><td class="feed_sub_header">Partner Name</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_partner" class="input_text" style="width: 700px;"></td></tr>

				<tr><td class="feed_sub_header">Partner Media Contact</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_media_contact" class="input_text" style="width: 700px;"></td></tr>

				<tr><td class="feed_sub_header">Client or Account</td></tr>
				<tr><td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="support_client" class="input_text" style="width: 700px;"></td></tr>

				<tr><td class="feed_sub_header">About the Partnership/Why is it Newsworthy</td></tr>
				<tr><td><textarea name="support_message" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  style="width: 700px; height: 100px;"></textarea></td></tr>

				<tr><td height=10></td></tr>
				<tr><td><hr></td></tr>
				<tr><td height=15></td></tr>

				<tr><td><input type="submit" name="button" class="button_blue_large" value="Submit"></td></tr>

			   </form>

			</table>




      </td></tr>

      </table>

      </div>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>