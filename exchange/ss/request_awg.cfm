<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td>
           <td class="feed_sub_header" align=right><a href="index.cfm">More Support Services</a></td></tr>
       <tr><td colspan=2><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

      <cfinclude template="menu.cfm">

      </td><td width=40>&nbsp;</td><td valign=top>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=15></td></tr>

           <cfoutput>

		   <tr><td class="feed_header">Alliance Working Group (AWG)</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">Partnership/Alliance agreements enable the firm to collaborate with other companies to bring new, evolving/emerging, or innovative solutions to our clients. These agreements come in various forms, which set the roles and obligations of the firm and its partner and governs the overall relationship. </td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td class="feed_sub_header"><i>Strategic†Alliances, Collaborations, Partnerships, Cobranding, and Resellers that are handled by this team are for firmwide capability offerings only. If you have a need to engage another company for an opportunity-specific engagement, please reach out to the appropriate Contracts or Subcontracts professional supporting that specific opportunity.</i></td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">Please select this portal link to access the ticketing support system to ensure that partnerships/alliances you want to enter into are in compliance with Booz Allenís Risk Matrix.</td></tr>
		   <tr><td height=10></td></tr>
		   <tr><td><hr></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td><input type="submit" name="button" class="button_blue_large" value="AWG Ticketing Support System"></td></tr>

           </cfoutput>


		   <tr><td height=10></td></tr>

		  </table>

      </td></tr>

      </table>

      </div>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>