<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_header">Contract Competitive Analysis</td>
               <td align=right class="feed_option"><a href="results.cfm?number=#number#">Return</a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=5></td></tr>
          </table>

          </cfoutput>

           <!--- Get Contract Information --->

  		   <cfquery name="contract" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data
			where id = #id#
		   </cfquery>

           <cfif not isdefined("session.comp_agency")>

             <cfset #session.comp_agency# = '#contract.awarding_sub_agency_code#'>
             <cfset #session.comp_office# = '#contract.awarding_office_code#'>
             <cfset #session.comp_naics# = '#contract.naics_code#'>
             <cfset #session.comp_psc# = '#contract.product_or_service_code#'>
             <cfset #session.comp_cstatus# = 0>

           </cfif>

  		   <cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(id) as mods, sum(federal_action_obligation) as base, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as ceiling from award_data
			where award_id_piid = '#contract.award_id_piid#'
		   </cfquery>

           <cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall"><b>CONTRACT NUMBER</b></td>
						<td class="text_xsmall">#contract.award_id_piid#</td></tr>
					<tr><td class="text_xsmall" width=150><b>VENDOR</b></td>
						<td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#contract.recipient_name#</a>&nbsp;-&nbsp;<a href="/exchange/include/award_dashboard.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">Company Footprint</a></td></tr>
					<tr><td class="text_xsmall"><b>PARENT CONTRACT ##</b></td>
						<td class="text_xsmall">#contract.parent_award_id#</td></tr>
					<tr><td class="text_xsmall"><b>AWARD & MODS</b></td>
						<td class="text_xsmall">#totals.mods#</td></tr>
					<tr><td class="text_xsmall"><b>NAICS</b></td>
						<td class="text_xsmall">#contract.naics_code# - #contract.naics_description#</td></tr>
					<tr><td class="text_xsmall"><b>PSC</b></td>
						<td class="text_xsmall">#contract.product_or_service_code# - #contract.product_or_service_code_description#</td></tr>
                </table>

                </td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.awarding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AGENCY</b></td>
						<td class="text_xsmall">#contract.awarding_sub_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AWARDING OFFICE</b></td>
						<td class="text_xsmall">#contract.awarding_office_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.funding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING AGENCY</b></td>
						<td class="text_xsmall">#contract.funding_sub_agency_name#</td></tr>

					<tr><td class="text_xsmall"><b>FUNDING OFFICE</b></td>
						<td class="text_xsmall">#contract.funding_office_name#</td></tr>
                </table>

                </td></td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>BASE</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base,'$999,999,999.99')#</td></tr>
					<tr><td class="text_xsmall"><b>OPTIONS</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.options,'$999,999,999.99')#</td></tr>
					<tr><td class="text_xsmall"><b>CEILING</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base + totals.options,'$999,999,999.99')#</td></tr>
					<tr><td class="text_xsmall"><b>POP START</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_start_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP CURRENT END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_current_end_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP POTENTIAL END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_potential_end_date,'mm/dd/yyyy')#</td></tr>

                </table>

                </td></tr>

                <tr><td height=5></td></tr>

                <tr><td colspan=3 class="text_xsmall"><b>Award Description</b></td></tr>
                <tr><td colspan=3 class="text_xsmall">#contract.award_description#</td></tr>

                <tr><td height=5></td></tr>

          <tr><td colspan=3><hr></td></tr>

          </table>

          </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td class="feed_header">Determine Your Competition</td></tr>
           <tr><td class="feed_option" colspan=4>There are multiple ways to determine the competitive landscape around this contract.  We provide you the tools to slice and dice data to get a market analysis around this contract.  By default, we have created an initial list of competitors based on information from the contract you selected.</td></tr>
           <tr><td height=10></td></tr>
		  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr>
				   <td class="feed_option"><b>Department</b></td>
				   <td class="feed_option"><b>Agency</b></td>
				   <td class="feed_option"><b>Office</b></td>
				   <td class="feed_option"><b>NAICS Code *</b></td>
				   <td class="feed_option"><b>PSC Code *</b></td>
				   <td class="feed_option"><b>Contract Status</b></td>
			   </tr>

               <form action="refresh.cfm" method="post">

			   <tr>
			       <td class="feed_option"><cfoutput>#contract.awarding_agency_name#</cfoutput></td>

				   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					select distinct(awarding_sub_agency_code), awarding_sub_agency_name from award_data
					where (action_date > '10/1/2016' and action_date <= '9/30/2018') and
					       awarding_agency_code = '#contract.awarding_agency_code#'
					order by awarding_sub_agency_name
				   </cfquery>

			       <td class="feed_option">
			       <select name="agency">
			       <option value=0>ALL AGENCIES
			       <cfoutput query="agencies">
			        <option value='#agencies.awarding_sub_agency_code#' <cfif #agencies.awarding_sub_agency_code# is #session.comp_agency#>selected</cfif>>#awarding_sub_agency_name#
			       </cfoutput>

			       </td>

				   <cfquery name="office" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					select distinct(awarding_office_code), awarding_office_name from award_data
					where (action_date > '10/1/2016' and action_date <= '9/30/2018') and
					      awarding_agency_code = '#contract.awarding_agency_code#' and
					      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#'
					order by awarding_office_name
				   </cfquery>

			       <td class="feed_option">
			       <select name="office">
			       <option value=0>ALL OFFICES
			       <cfoutput query="office">
			        <option value='#office.awarding_office_code#' <cfif #office.awarding_office_code# is #session.comp_office#>selected</cfif>>#office.awarding_office_name#
			       </cfoutput>

			       </td>

			       <cfoutput>

                   <td><input type="text" name="naics" size=6 maxlength="6" <cfif #session.comp_naics# is 0>value=""<cfelse>value="#session.comp_naics#"</cfif>></td>
                   <td><input type="text" name="psc" size=4 maxlength="4" <cfif #session.comp_psc# is 0>value=""<cfelse>value="#session.comp_psc#"</cfif>></td>

                   </cfoutput>

			       <td class="feed_option">
			       <select name="contract_status">
			       <option value=0 <cfif #session.comp_cstatus# is 0>selected</cfif>>All CONTRACTS
			       <option value=1 <cfif #session.comp_cstatus# is 1>selected</cfif>>EXPIRED CONTRACTS
			       <option value=2 <cfif #session.comp_cstatus# is 2>selected</cfif>>CURRENT CONTRACTS

			       </td>

                   <td align=right><input class="button_blue" style="width:60px; height:20px;" type="submit" name="button" value="Refresh">                   </td>

			   </tr>
			   <tr><td colspan=7 align=right class="text_xsmall"><b>*</b>&nbsp; - leave blank to search all NAICS or PSC codes</td></tr>
               <tr><td colspan=7><hr></td></tr>

			  </table>

              <cfoutput>
               <input type="hidden" name="id" value="#id#">
               <input type="hidden" name="number" value="#number#">
              </cfoutput>

              </form>

			  <cfquery name="competition" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from award_data
				where (action_date > '10/1/2016' and action_date <= '9/30/2018') and
				awarding_agency_code = '#contract.awarding_agency_code#' and

				      <cfif session.comp_agency is not 0>
						awarding_sub_agency_code = '#session.comp_agency#' and
				      </cfif>

				      <cfif session.comp_office is not 0>
				        awarding_office_code = '#session.comp_office#' and
				      </cfif>

				      <cfif session.comp_naics is not 0>
				        naics_code = '#session.comp_naics#' and
				      </cfif>

				      <cfif session.comp_cstatus is not 0>

				        <cfif session.comp_cstatus is 1>
                          period_of_performance_potential_end_date < #now()# and
				        <cfelse>
				          period_of_performance_potential_end_date > #now()# and
				        </cfif>

				      </cfif>


				      <cfif session.comp_psc is not 0>
				       product_or_service_code = '#session.comp_psc#' and
				      </cfif>

				      award_id_piid <> '#contract.award_id_piid#'
				order by action_date DESC
			  </cfquery>

              <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr><td height=5></td></tr>

               <cfif #competition.recordcount# is 0>

               <tr><td class="text_xsmall">No contracts or mods were found.  Please try to modify some of your competitive filters above to expand your market.</td></tr>

               <cfelse>

               <tr><td colspan=9 class="text_xsmall"><b>Total contracts and mods found - <cfoutput>#competition.recordcount#</cfoutput></b></td></tr>
               <tr><td height=10></td></tr>

               <cfset counter = 0>
               <cfset tot = 0>

               <tr>
                  <td class="text_xsmall"><b>Award Date</b></td>
                  <td class="text_xsmall"><b>Contract ID</b></td>
                  <td class="text_xsmall"><b>MOD</b></td>
                  <td class="text_xsmall"><b>Vendor</b></td>
                  <td class="text_xsmall"><b>Award Description</b></td>
                  <td class="text_xsmall" align=center width-75><b>NAICS</b></td>
                  <td class="text_xsmall" align=center width=50><b>PSC</b></td>
                  <td class="text_xsmall" width=75><b>Start Date</b></td>
                  <td class="text_xsmall" width-75><b>End Date</b></td>
                  <td class="text_xsmall" align=right><b>Obligation</b></td>
               </tr>

               <cfoutput query="competition">

               <cfif counter is 0>
                <tr bgcolor="ffffff">
               <cfelse>
                <tr bgcolor="e0e0e0">
               </cfif>

               <td class="text_xsmall">#dateformat(action_date,'mm/dd/yyyy')#</td>
                   <td class="text_xsmall"><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
                   <td class="text_xsmall">#modification_number#</td>
                   <td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
                   <td class="text_xsmall">#left(award_description,100)#...</td>
                   <td class="text_xsmall" align=center>#naics_code#</td>
                   <td class="text_xsmall" align=center>#product_or_service_code#</td>
                   <td class="text_xsmall">#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
                   <td class="text_xsmall">#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
                   <td class="text_xsmall" align=right>#numberformat(federal_action_obligation,'$9,999,999')#</td>
                </tr>

                <cfif counter is 0>
                 <cfset counter = 1>
                <cfelse>
                 <cfset counter = 0>
                </cfif>

                <cfset tot = tot + #federal_action_obligation#>

               </cfoutput>

               <tr><td class="feed_option" colspan=9><b>Total Spend</b></td>
                   <td class="feed_option" align=right><b><cfoutput>#numberformat(tot,'$9,999,999')#</cfoutput></b></td></tr>

               </cfif>

              </table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>