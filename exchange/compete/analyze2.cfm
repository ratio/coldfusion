<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">COMPETITOR ANALYSIS</td>
               <td align=right class="feed_option"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
           <tr><td height=5></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=5></td></tr>
          </table>

          </cfoutput>

           <!--- Get Contract Information --->

  		   <cfquery name="contract" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data
			where id = #id#
		   </cfquery>

           <cfif not isdefined("session.comp_naics")>
            <cfset #session.comp_naics# = '#contract.naics_code#'>
           </cfif>

           <cfif not isdefined("session.comp_psc")>
            <cfset #session.comp_psc# = '#contract.product_or_service_code#'>
           </cfif>

           <cfif not isdefined("session.comp_cstatus")>
            <cfset #session.comp_cstatus# = 3>
           </cfif>

  		   <cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(id) as mods, sum(federal_action_obligation) as base, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as ceiling from award_data
			where award_id_piid = '#contract.award_id_piid#' and
			      recipient_duns = '#contract.recipient_duns#'

		   </cfquery>

           <cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall"><b>AWARD NUMBER</b></td>
						<td class="text_xsmall">#contract.award_id_piid#</td></tr>
					<tr><td class="text_xsmall" width=150><b>VENDOR</b></td>
						<td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#contract.recipient_name#</a>&nbsp;-&nbsp;<a href="/exchange/include/award_dashboard.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">Company Footprint</a></td></tr>
					<tr><td class="text_xsmall"><b>PARENT CONTRACT ##</b></td>
						<td class="text_xsmall">#contract.parent_award_id#</td></tr>
					<tr><td class="text_xsmall"><b>AWARD & MODS</b></td>
						<td class="text_xsmall">#totals.mods#</td></tr>
					<tr><td class="text_xsmall"><b>NAICS</b></td>
						<td class="text_xsmall">#contract.naics_code# - #contract.naics_description#</td></tr>
					<tr><td class="text_xsmall"><b>PSC</b></td>
						<td class="text_xsmall">#contract.product_or_service_code# - #contract.product_or_service_code_description#</td></tr>
                </table>

                </td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.awarding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AGENCY</b></td>
						<td class="text_xsmall">#contract.awarding_sub_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AWARDING OFFICE</b></td>
						<td class="text_xsmall">#contract.awarding_office_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.funding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING AGENCY</b></td>
						<td class="text_xsmall">#contract.funding_sub_agency_name#</td></tr>

					<tr><td class="text_xsmall"><b>FUNDING OFFICE</b></td>
						<td class="text_xsmall">#contract.funding_office_name#</td></tr>
                </table>

                </td></td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>TOTAL CONTRACT BASE</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base,'$999,999,999')#</td></tr>
					<tr><td class="text_xsmall"><b>TOTAL CONTRACT OPTIONS</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.options,'$999,999,999')#</td></tr>
					<tr><td class="text_xsmall"><b>TOTAL CONTRACT CEILING</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base + totals.options,'$999,999,999')#</td></tr>
					<tr><td class="text_xsmall"><b>POP START</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_start_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP CURRENT END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_current_end_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP POTENTIAL END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_potential_end_date,'mm/dd/yyyy')#</td></tr>

                </table>

                </td></tr>

                <tr><td height=5></td></tr>

                <tr><td colspan=3 class="text_xsmall"><b>Award Description</b></td></tr>
                <tr><td colspan=3 class="text_xsmall">#contract.award_description#</td></tr>

                <tr><td height=5></td></tr>

          <tr><td colspan=3><hr></td></tr>

          </table>

          </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header">COMPETITION OPTIONS</td></tr>
           <tr><td class="feed_option">To determine competition, we analyzed the selected contract you entered compared it to Federal Awards within the same Department, Agency, Awarding Office, Funding Office and contract NAICS and PSC codes.   To analyze other contracts, please change the NAICS and PSC.</td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		   <!--- Primes --->

  		   <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as total, sum(federal_action_obligation) as amount from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      product_or_service_code = '#contract.product_or_service_code#' and
			      recipient_duns <> '#contract.recipient_duns#'

            <cfif #session.comp_cstatus# is 3>
                  and period_of_performance_current_end_date > #now()#
            <cfelseif #session.comp_cstatus# is 2>
                  and period_of_performance_current_end_date < #now()#
            </cfif>

		   </cfquery>

  		   <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as total, sum(federal_action_obligation) as amount from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      product_or_service_code = '#contract.product_or_service_code#' and
			      recipient_duns <> '#contract.recipient_duns#'

            <cfif #session.comp_cstatus# is 3>
                  and period_of_performance_current_end_date > #now()#
            <cfelseif #session.comp_cstatus# is 2>
                  and period_of_performance_current_end_date < #now()#
            </cfif>

		   </cfquery>

  		   <cfquery name="office" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as total, sum(federal_action_obligation) as amount from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      product_or_service_code = '#contract.product_or_service_code#' and
			      awarding_office_code = '#contract.awarding_office_code#' and
			      recipient_duns <> '#contract.recipient_duns#'

            <cfif #session.comp_cstatus# is 3>
                  and period_of_performance_current_end_date > #now()#
            <cfelseif #session.comp_cstatus# is 2>
                  and period_of_performance_current_end_date < #now()#
            </cfif>

		   </cfquery>

  		   <cfquery name="funding" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as total, sum(federal_action_obligation) as amount from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      product_or_service_code = '#contract.product_or_service_code#' and
			      awarding_office_code = '#contract.awarding_office_code#' and
			      funding_office_code = '#contract.funding_office_code#'


            <cfif #session.comp_cstatus# is 3>
                  and period_of_performance_current_end_date > #now()#
            <cfelseif #session.comp_cstatus# is 2>
                  and period_of_performance_current_end_date < #now()#
            </cfif>

		   </cfquery>

		  <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_option" width=100><b>NAICS Code</b></td>
				 <td class="feed_option" width=100><b>PSC Code</b></td>
				 <td class="feed_option" width=200><b>Contract Status</b></td>
			  </tr>

              <form action="refresh.cfm" method="post">

              <tr><td height=5></td></tr>
			  <tr>
				 <td><input type="text" name="naics" class="input_text" size=6 maxlength="6" value="#session.comp_naics#" required></td>
				 <td><input type="text" name="psc" class="input_text" size=6 maxlength="6" value="#session.comp_psc#" required></td>
                 <td>

			       <select name="contract_status" class="input_select" required>
			       <option value=1 <cfif #session.comp_cstatus# is 1>selected</cfif>>All CONTRACTS
			       <option value=2 <cfif #session.comp_cstatus# is 2>selected</cfif>>EXPIRED CONTRACTS
			       <option value=3 <cfif #session.comp_cstatus# is 3>selected</cfif>>CURRENT CONTRACTS

                 </td>

                 <td><input class="button_blue" type="submit" name="button" value="Refresh"></td>

                 <cfoutput>
                  <input type="hidden" name="id" value="#id#">
                  <input type="hidden" name="number" value="#number#">
                 </cfoutput>

                 </form>

			  </tr>

			  <tr><td height=10></td></tr>

			  <tr><td colspan=7><hr></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header" colspan=2>POTENTIAL COMPETITORS</td></tr>
           <tr><td class="feed_option" colspan=5>The below analysis compares the contract and filtering information above to other awards within same depatment, agency, office, and funding office of this contract.  To find out more, click on one of the links below.</td></tr>
           <tr><td height=10></td></tr>

           <tr height=20><td></td>
               <td class="feed_option" align=center><b>Department</b></td>
               <td class="feed_option" align=center><b>+ Agency</b></td>
               <td class="feed_option" align=center><b>+ Awarding Office</b></td>
               <td class="feed_option" align=center><b>+ Funding Office</b></td>
           </tr>

           <tr height=30><td class="feed_option">Potential Competitors (Primes)</td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=1&number=#number#"><b>#dept.total#</b></a></td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=2&number=#number#"><b>#agency.total#</b></a></td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=3&number=#number#"><b>#office.total#</b></a></td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=4&number=#number#"><b>#funding.total#</b></a></td>
           </tr>

           <tr height=30 bgcolor="e0e0e0"><td class="feed_option">Total Awards (Primes)</td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=1&number=#number#"><b>#numberformat(dept.amount,'$999,999,999,999')#</b></a></td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=2&number=#number#"><b>#numberformat(agency.amount,'$999,999,999,999')#</b></a></td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=3&number=#number#"><b>#numberformat(office.amount,'$999,999,999,999')#</b></a></td>
               <td class="feed_option" align=center><a href="vendors.cfm?id=#contract.id#&l=4&number=#number#"><b>#numberformat(funding.amount,'$999,999,999,999')#</b></a></td>
           </tr>

          </table>

          </cfoutput>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>