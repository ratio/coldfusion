<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"comp_naics")>
<cfset StructDelete(Session,"comp_psc")>
<cfset StructDelete(Session,"comp_cstatus")>
<cfset StructDelete(Session,"comp_size")>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

          <cfoutput>
          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">COMPETITOR ANALYSIS</td>
               <td align=right><a href="/exchange/compete/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header" colspan=4>YOU SEARCHED FOR: "#number#"</td></tr>
          </table>
          </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select distinct(recipient_duns) from award_data
             where contains((award_id_piid, parent_award_id, solicitation_identifier),'"#number#"')
		   </cfquery>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
             select id, award_description, award_id_piid, modification_number, naics_code, product_or_service_code, period_of_performance_start_date, period_of_performance_current_end_date, base_and_exercised_options_value, award_description, period_of_performance_potential_end_date, awarding_office_name, funding_office_name, action_date, federal_action_obligation, awarding_agency_name, awarding_sub_agency_name, recipient_name, recipient_duns from award_data
             where contains((award_id_piid, parent_award_id, solicitation_identifier),'"#trim(number)#"') and
             federal_action_obligation > 0

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by action_date ASC
		    <cfelseif #sv# is 10>
             order by action_date DESC
		    <cfelseif #sv# is 2>
             order by recipient_name ASC
		    <cfelseif #sv# is 20>
             order by recipient_name DESC
		    <cfelseif #sv# is 3>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 4>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 5>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 50>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 6>
             order by naics_code ASC
		    <cfelseif #sv# is 60>
             order by naics_code DESC
		    <cfelseif #sv# is 7>
             order by type_of_set_aside ASC
		    <cfelseif #sv# is 70>
             order by type_of_set_aside DESC
		    <cfelseif #sv# is 8>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 80>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 9>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 90>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 10>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 100>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 11>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 110>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 12>
             order by product_or_service_code ASC
		    <cfelseif #sv# is 120>
             order by product_or_service_code DESC
		    <cfelseif #sv# is 14>
             order by award_id_piid ASC
		    <cfelseif #sv# is 140>
             order by award_id_piid DESC
		    <cfelseif #sv# is 15>
             order by awarding_agency_name ASC
		    <cfelseif #sv# is 150>
             order by awarding_agency_name DESC
		    <cfelseif #sv# is 16>
             order by base_and_exercised_options_value ASC
		    <cfelseif #sv# is 160>
             order by base_and_exercised_options_value DESC

		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>
		  <cfset tot2 = 0>

		 <cfif agencies.recordcount is 0>

		 <tr><td class="feed_sub_header" style="font-weight: normal;">No contracts or awards were found with the number you provided. <a href="index.cfm"><b>Please try again</b></a>.</td></tr>

		 <cfelse>

 		 <cfoutput>

         <tr><td class="feed_option" colspan=14><b>Total Awards & Mods: #agencies.recordcount#</b></td>
             <td class="feed_option" align=right></td></tr>
         <tr><td colspan=15><hr></td></tr>
         <tr><td class="feed_sub_header" colspan=15>To <b>analyze competitors</b>, click on the Analyze link next to the award.</td></tr>
         <tr><td height=10></td><tr>
          <tr height=40>

             <td>&nbsp;</td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Award ID</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Mod</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Vendor Name</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=15<cfelse><cfif #sv# is 15>sv=150<cfelse>sv=15</cfif></cfif>"><b>Department</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Agency</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Office</b></a></td>

             <td class="text_xsmall"><b>Award Description</b></td>


             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>NAICS</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>PSC</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PoP Start</b></a></td>
             <td class="text_xsmall"><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP End</b></a></td>
             <td class="text_xsmall" align=right><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Base</b></a></td>
             <td class="text_xsmall" align=right><a href="results.cfm?number=#number#&<cfif not isdefined("sv")>sv=16<cfelse><cfif #sv# is 16>sv=160<cfelse>sv=16</cfif></cfif>"><b>+ Options</b></a></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff" height=30>
           <cfelse>
            <tr bgcolor="e0e0e0" height=30>

           </cfif>

               <td class="text_xsmall" valign=top width=30><a href="analyze.cfm?id=#id#&number=#number#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_analyze.png" border=0 width=20 alt="Analyze" title="Analyze"></a></a></td>
               <td class="text_xsmall" width=75 valign=top>#dateformat(action_date,'mm/dd/yy')#</a></td>
               <td class="text_xsmall" width=100 valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
               <td class="text_xsmall" valign=top width=50>#modification_number#</a></td>
               <td class="text_xsmall" valign=top>#recipient_name#</td>
               <td class="text_xsmall" valign=top>#awarding_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>
               <td class="text_xsmall" valign=top width=350>#award_description#</td>
               <td class="text_xsmall" valign=top>#naics_code#</td>
               <td class="text_xsmall" valign=top>#product_or_service_code#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_start_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_potential_end_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top align=right width=75>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
               <td class="text_xsmall" valign=top align=right width=75>#numberformat(base_and_exercised_options_value,'$999,999,999,999')#</td>

             </tr>

          <cfset tot = tot + #federal_action_obligation#>
          <cfif #base_and_exercised_options_value# is "">
          <cfset tot2 = tot2 + 0>
          <cfelse>
          <cfset tot2 = tot2 + #base_and_exercised_options_value#>
          </cfif>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          </cfif>


          <tr><td>&nbsp;</td></tr>

		  </table>

		  </td><td valign=top></td></tr>

		</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>