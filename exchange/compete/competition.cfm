<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_header">Contract Competitive Analysis</td>
               <td align=right class="feed_option"><a href="results.cfm?number=#number#">Return</a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=5></td></tr>
          </table>

          </cfoutput>


           <!--- Get Contract Information --->

  		   <cfquery name="contract" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data
			where id = #id#
		   </cfquery>

  		   <cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(id) as mods, sum(federal_action_obligation) as base, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as ceiling from award_data
			where award_id_piid = '#contract.award_id_piid#'
		   </cfquery>

           <cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall"><b>CONTRACT NUMBER</b></td>
						<td class="text_xsmall">#contract.award_id_piid#</td></tr>
					<tr><td class="text_xsmall" width=150><b>VENDOR</b></td>
						<td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#contract.recipient_name#</a>&nbsp;-&nbsp;<a href="/exchange/include/award_dashboard.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">Company Footprint</a></td></tr>
					<tr><td class="text_xsmall"><b>PARENT CONTRACT ##</b></td>
						<td class="text_xsmall">#contract.parent_award_id#</td></tr>
					<tr><td class="text_xsmall"><b>AWARD & MODS</b></td>
						<td class="text_xsmall">#totals.mods#</td></tr>
					<tr><td class="text_xsmall"><b>NAICS</b></td>
						<td class="text_xsmall">#contract.naics_code# - #contract.naics_description#</td></tr>
					<tr><td class="text_xsmall"><b>PSC</b></td>
						<td class="text_xsmall">#contract.product_or_service_code# - #contract.product_or_service_code_description#</td></tr>
                </table>

                </td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.awarding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AGENCY</b></td>
						<td class="text_xsmall">#contract.awarding_sub_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AWARDING OFFICE</b></td>
						<td class="text_xsmall">#contract.awarding_office_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.funding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING AGENCY</b></td>
						<td class="text_xsmall">#contract.funding_sub_agency_name#</td></tr>

					<tr><td class="text_xsmall"><b>FUNDING OFFICE</b></td>
						<td class="text_xsmall">#contract.funding_office_name#</td></tr>
                </table>

                </td>

                </td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>BASE</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base,'$999,999,999.99')#</td></tr>
					<tr><td class="text_xsmall"><b>OPTIONS</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.options,'$999,999,999.99')#</td></tr>
					<tr><td class="text_xsmall"><b>CEILING</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base + totals.options,'$999,999,999.99')#</td></tr>
					<tr><td class="text_xsmall"><b>POP START</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_start_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP CURRENT END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_current_end_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP POTENTIAL END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_potential_end_date,'mm/dd/yyyy')#</td></tr>

                </table>

                </td>

                </tr>

                <tr><td height=5></td></tr>

                <tr><td colspan=3 class="text_xsmall"><b>Award Description</b></td></tr>
                <tr><td colspan=3 class="text_xsmall">#contract.award_description#</td></tr>

                <tr><td height=5></td></tr>


          <tr><td colspan=3><hr></td></tr>

          </table>

          </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="stat_dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as count, count(distinct(award_id_piid)) as contracts from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      award_id_piid <> '#contract.award_id_piid#'
		   </cfquery>

  		   <cfquery name="stat_dept_current" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as count, count(distinct(award_id_piid)) as contracts from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      period_of_performance_current_end_date >= #now()# and
			      award_id_piid <> '#contract.award_id_piid#'
		   </cfquery>

  		   <cfquery name="stat_agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as count, count(distinct(award_id_piid)) as contracts from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
			      naics_code = '#contract.naics_code#' and
			      award_id_piid <> '#contract.award_id_piid#'
		   </cfquery>

  		   <cfquery name="stat_office" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as count, count(distinct(award_id_piid)) as contracts from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
			      awarding_office_code = '#contract.awarding_office_code#' and
			      naics_code = '#contract.naics_code#' and
			      award_id_piid <> '#contract.award_id_piid#'
		   </cfquery>

  		   <cfquery name="stat_funding" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(distinct(recipient_duns)) as count, count(distinct(award_id_piid)) as contracts from award_data
			where awarding_agency_code = '#contract.awarding_agency_code#' and
			      awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
			      awarding_office_code = '#contract.awarding_office_code#' and
			      funding_office_code = '#contract.funding_office_code#' and
			      naics_code = '#contract.naics_code#' and
			      award_id_piid <> '#contract.award_id_piid#'
		   </cfquery>

          <cfoutput>

           <tr><td height=10></td></tr>
           <tr><td class="feed_header">Competitive Lenses</td></tr>
           <tr><td class="feed_option" colspan=4>Competitive lenses are analytical summaries of the common scenarios that organizations consider when competing for a contract.  These lenses compare the information / details of this contract with over 21m contract awards from 2012 - 2018 to provide you with insights and intelligence to help you win.</td></tr>
           <tr><td height=10></td></tr>

           <tr><td class="feed_header">Direct Competition</td></tr>

           <tr><td height=10></td></tr>

           <tr><td class="feed_option"><b>Lens Description</b></td>
               <td class="feed_option" align=right><b>Vendors</b></td>
               <td class="feed_option" align=right><b>Contracts</b></td></tr>

           <tr><td class="feed_option">Other vendors who <b>have won</b> contracts delivering the same NAICS code for the Department</td>
               <td class="feed_option" align=right><a href="competition.cfm?lens=1&id=#id#&number=#number#">#numberformat(stat_dept.count,'999,999')#</a></td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_dept.contracts,'999,999')#</a></td></tr>

           <tr bgcolor="e0e0e0"><td class="feed_option">Other vendors who <b>have won</b> contracts delivering the same NAICS code for the Agency</td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_agency.count,'999,999')#</a></td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_agency.contracts,'999,999')#</a></td></tr>

           <tr><td class="feed_option">Other vendors who <b>have won</b> contracts delivering the same NAICS code for the Awarding Office</td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_office.count,'999,999')#</a></td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_office.contracts,'999,999')#</a></td></tr>

           <tr bgcolor="e0e0e0"><td class="feed_option">Other vendors who <b>have won</b> contracts delivering the same NAICS code for the Funding Office</td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_funding.count,'999,999')#</a></td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_funding.contracts,'999,999')#</a></td></tr>

           <tr><td class="feed_option">Other vendors who <b>are currently delivering</b> the same NAICS code for the Department</td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_dept_current.count,'999,999')#</a></td>
               <td class="feed_option" align=right><a href="open.cfm">#numberformat(stat_dept_current.contracts,'999,999')#</a></td></tr>

          </cfoutput>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>