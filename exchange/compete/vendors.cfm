<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">COMPETITOR ANALYSIS</td>
               <td align=right class="feed_option"><a href="analyze.cfm?id=#id#&number=#number#"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td height=5></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=5></td></tr>
          </table>

          </cfoutput>

           <!--- Get Contract Information --->

  		   <cfquery name="contract" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data
			where id = #id#
		   </cfquery>

           <cfset #session.comp_naics# = '#contract.naics_code#'>
           <cfset #session.comp_psc# = '#contract.product_or_service_code#'>

  		   <cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(id) as mods, sum(federal_action_obligation) as base, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as ceiling from award_data
			where award_id_piid = '#contract.award_id_piid#' and
			      recipient_duns = '#contract.recipient_duns#'
		   </cfquery>

           <cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall"><b>AWARD NUMBER</b></td>
						<td class="text_xsmall">#contract.award_id_piid#</td></tr>
					<tr><td class="text_xsmall" width=150><b>VENDOR</b></td>
						<td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#contract.recipient_name#</a>&nbsp;-&nbsp;<a href="/exchange/include/award_dashboard.cfm?duns=#contract.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">Company Footprint</a></td></tr>
					<tr><td class="text_xsmall"><b>PARENT CONTRACT ##</b></td>
						<td class="text_xsmall">#contract.parent_award_id#</td></tr>
					<tr><td class="text_xsmall"><b>AWARD & MODS</b></td>
						<td class="text_xsmall">#totals.mods#</td></tr>
					<tr><td class="text_xsmall"><b>NAICS</b></td>
						<td class="text_xsmall">#contract.naics_code# - #contract.naics_description#</td></tr>
					<tr><td class="text_xsmall"><b>PSC</b></td>
						<td class="text_xsmall">#contract.product_or_service_code# - #contract.product_or_service_code_description#</td></tr>
                </table>

                </td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.awarding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AGENCY</b></td>
						<td class="text_xsmall">#contract.awarding_sub_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>AWARDING OFFICE</b></td>
						<td class="text_xsmall">#contract.awarding_office_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING DEPARTMENT</b></td>
						<td class="text_xsmall">#contract.funding_agency_name#</td></tr>
					<tr><td class="text_xsmall"><b>FUNDING AGENCY</b></td>
						<td class="text_xsmall">#contract.funding_sub_agency_name#</td></tr>

					<tr><td class="text_xsmall"><b>FUNDING OFFICE</b></td>
						<td class="text_xsmall">#contract.funding_office_name#</td></tr>
                </table>

                </td></td><td valign=top>

                <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="text_xsmall" width=150><b>TOTAL CONTRACT BASE</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base,'$999,999,999')#</td></tr>
					<tr><td class="text_xsmall"><b>TOTAL CONTRACT OPTIONS</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.options,'$999,999,999')#</td></tr>
					<tr><td class="text_xsmall"><b>TOTAL CONTRACT CEILING</b></td>
						<td class="text_xsmall" align=right>#numberformat(totals.base + totals.options,'$999,999,999')#</td></tr>
					<tr><td class="text_xsmall"><b>POP START</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_start_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP CURRENT END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_current_end_date,'mm/dd/yyyy')#</td></tr>
					<tr><td class="text_xsmall"><b>POP POTENTIAL END</b></td>
						<td class="text_xsmall" align=right>#dateformat(contract.period_of_performance_potential_end_date,'mm/dd/yyyy')#</td></tr>

                </table>

                </td></tr>

                <tr><td height=5></td></tr>

                <tr><td colspan=3 class="text_xsmall"><b>Award Description</b></td></tr>
                <tr><td colspan=3 class="text_xsmall">#contract.award_description#</td></tr>

                <tr><td height=5></td></tr>

          <tr><td colspan=3><hr></td></tr>

          </table>

          </cfoutput>

           <cfif l is 1>

			   <cfquery name="contracts" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from award_data
				where awarding_agency_code = '#contract.awarding_agency_code#' and
					  naics_code = '#contract.naics_code#' and
					  product_or_service_code = '#contract.product_or_service_code#' and
					  recipient_duns <> '#contract.recipient_duns#'

					<cfif #session.comp_cstatus# is 3>
						  and period_of_performance_current_end_date > #now()#
					<cfelseif #session.comp_cstatus# is 2>
						  and period_of_performance_current_end_date < #now()#
					</cfif>

               <cfif isdefined("sv")>

                <cfif sv is 1>
                 order by action_date DESC
                <cfelseif sv is 10>
                 order by action_date ASC
                <cfelseif sv is 2>
                 order by award_id_piid ASC, modification_number ASC
                <cfelseif sv is 20>
                 order by award_id_piid DESC, modification_number ASC
                <cfelseif sv is 4>
                 order by recipient_name ASC
                <cfelseif sv is 40>
                 order by recipient_name DESC
                <cfelseif sv is 5>
                 order by awarding_agency_name ASC
                <cfelseif sv is 50>
                 order by awarding_agency_name DESC
                <cfelseif sv is 6>
                 order by awarding_sub_agency_name ASC
                <cfelseif sv is 60>
                 order by awarding_sub_agency_name DESC
                <cfelseif sv is 7>
                 order by awarding_office_name ASC
                <cfelseif sv is 70>
                 order by awarding_office_name DESC
                <cfelseif sv is 8>
                 order by type_of_set_aside_code ASC
                <cfelseif sv is 80>
                 order by type_of_set_aside_code DESC
                <cfelseif sv is 9>
                 order by period_of_performance_start_date ASC
                <cfelseif sv is 90>
                 order by period_of_performance_start_date DESC
                <cfelseif sv is 10>
                 order by period_of_performance_current_end_date ASC
                <cfelseif sv is 100>
                 order by period_of_performance_current_end_date DESC
                <cfelseif sv is 11>
                 order by federal_action_obligation DESC
                <cfelseif sv is 110>
                 order by federal_action_obligation ASC
                </cfif>
               <cfelse>
                order by action_date DESC
               </cfif>

			   </cfquery>

		   <cfelseif l is 2>

			   <cfquery name="contracts" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from award_data
				where awarding_agency_code = '#contract.awarding_agency_code#' and
					  naics_code = '#contract.naics_code#' and
					  awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
					  product_or_service_code = '#contract.product_or_service_code#' and
					  recipient_duns <> '#contract.recipient_duns#'

					<cfif #session.comp_cstatus# is 3>
						  and period_of_performance_current_end_date > #now()#
					<cfelseif #session.comp_cstatus# is 2>
						  and period_of_performance_current_end_date < #now()#
					</cfif>

              <cfif isdefined("sv")>

                <cfif sv is 1>
                 order by action_date DESC
                <cfelseif sv is 10>
                 order by action_date ASC
                <cfelseif sv is 2>
                 order by award_id_piid ASC, modification_number ASC
                <cfelseif sv is 20>
                 order by award_id_piid DESC, modification_number ASC
                <cfelseif sv is 4>
                 order by recipient_name ASC
                <cfelseif sv is 40>
                 order by recipient_name DESC
                <cfelseif sv is 5>
                 order by awarding_agency_name ASC
                <cfelseif sv is 50>
                 order by awarding_agency_name DESC
                <cfelseif sv is 6>
                 order by awarding_sub_agency_name ASC
                <cfelseif sv is 60>
                 order by awarding_sub_agency_name DESC
                <cfelseif sv is 7>
                 order by awarding_office_name ASC
                <cfelseif sv is 70>
                 order by awarding_office_name DESC
                <cfelseif sv is 8>
                 order by type_of_set_aside_code ASC
                <cfelseif sv is 80>
                 order by type_of_set_aside_code DESC
                <cfelseif sv is 9>
                 order by period_of_performance_start_date ASC
                <cfelseif sv is 90>
                 order by period_of_performance_start_date DESC
                <cfelseif sv is 10>
                 order by period_of_performance_current_end_date ASC
                <cfelseif sv is 100>
                 order by period_of_performance_current_end_date DESC
                <cfelseif sv is 11>
                 order by federal_action_obligation DESC
                <cfelseif sv is 110>
                 order by federal_action_obligation ASC
                </cfif>
               <cfelse>
                order by action_date DESC
               </cfif>


			   </cfquery>

		   <cfelseif l is 3>

			   <cfquery name="contracts" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from award_data
				where awarding_agency_code = '#contract.awarding_agency_code#' and
					  naics_code = '#contract.naics_code#' and
					  awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
					  awarding_office_code = '#contract.awarding_office_code#' and
					  product_or_service_code = '#contract.product_or_service_code#' and
					  recipient_duns <> '#contract.recipient_duns#'

					<cfif #session.comp_cstatus# is 3>
						  and period_of_performance_current_end_date > #now()#
					<cfelseif #session.comp_cstatus# is 2>
						  and period_of_performance_current_end_date < #now()#
					</cfif>

              <cfif isdefined("sv")>

                <cfif sv is 1>
                 order by action_date DESC
                <cfelseif sv is 10>
                 order by action_date ASC
                <cfelseif sv is 2>
                 order by award_id_piid ASC, modification_number ASC
                <cfelseif sv is 20>
                 order by award_id_piid DESC, modification_number ASC
                <cfelseif sv is 4>
                 order by recipient_name ASC
                <cfelseif sv is 40>
                 order by recipient_name DESC
                <cfelseif sv is 5>
                 order by awarding_agency_name ASC
                <cfelseif sv is 50>
                 order by awarding_agency_name DESC
                <cfelseif sv is 6>
                 order by awarding_sub_agency_name ASC
                <cfelseif sv is 60>
                 order by awarding_sub_agency_name DESC
                <cfelseif sv is 7>
                 order by awarding_office_name ASC
                <cfelseif sv is 70>
                 order by awarding_office_name DESC
                <cfelseif sv is 8>
                 order by type_of_set_aside_code ASC
                <cfelseif sv is 80>
                 order by type_of_set_aside_code DESC
                <cfelseif sv is 9>
                 order by period_of_performance_start_date ASC
                <cfelseif sv is 90>
                 order by period_of_performance_start_date DESC
                <cfelseif sv is 10>
                 order by period_of_performance_current_end_date ASC
                <cfelseif sv is 100>
                 order by period_of_performance_current_end_date DESC
                <cfelseif sv is 11>
                 order by federal_action_obligation DESC
                <cfelseif sv is 110>
                 order by federal_action_obligation ASC
                </cfif>
               <cfelse>
                order by action_date DESC
               </cfif>

		     </cfquery>

           <cfelseif l is 4>

			   <cfquery name="contracts" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from award_data
				where awarding_agency_code = '#contract.awarding_agency_code#' and
					  naics_code = '#contract.naics_code#' and
					  awarding_sub_agency_code = '#contract.awarding_sub_agency_code#' and
					  awarding_office_code = '#contract.awarding_office_code#' and
					  funding_office_code = '#contract.funding_office_code#' and
					  product_or_service_code = '#contract.product_or_service_code#'

					<cfif #session.comp_cstatus# is 3>
						  and period_of_performance_current_end_date > #now()#
					<cfelseif #session.comp_cstatus# is 2>
						  and period_of_performance_current_end_date < #now()#
					</cfif>

              <cfif isdefined("sv")>

                <cfif sv is 1>
                 order by action_date DESC
                <cfelseif sv is 10>
                 order by action_date ASC
                <cfelseif sv is 2>
                 order by award_id_piid ASC, modification_number ASC
                <cfelseif sv is 20>
                 order by award_id_piid DESC, modification_number ASC
                <cfelseif sv is 4>
                 order by recipient_name ASC
                <cfelseif sv is 40>
                 order by recipient_name DESC
                <cfelseif sv is 5>
                 order by awarding_agency_name ASC
                <cfelseif sv is 50>
                 order by awarding_agency_name DESC
                <cfelseif sv is 6>
                 order by awarding_sub_agency_name ASC
                <cfelseif sv is 60>
                 order by awarding_sub_agency_name DESC
                <cfelseif sv is 7>
                 order by awarding_office_name ASC
                <cfelseif sv is 70>
                 order by awarding_office_name DESC
                <cfelseif sv is 8>
                 order by type_of_set_aside_code ASC
                <cfelseif sv is 80>
                 order by type_of_set_aside_code DESC
                <cfelseif sv is 9>
                 order by period_of_performance_start_date ASC
                <cfelseif sv is 90>
                 order by period_of_performance_start_date DESC
                <cfelseif sv is 10>
                 order by period_of_performance_current_end_date ASC
                <cfelseif sv is 100>
                 order by period_of_performance_current_end_date DESC
                <cfelseif sv is 11>
                 order by federal_action_obligation DESC
                <cfelseif sv is 110>
                 order by federal_action_obligation ASC
                </cfif>
               <cfelse>
                order by action_date DESC
               </cfif>

			   </cfquery>

		   </cfif>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header">CONTRACT AWARDS AND MODIFICATIONS <cfoutput>(#contracts.recordcount#)</cfoutput></td></tr>
           <tr><td class="feed_option">The following awards represent contracts that were awarded to vendors in the same Department with the same NAICS and PSC code.</td></tr>
           <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				  <cfoutput>

				  <tr>

					 <td class="text_xsmall" width=75><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
					 <td class="text_xsmall" width=100><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Award ID</b></a></td>
					 <td class="text_xsmall" width=50><b>Mod</b></td>
					 <td class="text_xsmall" width=200><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Vendor Name</b></a></td>
					 <td class="text_xsmall" width=300><b>Award Description</b></td>
					 <td class="text_xsmall"><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Department</b></a></td>
					 <td class="text_xsmall"><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Agency</b></a></td>
					 <td class="text_xsmall"><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Office</b></a></td>
					 <td class="text_xsmall"><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Set Aside</b></a></td>
					 <td class="text_xsmall" width=75><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PoP Start</b></a></td>
					 <td class="text_xsmall" width=75><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP End</b></a></td>
					 <td class="text_xsmall" align=right width=75><a href="vendors.cfm?id=#contract.id#&l=#l#&number=#number#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Amount</b></a></td>

				  </tr>

				  </cfoutput>

				  <cfset counter = 0>

                  <cfoutput query="contracts">

                   <cfif counter is 0>
                    <tr bgcolor="ffffff">
                   <cfelse>
                    <tr bgcolor="e0e0e0">
                   </cfif>

                      <td class="text_xsmall" valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
                      <td class="text_xsmall" valign=top width=125><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
                      <td class="text_xsmall" valign=top>#modification_number#</td>
                      <td class="text_xsmall" valign=top width=200><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#recipient_name#</b></a></td>
                      <td class="text_xsmall" valign=top>#award_description#</td>
                      <td class="text_xsmall" valign=top>#awarding_agency_name#</td>
                      <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
                      <td class="text_xsmall" valign=top>#awarding_office_name#</td>
                      <td class="text_xsmall" valign=top>#type_of_set_aside#</td>
                      <td class="text_xsmall" valign=top>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
                      <td class="text_xsmall" valign=top>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
                      <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>

                   </tr>

                   <cfif counter is 0>
                    <cfset counter = 1>
                   <cfelse>
                    <cfset counter = 0>
                   </cfif>

                  </cfoutput>

			  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>