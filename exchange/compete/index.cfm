<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"comp_agency")>
<cfset StructDelete(Session,"comp_office")>
<cfset StructDelete(Session,"comp_naics")>
<cfset StructDelete(Session,"comp_cstatus")>
<cfset StructDelete(Session,"comp_psc")>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
          <cfinclude template="/exchange/portfolio/recent.cfm">


      </td><td valign=top>

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><img src="/images/icon_dashboard.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;COMPETITOR ANALYSIS</td>
               <td align=right></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>The competitor engine allows you to profile a Contract Number or Award ID to identify potential competition.</td></tr>

         <tr><td class="feed_sub_header" height=10><b>Search for Contract or Award Number</b></td></tr>
         <form action="/exchange/compete/results.cfm" method="post">
         <tr><td class="feed_option"><input name="number" type="text" style="width: 275px;" class="input_text" required></td></tr>
         <tr><td height=5></td></tr>
         <tr><td><input class="button_blue" type="submit" name="button" value="Search"></td></tr>
         </form>

         <tr><td height=10></td></tr>

         <tr><td class="link_small_gray">Note - some contracts or awards do not include dashes.  Please remove if needed.  To find or locate a Contract Number or Award ID, please search <a href="/exchange/awards/"><b>Federal Awards</b></a>.</td></tr>
         </table>

        </td><td valign=top></td></tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>