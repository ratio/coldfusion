<cfinclude template="/exchange/security/check.cfm">

<cfset #session.comp_agency# = '#agency#'>
<cfset #session.comp_office# = '#office#'>
<cfset #session.comp_cstatus# = #contract_status#>

<cfif #naics# is "">
 <cfset #session.comp_naics# = 0>
<cfelse>
 <cfset #session.comp_naics# = '#naics#'>
</cfif>

<cfif #psc# is "">
 <cfset #session.comp_psc# = 0>
<cfelse>
 <cfset #session.comp_psc# = '#psc#'>
</cfif>

<cflocation URL="analyze.cfm?id=#id#&number=#number#" addtoken="no">

