<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="data" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select outfitter, count(player) as total from players
 group by outfitter
 order by outfitter
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="menu_1.cfm">

      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Leagues</td></tr>
         <tr><td><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfset counter = 0>

         <tr>
            <td></td>
            <td class="feed_sub_header">Outfitter Name</td>
            <td class="feed_sub_header" align=center>Players</td>
         </tr>

         <cfoutput query="data">
          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td style="padding-top: 10px; padding-bottom: 10px;" width=60>

                 <cfif data.outfitter is not "">
	                 <cfset name = #replace(data.outfitter," ","","all")#>
	           		 <img src="//logo.clearbit.com/www.#name#.com" width=40 border=0 onerror="this.src='/images/no_logo.png'">
                 </cfif>

             </td>

             <td class="feed_sub_header" height=40>

             <cfif data.outfitter is "">
	             <a href="player_list.cfm?val=#data.outfitter#&t=outfitter">Unassigned</a>
             <cfelse>
	             <a href="player_list.cfm?val=#data.outfitter#&t=outfitter">#ucase(data.outfitter)#</a>
             </cfif></td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#data.total#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>
        </table>



        </td></tr>

        </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>