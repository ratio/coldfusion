<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="data" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from players
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="menu_1.cfm">

      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Players</td></tr>
         <tr><td><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfset counter = 0>

         <tr>
            <td class="feed_sub_header">Name / Number</td>
            <td class="feed_sub_header">League</td>
            <td class="feed_sub_header">Club</td>
            <td class="feed_sub_header">Position</td>
            <td class="feed_sub_header" align=center>Age</td>
            <td class="feed_sub_header" align=center>Foot</td>
            <td class="feed_sub_header" align=center>Height</td>
            <td class="feed_sub_header" align=center>Caps</td>
            <td class="feed_sub_header" align=center>Goals</td>
            <td class="feed_sub_header" align=center>Assists</td>
            <td class="feed_sub_header" align=center>Played</td>
            <td class="feed_sub_header">Agent</td>
            <td class="feed_sub_header" align=right>Value</td>
         </tr>

         <cfoutput query="data">
          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_sub_header" height=40>#data.player# (#data.number#)</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" width=150>#data.league#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#data.club#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#data.position#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#data.age#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.foot#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.height#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.caps#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.totalgoals19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.totalassist19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.totalminutesplayed19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#data.agent#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=right>#data.highestmarketvalue#</td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>
        </table>



        </td></tr>

        </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>