<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="players" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from players
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Players</td></tr>
         <tr><td><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfset counter = 0>

         <tr>
            <td class="feed_sub_header">Name</td>
            <td class="feed_sub_header" align=center>Number</td>
            <td class="feed_sub_header">Position</td>
            <td class="feed_sub_header" align=center>Age</td>
            <td class="feed_sub_header" align=center>Foot</td>
            <td class="feed_sub_header" align=center>Height</td>
            <td class="feed_sub_header" align=center>Caps</td>
            <td class="feed_sub_header" align=center>Goals</td>
            <td class="feed_sub_header" align=center>Assists</td>
            <td class="feed_sub_header" align=center>Played</td>
            <td class="feed_sub_header">Agent</td>
            <td class="feed_sub_header" align=right>Market Value</td>
         </tr>

         <cfoutput query="players">
          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_sub_header" height=40>#players.player#</td>
             <td class="feed_sub_header" height=40 align=center style="font-weight: normal;">#players.number#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#players.position#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.age#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.foot#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.height#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.caps#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.totalgoals19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.totalassist19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#players.totalminutesplayed19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#players.agent#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=right>#players.highestmarketvalue#</td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>
        </table>



        </td></tr>

        </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>