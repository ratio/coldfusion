<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfquery name="data" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from players

 <cfif t is "League">
  where league = '#val#'
 <cfelseif t is "Club">
  where club = '#val#'
 <cfelseif t is "Outfitter">
  where outfitter = '#val#'
 <cfelseif t is "Agent">
  where agent = '#val#'
 <cfelseif t is "Position">
  where position = '#val#'
 </cfif>

 <cfif sv is 1>
  order by player ASC
 <cfelseif sv is 10>
  order by player DESC
 <cfelseif sv is 2>
  order by league ASC
 <cfelseif sv is 20>
  order by league DESC
 <cfelseif sv is 3>
  order by club ASC
 <cfelseif sv is 30>
  order by club DESC
 <cfelseif sv is 4>
  order by position ASC
 <cfelseif sv is 40>
  order by position DESC
 <cfelseif sv is 5>
  order by age ASC
 <cfelseif sv is 50>
  order by age DESC
 <cfelseif sv is 6>
  order by foot ASC
 <cfelseif sv is 60>
  order by foot DESC
 <cfelseif sv is 7>
  order by height ASC
 <cfelseif sv is 70>
  order by height DESC
 <cfelseif sv is 8>
  order by caps ASC
 <cfelseif sv is 80>
  order by caps DESC
 <cfelseif sv is 9>
  order by totalgoals19_20 ASC
 <cfelseif sv is 90>
  order by totalgoals19_20 DESC
 <cfelseif sv is 11>
  order by totalassist19_20 ASC
 <cfelseif sv is 110>
  order by totalassist19_20 DESC
 <cfelseif sv is 12>
  order by totalminutesplayed19_20 ASC
 <cfelseif sv is 120>
  order by totalminutesplayed19_20 DESC
 <cfelseif sv is 13>
  order by agent ASC
 <cfelseif sv is 130>
  order by agent DESC
 <cfelseif sv is 14>
  order by highestmarketvalue ASC
 <cfelseif sv is 140>
  order by highestmarketvalue DESC
 </cfif>

</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="menu_1.cfm">

      </td><td valign=top>

      <div class="main_box">

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">#ucase(val)#</td>
             <td class="feed_sub_header" align=right><a href="#cgi.http_referer#">Return</a></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>
         <tr><td class="feed_header">Players (#data.recordcount#)</td></tr>
         <tr><td height=10></td></tr>
        </table>
        </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfset counter = 0>

         <cfoutput>

         <tr>

            <td class="feed_sub_header"><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Name / Number</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>League</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Club</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Position</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Age</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Foot</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Height</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 7><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 70><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Caps</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 8><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 80><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Goals</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 9><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 90><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Assists</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 11><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 110><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=center><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Played</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 12><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 120><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header"><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Agent</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 13><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 130><img src="/images/icon_sort_down.png" width=10></cfif></td>
            <td class="feed_sub_header" align=right><a href="player_list.cfm?val=#val#&t=#t#&<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Value</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 14><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 140><img src="/images/icon_sort_down.png" width=10></cfif></td>

         </tr>

         </cfoutput>

         <cfoutput query="data">
          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

             <td class="feed_sub_header" height=40>#data.player# (#data.number#)</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" width=150>#data.league#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#data.club#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#data.position#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=center>#data.age#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.foot#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.height#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.caps#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.totalgoals19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.totalassist19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal; padding-right: 10px;" align=center>#data.totalminutesplayed19_20#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;">#data.agent#</td>
             <td class="feed_sub_header" height=40 style="font-weight: normal;" align=right>#data.highestmarketvalue#</td>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>
        </table>



        </td></tr>

        </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>