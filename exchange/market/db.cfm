<cfinclude template="/exchange/security/check.cfm">
<cfinclude template="/exchange/security/check_token.cfm">

<cfset search_string = #replace(market_report_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

<cfif button is "Save">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into market_report
	 (

	  market_report_name,
	  market_report_company_name,
	  market_report_company_parent_duns,
	  market_report_desc,
	  market_report_created_by_usr_id,
	  market_report_hub_id,
	  market_report_dept_list,
	  market_report_agency_list,
	  market_report_keywords,
	  market_report_updated,
	  market_report_from,
	  market_report_to
	 )
	 values
	 (
	  '#market_report_name#',
	  '#market_report_company_name#',
      '#market_report_company_parent_duns#',
	  '#market_report_desc#',
	   #session.usr_id#,
	   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	  <cfif isdefined("market_report_dept_list")>'#market_report_dept_list#'<cfelse>null</cfif>,
	  <cfif isdefined("market_report_agency_list")>'#market_report_agency_list#'<cfelse>null</cfif>,
	  '#search_string#',
	   #now()#,
	  '#market_report_from#',
	  '#market_report_to#'
	 )
	</cfquery>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif button is "Update">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update market_report
	 set market_report_name = '#market_report_name#',
	     market_report_company_name = '#market_report_company_name#',
	     market_report_company_duns = '#market_report_company_duns#',
	     market_report_company_parent_duns = '#market_report_company_parent_duns#',
	     market_report_desc = '#market_report_desc#',
   	     market_report_dept_list = <cfif isdefined("market_report_dept_list")>'#market_report_dept_list#'<cfelse>null</cfif>,
	     market_report_agency_list = <cfif isdefined("market_report_agency_list")>'#market_report_agency_list#'<cfelse>null</cfif>,
	     market_report_keywords = '#search_string#',
	     market_report_updated = #now()#,
	     market_report_from = '#market_report_from#',
	     market_report_to = '#market_report_to#'
	 where market_report_id = #session.market_report_id#
	</cfquery>

	<cflocation URL="run_report.cfm?u=1" addtoken="no">

<cfelseif button is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete market_report
	 where market_report_id = #session.market_report_id#
	</cfquery>

	<cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>