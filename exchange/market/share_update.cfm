<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

<cfif isdefined("share_id")>

 <cfloop index="d" list="#share_id#">

 <cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  delete sharing
  where sharing_hub_id = #session.hub# and
        sharing_report_id = #session.market_report_id# and
        sharing_to_usr_id = #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#
 </cfquery>

 </cfloop>

 <cfloop index="d" list="#share_id#">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into sharing
	 (
	  sharing_hub_id,
	  sharing_report_id,
	  sharing_from_usr_id,
	  sharing_to_usr_id,
	  sharing_access
	 )
	 values
	 (
	 #session.hub#,
	 #session.market_report_id#,
	 #session.usr_id#,
	 #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	 1
	 )
	</cfquery>

 </cfloop>

</cfif>

</cftransaction>

<cflocation URL="share.cfm?u=1" addtoken="no">