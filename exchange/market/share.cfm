<cfinclude template="/exchange/security/check.cfm">

<cfset session.group_return_page = "Market Reports">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif session.market_report_access_level GT 1>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="report_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<cfquery name="sharing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing
 join usr on usr_id = sharing_to_usr_id
 where sharing_report_id = #session.market_report_id# and
       sharing_hub_id = #session.hub#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">
        <cfoutput>
        SHARE MARKET REPORT - #ucase(report_info.market_report_name)#
        </cfoutput>

        </td>
            <td align=right class="feed_sub_header"><a href="run_report.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

          <form action="share_search.cfm" method="post">

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" style="font-weight: normal;">

                  <b>Add User</b>&nbsp;&nbsp;
		          <input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="keyword" style="width: 250px;" class="input_text" placeholder="Search for name" required>
		          &nbsp;
		          <input type="submit" name="button" class="button_blue" value="Go">
            </form>
          </td>

          <td align=right class="feed_sub_header">

          <a href="import.cfm"><img src="/images/plus3.png" width=18 border=0 hspace=5 title="Import from Group" alt="Import from Group"></a>
          <a href="import.cfm">Import Users from Group</a>
          &nbsp;|&nbsp;
          <a href="/exchange/groups/"><img src="/images/group.png" width=22 border=0 hspace=5 title="Manage My Groups" alt="Manage My Groups"></a>
          <a href="/exchange/groups/">Manage My Groups</a>

          </td>

          </tr>

       </table>
       </form>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("u")>
           <cfif u is 1>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">User(s) have been successfully granted Share access.</td></tr>
           <cfelseif u is 2>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">User(s) Share access has been successfully removed.</td></tr>
           <cfelseif u is 3>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">Users rights have been successfully updated.</td></tr>
           <cfelseif u is 4>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">Selected user(s) have been successfully imported.</td></tr>
           <cfelseif u is 5>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">No users were selected for import.</td></tr>
           <cfelseif u is 6>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">View access has been granted for the Company.</td></tr>
           <cfelseif u is 7>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">View access has been revoked for the Company.</td></tr>
           </cfif>
          </cfif>

         </table>

         <cfif sharing.recordcount is 0>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_sub_header" style="font-weight: normal;">No users have been granted Share access to this Market Report. To add users, please search above or import from a Group.</td></tr>
          </table>

         <cfelse>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
             <td class="feed_sub_header">REMOVE</td>
             <td></td>
             <td class="feed_sub_header">NAME</td>
             <td class="feed_sub_header">TITLE</td>
             <td class="feed_sub_header">EMAIL</td>
             <td class="feed_sub_header" align=center>VIEW</td>
             <td class="feed_sub_header" align=center>UPDATE</td>
          </tr>

          <form action="share_remove.cfm" method="post">

          <cfoutput query="sharing">
             <tr>
                 <td width=100>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="input_text" name="share_id" value=#encrypt(sharing.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;"></td>
                 <td width=80 class="feed_sub_header">

				 <cfif #sharing.usr_photo# is "">
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(sharing.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 border=0>
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(sharing.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 0px;" src="/exchange/media/#sharing.usr_photo#" height=40 width=40 border=0>
				 </cfif>

				</td>

			    <td width=200 class="feed_sub_header"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(sharing.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#ucase(sharing.usr_first_name)# #ucase(sharing.usr_last_name)#</a></td>
			    <td width=200 class="feed_sub_header" style="font-weight: normal;">#ucase(sharing.usr_title)#</td>
			    <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(sharing.usr_email))#</td>
			    <td align=center width=70><cfif sharing.sharing_access is 1><img src="/images/icon_radio_check.png" width=30><cfelse><a href="access_update.cfm?i=#encrypt(sharing.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=1"><img src="/images/icon_radio_uncheck.png" width=30 border=0 alt="Select" title="Select"></a></cfif></td>
			    <td align=center width=70><cfif sharing.sharing_access is 2><img src="/images/icon_radio_check.png" width=30><cfelse><a href="access_update.cfm?i=#encrypt(sharing.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#&a=2"><img src="/images/icon_radio_uncheck.png" width=30 border=0 alt="Select" title="Select"></a></cfif></td>
			 </tr>

			 <tr><td colspan=7><hr></td></tr>

			</cfoutput>

            <tr><td height=10></td></tr>

            <tr><td class="link_small_gray" colspan=7><b>VIEW</b> - Users can view all information with the Market Report.</td></tr>
            <tr><td class="link_small_gray" colspan=7><b>UPDATE</b> - Users can modify all information in the Market Report.</td></tr>
            <tr><td height=20></td></tr>
            <tr><td colspan=5><input type="submit" name="button" class="button_blue_large" value="Remove Sharing" onclick="return confirm('Remove Sharing?\r\nAre you sure you want to remove sharing access from the selected users?');"></td></tr>


			</form>

			</table>

			</cfif>

          </td></tr>
          </table>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>