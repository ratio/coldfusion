<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -6px;
    margin-top: 20px;
    margin-left: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    padding-right: 20px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -4px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}
.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfif not isdefined("sv")>
 <cfset sv = 7>
</cfif>

<cfquery name="report" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select awarding_sub_agency_name, awarding_sub_agency_code, count(distinct(award_id_piid)) as contracts, count(distinct(recipient_parent_duns)) as vendors, count(id) as awards,
        sum(federal_action_obligation) as obligated,
        sum(base_and_exercised_options_value) as exercised,
        sum(base_and_all_options_value) as all_options
        from award_data
 left join company on company_duns = recipient_parent_duns
 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

 <cfelse>

	 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
       and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
     <cfelseif report.market_report_dept_list is not "">
       and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
     <cfelseif report.market_report_agency_list is not "">
       and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
     </cfif>

 </cfif>

)

and contains((award_description),'#trim(report.market_report_keywords)#')
group by awarding_sub_agency_name, awarding_sub_agency_code

	<cfif #sv# is 1>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 10>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 2>
	 order by vendors DESC
	<cfelseif #sv# is 20>
	 order by vendors ASC
	<cfelseif #sv# is 3>
	 order by awards DESC
	<cfelseif #sv# is 30>
	 order by awards ASC
	<cfelseif #sv# is 4>
	 order by contracts DESC
	<cfelseif #sv# is 40>
	 order by contracts ASC
	<cfelseif #sv# is 5>
	 order by exercised DESC
	<cfelseif #sv# is 50>
	 order by exercised ASC
	<cfelseif #sv# is 6>
	 order by all_options DESC
	<cfelseif #sv# is 60>
	 order by all_options ASC
	<cfelseif #sv# is 7>
	 order by obligated DESC
	<cfelseif #sv# is 70>
	 order by obligated ASC
	</cfif>

</cfquery>

<cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select sum(federal_action_obligation) as obligated,
        sum(base_and_exercised_options_value) as exercised,
        sum(base_and_all_options_value) as all_options
        from award_data
 left join company on company_duns = recipient_parent_duns
 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

 <cfelse>

	 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
       and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
     <cfelseif report.market_report_dept_list is not "">
       and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
     <cfelseif report.market_report_agency_list is not "">
       and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
     </cfif>

 </cfif>

)

and contains((award_description),'#trim(report.market_report_keywords)#')
</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="URL.PageId" default="0">
<cfset RecordsPerPage = 250>
<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
<cfset EndRow = StartRow+RecordsPerPage-1>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>

      <td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td>

	  <td valign=top width=100%>

		<!--- Start --->

		<cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_report.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_companies.cfm">The Market</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;

           <a href="run_select.cfm">

           <cfif len(report.market_report_company_name) GT 10>
            #left(report.market_report_company_name,10)#... &nbsp; Awards
           <cfelse>
            #report.market_report_company_name# Awards
           </cfif>

	       </a></span>

          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_awards.cfm">All Awards</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_assist.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="run_customers.cfm">Customers</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_competitors.cfm">Competitors</a></span>
          </div>


        </cfoutput>

	      <div class="main_box_2">

	      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">#report.market_report_name#</td>
	              <td class="feed_sub_header" align=right><a href="index.cfm">All Reports</a></td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif agencies.recordcount is 0>
            <tr><td class="feed_sub_header" style="font-weight: normal;">Customers</td></tr>
           <cfelse>

           <cfoutput>
           <tr><td colspan=7 align=right class="feed_sub_header"><a href="run_companies.cfm?export=1&sv=#sv#"><img src="/images/icon_export_excel.png" width=20 hspace=10 border=0 alt="Export to Excel" title="Export to Excel"></a><a href="run_companies.cfm?export=1&sv=#sv#">Export to Excel</a></td></tr>
           </cfoutput>

           <cfif agencies.recordcount GT 0>

               <tr><td colspan=7 class="feed_sub_header" align=center>Top 25 Customers</td></tr>

			   <tr><td colspan=7>

			    <cfset counter = 1>

				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">

				google.charts.load('current', {packages: ['corechart', 'bar']});
				google.charts.setOnLoadCallback(drawBasic);

				function drawBasic() {

					  var data = google.visualization.arrayToDataTable([
						['Agency', 'Amount'],
						<cfoutput query="agencies">
						 <cfif counter LT 25>
						 <cfif sv is 7 or sv is 70>

						 ["#awarding_sub_agency_name#", #round(obligated)#],
						 <cfelseif sv is 5 or sv is 50>
						 ["#awarding_sub_agency_name#", #round(exercised)#],

						 <cfelseif sv is 6 or sv is 60>
						 ["#awarding_sub_agency_name#", #round(all_options)#],

						 </cfif>
						 </cfif>
						<cfset counter = counter + 1>
						</cfoutput>
					  ]);

					  var options = {
						title: '',
						chartArea: {left: 100, right: 0, width: '100%'},
						legend: 'none',
						height: 300,
						hAxis: {
						  textStyle: {color: 'black', fontSize: 12},
						  format: 'currency',
						  title: '',
						  minValue: 0
						},
						vAxis: {
						  title: 'Award Value',
						}
					  };

					  var chart = new google.visualization.ColumnChart(document.getElementById('spend'));

					  chart.draw(data, options);
					}

				  </script>

				  <div id="spend" style="width: 100%;"></div>

			   </td></tr>

			   <tr><td height=10></td></tr>

           </cfif>

           <cfoutput>
            <tr>
                <td class="feed_sub_header"><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>">Agency</a></td>
                <td class="feed_sub_header" align=center><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>">Contractors</a></td>
                <td class="feed_sub_header" align=center><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>">Contracts</a></td>
                <td class="feed_sub_header" align=center><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>">Awards</a></td>
                <td class="feed_sub_header" align=right><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>">Obligations</a></td>
                <td class="feed_sub_header" align=right><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>">Base & Exercised Options</a></td>
                <td class="feed_sub_header" align=right><a href="run_customers.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>">Base & All Options</a></td>
            </tr>
            </cfoutput>

            <cfset counter = 0>

            <cfoutput query="agencies">

              <cfif counter is 0>
               <tr bgcolor="ffffff" height=40>
              <cfelse>
               <tr bgcolor="e0e0e0" height=40>
              </cfif>

                 <td class="feed_sub_header"><a href="run_customers_awards.cfm?awarding_sub_agency_code=#awarding_sub_agency_code#"><b><cfif #awarding_sub_agency_name# is "">Unknown<cfelse>#awarding_sub_agency_name#</cfif></b></a></td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=center>#vendors#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=center>#contracts#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=center>#awards#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(obligated,'$999,999,999')#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(exercised,'$999,999,999')#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(all_options,'$999,999,999')#</td>
              </tr>

              <cfif counter is 0>
               <cfset counter = 1>
              <cfelse>
               <cfset counter = 0>
              </cfif>

            </cfoutput>

            <cfoutput>

             <tr><td colspan=7><hr></td></tr>
             <tr>
                <td colspan=4>&nbsp;</td>
                <td class="feed_sub_header" align=right>#numberformat(totals.obligated,'$999,999,999')#</td>
                <td class="feed_sub_header" align=right>#numberformat(totals.exercised,'$999,999,999')#</td>
                <td class="feed_sub_header" align=right>#numberformat(totals.all_options,'$999,999,999')#</td>
             </tr>


            </cfoutput>

           </cfif>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>