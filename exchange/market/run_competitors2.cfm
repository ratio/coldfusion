<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="report" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<style>
.follow_scroll_box {
    height: 140px;
    background-color: ffffff;
    overflow:auto;
}

.partner_badge {
    width: 31%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 325px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.partner_badge_title {
    font-family: calibri, arial;
    font-size: 14px;
    padding-bottom: 10px;
    color: 000000;
    font-weight: bold;
}
.partner_badge_text {
    font-family: calibri, arial;
    font-size: 12px;
    color: 000000;
}

</style>

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -6px;
    margin-top: 20px;
    margin-left: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    padding-right: 20px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -4px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}
.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfif not isdefined("sv")>
 <cfset sv = 4>
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>

      <td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td>

	  <td valign=top width=100%>

		<!--- Start --->

		<cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_report.cfm">DASHBOARD</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_companies.cfm">THE MARKET</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;

           <a href="run_select.cfm">

           <cfif len(report.market_report_company_name) GT 10>
            #ucase(left(report.market_report_company_name,10))#... &nbsp; AWARDS
           <cfelse>
            #ucase(report.market_report_company_name)# AWARDS
           </cfif>

	       </a></span>

          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_awards.cfm">ALL AWARDS</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_customers.cfm">CUSTOMERS</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_assist.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="run_competitors.cfm">COMPETITORS</a></span>
          </div>

        </cfoutput>

	      <div class="main_box_2">

	      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">#report.market_report_name#</td>
	              <td class="feed_sub_header" align=right><a href="index.cfm">All Reports</a></td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

          </cfoutput>

		  <cfif #report.market_report_competitor_parent_duns# is "" and #report.market_report_competitor_duns# is "">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_sub_header" style="font-weight: normal;">Competitor DUNS or Parent DUNS not identified.</td></tr>
          </table>

          <cfelse>

			<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select recipient_name, recipient_duns, count(id) as awards,
					sum(federal_action_obligation) as obligated,
					sum(base_and_exercised_options_value) as exercised,
					sum(base_and_all_options_value) as all_options,
					count(distinct(award_id_piid)) as contracts
					from award_data
			 left join company on company_duns = recipient_duns
			 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

			 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

			 <cfelse>

				 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
				   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 <cfelseif report.market_report_dept_list is not "">
				   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
				 <cfelseif report.market_report_agency_list is not "">
				   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 </cfif>

			 </cfif>


		     <cfif #report.market_report_competitor_parent_duns# is not "" and #report.market_report_competitor_duns# is not "">
			  and (recipient_duns = #report.market_report_competitor_parent_duns# or recipient_duns = #report.market_report_competitor_duns#)
		     <cfelseif #report.market_report_competitor_parent_duns# is "" and #report.market_report_competitor_duns# is not "">
			  and (recipient_duns = #report.market_report_competitor_duns#)
			 <cfelse>
			  and (recipient_duns = #report.market_report_competitor_parent_duns#)
			 </cfif>

			)

			and contains((award_description),'#trim(report.market_report_keywords)#')
			group by recipient_duns, recipient_name

				<cfif #sv# is 1>
				 order by recipient_name ASC
				<cfelseif #sv# is 10>
				 order by recipient_name DESC
				<cfelseif #sv# is 2>
				 order by recipient_duns ASC
				<cfelseif #sv# is 20>
				 order by recipient_duns DESC
				<cfelseif #sv# is 3>
				 order by awards DESC
				<cfelseif #sv# is 30>
				 order by awards ASC
				<cfelseif #sv# is 4>
				 order by obligated DESC
				<cfelseif #sv# is 40>
				 order by obligated ASC
				<cfelseif #sv# is 5>
				 order by exercised DESC
				<cfelseif #sv# is 50>
				 order by exercised ASC
				<cfelseif #sv# is 6>
				 order by all_options DESC
				<cfelseif #sv# is 60>
				 order by all_options ASC
				<cfelseif #sv# is 7>
				 order by contracts DESC
				<cfelseif #sv# is 70>
				 order by contracts ASC
				</cfif>

			</cfquery>

			<cfif isdefined("export")>
			 <cfinclude template="/exchange/include/export_to_excel.cfm">
			</cfif>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif agencies.recordcount is 0>
            <tr><td class="feed_sub_header" style="font-weight: normal;">No Competitors identified.</td></tr>
           <cfelse>

           <cfoutput>
           <tr><td class="feed_sub_header">&nbsp;</td>
               <td class="feed_sub_header" colspan=6 align=right><a href="run_competitors.cfm?export=1&sv=#sv#"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" hspace=10 title="Export to Excel"></a><a href="run_companies.cfm?export=1&sv=#sv#">Export to Excel</a></td>
           </tr>

           </cfoutput>

           </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>


              <tr>
                  <td class="feed_sub_header" align=center>COMPETITOR REVENUE</td>
                  <td></td>
                  <td class="feed_sub_header" align=center>COMPETITOR CONTRACTS & AWARDS</td>
                  </tr>

			   <tr><td width=48%>

			    <cfset counter = 1>

				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">

				google.charts.load('current', {packages: ['corechart', 'bar']});
				google.charts.setOnLoadCallback(drawBasic);

				function drawBasic() {

					  var data = google.visualization.arrayToDataTable([
						['Company', 'Amount'],
						<cfoutput query="agencies">
						 <cfif counter LT 25>
                         <cfif sv is 4 or sv is 40>
						 ["#recipient_name#", #round(obligated)#],
						 <cfelseif sv is 5 or sv is 50>
						 ["#recipient_name#", #round(exercised)#],
						 <cfelseif sv is 6 or sv is 60>
						 ["#recipient_name#", #round(all_options)#],
						 <cfelse>
						 ["#recipient_name#", #round(all_options)#],
						 </cfif>
						 </cfif>
						<cfset counter = counter + 1>
						</cfoutput>
					  ]);

					  var options = {
						title: '',
						chartArea: {left: 100, right: 0, width: '100%'},
						legend: 'none',
						height: 300,
						hAxis: {
						  textStyle: {color: 'black', fontSize: 12},
						  format: 'currency',
						  title: '',
						  minValue: 0
						},
						vAxis: {
						  title: 'Award Value',
						}
					  };

					  var chart = new google.visualization.ColumnChart(document.getElementById('spend'));

					  chart.draw(data, options);
					}

				  </script>

				  <div id="spend" style="width: 100%;"></div>

			   </td><td width=40>&nbsp;</td>

			   <td width=48%>



			   			    <cfset counter = 1>

			   				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			   				<script type="text/javascript">

			   				google.charts.load('current', {packages: ['corechart', 'bar']});
			   				google.charts.setOnLoadCallback(drawBasic);

			   				function drawBasic() {

			   					  var data = google.visualization.arrayToDataTable([
			   						['Company', 'Amount'],
			   						<cfoutput query="agencies">
			   						 <cfif counter LT 25>
			                            <cfif sv is 4 or sv is 40>
			   						 ["#recipient_name#", #round(obligated)#],
			   						 <cfelseif sv is 5 or sv is 50>
			   						 ["#recipient_name#", #round(exercised)#],
			   						 <cfelseif sv is 6 or sv is 60>
			   						 ["#recipient_name#", #round(all_options)#],
			   						 <cfelse>
			   						 ["#recipient_name#", #round(all_options)#],
			   						 </cfif>
			   						 </cfif>
			   						<cfset counter = counter + 1>
			   						</cfoutput>
			   					  ]);

			   					  var options = {
			   						title: '',
			   						chartArea: {left: 100, right: 0, width: '100%'},
			   						legend: 'none',
			   						height: 300,
			   						hAxis: {
			   						  textStyle: {color: 'black', fontSize: 12},
			   						  format: 'currency',
			   						  title: '',
			   						  minValue: 0
			   						},
			   						vAxis: {
			   						  title: 'Award Value',
			   						}
			   					  };

			   					  var chart = new google.visualization.ColumnChart(document.getElementById('spend2'));

			   					  chart.draw(data, options);
			   					}

			   				  </script>

			   				  <div id="spend2" style="width: 100%;"></div>

			   </tr>

			   <tr><td height=20></td></tr>

            </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>


            <cfoutput>

            <tr>
                <td class="feed_sub_header"><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>">Company Name</a></td>
                <td class="feed_sub_header"><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>">DUNS</a></td>
                <td class="feed_sub_header" align=center><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>">Contracts</a></td>
                <td class="feed_sub_header" align=center><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>">Awards</a></td>
                <td class="feed_sub_header" align=right><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>">Obligations</a></td>
                <td class="feed_sub_header" align=right><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>">Base & Exercised Options</a></td>
                <td class="feed_sub_header" align=right><a href="run_competitors.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>">Base & All Options</a></td>
            </tr>
            </cfoutput>

            <cfset counter = 0>

            <cfoutput query="agencies">

              <cfif counter is 0>
               <tr bgcolor="ffffff" height=40>
              <cfelse>
               <tr bgcolor="e0e0e0" height=40>
              </cfif>

                 <td class="feed_sub_header"><a href="run_competitors_awards.cfm"><b><cfif #recipient_name# is "">Unknown<cfelse>#recipient_name#</cfif></b></a></td>
                 <td class="feed_sub_header" style="font-weight: normal;">#recipient_duns#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=center>#contracts#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=center>#awards#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(obligated,'$999,999,999')#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(exercised,'$999,999,999')#</td>
                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(all_options,'$999,999,999')#</td>
              </tr>

              <cfif counter is 0>
               <cfset counter = 1>
              <cfelse>
               <cfset counter = 0>
              </cfif>

            </cfoutput>

			<cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select sum(federal_action_obligation) as obligated,
					sum(base_and_exercised_options_value) as exercised,
					sum(base_and_all_options_value) as all_options,
					count(distinct(award_id_piid)) as contracts
					from award_data
			 left join company on company_duns = recipient_duns
			 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

			 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

			 <cfelse>

				 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
				   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 <cfelseif report.market_report_dept_list is not "">
				   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
				 <cfelseif report.market_report_agency_list is not "">
				   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 </cfif>

			 </cfif>


			 <cfif report.market_report_competitor_parent_duns is not "">
			  and recipient_duns in (#report.market_report_competitor_parent_duns#)
			  <cfset d = "p">
			 <cfelse>
			  <cfif report.market_report_competitor_duns is not "">
			   and recipient_duns in (#report.market_report_competitor_duns#)
			   <cfset d = "c">
			  <cfelse>
			   <cfset d = "c">
			   and recipient_duns = '0'
			  </cfif>
			 </cfif>


			)

			and contains((award_description),'#trim(report.market_report_keywords)#')
           </cfquery>

           <tr><td colspan=7><hr></td></tr>

           <cfoutput>
           <tr>
               <td colspan=4></td>
               <td class="feed_sub_header" align=right>#numberformat(totals.obligated,'$999,999,999')#</td>
               <td class="feed_sub_header" align=right>#numberformat(totals.exercised,'$999,999,999')#</td>
               <td class="feed_sub_header" align=right>#numberformat(totals.all_options,'$999,999,999')#</td>
           </tr>
           </cfoutput>

           </cfif>

          </table>

			<cfquery name="comp_dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select distinct(awarding_sub_agency_code), awarding_sub_agency_name
					from award_data
			 left join company on company_duns = recipient_duns
			 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

			 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

			 <cfelse>

				 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
				   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 <cfelseif report.market_report_dept_list is not "">
				   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
				 <cfelseif report.market_report_agency_list is not "">
				   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 </cfif>

			 </cfif>

			 <cfif report.market_report_competitor_parent_duns is not "">
			  and recipient_duns in (#report.market_report_competitor_parent_duns#)
			  <cfset d = "p">
			 <cfelse>
			  <cfif report.market_report_competitor_duns is not "">
			   and recipient_duns in (#report.market_report_competitor_duns#)
			   <cfset d = "c">
			  <cfelse>
			   <cfset d = "c">
			   and recipient_duns = '0'
			  </cfif>
			 </cfif>

			)

			and contains((award_description),'#trim(report.market_report_keywords)#')
			order by awarding_sub_agency_name
			</cfquery>

            </cfif>

		   <center>
          </table>

          </center>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>