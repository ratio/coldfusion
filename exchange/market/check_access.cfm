<cfset market_report_access = 0>
<cfset session.market_report_access_level = 0>

<!--- Check for Admin access --->

<cfquery name="admin_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select market_report_id from market_report
 where market_report_created_by_usr_id = #session.usr_id# and
       market_report_id = #session.market_report_id#
</cfquery>

<cfif admin_access.recordcount is 1>

	 <cfset market_report_access = 1>
	 <cfset session.market_report_access_level = 3>

<cfelse>

	 <!--- Check for user Access --->

	<cfquery name="user_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_access from sharing
	 where  sharing_report_id = #session.market_report_id# and
			sharing_hub_id = #session.hub# and
			sharing_to_usr_id = #session.usr_id#
	</cfquery>

	<cfif user_access.recordcount is 1>

	 	<cfset market_report_access = 1>
	 	<cfset session.market_report_access_level = #user_access.sharing_access#>

    </cfif>

</cfif>

<cfif market_report_access is 0>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

