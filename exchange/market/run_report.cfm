<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 20px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	display: inline-block;
	margin-left: -4px;
	width: auto;
	margin-right: 0px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	padding-right: 20px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}

.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<cfquery name="report" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<cfinclude template = "check_access.cfm">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>

      <td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

       </td>

	  <td valign=top width=100%>

      <cfoutput>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_home2.png" width=20 align=absmiddle>&nbsp;&nbsp;<a href="run_report.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_companies.cfm">The Market</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;

           <a href="run_select.cfm">

           <cfif len(report.market_report_company_name) GT 10>
            #left(report.market_report_company_name,10)#... &nbsp; Awards
           <cfelse>
            #report.market_report_company_name# Awards
           </cfif>

	       </a></span>

          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="run_awards.cfm">All Awards</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="run_customers.cfm">Customers</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_feed2.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="run_competitors.cfm">Competitors</a></span>
          </div>

      </cfoutput>

		<!--- Start --->

	      <div class="main_box_2">

	      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">#report.market_report_name#</td>
	              <td class="feed_sub_header" align=right>

			      <cfif session.market_report_access_level GTE 2>

    	          <a href="share.cfm"><img src="/images/icon_sharing.png" width=20 alt="Share Report" title="Share Report" hspace=10></a><a href="share.cfm">Share Report</a>

	              <a href="edit.cfm"><img src="/images/icon_edit.png" width=20 hspace=10><a href="edit.cfm"></a>
	              <a href="edit.cfm">Edit Report</a>

                  &nbsp;&nbsp;

                  </cfif>

	              <a href="index.cfm">All Reports</a></td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr>
                <td class="feed_sub_header">Report For</td>
                <td class="feed_sub_header">Company DUNS</td>
                <td class="feed_sub_header">Search String</td>
                <td class="feed_sub_header" align=center>From</td>
                <td class="feed_sub_header" align=center>To</td>
                <td class="feed_sub_header">Department(s)</td>
                <td class="feed_sub_header">Agency(ies)</td>

            </tr>

            <tr>
                <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #report.market_report_company_name# is "">Not Specified<cfelse>#report.market_report_company_name#</cfif></td>
                <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #report.market_report_company_parent_duns# is "">Not Specified<cfelse>#report.market_report_company_parent_duns#</cfif></td>
                <td class="feed_sub_header" style="font-weight: normal;" valign=top width=400>#replaceNoCase(report.market_report_keywords,'"','',"all")#</td>
                <td class="feed_sub_header" style="font-weight: normal;" valign=top align=center>#dateformat(report.market_report_from,'mm/dd/yyyy')#</td>
                <td class="feed_sub_header" style="font-weight: normal;" valign=top align=center>#dateformat(report.market_report_to,'mm/dd/yyyy')#</td>
                <td class="feed_sub_header" style="font-weight: normal;" valign=top>

                <cfif report.market_report_dept_list is "">
                All Departments
                <cfelse>


                <cfloop index="l" list="#report.market_report_dept_list#">

                 <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
                   select department_name from department
                   where department_code in (#preservesinglequotes(l)#)
                 </cfquery>

                 <cfoutput>#dept.department_name#<br></cfoutput>

                </cfloop>

                </cfif>

                </td>

                <td class="feed_sub_header" style="font-weight: normal;" valign=top>

                <cfif report.market_report_agency_list is "">
                All Agencies
                <cfelse>

                <cfloop index="a" list="#report.market_report_agency_list#">

                 <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
                   select agency_name from agency
                   where agency_code in (#preservesinglequotes(a)#)
                 </cfquery>

                 <cfoutput>#agency.agency_name#<br></cfoutput>

                </cfloop>

                </cfif>


                </td>

            </tr>

            </table>

            </cfoutput>

<!--- Company Stats --->

		<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select count(id) as awards, sum(federal_action_obligation) as obligated, count(distinct(parent_award_id)) as contracts from award_data
		 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

		 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

		 <cfelse>

			 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
			   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
			 <cfelseif report.market_report_dept_list is not "">
			   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
			 <cfelseif report.market_report_agency_list is not "">
			   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
			 </cfif>

		 </cfif>

		 and (recipient_parent_duns = '#report.market_report_company_parent_duns#' or recipient_duns = '#report.market_report_company_duns#')

		 )

		and contains((award_description),'#trim(report.market_report_keywords)#')

		</cfquery>

		<!--- Competitor Stats --->

		<cfquery name="competitors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select sum(federal_action_obligation) as obligated, count(id) as awards, count(distinct(recipient_duns)) as vendors, count(distinct(parent_award_id)) as contracts from award_data
		 left join company on company_duns = recipient_parent_duns
		 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

		 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

		 <cfelse>

			 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
			   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
			 <cfelseif report.market_report_dept_list is not "">
			   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
			 <cfelseif report.market_report_agency_list is not "">
			   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
			 </cfif>

		 </cfif>

		)

		and contains((award_description),'#trim(report.market_report_keywords)#')
		</cfquery>


        <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td colspan=8><hr></td></tr>
            <tr><td height=20></td></tr>
            <tr>
                <td class="feed_header" colspan=3 align=center width=48%><a href="run_select.cfm" align=center>#report.market_report_company_name#</a></td>
                <td width=50>&nbsp;</td>
                <td class="feed_header" colspan=4 align=center width=48%><a href="run_companies.cfm" align=center>The Market</a></td>
            </tr>
            <tr><td height=20></td></tr>


            <tr>
                <td class="big_number" align=center style="background-color: 5368AA; color: FFFFFF;"><a href="run_select.cfm" style="color: FFFFFF;">#numberformat(agencies.contracts,'99,999')#</a></td>
                <td class="big_number" align=center style="background-color: 5368AA; color: FFFFFF;"><a href="run_select.cfm" style="color: FFFFFF;">#numberformat(agencies.awards,'99,999')#</a></td>
                <td class="big_number" align=center style="background-color: 5368AA; color: FFFFFF;">

                <a href="run_select.cfm" style="color: FFFFFF;">
                <cfif agencies.obligated LT 9999>
	                #numberformat(agencies.obligated,'$999,999,999')#
	            <cfelseif agencies.obligated GT 9999 and agencies.obligated LT 999999>
	                #numberformat(evaluate(agencies.obligated/1000),'$999')#K
	            <cfelseif agencies.obligated GTE 1000000 and agencies.obligated LT 1000000000 >
	                #numberformat(evaluate(agencies.obligated/1000000),'$999.9')#m
                <cfelse>
	                #numberformat(evaluate(agencies.obligated/1000000000),'$999.9')#b
                </cfif>



                </a>

                </td>
                <td></td>
                <td class="big_number" align=center style="background-color: 49A16D; color: FFFFFF;"><a href="run_companies.cfm" style="color: FFFFFF;">#numberformat(competitors.contracts,'99,999')#</a></td>
                <td class="big_number" align=center style="background-color: 49A16D; color: FFFFFF;"><a href="run_companies.cfm" style="color: FFFFFF;">#numberformat(competitors.awards,'99,999')#</a></td>
                <td class="big_number" align=center style="background-color: 49A16D; color: FFFFFF;">

                <a href="run_companies.cfm" style="color: FFFFFF;">
                <cfif competitors.obligated LT 9999>
	                #numberformat(competitors.obligated,'$999,999,999')#
	            <cfelseif competitors.obligated GT 9999 and competitors.obligated LT 1000000>
	                #numberformat(evaluate(competitors.obligated/1000),'$999')#k
	            <cfelseif competitors.obligated GTE 1000000 and competitors.obligated LT 1000000000 >
	                #numberformat(evaluate(competitors.obligated/1000000),'$999.9')#m
                <cfelse>
	                #numberformat(evaluate(competitors.obligated/1000000000),'$999.9')#b
                </cfif>
                </a>

                </td>
                <td class="big_number" align=center style="background-color: 49A16D; color: FFFFFF;"><a href="run_companies.cfm" style="color: FFFFFF;">#numberformat(competitors.awards,'99,999')#</a></td>
            </tr>

            <tr>

                <td class="feed_sub_header" align=center>Contracts</td>
                <td class="feed_sub_header" align=center>Awards</td>
                <td class="feed_sub_header" align=center>Wins</td>
                <td width=2%>&nbsp;</td>
                <td class="feed_sub_header" align=center>Contracts</td>
                <td class="feed_sub_header" align=center>Awards</td>
                <td class="feed_sub_header" align=center>Wins</td>
                <td class="feed_sub_header" align=center>Contractors</td>
            </tr>

            <tr><td colspan=8><hr></td></tr>

            </table>

            </cfoutput>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>