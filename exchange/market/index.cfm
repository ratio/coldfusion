<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.market_report_view")>
 <cfset session.market_report_view = 1>
</cfif>

<cfif session.market_report_view is 1>

	<cfquery name="report_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_report_id from sharing
	 where sharing_hub_id = #session.hub# and
		   sharing_report_id is not null and
		   (sharing_to_usr_id = #session.usr_id#)
	</cfquery>

	<cfif report_access.recordcount is 0>
	 <cfset report_list = 0>
	<cfelse>
	 <cfset report_list = valuelist(report_access.sharing_report_id)>
	</cfif>

	<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from market_report
	 join usr on usr_id = market_report_created_by_usr_id
	 where market_report_hub_id = #session.hub# and
		  (market_report_id in (#report_list#) or market_report_created_by_usr_id = #session.usr_id#)
	 order by market_report_name
	</cfquery>

<cfelseif session.market_report_view is 2>

	<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from market_report
	 join usr on usr_id = market_report_created_by_usr_id
	 where market_report_hub_id = #session.hub# and
		   market_report_created_by_usr_id = #session.usr_id#
	 order by market_report_name
	</cfquery>

<cfelseif session.market_report_view is 3>



	<cfquery name="report_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_report_id from sharing
	 where sharing_hub_id = #session.hub# and
		   sharing_report_id is not null and
		   (sharing_to_usr_id = #session.usr_id#)
	</cfquery>

	<cfif report_access.recordcount is 0>
	 <cfset report_list = 0>
	<cfelse>
	 <cfset report_list = valuelist(report_access.sharing_report_id)>
	</cfif>

	<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from market_report
	 join usr on usr_id = market_report_created_by_usr_id
	 where market_report_hub_id = #session.hub# and
		  (market_report_id in (#report_list#) or market_report_created_by_usr_id = #session.usr_id#) and
		   market_report_created_by_usr_id <> #session.usr_id#
	 order by market_report_name
	</cfquery>

</cfif>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top width=100%>

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Market Reports</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right>
	              <form action="report_refresh.cfm">
	              <a href="add.cfm"><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm"></a>
	              <a href="add.cfm"><b>Add Report</b></a>

	              &nbsp;&nbsp;|&nbsp;&nbsp;

                  <b>Filter Reports</b>&nbsp;&nbsp;

                  <select name="market_report_view" class="input_select" style="width: 150px;" onchange="form.submit()">
                   <option value=1 <cfif #session.market_report_view# is 1>selected</cfif>>All Reports
                   <option value=2 <cfif #session.market_report_view# is 2>selected</cfif>>My Reports
                   <option value=3 <cfif #session.market_report_view# is 3>selected</cfif>>Shared with Me
	              </select>

	              </form>

	              </td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif isdefined("u")>
            <cfif u is 3>
             <tr><td class="feed_sub_header" colspan=3 style="color: green;">Market Report has been successfully deleted.</td></tr>
            </cfif>
           </cfif>

           <cfif reports.recordcount is 0>
            <tr><td class="feed_sub_header" style="font-weight: normal;">No Market Reports have been created.</td></tr>
           <cfelse>

           <tr>
              <td class="feed_sub_header">Report Name</td>
              <td class="feed_sub_header">Description</td>
              <td class="feed_sub_header">Search String</td>
              <td class="feed_sub_header" align=center>Report Timeline</td>
              <td class="feed_sub_header">Owner</td>
           </tr>

           <cfset counter = 0>

            <cfoutput query="reports">

            	<cfif counter is 0>
            	 <tr bgcolor="ffffff">
            	<cfelse>
            	 <tr bgcolor="e0e0e0">
            	</cfif>

            		<td class="feed_sub_header" width=300><a href="set_report.cfm?i=#encrypt(market_report_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#market_report_name#</a></td>
            		<td class="feed_sub_header" style="font-weight: normal;" width=300>#market_report_desc#</td>
            		<td class="feed_sub_header" style="font-weight: normal;">#replaceNoCase(market_report_keywords,'"','',"all")#</td>
            		<td class="feed_sub_header" style="font-weight: normal;" align=center width=250>#dateformat(market_report_from,'mm/dd/yyyy')# - #dateformat(market_report_to,'mm/dd/yyyy')#</td>
            		<td class="feed_sub_header" style="font-weight: normal;" width=150>#usr_first_name# #usr_last_name#</td>

            	</tr>

            	<cfif counter is 0>
            	 <cfset counter = 1>
            	<cfelse>
            	 <cfset counter = 0>
            	</cfif>

            </cfoutput>

           </cfif>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>