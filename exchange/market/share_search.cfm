<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="report_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<cfif isdefined("keyword")>

<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  join hub_xref on hub_xref_usr_id = usr_id
  where hub_xref_hub_id = #session.hub# and
  hub_xref_active = 1 and
  usr_id <> #session.usr_id# and
  contains((usr_full_name, usr_first_name, usr_last_name, usr_about, usr_background, usr_keywords, usr_experience),'"#trim(keyword)#"')
  order by usr_last_name, usr_first_name
</cfquery>

<cfquery name="existing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_id from sharing
 join usr on usr_id = sharing_to_usr_id
 where sharing_report_id = #session.market_report_id# and
       sharing_hub_id = #session.hub#
</cfquery>

<cfif existing.recordcount is 0>
 <cfset user_list = 0>
<cfelse>
 <cfset user_list = #valuelist(existing.usr_id)#>
</cfif>

</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">
        <cfoutput>
        #ucase(report_info.market_report_name)#
        </cfoutput>

        </td>
            <td align=right class="feed_sub_header"><a href="share.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="share_search.cfm" method="post">

        <tr>

        <td class="feed_sub_header" style="font-weight: normal;">
        <b>Add User</b>&nbsp;&nbsp;
        <input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  name="keyword" style="width: 250px;" class="input_text" placeholder="Search for name" required>
        &nbsp;
        <input type="submit" name="button" class="button_blue" value="Go">

        </td></tr>
       </form>
   	   </table>

       <cfif not isdefined("keyword")>

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
	         <tr><td class="feed_sub_header" style="font-weight: normal;">Please search for a users name.</td></tr>
	       </table>

       <cfelse>

        <cfif search.recordcount is 0>
	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
	         <tr><td class="feed_sub_header" style="font-weight: normal;">No users were found.<br><br>Note, users who have already been granted share access will not appear in search results.</td></tr>
	       </table>
        <cfelse>

          <form action="share_update.cfm" method="post">

	       <table cellspacing=0 cellpadding=0 border=0 width=100%>


          <tr>
             <td class="feed_sub_header">SELECT</td>
             <td></td>
             <td class="feed_sub_header">NAME</td>
             <td class="feed_sub_header">TITLE</td>
             <td class="feed_sub_header">EMAIL</td>
          </tr>

          <cfoutput query="search">

             <tr>
                 <td width=100 class="feed_sub_header">

                 <cfif listfind(user_list,search.usr_id)>
                 <span style="color: green;">Granted</span>
                 <cfelse>
                 	&nbsp;&nbsp;&nbsp;<input type="checkbox" class="input_text" name="share_id" value=#encrypt(search.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;">
                 </cfif>

                 </td>
                 <td width=80 class="feed_sub_header">

				 <cfif #search.usr_photo# is "">
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(search.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" height=40 width=40 border=0>
				 <cfelse>
				  <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(search.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 0px;" src="#media_virtual#/#search.usr_photo#" height=40 width=40 border=0>
				 </cfif>

				</td>

			    <td width=200 class="feed_sub_header"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(search.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#ucase(search.usr_first_name)# #ucase(search.usr_last_name)#</a></td>
			    <td width=200 class="feed_sub_header" style="font-weight: normal;">#ucase(search.usr_title)#</td>
			    <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(search.usr_email))#</td>
			 </tr>

			    <tr><td colspan=5><hr></td></tr>

           </cfoutput>

           <tr><td height=10></td></tr>
           <tr>
              <td colspan=3><input type="submit" name="button" class="button_blue_large" value="Share" onclick="return confirm('Grant Sharing?\r\nAre you sure you want to grant sharing access to the selected users?');"></td></tr>

           </form>

	       </table>

          </cfif>

        </cfif>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>