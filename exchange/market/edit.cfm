<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="report" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 <cfif report.market_report_dept_list is not "">
 	where department_code not in (#preservesinglequotes(report.market_report_dept_list)#)
 </cfif>
 order by department_name
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 <cfif report.market_report_agency_list is not "">
 	where agency_code not in (#preservesinglequotes(report.market_report_agency_list)#)
 </cfif>
 order by agency_name
</cfquery>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

			<script type="text/javascript">
				$(function () {
					function moveItems(origin, dest) {
						$(origin).find(':selected').appendTo(dest);
					}

					function moveAllItems(origin, dest) {
						$(origin).children().appendTo(dest);
					}

					$('#left').click(function () {
						moveItems('#sbTwo_Dept', '#sbOne_Dept');
					});

					$('#right').on('click', function () {
						moveItems('#sbOne_Dept', '#sbTwo_Dept');
					});

					$('#leftall').on('click', function () {
						moveAllItems('#sbTwo_Dept', '#sbOne_Dept');
					});

					$('#rightall').on('click', function () {
						moveAllItems('#sbOne_Dept', '#sbTwo_Dept');
					});
				});
			</script>


			<script type="text/javascript">
				$(function () {
					function moveItems(origin, dest) {
						$(origin).find(':selected').appendTo(dest);
					}

					function moveAllItems(origin, dest) {
						$(origin).children().appendTo(dest);
					}

					$('#left2').click(function () {
						moveItems('#sbTwo_Agency', '#sbOne_Agency');
					});

					$('#right2').on('click', function () {
						moveItems('#sbOne_Agency', '#sbTwo_Agency');
					});

					$('#leftall2').on('click', function () {
						moveAllItems('#sbTwo_Agency', '#sbOne_Agency');
					});

					$('#rightall2').on('click', function () {
						moveAllItems('#sbOne_Agency', '#sbTwo_Agency');
					});
				});
			</script>

<cfinclude template = "/exchange/include/header.cfm">

<cfinclude template = "check_access.cfm">

<cfif session.market_report_access_level GTE 2>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Edit Market Report</td>
	              <td class="feed_sub_header" align=right><a href="run_report.cfm">Return</a></td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

          <form action="db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfoutput>

			  <tr>
				 <td class="feed_sub_header" width=200>Report Name</td>
				 <td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 860px;" name="market_report_name" value="#report.market_report_name#"></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" width=200 valign=top>Description</td>
				 <td><textarea name="market_report_desc" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  style="width: 860px; height: 100px;">#report.market_report_desc#</textarea></td>
			  </tr>

              <tr>
				 <td class="feed_sub_header" width=200 valign=middle>Select Timeframe</td>
				 <td class="feed_sub_header" style="font-weight: normal;" valign=middle>

				 <b>From:</b>&nbsp;&nbsp;

				 <input type="date" class="input_date" name="market_report_from" value="#report.market_report_from#" required>&nbsp;&nbsp;&nbsp;

				 <b>To:</b>&nbsp;&nbsp;

				 <input type="date" class="input_date" name="market_report_to" value="#report.market_report_to#" required>

                 <span class="link_small_gray">Timeline is used to find awards related to this Market Report.

				 </td></tr>

			  <tr>
				 <td class="feed_sub_header" width=200>Company Name</td>
				 <td><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 560px;" name="market_report_company_name" value="#report.market_report_company_name#"></td>
			  </tr>

			  <tr>
				 <td class="feed_sub_header" width=200>Company DUNS</td>
				 <td class="feed_sub_header" style="font-weight: normal;"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 162px;" name="market_report_company_duns" value="#report.market_report_company_duns#">
				  &nbsp;&nbsp;
                  <b>Or, Parent Company DUNS</b>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 162px;" name="market_report_company_parent_duns" value="#report.market_report_company_parent_duns#"></td>
			  </tr>

			  <tr><td></td><td colspan=2 class="link_small_gray">Company information is used to compare the results for the Market.  Company can be your company or a competitor.</td></tr>

              <tr><td height=10></td></tr>


              </cfoutput>

			  <tr><td colspan=2><hr></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td height=20></td></tr>

          <tr>
             <td class="feed_sub_header" width=200 valign=middle>Select Department(s) to<br>include in the report</td>

          <td width=300>


			<select id="sbOne_Dept" class="input_select" multiple="multiple" style="width: 400px; height: 200px;">
				<cfoutput query="department">
				 <option value="'#department_code#'">#department_name#</option>
				</cfoutput>
			</select>

            </td><td width=50 align=center>

			<input type="button" class="button_blue" style="margin-bottom: 10px; margin-right: 10px;" id="right" value=">" /><br>
			<input type="button" class="button_blue" style="margin-right: 10px;" id="left" value="<" />

			</td><td>

			<select name="market_report_dept_list" id="sbTwo_Dept" class="input_select" multiple="multiple" style="width: 400px; height: 200px;">

            <cfif report.market_report_dept_list is not "">

				<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select * from department
					where department_code in (#preservesinglequotes(report.market_report_dept_list)#)
				 order by department_name
				</cfquery>

				<cfif dept.recordcount GT 0>
				<cfoutput query="dept">
				 <option value="'#department_code#'" selected>#department_name#</option>
				</cfoutput>
				</cfif>

            </cfif>

			</select>

			</td>

			</tr>
	     <tr><td height=30></td></tr>

         <tr>
             <td class="feed_sub_header">Select Agency(ies) to<br>include in the report</td>
          <td width=300>

			<select id="sbOne_Agency" class="input_select" name="agencies" multiple="multiple" style="width: 400px; height: 200px;">
				<cfoutput query="agency">
				 <option value="'#agency_code#'">#agency_name#</option>
				</cfoutput>
			</select>

            </td><td width=50 align=center>

			<input type="button" class="button_blue" style="margin-bottom: 10px; margin-right: 10px;" id="right2" value=">" /><br>
			<input type="button" class="button_blue" style="margin-right: 10px;" id="left2" value="<" />

			</td><td>

			<select name="market_report_agency_list" id="sbTwo_Agency" class="input_select" multiple="multiple" style="width: 400px; height: 200px;">

            <cfif report.market_report_agency_list is not "">

				<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select * from agency
				 where agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
				 order by agency_name
				</cfquery>

				<cfif agencies.recordcount GT 0>
				<cfoutput query="agencies">
				 <option value="'#agency_code#'" selected>#agency_name#</option>
				</cfoutput>
				</cfif>

             </cfif>

			</select>

			</td>

			</tr>

            <tr><td height=10></td></tr>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header" width=200>Keywords</td>
				 <td colspan=3><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  class="input_text" style="width: 860px;" name="market_report_keywords" value='#replaceNoCase(report.market_report_keywords,'"','',"all")#' required></td>
			  </tr>

              <tr><td></td>
                  <td class="link_small_gray" colspan=3>Format - (machine learning or architecture and defense)</td></tr>


			  <tr><td height=10></td></tr>

            </cfoutput>

            <tr><td colspan=4><hr></td></tr>
            <tr><td height=10></td></tr>

            <tr><td></td><td colspan=2><input type="submit" name="button" value="Update" class="button_blue_large">

            <cfif report.market_report_created_by_usr_id is #session.usr_id#>
            &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Market Report?\r\nAre you sure you want to delete this Market Report?');">
            </cfif>

            </td></tr>

			<cfoutput>
			  <input type="hidden" name="form_token" value=#CSRFGenerateToken( forcenew = true)#>
			</cfoutput>

          </form>

          </td></tr>

          </table>

		  </div>

		<!--- End --->

      </td><td valign=top width=185>

      <table>
       <tr><td height=40>&nbsp;</td></tr>
      </table>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>