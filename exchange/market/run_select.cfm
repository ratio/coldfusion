<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -6px;
    margin-top: 20px;
    margin-left: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    padding-right: 20px;
    display: inline-block;
    margin-left: 0px;
    width: auto;
    margin-right: -4px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}
.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfquery name="report" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from market_report
 where market_report_id = #session.market_report_id#
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select id, recipient_name, recipient_duns, action_date, award_id_piid, modification_number, awarding_agency_name,
        awarding_sub_agency_name, awarding_office_name, award_description,
        federal_action_obligation, total_dollars_obligated, base_and_exercised_options_value,
        base_and_all_options_value, current_total_value_of_award, potential_total_value_of_award
        from award_data where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

 <cfelse>

	 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
       and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
     <cfelseif report.market_report_dept_list is not "">
       and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
     <cfelseif report.market_report_agency_list is not "">
       and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
     </cfif>

 </cfif>

and (recipient_parent_duns = '#report.market_report_company_parent_duns#' or recipient_duns = '#report.market_report_company_duns#')

 )

and contains((award_description),'#trim(report.market_report_keywords)#')

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 3>
	 order by recipient_name ASC
	<cfelseif #sv# is 30>
	 order by recipient_name DESC
	<cfelseif #sv# is 4>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 40>
	 order by awarding_agency_name DESC
	<cfelseif #sv# is 5>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 50>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 6>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 60>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 7>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 70>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 8>
	 order by total_dollars_obligated DESC
	<cfelseif #sv# is 80>
	 order by total_dollars_obligated ASC
	<cfelseif #sv# is 9>
	 order by base_and_exercised_options_value DESC
	<cfelseif #sv# is 90>
	 order by base_and_exercised_options_value ASC
	<cfelseif #sv# is 11>
	 order by base_and_all_options_value DESC
	<cfelseif #sv# is 110>
	 order by base_and_all_options_value ASC
	<cfelseif #sv# is 12>
	 order by current_total_value_of_award DESC
	<cfelseif #sv# is 120>
	 order by current_total_value_of_award ASC
	<cfelseif #sv# is 13>
	 order by potential_total_value_of_award DESC
	<cfelseif #sv# is 130>
	 order by potential_total_value_of_award ASC
	</cfif>

</cfquery>

   <cfif isdefined("export")>
	 <cfinclude template="/exchange/include/export_to_excel.cfm">
	</cfif>

  <cfparam name="URL.PageId" default="0">
  <cfset RecordsPerPage = 250>
  <cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
  <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
  <cfset EndRow = StartRow+RecordsPerPage-1>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>

      <td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

      </td>

	  <td valign=top width=100%>

		<!--- Start --->

		<cfoutput>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_report.cfm">Dashboard</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_companies.cfm">The Market</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_assist.png" width=20 valign=absmiddle>&nbsp;&nbsp;

           <a href="run_select.cfm">

           <cfif len(report.market_report_company_name) GT 10>
            #left(report.market_report_company_name,10)#... &nbsp; Awards
           <cfelse>
            #report.market_report_company_name# Awards
           </cfif>

	       </a></span>

          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_awards.cfm">All Awards</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_customers.cfm">Customers</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_assist.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="run_competitors.cfm">Competitors</a></span>
          </div>


        </cfoutput>

	      <div class="main_box_2">

	      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">#report.market_report_name#</td>
	              <td class="feed_sub_header" align=right><a href="index.cfm">All Reports</a></td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif agencies.recordcount is 0>

            <tr><td class="feed_sub_header" style="font-weight: normal;">No information was found.</td></tr>

           <cfelse>

            <tr><td>


          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif isdefined("u")>
           <cfif u is 1>
            <tr><td class="feed_sub_header" colspan=3 style="color: green;">Keyword / Search String updated.</td></tr>
           </cfif>
          </cfif>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" colspan=16>No awards or mods were found with the information you entered.</td></tr>
          <cfelse>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td class="feed_sub_header">

              Awards Found - <cfoutput>#numberformat(agencies.recordcount,'999,999')#</cfoutput>
              </td>

              <td align=right class="feed_sub_header">
				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&sv=#sv#">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
              </td></tr>

              </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>


              <tr>
              <td class="feed_sub_header" style="font-weight: normal;">

              <cfoutput>
	              </td><td align=right class="feed_sub_header"><a href="run_select.cfm?export=1&sv=#sv#"><img src="/images/icon_export_excel.png" width=20 border=0 hspace=10 alt="Export to Excel" title="Export to Excel"></a><a href="run_select.cfm?export=1&sv=#sv#">Download to Excel</a>
              </cfoutput>

              </td></tr>

              <tr><td colspan=2><hr></td></tr>

          </table>


          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td height=20></td></tr>
          <tr>
              <td class="feed_header" align=center>BY DEPARTMENT</td>
              <td></td>
              <td class="feed_header" align=center>BY AGENCY</td>
              <td></td>
              <td class="feed_header" align=center>BY CONTRACT</td>
          </tr>

          <tr><td width=32%>

				<cfquery name="dept_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select awarding_agency_name, sum(federal_action_obligation) as obligated
						from award_data

				 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

				 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

				 <cfelse>

					 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
					   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
					 <cfelseif report.market_report_dept_list is not "">
					   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
					 <cfelseif report.market_report_agency_list is not "">
					   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
					 </cfif>

				 </cfif>

				and (recipient_parent_duns = '#report.market_report_company_parent_duns#' or recipient_duns = '#report.market_report_company_duns#')

				 )

				and contains((award_description),'#trim(report.market_report_keywords)#')
				group by awarding_agency_name
				order by obligated DESC
				</cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('vendor_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="dept_total">
 						  <cfif obligated GTE 0>
							   ['#awarding_agency_name#',#round(obligated)#,''],
 						  </cfif>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 325,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="vendor_graph" style="width: 100%;"></div>

             </td><td width=75>&nbsp;</td><td width=32%>

				<cfquery name="agency_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select awarding_sub_agency_name, sum(federal_action_obligation) as obligated
						from award_data

				 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

				 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

				 <cfelse>

					 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
					   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
					 <cfelseif report.market_report_dept_list is not "">
					   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
					 <cfelseif report.market_report_agency_list is not "">
					   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
					 </cfif>

				 </cfif>

				and (recipient_parent_duns = '#report.market_report_company_parent_duns#' or recipient_duns = '#report.market_report_company_duns#')

				 )

				and contains((award_description),'#trim(report.market_report_keywords)#')
				group by awarding_sub_agency_name
				order by obligated DESC
				</cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('office_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="agency_total">
 						  <cfif obligated GTE 0>
							   ['#awarding_sub_agency_name#',#round(obligated)#,''],
 						  </cfif>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 325,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="office_graph" style="width: 100%;"></div>


             </td>

			<td width=75>&nbsp;</td><td width=32%>

				<cfquery name="award_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select award_id_piid, sum(federal_action_obligation) as obligated
						from award_data

				 where ((action_date between '#report.market_report_from#' and '#report.market_report_to#')

				 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">

				 <cfelse>

					 <cfif report.market_report_dept_list is "" and report.market_report_agency_list is "">
					   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#) or awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
					 <cfelseif report.market_report_dept_list is not "">
					   and awarding_agency_code in (#preservesinglequotes(report.market_report_dept_list)#)
					 <cfelseif report.market_report_agency_list is not "">
					   and awarding_sub_agency_code in (#preservesinglequotes(report.market_report_agency_list)#)
					 </cfif>

				 </cfif>

				and (recipient_parent_duns = '#report.market_report_company_parent_duns#' or recipient_duns = '#report.market_report_company_duns#')

				 )

				and contains((award_description),'#trim(report.market_report_keywords)#')
				group by award_id_piid
				order by obligated DESC
				</cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('contract_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="award_total">
 						  <cfif obligated GTE 0>
							   ['#award_id_piid#',#round(obligated)#,''],
 						  </cfif>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 325,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="contract_graph" style="width: 100%;"></div>

             </td>

             </tr>
           </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
				 <td class="text_xsmall" width=75><a href="run_select.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
				 <td class="text_xsmall"><a href="run_select.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract ##</b></a></td>
				 <td class="text_xsmall"><a href="run_select.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Vendor</b></a></td>
			 	 <td class="text_xsmall"><a href="run_select.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Department</b></a></td>
				 <td class="text_xsmall"><a href="run_select.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Agency</b></a></td>
				 <td class="text_xsmall"><a href="run_select.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Office</b></a></td>
				 <td class="text_xsmall"><b>Award Description</b></td>
				 <td class="text_xsmall" align=right><a href="run_select.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Action<br>Obligation</b></a></td>
				 <td class="text_xsmall" align=right><a href="run_select.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Base & Awarded<br>Options</b></a></td>
				 <td class="text_xsmall" align=right><a href="run_select.cfm?<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Base & All<br>Options</b></a></td>
				 <td class="text_xsmall" align=right><a href="run_select.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Total<br>Obligations</b></a></td>


				 <td class="text_xsmall" align=right><a href="run_select.cfm?<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Current<br>Total Value</b></a></td>
				 <td class="text_xsmall" align=right><a href="run_select.cfm?<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Potential<br>Total Value</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

			   <cfoutput>

				   <td class="text_xsmall" valign=top width=100>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top width=120><a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener"><b>#agencies.award_id_piid#</b></a><br>MOD - #agencies.modification_number#</td>
				   <td class="text_xsmall" valign=top width=200><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener"><b>#agencies.recipient_name#</b></a></td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_agency_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_sub_agency_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_office_name#</td>
				   <td class="text_xsmall" valign=top width=300>#agencies.award_description#</td>
				   <td class="text_xsmall" valign=top align=right width=125>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>
				   <td class="text_xsmall" valign=top align=right width=125>#numberformat(agencies.base_and_exercised_options_value,'$999,999,999')#</td>
				   <td class="text_xsmall" valign=top align=right width=125>#numberformat(agencies.base_and_all_options_value,'$999,999,999')#</td>
				   <td class="text_xsmall" valign=top align=right width=125>#numberformat(agencies.total_dollars_obligated,'$999,999,999')#</td>
				   <td class="text_xsmall" valign=top align=right width=125>#numberformat(agencies.current_total_value_of_award,'$999,999,999')#</td>
				   <td class="text_xsmall" valign=top align=right width=125>#numberformat(agencies.potential_total_value_of_award,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

		  <cfset tot = tot + #federal_action_obligation#>

          </cfif>

			  <cfif CurrentRow eq EndRow>
			   <cfbreak>
			  </cfif>

          </cfloop>

          <tr><td colspan=17><hr></td></tr>
          <tr>
             <td class="feed_option" colspan=7><b>Total</b></td>
             <td class="feed_option" align=right><b><cfoutput>#numberformat(tot,'$999,999,999')#</cfoutput></b></td>
             <td colspan=3>&nbsp;</td>
          </tr>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

            </td></tr>

           </cfif>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>