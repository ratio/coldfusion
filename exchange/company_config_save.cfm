<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  update usr
  set usr_dashboard_comp = 1,
      usr_dashboard_comp_keywords = '#trim(usr_dashboard_comp_keywords)#'
  where usr_id = #session.usr_id#
</cfquery>

<cflocation URL="/exchange/index.cfm?u=11" addtoken="no">