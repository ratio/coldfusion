<div class="left_box">

<cfquery name="reports" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_type = 'Buying Pattern' and
        insight_hub_id = #session.hub#
  order by insight_order
</cfquery>

	<center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><a href="/exchange/insights/"><b>Insights</b></a></td>
		      <td align=right valign=top></td></tr>
		  <tr><td><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfif reports.recordcount is 0>

		   <tr><td class="feed_sub_header" style="font-weight: normal;">No Insights have been created.</td></tr>

		  <cfelse>

		  <cfset count = 1>

		  <cfloop query="reports">

		  <cfoutput>

				   <tr>
				       <td width=40><a href="run_insight.cfm?i=#encrypt(reports.insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/insight.png" height=15 border=0></a></td>
					   <td class="link_med_blue"><a href="run_insight.cfm?i=#encrypt(reports.insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><cfif len(reports.insight_name) GT 20>#left(reports.insight_name,'20')#...<cfelse>#reports.insight_name#</cfif></a></td>
					</tr>

			  <cfif count is not reports.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

			  </cfoutput>

			  </td></tr>

			  </cfloop>

	      </cfif>

	</center>

	</table>

</div>