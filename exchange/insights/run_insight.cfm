<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset weekly_end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset weekly_start_date = #dateformat(dateadd("d",-7,now()),'mm/dd/yyyy')#>

<cfset monthly_end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset monthly_start_date = #dateformat(dateadd("d",-30,now()),'mm/dd/yyyy')#>

<cfset quarterly_end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset quarterly_start_date = #dateformat(dateadd("d",-90,now()),'mm/dd/yyyy')#>

<cfset halfyear_end_date = #dateformat(dateadd("d",-91,now()),'mm/dd/yyyy')#>
<cfset halfyear_start_date = #dateformat(dateadd("d",-180,now()),'mm/dd/yyyy')#>

<cfset year_end_date = #dateformat(dateadd("d",-181,now()),'mm/dd/yyyy')#>
<cfset year_start_date = #dateformat(dateadd("d",-365,now()),'mm/dd/yyyy')#>

<cfset graph_start_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset graph_end_date = #dateformat(dateadd("d",-365,now()),'mm/dd/yyyy')#>

<cfset opp_start_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset opp_end_date = #dateformat(dateadd("d",-180,now()),'mm/dd/yyyy')#>

<cfquery name="insight" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from insight
 where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="sb_fbo_monthly" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
  where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
  and fbo_pub_date between '#monthly_start_date#' and '#monthly_end_date#'
  and (fbo_setaside_original <> '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="sb_fbo_quarterly" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
  where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
  and fbo_pub_date between '#quarterly_start_date#' and '#quarterly_end_date#'
  and (fbo_setaside_original <> '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="sb_fbo_halfyear" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
  where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
  and fbo_pub_date between '#halfyear_start_date#' and '#halfyear_end_date#'
  and (fbo_setaside_original <> '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="other_fbo_monthly" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
  where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
  and fbo_pub_date between '#monthly_start_date#' and '#monthly_end_date#'
  and (fbo_setaside_original is null or fbo_setaside_original = '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="other_fbo_quarterly" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
  where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
  and fbo_pub_date between '#quarterly_start_date#' and '#quarterly_end_date#'
  and (fbo_setaside_original is null or fbo_setaside_original = '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>

<cfquery name="other_fbo_halfyear" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(distinct(fbo_solicitation_number)) as total from fbo
  where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
  and fbo_pub_date between '#halfyear_start_date#' and '#halfyear_end_date#'
  and (fbo_setaside_original is null or fbo_setaside_original = '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
</cfquery>


<!--- All SB Contracts --->

<cfquery name="contract_sb_quarterly" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(award_id_piid) as total, sum(federal_action_obligation) as amount from award_data
  where (contains((award_description),'#trim(insight.insight_keywords)#') )
  and action_date between '#quarterly_start_date#' and '#quarterly_end_date#'
  and (type_of_set_aside_code <> 'None' or type_of_set_aside_code is not null or type_of_set_aside_code <> '')
</cfquery>

<cfquery name="contract_sb_halfyear" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(award_id_piid) as total, sum(federal_action_obligation) as amount from award_data
  where (contains((award_description),'#trim(insight.insight_keywords)#') )
  and action_date between '#halfyear_start_date#' and '#halfyear_end_date#'
  and (type_of_set_aside_code <> 'None' or type_of_set_aside_code is not null or type_of_set_aside_code <> '')
</cfquery>

<cfquery name="contract_sb_year" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(award_id_piid) as total, sum(federal_action_obligation) as amount from award_data
  where (contains((award_description),'#trim(insight.insight_keywords)#') )
  and action_date between '#year_start_date#' and '#year_end_date#'
  and (type_of_set_aside_code <> 'None' or type_of_set_aside_code is not null or type_of_set_aside_code <> '')
</cfquery>

<!--- All Contracts --->

<cfquery name="contract_all_quarterly" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(award_id_piid) as total, sum(federal_action_obligation) as amount from award_data
  where (contains((award_description),'#trim(insight.insight_keywords)#') )
  and action_date between '#quarterly_start_date#' and '#quarterly_end_date#'
  and (type_of_set_aside_code = 'None' or type_of_set_aside_code is null or type_of_set_aside_code = '')
</cfquery>

<cfquery name="contract_all_halfyear" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(award_id_piid) as total, sum(federal_action_obligation) as amount from award_data
  where (contains((award_description),'#trim(insight.insight_keywords)#') )
  and action_date between '#halfyear_start_date#' and '#halfyear_end_date#'
  and (type_of_set_aside_code = 'None' or type_of_set_aside_code is null or type_of_set_aside_code = '')
</cfquery>

<cfquery name="contract_all_year" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(award_id_piid) as total, sum(federal_action_obligation) as amount from award_data
  where (contains((award_description),'#trim(insight.insight_keywords)#') )
  and action_date between '#year_start_date#' and '#year_end_date#'
  and (type_of_set_aside_code = 'None' or type_of_set_aside_code is null or type_of_set_aside_code = '')
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/insights/reports.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Insights</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right><a href="index.cfm">Return</a>
	              </td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_header" align=center>#insight.insight_name#</td></tr>
           <tr><td class="feed_header" style="font-weight: normal;" align=center><i>#replaceNoCase(insight.insight_keywords,'"','',"all")#</i></td></tr>
           <tr><td height=20></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
              <td align=center class="feed_header" colspan=5>New Contracts (Small Business)</td>
              <td></td>
              <td class="feed_header" colspan=5 align=center>New Contracts (Others)</td>
           </tr>

           <tr><td height=20></td></tr>

           <tr>
              <td class="feed_header" align=center width=13%>Last 6 Months</td>
              <td width=1%>&nbsp;</td>
              <td class="feed_header" align=center width=13%>Last 3 Months</td>
              <td width=1%>&nbsp;</td>
              <td class="feed_header" align=center width=13%>Last 30 Days</td>
              <td width=6%>&nbsp;</td>
              <td class="feed_header" align=center width=13%>Last 6 Months</td>
              <td width=1%>&nbsp;</td>
              <td class="feed_header" align=center width=13%>Last 3 Months</td>
              <td width=1%>&nbsp;</td>
              <td class="feed_header" align=center width=13%>Last 30 Days</td>

           </tr>

           <tr><td height=10></td></tr>

           <tr bgcolor="ffffff">

              <td class="feed_header" style="font-size: 40px; background-color: green; color: ffffff;" align=center height=50><cfif sb_fbo_halfyear.total is 0>0<cfelse><a href="new.cfm?i=#i#&d=halfyear&t=sb" style="font-size: 40px; background-color: green; color: ffffff;">#numberformat(sb_fbo_halfyear.total,'9,999')#</cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: green; color: ffffff;" align=center height=50><cfif sb_fbo_quarterly.total is 0>0<cfelse><a href="new.cfm?i=#i#&d=quarterly&t=sb" style="font-size: 40px; background-color: green; color: ffffff;">#numberformat(sb_fbo_quarterly.total,'9,999')#</cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: green; color: ffffff;" align=center height=50><cfif sb_fbo_monthly.total is 0>0<cfelse><a href="new.cfm?i=#i#&d=monthly&t=sb" style="font-size: 40px; background-color: green; color: ffffff;">#numberformat(sb_fbo_monthly.total,'9,999')#</cfif></a></td>

              <td width=100>&nbsp;</td>

              <td class="feed_header" style="font-size: 40px; background-color: green; color: ffffff;" align=center height=50><cfif other_fbo_halfyear.total is 0>0<cfelse><a href="new.cfm?i=#i#&d=halfyear&t=ot" style="font-size: 40px; background-color: green; color: ffffff;">#numberformat(other_fbo_halfyear.total,'9,999')#</cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: green; color: ffffff;" align=center height=50><cfif other_fbo_quarterly.total is 0>0<cfelse><a href="new.cfm?i=#i#&d=quarterly&t=ot" style="font-size: 40px; background-color: green; color: ffffff;">#numberformat(other_fbo_quarterly.total,'9,999')#</cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: green; color: ffffff;" align=center height=50><cfif other_fbo_monthly.total is 0>0<cfelse><a href="new.cfm?i=#i#&d=monthly&t=ot" style="font-size: 40px; background-color: green; color: ffffff;">#numberformat(other_fbo_monthly.total,'9,999')#</cfif></td>

          </tr>

          </cfoutput>

           <tr><td height=30></td></tr>

           <tr>
              <td colspan=5 align=center width=49%>

              <cfquery name="opp_award_graph" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 			   select DATEPART(Year, fbo_pub_date) Year, DATEPART(Month, fbo_pub_date) Month, DATENAME(Month, fbo_pub_date) Monthname, count(distinct(fbo_solicitation_number)) as total from fbo
			   where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
			   and fbo_pub_date between '#opp_end_date#' and '#opp_start_date#'
               and (fbo_setaside_original <> '')
               and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
			   GROUP BY DATEPART(Year, fbo_pub_date), DATEPART(Month, fbo_pub_date), DATENAME(Month, fbo_pub_date)
			   ORDER BY Year, Month
			  </cfquery>

              <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
              <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

				  <cfset record = 1>

						var data = google.visualization.arrayToDataTable([
						  ['Month', 'New Contracts'],

						  <cfoutput query="opp_award_graph">
						    ['#monthname#',#round(total)#],
						  </cfoutput>
						]);


			      var options = {
                    chartArea:{right: 20, left:70,top:20,width:'93%',height:'80%'},
                    series: {
					            0: { lineWidth: 3 },
                                1: { lineWidth: 3 },
                                },
                    pointSize: 10,
                    pointShape: 'circle',
                    hAxis: { format: '', textStyle: {color: 'black', fontSize: 11}},
                    vAxis: { format: '', textStyle: {color: 'black', fontSize: 11}},
                    legend: { position: 'bottom' }
				  };

                var chart = new google.visualization.LineChart(document.getElementById('opp_chart_1'));
                chart.draw(data, options);
				}
			  </script>

			  <div id="opp_chart_1" style="height: 250px;"></div>

              </td>

              <td></td>

              <td colspan=5 width=49%>

              <cfquery name="opp_award_graph" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 			   select DATEPART(Year, fbo_pub_date) Year, DATEPART(Month, fbo_pub_date) Month, DATENAME(Month, fbo_pub_date) Monthname, count(distinct(fbo_solicitation_number)) as total from fbo
			   where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )
			   and fbo_pub_date between '#opp_end_date#' and '#opp_start_date#'
               and (fbo_setaside_original is null or fbo_setaside_original = '')
               and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
			   GROUP BY DATEPART(Year, fbo_pub_date), DATEPART(Month, fbo_pub_date), DATENAME(Month, fbo_pub_date)
			   ORDER BY Year, Month
			  </cfquery>

              <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
              <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

				  <cfset record = 1>

						var data = google.visualization.arrayToDataTable([
						  ['Month', 'New Contracts'],

						  <cfoutput query="opp_award_graph">
						    ['#monthname#',#round(total)#],
						  </cfoutput>
						]);


			      var options = {
                    chartArea:{right: 20, left:70,top:20,width:'93%',height:'80%'},
                    series: {
					            0: { lineWidth: 3 },
                                1: { lineWidth: 3 },
                                },
                    pointSize: 10,
                    pointShape: 'circle',
                    hAxis: { format: '', textStyle: {color: 'black', fontSize: 11}},
                    vAxis: { format: '', textStyle: {color: 'black', fontSize: 11}},
                    legend: { position: 'bottom' }
				  };

                var chart = new google.visualization.LineChart(document.getElementById('opp_chart_2'));
                chart.draw(data, options);
				}
			  </script>

			  <div id="opp_chart_2" style="height: 250px;"></div>


              </td>

           </tr>

           <tr><td height=20 width=2%>&nbsp;</td></tr>


           <cfoutput>

           <tr><td colspan=12><hr></td></tr>
           <tr><td height=30></td></tr>

           <tr>
              <td align=center class="feed_header" colspan=5>Recent Awards (Small Business)</td>
              <td></td>
              <td class="feed_header" colspan=5 align=center>Recent Awards (Others)</td>
           </tr>

           <tr><td height=20></td></tr>

           <tr>
              <td class="feed_header" align=center width=15%>Last 6-12 Months</td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" align=center width=15%>Last 3-6 Months</td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" align=center width=15%>Last 3 Months</td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" align=center width=15%>Last 6-12 Months</td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" align=center width=15%>Last 3-6 Months</td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" align=center width=15%>Last 3 Months</td>
           </tr>

           <tr><td height=10></td></tr>

           <tr bgcolor="ffffff">

              <td class="feed_header" style="font-size: 40px; background-color: 4C7988; color: ffffff;" align=center height=50><cfif contract_sb_year.total is 0>0<cfelse><a href="sb_awards.cfm?i=#i#&d=year" style="font-size: 40px; background-color: 4C7988; color: ffffff;">#numberformat(contract_sb_year.total,'9,999')#</a></cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: 4C7988; color: ffffff;" align=center height=50><cfif contract_sb_halfyear.total is 0>0<cfelse><a href="sb_awards.cfm?i=#i#&d=halfyear" style="font-size: 40px; background-color: 4C7988; color: ffffff;">#numberformat(contract_sb_halfyear.total,'9,999')#</a></cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: 4C7988; color: ffffff;" align=center height=50><cfif contract_sb_quarterly.total is 0>0<cfelse><a href="sb_awards.cfm?i=#i#&d=quarterly" style="font-size: 40px; background-color: 4C7988; color: ffffff;">#numberformat(contract_sb_quarterly.total,'9,999')#</a></cfif></td>

              <td width=100>&nbsp;</td>

              <td class="feed_header" style="font-size: 40px; background-color: 4C7988; color: ffffff;" align=center height=50><cfif contract_all_year.total is 0>0<cfelse><a href="other_awards.cfm?i=#i#&d=year" style="font-size: 40px; background-color: 4C7988; color: ffffff;">#numberformat(contract_all_year.total,'9,999')#</a></cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: 4C7988; color: ffffff;" align=center height=50><cfif contract_all_halfyear.total is 0>0<cfelse><a href="other_awards.cfm?i=#i#&d=halfyear" style="font-size: 40px; background-color: 4C7988; color: ffffff;">#numberformat(contract_all_halfyear.total,'9,999')#</a></cfif></td>
              <td width=30>&nbsp;</td>
              <td class="feed_header" style="font-size: 40px; background-color: 4C7988; color: ffffff;" align=center height=50><cfif contract_all_quarterly.total is 0>0<cfelse><a href="other_awards.cfm?i=#i#&d=quarterly" style="font-size: 40px; background-color: 4C7988; color: ffffff;">#numberformat(contract_all_quarterly.total,'9,999')#</a></cfif></td>

           </tr>

           <tr><td height=10></td></tr>

           <tr bgcolor="ffffff">

              <td class="feed_sub_header" align=center>#numberformat(contract_sb_year.amount,'$999,999,999')#</td>
              <td width=30>&nbsp;</td>
              <td class="feed_sub_header" align=center>#numberformat(contract_sb_halfyear.amount,'$999,999,999')#</td>
              <td width=30>&nbsp;</td>
              <td class="feed_sub_header" align=center>#numberformat(contract_sb_quarterly.amount,'$999,999,999')#</td>

              <td width=100>&nbsp;</td>

              <td class="feed_sub_header" align=center>#numberformat(contract_all_year.amount,'$999,999,999')#</td>
              <td width=30>&nbsp;</td>
              <td class="feed_sub_header" align=center>#numberformat(contract_all_halfyear.amount,'$999,999,999')#</td>
              <td width=30>&nbsp;</td>
              <td class="feed_sub_header" align=center>#numberformat(contract_all_quarterly.amount,'$999,999,999')#</td>

           </tr>

          </cfoutput>

          <tr><td height=20></td></tr>

           <tr>
              <td colspan=5 align=center width=49%>

              <cfquery name="sb_award_graph" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 			   select DATEPART(Year, action_date) Year, DATEPART(Month, action_date) Month, DATENAME(Month, action_date) Monthname, count(distinct(award_id_piid)) as total, sum(federal_action_obligation) as amount from award_data
			   where (contains((award_description),'#trim(insight.insight_keywords)#') )
			   and action_date between '#graph_end_date#' and '#graph_start_date#'
			   and (type_of_set_aside_code <> 'None' or type_of_set_aside_code is not null or type_of_set_aside_code <> '')
			   GROUP BY DATEPART(Year, action_date), DATEPART(Month, action_date), DATENAME(Month, action_date)
			   ORDER BY Year, Month
			  </cfquery>

              <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
              <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

				  <cfset record = 1>

						var data = google.visualization.arrayToDataTable([
						  ['Month', 'Awards'],

						  <cfoutput query="sb_award_graph">
						    ['#monthname#',#round(amount)#],
						  </cfoutput>
						]);


			      var options = {
                    chartArea:{right: 20, left:70,top:20,width:'93%',height:'80%'},
                    series: {
					            0: { lineWidth: 3 },
                                1: { lineWidth: 3 },
                                },
                    pointSize: 10,
                    pointShape: 'circle',
                    hAxis: { format: 'currency', textStyle: {color: 'black', fontSize: 11}},
                    vAxis: { format: 'currency', textStyle: {color: 'black', fontSize: 11}},
                    legend: { position: 'bottom' }
				  };

                var chart = new google.visualization.LineChart(document.getElementById('line_chart_1'));
                chart.draw(data, options);
				}
			  </script>

			  <div id="line_chart_1" style="height: 250px;"></div>

              </td>

              <td></td>

              <td colspan=5 width=49%>

              <cfquery name="award_graph" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 			   select DATEPART(Year, action_date) Year, DATEPART(Month, action_date) Month, DATENAME(Month, action_date) Monthname, count(distinct(award_id_piid)) as total, sum(federal_action_obligation) as amount from award_data
			   where (contains((award_description),'#trim(insight.insight_keywords)#') )
			   and action_date between '#graph_end_date#' and '#graph_start_date#'
               and (type_of_set_aside_code = 'None' or type_of_set_aside_code is null or type_of_set_aside_code = '')
			   GROUP BY DATEPART(Year, action_date), DATEPART(Month, action_date), DATENAME(Month, action_date)
			   ORDER BY Year, Month
			  </cfquery>

              <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

				  <cfset record = 1>

						var data = google.visualization.arrayToDataTable([
						  ['Month', 'Awards'],

						  <cfoutput query="award_graph">
						    ['#monthname#',#round(amount)#],
						  </cfoutput>
						]);



			      var options = {
                    chartArea:{right: 20, left:70,top:20,width:'93%',height:'80%'},
                    series: {
					            0: { lineWidth: 3 },
                                1: { lineWidth: 3 },
                                },
                    pointSize: 10,
                    pointShape: 'circle',
                    hAxis: { format: 'currency', textStyle: {color: 'black', fontSize: 11}},
                    vAxis: { format: 'currency', textStyle: {color: 'black', fontSize: 11}},
                    legend: { position: 'bottom' }
				  };

                 var chart = new google.visualization.LineChart(document.getElementById('line_chart_2'));
                 chart.draw(data, options);
				}
			  </script>

			  <div id="line_chart_2" style="height: 250px;"></div>


              </td>

           </tr>

           <tr><td height=20 width=2%>&nbsp;</td></tr>

           <tr><td colspan=5 valign=top>

             <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr>
                  <td class="feed_sub_header">Month</td>
                  <td class="feed_sub_header" align=center># of Awards</td>
                  <td class="feed_sub_header" align=right width=200>Total Value</td>
               </tr>

              <cfset counter = 0>

              <cfoutput query="sb_award_graph">
               <cfif counter is 0>
                <tr bgcolor="ffffff">
               <cfelse>
                <tr bgcolor="e0e0e0">
               </cfif>

                  <td class="feed_sub_header" style="font-weight: normal;">#monthname# #year#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(total,'99,999')#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(amount,'$999,999,999')#</td>
               </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

              </cfoutput>

              </table>


              </td><td></td>
              <td colspan=5 valign=top>


             <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr>
                  <td class="feed_sub_header">Month</td>
                  <td class="feed_sub_header" align=center># of Awards</td>
                  <td class="feed_sub_header" align=right width=200>Total Value</td>
               </tr>

              <cfset counter = 0>

              <cfoutput query="award_graph">
               <cfif counter is 0>
                <tr bgcolor="ffffff">
               <cfelse>
                <tr bgcolor="e0e0e0">
               </cfif>

                  <td class="feed_sub_header" style="font-weight: normal;">#monthname# #year#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(total,'99,999')#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(amount,'$999,999,999')#</td>
               </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

              </cfoutput>

              </table>



              </td></tr>
           </table>



          </table>


          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>