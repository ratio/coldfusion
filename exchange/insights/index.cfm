<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="buying" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_type = 'Buying Pattern' and
        insight_hub_id = #session.hub# and
        insight_active = 1
  order by insight_order
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top width=100%>

		<!--- Start --->

	      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">Insights</td>
	              <td class="feed_sub_header" style="font-weight: normal;" align=right>
	              </td></tr>
	          <tr><td colspan=2><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif buying.recordcount is 0>

            <tr><td class="feed_sub_header" style="font-weight: normal;">No Buying Patterns have been created.</td></tr>

           <cfelse>

           <tr>
              <td class="feed_sub_header">Report Name</td>
              <td class="feed_sub_header">Description</td>
           </tr>

           <cfset counter = 0>

            <cfoutput query="buying">

            	<cfif counter is 0>
            	 <tr bgcolor="ffffff">
            	<cfelse>
            	 <tr bgcolor="e0e0e0">
            	</cfif>

            		<td class="feed_sub_header" width=400><a href="run_insight.cfm?i=#encrypt(insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';">#insight_name#</a></td>
            		<td class="feed_sub_header" style="font-weight: normal;">#insight_desc#</td>

            	</tr>

            	<cfif counter is 0>
            	 <cfset counter = 1>
            	<cfelse>
            	 <cfset counter = 0>
            	</cfif>

            </cfoutput>

           </cfif>

          </table>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>