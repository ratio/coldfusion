<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="insight" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 12>
</cfif>

<cfset monthly_end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset monthly_start_date = #dateformat(dateadd("d",-30,now()),'mm/dd/yyyy')#>

<cfset quarterly_end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset quarterly_start_date = #dateformat(dateadd("d",-90,now()),'mm/dd/yyyy')#>

<cfset halfyear_end_date = #dateformat(dateadd("d",-91,now()),'mm/dd/yyyy')#>
<cfset halfyear_start_date = #dateformat(dateadd("d",-180,now()),'mm/dd/yyyy')#>

<cfset perpage = 100>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics_code
 left join orgname on orgname_name = fbo_agency
 where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(insight.insight_keywords)#') )

 <cfif t is "sb">

  <cfif d is "monthly">
    and fbo_pub_date between '#monthly_start_date#' and '#monthly_end_date#'
  <cfelseif d is "quarterly">
    and fbo_pub_date between '#quarterly_start_date#' and '#quarterly_end_date#'
  <cfelse>
    and fbo_pub_date between '#halfyear_start_date#' and '#halfyear_end_date#'
  </cfif>

  and (fbo_setaside_original <> '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')

 <cfelse>

  <cfif d is "monthly">
    and fbo_pub_date between '#monthly_start_date#' and '#monthly_end_date#'
  <cfelseif d is "quarterly">
    and fbo_pub_date between '#quarterly_start_date#' and '#quarterly_end_date#'
  <cfelse>
    and fbo_pub_date between '#halfyear_start_date#' and '#halfyear_end_date#'
  </cfif>

  and (fbo_setaside_original is null or fbo_setaside_original = '')
  and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')

 </cfif>

 <cfif sv is 1>
  order by fbo_opp_name ASC
 <cfelseif sv is 10>
  order by fbo_opp_name DESC
 <cfelseif sv is 2>
  order by fbo_agency ASC
 <cfelseif sv is 20>
  order by fbo_agency DESC
 <cfelseif sv is 3>
  order by fbo_notice_type ASC
 <cfelseif sv is 30>
  order by fbo_notice_type DESC
 <cfelseif sv is 4>
  order by fbo_setaside_original ASC
 <cfelseif sv is 40>
  order by fbo_setaside_original DESC
 <cfelseif sv is 5>
  order by fbo_pub_date DESC
 <cfelseif sv is 50>
  order by fbo_pub_date ASC
 <cfelseif sv is 6>
  order by fbo_response_date_original DESC
 <cfelseif sv is 60>
  order by fbo_response_date_original ASC
 </cfif>


</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>

           <tr><td class="feed_header">

           <cfif t is "SB">
             New Contracts (Small Business) -
           <cfelse>
             New Contracts (Others) -
           </cfif>

			 <cfif d is "halfyear">
			  Last 6 Months
			 <cfelseif d is "quarterly">
			  Last 3 Months
			 <cfelseif d is "monthly">
			  Last 30 Days
			 </cfif>


           </td>
               <td class="feed_sub_header" align=right>

               <a href="new.cfm?i=#i#&d=#d#&t=#t#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" title="Export to Excel" hspace=10></a><a href="new.cfm?i=#i#&d=#d#&t=#t#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>

               &nbsp;|&nbsp;

               <a href="run_insight.cfm?i=#i#">Return</a>

               </td></tr>

           <tr><td colspan=2><hr></td></tr>

           <tr>
              <td class="feed_sub_header">#insight.insight_name# (#replace(insight.insight_keywords,'"','','all')#)</td>


              <td class="feed_sub_header" align=right>

					<cfif agencies.recordcount GT #perpage#>
						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#&i=#i#&d=#d#&t=#t#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#&i=#i#&d=#d#&t=#t#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

              </td>

           </tr>
           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" colspan=16>No opportunities were found.</td></tr>
          <cfelse>

          <tr>
              <td class="feed_sub_header">


              </td>
              <td class="feed_sub_header" align=right>


              </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

		     <tr>
                <td></td>

                <cfoutput>

			    <td class="feed_sub_header"><a href="new.cfm?i=#i#&d=#d#&t=#t#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Opportunity Name</b></a></td>
			    <td class="feed_sub_header"><a href="new.cfm?i=#i#&d=#d#&t=#t#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Organization</b></a></td>
			    <td class="feed_sub_header"><a href="new.cfm?i=#i#&d=#d#&t=#t#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Type</b></a></td>
			    <td class="feed_sub_header"><a href="new.cfm?i=#i#&d=#d#&t=#t#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Small Business</b></a></td>
			    <td class="feed_sub_header" align=right><a href="new.cfm?i=#i#&d=#d#&t=#t#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Date Posted</b></a></td>
			    <td class="feed_sub_header" align=right><a href="new.cfm?i=#i#&d=#d#&t=#t#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Date Due</b></a></td>

                </cfoutput>

               <td></td>
             </tr>

             <cfset counter = 0>

			<cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=40>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=40>
			 </cfif>

				 <td width=70 class="table_row" valign=middle>

				 <a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">
				<cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#agencies.orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>

				 </td>
				 <td class="feed_sub_header"><b><a href="/exchange/opps/opp_detail.cfm?fbo_id=#agencies.fbo_id#" target="_blank" rel="noopener" rel="noreferrer">#agencies.fbo_opp_name#</a></b><span class="link_small_gray"><br>#class_code_name#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;" width=300>#agencies.fbo_agency#<span class="link_small_gray"><br>#agencies.fbo_dept#</span></td>
				 <td class="feed_sub_header" style="font-weight: normal;">#agencies.fbo_notice_type#</td>
				 <td class="feed_sub_header" style="font-weight: normal;" width=125><cfif agencies.fbo_setaside_original is "">Not specified<cfelse>#agencies.fbo_setaside_original#</cfif></td>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>

                 <cfif agencies.fbo_pub_date_updated is not "">
                  #dateformat(agencies.fbo_pub_date_updated,'mmm dd, yyyy')#
                 <cfelse>
                  #dateformat(agencies.fbo_pub_date_original,'mmm dd, yyyy')#
                 </cfif>
				 </td>

				 <td class="feed_sub_header" style="font-weight: normal;" align=right width=125>


                 <cfif agencies.fbo_response_date_updated is not "">
                  #dateformat(agencies.fbo_response_date_updated,'mmm dd, yyyy')#
                 <cfelse>
                  <cfif #agencies.fbo_response_date_original# is "">Not Specified<cfelse>#dateformat(agencies.fbo_response_date_original,'mmm dd, yyyy')#</cfif>
                 </cfif>

                 </td>

                 <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=30 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.fbo_id#&t=contract','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>

		 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

              </cfoutput>


         </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>