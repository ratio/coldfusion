<cfinclude template="/exchange/security/check.cfm">

<cfquery name="insight" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_type = 'Buying Pattern' and
        insight_hub_id = #session.hub#
  order by insight_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Insights</td>
	       <td class="feed_sub_header" align=right></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <td class="feed_sub_header" style="color: green;">Insight has been successfully added.</td>
        <cfelseif u is 2>
         <td class="feed_sub_header" style="color: green;">Insight has been successfully updated.</td>
        <cfelseif u is 3>
         <td class="feed_sub_header" style="color: red;">Insight has been successfully deleted.</td>
        </cfif>
       </cfif>

       </td>
       <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="add.cfm">Add Insight</a></td></tr>

      </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #insight.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">No Insights exist.</td></tr>

        <cfelse>
        <tr>
				<td class="feed_sub_header">Order</b></td>
				<td class="feed_sub_header" width=300>Insight</b></td>
				<td class="feed_sub_header">Keywords</b></td>
				<td class="feed_sub_header" align=center>Active</b></td>
				<td class="feed_sub_header" align=center>Homepage</b></td>
				<td class="feed_sub_header" align=right>Image</b></td>
				<td></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="insight">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=middle width=75><a href="edit.cfm?i=#encrypt(insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#insight_order#</a></td>
              <td class="feed_sub_header" valign=middle><a href="edit.cfm?i=#encrypt(insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#insight_name#</a></td>

              <td class="feed_sub_header" style="font-weight: normal;" valign=middle>#replace(insight_keywords,'"',"","all")#</a></td>

              <td class="feed_sub_header" style="font-weight: normal;" valign=middle align=center>

               <cfif insight_active is 1>
                Yes
               <cfelse>
                No
               </cfif></td>

              <td class="feed_sub_header" style="font-weight: normal;" valign=middle align=center>

               <cfif insight_display is 1>
                Yes
               <cfelse>
                No
               </cfif></td>

              <td class="feed_sub_header" valign=middle align=right><a href="edit.cfm?i=#encrypt(insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

              <cfif insight_image is "">
               No Image
              <cfelse>
              <img src="#media_virtual#/#insight_image#" width=80>
              </cfif>

              </a>

              </td>

             <td align=right width=40><a href="/exchange/insights/run_insight.cfm?i=#encrypt(insight_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" target="_blank"><img src="/images/icon_play.png" width=28 alt="Run Insight" title="Run Insight"></a></td>


          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

