<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Add Insight</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_sub_header" width=150>Order</td>
		       <td><input class="input_text" style="width: 100px;" type="number" name="insight_order" step=.1 required></td></tr>

		   <tr><td class="feed_sub_header">Name</td>
		       <td><input class="input_text" type="text" name="insight_name" style="width: 700px;" maxlength="400" required></td></tr>

		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" type="text" name="insight_keywords" style="width: 900px;" required></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea name="insight_desc" class="input_textarea" style="width: 900px; height: 150px;"></textarea></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Comments</td>
		       <td><textarea name="insight_comments" class="input_textarea" style="width: 900px; height: 150px;"></textarea></td></tr>

           <tr><td class="feed_sub_header" valign=top>Image</td>
               <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="insight_image"></td></tr>

		   <tr><td class="feed_sub_header" width=150>Active</td>
		       <td><select name="insight_active" class="input_select">
		           <option value=0>No
		           <option value=1>Yes
		           </select></td></tr>

		   <tr><td class="feed_sub_header" width=150>Display on Homepage</td>
		       <td><select name="insight_display" class="input_select">
		           <option value=0>No
		           <option value=1>Yes
		           </select></td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10></td></tr>
		  </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

