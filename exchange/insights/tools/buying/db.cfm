<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(insight_keywords,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfif #button# is "Save">

	<cfif #insight_image# is not "">
		<cffile action = "upload"
		 fileField = "insight_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into insight (insight_type, insight_active, insight_display,insight_comments, insight_keywords, insight_desc, insight_created_by_usr_id, insight_name, insight_hub_id, insight_order, insight_image)
	 values ('Buying Pattern',#insight_active#,#insight_display#,'#insight_comments#','#search_string#','#insight_desc#',#session.usr_id#,'#insight_name#',#session.hub#,#insight_order#,<cfif #insight_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select insight_image from insight
		  where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		        insight_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.insight_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.insight_image#">
		</cfif>

	</cfif>

	<cfif insight_image is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select insight_image from insight
		  where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		        insight_hub_id = #session.hub#
		</cfquery>

		<cfif #getfile.insight_image# is not "">
         <cfif fileexists("#media_path#\#getfile.insight_image#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.insight_image#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "insight_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update insight
	 set insight_name = '#insight_name#',
	     insight_active = #insight_active#,
	     insight_display = #insight_display#,
	     insight_comments = '#insight_comments#',
	     insight_keywords = '#search_string#',
	     insight_desc = '#insight_desc#',

		  <cfif #insight_image# is not "">
		   insight_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   insight_image = null,
		  </cfif>

	     insight_order = #insight_order#

	 where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       insight_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select insight_image from insight
		  where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
		        insight_hub_id = #session.hub#
		</cfquery>

		<cfif remove.insight_image is not "">
         <cfif fileexists("#media_path#\#remove.insight_image#")>
			 <cffile action = "delete" file = "#media_path#\#remove.insight_image#">
         </cfif>
		</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete insight
	 where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       insight_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>

