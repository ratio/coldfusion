<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from insight
 where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Edit Insight</td>
		       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>

		   <tr><td class="feed_sub_header" width=150>Order</td>
		       <td><input class="input_text" style="width: 100px;" value=#edit.insight_order# type="number" name="insight_order" step=.1 required></td></tr>

		   <tr><td class="feed_sub_header">Name</td>
		       <td><input class="input_text" type="text" name="insight_name" value="#edit.insight_name#" style="width: 700px;" maxlength="400" required></td></tr>

		   <tr><td class="feed_sub_header">Keywords</td>
		       <td><input class="input_text" type="text" name="insight_keywords" style="width: 900px;" value="#replace(edit.insight_keywords,'"',"","all")#" required></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Description</td>
		       <td><textarea name="insight_desc" class="input_textarea" style="width: 900px; height: 150px;">#edit.insight_desc#</textarea></td></tr>

		   <tr><td class="feed_sub_header" valign=top>Comments</td>
		       <td><textarea name="insight_comments" class="input_textarea" style="width: 900px; height: 150px;">#edit.insight_comments#</textarea></td></tr>


			<tr><td class="feed_sub_header" valign=top>Image</td>
				<td class="feed_sub_header" style="font-weight: normal;">

				<cfif #edit.insight_image# is "">
				  <input type="file" name="insight_image">
				<cfelse>
				  <img src="#media_virtual#/#edit.insight_image#" width=100><br><br>
				  <input type="file" name="insight_image"><br><br>
				  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
				 </cfif>

				 </td></tr>

		   <tr><td class="feed_sub_header" width=150>Active</td>
		       <td><select name="insight_active" class="input_select">
		           <option value=0>No
		           <option value=1 <cfif #edit.insight_active# is 1>selected</cfif>>Yes
		           </select></td></tr>

		   <tr><td class="feed_sub_header" width=150>Display on Homepage</td>
		       <td><select name="insight_display" class="input_select">
		           <option value=0>No
		           <option value=1 <cfif #edit.insight_display# is 1>selected</cfif>>Yes
		           </select></td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

		   <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Record?\r\nAre you sure you want to delete this record?');"></td></tr>

           <input type="hidden" name="i" value=#i#>

           </cfoutput>

		  </table>

      </td></tr>
     </table>

  </div>

 </form>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

