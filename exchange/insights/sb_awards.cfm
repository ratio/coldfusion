<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="insight" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from insight
  where insight_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 12>
</cfif>

<cfset quarterly_end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset quarterly_start_date = #dateformat(dateadd("d",-90,now()),'mm/dd/yyyy')#>

<cfset halfyear_end_date = #dateformat(dateadd("d",-91,now()),'mm/dd/yyyy')#>
<cfset halfyear_start_date = #dateformat(dateadd("d",-180,now()),'mm/dd/yyyy')#>

<cfset year_end_date = #dateformat(dateadd("d",-181,now()),'mm/dd/yyyy')#>
<cfset year_start_date = #dateformat(dateadd("d",-365,now()),'mm/dd/yyyy')#>

<cfset perpage = 100>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select
 action_date, award_id_piid, recipient_name, recipient_duns, award_description, period_of_performance_current_end_date as max_date, period_of_performance_current_end_date as date, awarding_agency_name, awarding_sub_agency_name, potential_total_value_of_award as potential_value, base_and_all_options_value as all_options, current_total_value_of_award as current_value, federal_action_obligation as obligated, base_and_exercised_options_value as options, orgname_logo from award_data
 left join orgname on orgname_name = awarding_sub_agency_name

 where contains((award_description),'#trim(insight.insight_keywords)#') and
 (type_of_set_aside_code <> 'None' or type_of_set_aside_code is not null or type_of_set_aside_code <> '')

 <cfif d is "quarterly">
  and action_date between '#quarterly_start_date#' and '#quarterly_end_date#'
 <cfelseif d is "halfyear">
  and action_date between '#halfyear_start_date#' and '#halfyear_end_date#'
 <cfelseif d is "year">
  and action_date between '#year_start_date#' and '#year_end_date#'
 </cfif>

 <cfif sv is 12>
  order by action_date DESC
 <cfelseif sv is 120>
  order by action_date ASC

 <cfelseif sv is 2>
  order by award_id_piid ASC
 <cfelseif sv is 20>
  order by award_id_piid DESC

 <cfelseif sv is 3>
  order by award_id_piid ASC
 <cfelseif sv is 30>
  order by award_id_piid DESC

 <cfelseif sv is 4>
  order by awarding_sub_agency_name ASC
 <cfelseif sv is 40>
  order by awarding_sub_agency_name DESC

 <cfelseif sv is 5>
  order by obligated DESC
 <cfelseif sv is 50>
  order by obligated ASC

 <cfelseif sv is 6>
  order by options DESC
 <cfelseif sv is 60>
  order by options ASC

 <cfelseif sv is 7>
  order by all_options DESC
 <cfelseif sv is 70>
  order by all_options ASC

 <cfelseif sv is 8>
  order by current_value DESC
 <cfelseif sv is 80>
  order by current_value ASC

 <cfelseif sv is 9>
  order by potential_value DESC
 <cfelseif sv is 90>
  order by potential_value ASC

 <cfelseif sv is 11>
  order by recipient_name ASC
 <cfelseif sv is 110>
  order by recipient_name DESC

</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt agencies.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(agencies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>


  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>

           <tr><td class="feed_header">Small Business Awards -

			 <cfif d is "halfyear">
			  Last 3-6 Months
			 <cfelseif d is "quarterly">
			  Last 3 Months
			 <cfelseif d is "year">
			  Last 6-12 Months
			 </cfif>


           </td>
               <td class="feed_sub_header" align=right>

               <a href="sb_awards.cfm?i=#i#&d=#d#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" title="Export to Excel" hspace=10></a><a href="sb_awards.cfm?i=#i#&d=#d#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>

               &nbsp;|&nbsp;

               <a href="run_insight.cfm?i=#i#">Return</a>

               </td></tr>

           <tr><td colspan=2><hr></td></tr>

           <tr>
              <td class="feed_sub_header">#insight.insight_name# (#replace(insight.insight_keywords,'"','','all')#)</td>


              <td class="feed_sub_header" align=right>

					<cfif agencies.recordcount GT #perpage#>
						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#&i=#i#&d=#d#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt agencies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#&i=#i#&d=#d#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

              </td>

           </tr>
           </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" colspan=16>No opportunities were found.</td></tr>
          <cfelse>

          <tr>
              <td class="feed_sub_header">


              </td>
              <td class="feed_sub_header" align=right>


              </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
			     <td></td>
				 <td class="feed_option"><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Date</b></a></td>
				 <td class="feed_option"><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Department / Agency</b></a></td>
				 <td class="feed_option"><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Award ##</b></a></td>
				 <td class="feed_option"><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Incumbent</b></a></td>
				 <td class="feed_option"><b>Award Description</b></td>
				 <td class="feed_option" align=right><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Obligated</b></a></td>
				 <td class="feed_option" align=right><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>With Options</b></a></td>
				 <td class="feed_option" align=right><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>All Options</b></a></td>
				 <td class="feed_option" align=right><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Current Value</b></a></td>
				 <td class="feed_option" align=right><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Potential Value</b></a></td>
				 <td class="feed_option" align=right><a href="sb_awards.cfm?i=#i#<cfif isdefined("cus")>&cus=1</cfif>&d=#d#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>End Date</b></a></td>

			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

		<cfoutput query="agencies" startrow="#url.start#" maxrows="#perpage#">

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=50>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=50>
			   </cfif>

				<td width=70 class="table_row" valign=top>

                <a href="/exchange/include/find_award.cfm?award_id=#agencies.award_id_piid#" target="_blank" rel="noopener" rel="noreferrer">
                <cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>

				 </td>

				   <td class="feed_option" style="font-weight: normal;" width=100 valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
				   <td class="feed_option" style="font-weight: normal;" width=350 valign=top>#agencies.awarding_agency_name#<br>#agencies.awarding_sub_agency_name#</td>
				   <td class="feed_option" valign=top style="padding-right: 10px;"><a href="/exchange/include/find_award.cfm?award_id=#agencies.award_id_piid#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.award_id_piid#</b></a></td>
				   <td class="feed_option" style="font-weight: normal;" valign=top width=200><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#recipient_name#</b></a></td>
				   <td class="feed_option" valign=top style="font-weight: normal;" width=500 style="padding-right: 10px;">#award_description#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(options,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(all_options,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=100>#numberformat(current_value,'$999,999,999')#</td>
				   <td class="feed_option" valign=top style="font-weight: normal;" align=right width=150 style="padding-right: 10px;">#numberformat(potential_value,'$999,999,999')#</td>
				   <td class="feed_option" valign=top width=75 align=right>#dateformat(date,'mm/dd/yyyy')#</td>

                 <td align=right width=40 valign=top>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.award_id_piid#&t=award','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=625'); return false;">
			     </td>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

          </cfoutput>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>