<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="usr_vehicles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_vehicle
 where usr_vehicle_usr_id = #session.usr_id# or usr_vehicle_company_id = #session.company_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><img src="/images/icon_vehicle2.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;Contracts</td>
               <td align=right></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>


           <tr><td class="feed_header">My Vehicles</td>
           <td class="feed_sub_header" align=right colspan=3><a href="vlookup.cfm"><img src="/images/plus3.png" border=0 width=15 hspace=10 alt="Add Vehicle" title="Add Vehicle"></a><a href="vlookup.cfm">Add Vehicle</a></td></tr>

           <cfif #usr_vehicles.recordcount# is 0>
             <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any vehicles.</td></tr>
           <cfelse>

             <tr height=40>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Vehicle Name</b></td>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Description</b></td>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Pattern</b></td>
             </tr>

             <cfset counter = 0>

             <cfloop query="usr_vehicles">

             <cfif counter is 0>
              <tr bgcolor="ffffff" height=30>
             <cfelse>
              <tr bgcolor="e0e0e0" height=30>
             </cfif>

             <cfoutput>
              <td class="feed_sub_header" style="font-weight: normal;" width=500><a href="vresults.cfm?usr_vehicle_id=#usr_vehicles.usr_vehicle_id#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><b>#ucase(usr_vehicles.usr_vehicle_name)#</b></a></td>
              <td class="feed_sub_header" style="font-weight: normal;">#usr_vehicles.usr_vehicle_desc#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#usr_vehicles.usr_vehicle_pattern#</td>
             </cfoutput>

             </tr>

             <cfif counter is 0>
              <cfset counter = 1>
             <cfelse>
              <cfset counter = 0>
             </cfif>

             </cfloop>

           </cfif>

           <tr><td height=20></td></tr>
           <tr><td colspan=5><hr></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	      <cfquery name="evehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	       select * from cv
	       order by cv_name
	      </cfquery>

		  <cfset counter = 0>

          <tr><td colspan=3 class="feed_header">Exchange Government Contract Vehicles</td></tr>
          <tr><td class="feed_sub_header" colspan=5 style="font-weight: normal;">Contract Vehicles are qualified lists of vendors / contractors that have been awarded contracts to bid on opportunities.  This vehicle list is managed and maintained by the Exchange.</td></tr>
          <tr><td height=10></td></tr>

		   <tr>
			    <td class="feed_sub_header"><b>Org</b></td>
			    <td class="feed_sub_header"><b>Vehicle Name</b></td>
			</tr>

        <cfset counter = 0>

         <cfloop query="evehicles">

         <cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top style="font-weight: normal;">#evehicles.cv_org_acronym#</td>
              <td class="feed_sub_header" valign=top><a href="contracts.cfm?vehicle_id=#evehicles.cv_id#">#evehicles.cv_name#</a></td>
              <td class="feed_sub_header" valign=top align=right><a href="contracts.cfm?vehicle_id=#evehicles.cv_id#">Contracts</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

         </cfloop>

	    </table>


		  </td></tr>

	    </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>