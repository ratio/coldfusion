<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><img src="/images/icon_vehicle2.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;Contract Vehicles</td>
               <td align=right></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

 		  <cfquery name="usr_vehicles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 		   select * from usr_vehicle
 		   join usr on usr_id = usr_vehicle_usr_id
 		   where usr_vehicle_usr_id = #session.usr_id# or usr_vehicle_company_id = #session.company_id# and
 		   usr_vehicle_hub_id = #session.hub#
 		  </cfquery>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("u")>
           <cfif u is 3>
            <tr><td class="feed_sub_header" colspan=5><font color=green>Contract vehicle was successfully deleted.</td></tr>
           <cfelseif u is 1>
            <tr><td class="feed_sub_header" colspan=5><font color=green>Contract vehicle was successfully added.</td></tr>
           </cfif>
          </cfif>

           <tr><td class="feed_header">My Vehicles</td>
           <td class="feed_sub_header" align=right colspan=4><a href="vlookup.cfm"><img src="/images/plus3.png" border=0 width=15 hspace=10 alt="Add Vehicle" title="Add Vehicle"></a><a href="vlookup.cfm">Add Vehicle</a></td></tr>

           <tr><td colspan=4 class="feed_sub_header" style="font-weight: normal;">This section lists contract vehicles that either you, or members of your Company have created.</td></tr>


           <cfif #usr_vehicles.recordcount# is 0>
             <tr><td class="feed_sub_header" style="font-weight: normal;">You or members of your Company have not created any vehicles.</td></tr>
           <cfelse>

             <tr height=40>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Vehicle Name</b></td>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Description</b></td>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Created By</b></td>
                <td class="feed_sub_header" style="font-weight: normal;"><b>Pattern</b></td>
                <td>&nbsp;</td>
             </tr>

             <cfset counter = 0>

             <cfloop query="usr_vehicles">

             <cfif counter is 0>
              <tr bgcolor="ffffff" height=30>
             <cfelse>
              <tr bgcolor="e0e0e0" height=30>
             </cfif>

             <cfoutput>
              <td class="feed_sub_header" style="font-weight: normal;" width=500><a href="vresults.cfm?i=#encrypt(usr_vehicles.usr_vehicle_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><b>#usr_vehicles.usr_vehicle_name#</b></a></td>
              <td class="feed_sub_header" style="font-weight: normal;"><cfif #usr_vehicles.usr_vehicle_desc# is "">Not Provided<cfelse>#usr_vehicles.usr_vehicle_desc#</cfif></td>
              <td class="feed_sub_header" style="font-weight: normal;" width=225>#usr_vehicles.usr_first_name# #usr_vehicles.usr_last_name#</td>
              <td class="feed_sub_header" style="font-weight: normal;" width=150>#usr_vehicles.usr_vehicle_pattern#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right width=125><a href="vresults.cfm?i=#encrypt(usr_vehicles.usr_vehicle_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><b>Contracts</b></a></td>
             </cfoutput>

             </tr>

             <cfif counter is 0>
              <cfset counter = 1>
             <cfelse>
              <cfset counter = 0>
             </cfif>

             </cfloop>

           </cfif>

           <tr><td height=20></td></tr>
           <tr><td colspan=5><hr></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif not isdefined("sv")>
           <cfset sv = 2>
          </cfif>

	      <cfquery name="evehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	       select * from cv

	       <cfif sv is 1>
	        order by cv_org_acronym ASC
	       <cfelseif sv is 10>
	        Order by cv_org_acronym DESC
	       <cfelseif sv is 2>
	        order by cv_name ASC
	       <cfelseif sv is 20>
	        order by cv_name DESC
	       </cfif>


	      </cfquery>

		  <cfset counter = 0>

		  <tr><td height=10></td></tr>
          <tr><td colspan=3 class="feed_header">Exchange Government Contract Vehicles</td></tr>
          <tr><td class="feed_sub_header" colspan=5 style="font-weight: normal;">Contract Vehicles are qualified lists of vendors / contractors that have been awarded contracts to bid on opportunities.  This vehicle list is managed and maintained by the Exchange.</td></tr>
          <tr><td height=10></td></tr>

		   <tr>
			    <td class="feed_sub_header"><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Org</b></a></td>
			    <td class="feed_sub_header"><a href="index.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Vehicle Name</b></a></td>
			    <td class="feed_sub_header"><b>Description</b></td>
			</tr>

        <cfset counter = 0>

         <cfloop query="evehicles">

         <cfoutput>

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

              <td class="feed_sub_header" valign=top style="font-weight: normal;" width=75>#evehicles.cv_org_acronym#</td>
              <td class="feed_sub_header" valign=top width=300 style="padding-right: 10px;"><a href="evresults.cfm?i=#encrypt(evehicles.cv_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';">#evehicles.cv_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;" style="padding-right: 10px;"><cfif #evehicles.cv_desc# is "">Not Provided<cfelse>#evehicles.cv_desc#</cfif></td>
              <td class="feed_sub_header" valign=top align=right><a href="evresults.cfm?i=#encrypt(evehicles.cv_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';">Contracts</a></td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

         </cfloop>

	    </table>


		  </td></tr>

	    </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>