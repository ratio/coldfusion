<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfquery name="usr_vehicle" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr_vehicle
	where usr_vehicle_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">


  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      <div class="main_box">

      <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <tr><td class="feed_header" valign=top>Edit Contract Vehicle</td>
	  	  <td align=right class="feed_sub_header" valign=top><a href="vresults.cfm?i=#i#">Return</a></td></tr>
	     <tr><td colspan=2><hr></td></tr>
	    </table>

	  </cfoutput>


          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=10></td></tr>

          <form action="save.cfm" method="post">

          <cfoutput>

          <tr>
              <td class="feed_sub_header" width=100><b>Name</b></td>
              <td><input type="text" class="input_text" name="usr_vehicle_name" style="width: 400px;" value="#usr_vehicle.usr_vehicle_name#"></td>
          </tr>

          <tr><td height=10></td></tr>

          <tr>
              <td class="feed_sub_header" valign=top><b>Description</b></td>
              <td valign=top><textarea class="input_textarea" name="usr_vehicle_desc" style="width: 700px;" rows=3>#usr_vehicle.usr_vehicle_desc#</textarea></td>
          </tr>

          <tr><td height=10></td></tr>

          <tr>
              <td class="feed_sub_header"><b>Pattern</b></td>
              <td><input type=="text" class="input_text" name="usr_vehicle_pattern" style="width: 400px;" value="#usr_vehicle.usr_vehicle_pattern#"></td>
          </tr>

          <tr><td height=10></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>

          <tr>
             <td></td>
             <td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
                 <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Custom Vehicle?\r\nAre you sure you want to delete this record?');"></td></tr>

          <input type="hidden" name="i" value=#i#>

          </cfoutput>

          </form>

		  </table>

		  </td></tr>

		</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>