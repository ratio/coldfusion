<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <tr><td class="feed_header">Add Vehicle</td>
	  	  <td align=right class="feed_sub_header"><a href="/exchange/vehicles/">Return</a></td></tr>
	     <tr><td colspan=2><hr></td></tr>
	    </table>

          <form action="vlookup_results.cfm" method="post">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">This screen allows you to lookup contract vehicle numbers to see what companies are winning contracts under those vehicles.</td></tr>
              <tr><td height=10></td></tr>

			  <tr><td class="feed_sub_header"><b>Enter Contract Vehicle Number Pattern</b></td></tr>
			  <tr><td class="feed_option"><input type="text" class="input_text" name="vehicle_no" size=40></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td><input type="submit" class="button_blue_large" name="button" value="Lookup"></td></tr>

			  <tr><td height=10></td></tr>
			  <tr><td><hr></td></tr>
			  <tr><td class="feed_sub_header"><b>Instructions</b></td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">Entering a full contract vehicle number will bring back information for that contract number only.</td></tr>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">To analyze multiple contract vehicle holders, enter the contract number "pattern" that's consistent across contract awards.  For instance, entering "FA807516" will include FA807516-0001, 0002, 0003, etc.</td></tr>

			  <tr><td>&nbsp;</td></tr>

			  </table>

		  </form>

		  </td>

		  </tr>

	    </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>