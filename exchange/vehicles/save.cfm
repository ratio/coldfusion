<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Save">

	<cfquery name="save" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into usr_vehicle
	 (usr_vehicle_hub_id, usr_vehicle_pattern, usr_vehicle_name, usr_vehicle_usr_id, usr_vehicle_company_id, usr_vehicle_updated)
	 values
	 (#session.hub#,'#usr_vehicle_pattern#','#usr_vehicle_name#',#session.usr_id#,#session.company_id#,#now()#)
	</cfquery>

	<cflocation URL="/exchange/vehicles/index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete usr_vehicle
	 where usr_vehicle_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

	<cflocation URL="/exchange/vehicles/index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Update">

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update usr_vehicle
      set usr_vehicle_name = '#usr_vehicle_name#',
          usr_vehicle_hub_id = #session.hub#,
          usr_vehicle_pattern = '#usr_vehicle_pattern#',
          usr_vehicle_desc = '#usr_vehicle_desc#',
          usr_vehicle_updated = #now()#
      where usr_vehicle_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

	<cflocation URL="/exchange/vehicles/vresults.cfm?i=#i#&u=2" addtoken="no">

</cfif>




