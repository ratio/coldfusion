<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfquery name="vehicle" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select * from vlookup
    where vlookup_vehicle_abbr = '#session.vehicle_source#'
  </cfquery>

  <cfif not isdefined("sv")>
   <cfset sv = 1>
  </cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

			<cfquery name="name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select top(1) * from award_data
			 where recipient_duns = '#duns#'
			</cfquery>

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header"><cfoutput>#ucase(vehicle.vlookup_vehicle_name)#</cfoutput> - AWARDS</td>

             <td class="feed_option" align=right><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" border=0 width=20 alt="Export to Excel" title="Export to Excel"></a>&nbsp;&nbsp;&nbsp;

             <a href="/exchange/vehicles/vendors.cfm?source=#session.vehicle_source#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a>

             </td></tr>

         <tr><td colspan=2><hr></td></tr>

         <form action="/exchange/vehicles/set2.cfm" method="post">
         <tr><td class="feed_sub_header"><a href="/exchange/include/federal_profile.cfm?duns=#name.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><u>#name.recipient_name#</u></a><br><b>Contract Number - #contract_number#</b></td>
             <td class="feed_sub_header" align=right>
					<cfoutput>
                       Filter by Keyword:&nbsp;&nbsp;
                       <input type="text" class="input_text" name="filter" size=20 <cfif isdefined("session.filter_keyword")> value="#session.filter_keyword#"</cfif>>&nbsp;

		               <input class="button_blue" required type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="vehicle_1">
                       <input type="hidden" name="contract_number" value="#contract_number#">
                       <input type="hidden" name="duns" value="#duns#">
                       <cfif isdefined("sv")>
                       <input type="hidden" name="sv" value="#sv#">
                       </cfif>

					</cfoutput>
             </td></tr>
             </form>
         <tr><td colspan=2><hr></td></tr>

        </table>
        </cfoutput>


			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select sum(federal_action_obligation) as total from award_data
				 where (recipient_duns = '#duns#' and
				        parent_award_id = '#contract_number#' and
				       federal_action_obligation > 0)
				 <cfif isdefined("session.filter_keyword")>
				  and contains((award_description, product_or_service_code_description),'"#session.filter_keyword#"')
				 </cfif>

				</cfquery>

				<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select action_date, id, award_id_piid, modification_number, awarding_agency_name, awarding_sub_agency_name,
				 awarding_office_name, award_description, product_or_service_code_description, naics_code,
                 type_of_contract_pricing, period_of_performance_start_date,period_of_performance_potential_end_date,federal_action_obligation
				 from award_data
				 where (recipient_duns = '#duns#' and
				       federal_action_obligation > 0 and
				       parent_award_id = '#contract_number#'
				       )

				 <cfif isdefined("session.filter_keyword")>
				  and contains((award_description, product_or_service_code_description),'"#session.filter_keyword#"')
				 </cfif>

				 <cfif isdefined("sv")>

					<cfif #sv# is 1>
					 order by action_date DESC
					<cfelseif #sv# is 10>
					 order by action_date ASC
					<cfelseif #sv# is 2>
					 order by award_id_piid ASC
					<cfelseif #sv# is 20>
					 order by award_id_piid DESC
					<cfelseif #sv# is 3>
					 order by product_or_service_code ASC
					<cfelseif #sv# is 30>
					 order by product_or_service_code DESC
					<cfelseif #sv# is 4>
					 order by awarding_sub_agency_name ASC
					<cfelseif #sv# is 40>
					 order by awarding_sub_agency_name DESC
					<cfelseif #sv# is 5>
					 order by naics_code ASC
					<cfelseif #sv# is 50>
					 order by naics_code DESC
					<cfelseif #sv# is 6>
					 order by type_of_contract_pricing ASC
					<cfelseif #sv# is 60>
					 order by type_of_contract_pricing DESC
					<cfelseif #sv# is 7>
					 order by type_of_set_aside ASC
					<cfelseif #sv# is 70>
					 order by type_of_set_aside DESC
					<cfelseif #sv# is 8>
					 order by period_of_performance_start_date ASC
					<cfelseif #sv# is 80>
					 order by period_of_performance_start_date DESC
					<cfelseif #sv# is 9>
					 order by period_of_performance_potential_end_date ASC
					<cfelseif #sv# is 90>
					 order by period_of_performance_potential_end_date DESC
					<cfelseif #sv# is 11>
					 order by federal_action_obligation ASC
					<cfelseif #sv# is 110>
					 order by federal_action_obligation DESC
					</cfif>

				   <cfelse>
					 order by action_date DESC
				   </cfif>

				</cfquery>

           <cfif isdefined("export")>

             <cfinclude template="/exchange/include/export_to_excel.cfm">

            </cfif>

			    <cfparam name="URL.PageId" default="0">
			    <cfset RecordsPerPage = 250>
			    <cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
			    <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
			    <cfset EndRow = StartRow+RecordsPerPage-1>

			    </table>

			    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <cfoutput>
                <tr><td class="feed_option" colspan=5><b>Total Awards - #numberformat(agencies.recordcount,'999,999')#<br>Total Award Value: #numberformat(total.total,'$999,999,999,999')#</b></td>
                    <td class="feed_option" align=right colspan=9>

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&id=0&usr_vehicle_id=#usr_vehicle.usr_vehicle_id#&contract_no=#contract_no#&duns=#duns#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>

                    </td>

                </tr>
                </cfoutput>

				<tr><td>

                                <cfoutput>
									<tr height=50>
									   <td class="text_xsmall" width=75><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
									   <td class="text_xsmall"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract Number</b></a></td>
									   <td class="text_xsmall"><b>Mod</b></td>
									   <td class="text_xsmall"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
									   <td class="text_xsmall"><b>Award Description</b></td>
									   <td class="text_xsmall"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Product or Service</b></a></td>
									   <td class="text_xsmall"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a></td>
									   <td class="text_xsmall" align=center><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Pricing</b></a></td>
									   <td class="text_xsmall"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>POP Start</b></a></td>
									   <td class="text_xsmall"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>POP End</b></a></td>
									   <td class="text_xsmall" align=right><a href="/exchange/vehicles/vendor_awards.cfm?duns=#duns#&contract_number=#contract_number#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Obligation</b></a></td>
									</tr>
								</cfoutput>

								<cfset #total_value# = 0>

								<cfset #counter# = 0>

								<cfloop query="agencies">

						        <cfif CurrentRow gte StartRow >

								  <tr

								  <cfif #counter# is 0>
								   bgcolor="ffffff"
								  <cfelse>
								   bgcolor="e0e0e0"
								  </cfif>


								  height=30>

								  <cfoutput>

									 <td class="text_xsmall" valign=middle>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=middle><a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.award_id_piid#</b></a></td>
									 <td class="text_xsmall" valign=middle>#agencies.modification_number#</td>
									 <td class="text_xsmall" valign=middle>#agencies.awarding_sub_agency_name#</td>
									 <td class="text_xsmall" valign=middle width=350>

								   <cfif isdefined("session.filter_keyword") and #session.filter_keyword# is not "">
									#replaceNoCase(agencies.award_description,session.filter_keyword,"<span style='background:yellow'>#ucase(session.filter_keyword)#</span>","all")#
								   <cfelse>
								   #agencies.award_description#
								   </cfif>


									 </td>
									 <td class="text_xsmall" valign=middle>#agencies.product_or_service_code_description#</td>
									 <td class="text_xsmall" valign=middle>#agencies.naics_code#</td>
									 <td class="text_xsmall" valign=middle align=center>
									 <cfif agencies.type_of_contract_pricing is "Firm Fixed Price">
									  FFP
									 <cfelseif agencies.type_of_contract_pricing is "Cost Plus Fixed Fee">
									  CPFF
									 <cfelseif agencies.type_of_contract_pricing is "Time and Materials">
									  T&M
									 <cfelseif agencies.type_of_contract_pricing is "Cost Plus Award Fee">
									  CPAF
									 <cfelse>
									  #left(agencies.type_of_contract_pricing,21)#
									 </cfif>
									 </td>
									 <td class="text_xsmall" valign=middle width=75>#dateformat(agencies.period_of_performance_start_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=middle width=75>#dateformat(agencies.period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=middle width=50 align=right>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>

								  </cfoutput>

								  </tr>

								  <cfset #total_value# = #total_value# + #agencies.federal_action_obligation#>

								  <cfif #counter# is 0>
								   <cfset #counter# = 1>
								  <cfelse>
								   <cfset #counter# = 0>
								  </cfif>

								  </cfif>

								  <cfif CurrentRow eq EndRow>
								   <cfbreak>
								  </cfif>

								</cfloop>

								<tr><td class="feed_option" colspan=10><b>Total:</b></td>

								<cfoutput>
									<td class="feed_option" align=right><b>#numberformat(total_value,'$999,999,999')#</b></td>
								</cfoutput>

                 </tr>
            </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>