<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfset StructDelete(Session,"filter_keyword")>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">


      </td><td valign=top>

      <cfset session.vehicle_source = #source#>

      <div class="main_box">

	    <cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from vlookup
		 where vlookup_vehicle_abbr = '#source#'
	    </cfquery>

  		<cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select distinct(duns) as duns, state, phone, email, vendor, contract_end_date, contractno from vehicles
		  where source = '#source#'
		  order by vendor
        </cfquery>

		<cfparam name="URL.PageId" default="0">
		<cfset RecordsPerPage = 250>
		<cfset TotalPages = (vendors.Recordcount/RecordsPerPage)>
		<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		<cfset EndRow = StartRow+RecordsPerPage-1>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">CONTRACT VEHICLE</td>
             <td class="feed_option" align=right><a href="/exchange/vehicles/"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <cfoutput>
			 <tr><td class="feed_sub_header">#ucase(info.vlookup_agency)# - #ucase(info.vlookup_vehicle_name)# (#ucase(source)#)</td></tr>
			 <tr><td class="feed_option"><a href="#info.vlookup_vehicle_url#" target="_blank" rel="noopener" rel="noreferrer"><u>#info.vlookup_vehicle_url#</u></a></td>
			     <td class="feed_option" align=right>

              <cfif vendors.recordcount GT #RecordsPerPage#>
				  <b>Page: </b>&nbsp;|
				  <cfloop index="Pages" from="0" to="#TotalPages#">
				   <cfoutput>
				   <cfset DisplayPgNo = Pages+1>
					  <cfif URL.PageId eq pages>
						 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
					  <cfelse>
						 <a href="?pageid=#pages#&source=#source#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
					  </cfif>
				   </cfoutput>
				  </cfloop>
			   </cfif>

			     </td></tr>

         </cfoutput>
        </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfset counter = 0>

          <tr height=40>
          <td class="feed_option"><b>Vendor</b></td>
          <td class="feed_option" width=75><b>DUNS</b></td>
          <td class="feed_option" width=125><b>Contract Number</b></td>
          <td class="feed_option"><b>POC Phone</b></td>
          <td class="feed_option"><b>POC Email</b></td>
          <td class="feed_option" align=right width=125><b>Contract End Date</b></td>
          <td class="feed_option" align=center width=75><b>Awards</b></td>
          <td class="feed_option" align=right width=100><b>Award Value</b></td>
          </tr>

          <cfloop query="vendors">

            <cfif counter is 0>
              <tr bgcolor="ffffff" height=30>
            <cfelse>
              <tr bgcolor="e0e0e0" height=30>
            </cfif>

          <cfif CurrentRow gte StartRow >

          <cfoutput>

			   <cfset #contract_number# = #replace(vendors.contractno,"-","","all")#>

               <cfquery name="vehicle_awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select count(id) as awards, sum(federal_action_obligation) as total from award_data
				 where recipient_duns = '#duns#' and
					   parent_award_id = '#contract_number#' and
					   federal_action_obligation > 0
			   </cfquery>

               <td class="feed_option"><a href="/exchange/vehicles/vendor_awards.cfm?duns=#vendors.duns#&contract_number=#contract_number#"><b>#ucase(vendors.vendor)#</b></a></td>
               <td class="feed_option">#vendors.duns#</td>
               <td class="feed_option">#vendors.contractno#</td>
               <td class="feed_option">#vendors.phone#</td>
               <td class="feed_option">#vendors.email#</td>
               <td class="feed_option" align=right>#dateformat(vendors.contract_end_date,'mm/dd/yyyy')#</td>
               <td class="feed_option" align=center>#vehicle_awards.awards#</td>
               <td class="feed_option" align=right>#numberformat(vehicle_awards.total,'$999,999,999')#</td>
             </tr>

           </cfoutput>

           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfif>

		  <cfif CurrentRow eq EndRow>
		   <cfbreak>
		  </cfif>

          </cfloop>

          <tr><td>&nbsp;</td></tr>

          <tr><td colspan=7 class="link_small_gray"><b>Note - </b> some vehicle sources do not include information such as State, POC Phone, or Contract End Dates.  Please refer to the vehicle source for additional information.</td></tr>

          <tr><td>&nbsp;</td></tr>

		  </table>

		</td></tr>

	  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>