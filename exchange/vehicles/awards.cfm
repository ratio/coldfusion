<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

				<cfquery name="name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select top(1) * from award_data
				 where recipient_duns = '#duns#' and
				       parent_award_id = '#contract_no#'
				</cfquery>

        <cfoutput>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Contract Vehicle Lookup - Search Results</td>

             <td class="feed_sub_header" align=right><a href="vlookup_results.cfm?vehicle_no=#vehicle_no#">Return</a></td></tr>
         <tr><td colspan=2><hr></td></tr>

         <tr><td class="feed_sub_header"><b>Company - <a href="/exchange/include/company_profile.cfm?duns=#name.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><u>#name.recipient_name#</u></a></b></td></tr>
         <tr><td class="feed_sub_header"><b>Contract Number - #name.parent_award_id#</b></td></tr>

        </table>
        </cfoutput>


			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select * from award_data
				 where recipient_duns = '#duns#' and
				       parent_award_id = '#contract_no#'

				   <cfif isdefined("sv")>

					<cfif #sv# is 1>
					 order by action_date DESC
					<cfelseif #sv# is 10>
					 order by action_date ASC
					<cfelseif #sv# is 2>
					 order by award_id_piid, modification_number ASC
					<cfelseif #sv# is 20>
					 order by award_id_piid, modification_number DESC
					<cfelseif #sv# is 3>
					 order by product_or_service_code ASC
					<cfelseif #sv# is 30>
					 order by product_or_service_code DESC
					<cfelseif #sv# is 4>
					 order by awarding_sub_agency_name ASC
					<cfelseif #sv# is 40>
					 order by awarding_sub_agency_name DESC
					<cfelseif #sv# is 5>
					 order by naics_code ASC
					<cfelseif #sv# is 50>
					 order by naics_code DESC
					<cfelseif #sv# is 6>
					 order by type_of_contract_pricing ASC
					<cfelseif #sv# is 60>
					 order by type_of_contract_pricing DESC
					<cfelseif #sv# is 7>
					 order by type_of_set_aside ASC
					<cfelseif #sv# is 70>
					 order by type_of_set_aside DESC
					<cfelseif #sv# is 8>
					 order by period_of_performance_start_date ASC
					<cfelseif #sv# is 80>
					 order by period_of_performance_start_date DESC
					<cfelseif #sv# is 9>
					 order by period_of_performance_potential_end_date ASC
					<cfelseif #sv# is 90>
					 order by period_of_performance_potential_end_date DESC
					<cfelseif #sv# is 11>
					 order by federal_action_obligation ASC
					<cfelseif #sv# is 110>
					 order by federal_action_obligation DESC
					</cfif>

				   <cfelse>
					 order by action_date DESC
				   </cfif>

				</cfquery>

			    <cfparam name="URL.PageId" default="0">
			    <cfset RecordsPerPage = 250>
			    <cfset TotalPages = (awards.Recordcount/RecordsPerPage)>
			    <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
			    <cfset EndRow = StartRow+RecordsPerPage-1>

			    </table>

			    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <cfoutput>
                <tr><td class="feed_sub_header" colspan=2><b>Total Awards - #numberformat(awards.recordcount,'999,999')#</b></td>
                    <td class="feed_option" align=right colspan=9>

				  <cfif awards.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&id=0&vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>

                    </td>

                </tr>
                </cfoutput>
			    <tr><td colspan=11><hr></td></tr>
			    <tr><td height=10></td></tr>

				<tr><td>

                                <cfoutput>
									<tr>
									   <td class="text_xsmall" width=75><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
									   <td class="text_xsmall"><b><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract Number</b></a></td>
									   <td class="text_xsmall"><b>Mod</b></td>
									   <td class="text_xsmall"><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
									   <td class="text_xsmall"><b>Award Description</b></td>
									   <td class="text_xsmall"><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Product or Service</b></a></td>
									   <td class="text_xsmall"><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a></td>
									   <td class="text_xsmall" align=center><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Pricing</b></a></td>
									   <td class="text_xsmall"><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>POP Start</b></a></td>
									   <td class="text_xsmall"><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>POP End</b></a></td>
									   <td class="text_xsmall" align=right><a href="awards.cfm?vehicle_no=#vehicle_no#&contract_no=#contract_no#&duns=#duns#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Obligation</b></a></td>
									</tr>
								</cfoutput>

								<cfset #total_value# = 0>

								<cfset #counter# = 0>

								<cfloop query="awards">

						        <cfif CurrentRow gte StartRow >

								  <tr

								  <cfif #counter# is 0>
								   bgcolor="ffffff"
								  <cfelse>
								   bgcolor="e0e0e0"
								  </cfif>


								  >

								  <cfoutput>

									 <td class="text_xsmall">#dateformat(awards.action_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall"><b><a href="/exchange/include/award_information.cfm?id=#awards.id#" target="_blank" rel="noopener" rel="noreferrer">#awards.award_id_piid#</a></b></td>
									 <td class="text_xsmall">#awards.modification_number#</td>
									 <td class="text_xsmall">#awards.awarding_sub_agency_name#</td>
									 <td class="text_xsmall" width=300>



									   <cfif isdefined("session.keywords")>
										#replaceNoCase(award_description,session.keywords,"<span style='background:yellow'>#session.keywords#</span>","all")#
									   <cfelse>
										#award_description#
									   </cfif>



									 #awards.award_description#</td>
									 <td class="text_xsmall">#awards.product_or_service_code_description#</td>
									 <td class="text_xsmall">#awards.naics_code#</td>
									 <td class="text_xsmall" align=center>
									 <cfif awards.type_of_contract_pricing is "Firm Fixed Price">
									  FFP
									 <cfelseif awards.type_of_contract_pricing is "Cost Plus Fixed Fee">
									  CPFF
									 <cfelseif awards.type_of_contract_pricing is "Time and Materials">
									  T&M
									 <cfelseif awards.type_of_contract_pricing is "Cost Plus Award Fee">
									  CPAF
									 <cfelse>
									  #left(awards.type_of_contract_pricing,21)#
									 </cfif>
									 </td>
									 <td class="text_xsmall" width=75>#dateformat(awards.period_of_performance_start_date,'mm/dd/yy')#</td>
									 <td class="text_xsmall" width=75>#dateformat(awards.period_of_performance_potential_end_date,'mm/dd/yy')#</td>
									 <td class="text_xsmall" width=50 align=right>#numberformat(awards.federal_action_obligation,'$999,999,999')#</td>

								  </cfoutput>

								  </tr>

								  <cfset #total_value# = #total_value# + #awards.federal_action_obligation#>

								  <cfif #counter# is 0>
								   <cfset #counter# = 1>
								  <cfelse>
								   <cfset #counter# = 0>
								  </cfif>

								  </cfif>

								  <cfif CurrentRow eq EndRow>
								   <cfbreak>
								  </cfif>

								</cfloop>

								<tr><td class="feed_option" colspan=10><b>Total:</b></td>

								<cfoutput>
									<td class="feed_option" align=right><b>#numberformat(total_value,'$999,999,999')#</b></td>
								</cfoutput>

                 </tr>
            </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>