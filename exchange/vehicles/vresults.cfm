<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.vehicle_setaside")>
 <cfset #session.vehicle_setaside# = 0>
</cfif>

  <cfquery name="usr_vehicle" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	select * from usr_vehicle
	where usr_vehicle_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfquery name="type" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select distinct(type_of_set_aside_code), type_of_set_aside, count(id) as total, sum(federal_action_obligation) as amount from award_data
	where parent_award_id like '#usr_vehicle.usr_vehicle_pattern#%' and
		  federal_action_obligation > 0 and
		  type_of_set_aside_code is not null
	group by type_of_set_aside_code, type_of_set_aside
	order by type_of_set_aside
  </cfquery>

  <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select sum(federal_action_obligation) as total from award_data
	where parent_award_id like '#usr_vehicle.usr_vehicle_pattern#%' and
		  federal_action_obligation > 0
		  <cfif session.vehicle_setaside is not 0>
           and type_of_set_aside_code = '#session.vehicle_setaside#'
		  </cfif>
  </cfquery>

  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select distinct(parent_award_id), recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as amount from award_data
	where parent_award_id like '#usr_vehicle.usr_vehicle_pattern#%' and
		  federal_action_obligation > 0
		  <cfif session.vehicle_setaside is not 0>
           and type_of_set_aside_code = '#session.vehicle_setaside#'
		  </cfif>
	group by parent_award_id, recipient_name, recipient_duns

	<cfif isdefined("sv")>

		<cfif #sv# is 1>
		 order by recipient_name DESC
		<cfelseif #sv# is 10>
		 order by recipient_name ASC
		<cfelseif #sv# is 2>
		 order by parent_award_id ASC
		<cfelseif #sv# is 20>
		 order by parent_award_id DESC
		<cfelseif #sv# is 3>
		 order by total DESC
		<cfelseif #sv# is 30>
		 order by total ASC
		<cfelseif #sv# is 4>
		 order by amount DESC
		<cfelseif #sv# is 40>
		 order by amount ASC
		</cfif>
   <cfelse>
		order by amount DESC
   </cfif>

  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">


      </td><td valign=top width=100%>

      <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <tr><td class="feed_header">Contract Vehicles</td>
	  	  <td align=right class="feed_sub_header"><a href="/exchange/vehicles/">Return</a></td></tr>
	     <tr><td colspan=2><hr></td></tr>
	    </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header"><cfoutput>#usr_vehicle.usr_vehicle_name#</cfoutput></td>
             <td class="feed_sub_header" style="font-weight: normal;" align=right>

             <cfif agencies.recordcount GT 0>

				 <form action="refresh.cfm" method="post">

				 <b>Filter by Set Aside: </b>&nbsp;

				 <select name="setaside" class="input_select" style="width: 250px;">
				 <option value=0>All
				 <cfoutput query="type">
				  <option value="#type.type_of_set_aside_code#" <cfif session.vehicle_setaside is "#type.type_of_set_aside_code#">selected</cfif>>#type.type_of_set_aside#
				 </cfoutput>
				 </select>

				 <cfoutput><input type="hidden" name="i" value=#i#></cfoutput>
                 <input type="hidden" name="location" value=1>
				 &nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Refresh">

				 </form>

             </cfif>

             </td></tr>

         <cfoutput>
         <tr><td class="feed_sub_header"><b>Total Awards: <cfoutput>#numberformat(total.total,'$999,999,999,999')#</cfoutput></td>

             </td>

             <td align=right class="feed_sub_header">

             <cfif agencies.recordcount GT 0>


              <a href="modify.cfm?i=#i#"><img src="/images/icon_edit.png" width=20 hspace=10 hspace=10 border=0 alt="Edit Vehicle" title="Edit Vehicle"></a>
              <a href="modify.cfm?i=#i#">Edit Vehicle</a>
              &nbsp;&nbsp;
              <a href="vdashboard.cfm?i=#i#&db=1"><img src="/images/icon_dashboard.png" width=20 hspace=10 border=0 alt="Dashboard" title="Dashboard"></a>
              <a href="vdashboard.cfm?i=#i#&db=1">Dashboard</a>
              &nbsp;&nbsp;
              <a href="vresults.cfm?i=#i#&export=1"><img src="/images/icon_export_excel.png" width=20 hspace=10 border=0 alt="Export to Excel" title="Export to Excel"></a>
              <a href="vresults.cfm?i=#i#&export=1">Export to Excel</a>
              </cfif>

             </td></tr>
         </cfoutput>
        </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td height=10></td></tr>

          <cfif isdefined("export")>
            <cfinclude template="/exchange/include/export_to_excel.cfm">
          </cfif>

          <cfif isdefined("u")>
           <cfif u is 2>
            <tr><td class="feed_sub_header" colspan=5><font color=green>Contract vehicle was successfully updated.</td></tr>
           </cfif>
          </cfif>

          <cfif agencies.recordcount is 0>

          <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3>No contracts or awards were found.</td></tr>

          <cfelse>

		  <cfoutput>

			  <tr height=40>
				  <td class="feed_sub_header"><a href="vresults.cfm?i=#i#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Vendor</b></a></td>
				  <td class="feed_sub_header"><a href="vresults.cfm?i=#i#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract Number</b></a></td>
				  <td class="feed_sub_header" align=center><a href="vresults.cfm?i=#i#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Awards</b></a></td>
				  <td class="feed_sub_header" align=right><a href="vresults.cfm?i=#i#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Amount</b></a></td>
				  <td class="feed_sub_header" align=right><b>$ Share</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>

          <cfoutput query="agencies">

          <cfif counter is 0>
           <tr bgcolor="ffffff" height=30>
          <cfelse>
           <tr bgcolor="e0e0e0" height=30>
          </cfif>

              <td class="feed_sub_header"><a href="vawards.cfm?i=#i#&duns=#recipient_duns#&contract_no=#parent_award_id#"><b>#recipient_name#</b></a></td>
              <td class="feed_sub_header" style="font-weight: normal;">#parent_award_id#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(total,'999,999')#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(amount,'$999,999,999')#</td>
               <cfif evaluate(agencies.total/total.total) LT 0>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>0.00%</td>
               <cfelse>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(evaluate((agencies.amount/total.total)*100),'99.99')#%</td>
               </cfif>

          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <tr><td>&nbsp;</td></tr>

          </cfif>

		  </table>

        </td>

        </tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>