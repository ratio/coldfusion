<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

        <form action="save.cfm" method="post">

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
	    <tr><td class="feed_header">Create Customer Vehicle - Search Results</td>
	  	  <td align=right class="feed_sub_header"><a href="/exchange/vehicles/">Return</a></td></tr>
	     <tr><td colspan=2><hr></td></tr>
	    </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header">Search Pattern - <cfoutput>"#vehicle_no#"</cfoutput></td>

             <td class="feed_sub_header" align=right style="font-weight: normal;"><b>Save Custom Vehicle as:</b> &nbsp;&nbsp;
              <input type="text" name="usr_vehicle_name" class="input_text" size=30>&nbsp;&nbsp;
   		   	  <input class="button_blue" type="submit" name="button" value="Save">

             </td></tr>

        </table>

        <cfoutput>

        <input type="hidden" name="usr_vehicle_pattern" value="#vehicle_no#">

        </cfoutput>

       </form>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=5 class="feed_header">Search Results</td></tr>
          <tr><td colspan=5><hr></td></tr>
          <tr><td height=10></td></tr>

		  <cfquery name="results" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select distinct(parent_award_id), recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as amount from award_data
			where parent_award_id like '#vehicle_no#%'
			group by parent_award_id, recipient_name, recipient_duns

			<cfif isdefined("sv")>

				<cfif #sv# is 1>
				 order by recipient_name DESC
				<cfelseif #sv# is 10>
				 order by recipient_name ASC
				<cfelseif #sv# is 2>
				 order by parent_award_id ASC
				<cfelseif #sv# is 20>
				 order by parent_award_id DESC
				<cfelseif #sv# is 3>
				 order by total DESC
				<cfelseif #sv# is 30>
				 order by total ASC
				<cfelseif #sv# is 4>
				 order by amount DESC
				<cfelseif #sv# is 40>
				 order by amount ASC
				</cfif>
		   <cfelse>
			 	order by recipient_name ASC
		   </cfif>

		  </cfquery>

		  <cfif results.recordcount is 0>

		  <tr><td class="feed_sub_header" style="font-weight: normal;">No contracts were found matching the search criteria you entered.</td></tr>

		  <cfelse>

		  <cfoutput>

			  <tr>
				  <td class="feed_sub_header"><a href="vlookup_results.cfm?vehicle_no=#vehicle_no#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Vendor</b></a></td>
				  <td class="feed_sub_header"><a href="vlookup_results.cfm?vehicle_no=#vehicle_no#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract Number</b></a></td>
				  <td class="feed_sub_header" align=center><a href="vlookup_results.cfm?vehicle_no=#vehicle_no#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Awards</b></a></td>
				  <td class="feed_sub_header" align=right><a href="vlookup_results.cfm?vehicle_no=#vehicle_no#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Amount</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>

          <cfoutput query="results">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

              <td class="feed_sub_header"><a href="awards.cfm?vehicle_no=#vehicle_no#&duns=#recipient_duns#&contract_no=#parent_award_id#">#recipient_name#</a></td>
              <td class="feed_sub_header" style="font-weight: normal;">#parent_award_id#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#total#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(amount,'$999,999,999')#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <tr><td>&nbsp;</td></tr>

          </cfif>

		  </table>

		  </td>

		  </tr>

	   </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>