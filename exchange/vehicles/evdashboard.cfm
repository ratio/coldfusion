<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfquery name="usr_vehicle" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from cv
	where cv_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header"><cfoutput>#usr_vehicle.cv_name#</cfoutput></td>
         <cfoutput>
             <td class="feed_sub_header" align=right>

             <a href="/exchange/vehicles/evresults.cfm?i=#i#">Return</a></td></tr>
         </cfoutput>
         <tr><td colspan=2><hr></td></tr>
        </table>

	    <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	      select distinct(parent_award_id), recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as amount from award_data
		  where parent_award_id like '#usr_vehicle.cv_pattern#%' and
			    federal_action_obligation > 0
			  <cfif session.vehicle_setaside is not 0>
			   and type_of_set_aside_code = '#session.vehicle_setaside#'
			  </cfif>
		  group by parent_award_id, recipient_name, recipient_duns
		  order by amount DESC
        </cfquery>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td valign=top>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Vendor', 'Award Value',{ role: 'link' }],
			  <cfoutput query="agencies">

			   <cfset vendor_name_1 = #replace(recipient_name,"&","all")#>
			   <cfset vendor_name_2 = #replace(recipient_name,"'","all")#>

			   ['#vendor_name_2#',#round(amount)#, '/exchange/vehicles/evawards.cfm?i=#i#&duns=#recipient_duns#&contract_no=#parent_award_id#&db=1'],


			  </cfoutput>
			]);

			var options = {
			'legend'   : 'right',
			'title'    : '',
			'pieHole'  : 0.4,
			'fontSize' : 11,
			'height'   : 600
			};

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));

 		google.visualization.events.addListener(chart, 'select', function (e) {
            var selection = chart.getSelection();
                if (selection.length) {
                    var row = selection[0].row;
                    let link =data.getValue(row, 2);
                    location.href = link;
                }
        });

        chart.draw(data, options);

      }

		</script>

        <div id="donutchart"></div>

        </td></tr>

      </table>



</div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>