<cfinclude template="/exchange/security/check.cfm">
<cfset #session.selected_company# = #selected_company#>

<cfif #session.selected_company# is 0>
 <cflocation URL="/exchange/company/create.cfm" addtoken="no">
<cfelse>

	<cfquery name="registered" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select company_id from company
	 where company_id = #selected_company# and
	       company_admin_id is null
	</cfquery>

	<cfif registered.recordcount is 1>
	 <cflocation url="/exchange/company/register.cfm" addtoken="no">
	<cfelse>
     <cflocation URL="/exchange/company/associate.cfm" addtoken="no">
	</cfif>

</cfif>