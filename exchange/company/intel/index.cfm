<cfinclude template="/exchange/security/check.cfm">

<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company_intel
 where company_intel_company_id = #session.company_id#
 order by company_intel_created_date DESC
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Private Intel<cfif #intel.recordcount# GT 0> (<cfoutput>#intel.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header">
			       <cfif company_admin is 1>
                    <a href="/exchange/company/intel/add.cfm?l=11"><img src="/images/icon_add.png" width=20 border=0 alt="Add Intel" title="Add Intel" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/intel/add.cfm?l=13">Add Intel</a>
                   </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Company Intel has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Company Intel has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Company Intel has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif intel.recordcount is 0>
                <tr><td class="feed_sub_header">No Company Intel has been created.</td></tr>
               <cfelse>

               <cfset count = 1>

			   <cfloop query="intel">

			   <cfoutput>

				   <tr>
				       <td class="feed_sub_header"><a href="edit.cfm?l=13&company_intel_id=#intel.company_intel_id#">#intel.company_intel_name#</a></td>
				       <td class="feed_sub_header">#intel.company_intel_desc#</td>
				       <td class="feed_sub_header">#dateformat(intel.company_intel_created_date,'mm/dd/yyyy')#</td>
				   </tr>

                 </cfoutput>

				  <cfif count is not cert.recordcount>
				   <tr><td height=20></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfloop>

				</cfif>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 	   </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>