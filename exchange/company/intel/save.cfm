<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Cancel">

     <cflocation URL="/exchange/company/" addtoken="no">

<cfelseif #button# is "Add Certification">

	<cfif #cert_attachment# is not "">
		<cffile action = "upload"
		 fileField = "cert_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert cert
	  (cert_order,
	   cert_date_acquired,
	   cert_public,
	   cert_attachment,
	   cert_keywords,
	   cert_name,
	   cert_desc,
	   cert_updated,
	   cert_company_id,
	   cert_hub_id,
	   cert_created_by)
	  values
	  (#cert_order#,
	   <cfif cert_date_acquired is "">null<cfelse>'#cert_date_acquired#'</cfif>,
	   <cfif isdefined("cert_public")>1<cfelse>null</cfif>,
	   <cfif #cert_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	   '#cert_keywords#',
	   '#cert_name#',
	   '#cert_desc#',
	    #now()#,
	    #session.company_id#,
	   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	    #session.usr_id#
	   )
	</cfquery>

	<cflocation URL="/exchange/company/certs/index.cfm?l=11&u=3" addtoken="no">

<cfelseif #button# is "Delete">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select cert_attachment from cert
		  where (cert_id = #cert_id#) and
				(cert_company_id = #session.company_id#)
		</cfquery>

		<cfif remove.cert_attachment is not "">
		 <cffile action = "delete" file = "#media_path#\#remove.cert_attachment#">
		</cfif>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete cert
		  where (cert_id = #cert_id#) and
				(cert_company_id = #session.company_id#)
		</cfquery>

	</cftransaction>

	<cflocation URL="/exchange/company/certs/index.cfm?l=11&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select cert_attachment from cert
		  where cert_id = #cert_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.cert_attachment#">

	</cfif>

	<cfif #cert_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select cert_attachment from cert
		  where cert_id = #cert_id#
		</cfquery>

		<cfif #getfile.cert_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.cert_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "cert_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update cert
	  set cert_name = '#cert_name#',

		  <cfif #cert_attachment# is not "">
		   cert_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   cert_attachment = null,
		  </cfif>

	      cert_keywords = '#cert_keywords#',
	      cert_date_acquired = <cfif #cert_date_acquired# is "">null<cfelse>'#cert_date_acquired#'</cfif>,
	      cert_desc = '#cert_desc#',
	      cert_public = <cfif isdefined("cert_public")>1<cfelse>null</cfif>,
	      cert_order = #cert_order#,
	      cert_updated = #now()#
	  where (cert_id = #cert_id# ) and
	        (cert_company_id = #session.company_id#)
	</cfquery>

	<cflocation URL="/exchange/company/certs/index.cfm?l=11&u=1" addtoken="no">

</cfif>