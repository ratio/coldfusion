<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from comments_rating
 order by comments_rating_value DESC
</cfquery>

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" colspan=2>Add Company Intelligence</td><td align=right><a href="index.cfm?l=13"><img src="/images/delete.png" width=20 alt="Close" title="Close" valign=middle border=0></a></td></tr>
			   <tr><td colspan=2 colspan=2><hr></td></tr>
			   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>Company Intelligence is private to you and/or your company.  Company Intelligence is not shared with people or organizations outside your company.</td></tr>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" width=200><b>Context</b></td>
                    <td><input name="company_intel_context" class="input_text" type="text" size=100 maxlength="1000" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
                    <td><textarea name="company_intel_desc" class="input_textarea" rows=6 cols=101 placeholder="Please provide any comments related to this intel."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Reference URL</b></td>
                    <td><input name="company_intel_url" class="input_text" type="text" size=100 maxlength="1000"></td>
                </tr>

                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Attachment</td>
                    <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="company_intel_attachment"></td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Company Intel"></td></tr>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>