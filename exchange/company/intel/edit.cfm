<cfinclude template="/exchange/security/check.cfm">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from cert
 where (cert_company_id = #session.company_id#) and
       (cert_id = #cert_id#)
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Edit Certification</td><td align=right>
                   <a href="index.cfm?l=11"><img src="/images/delete.png" width=20 alt="Close" title="Close" valign=middle border=0></a>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Certification</b></td>
                    <td><input name="cert_name" class="input_text" type="text" size=100 maxlength="100" value="#info.cert_name#" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
                    <td><textarea name="cert_desc" class="input_textarea" rows=6 cols=101>#info.cert_desc#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Tags & Keywords</b></td>
                    <td><input name="cert_keywords" class="input_text" type="text" size=100 maxlength="1000" value="#info.cert_keywords#"></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Certification Date</b></td>
                    <td><input name="cert_date_acquired" type="date" class="input_date" type="text" value="#info.cert_date_acquired#"></td>
                </tr>

                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Certification Logo / Picture</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #info.cert_attachment# is "">
					  <input type="file" name="cert_attachment">
					<cfelse>
					  <img src="#media_virtual#/#info.cert_attachment#" width=150><br><br>
					  <input type="file" name="cert_attachment"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					 </cfif>

                <input type="hidden" name="cert_id" value=#cert_id#>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="cert_order" required class="input_text" style="width: 75px;" required type="number" step=".1" value="#info.cert_order#">&nbsp;&nbsp;Determines the order this product or service will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="cert_public" style="width: 20px; height: 20px;" value=1 <cfif #info.cert_public# is 1>checked</cfif>>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update">
				     &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Certification?\r\nAre you sure you want to delete this Certification?');">
				</td></tr>


                 </cfoutput>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>