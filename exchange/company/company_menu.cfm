<cfquery name="cert_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(cert_id) as total from cert
 where cert_company_id = #session.company_id#
</cfquery>

<cfquery name="product_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(product_id) as total from product
 where product_company_id = #session.company_id#
</cfquery>

<cfquery name="customer_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(customer_id) as total from customer
 where customer_company_id = #session.company_id#
</cfquery>

<cfquery name="partner_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(partner_id) as total from partner
 where partner_company_id = #session.company_id#
</cfquery>

<cfquery name="media_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(media_id) as total from media
 where media_company_id = #session.company_id#
</cfquery>

<cfquery name="marketing_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(doc_id) as total from doc
 where doc_company_id = #session.company_id#
</cfquery>

<cfquery name="news_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(news_id) as total from news
 where news_company_id = #session.company_id#
</cfquery>

<cfquery name="user_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(usr_comp_usr_id) as total from usr
 join usr_comp on usr_comp_usr_id = usr_id
 where usr_comp_company_id = #session.company_id#
</cfquery>

<cfoutput>
	<table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Company Profile</td></tr>
		   <tr><td colspan=3><hr></td></tr>
           <tr><td height=20></td></tr>
		   <tr><td width=20><cfif not isdefined("l")><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" height=25 style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" colspan=2><a href="/exchange/company/">Overview</a></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 3><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/products/index.cfm?l=3">Products & Services</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #product_count.total# GT 0>#product_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 4><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/customers/index.cfm?l=4">Customer Snapshots</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #customer_count.total# GT 0>#customer_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 5><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/partners/index.cfm?l=5">Partners & Alliances</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #partner_count.total# GT 0>#partner_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 7><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/media/index.cfm?l=7">Press & Media</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #media_count.total# GT 0>#media_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 6><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/marketing/index.cfm?l=6">Marketing Material</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #marketing_count.total# GT 0>#marketing_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 11><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/certs/index.cfm?l=11">Certifications</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #cert_count.total# GT 0>#cert_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 10><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/news/index.cfm?l=10">Profile News</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #news_count.total# GT 0>#news_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 12><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/users/index.cfm?l=12">Users</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##283E4A; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #user_count.total# GT 0>#user_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 9><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/company/federal/index.cfm?l=9">Federal Profile</a></td><td height=25 class="feed_sub_header"></td></tr>
           <tr><td height=15></td></tr>

	</table>
</cfoutput>