<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Register Company">

	    <cftransaction>

			<cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  update company
			  set company_name = '#company_name#',
				  company_city = '#company_city#',
				  company_state = '#company_state#',
				  company_about = '#company_about#',
				  company_keywords = '#company_keywords#',
				  company_duns = '#company_duns#',
				  company_registered = #now()#,
				  company_updated = #now()#,
				  company_certify_rep = #session.usr_id#,
				  company_admin_id = #session.usr_id#
			  where company_id = #session.selected_company#
			</cfquery>

			<cfquery name="update_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 update usr
			 set usr_company_id = #session.selected_company#
			 where usr_id = #session.usr_id#
			</cfquery>

	    </cftransaction>

		<cfset session.company_id = #session.selected_company#>

        <cflocation URL="/exchange/company/index.cfm?u=7" addtoken="no">

<cfelseif #button# is "Create Company">

    <cftransaction>

		<cfif #company_logo# is not "">

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

	    <cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into company
		 (
		  company_name,
		  company_city,
		  company_state,
		  company_keywords,
		  company_about,
          company_duns,
	      company_logo,
		  company_registered,
		  company_updated,
		  company_website,
		  company_certify_rep,
		  company_admin_id
		  )
		  values
		  ('#company_name#',
		   '#company_city#',
		   '#company_state#',
		   '#company_keywords#',
		   '#company_about#',
           '#company_duns#',
 			<cfif #company_logo# is not "">
			'#cffile.serverfile#',
			<cfelse>
			 null,
			</cfif>
            #now()#,
			#now()#,
		   '#company_website#',
		    1,
		    #session.usr_id#
		   )
		</cfquery>

	    <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(company_id) as id from company
		</cfquery>

        <cfset #session.company_id# = #max.id#>

	        <cfquery name="update_usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		     update usr
		     set usr_company_id = #max.id#
		     where usr_id = #session.usr_id#
		    </cfquery>

			<cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select market_id from market, align
			  where (market.market_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 2)
					<cfif isdefined("session.hub")>
					 and (align.align_hub_id = #session.hub#)
					<cfelse>
					 and (align.align_hub_id is null)
					</cfif>
					order by market.market_name
			</cfquery>

			<cfset market_list = #valuelist(market.market_id)#>

			<cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select sector_id from sector, align
			  where (sector.sector_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 3)
					<cfif isdefined("session.hub")>
					 and (align.align_hub_id = #session.hub#)
					<cfelse>
					 and (align.align_hub_id is null)
					</cfif>
					order by sector.sector_name
			</cfquery>

			<cfset sector_list = #valuelist(sector.sector_id)#>

			<cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select topic_id from topic, align
			  where (topic.topic_id = align.align_type_value) and
					(align.align_usr_id = #session.usr_id#) and
					(align.align_type_id = 4)
					<cfif isdefined("session.hub")>
					 and (align.align_hub_id = #session.hub#)
					<cfelse>
					 and (align.align_hub_id is null)
					</cfif>
					order by topic.topic_name
			</cfquery>

			<cfset topic_list = #valuelist(topic.topic_id)#>

		<cfloop index="m_element" list=#market_list#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#session.company_id#, 2, #m_element#)
			</cfquery>

		</cfloop>

		<cfloop index="s_element" list=#sector_list#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#session.company_id#, 3, #s_element#)
			</cfquery>

		</cfloop>

		<cfloop index="t_element" list=#topic_list#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value)
			 Values
			 (#session.company_id#, 4, #t_element#)
			</cfquery>

		</cfloop>

	</cftransaction>

</cfif>

<cflocation URL="/exchange/company/index.cfm?u=4" addtoken="no">