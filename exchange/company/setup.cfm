<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<style>
.banner {
    background-image: url('/images/company_stock_banner.png');
    max-width: 100%;
    background-size: 100%;
    height:250px;
}
</style>

<cfinclude template = "/exchange/include/header.cfm">

<div class="main_box">

<table cellspacing=0 cellpadding=0 border=0 width=100% class="banner">
   <tr><td height=30></td></tr>
   <tr><td align=middle valign=middle width=175>
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td align=middle><img src="/images/no_logo.png" width=150 style="border: 2px solid ##ffffff;"></td></tr>
 	   </table>

	   </td><td width=30>&nbsp;</td><td valign=top align=left>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" style="font-size: 40px; color: ffffff;">My Company</td>
		     <td align=right>
         </td></tr>
		 </tr>
 	 	 <tr><td height=5></td></tr>
	 	 <tr><td class="feed_header" style="font-size: 20px; color: ffffff;">Company Tag Line</td></tr>
		 <tr><td height=10></td></tr>
		 <tr><td class="feed_header" style="font-size: 16px; color: ffffff;">City, State</td></tr>
		 <tr><td height=15></td></tr>
		 <tr><td class="feed_header" style="font-size: 20px; color: ffffff;"><a href="/exchange/profile" style="color: #FFFFFF;">My Profile</a> | <a href="/exchange/company/" style="color: #FFFFFF;">My Company Profile</a></td></tr>
	   </table>

	   </td></tr>

   <tr><td height=30></td></tr>

   </table>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=20></td></tr>
	   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Setup Company</td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">We collect information from dozens of sources to create profiles on over 1 million companies.   To create your company profile, let's first see if it's already registered by someone or if we have any information to start with.</td></tr>

	   <form action="/exchange/company/lookup.cfm" method="post">
	    <tr><td class="feed_sub_header">Company Name</td></tr>
	    <tr><td><input type="text" class="input_text" size=50 name="company_keyword" placeholder="Enter Company Name" required></td></tr>
	    <tr><td height=10></td></tr>
	    <tr><td><input type="submit" class="button_blue_large" value="Search"></td></tr>
	   </form>

   </table>

   </div>

   </td></tr>

</table>

</div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>