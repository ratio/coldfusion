<cfinclude template="/exchange/security/check.cfm">
<cfset session.company_id = #id#>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_comp_id from usr_comp
 where usr_comp_company_id = #id# and
       usr_comp_usr_id = #session.usr_id#
</cfquery>

<cfif check.recordcount is 0>
 <cflocation URL="profiles.cfm?u=2" addtoken="no">
</cfif>

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update usr
 set usr_company_id = #id#
 where usr_id = #session.usr_id#
</cfquery>

<cflocation URL="profiles.cfm?u=1" addtoken="no">
