<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfif not isdefined("session.selected_company") and session.selected_company is not 0>
 <cflocation URL="/exchange/company/setup.cfm" addtoken="no">
</cfif>

<cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.selected_company#
</cfquery>

<cfquery name="user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #info.company_admin_id#
</cfquery>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">ASSOCIATE WITH COMPANY</td>
			       <td align=right><a href="/exchange/company/setup.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
		      </table>

        <form action="/exchange/company/create_db.cfm" enctype="multipart/form-data" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		    <tr>
		        <td class="feed_sub_header"style="font-weight: normal;">This company has already been registered by an authorized company user.</td></tr>
  		    <tr><td><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

 			<cfoutput>

 			<tr><td class="feed_sub_header" width=15%>Company Name</td>
 			    <td class="feed_sub_header" style="font-weight: normal;">#info.company_name#</td></tr>

 			<tr><td class="feed_sub_header" valign=top>Company Administrator</td>
 			    <td class="feed_sub_header" style="font-weight: normal;">#user.usr_first_name# #user.usr_last_name#<br>#user.usr_email#<br>#user.usr_phone#</td></tr>

			</cfoutput>

			<tr><td colspan=2><hr></td></tr>

		    <tr><td class="feed_sub_header"style="font-weight: normal;" colspan=2>Please contact this user to be associated with this company.</td></tr>

       </table>

        </form>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>