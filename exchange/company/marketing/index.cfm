<cfinclude template="/exchange/security/check.cfm">

<cfquery name="doc" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from doc
 where doc_company_id = #session.company_id#
 order by doc_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Marketing Material<cfif #doc.recordcount# GT 0> (<cfoutput>#doc.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header" valign=middle>
                    <cfif session.update_access is 1>
                    <a href="/exchange/company/marketing/add.cfm?l=6"><img src="/images/plus3.png" width=15 valign=middle border=0 alt="Add Marketing Material" title="Add Marketing Material"></a>&nbsp;&nbsp;<a href="/exchange/company/marketing/add.cfm?l=6">Add Marketing Material</a>
                    </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Marketing material has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Marketing material has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Marketing material has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif doc.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">No Marketing Material has been entered.</td></tr>

               <cfelse>

               <cfset count = 1>

			   <cfoutput query="doc">

               <tr>
                   <td valign=top class="feed_sub_header" align=center>

                   </td>

				   <td></td><td valign=top>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=top colspan=2 valign=middle>#doc_name#</td>
				       <td align=right class="feed_sub_header" valign=middle>
				       <cfif session.update_access is 1>
				       <a href="/exchange/company/marketing/edit.cfm?l=6&doc_id=#doc_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Marketing Material" title="Edit Marketing Material" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/marketing/edit.cfm?l=6&doc_id=#doc_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;" valign=top>#replace(doc_description,"#chr(10)#","<br>","all")#</td></tr>

				   <cfif doc_embed is not "">
				   <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;" valign=top>#doc_embed#</td></tr>
				   </cfif>

				   <cfif doc_attachment is not "">
				   	<tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;" valign=top><a href="#media_virtual#/#doc_attachment#" target="_blank" rel="noopener" rel="noreferrer"><b>Download Document</b></a></td></tr>
				   </cfif>
				   <cfif doc_url is not "">
				   	<tr><td class="feed_option" colspan=3 style="font-weight: normal;" valign=top><a href="#doc_url#" target="_blank" rel="noopener" rel="noreferrer">#doc_url#</a></td></tr>
				   </cfif>

                 </table>

                 </td></tr>

				  <cfif count is not doc.recordcount>
				   <tr><td height=10></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

				</cfif>

              </table>

          </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>