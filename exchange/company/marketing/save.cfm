<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Marketing Material">

	<cfif #doc_attachment# is not "">
		<cffile action = "upload"
		 fileField = "doc_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert doc
	  (doc_url, doc_embed, doc_order, doc_public, doc_attachment, doc_name, doc_description, doc_company_id, doc_created_by_id, doc_updated)
	  values
	  ('#doc_url#','#doc_embed#',#doc_order#, <cfif isdefined("doc_public")>1<cfelse>null</cfif>,<cfif #doc_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,'#doc_name#','#doc_description#',#session.company_id#,#session.usr_id#,#now()#)
 	</cfquery>

    <cflocation URL="/exchange/company/marketing/index.cfm?l=6&u=3" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select doc_attachment from doc
	  where (doc_id = #doc_id#) and
	        (doc_company_id = #session.company_id#)
	</cfquery>

    <cfif remove.doc_attachment is not "">
     <cfif fileexists("#meia_path#\#remove.doc_attachment#")>
		 <cffile action = "delete" file = "#media_path#\#remove.doc_attachment#">
     </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete doc
	  where (doc_id = #doc_id#) and
	        (doc_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/marketing/index.cfm?l=6&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select doc_attachment from doc
		  where doc_id = #doc_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.doc_attachment#")>
			<cffile action = "delete" file = "#media_path#\#remove.doc_attachment#">
		</cfif>

	</cfif>

	<cfif #doc_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select doc_attachment from doc
		  where doc_id = #doc_id#
		</cfquery>

		<cfif #getfile.doc_attachment# is not "">
         <cfif fileexists("#media_path#\#getfile.doc_attachment#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.doc_attachment#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "doc_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update doc
	  set doc_name = '#doc_name#',

		  <cfif #doc_attachment# is not "">
		   doc_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   doc_attachment = null,
		  </cfif>

	      doc_description = '#doc_description#',
	      doc_updated = #now()#,
	      doc_url = '#doc_url#',
	      doc_order = #doc_order#,
	      doc_embed = '#doc_embed#',
	      doc_public = <cfif #isdefined("doc_public")#>1<cfelse>null</cfif>
	  where (doc_id = #doc_id# ) and
	        (doc_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/marketing/index.cfm?l=6&u=1" addtoken="no">

</cfif>