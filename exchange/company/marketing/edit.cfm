<cfinclude template="/exchange/security/check.cfm">

<cfquery name="doc_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from doc
 where (doc_company_id = #session.company_id#) and
       (doc_id = #doc_id#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Edit Marketing Material</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm">Return</a>
               </td></tr>

			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Name</b></td>
                    <td><input name="doc_name" class="input_text" type="text" size=100 maxlength="100" value="#doc_profile.doc_name#" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
                    <td><textarea name="doc_description" class="input_textarea" rows=6 cols=101 placeholder="Please provide an overview of this Marketing Material.">#doc_profile.doc_description#</textarea></td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top><b>Embed Video or Media</b></td>
                    <td><textarea name="doc_embed" class="input_textarea" rows=4 cols=101 placeholder="Use this if you want to embed an image or video.  If not, leave blank.">#doc_profile.doc_embed#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top>Attachment</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #doc_profile.doc_attachment# is "">
					  <input type="file" name="doc_attachment">
					<cfelse>
					  <a href="#media_virtual#/#doc_profile.doc_attachment#" target="_blank" rel="noopener" rel="noreferrer">#doc_profile.doc_attachment#</a><br><br>
					  <input type="file" name="doc_attachment"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove existing attachment
					 </cfif>

					</td></tr>

                <tr><td class="feed_sub_header"><b>URL</b></td>
                    <td><input name="doc_url" class="input_text" type="url" size=100 maxlength="300" value="#doc_profile.doc_url#" placeholder="http://www..."></td>
                </tr>

                <input type="hidden" name="doc_id" value=#doc_id#>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="doc_order" required class="input_text" style="width: 75px;" required type="number" step=".1" value="#doc_profile.doc_order#">&nbsp;&nbsp;Determines the order this doc will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="doc_public" style="width: 20px; height: 20px;" value=1 <cfif #doc_profile.doc_public# is 1>checked</cfif>>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update">
				     &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Marketing Material?\r\nAre you sure you want to delete this record?');">
				</td></tr>

                </cfoutput>

               </table>

			   </form>

               </td></tr>

             </table>

           </td></tr>

         </table>

 		</div>

	 </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>