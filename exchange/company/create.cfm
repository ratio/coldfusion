<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfif not isdefined("session.selected_company") and session.selected_company is not 0>
 <cflocation URL="/exchange/company/setup.cfm" addtoken="no">
</cfif>

<cfquery name="states" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">CREATE COMPANY</td>
			       <td align=right><a href="/exchange/company/setup.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
		      </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="/exchange/company/create_db.cfm" enctype="multipart/form-data" method="post">

 			<tr><td class="feed_sub_header">Company Name&nbsp;<b>*</b></td></tr>
			<tr><td><input class="input_text" type="text" name="company_name" style="width: 400px;" required placeholder="Enter Company Name"></td></tr>
 			<tr><td class="feed_sub_header">Location&nbsp;<b>*</b></td></tr>
			<tr><td><input class="input_text" type="text" name="company_city" style="width: 225px;" required placeholder="City">

		    <select name="company_state" class="input_select">
		    <option value=0>Select State
		    <cfoutput query="states">
		     <option value="#state_abbr#">#state_name#
            </cfoutput>

			</td></tr>
		    <tr><td class="feed_sub_header">Website</td></tr>
		    <tr><td class="feed_option"><input type="url" class="input_text" style="width: 400px;" name="company_website" placeholder="http://www..."></td></tr>
            <tr><td class="feed_sub_header">Company Description</td></tr>
			<tr><td><textarea name="company_about" class="input_textarea" style="width: 700px;" rows=3></textarea></td></tr>
            <tr><td class="feed_sub_header">Company Tags or Keywords</td></tr>
            <tr><td class="feed_option"><input type="text" class="input_text" style="width: 700px;" placeholder="i.e., machine learning, intelligence, cloud, etc." name="company_keywords"></td></tr>
            <tr><td class="feed_sub_header">DUNS Number</td></tr>
            <tr><td class="feed_option"><input type="text" class="input_text" style="width: 200px;" name="company_duns"></td></tr>
            <tr><td class="feed_sub_header">Company Logo or Image</td></tr>
			<tr><td><input type="file" name="company_logo"></td></tr>
			<tr><td height=15></td></tr>
            <tr><td class="feed_option"><input type="checkbox" name="certify" style="height: 22px; width: 22px;" required>&nbsp;&nbsp;&nbsp;I verify that I am an official representative of this company and have the right to act on behalf of the company.&nbsp;<b>*</b></td></tr>
			<tr><td height=15></td></tr>
			<tr><td><input class="button_blue_large" type="submit" name="button" value="Create Company"></td></tr>
			<tr><td height=10></td></tr>
			<tr><td class="text_xsmall">* - Required Field</td></tr>

        </form>

        </table>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

