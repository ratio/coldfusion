<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

	<cftransaction>

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select count(broker_id) as total from broker
		 where broker_company_id = #session.company_id#
		</cfquery>

			<cfquery name="update_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  update broker
			  set broker_services = '#broker_services#',
				  broker_tagline = '#broker_tagline#',
				  broker_services_keywords = '#broker_services_keywords#'
			  where broker_company_id = #session.company_id#
			</cfquery>

			<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 delete xref_comp_broker
			 where xref_comp_broker_company_id = #session.company_id#
			</cfquery>

			<cfif isdefined("broker_type_id")>

				<cfloop index="broker" list=#broker_type_id#>

					<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 Insert into xref_comp_broker
					 (xref_comp_broker_company_id, xref_comp_broker_broker_type_id)
					 Values
					 (#session.company_id#, #broker#)
					</cfquery>

				</cfloop>

			 </cfif>

	</cftransaction>

</cfif>

<cflocation URL="/exchange/company/broker/index.cfm?u=1&l=14" addtoken="no">