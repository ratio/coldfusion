<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfquery name="broker_types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from broker_type
 order by broker_type_name
</cfquery>

<cfquery name="alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select xref_comp_broker_broker_type_id from xref_comp_broker
 where xref_comp_broker_company_id = #session.company_id#
</cfquery>

<cfif alignments.recordcount is 0>
 <cfset broker_type_list = 0>
<cfelse>
 <cfset broker_type_list = valuelist(alignments.xref_comp_broker_broker_type_id)>
</cfif>

<cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Update Broker Services</td></tr>
			   <tr><td colspan=2><hr></td></tr>

               <tr><td>

	             <form action="update_db.cfm" method="post">

	             <cfoutput>

	             <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Tagline</td>
                      <td><input type="text" class="input_text" name="broker_tagline" maxlength="300" size=100 value="#company_profile.broker_tagline#"></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Describe Broker<br>Services</td>
                      <td><textarea name="broker_services" class="input_textarea" cols=101 rows=8 placeholder="Please describe the Broker services you provide to customers.">#company_profile.broker_services#</textarea></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150 valign=top>Keywords</td>
                      <td valign=top><input type="text" name="broker_services_keywords" class="input_text" size=100 value="#company_profile.broker_services_keywords#" placeholder="Keywords that describe your products or services." maxlength="1000">
                      <br><span class="link_small_gray">Please seperate keywords by a comma (i.e., Business Development, Recruiting, etc.)</span></td></tr>

                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

                  </cfoutput>

                  <tr>

                      <td class="feed_sub_header" valign=top>Broker<br>Alignments</td>
                      <td class="feed_sub_header" style="font-weight: normal;">

					  <select class="input_select" name="broker_type_id" style="width: 300px; height: 175px;" multiple>
					   <cfoutput query="broker_types">
						<option value=#broker_type_id# <cfif listfind(broker_type_list,broker_type_id)>selected</cfif>>#broker_type_name#
					   </cfoutput>
					  </select>

                  </td></tr>

                  <tr><td></td><td class="link_small_gray">Use the CTRL button to select multiple values.</td></tr>


                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

		          <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>

		          </form>

           </table>

           </td></tr>

         </table>

         </td></tr>


       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>