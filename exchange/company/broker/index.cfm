<cfinclude template="/exchange/security/check.cfm">

<cfif session.company_id is 0>
 <cflocation URL="/exchange/company/setup.cfm" addtoken="no">
</cfif>

<cfquery name="broker_type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from broker_type
 order by broker_type_name
</cfquery>

<cfquery name="broker" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from xref_comp_broker
 join broker_type on broker_type_id = xref_comp_broker_broker_type_id
 where xref_comp_broker_company_id = #session.company_id#
 order by broker_type_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Broker Services</td>
                   <cfif company_admin is 1>
                     <td align=right class="feed_sub_header" valign=middle><a href="/exchange/company/broker/update.cfm?l=14"><img src="/images/icon_edit.png" width=20 border=0 alt="Update" title="Update" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/broker/update.cfm?l=14">Update</a>
                   </cfif>
                   </td></tr>
               <tr><td colspan=2><hr></td></tr>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Broker services information has been successfully changed.</b></font></td></tr>
                </cfif>
              </cfif>

               <tr><td colspan=2>

	             <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Tagline</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif #company_profile.broker_tagline# is not "">
                        #company_profile.broker_tagline#
                       <cfelse>
                        Not provided.
                       </cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Broker Services</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif #company_profile.broker_services# is not "">
                        #replace(company_profile.broker_services,"#chr(10)#","<br>","all")#
                       <cfelse>
                        Not provided.
                       </cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150 valign=top>Keywords</td>
                      <td class="feed_sub_header" valign=top style="font-weight: normal;">
                       <cfif #company_profile.broker_services_keywords# is not "">
                       #company.company_broker_services_keywords#
                       <cfelse>
                       Not provided.
                       </cfif>
                       </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>

   				</cfoutput>

                  <tr>
                      <td class="feed_sub_header" valign=top>Broker<br>Alignments</td>
                      <td class="feed_sub_header" style="font-weight: normal;" valign=top>

				      <cfoutput query="broker">
				      -&nbsp;&nbsp;#broker.broker_type_name#<br>
				      </cfoutput>

                       </td></tr>

               </table>
           </td></tr>
         </table>
        </td></tr>
     </table>
   </div>

   </td></tr>
</table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>