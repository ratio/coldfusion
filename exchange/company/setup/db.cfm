<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Associate">

		<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select company_admin_id, company_name from company
		  where company_id = #i#
		</cfquery>

		<cfif #comp.company_admin_id# is "">
		 <cfset company_admin = 0>
		<cfelse>
 		 <cfset company_admin = 1>
		</cfif>

		<cftransaction>

			<cfquery name="associate" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  delete usr_comp
			  where usr_comp_company_id = #i# and
					usr_comp_usr_id = #session.usr_id#
			</cfquery>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert usr_comp
			  (
			  usr_comp_usr_id,
			  usr_comp_company_id,
			  usr_comp_access_rights,
			  usr_comp_updated
			  )
			  values
			  (
			  #session.usr_id#,
			  #i#,
			  0,
			  #now()#
			  )
			</cfquery>

		</cftransaction>

		<cfquery name="user_update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update usr
		  set usr_company_id = #i#,
		      usr_company_name = '#comp.company_name#'
		  where usr_id = #session.usr_id#
		</cfquery>

		<cfset session.company_id = #i#>

	<cflocation URL="complete.cfm?i=#i#&u=2&a=#company_admin#" addtoken="no">

<cfelseif button is "Add Company">

		<cfif #company_logo# is not "">

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

			<cftransaction>

			<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  insert into company
			   (
				company_size_id,
				company_admin_id,
				company_product_summary,
				company_tagline,
				company_long_desc,
				company_entity_id,
				company_registered,
				company_registered_by_usr_id,
				company_registered_by_hub_id,
				company_name,
				company_about,
				company_logo,
				company_website,
				company_poc_first_name,
				company_poc_last_name,
				company_poc_title,
				company_poc_email,
				company_poc_phone,
				company_address_l1,
				company_address_l2,
				company_city,
				company_state,
				company_zip,
				company_duns,
				company_history,
				company_leadership,
				company_founded,
				company_employees,
				company_keywords,
				company_fy16_revenue,
				company_fy17_revenue,
				company_fy18_revenue,
				company_updated
				)
			  values
			  (
				 <cfif #company_size_id# is 0>null<cfelse>#company_size_id#</cfif>,
				 #session.usr_id#,
				'#company_product_summary#',
				'#company_tagline#',
				'#company_long_desc#',
				 <cfif #company_entity_id# is 0>null<cfelse>#company_entity_id#</cfif>,
				 #now()#,
				 #session.usr_id#,
				 #session.hub#,
				'#company_name#',
				'#company_about#',

				  <cfif #company_logo# is not "">
				   '#cffile.serverfile#',
				  <cfelse>
				   null,
				  </cfif>

				'#company_website#',
				'#company_poc_first_name#',
				'#company_poc_last_name#',
				'#company_poc_title#',
				'#company_poc_email#',
				'#company_poc_phone#',
				'#company_address_l1#',
				'#company_address_l2#',
				'#company_city#',
				<cfif #company_state# is 0>null<cfelse>'#company_state#'</cfif>,
				'#company_zip#',
				'#company_duns#',
				'#company_history#',
				'#company_leadership#',
				 <cfif #company_founded# is "">null<cfelse>#company_founded#</cfif>,
				 <cfif #company_employees# is "">null<cfelse>#company_employees#</cfif>,
				'#company_keywords#',
				 <cfif #company_fy16_revenue# is "">null<cfelse>#company_fy16_revenue#</cfif>,
				 <cfif #company_fy17_revenue# is "">null<cfelse>#company_fy17_revenue#</cfif>,
				 <cfif #company_fy18_revenue# is "">null<cfelse>#company_fy18_revenue#</cfif>,
				 #now()#
			   )
			</cfquery>

			<cfquery name="max" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select max(company_id) as id from company
			</cfquery>

			</cftransaction>

			<cftransaction>

			<cfquery name="add_to_company_users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert usr_comp
			 (
			 usr_comp_usr_id,
			 usr_comp_company_id,
			 usr_comp_hub_id,
			 usr_comp_access_rights,
			 usr_comp_updated
			 )
			 values
			 (
			 #session.usr_id#,
			 #max.id#,
			 #session.hub#,
			 1,
			 #now()#
			 )
			</cfquery>

				<cfquery name="update_usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
                 update usr
                 set usr_company_id = #max.id#
                 where usr_id = #session.usr_id#
				</cfquery>

				<cfset session.company_id = #max.id#>

				<cfquery name="add_to_network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert hub_comp
				 (
				 hub_comp_hub_id,
				 hub_comp_company_id,
				 hub_comp_added_by_usr_id,
				 hub_comp_added
				 )
				 values
				 (
				  #session.hub#,
				  #max.id#,
				  #session.usr_id#,
				  #now()#
				 )
				</cfquery>

			</cftransaction>

			 <cfset #session.crawl_company_id# = #max.id#>
	         <cfinclude template="/exchange/include/crawl_website.cfm">

			<cflocation URL="complete.cfm?u=1&i=#max.id#" addtoken="no">

</cfif>