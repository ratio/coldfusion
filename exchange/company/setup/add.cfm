<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.check") and session.check is 1>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="certify" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<cfquery name="size" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from size
</cfquery>

<cfquery name="entity" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from entity
</cfquery>

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Company Setup - Add Company</td>
		   <td class="feed_sub_header" align=right><a href="results.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Search for Company</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header"align=center width=200>Select or Add New</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header"  style="color: ffffff;" align=center width=200>Finalize</td>
	   </tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td height=10></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

            Great.  Let's create your Company record.  Your Company record is what Exchange users will see
            where searching for Companies and capabilities on the Exchange.  Because you are creating a new
            Company you will be owner of the Company record and can make changes and updates as needed.

        </td></tr>

      <tr><td height=10></td></tr>
      <tr><td>

      <form action="db.cfm" enctype="multipart/form-data" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

			<cfoutput>
			<tr><td class="feed_sub_header" width=20%>Company Name</td>
				<td><input class="input_text" style="width: 320px;" type="text" name="company_name" maxlength="100" required></td></tr>
			<tr><td class="feed_sub_header">Address (Line 1)</td>
				<td><input class="input_text" style="width: 320px;" type="text" name="company_address_l1"></td></tr>
			<tr><td class="feed_sub_header">Address (Line 2)</td>
				<td><input class="input_text" style="width: 320px;" maxlength="100" type="text" name="company_address_l2"></td></tr>
			<tr><td class="feed_sub_header">City/State/Zip</td>
				<td><input class="input_text" style="width: 200px;" type="text" name="company_city">&nbsp;&nbsp;
            </cfoutput>
				<select name="company_state" class="input_select">
				<option value=0>Select State
				<cfoutput query="state">
				 <option value="#state_abbr#">#state_name#
				</cfoutput>
				</select>
				<input class="input_text" style="width: 150px;" type="text" name="company_zip" maxlength="25">

				</td></tr>

			<cfoutput>
			<tr><td class="feed_sub_header">Website</td>
			    <td><input name="company_website" maxlength="200"class="input_text" style="width: 320px;" type="url" vspace=5></td></tr>
			<tr><td class="feed_sub_header">Domain</td>
			    <td><input name="company_domain" maxlength="200"class="input_text" style="width: 320px;" type="text" placeholder="company.com" vspace=5></td></tr>
			<tr><td class="feed_sub_header">POC First Name</td><td><input class="input_text" style="width: 200px;" type="text" name="company_poc_first_name" maxlength="100" required></td></tr>
			<tr><td class="feed_sub_header">Last Name</td><td><input class="input_text" style="width: 200px;" type="text" name="company_poc_last_name" maxlength="100" required></td></tr>
			<tr><td class="feed_sub_header">Title</td><td><input class="input_text" style="width: 200px;" type="text" name="company_poc_title" maxlength="100"></td></tr>
			<tr><td class="feed_sub_header">Email</td><td><input class="input_text" style="width: 200px;" type="email" name="company_poc_email" maxlength="100" required></td></tr>
			<tr><td class="feed_sub_header">Phone</td><td><input class="input_text" style="width: 200px;" type="text" name="company_poc_phone" maxlength="100"></td></tr>
			<tr><td class="feed_sub_header">Company logo</td><td class="feed_sub_header" style="font-weight: normal;">

			<input type="file" id="image" onchange="validate_img()" name="company_logo"></td></tr>
            <tr><td class="feed_sub_header">Year Founded</td><td><input class="input_text" style="width: 100px;" type="text" maxlength="4" name="company_founded"></td></tr>
            <tr><td class="feed_sub_header">DUNS Number</td><td><input class="input_text" style="width: 100px;" maxlength="12" type="text" name="company_duns"></td></tr>
            <tr><td class="feed_sub_header">Employees</td><td><input class="input_text" style="width: 100px;" maxlength="12" type="number" name="company_employees"></td></tr>
 		    </cfoutput>
 		    <tr><td class="feed_sub_header">Company Size</td>
 		        <td><select name="company_size_id" class="input_select">

					<option value=0>Select
					<cfoutput query="size">
					 <option value=#size_id#>#size_name#
					</cfoutput>

				</select>
				</td></tr>

 		    <tr><td class="feed_sub_header">Business Entity</td>
			    <td><select name="company_entity_id" class="input_select">

					<option value=0>Select
					<cfoutput query="entity">
					 <option value=#entity_id#>#entity_name#
					</cfoutput>

				</select>
				</td></tr>

            <cfoutput>

            <tr><td class="feed_sub_header">Revenue (2018)</td><td><input class="input_text" style="width: 200px;" type="number" name="company_fy18_revenue"></td></tr>
            <tr><td class="feed_sub_header">Revenue (2017)</td><td><input class="input_text" style="width: 200px;" type="number" name="company_fy17_revenue"></td></tr>
            <tr><td class="feed_sub_header">Revenue (2016) </td><td><input class="input_text" style="width: 200px;" type="number" name="company_fy16_revenue"></td></tr>

            </cfoutput>

            <tr><td class="feed_sub_header" colspan=2>Company Tagline</td></tr>
			<tr><td colspan=2><input type="text" name="company_tagline" class="input_text" style="width: 1000px;"></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Company Description</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 150px;" name="company_about"></textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Long Description</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 150px;" name="company_long_desc"></textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Summary of Products & Services</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 100px;" name="company_product_summary"></textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Company History</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 100px;" name="company_history"></textarea></td></tr>
            <tr><td class="feed_sub_header" colspan=2>Leadership Team</td></tr>
			<tr><td colspan=2><textarea class="input_textarea" style="width: 1000px; height: 100px;" name="company_leadership"></textarea></td></tr>
			<tr><td class="feed_sub_header" colspan=2>Company Keywords</td></tr>
			<tr><td colspan=2><input name="company_keywords" maxlength="300" class="input_text" style="width: 800px;" type="text"></td></tr>
            <tr><td height=10></td></tr>
            </table>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_1" required></td>
                <td class="feed_sub_header" style="font-weight: normal;">I certify that <cfoutput><b>#certify.usr_full_name#</b></cfoutput> is an authorized representative of this Company and the above information is accurate to the best of my knowledge.</td></tr>

            <tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_2" required></td>
                <td class="feed_sub_header" style="font-weight: normal;">I understand that I may be liable for any false or inflammatory information entered and therefore hold Ratio Exchange, LLC. harmless.</td></tr>

            <tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_3" required></td>
                <td class="feed_sub_header" style="font-weight: normal;">I understand that by adding this Company the above information will be discoverable on <u>any Exchange</u>.</td></tr>

            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>
			<tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Add Company" onclick="return confirm('Add Company?\r\nAre you sure you want to create this Company and add it to the Exchange?');"></td></tr>

            </form>

          </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

