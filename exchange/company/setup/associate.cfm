<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.check") and session.check is 1>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from company
  where company_id = #i#
</cfquery>

<cfquery name="usr_comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr_comp
  where usr_comp_company_id = #i# and
        usr_comp_usr_id = #session.usr_id#
</cfquery>

<cfquery name="certify" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Company Setup</td>
		   <td class="feed_sub_header" align=right><a href="results.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Search for Company</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header"align=center width=200>Select or Add New</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header"  style="color: ffffff;" align=center width=200>Finalize</td>
	   </tr>
	  </table>

      <cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td height=20></td></tr>

		<tr>
			<td width=60></td>
			<td class="feed_sub_header">Company Selected</td>
			<td class="feed_sub_header" align=center>Registered</td>
			<td class="feed_sub_header">DUNS</td>
			<td class="feed_sub_header">Website</td>
			<td class="feed_sub_header">City</td>
			<td class="feed_sub_header" align=center>State</td>
			<td class="feed_sub_header" align=right>Zipcode</td>
		</tr>

        <tr><td height=10></td></tr>

				<tr>

					<td class="feed_option" width=60>

                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40>
					</cfif>

					</td>
					<td class="feed_sub_header" width=300><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal" width=125 align=center><cfif companies.company_registered is "">No<cfelse>Yes</cfif></td>
					<td class="feed_sub_header" style="font-weight: normal" width=100><cfif companies.company_duns is "">Unknown<cfelse>#ucase(companies.company_duns)#</cfif></a></td>
					<td class="feed_sub_header" style="font-weight: normal"><a href="#companies.company_website#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><u><cfif len(companies.company_website) GT 40>#ucase(left(companies.company_website,40))#...<cfelse>#ucase(companies.company_website)#</cfif></u></a></td>
					<td class="feed_sub_header" style="font-weight: normal" width=100>#ucase(companies.company_city)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal" align=center>#ucase(companies.company_state)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal" align=right width=100>#companies.company_zip#</a></td>
				</tr>

        </table>

	  </cfoutput>

      <form action="db.cfm" enctype="multipart/form-data" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td height=20></td></tr>

		<cfif companies.company_admin_id is "">

			<tr><td class="feed_header" colspan=2>Company Not Registered</td></tr>
			<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>This Company has not been registered on the Exchange and does not have a Company Manager assigned.  Company Managers are authorized to update the Company's Profile in the Exchange and require Exchange approval prior to being assigned.
			If you would like to be assigned as a Company Manager please contact <b>support@ratio.exchange</b>.</td></tr>

			<tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_1" required></td>
				<td class="feed_sub_header" style="font-weight: normal;">I would like to associate myself with this Company.</td></tr>

			<tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_1" required></td>
				<td class="feed_sub_header" style="font-weight: normal;">I certify that <cfoutput><b>#certify.usr_full_name# (#tostring(tobinary(certify.usr_email))#)</b></cfoutput> is an authorized representative of this Company.</td></tr>

        <cfelse>

		<cfquery name="registered" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select * from usr
		  where usr_id = #companies.company_admin_id#
		</cfquery>

			<tr><td class="feed_header" colspan=2>Company Registered</td></tr>
			<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>This Company is already registered with the Exchange.  <b><cfoutput>#registered.usr_first_name# #registered.usr_last_name#</cfoutput></b> has been authorized as the Company Manager and is responsible for managing and maintaining the Company's profile.   If this person is no longer with this Company, please contact <b>support@ratio.exchange</b>.</td></tr>

			<tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_1" required></td>
				<td class="feed_sub_header" style="font-weight: normal;">I would like to associate myself with this Company.</td></tr>

			<tr><td class="feed_sub_header" width=3%><input type="checkbox" style="width: 20px; height: 20px;" name="legal_1" required></td>
				<td class="feed_sub_header" style="font-weight: normal;">I certify that <cfoutput><b>#certify.usr_full_name# (#tostring(tobinary(certify.usr_email))#)</b></cfoutput> is an authorized representative of this Company.</td></tr>

		</cfif>

		<tr><td height=10></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>
		<tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Associate" onclick="return confirm('Associate with Company?\r\nPlease confirm that you are an authorized representative of this Company.');"></td></tr>

        <cfoutput>
          <input type="hidden" name="i" value=#i#>
        </cfoutput>

        </form>

          </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

