<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <cfoutput>
		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Company Setup</td>
			   <td class="feed_sub_header" align=right></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>
		  </table>
	  </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

      <form action="set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="green" class="feed_sub_header" style="color: ffffff;" align=center width=200>Search for Company</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Select or Add New</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Send Invite</td>
	   </tr>
	  </table>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

            The first step in associating or setting up a new Company is to check whether it already exists in the Exchange Marketplace.  The
            Exchange aggregrates Company information from multiple sources and this step is required to ensure the integrity
            of our information.  Please enter your Company Name and/or Website below.

        </td></tr>

        <tr><td height=10></td></tr>

		<tr><td class="feed_sub_header" width=250>Company Name or Website</td>
			<td><input class="input_text" style="width: 320px;" type="text" name="search" maxlength="100" required></td></tr>

            <tr><td height=10></td></tr>
            <tr><td colspan=2><hr></td></tr>
            <tr><td height=10></td></tr>
			<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Next >>" vspace=10></td></tr>

            </form>

          </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

