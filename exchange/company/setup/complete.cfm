<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.check") and session.check is 1>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="hinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfquery name="cinfo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #i#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Company Setup</td>
		   <td class="feed_sub_header" align=right></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Search for Company</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header"align=center width=200>Select or Add New</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header"  style="color: ffffff;" align=center width=200>Finalize</td>
	   </tr>
	  </table>


	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td height=20></td></tr>

        <cfif u is 1>

        <tr><td class="feed_header">Congratulations!  You have successfully added your Company to the Exchange.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">To help you maximize the use of the Exchange, here are some tips and recommendations for what to do next.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>View and manage your <a href="/exchange/company/">Company Profile</a></li>
        </td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Tell users about the <a href="/exchange/company/products/">Product and Services</a> you offer.</li>
         </td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Add <a href="/exchange/company/customers/">Customer Snapshots</a>.</li>
        </td></tr>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Add <a href="/exchange/company/marketing/">Marketing Material</a>.</li>
        </td></tr>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Link Federal Information to your Company Profile by adding your DUNS number.</li>
        </td></tr>

        <cfelseif u is 2>

        <tr><td height=10></td></tr>
        <tr><td class="feed_header">Congratulations!  You are now associated with <cfoutput>#cinfo.company_name#</cfoutput>.</td></tr>
        <tr><td height=10></td></tr>

			<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select company_domain, company_admin_id, company_name from company
			  where company_id = #i#
			</cfquery>

        <cfif a is 1>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">To make updates to your Company profile you will need access from your Company Manager listed below.  If the Company Manager is no longer with this Company then please contact customer support below.</td></tr>

 			<cfquery name="uinfo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 			  select * from usr
 			  where usr_id = #comp.company_admin_id#
			</cfquery>

         <cfoutput>
       	 	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><b>#uinfo.usr_first_name# #uinfo.usr_last_name#</b></td></tr>
         	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#uinfo.usr_email#</td></tr>
         </cfoutput>

	    <cfelse>

	    <!--- Proces to be a Company Manager --->

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">This Company has not identified a Company Manager who is responsible for managing and maintaining the Company's profile in the Exchange.</td></tr>

        <tr><td height=10>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10>


        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">To register as the Company Manager for this Company you have two options.</td></tr>
        <tr><td height=10>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-bottom: 5px;"><b>Option 1.  Contact Customer Support</b></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">Please contact the Customer Support team below and we will validate your information and authorize you as the Company Manager.</td></tr>


        <tr><td height=10>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-bottom: 5px;"><b>Option 2.  Email Validation</b></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">Please enter your email name below with the registered Company domain we have on file and we will send you a validation code.</td></tr>

        <cfoutput>
        <form action="send_validation.cfm" method="post">
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><input type="text" name="email_name" required class="input_text" placeholder="email name">@#comp.company_domain#<input type="submit" name="button" class="button_blue" value="Send Validation Code" style="margin-left: 30px;"></td></tr>
        <input type="hidden" name="i" value=#i#>
        </form>
        </cfoutput>



        </cfif>

        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>

        <cfoutput>

        	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><b>#hinfo.hub_support_name#</b></td></tr>
        	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-bottom: 5px;">#hinfo.hub_support_phone#</td></tr>
        	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#hinfo.hub_support_email#</td></tr>

        </cfoutput>

        </cfif>

     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

