<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Send Validation Code">

    <cfset validation_code = #randrange(1000000,9999999)#>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update usr_comp
	  set usr_comp_validation_code = '#validation_code#',
	      usr_comp_validation_code_sent = #now()#,
	      usr_comp_validation_code_sent_to_email = '#email_name#'
	  where usr_comp_usr_id = #session.usr_id# and
	        usr_comp_company_id = #i#
	</cfquery>

	<cflocation URL="validate.cfm?i=#i#&code=#validation_code#" addtoken="no">

<cfelseif button is "Validate">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select usr_comp_id from usr_comp
	 where usr_comp_usr_id = #session.usr_id# and
	       usr_comp_company_id = #i# and
	       usr_comp_validation_code = '#validation_code#'
	</cfquery>

	<cfif check.recordcount is 0>
	 <cflocation URL="validate.cfm?i=#i#&u=1" addtoken="no">
	<cfelse>
		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update usr_comp
		  set usr_comp_validation_code_confirmed = #now()#,
		      usr_comp_access_rights = 1
		  where usr_comp_usr_id = #session.usr_id# and
				usr_comp_company_id = #i#
		</cfquery>

		<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  update company
		  set company_admin_id = #session.usr_id#
		  where company_id = #i#
		</cfquery>

		<cfset session.company_id = #i#>

		<cflocation URL="validated.cfm?i=#i#" addtoken="no">

	</cfif>

</cfif>