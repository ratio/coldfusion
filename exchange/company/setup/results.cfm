<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 10>
</cfif>

<cfset perpage = 100>

<cfset session.check = 0>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_name like '%#session.discover_keywords#%' or company_website like '%#session.discover_keywords#%'

	<cfif #sv# is 1>
	 order by company_name DESC
	<cfelseif #sv# is 10>
	 order by company_name ASC
	<cfelseif #sv# is 2>
	 order by company_city ASC
	<cfelseif #sv# is 20>
	 order by company_city DESC
	<cfelseif #sv# is 3>
	 order by company_state ASC
	<cfelseif #sv# is 30>
	 order by company_state DESC
	<cfelseif #sv# is 4>
	 order by company_zip ASC
	<cfelseif #sv# is 40>
	 order by company_zip DESC
	<cfelseif #sv# is 6>
	 order by company_duns ASC
	<cfelseif #sv# is 60>
	 order by company_duns DESC
	<cfelseif #sv# is 7>
	 order by total DESC
	<cfelseif #sv# is 70>
	 order by total ASC
	<cfelseif #sv# is 8>
	 order by total DESC
	<cfelseif #sv# is 80>
	 order by total ASC
	</cfif>

</cfquery>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt companies.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(companies.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Company Setup</td>
		   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Search for Company</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header" style="color: ffffff;" align=center width=200>Select or Add New</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Send Invite</td>
	   </tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>


        <cfoutput>
        <tr><td colspan=2 class="feed_sub_header">You searched for "<i>#session.discover_keywords#</i>".&nbsp;&nbsp;&nbsp;<a href="index.cfm"><u>Search again</u>.</a></td></tr>
        </cfoutput>

        <cfif companies.recordcount is 0>

        <tr><td>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Companies were found that match your search criteria.  Please continue and add a new Company.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>

        <form action="redirect.cfm" method="post">
		<tr><td><input class="button_blue_large" type="submit" name="button" value="Continue >>" vspace=10></td></tr>
		<input type="hidden" name="option" value=2>
        </form>

        <cfelse>

        <form action="redirect.cfm" method="post">

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

            Great!  One or more Companies were found.  If any of these
            Companies are your Company then please choose <b>Option 1</b>.   If none of these
            Companies are your Company please choose <b>Option 2</b> and create your own.   Tip - to find out more information about a Company click on the Company Name or Website.

        </td></tr>

        <tr><td height=10></td></tr>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif isdefined("u")>
         <tr><td class="feed_sub_header" style="color: red;">No Company selected.  Please select a Company to associate with below.</td></tr>
        </cfif>

        <cfoutput>

        <cfif companies.recordcount GT perpage>

        <tr>

				    <td colspan=2 class="feed_sub_header" align=right>

						<b>Page #thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt companies.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
						<cfelse>
						</cfif>
				</td>
	    </tr>

	    </cfif>

	    </cfoutput>

        <tr><td width=40><input type="radio" name="option" style="width: 22px; height: 22px;" value=1 checked></td>
            <td class="feed_sub_header"><b>Option 1. </b>   Associate yourself with a company that was found in the Exchange Marketplace.</td></tr>
        <tr><td height=10></td></tr>

        </table>

        </td></tr>

        <tr><td>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td width=50>&nbsp;</td>
			<td class="feed_sub_header">Select</td>
			<td width=60></td>
			<td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company Name</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header" align=center>Registered *</td>
			<td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 6><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 60><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header">Website</td>
			<td class="feed_sub_header"><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>City</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header" align=center><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
			<td class="feed_sub_header" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Zipcode</b></a>&nbsp;&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
		</tr>

        <tr><td height=10></td></tr>

                <cfset count = 1>

				<cfoutput query="companies" startrow="#url.start#" maxrows="#perpage#">

				<tr>

					<td width=50>&nbsp;</td>
				    <td>&nbsp;&nbsp;<input type="radio" name="comp_id" style="width: 22px; height: 22px;" value=#companies.company_id#></td>

					<td class="feed_option" width=60>

                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40>
					</cfif>

					</td>
					<td class="feed_sub_header" width=300><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#wrap(ucase(companies.company_name),30)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal" width=125 align=center><cfif companies.company_registered is "">No<cfelse>Yes</cfif></td>
					<td class="feed_sub_header" style="font-weight: normal" width=100><cfif companies.company_duns is "">Unknown<cfelse>#ucase(companies.company_duns)#</cfif></a></td>
					<td class="feed_sub_header" style="font-weight: normal"><a href="#companies.company_website#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><u><cfif len(companies.company_website) GT 40>#ucase(left(companies.company_website,30))#...<cfelse>#ucase(companies.company_website)#</cfif></u></a></td>
					<td class="feed_sub_header" style="font-weight: normal" width=100>#ucase(companies.company_city)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal" align=center>#ucase(companies.company_state)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal" align=right width=100>#companies.company_zip#</a></td>
				</tr>

				<cfif count LT companies.recordcount>
				   <tr><td></td><td colspan=9><hr></td></tr>
				</cfif>
				<cfset count = count + 1>

				</cfoutput>
        </table>

        </td></tr>

        </table>

        <tr><td>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
        <tr><td width=40><input type="radio" name="option" style="width: 22px; height: 22px;" value=2></td>
            <td class="feed_sub_header"><b>Option 2. </b>   I did not find my Company and would like to create a new Company.</td></tr>
        <tr><td height=10></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td colspan=2 class="link_small_gray">* - Registered indicates that an Exchange user has already associated themselves as the Manager of the Company record.</td></tr>
        <tr><td height=10></td></tr>

		<tr><td colspan=2><input class="button_blue_large" type="submit" name="button" value="Continue >>" vspace=10></td></tr>


        </table>

        </form>

        </cfif>

        </td></tr>

        </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

