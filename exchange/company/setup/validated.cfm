<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.check") and session.check is 1>
<cfelse>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="hinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfquery name="cinfo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_name from company
 where company_id = #i#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">Company Setup</td>
		   <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=50%>
	   <tr><td height=10></td></tr>
	   <tr>
		  <td bgcolor="e0e0e0" class="feed_sub_header" align=center width=200>Search for Company</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="e0e0e0" class="feed_sub_header"align=center width=200>Select or Add New</td>
		  <td width=30><img src="/images/spacer.png" height=15 width=40></td>
		  <td bgcolor="green" class="feed_sub_header"  style="color: ffffff;" align=center width=200>Finalize</td>
	   </tr>
	  </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td height=20></td></tr>
        <tr><td class="feed_header">Success!  The code has been validated.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">You are now assigned as the Company Manager for <cfoutput>#cinfo.company_name#</cfoutput>.  You are now authorized to make updates to the Company's profile.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Recommended next steps...</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>View and manage your <a href="/exchange/company/">Company Profile</a></li>
        </td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Tell users about the <a href="/exchange/company/products/">Product and Services</a> you offer.</li>
         </td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Add <a href="/exchange/company/customers/">Customer Snapshots</a>.</li>
        </td></tr>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Add <a href="/exchange/company/marketing/">Marketing Material</a>.</li>
        </td></tr>


        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">
         <li>Link Federal Information to your Company Profile by added a SAM.Gov approved DUNS Number.</li>
        </td></tr>












        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">If you have problems validating your code or did not receive the email please contact Customer Support below.</td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>

        <cfoutput>

        	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><b>#hinfo.hub_support_name#</b></td></tr>
        	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-bottom: 5px;">#hinfo.hub_support_phone#</td></tr>
        	<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#hinfo.hub_support_email#</td></tr>

        </cfoutput>

     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

