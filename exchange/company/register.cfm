<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfif not isdefined("session.selected_company") and session.selected_company is not 0>
 <cflocation URL="/exchange/company/setup.cfm" addtoken="no">
</cfif>

<cfquery name="states" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 where company_id = #session.selected_company#
</cfquery>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">REGISTER COMPANY</td>
			       <td align=right><a href="/exchange/company/setup.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
		      </table>

        <form action="/exchange/company/create_db.cfm" enctype="multipart/form-data" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
		    <tr><td width=40><input type="checkbox" name="verify" style="width: 22px; height: 22px;" required></td>
		        <td class="feed_sub_header"style="font-weight: normal;">I certify that I am legally authorized to update this company's information.  I also understand that my account information will be associated with this company.</td></tr>
  		    <tr><td colspan=2><hr></td></tr>
        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

 			<cfoutput>
 			<tr><td class="feed_sub_header">Company Name&nbsp;<b>*</b></td></tr>
			<tr><td><input class="input_text" type="text" name="company_name" style="width: 400px;" required placeholder="Enter Company Name" value="#info.company_name#"></td></tr>
 			<tr><td class="feed_sub_header">Location&nbsp;<b>*</b></td></tr>
			<tr><td><input class="input_text" type="text" name="company_city" style="width: 225px;" required placeholder="City" value="#info.company_city#">
			</cfoutput>

		    <select name="company_state" class="input_select">
		    <option value=0>Select State
		    <cfoutput query="states">
		     <option value="#state_abbr#" <cfif #state_abbr# is #info.company_state#>selected</cfif>>#state_name#
            </cfoutput>

			</td></tr>

			<cfoutput>
		    <tr><td class="feed_sub_header">Website</td></tr>
		    <tr><td class="feed_option"><input type="url" class="input_text" style="width: 400px;" name="company_website" placeholder="http://www..." value="#info.company_website#"></td></tr>
            <tr><td class="feed_sub_header">Company Description</td></tr>
			<tr><td><textarea name="company_about" class="input_textarea" style="width: 700px;" rows=3>#info.company_about#</textarea></td></tr>
            <tr><td class="feed_sub_header">Company Tags or Keywords</td></tr>
            <tr><td class="feed_option"><input type="text" class="input_text" style="width: 700px;" placeholder="i.e., machine learning, intelligence, cloud, etc." name="company_keywords" value="#info.company_keywords#"></td></tr>
            <tr><td class="feed_sub_header">DUNS Number</td></tr>
            <tr><td class="feed_option"><input type="text" class="input_text" style="width: 200px;" name="company_duns" value="#info.company_duns#"></td></tr>
			<tr><td height=15></td></tr>
			<tr><td><input class="button_blue_large" type="submit" name="button" value="Register Company"></td></tr>
			<tr><td height=10></td></tr>
			<tr><td class="text_xsmall">* - Required Field</td></tr>
            </cfoutput>
       </table>

        </form>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

