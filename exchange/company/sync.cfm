<cfinclude template="/exchange/security/check.cfm">

	<cftransaction>

		<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select company_duns from company
		 where company_id = #session.company_id#
		</cfquery>

		<cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from sams
		 where duns = '#company.company_duns#'
		</cfquery>

		<cfif #sams.recordcount# is 0>

		 <cflocation URL="/exchange/company/index.cfm?u=3" addtoken="no">

		</cfif>

		<cfquery name="update_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update company
		 set company_address_l1 = '#sams.phys_address_l1#',
		     company_address_l2 = '#sams.phys_address_line_2#',
		     company_city = '#sams.city_1#',
		     company_state = '#sams.state_1#',
		     company_zip = '#sams.zip_1#',
  		     company_founded = #left(sams.biz_start_date,4)#,
		     company_cage_code = '#sams.cage_code#',
		     company_updated = #now()#,
		     company_sam_pull_updated = #now()#
         where company_id = #session.company_id#
        </cfquery>

		<cfquery name="delete_naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
         delete naics_xref
         where naics_xref_company_id = #session.company_id#
        </cfquery>

        <cfloop index="element" list="#listsort(sams.naisc_code_string,'Text','ASC','~')#" delimiters="~">

		<cfquery name="insert_naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into naics_xref
		 (
		  naics_xref_company_id,
		  naics_xref_code,
		  naics_xref_code_pri
		 )
		 values
		 (
		  #session.company_id#,
		 '#left(element,'6')#',
		 <cfif #left(element,'6')# is #sams.pri_naics#>1<cfelse>0</cfif>
		 )
		</cfquery>

	   </cfloop>

		<cfquery name="delete_bizcode" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
         delete biz_code_xref
         where biz_code_xref_company_id = #session.company_id#
        </cfquery>

       <cfloop index="biz_element" list="#listsort(sams.biz_type_string,'Text','ASC','~')#" delimiters="~">

		<cfquery name="insert_biz_class" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into biz_code_xref
		 (
		  biz_code_xref_code,
		  biz_code_xref_company_id
		 )
		 values
		 ('#biz_element#',
		   #session.company_id#
         )
		</cfquery>

	   </cfloop>

	</cftransaction>

<cflocation URL="/exchange/company/index.cfm?u=2" addtoken="no">