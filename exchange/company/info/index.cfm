<cfinclude template="/exchange/security/check.cfm">

<cfquery name="section_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from section
 where section_id = #section_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

               <cfoutput>
		       <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header" style="font-size: 30;" valign=middle>#section_info.section_name#</td>
					   <td align=right class="feed_sub_header" valign=middle>
						<a href="/exchange/company/media/add.cfm?l=#l#" valign=middle>
						<cfif company_admin is 1>
                         <a href="/exchange/company/info/edit.cfm?section_id=#section_id#&l=#l#"><img src="/images/icon_edit.png" width=20 border=0 alt="Update" title="Update" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/info/edit.cfm?section_id=#section_id#&l=#l#">Update</a>
						</cfif>
					   </td></tr>
				   <tr><td colspan=2><hr></td></tr>
				   </table>
			   </cfoutput>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Information has been successfully updated.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfquery name="fields" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from section_field
                     where section_field_section_id = #section_id#
					 order by section_field_order
				</cfquery>

			     <cfloop query="fields">

				 <cfquery name="getval" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from section_field_value
					 where section_field_value_section_id = #section_id# and
					       section_field_value_field_id = #fields.section_field_id# and
					       section_field_value_company_id = #session.company_id#
			     </cfquery>

			     <cfoutput>
			      <tr><td class="feed_sub_header">#fields.section_field_name#</td></tr>

                  <!--- Text Field --->
                  <cfif fields.section_field_type is 1>
                    <cfif #getval.section_field_value_text# is not "">
                      <tr><td class="feed_sub_header" style="font-weight: normal;">#getval.section_field_value_text#</td></tr>
                    <cfelse>
                      <tr><td class="feed_sub_header" style="font-weight: normal;">No information has been entered.</td></tr>
                    </cfif>
                  <!-- Memo Field --->
                  <cfelseif fields.section_field_type is 2>
                    <cfif #getval.section_field_value_textarea# is not "">
                    <tr><td class="feed_sub_header" style="font-weight: normal;">#replace(getval.section_field_value_textarea,"#chr(10)#","<br>","all")#</td></tr>
                    <cfelse>
                      <tr><td class="feed_sub_header" style="font-weight: normal;">No information has been entered.</td></tr>
                    </cfif>
                  </cfif>

			     </cfoutput>

			     <tr><td height=5></td></tr>
                 <tr><td><hr></td></tr>
			     <tr><td height=5></td></tr>

			     </cfloop>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>