<cfinclude template="/exchange/security/check.cfm">

<cfquery name="section_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from section
 where section_id = #section_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

               <cfoutput>
		       <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header" style="font-size: 30;" valign=middle>#section_info.section_name#</td>
					   <td align=right class="feed_sub_header" valign=middle>
						<a href="/exchange/company/media/add.cfm?l=#l#" valign=middle>
						<cfif company_admin is 1>
                         <a href="/exchange/company/info/index.cfm?section_id=#section_id#&l=#l#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close" valign=middle></a>
						</cfif>
					   </td></tr>
				   <tr><td colspan=2><hr></td></tr>
				   </table>
			   </cfoutput>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

	    <form action="db.cfm" method="post">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfquery name="fields" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from section_field
                     where section_field_section_id = #section_id#
					 order by section_field_order
				</cfquery>

			     <cfloop query="fields">

				 <cfquery name="getval" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from section_field_value
					 where section_field_value_section_id = #section_id# and
					       section_field_value_field_id = #fields.section_field_id# and
					       section_field_value_company_id = #session.company_id#
			     </cfquery>

			     <cfoutput>
			      <tr><td class="feed_sub_header">#fields.section_field_name#</td></tr>

                  <!--- Text Field --->
                  <cfif fields.section_field_type is 1>
                    <tr><td class="feed_option"><input type="text" class="input_text" size=60 name="#fields.section_field_unique_id#" value="#getval.section_field_value_text#"></td></tr>

                  <!-- Memo Field --->
                  <cfelseif fields.section_field_type is 2>
                    <tr><td class="feed_option"><textarea name="#fields.section_field_unique_id#" class="input_textarea" cols=100 rows=8>#getval.section_field_value_textarea#</textarea></td></tr>

                  </cfif>

			     </cfoutput>

			     <tr><td height=10></td></tr>

			     </cfloop>

			     <cfoutput>
                 	<input type="hidden" name="section_id" value=#section_id#>
                 	<input type="hidden" name="l" value=#l#>
                 </cfoutput>

		         <tr><td><input class="button_blue_large" type="submit" name="button" value="Save" vspace=10></td></tr>

        </table>

        </form>

          </td></tr>

          </table>

         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>