<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

<cftransaction>

	 <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete section_field_value
	  where section_field_value_section_id = #section_id# and
	        section_field_value_company_id = #session.company_id#
	 </cfquery>

	 <cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from section_field
	  where section_field_section_id = #section_id#
	 </cfquery>

     <cfoutput query="info">

      <cfset field_name = "form." & "#section_field_unique_id#">

      <cfif info.section_field_type is 1>

		  <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   insert into section_field_value
		   (section_field_value_company_id, section_field_value_section_id, section_field_value_field_id, section_field_value_field_updated, section_field_value_updated_usr_id, section_field_value_hub_id, section_field_value_text)
		   values
		   (#session.company_id#, #section_id#, #info.section_field_id#, #now()#, #session.usr_id#, #session.hub#, '#evaluate(field_name)#')
		  </cfquery>

	  <cfelseif info.section_field_type is 2>

		  <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   insert into section_field_value
		   (section_field_value_company_id, section_field_value_section_id, section_field_value_field_id, section_field_value_field_updated, section_field_value_updated_usr_id, section_field_value_hub_id, section_field_value_textarea)
		   values
		   (#session.company_id#, #section_id#, #info.section_field_id#, #now()#, #session.usr_id#, #session.hub#,'#evaluate(field_name)#')
		  </cfquery>

	  </cfif>

      Field - #section_field_unique_id#, #evaluate(field_name)#<br>
     </cfoutput>

 </cftransaction>

</cfif>

<cflocation URL="/exchange/company/info/index.cfm?section_id=#section_id#&l=#l#&u=1" addtoken="no">