<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<cfif not isdefined("company_keyword")>
 <cflocation URL="/exchange/company/setup.cfm" addtoken="no">
</cfif>

<cfquery name="search" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_name like '#company_keyword#%'
 order by company_name
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

<div class="main_box">

   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header" style="font-size: 30;" valign=middle>SEARCH RESULTS</td>
	       <td align=right><a href="/exchange/company/setup.cfm"><img src="/images/delete.png" width=20 alt="Close" title="Close" border=0></a></td></tr>
	   <tr><td colspan=2><hr></td></tr>
   </table>

<cfif search.recordcount is 0>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No results were found.</td></tr>

       <tr><td height=10></td></tr>
       <form action="/exchange/company/process.cfm" method="post">
        <tr><td><input type="submit" class="button_blue_large" name="button" value="Create Company"></td></tr>
        <input type="hidden" name="selected_company" value=0>
       </form>
       <tr><td height=10></td></tr>

   </table>

<cfelse>

   <cfoutput>
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">We found the following companies matching "<i><b>#company_keyword#</b></i>".  If one of these companies is your company, please select it.  If not, please select Not Listed at the bottom of the page.</td></tr>
	   </table>
   </cfoutput>

   <form action="/exchange/company/process.cfm" method="post">

   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	<tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
		<td class="feed_sub_header"><b>Company Name</b></td>
		<td class="feed_sub_header"><b>DUNS</b></td>
		<td class="feed_sub_header"><b>Keywords</b></td>
		<td class="feed_sub_header"><b>Website</b></td>
		<td class="feed_sub_header"><b>City</b></td>
		<td class="feed_sub_header" align=center><b>State</b></td>
	</tr>

	<cfset count = 1>

	<cfloop query="search">
		<cfoutput>
			<tr>
				<td align=center width=50>

				<input type="radio" name="selected_company" value=#search.company_id# style="width: 22px; height: 25px;"></td>

				</td>
				<td valign=middle width=80>

                    <cfif search.company_logo is "">
					  <img src="//logo.clearbit.com/#search.company_website#" width=60 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#search.company_logo#" width=60>
					</cfif>

				</td>
				<td valign=middle class="feed_sub_header" style="font-weight: normal;" width=300><b>#ucase(search.company_name)#</b></td>
				<td valign=middle class="feed_sub_header" style="font-weight: normal;" width=150>
				 <cfif search.company_duns is "">
				  Not Found
				 <cfelse>
				  #search.company_duns#
				 </cfif></td>

					<td class="feed_sub_header" style="font-weight: normal;">
					<cfif search.company_keywords is "">
					 Not Found
					<cfelse>
					#ucase(wrap(search.company_keywords,40,0))#
					</cfif>

					</td>

					<td class="feed_sub_header" style="font-weight: normal;" width=300><u>
					<a href="#search.company_website#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">
					<cfif len(search.company_website) GT 40>
					#ucase(left(search.company_website,40))#...
					<cfelse>
					#ucase(search.company_website)#
					</cfif>
					</a></u>
					</td>

					<td class="feed_sub_header" style="font-weight: normal;" width=100>#ucase(search.company_city)#</a></td>
					<td class="feed_sub_header" style="font-weight: normal;" align=center width=50>#ucase(search.company_state)#</a></td>

			</tr>
		</cfoutput>

  	    <tr><td colspan=9><hr></td></tr>

	</cfloop>

			<tr>
				<td align=center width=50><input type="radio" name="selected_company" value=0 style="width: 22px; height: 25px;" required></td>
				<td colspan=2 class="feed_sub_header">NOT LISTED, CREATE NEW COMPANY</td>
		    </tr>

		    <tr><td height=10></td></tr>
		    <tr><td colspan=3><input type="submit" class="button_blue_large" value="Select"></td></tr>

    </table>

    </form>

   </cfif>

 </div>

</td></tr>

</table>

</div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>