<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update Company Profile">

    <cftransaction>

        <cfif isdefined("remove_logo")>

			<cfquery name="remove" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select company_logo from company
			  where company_id = #session.company_id#
			</cfquery>

			<cfif FileExists("#media_path#\#remove.company_logo#")>
        	  <cffile action = "delete" file = "#media_path#\#remove.company_logo#">
        	</cfif>

        </cfif>

		<cfif #company_logo# is not "">

			<cfquery name="getfile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select company_logo from company
			  where company_id = #session.company_id#
			</cfquery>

			<cfif #getfile.company_logo# is not "">
			 <cfif FileExists("#media_path#\#getfile.company_logo#")>
			  <cffile action = "delete" file = "#media_path#\#getfile.company_logo#">
			 </cfif>
			</cfif>

			<cffile action = "upload"
			 fileField = "company_logo"
			 destination = "#media_path#"
			 nameConflict = "MakeUnique">

		</cfif>

		<cfquery name="update_company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  update company
		  set company_name = '#company_name#',
		      company_about = '#company_about#',
			  company_long_desc = '#company_long_desc#',
			  company_address_l1 = '#company_address_l1#',
			  company_address_l2 = '#company_address_l2#',
			  company_entity_id = #company_entity_id#,
			  company_stock_symbol = '#company_stock_symbol#',
			  company_size_id = #company_size_id#,
			  company_history = '#company_history#',
			  company_founded = <cfif #company_founded# is "">null<cfelse>'#company_founded#'</cfif>,
			  company_duns = '#company_duns#',
			  company_city = '#company_city#',
			  company_poc_first_name = '#company_poc_first_name#',
			  company_poc_last_name = '#company_poc_last_name#',
			  company_poc_title = '#company_poc_title#',
			  company_tagline = '#company_tagline#',
			  company_product_summary = '#company_product_summary#',
			  company_poc_email = '#company_poc_email#',
			  company_poc_phone = '#company_poc_phone#',
			  company_discoverable = #company_discoverable#,
			  company_fy18_revenue = <cfif #company_fy18_revenue# is "">null<cfelse>#company_fy18_revenue#</cfif>,
			  company_fy17_revenue = <cfif #company_fy17_revenue# is "">null<cfelse>#company_fy17_revenue#</cfif>,
			  company_fy16_revenue = <cfif #company_fy16_revenue# is "">null<cfelse>#company_fy16_revenue#</cfif>,
 			  <cfif #company_logo# is not "">
			   company_logo = '#cffile.serverfile#',
			  </cfif>
			  <cfif isdefined("remove_logo")>
			   company_logo = null,
			  </cfif>
			  company_zip = '#company_zip#',
			  company_state = '#company_state#',
			  company_updated = #now()#,
			  company_leadership = '#company_leadership#',
			  company_website = '#company_website#',
			  company_twitter_url = '#company_twitter_url#',
			  company_facebook_url = '#company_facebook_url#',
			  company_linkedin_url = '#company_linkedin_url#',
			  company_keywords = '#company_keywords#'
		  where company_id = #session.company_id#
		</cfquery>

	</cftransaction>

</cfif>

<cflocation URL="/exchange/company/index.cfm?&u=6" addtoken="no">