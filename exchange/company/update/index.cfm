<cfinclude template="/exchange/security/check.cfm">

<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_id#
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="size" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from size
</cfquery>

<cfquery name="entity" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from entity
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">


           <div class="main_box">

           <cfoutput>

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Update Company Profile</td></tr>
			   <tr><td colspan=2><hr></td></tr>

               <tr><td>

	             <form action="update_db.cfm" enctype="multipart/form-data" method="post">

	             <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150>Company Name</td>
                      <td><input type="text" name="company_name" class="input_text" size=100 value="#company.company_name#" placeholder="Legal Company Name" maxlength="299"></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150>Tagline</td>
                      <td><input type="text" name="company_tagline" class="input_text" size=100 value="#company.company_tagline#" placeholder="Please describe your companies value proposition to the market." maxlength="999"></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>About</td>
                      <td><textarea name="company_about" class="input_textarea" cols=101 rows=3 placeholder="Please provide a brief description about what your company does.">#company.company_about#</textarea></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Full Description</td>
                      <td><textarea name="company_long_desc" class="input_textarea" cols=101 rows=8 placeholder="Please provide a full description of what your company does.">#company.company_long_desc#</textarea></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150 valign=top>Keywords</td>
                      <td valign=top><input type="text" name="company_keywords" class="input_text" size=100 value="#company.company_keywords#" placeholder="Keywords that describe your products or services." maxlength="299">
                      <br><span class="link_small_gray">Please seperate keywords by a comma (i.e., Machine Learning, AI, Clinical, etc.)</span></td></tr>

                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Product or Services<br>Summary</td>
                      <td><textarea name="company_product_summary" class="input_textarea" cols=101 rows=5 placeholder="Please provide a brief description of your products and/or services.">#company.company_product_summary#</textarea></td></tr>

                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

 			    <tr><td class="feed_sub_header" valign=top width=175><b>Company Logo</b></td>
                 <cfif #company.company_logo# is "">
 			      <td><input type="file" id="image" onchange="validate_img()" name="company_logo"></td></tr>
 			    <cfelse>
 			      <td><img src="#media_virtual#/#company.company_logo#" width=200></td></tr>
 			      <tr><td height=5></td></tr>
 			      <tr><td></td><td class="feed_sub_header">Change Logo</td></tr>
 			      <tr><td height=5></td></tr>
 			      <tr><td></td><td class="feed_sub_header" style="font-weight: normal;">

 			      <input type="file" id="image" onchange="validate_img()" name="company_logo">

 			      </td></tr>
 			      <tr><td height=5></td></tr>
 			      <tr><td></td><td class="feed_sub_header" style="font-weight: normal;" valign=middle><input type="checkbox" name="remove_logo" style="width: 20px; height: 20px;">&nbsp;&nbsp;&nbsp;or, check to remove logo</td></tr>
                 </cfif>


                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

                   <tr>
                      <td class="feed_sub_header" valign=top>Point of Contact</td>
                      <td>

                      <input type="text" name="company_poc_first_name" class="input_text" size=30 value="#company.company_poc_first_name#" maxlength="49" placeholder="First Name">
                      <input type="text" name="company_poc_last_name" class="input_text" size=30 value="#company.company_poc_last_name#" maxlength="49" placeholder="Last Name"><br>
                      <input type="text" name="company_poc_title" class="input_text" size=30 value="#company.company_poc_title#" maxlength="49" placeholder="Title"><br>
                      <input type="text" name="company_poc_phone" class="input_text" size=30 value="#company.company_poc_phone#" maxlength="49" placeholder="Phone"><br>
                      <input type="email" name="company_poc_email" class="input_text" size=30 value="#company.company_poc_email#" maxlength="49" placeholder="Email Address">

                       </td>
                   </tr>

                   <tr>
                      <td class="feed_sub_header" valign=top>Address</td>
                      <td>

                      <input type="text" name="company_address_l1" class="input_text" size=60 value="#company.company_address_l1#" maxlength="249" placeholder="Address (Line 1)"><br>
                      <input type="text" name="company_address_l2" class="input_text" size=60 value="#company.company_address_l2#" maxlength="249" placeholder="Address (Line 2)"><br>
                      <input type="text" name="company_city" class="input_text" size=30 value="#company.company_city#" maxlength="99" placeholder="City">

                 </cfoutput>
                      <select name="company_state" class="input_select">
                      <option value=0>Select State
                      <cfoutput query="states">
                       <option value="#state_abbr#" <cfif #state_abbr# is #company.company_state#>selected</cfif>>#state_name#
                      </cfoutput>

                <cfoutput>

                      <input type="text" name="company_zip" class="input_text" size=20 value="#company.company_zip#" maxlength="19" placeholder="Zipcode"><br>

                       </td>
                   </tr>

                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>URLs</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <img src="/images/icon_home.png" width=20 vspace=5 style="margin-right: 20px;"><input type="text" name="company_website" class="input_text" size=80 maxlength="1200" value="#company.company_website#" placeholder="Company Website"><br>
						  <img src="/images/icon_linkedin.png" width=20 vspace=5 style="margin-right: 20px;"><input type="text" name="company_linkedin_url" class="input_text" size=80 maxlength="1200" value="#company.company_linkedin_url#" placeholder="Company LinkedIn URL"><br>
						  <img src="/images/icon_twitter.png" width=20 vspace=5 style="margin-right: 20px;"><input type="text" name="company_twitter_url" class="input_text" size=80 maxlength="1200" value="#company.company_twitter_url#" placeholder="Company Twitter URL"><br>
						  <img src="/images/icon_facebook.png" width=20 vspace=5 style="margin-right: 20px;"><input type="text" name="company_facebook_url" class="input_text" size=80 maxlength="1200" value="#company.company_facebook_url#" placeholder="Company Facebook URL">
                       </td></tr>

                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Leadership Team</td>
                      <td><textarea name="company_leadership" class="input_textarea" placeholder="Please provide a brief description of your leadership team is." cols=101 rows=8>#company.company_leadership#</textarea></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Company History</td>
                      <td><textarea name="company_history" class="input_textarea" cols=101 rows=8 placeholder="Please provide a brief description of your Company's history">#company.company_history#</textarea></td></tr>


                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150>Founded</td>
                      <td><input type="text" name="company_founded" class="input_text" size=10 value="#company.company_founded#" maxlength="4"></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150>DUNS</td>
                      <td class="feed_option"><input type="text" name="company_duns" class="input_text" size=20 value="#company.company_duns#" maxlength="10"><span class="link_small_gray">do not include leading zero's when entering your DUNS number</span></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Stock Symbol</td>
                      <td><input type="text" name="company_stock_symbol" class="input_text" size=8 maxlength=8 value="#company.company_stock_symbol#"></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>2018 Revenue</td>
                      <td><input type="number" name="company_fy18_revenue" class="input_text" size=20 value="#company.company_fy18_revenue#"></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>2017 Revenue</td>
                      <td><input type="number" name="company_fy17_revenue" class="input_text" size=20 value="#company.company_fy17_revenue#"></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>2016 Revenue</td>
                      <td><input type="number" name="company_fy16_revenue" class="input_text" size=20 value="#company.company_fy16_revenue#"></td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>


              </cfoutput>

 		         <tr><td class="feed_sub_header"><b>Type</b></td>
			         <td><select name="company_entity_id" class="input_select">

 					 <option value=0>Select
					 <cfoutput query="entity">
					  <option value=#entity_id# <cfif #company.company_entity_id# is #entity_id#>selected</cfif>>#entity_name#
					 </cfoutput>

				     </select>
				     </td></tr>

 		         <tr><td class="feed_sub_header"><b>Size</b></td>
 		             <td><select name="company_size_id" class="input_select">

					 <option value=0>Select
					 <cfoutput query="size">
					  <option value=#size_id# <cfif #company.company_size_id# is #size_id#>selected</cfif>>#size_name#
					 </cfoutput>

				     </select>

				     </td></tr>


				<tr><td class="feed_sub_header">Discoverable?</td>
				    <td class="feed_option">

				<select name="company_discoverable" class="input_select">
				 <option value=1 <cfif #company.company_discoverable# is 1 or #company.company_discoverable# is "">selected</cfif>>Yes
				 <option value=0 <cfif #company.company_discoverable# is 0>selected</cfif>>No
				<select>

				<span class="link_small_gray">Indicates whether your company is discoverable on the Exchange or not.</span>

				</td></tr>

                  <tr><td height=10></td></tr>
                  <tr><td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>

		          <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update Company Profile" vspace=10></td></tr>

		          </form>

           </table>

           </td></tr>

         </table>

         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>