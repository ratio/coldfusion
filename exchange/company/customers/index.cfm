<cfinclude template="/exchange/security/check.cfm">

<cfquery name="customers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from customer
 where customer_company_id = #session.company_id#
 order by customer_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Customer Snapshots<cfif #customers.recordcount# GT 0> (<cfoutput>#customers.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header" valign=middle>
			       <cfif session.update_access is 1>
                    <a href="/exchange/company/customers/add.cfm?l=4"><img src="/images/plus3.png" width=15 border=0 alt="Add Customer Snapshot" title="Add Customer Snapshot" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/customers/add.cfm?l=4">Add Customer Snapshot</a>
                   </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Customer snapshot has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Customer snapshot has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Customer snapshot has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif customers.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">No Customer Snapshots have been entered.</td></tr>

               <cfelse>

               <cfset count = 1>

			   <cfoutput query="customers">

               <tr>
                   <td valign=top class="feed_sub_header" width=165>

                   <cfif #customers.customer_logo# is "">
                   <img src="/images/no_logo.png" width=150 vspace=5 border=0>
                   <cfelse>
                   <img src="#media_virtual#/#customer_logo#" width=150 vspace=5 border=0>
                   </cfif>
                   <br><br>
                   #ucase(customer_type)#
                   <br><br>

                   <cfif customer_topic_id is not "">

						<cfquery name="capability" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						 select topic_name from topic
						 where topic_id in (#customer_topic_id#)
						</cfquery>

						<cfset #cap_list# = #valuelist(capability.topic_name)#>

						<span class="feed_option"><cfoutput>#replace(cap_list,",","<br>","all")#</cfoutput></span>

                   </cfif>


                   </td>

				   <td width=20>&nbsp;</td><td valign=top>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=middle valign=top>#ucase(customer_name)#</td>
				       <td align=right class="feed_sub_header" valign=middle>
				       <cfif session.update_access is 1>
				       <a href="/exchange/company/customers/edit.cfm?l=4&customer_id=#customer_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Customer Snapshot" title="Edit Customer Snapshot" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/customers/edit.cfm?l=4&customer_id=#customer_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <tr><td colspan=2><hr></td></tr>
				   <tr><td class="feed_sub_header" colspan=2 valign=top>Overview of Engagement</td></tr>
				   <tr><td class="feed_option" colspan=2 style="font-weight: normal;" valign=top>#replace(customer_work,"#chr(10)#","<br>","all")#</td></tr>
				   <tr><td class="feed_sub_header" colspan=2 valign=top>Results Achieved</td></tr>
				   <tr><td class="feed_option" colspan=2 style="font-weight: normal;" valign=top>#replace(customer_results,"#chr(10)#","<br>","all")#</td></tr>

                   <cfif customer_case_study_url is not "">
                     <tr><td class="feed_sub_header" colspan=2 valign=top>More Information</td></tr>
                     <tr><td class="feed_option"><a href="#customer_case_study_url#"><u>#customer_case_study_url#</u></a></td></tr>
                   </cfif>

                   </table>

                 </td></tr>

				  <cfif count is not customers.recordcount>
				   <tr><td height=20></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

				</cfif>

              </table>

          </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>