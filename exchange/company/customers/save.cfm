<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Cancel">

    <cflocation URL="/exchange/company/" addtoken="no">

<cfelseif #button# is "Add Customer Snapshot">

	<cfif #customer_logo# is not "">
		<cffile action = "upload"
		 fileField = "customer_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert customer
	  (customer_public, customer_order, customer_role, customer_paid, customer_type, customer_case_study_url, customer_topic_id, customer_naics, customer_logo, customer_name, customer_work, customer_results, customer_start, customer_end, customer_company_id)
	  values
	  (<cfif isdefined("customer_public")>1<cfelse>null</cfif>, #customer_order#, #customer_role#, #customer_paid#,'#customer_type#','#customer_case_study_url#',<cfif isdefined("customer_topic_id")>'#customer_topic_id#'<cfelse>null</cfif>,'#customer_naics#',<cfif #customer_logo# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,'#customer_name#','#customer_work#','#customer_results#',<cfif #customer_start# is "">null<cfelse>'#customer_start#'</cfif>,<cfif #customer_end# is "">null<cfelse>'#customer_end#'</cfif>,#session.company_id#)
 	</cfquery>

    <cflocation URL="/exchange/company/customers/index.cfm?l=4&u=1" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select customer_logo from customer
	  where (customer_id = #customer_id#) and
	        (customer_company_id = #session.company_id#)
	</cfquery>

    <cfif remove.customer_logo is not "">
     <cfif fileexists("#media_path#\#remove.customer_logo#")>
		 <cffile action = "delete" file = "#media_path#\#remove.customer_logo#">
     </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete customer
	  where (customer_id = #customer_id#) and
	        (customer_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/customers/index.cfm?l=4&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select customer_logo from customer
		  where customer_id = #customer_id#
		</cfquery>

	    <cfif fileexists("#media_path#\#remove.customer_logo#")>
			<cffile action = "delete" file = "#media_path#\#remove.customer_logo#">
		</cfif>

	</cfif>

	<cfif #customer_logo# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select customer_logo from customer
		  where customer_id = #customer_id#
		</cfquery>

		<cfif #getfile.customer_logo# is not "">
     	 <cfif fileexists("#media_path#\#getfile.customer_logo#")>
		 	<cffile action = "delete" file = "#media_path#\#getfile.customer_logo#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "customer_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update customer
	  set customer_name = '#customer_name#',

		  <cfif #customer_logo# is not "">
		   customer_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   customer_logo = null,
		  </cfif>

	      customer_work = '#customer_work#',
	      customer_type = '#customer_type#',
	      customer_paid = #customer_paid#,
	      customer_public = <cfif isdefined("customer_public")>1<cfelse>null</cfif>,
	      customer_role = #customer_role#,
	      customer_order = #customer_order#,
	      customer_case_study_url = '#customer_case_study_url#',
	      customer_naics = '#customer_naics#',
	      customer_topic_id = <cfif isdefined("customer_topic_id")>'#customer_topic_id#'<cfelse>null</cfif>,
	      customer_results = '#customer_results#',
	      customer_start = <cfif #customer_start# is "">null<cfelse>'#customer_start#'</cfif>,
	      customer_end = <cfif #customer_end# is "">null<cfelse>'#customer_end#'</cfif>
	  where (customer_id = #customer_id# ) and
	        (customer_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/customers/index.cfm?l=4&u=1" addtoken="no">

</cfif>

