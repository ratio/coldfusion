<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Add Customer Snapshot</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Customer Name</b></td>
                    <td><input name="customer_name" class="input_text" type="text" size=100 maxlength="100" required>

 			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
 			  <span class="tooltiptext">If you don't want to disclose your Customers name, feel free to describe them as "Major Financial Client" or something less specific.</span>
			 </div>

			 </td>

                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Overview of Engagement</b></td>
                    <td><textarea name="customer_work" class="input_textarea" rows=6 cols=101 placeholder="Please describe the type of work you performed for this customer."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Results Achieved</b></td>
                    <td><textarea name="customer_results" class="input_textarea" rows=6 cols=101 placeholder="Please describe the results and accomplishments you achieved."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>More Information (URL)</b></td>
                    <td><input name="customer_case_study_url" class="input_text" type="url" size=100 maxlength="300" placeholder="http://www..."></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Start Date</b></td>
                    <td><input name="customer_start" class="input_date" type="date" size=15></td>
                </tr>

                <tr><td class="feed_sub_header"><b>End Date</b></td>
                    <td><input name="customer_end" class="input_date" type="date" size=15></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Type of Engagement</b></td>
                    <td>
                        <select name="customer_type" class="input_select">
                          <option>Proof of Concept
                          <option>Prototype / Pilot
                          <option selected>Production Implementation
                        </select>

                        <span class="feed_sub_header">&nbsp;Paid?&nbsp;&nbsp;</span>

                        <select name="customer_paid" class="input_select">
                          <option value=1>Yes
                          <option value=2>No
                        </select>

                        <span class="feed_sub_header">&nbsp;Role&nbsp;&nbsp;</span>

                        <select name="customer_role" class="input_select">
                          <option value=1>Lead
                          <option value=2>Support
                        </select>

                    </td>
                </tr>

                <tr><td class="feed_sub_header"><b>Federal NAICS Code</b></td>
                    <td class="feed_option"><input name="customer_naics" class="input_text" type="text" size=10>
                    &nbsp;&nbsp;If this was a Federal award please include the award NAICS code.  If not, please leave blank.</td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Customer Logo</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					  <input type="file" name="customer_logo"></td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="customer_order" required class="input_text" style="width: 75px;" required type="number" step=".1">&nbsp;&nbsp;Determines the order this Customer Snapshot will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="customer_public" style="width: 20px; height: 20px;" value=1>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Customer Snapshot"></td></tr>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>