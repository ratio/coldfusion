<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<cfquery name="customer_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from customer
 where (customer_company_id = #session.company_id#) and
       (customer_id = #customer_id#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Edit Customer Snapshot</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>

			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Customer snapshot has been successfully updated.</b></font></td></tr>
                 </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Customer Name</b></td>
                    <td><input name="customer_name" class="input_text" type="text" size=100 maxlength="100" value="#customer_profile.customer_name#" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Overview of Engagement</b></td>
                    <td><textarea name="customer_work" class="input_textarea" rows=6 cols=101>#customer_profile.customer_work#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Results Achieved</b></td>
                    <td><textarea name="customer_results" class="input_textarea" rows=6 cols=101>#customer_profile.customer_results#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>More Information (URL)</b></td>
                    <td><input name="customer_case_study_url" class="input_text" type="url" size=100 maxlength="300" value="#customer_profile.customer_case_study_url#" placeholder="http://www..."></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Start Date</b></td>
                    <td><input name="customer_start" class="input_date" type="date" size=15 value="#customer_profile.customer_start#"></td>
                </tr>

                <tr><td class="feed_sub_header"><b>End Date</b></td>
                    <td><input name="customer_end" class="input_date" type="date" size=15 value="#customer_profile.customer_end#"></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Type of Engagement</b></td>
                    <td>
                        <select name="customer_type" class="input_select">
                          <option <cfif #customer_profile.customer_type# is "Proof of Concept">selected</cfif>>Proof of Concept
                          <option <cfif #customer_profile.customer_type# is "Prototype / Pilot">selected</cfif>>Prototype / Pilot
                          <option <cfif #customer_profile.customer_type# is "Production Implementation">selected</cfif>>Production Implementation
                        </select>

                        <span class="feed_sub_header">&nbsp;Paid?&nbsp;&nbsp;</span>

                        <select name="customer_paid" class="input_select">
                          <option value=1 <cfif #customer_profile.customer_paid# is 1>selected</cfif>>Yes
                          <option value=2 <cfif #customer_profile.customer_paid# is 2>selected</cfif>>No
                        </select>

                        <span class="feed_sub_header">&nbsp;Role&nbsp;&nbsp;</span>

                        <select name="customer_role" class="input_select">
                          <option value=1 <cfif #customer_profile.customer_role# is 1>selected</cfif>>Lead
                          <option value=2 <cfif #customer_profile.customer_role# is 2>selected</cfif>>Support
                        </select>

                    </td>
                </tr>

                <tr><td class="feed_sub_header"><b>Federal NAICS Code</b></td>
                    <td class="feed_option"><input name="customer_naics" class="input_text" type="text" size=10 value="#customer_profile.customer_naics#">
                    &nbsp;&nbsp;If this was a Federal award please include the award NAICS code.  If not, please leave blank.</td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Customer Logo</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #customer_profile.customer_logo# is "">
					  <input type="file" name="customer_logo">
					<cfelse>
					  <img src="#media_virtual#/#customer_profile.customer_logo#" width=150><br><br>
					  <input type="file" name="customer_logo"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo
					 </cfif>

                 <input type="hidden" name="customer_id" value=#customer_id#>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="customer_order" required class="input_text" style="width: 75px;" required type="number" step=".1" value="#customer_profile.customer_order#">&nbsp;&nbsp;Determines the order this Customer Snapshot will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="customer_public" style="width: 20px; height: 20px;" value=1 <cfif #customer_profile.customer_public# is 1>checked</cfif>>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>


                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update">
				     &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Customer Snapshot?\r\nAre you sure you want to delete this customer snapshot?');">
				</td></tr>


                 </cfoutput>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>