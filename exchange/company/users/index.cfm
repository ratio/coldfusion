<cfinclude template="/exchange/security/check.cfm">

<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 join usr_comp on usr_comp_usr_id = usr_id
 where usr_comp_company_id = #session.company_id#
 order by usr_last_name
</cfquery>

<cfquery name="comp_rights" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_comp
 where usr_comp_usr_id = #session.usr_id# and
       usr_comp_company_id = #session.company_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Users <cfif #users.recordcount# GT 0> (<cfoutput>#users.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header" valign=middle>

			        <cfif comp_rights.usr_comp_access_rights is 1>
                    	<a href="/exchange/company/users/add.cfm?l=8"><img src="/images/plus3.png" width=15 vspace=5 valign=middle border=0 alt="Add User" title="Invite User"></a>&nbsp;&nbsp;<a href="/exchange/company/users/add.cfm?l=8">Invite User</a>
                    </cfif>

			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Password successfully changed.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>User information has been successfully saved.</b></font></td></tr>
                <cfelseif u is 3>
                  <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Notification settings successfully updated.</b></font></td></tr>
                <cfelseif u is 4>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Market, Sector, and Capability & Services alignments have been successfully updated.</b></font></td></tr>
                <cfelseif u is 5>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Company banner has been successfully updated.</b></font></td></tr>
                <cfelseif u is 9>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>User has been successfully removed from the Company.</b></font></td></tr>
                 </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif #users.recordcount# is 0>

               <tr><td class="feed_sub_header">No users exist for this Company.</td></tr>

               <cfelse>

			   <tr>
			      <td></td>
			      <td class="feed_sub_header">Name</td>
			      <td class="feed_sub_header">Email</td>
			      <td class="feed_sub_header">Company</td>
			      <td class="feed_sub_header">Title</td>
                  <td></td>
			   </tr>

               <cfset count = 1>

			   <cfoutput query="users">

				<cfif users.usr_company_id is "">
				 <cfset usr_id = 0>
				<cfelse>
				 <cfset usr_id = #users.usr_company_id#>
				</cfif>

				<cfquery name="cname" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select company_id, company_name from company
				 where company_id = #usr_id#
				</cfquery>

               <tr>

                   <td valign=top class="feed_sub_header" width=60>

                   <cfif #users.usr_photo# is "">
                   <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" width=40 height=40 vspace=5 border=0 alt="Edit User" title="Edit User"></a>
                   <cfelse>
                   <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#users.usr_photo#" width=40 height=40 vspace=5 border=0 alt="Edit User" title="Edit User"></a>
                   </cfif>

                   </td>

				       <td class="feed_header" width=250><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#usr_first_name# #usr_last_name#</a></td>
				       <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>

				       <td class="feed_sub_header" style="font-weight: normal;">

				       <cfif cname.company_name is "">
				        Not Provided
				       <cfelse>
				       #cname.company_name#
				       </cfif>
				       </td>

				       <td class="feed_sub_header" style="font-weight: normal;">

				       <cfif usr_title is "">
				        Not Provided
				       <cfelse>
				       #usr_title#
				       </cfif>
				       </td>

				       <td align=right class="feed_sub_header">

			           <cfif comp_rights.usr_comp_access_rights is 1>
				       	<a href="/exchange/company/users/edit.cfm?l=12&i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit User" title="Edit User" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/users/edit.cfm?l=12&i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Edit</a>
				       </cfif>


				       </td>
                  </tr>

				  <cfif count is not users.recordcount>
				   <tr><td colspan=7><hr></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

				</cfif>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>