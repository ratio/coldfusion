<cfinclude template="/exchange/security/check.cfm">

<cfquery name="states" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Invite User</td><td>

			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>User email address already exists.</b></font></td></tr>
                </cfif>
                <tr><td height=10></td></tr>
               </cfif>

               <tr><td>

           <form action="invite_save.cfm" method="post">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" width=20%><b>First Name</b></td>
                    <td><input name="usr_first_name" class="input_text" type="text" size=30 maxlength="100" required></td>
                </tr>

                <tr><td class="feed_sub_header" width=20%><b>Last Name</b></td>
                    <td><input name="usr_last_name" class="input_text" type="text" size=30 maxlength="100" required></td>
                </tr>

                <tr><td class="feed_sub_header" width=20%><b>Email Address</b></td>
                    <td class="feed_option"><input name="usr_email" class="input_text" type="email" size=30 maxlength="100" required> email address must not exist already</td>
                </tr>

                <tr><td class="feed_sub_header" width=20%><b>Title</b></td>
                    <td><input name="usr_title" class="input_text" type="text" size=30 maxlength="100"></td>
                </tr>


                   <tr>
                      <td class="feed_sub_header" valign=top>Address</td>
                      <td>

                      <input type="text" name="usr_address_line_1" class="input_text" size=60 maxlength="249" placeholder="Address (Line 1)"><br>
                      <input type="text" name="usr_address_line_2" class="input_text" size=60 maxlength="249" placeholder="Address (Line 2)"><br>
                      <input type="text" name="usr_city" class="input_text" size=30 maxlength="99" placeholder="City">

                      <select name="usr_state" class="input_select">
                      <option value=0>Select State
                      <cfoutput query="states">
                       <option value="#state_abbr#">#state_name#
                      </cfoutput>

                      <input type="text" name="usr_zip" class="input_text" size=20 maxlength="19" placeholder="Zipcode"><br>

                       </td>
                   </tr>

                <tr><td class="feed_sub_header" width=20%><b>Phone</b></td>
                    <td><input name="usr_phone" class="input_text" type="tel" size=30 maxlength="20"></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Status</b></td>
                    <td class="feed_option">

                        <select name="usr_active" class="input_select">
                         <option value=1>Active
                         <option value=0>Not Active
                        </select>

                        &nbsp;indicates whether a user is authorized to login and be associated with your company.

                    </td>
                </tr>

                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" colspan=2>By clicking Invite User we will email the above user an invitation to join your company.</td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Invite User"></td></tr>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>