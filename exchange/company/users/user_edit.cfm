<cfinclude template="/exchange/security/check.cfm">

<cfquery name="user_data" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where (usr_id = #usr_id#) and
       (usr_company_id = #session.company_id#)
</cfquery>

<cfquery name="state" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from state
 order by state_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  <cfinclude template="/exchange/company/users.cfm">

	  </td><td valign=top>

	  <div class="main_box">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header"><b>Edit User</b></td>
			       <td class="feed_option" align=right><a href="/exchange/company/"><img src="/images/delete.png" border=0 width=20></a></td></tr>

			   <tr><td>&nbsp;</td></tr>

           </table>

               <cfoutput>

               <form action="invite_save.cfm" method="post">

  			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td class="feed_option"><b>First Name</b></td></tr>
                   <tr><td><input type="text" name="usr_first_name" size=25 value="#user_data.usr_first_name#"> *</td></tr>
                   <tr><td>&nbsp;</td></tr>

			       <tr><td class="feed_option"><b>Last Name</b></td></tr>
                   <tr><td><input type="text" name="usr_last_name" size=25 value="#user_data.usr_last_name#"> *</td></tr>

				   <tr><td>&nbsp;</td></tr>

				   <tr><td class="feed_option"><b>Email</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_email" size=25 value="#user_data.usr_email#"><font size=2> *</td></tr>
                   <tr><td>&nbsp;</td></tr>

				   <tr><td class="feed_option"><b>Title</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_title" size=25 value="#user_data.usr_title#"></td></tr>
                   <tr><td>&nbsp;</td></tr>

			       <tr><td class="feed_option"><b>Address</b></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_address_line_1" size=50 value="#user_data.usr_address_line_1#"></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_address_line_2" size=50 value="#user_data.usr_address_line_2#"></td></tr>
                   <tr><td class="feed_option"><input type="text" name="usr_city" size=30 value="#user_data.usr_city#">&nbsp;&nbsp;

                   </cfoutput>

                   <select name="usr_state" style="height: 21px;">
                   <option value=0>
                   <cfoutput query="state">
                   <option value="#state_abbr#" <cfif #user_data.usr_state# is #state_abbr#>selected</cfif>>#state_name#
                   </cfoutput>
                   </select>
                   &nbsp;&nbsp;

                   <cfoutput>

                   <input type="text" name="usr_zip" size=10 value="#user_data.usr_zip#">

                   </td></tr>

                   <tr><td>&nbsp;</td></tr>
			       <tr><td class="feed_option"><b>Phone</b></td></tr>
                   <tr><td><input type="tel" name="usr_phone" size=25 value="#user_data.usr_phone#"></td></tr>
                   <tr><td>&nbsp;</td></tr>

                   <tr><td class="feed_option"><b>User Status</b>&nbsp;
                   <select name="usr_active">
                    <option value=1 <cfif #user_data.usr_active# is 1>selected</cfif>>Active
                    <option value=0 <cfif #user_data.usr_active# is 0 or #user_data.usr_active# is "">selected</cfif>>Not Active
                   </select>

                   &nbsp; (determines whether the user can see your company profile)

                   </td></tr>

                   <tr><td height=10></td></tr>

			       <tr><td><input class="button_blue" type="submit" name="button" value="Save" vspace=10>&nbsp;&nbsp;
			               <input class="button_blue" type="submit" name="button" value="Remove User" vspace=10 onclick="return confirm('Remove User?\r\nAre you sure you want to remove this user from your Company?  By doing so, this user will no longer be associated with your Company.');">&nbsp;&nbsp;
			               </td></tr>

                   <input type="hidden" name="user_id" value=#user_data.usr_id#>

			   </table>

			   </form>

			   </cfoutput>

		   </td></tr>

 		  </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

