<cfinclude template="/exchange/security/check.cfm">

<cfquery name="rights" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_comp
 where usr_comp_company_id = #session.company_id# and
       usr_comp_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Edit User</td><td>

			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>User email address already exists.</b></font></td></tr>
                </cfif>
                <tr><td height=10></td></tr>
               </cfif>

               <tr><td>

           <form action="invite_save.cfm" method="post">

		   <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" width=125><b>Access Rights</b></td>
                    <td>
                    <select name="usr_comp_access_rights" class="input_select">
                    <option value=0>Regular
                    <option value=1 <cfif #rights.usr_comp_access_rights# is 1>selected</cfif>>Administrator
                    </select>
                    </td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td>

				<input class="button_blue_large" type="submit" name="button" value="Save">&nbsp;&nbsp;
				<input class="button_blue_large" type="submit" name="button" value="Remove User" onclick="return confirm('Remove User?\r\nAre you sure you want to remove this user from your Company?');">

				</td></tr>

               <input type="hidden" name="i" value=#i#>

                </cfoutput>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>