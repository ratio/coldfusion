<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Invite User">

 <cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select usr_id from usr
  where usr_email = '#usr_email#'
 </cfquery>

 <cfif check.recordcount GT 0>
  <cflocation URL="/exchange/company/users/add.cfm?u=1&l=12" addtoken="no">
 </cfif>

 <cftransaction>

	<!-- Generate Validation Code --->

	<cfset nmbr1=randrange(1000,9999)>
	<cfset nmbr2=randrange(1000,9999)>

	<cfset result1="">
	<cfset i=0>

	<cfloop index="i" from="1" to="4">
	 <cfset result1=result1&Chr(RandRange(65, 90))>
	</cfloop>

	<cfset result2="">
	<cfset j=0>

	<cfloop index="j" from="1" to="4">
	 <cfset result2=result2&Chr(RandRange(65, 90))>
	</cfloop>

	<cfset invite_password = #nmbr1#&#result1#&#nmbr2#>

	<cfquery name="invite_user" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr.usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select company_name from company
	 where company_id = #invite_user.usr_company_id#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert usr
	 (usr_active,
	  usr_password,
	  usr_first_name,
	  usr_last_name,
	  usr_email,
	  usr_title,
	  usr_address_line_1,
	  usr_address_line_2,
	  usr_city,
	  usr_state,
	  usr_zip,
	  usr_phone,
	  usr_invite,
	  usr_invite_date,
	  usr_created,
	  usr_updated,
	  usr_setting_notification,
	  usr_company_id)
	 values(
	   #usr_active#,
	  '#invite_password#',
	  '#usr_first_name#',
	  '#usr_last_name#',
	  '#usr_email#',
	  '#usr_title#',
	  '#usr_address_line_1#',
	  '#usr_address_line_2#',
	  '#usr_city#',
	  '#usr_state#',
	  '#usr_zip#',
	  '#usr_phone#',
	   1,
	   #now()#,
	   #now()#,
	   #now()#,
	   1,
	   #session.company_id#
	   )
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      select max(usr_id) as id from usr
    </cfquery>

    <!--- Associate user with Hub --->

    <cfif isdefined("session.hub")>

		<cfquery name="align_to_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert hub_xref(hub_xref_usr_id, hub_xref_hub_id, hub_xref_active, hub_xref_default, hub_xref_joined, hub_xref_usr_role)
		 values(#max.id#,#session.hub#,1,1,#now()#,2)
		</cfquery>

	</cfif>

	<cfquery name="lenses" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from lens
	 where lens_hub_id = #session.hub#
	 order by lens_name
	</cfquery>

	<cfset lens_list = valuelist(lenses.lens_id)>

		<cfloop index="element" list=#lens_list#>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_usr_id, align_type_id, align_type_value)
			 Values
			 (#max.id#, 1, #element#)
			</cfquery>

		</cfloop>

 </cftransaction>

 <cfif isdefined("session.hub")>

 	<cfquery name="hub_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 	 select * from hub
 	 where hub_id = #session.hub#
	</cfquery>

    <!-- Notify Hub User --->

 	<cfmail from="#hub_info.hub_name# <noreply@ratio.exchange>"
 			  to="#usr_email#"
 	  username="noreply@ratio.exchange"
 	  password="Gofus107!"
 		  port="25"
 		useSSL="false"
 		type="html"
 		server="mail.ratio.exchange"
 	   subject="#hub_info.hub_name# - Invitation to Join">
 	<html>
 	<head>
 	<title><cfoutput>#session.network_name#</cfoutput></title>
 	</head><div class="center">
 	<body class="body">

	 <cfoutput>
		 <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 20px; font-weight: bold; padding-top: 20px; padding-bottom: 10px;">#invite_user.usr_first_name# #invite_user.usr_last_name# from #company.company_name# has created an account on the #hub_info.hub_name# for you.</td></tr>
		  <tr><td>&nbsp;</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To login,  please <a href="#hub_info.hub_login_page#"><b>click here</b></a> or go to <a href="#hub_info.hub_login_page#"><b>#hub_info.hub_login_page#</b></a>.</td></tr>
		  <tr><td>&nbsp;</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Username: </b>#usr_email#</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Password: </b>#invite_password#</td></tr>
		  <tr><td>&nbsp;</td></tr>
 		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
 		  <tr><td>&nbsp;</td></tr>
 		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>#hub_info.hub_support_name#</b></td></tr>
 		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_email#</td></tr>
 		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#hub_info.hub_support_phone#</td></tr>
 		  <tr><td>&nbsp;</td></tr>
		 </table>
	 </cfoutput>

 	</body>
 	</html>

 	</cfmail>

 <cfelse>

	 <cfmail from="EXCHANGE <noreply@ratio.exchange>"
			  to="#usr_email#"
	   username="noreply@ratio.exchange"
	   password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
		subject="Invitation to join the EXCHANGE">
	 <html>
	 <head>
	 <title><cfoutput>#session.network_name#</cfoutput></title>
	 </head><div class="center">
	 <body class="body">

	 <cfoutput>
		 <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 20px; font-weight: bold; padding-top: 20px; padding-bottom: 10px;">#invite_user.usr_first_name# #invite_user.usr_last_name# from #company.company_name# has created an account on the EXCHANGE for you.</td></tr>
		  <tr><td>&nbsp;</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To login,  please <a href="https://go.ratio.exchange/signin/"><b>click here</b></a> or go to <a href="https://www.ratio.exchange/signin/"><b>https://www.ratio.exchange/signin/</b></a>.</td></tr>
		  <tr><td>&nbsp;</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Username: </b>#usr_email#</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Password: </b>#invite_password#</td></tr>
		  <tr><td>&nbsp;</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Thank You</b></td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">EXCHANGE Suppport Team</td></tr>
		  <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">support@ratio.exchange</td></tr>
		  <tr><td>&nbsp;</td></tr>
		 </table>
		 <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="223a60">
		  <tr><td height=75 valign=middle>&nbsp;<img src="http://go.ratio.exchange/images/exchange_logo.png" width=160 hspace=10></td></tr>
		 </table>
	 </cfoutput>
	 </body>
	 </html>

	</cfmail>

</cfif>

  <cflocation URL="/exchange/company/users/index.cfm?u=8&l=8" addtoken="no">

 <cfelseif #button# is "Remove User">

 <cftransaction>

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select usr_id from usr
	 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       usr_company_id = #session.company_id#
	</cfquery>

	<cfif check.recordcount is 1>

		<cfquery name="compid" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update usr
		 set usr_company_id = null
		 where usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		</cfquery>

	</cfif>

	<cfquery name="remove_association" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     delete usr_comp
	 where usr_comp_company_id = #session.company_id# and
	       usr_comp_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
    </cfquery>

</cftransaction>

    <cflocation URL="/exchange/company/users/index.cfm?u=9&l=8" addtoken="no">

 <cfelseif #button# is "Save">

	<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update usr_comp
	  set usr_comp_access_rights = #usr_comp_access_rights#
	  where usr_comp_company_id = #session.company_id# and
	        usr_comp_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

 <cflocation URL="/exchange/company/users/index.cfm?u=2&l=12" addtoken="no">

</cfif>

<cflocation URL="index.cfm" addtoken="no">