<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Partner">

	<cfif #partner_logo# is not "">
		<cffile action = "upload"
		 fileField = "partner_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert partner
	  (partner_order, partner_logo, partner_relationship, partner_website, partner_public, partner_name, partner_services, partner_type_id, partner_year_established, partner_created, partner_company_id)
	  values
	  (#partner_order#, <cfif #partner_logo# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,'#partner_relationship#','#partner_website#',<cfif isdefined("partner_public")>1<cfelse>null</cfif>,'#partner_name#','#partner_services#',#partner_type_id#,<cfif #partner_year_established# is "">null<cfelse>#partner_year_established#</cfif>, #now()#, #session.company_id#)
 	</cfquery>

    <cflocation URL="/exchange/company/partners/index.cfm?l=5&u=3" addtoken="no">

<cfelseif #button# is "Delete">


	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select partner_logo from partner
	  where (partner_id = #partner_id#) and
	        (partner_company_id = #session.company_id#)
	</cfquery>

    <cfif remove.partner_logo is not "">

     <cfif fileexists("#media_path#\#remove.partner_logo#")>
		 <cffile action = "delete" file = "#media_path#\#remove.partner_logo#">
	 </cfif>

	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete partner
	  where (partner_id = #partner_id#) and
	        (partner_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/partners/index.cfm?l=5&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select partner_logo from partner
		  where partner_id = #partner_id#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.partner_logo#")>
		 <cffile action = "delete" file = "#media_path#\#remove.partner_logo#">
		</cfif>

	</cfif>

	<cfif #partner_logo# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select partner_logo from partner
		  where partner_id = #partner_id#
		</cfquery>

		<cfif #getfile.partner_logo# is not "">
         <cfif fileexists("#media_path#\#getfile.partner_logo#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.partner_logo#">
		 </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "partner_logo"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update partner
	  set partner_name = '#partner_name#',

		  <cfif #partner_logo# is not "">
		   partner_logo = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   partner_logo = null,
		  </cfif>

	      partner_order = #partner_order#,
	      partner_website = '#partner_website#',
	      partner_relationship = '#partner_relationship#',
	      partner_services = '#partner_services#',
	      partner_public = <cfif isdefined("partner_public")>1<cfelse>null</cfif>,
	      <cfif #partner_year_established# is not "">
	      	partner_year_established = #partner_year_established#,
	      </cfif>
	      partner_type_id = #partner_type_id#

	  where partner_id = #partner_id# and
	        partner_company_id = #session.company_id#
	</cfquery>

    <cflocation URL="/exchange/company/partners/index.cfm?l=5&u=1" addtoken="no">

</cfif>

