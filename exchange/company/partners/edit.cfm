<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<cfquery name="partner_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner
 where (partner_company_id = #session.company_id#) and
       (partner_id = #partner_id#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Edit Partner</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm">Return</a>
               </td></tr>

			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Partner Name</b></td>
                    <td><input name="partner_name" class="input_text" type="text" size=100 maxlength="100" value="#partner_profile.partner_name#" required></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Partnership Type</b></td>
                    <td>
                        <select name="partner_type_id" class="input_select">
                                  <option value=1 <cfif #partner_profile.partner_type_id# is 1>selected</cfif>>Strategic
                                  <option value=2 <cfif #partner_profile.partner_type_id# is 2>selected</cfif>>Business
                                  <option value=3 <cfif #partner_profile.partner_type_id# is 3>selected</cfif>>Technical
                                  <option value=4 <cfif #partner_profile.partner_type_id# is 4>selected</cfif>>Operational
                       </select>

                       <span class="feed_sub_header">&nbsp;Year Established&nbsp;&nbsp;</span>

                       <input name="partner_year_established" class="input_text" type="number" style="width: 100px;" maxlength="4" value="#partner_profile.partner_year_established#">

                    </td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Partner Services</b></td>
                    <td><textarea name="partner_services" class="input_textarea" rows=6 cols=101 placeholder="Please describe the type of services this partner provides to your company or customers.">#partner_profile.partner_services#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Relationship Summary</b></td>
                    <td><textarea name="partner_relationship" class="input_textarea" rows=6 cols=101 placeholder="Please describe the relationship your company has with this partner.">#partner_profile.partner_relationship#</textarea></td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                </cfoutput>

                <cfoutput>

                <tr><td class="feed_sub_header" valign=top>Partner Logo</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #partner_profile.partner_logo# is "">
					  <input type="file" name="partner_logo">
					<cfelse>
					  <img src="#media_virtual#/#partner_profile.partner_logo#" width=150><br><br>
					  <input type="file" name="partner_logo"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo
					 </cfif>

					</td></tr>

                <tr><td class="feed_sub_header"><b>Partner Website</b></td>
                    <td><input name="partner_website" class="input_text" type="url" size=100 maxlength="300" value="#partner_profile.partner_website#" placeholder="http://www..."></td>
                </tr>


                <input type="hidden" name="partner_id" value=#partner_id#>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="partner_order" required class="input_text" style="width: 75px;" required type="number" step=".1" value="#partner_profile.partner_order#">&nbsp;&nbsp;Determines the order this Partner will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="partner_public" style="width: 20px; height: 20px;" value=1 <cfif #partner_profile.partner_public# is 1>checked</cfif>>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update">
				     &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Partner?\r\nAre you sure you want to delete this Partner?');">
				</td></tr>

                </cfoutput>

               </table>

			   </form>

               </td></tr>

             </table>

           </td></tr>

         </table>

 		</div>

	 </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>