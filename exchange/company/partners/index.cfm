<cfinclude template="/exchange/security/check.cfm">

<cfquery name="partners" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner
 where partner_company_id = #session.company_id#
 order by partner_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Partners & Alliances<cfif #partners.recordcount# GT 0> (<cfoutput>#partners.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header" valign=middle>
			       <cfif session.update_access is 1>
                    <a href="/exchange/company/customers/add.cfm?l=4"><img src="/images/plus3.png" width=15 valign=middle border=0 alt="Add Partner" title="Add Partner"></a>&nbsp;&nbsp;<a href="/exchange/company/partners/add.cfm?l=5">Add Partner</a>
                   </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Partner has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Partner has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Partner has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif partners.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">No Partners or Alliances have been entered.</td></tr>

               <cfelse>

               <cfset count = 1>

			   <cfoutput query="partners">

               <tr>
                   <td valign=top class="feed_sub_header" width=165 align=center>

                   <cfif #partners.partner_logo# is "">
                   <img src="/images/no_logo.png" width=150 vspace=5 border=0>
                   <cfelse>
                   <img src="#media_virtual#/#partner_logo#" width=150 vspace=5 border=0>
                   </cfif>

                   <br><br>

                   <cfif #partner_type_id# is 1>
                   Strategic Partner
                   <cfelseif #partner_type_id# is 2>
                   Business Partner
                   <cfelseif #partner_type_id# is 3>
                   Technical Partner
                   <cfelseif #partner_type_id# is 4>
                   Operational Partner
                   </cfif>

                   </td>

				   <td width=40>&nbsp;</td><td valign=top>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=middle>#partner_name#</td>
				       <td align=right class="feed_sub_header" valign=middle>
				       <cfif session.update_access is 1>
				       <a href="/exchange/company/partners/edit.cfm?l=5&partner_id=#partner_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Partner" title="Edit Partner" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/partners/edit.cfm?l=5&partner_id=#partner_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <cfif #partner_services# is not "">
				   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2 valign=top>#replace(partner_services,"#chr(10)#","<br>","all")#</td></tr>
				   <cfelse>
				   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2 valign=top>No description of Partner services or purpose has been created.</td></tr>
				   </cfif>

				   <cfif #partner_website# is not "">
				   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2 valign=top><a href="#partner_website#" target="_blank" rel="noopener" rel="noreferrer">#partner_website#</a></td></tr>
				   <cfelse>
				   <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2 valign=top>Partner website not provided</td></tr>
				   </cfif>


                 </table>

                 </td></tr>

				  <cfif count is not partners.recordcount>
				   <tr><td height=10></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

				</cfif>

              </table>

          </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>