<cfquery name="company_profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_id#
</cfquery>

<cfquery name="update_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_comp
 where usr_comp_usr_id = #session.usr_id# and
       usr_comp_company_id = #session.company_id#
</cfquery>

<cfif update_access.usr_comp_access_rights is 1>
 <cfset #session.update_access# = 1>
<cfelse>
 <cfset #session.update_access# = 0>
</cfif>

<cfoutput>
<style>
.banner {
    <cfif #company_profile.company_banner# is "">
    background-image: url('/images/company_stock_banner.png');
    <cfelse>
    background-image: url('#media_virtual#/#company_profile.company_banner#');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:185px;
}
</style>
</cfoutput>

<cfoutput>

   <table cellspacing=0 cellpadding=0 border=0 width=100% class="banner">
   <tr><td height=30></td></tr>

	   <tr><td align=middle valign=middle width=175>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <cfif #company_profile.company_logo# is "">
			   <tr><td align=middle><img src="//logo.clearbit.com/#company_profile.company_website#" width=125 style="border: 2px solid ##ffffff;" onerror="this.src='/images/no_logo.png'"></td></tr>
			  <cfelse>
			   <tr><td align=middle><img src="#media_virtual#/#company_profile.company_logo#" width=125 style="border: 2px solid ##ffffff;"></td></tr>
			  </cfif>

		  </table>

	   </td><td width=30>&nbsp;</td><td valign=top align=left>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" style="font-size: 40px; color: ffffff;">#company_profile.company_name#</td>
		     <td align=right><a href="/exchange/company/background.cfm"><img src="/images/pencil_white.png" width=40 hspace=30 border=0 alt="Change Banner" title="Change Banner"></a></td></tr>
		 <tr><td height=10></td></tr>
		 <tr><td class="feed_header" style="font-size: 20px; color: ffffff;"><cfif #company_profile.company_city# is "">CITY<cfelse>#ucase(company_profile.company_city)#</cfif>, <cfif #company_profile.company_state# is 0 or #company_profile.company_state# is "">STATE<cfelse>#ucase(company_profile.company_state)#</cfif></td></tr>
		 <tr><td height=10></td></tr>
		 <tr><td class="feed_header" style="font-size: 20px; color: ffffff;"><a class="feed_header" style="font-size: 20px; color: ffffff;" href="#company_profile.company_website#" target="_blank" rel="noopener" rel="noreferrer">#company_profile.company_website#</a></td></tr>
		 <tr><td height=10></td></tr>
		 <tr><td height=15></td></tr>
		 </table>

       </td></tr>

   <tr><td height=30></td></tr>

   </table>
</cfoutput>