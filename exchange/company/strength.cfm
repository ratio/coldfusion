<cfinclude template="/exchange/security/check.cfm">

<cfif session.company_id is 0>
 <cflocation URL="/exchange/company/setup.cfm" addtoken="no">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Company Profile Strength</td></tr>
               <tr><td><hr></td></tr>
               <tr><td class="feed_sub_header" style="font-weight: normal;">Your Company Profile Strength is a visual progress indictor that <b>only you can see and manage</b> that measures how much information in your Company Profile is complete / filled out.  Below are some recommendations we've noticed that you may want to consider completing to strengthen your Company Profile.</td></tr>
               <tr><td height=10></td></tr>
               <tr><td>

<cfquery name="ignore" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select strength_ignore_area_id from strength_ignore
 where strength_ignore_company_id = #session.company_id#
</cfquery>

<cfif ignore.recordcount is 0>
 <cfset ignore_list = 0>
<cfelse>
 <cfset ignore_list = valuelist(ignore.strength_ignore_area_id)>
</cfif>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 left join size on size_id = company_size_id
 left join entity on entity_id = company_entity_id
 left join usr on usr_id = company_admin_id
 where company_id = #session.company_id#
</cfquery>

<cfset counter = 0>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>
                <tr><td class="feed_header">Recommendations to strengthen your Company Profile ...</td>
                    <td class="feed_sub_header" align=right><a href="reset.cfm" onclick="return confirm('Reset Recommendations?\r\nResetting this page will bring any recommendations that you ignored back to this page.  Continue?');"><img src="/images/icon_reset.png" width=20 border=0 alt="Reset Recommendations" title="Reset Recommendations" hspace=10></a><a href="reset.cfm" onclick="return confirm('Reset Recommendations?\r\nResetting this page will bring any recommendations that you ignored back to this page.  Continue?');">Reset Recommendations</a></td></tr>

                <cfif isdefined("u")>
                 <cfif u is 1>
                  <tr><td colspan=2 class="feed_sub_header" style="color: green;">Recommendation ignored.</td></tr>
                 <cfelseif u is 2>
                  <tr><td colspan=2 class="feed_sub_header" style="color: green;">Recommendations have been successfully reset.</td></tr>
                 </cfif>
                </cfif>


               </table>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td height=10></td></tr>

                <!--- Company Description --->

                <cfif comp.company_about is "" and not listfind(ignore_list,1)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/update/';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>ABOUT YOUR COMPANY</b><br>You have not provided a description of your Company.  This information is critical
					   for members to understand who you are and what you do.
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=1';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- Company Keywords --->

                <cfif comp.company_keywords is "" and not listfind(ignore_list,2)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/update/';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>COMPANY KEYWORDS (IMPORTANT)</b><br>Company keywords is an important field that allows members to find your company and the products or services you deliver.  For instance,
					   "machine learning, clinical, sensors, etc.".  Please update your Company Profile as soon as possible as you don't want to miss an opportunity.
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=2';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- Company History --->

                <cfif comp.company_history is "" and not listfind(ignore_list,4)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/update/';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>COMPANY HISTORY</b><br>Members are curious about your history and how your company came to be.  This provides them
					   a background of your experience and how your company came to deliver the products and services you have.   You don't
					   need to write a novel, but anything about your history would be great.
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=4';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- Company History --->

                <cfif comp.company_history is "" and not listfind(ignore_list,5)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/update/';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>COMPANY LEADERSHIP / TEAM</b><br>You've got great people!!  Here's your chance to showcase them and their role
					   in your company.  Members, investors, buyers, and partners want to understand who makes the magic.  Let'em know!
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=5';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>




                <!--- Company DUNS --->

                <cfif comp.company_duns is "" and not listfind(ignore_list,3)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/update/';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>COMPANY DUNS</b><br>Do you have a DUNS number?  If so, please add it to your Company Profile and we will
					   automatically link to public sources of information to build your Federal profile.
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=3';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>


                <!--- No Products --->

                <cfif product_count.total is 0 and not listfind(ignore_list,300)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/products/add.cfm?l=3';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>PRODUCTS & SERVICES</b><br>You do not added any Products or Services in your Company Profile.   This information
					   is extremely important for buyers and partners to understand what you do, your key differentiators and your experience.
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=300';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- One Product --->

                <cfif product_count.total is 1 and not listfind(ignore_list,301)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/products/add.cfm?l=3';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>PRODUCTS & SERVICES</b><br>Great job in adding a Product or Service.  If your company has any other
					   Products or Services, please add them so members can find out more about what you do.
					 </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=301';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- No Customers --->

                <cfif customer_count.total is 0 and not listfind(ignore_list,400)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/customers/add.cfm?l=4';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>CUSTOMER SNAPSHOTS	</b><br>You do not added any Customer Snapshots.   Customer Snapshots tell members
					   that you've piloted, prototyped, or sold your products or services to customers which reduces their risk of selecting you.  Plus, it provides more insights into the value of your products and services and how they were used.
					   </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=400';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- One Customer --->

                <cfif customer_count.total is 1 and not listfind(ignore_list,401)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/customers/add.cfm?l=4';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>CUSTOMER SNAPSHOTS	</b><br>Great, you added a Customer!  Nice job.  To demonstrate diversity, scale and that you
					   have a solid product or service you may want to add more customers to your profile.
					   </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=401';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- No Partners --->

                <cfif partner_count.total is 0 and not listfind(ignore_list,500)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/partners/add.cfm?l=5';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>PARTNERS & ALLIANCES	</b><br>You do not added any Partners or Alliances.   Partners tell members that
					   you're going to market with organizations that complement and enable your products and services.  Suggest
					   adding any organizations that are helping you G2M.
					   </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=500';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- No Marketing Material --->

                <cfif marketing_count.total LT 3 and not listfind(ignore_list,600)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/partners/add.cfm?l=6';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>MARKETING MATERIAL</b><br>You need to upload some Marketing material that describes your company, products, or services.   We're sure
					   you've already created this material... why not upload it in your Company Profile so members can download it and find out
					   more information.
					   </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=600';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>

                <!--- No Certifications --->

                <cfif cert_count.total is 0 and not listfind(ignore_list,1100)>

					<cfset counter = counter + 1>

					 <tr>
					   <td valign=top width=75><input type="submit" name="button" value="Fix" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/partners/add.cfm?l=11';"></td>
					   <td valign=top class="feed_sub_header" style="font-weight: normal;">

					   <b>CERTIFICATIONS</b><br>You do not added any company Certifications.  Certifications demonstrate to members that your
					   company has invested in standards, frameworks or methodologies to support products development or enable service delivery.
					   </td>

					 <td valign=top align=right width=100><input type="submit" name="button" value="Ignore" class="button_blue" style="margin-top: 15px;" onclick="location.href='/exchange/company/ignore.cfm?area_id=1100';"></td>

					 </tr>
					 <tr><td height=10></td></tr>
					 <tr><td colspan=3><hr></td></tr>
					</cfif>


               </table>

<cfif counter is 0>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	<tr><td class="feed_sub_header" style="font-weight: normal;">Congratulations, your profile looks great!<br><br>Please be sure to monitor your Company Profile Strength as we are always adding new functions and features to get you discovered faster.</td></tr>
   </table>

</cfif>


           </td></tr>
         </table>
        </td></tr>
     </table>
   </div>

   </cfoutput>

   </td></tr>
</table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>