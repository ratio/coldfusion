<cfinclude template="/exchange/security/check.cfm">

<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_id#
</cfquery>

<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns = '#company.company_duns#'
</cfquery>

<cfquery name="certs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sba
 where duns = '#company.company_duns#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Federal Profile</td>
                   <td align=right class="feed_sub_header"></td></tr>
               <tr><td colspan=2><hr></td></tr>

               <tr><td colspan=2>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td height=10></td></tr>

            <tr><td>

            <cfoutput>

                   <cfif sams.recordcount is 0>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>
                         <tr><td class="feed_sub_header">No Federal information could be found.</td></tr>
                         <tr><td class="feed_sub_header" style="font-weight: normal;">To link Federal information to your Company please ensure your <a href="/exchange/company"><b>DUNS Number</b></a> in entered and correct in your Company Profile.</td></tr>
                       </table>

                   <cfelse>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                         <tr><td colspan=4 class="feed_sub_header">The information in your Federal Profile was linked from <a href="http://www.sam.gov" target="_blank" rel="noopener" rel="noreferrer"><u>http://www.sam.gov</u></a> using your company's DUNS number.  This information cannot be changed.</td></tr>
                         <tr><td height=10></td></tr>

						<tr><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							<tr><td class="feed_option"><b>Corporate Information</b></td></tr>

							<tr><td class="feed_option"><b>Website: </b>

							<cfoutput>
							<cfif left("#sams.corp_url#",4) is "http" or left("#sams.corp_url#",5) is "https">
							  <a href="#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#sams.corp_url#</a>
							<cfelse>
							 <a href="http://#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#sams.corp_url#</a>
							</cfif>
							</cfoutput><br>

														<b>DUNS: </b>#sams.duns#<cfif #sams.duns_4# is not "">-#sams.duns_4#</cfif><br>
														<b>State of Incorporation: </b>#sams.state_of_inc#<br>
														<b>Cage Code: </b>#sams.cage_code#

														</td></tr>


							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							<tr><td class="feed_option"><b>Address</b></td></tr>
							<tr><td class="feed_option"><cfif #sams.phys_address_l1# is not "">#sams.phys_address_l1#<br></cfif>
														<cfif #sams.phys_address_line_2# is not "">#sams.phys_address_line_2#<br></cfif>
														#sams.city_1# #sams.state_1#, #sams.zip_1#<br>
														</td></tr>
							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							<tr><td class="feed_option"><b>Important Dates</b></td></tr>
							<tr><td class="feed_option">
														<b>Founded: </b>#mid(sams.biz_start_date,5,2)#/#right(sams.biz_start_date,2)#/#left(sams.biz_start_date,4)#<br>
														<b>Registration Date: </b>#mid(sams.init_reg_date,5,2)#/#right(sams.init_reg_date,2)#/#left(sams.init_reg_date,4)#<br>
														<b>Activation Date: </b>#mid(sams.activation_date,5,2)#/#right(sams.activation_date,2)#/#left(sams.activation_date,4)#<br>
														<b>Expiration Date: </b>#mid(sams.reg_exp_date,5,2)#/#right(sams.reg_exp_date,2)#/#left(sams.reg_exp_date,4)#<br>
														<b>Last Updated: </b>#mid(sams.last_update,5,2)#/#right(sams.last_update,2)#/#left(sams.last_update,4)#<br>

														 <cfif certs.eight_start is not "">
														 <b>8(a) Certification:</b> #dateformat(certs.eight_start,'mm/dd/yyyy')# - #dateformat(certs.eight_end,'mm/dd/yyyy')#<br>
														 </cfif>

														 <cfif certs.hub_start is not "">
														 <b>HubZone Certification:</b> #dateformat(certs.hub_start,'mm/dd/yyyy')# - #dateformat(certs.hub_end,'mm/dd/yyyy')#<br>
														 </cfif>

														 <cfif certs.sdb_start is not "">
														 <b>SDB Certification: </b> #dateformat(certs.sdb_start,'mm/dd/yyyy')# - #dateformat(certs.sdb_end,'mm/dd/yyyy')#<br>
														 </cfif>

														</td></tr>

							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

					   <tr>
					       <td class="feed_option"><b>Business Type</b></td>

					   <tr><td class="feed_option" valign=top>

                       <cfif sams.biz_type_string is "">
						   Unknown
                       <cfelse>

						   <cfloop index="element" list="#listsort(sams.biz_type_string,'Text','ASC','~')#" delimiters="~">

							<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
							 select biz_code_name from biz_code
							 where biz_code = '#element#'
							</cfquery>

							<cfoutput>
							#code.biz_code_name#<br>
							</cfoutput>
						   </cfloop>

					   </cfif>

						   </td>
						   <td class="feed_option" valign=top></td></tr>


							</table>

						</td></tr>

					   <tr><td colspan=4><hr></td></tr>

					   </table>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<tr><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							<tr><td class="feed_option"><b>Primary Point of Contact</b></td></tr>
							<tr><td class="feed_option">#sams.poc_fnme# #sams.poc_lname#<br>
														<cfif #sams.poc_title# is not "">#sams.poc_title#<br></cfif>
														(#left(sams.poc_us_phone,3)#) #mid(sams.poc_us_phone,4,3)#-#right(sams.poc_us_phone,4)#<br>
														<a href="mailto:#sams.poc_email#">#sams.poc_email#</a></td></tr>
							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							<tr><td class="feed_option"><b>Alternate Point of Contact</b></td></tr>
							<tr><td class="feed_option"><cfif #sams.alt_poc_lname# is not "">#sams.alt_poc_fname# #sams.alt_poc_lname#<br><cfelse>Not Provided</cfif>
														<cfif #sams.alt_poc_title# is not "">#sams.alt_poc_title#<br></cfif>
														<cfif #sams.akt_poc_phone# is not "">(#left(sams.akt_poc_phone,3)#) #mid(sams.akt_poc_phone,4,3)#-#right(sams.akt_poc_phone,4)#<br></cfif>
														<cfif #sams.alt_poc_email# is not ""><a href="mailto:#sams.alt_poc_email#">#sams.alt_poc_email#</a></cfif></td></tr>
							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							<tr><td class="feed_option"><b>Past Performance Point of Contact</b></td></tr>
							<tr><td class="feed_option"><cfif #sams.pp_poc_lname# is not "">#sams.pp_poc_fname# #sams.pp_poc_lname#<br><cfelse>Not Provided</cfif>
														<cfif #sams.pp_poc_title# is not "">#sams.pp_poc_title#<br></cfif>
														<cfif #sams.pp_poc_phone# is not "">(#left(sams.pp_poc_phone,3)#) #mid(sams.pp_poc_phone,4,3)#-#right(sams.pp_poc_phone,4)#<br></cfif>
														<cfif #sams.pp_poc_email# is not ""><a href="mailto:#sams.pp_poc_email#">#sams.pp_poc_email#</a></cfif></td></tr>
							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							<tr><td class="feed_option"><b>Electronic Point of Contact</b></td></tr>
							<tr><td class="feed_option"><cfif #sams.elec_bus_poc_lnmae# is not "">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#<br><cfelse>Not Provided</cfif>
														<cfif #sams.elec_bus_poc_title# is not "">#sams.elec_bus_poc_title#<br></cfif>
														<cfif #sams.elec_bus_poc_us_phone# is not "">(#left(sams.elec_bus_poc_us_phone,3)#) #mid(sams.elec_bus_poc_us_phone,4,3)#-#right(sams.elec_bus_poc_us_phone,4)#<br></cfif>
														<cfif #sams.elec_bus_poc_email# is not ""><a href="mailto:#sams.elec_bus_poc_email#">#sams.elec_bus_poc_email#</a></cfif></td></tr>
							</table>

						</td></tr>

					   <tr><td colspan=4><hr></td></tr>

					   </table>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                   <tr><td valign=top width=50%>

					   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					   <tr><td class="feed_option"><b>NAICS Codes</b></td></tr>
					   <tr><td class="feed_option" valign=top>

					   <cfif #sams.naisc_code_string# is "" or #sams.pri_naics# is "">
						No NAICS codes were found.

					   <cfelse>

					   <cfloop index="element" list="#listsort(sams.naisc_code_string,'Text','ASC','~')#" delimiters="~">

						<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						 select naics_code, naics_code_description from naics
						 where naics_code = '#left(element,'6')#'
						</cfquery>

						<cfoutput>
						<cfif #sams.pri_naics# is #left(element,'6')#>
						 <b>#left(element,'6')# - #code.naics_code_description#</b><br>
						<cfelse>
						 #left(element,'6')# - #code.naics_code_description#<br>
						</cfif>
						</cfoutput>
					   </cfloop>

					   <br>

                       <b><i>Bold - Indicates primary NAICS code</i></b>

					   </cfif>

					   </td></tr>

					   </table>

                   </td><td valign=top width=50%>

                   <table cellspacing=0 cellpadding=0 border=0 width=100%>
                    <tr><td class="feed_option"><b>Product or Service Codes</b></td></tr>
                    <tr><td class="feed_option">

 					   <cfif #sams.psc_code_string# is "">
						No Product or Service codes found.

					   <cfelse>

					   <cfloop index="psc_element" list="#listsort(sams.psc_code_string,'Text','ASC','~')#" delimiters="~">

						<cfquery name="psc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						 select psc_code, psc_description from psc
						 where psc_code = '#psc_element#'
						</cfquery>

						<cfoutput>
						 <b>#psc.psc_code#</b> - #psc.psc_description#<br>
						</cfoutput>

					   </cfloop>

	 			   </cfif>

                    </td></tr>

                    </table>

        		   </td></tr>

                 </table>

                 </cfif>

                 </cfoutput>

              </td></tr>
             </table>

           </td></tr>
         </table>
        </td></tr>
     </table>
   </div>

   </td></tr>
</table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>