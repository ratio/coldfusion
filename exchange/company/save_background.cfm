<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("remove_photo")>

	<cfquery name="remove" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  select company_banner from company
	  where company_id = #session.company_id#
	</cfquery>

	<cfif FileExists("#media_path#\#remove.company_banner#")>
	 <cffile action = "delete" file = "#media_path#\#remove.company_banner#">
	</cfif>

</cfif>

<cfif #company_banner# is not "">

	<cfquery name="getfile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  select company_banner from company
	  where company_id = #session.company_id#
	</cfquery>

	<cfif #getfile.company_banner# is not "">
	 <cfif FileExists("#media_path#\#getfile.company_banner#")>
	  <cffile action = "delete" file = "#media_path#\#getfile.company_banner#">
	 </cfif>
	</cfif>

	<cffile action = "upload"
	 fileField = "company_banner"
	 destination = "#media_path#"
	 nameConflict = "MakeUnique">

</cfif>

<cfquery name="usr" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 update company

  <cfif isdefined("remove_photo")>
   set company_banner = null
  <cfelse>
    <cfif #company_banner# is not "">
     set company_banner = '#cffile.serverfile#'
    <cfelse>
     set company_banner = null
    </cfif>
  </cfif>

where company_id = #session.company_id#
</cfquery>

<cflocation URL="/exchange/company/index.cfm?u=5" addtoken="no">