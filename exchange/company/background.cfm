<cfinclude template="/exchange/security/check.cfm">

<cfquery name="c_profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Change Banner</td></tr>
			   <tr><td colspan=2><hr></td></tr>

               <tr><td>

	             <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <form action="/exchange/company/save_background.cfm" method="post" enctype="multipart/form-data" >

				   <tr><td class="feed_sub_header" valign=top>Banner</td></tr>

				   <tr>

					<cfif #c_profile.company_banner# is "">
					  <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="company_banner"></td></tr>
					<cfelse>
					  <td class="feed_sub_header" style="font-weight: normal;"><img src="#media_virtual#/#c_profile.company_banner#" width=300><br><br>
					      <b>Change Photo</b><br><br>
					      <input type="file" name="company_banner"><br><br>
					      <input type="checkbox" name="remove_photo" style="width: 20px; height: 20px;">&nbsp;or, check to remove banner</td></tr>
					 </cfif>

                <tr><td height=10></td></tr>
                <tr><td class="link_small_gray">For best results, banner image should be 1,500 pixels wide and 300 pixels tall.</td></tr>
                <tr><td height=20></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

		        <tr><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10></td></tr>

			   </table>

			   </form>

                 </table>
           </td></tr>
         </table>
        </td></tr>
     </table>
   </div>

   </cfoutput>

   </td></tr>
</table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>