<cfinclude template="/exchange/security/check.cfm">

<cfif session.company_id is 0>
 <cflocation URL="/exchange/" addtoken="no">
</cfif>

<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 left join size on size_id = company_size_id
 left join entity on entity_id = company_entity_id
 left join usr on usr_id = company_admin_id
 where company_id = #session.company_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <cfoutput>

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Overview</td>
                   <td class="feed_sub_header" align=right>

			       <cfif session.update_access is 1>
	                   <a href="/exchange/company/update/"><img src="/images/icon_edit.png" width=20 hspace=10 alt="Update Company Profile" title="Update Company Profile"></a>
	                   <a href="/exchange/company/update/">Update Company Profile</a></td>
                   </cfif>

               </tr>
               <tr><td colspan=2><hr></td></tr>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Password successfully changed.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Profile successfully updated.</b></font></td></tr>
                <cfelseif u is 3>
                  <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Notification settings successfully updated.</b></font></td></tr>
                <cfelseif u is 4>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Company profile has been successfully created.</b></font></td></tr>
                <cfelseif u is 5>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Company banner has been successfully updated.</b></font></td></tr>
                <cfelseif u is 6>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Company profile has been successfully updated.</b></font></td></tr>
                <cfelseif u is 7>
                   <tr><td class="feed_sub_header" colspan=2><font color="green"><b>Company has been successfully registered.</b></font></td></tr>
               </cfif>
              </cfif>

               <tr><td colspan=2>

	             <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=200>Company Name</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       #company.company_name#
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" width=200>Tagline</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif company.company_tagline is "">Not provided<cfelse>#company.company_tagline#</cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>About</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif #company.company_about# is not "">
                        #replace(company.company_about,"#chr(10)#","<br>","all")#
                       <cfelse>
                        Not provided
                       </cfif>
                       </td></tr>


                  <tr>
                      <td class="feed_sub_header" valign=top>Full Description</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif #company.company_long_desc# is not "">
                        #replace(company.company_long_desc,"#chr(10)#","<br>","all")#
                       <cfelse>
                        Not provided
                       </cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150 valign=top>Keywords</td>
                      <td class="feed_sub_header" valign=top style="font-weight: normal;">
                       <cfif #company.company_keywords# is not "">
                       #company.company_keywords#
                       <cfelse>
                       Not provided
                       </cfif>
                       </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" width=150 valign=top>Products or Services Summary</td>
                      <td class="feed_sub_header" valign=top style="font-weight: normal;">
                       <cfif #company.company_product_summary# is not "">
                       #company.company_product_summary#
                       <cfelse>
                       Not provided
                       </cfif>
                       </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

                   <tr>
                      <td class="feed_sub_header" valign=top>Point of Contact</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                      <cfif #company.company_poc_first_name# is "" and #company.company_poc_last_name# is "">
                      	  Not disclosed
                      <cfelse>
						  #company.company_poc_first_name# #company.company_poc_last_name#<br>
						  <cfif #company.company_poc_title# is not "">#company.company_poc_title#<br></cfif>
						  #company.company_poc_phone#<br>
						  #company.company_poc_email#<br>
					  </cfif>
                       </td>
                   </tr>


                   <tr>
                      <td class="feed_sub_header" valign=top>Address</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <cfif #company_profile.company_address_l1# is not "">#company_profile.company_address_l1#<br></cfif>
						  <cfif #company_profile.company_address_l2# is not "">#company_profile.company_address_l2#<br></cfif>
						  #company_profile.company_city#, #company_profile.company_state#.  #company_profile.company_zip#
                       </td>
                   </tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>URLs</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <img src="/images/icon_home.png" width=20 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #company.company_website# is "">Not provided<cfelse><a href="#company.company_website#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#company.company_website#</a></cfif><br>
						  <img src="/images/icon_linkedin.png" width=20 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #company.company_linkedin_url# is "">Not provided<cfelse><a href="#company.company_linkedin_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#company.company_linkedin_url#</a></cfif><br>
						  <img src="/images/icon_twitter.png" width=20 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #company.company_twitter_url# is "">Not provided<cfelse><a href="#company.company_twitter_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#company.company_twitter_url#</a></cfif><br>
						  <img src="/images/icon_facebook.png" width=20 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #company.company_facebook_url# is "">Not provided<cfelse><a href="#company.company_facebook_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#company.company_facebook_url#</a></cfif>
                       </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Leadership Team</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif #company_profile.company_leadership# is not "">
                        #replace(company.company_leadership,"#chr(10)#","<br>","all")#
                       <cfelse>
                        Company leadership not provided
                       </cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Company History</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                       <cfif #company_profile.company_history# is not "">
                        #replace(company.company_history,"#chr(10)#","<br>","all")#
                       <cfelse>
                        Company background not provided
                       </cfif>
                       </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Founded</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <cfif #company_profile.company_founded# is "">Unknown<cfelse>#company_profile.company_founded#</cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>DUNS</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <cfif #company_profile.company_duns# is "">Unknown<cfelse>#company_profile.company_duns#</cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Stock Symbol</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <cfif #company_profile.company_stock_symbol# is "">Not provided<cfelse>#company_profile.company_stock_symbol#</cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Revenue</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
                      2018 - &nbsp;<cfif #company.company_fy18_revenue# is "">Not disclosed<cfelse>#numberformat(company.company_fy18_revenue,'$999,999,999')#</cfif><br>
                      2017 - &nbsp;<cfif #company.company_fy17_revenue# is "">Not disclosed<cfelse>#numberformat(company.company_fy17_revenue,'$999,999,999')#</cfif><br>
                      2016 - &nbsp;<cfif #company.company_fy16_revenue# is "">Not disclosed<cfelse>#numberformat(company.company_fy16_revenue,'$999,999,999')#</cfif>
                       </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=2><hr></td></tr>
                   <tr><td height=10></td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Type</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <cfif #company.entity_name# is "">Unknown<cfelse>#company.entity_name#</cfif>
                       </td></tr>

                  <tr>
                      <td class="feed_sub_header" valign=top>Size</td>
                      <td class="feed_sub_header" style="font-weight: normal;">
						  <cfif #company.size_name# is "">Unknown<cfelse>#company.size_name#</cfif>
                       </td></tr>

                  <tr>
                     <td colspan=2><hr></td></tr>
                  <tr><td height=10></td></tr>
                  <tr>
                     <td colspan=2 class="link_small_gray">The above Company Profile information was sourced and is managed through the Ratio Exchange.</td></tr>


              </table>
           </td></tr>
         </table>
        </td></tr>
     </table>
   </div>

   </cfoutput>

   </td></tr>
</table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>