<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<cfquery name="products_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from product
 where (product_company_id = #session.company_id#) and
       (product_id = #product_id#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Edit Product</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Name</b></td>
                    <td><input name="product_name" class="input_text" type="text" size=100 maxlength="100" value="#products_profile.product_name#" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
                    <td><textarea name="product_desc" class="input_textarea" rows=6 cols=101>#products_profile.product_desc#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Tags & Keywords</b></td>
                    <td><input name="product_keywords" class="input_text" type="text" size=100 maxlength="100" value="#products_profile.product_keywords#"></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Type</b></td>
                    <td class="feed_option">

                        <select name="product_type" class="input_select">
                         <option value="Product" <cfif #products_profile.product_type# is "Product">selected</cfif>>Product
                         <option value="Service" <cfif #products_profile.product_type# is "Service">selected</cfif>>Service
                         <option value="Product & Service" <cfif #products_profile.product_type# is "Both">selected</cfif>>Product & Service
                        </select>

                    </td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top><b>Why Choose This Product?</b></td>
                    <td><textarea name="product_pitch" class="input_textarea" rows=4 cols=101>#products_profile.product_pitch#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Key Differentiators?</b></td>
                    <td><textarea name="product_diff" class="input_textarea" rows=4 cols=101>#products_profile.product_diff#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Pricing Model?</b></td>
                    <td><textarea name="product_pricing" class="input_textarea" rows=3 cols=101>#products_profile.product_pricing#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>More Information (URL)</b></td>
                    <td><input name="product_url" class="input_text" type="url" size=100 maxlength="100" value="#products_profile.product_url#"></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Patent & Trademark Details</b></td>
					<td><textarea name="product_patent_details" class="input_textarea" rows=4 cols=101 placeholder="Describe details of patent">#products_profile.product_patent_details#</textarea></td>
		        </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Product or Service Logo / Picture</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #products_profile.product_attachment# is "">
					  <input type="file" name="product_attachment">
					<cfelse>
					  <img src="#media_virtual#/#products_profile.product_attachment#" width=150><br><br>
					  <input type="file" name="product_attachment"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					 </cfif>

					 </td></tr>

                 <input type="hidden" name="product_id" value=#product_id#>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="product_order" required class="input_text" style="width: 75px;" required type="number" step=".1" value="#products_profile.product_order#">&nbsp;&nbsp;Determines the order this product or service will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="product_public" style="width: 20px; height: 20px;" value=1 <cfif #products_profile.product_public# is 1>checked</cfif>>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update">
				     &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Product or Service?\r\nAre you sure you want to delete this product or service?');">
				</td></tr>


                 </cfoutput>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>