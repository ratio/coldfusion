<cfinclude template="/exchange/security/check.cfm">

<cfif session.company_id is 0>
 <cflocation URL="/exchange/" addtoken="no">
</cfif>

<cfquery name="prod" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from product
 where product_company_id = #session.company_id#
 order by product_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Products & Services<cfif #prod.recordcount# GT 0> (<cfoutput>#prod.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header">
			       <cfif session.update_access is 1>
                    <a href="/exchange/company/products/add.cfm?l=3"><img src="/images/plus3.png" align=absmiddle width=15 border=0 alt="Add Product or Service" title="Add Product or Service"></a>&nbsp;&nbsp;<a href="/exchange/company/products/add.cfm?l=3">Add Product or Service</a>
                   </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Product or Service has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Product or Service has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Product or Service has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif prod.recordcount is 0>
	               <tr><td class="feed_sub_header" style="font-weight: normal;">No Products or Services have been entered.</td></tr>
               <cfelse>

               <cfset count = 1>

			   <cfloop query="prod">

               <tr>
                   <td valign=top class="feed_sub_header" align=center width=175>

                   <cfoutput>

                   <cfif #prod.product_attachment# is "">
                   <img src="/images/icon_product_stock.png" width=150 vspace=5 border=0>
                   <cfelse>
                   <img src="#media_virtual#/#prod.product_attachment#" width=150 vspace=5 border=0>
                   </cfif>
                   <br><br>
                   #ucase(prod.product_type)#
                   <br><br>

                   </cfoutput>

                   <cfset list = #prod.product_topic#>

                   <cfif prod.product_topic is not "">

						<cfquery name="product_capability" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						 select topic_name from topic
						 where topic_id in (#list#)
						 order by topic_name
						</cfquery>

						<cfset #p_list# = #valuelist(product_capability.topic_name)#>

						<span class="feed_option"><cfoutput>#replace(p_list,",","<br>","all")#</cfoutput></span>

                   </cfif>

                   </td>

				   <td width=40>&nbsp;</td><td valign=top>

                   <cfoutput>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=middle>#ucase(prod.product_name)#</td>
				       <td align=right class="feed_sub_header">
				       <cfif #session.update_access# is 1>
				       <a href="edit_product.cfm?l=3&product_id=#prod.product_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit" title="Edit" valign=middle></a>&nbsp;&nbsp;<a href="edit_product.cfm?l=3&product_id=#prod.product_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <tr><td colspan=2><hr></td></tr>

                   <cfif #prod.product_desc# is not "">
					   <tr><td class="feed_sub_header" valign=top>Description</td></tr>
				       <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>#replace(prod.product_desc,"#chr(10)#","<br>","all")#</td></tr>
                   </cfif>

                   <cfif #prod.product_pitch# is not "">
					   <tr><td class="feed_sub_header" valign=top>Why Choose This Product</td></tr>
				       <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #prod.product_pitch# is "">Not provided<cfelse>#replace(prod.product_pitch,"#chr(10)#","<br>","all")#</cfif></td></tr>
                   </cfif>

                   <cfif #prod.product_diff# is not "">
				       <tr><td class="feed_sub_header" valign=top>Key Differentiators</td></tr>
				       <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #prod.product_diff# is "">Not provided<cfelse>#replace(prod.product_diff,"#chr(10)#","<br>","all")#</cfif></td></tr>
                   </cfif>

                   <cfif #prod.product_patent_details# is not "">
                       <tr><td colspan=2 class="feed_sub_header">Patent & Trademark Information</td></tr>
				       <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #prod.product_patent_details# is "">Not provided<cfelse>#replace(prod.product_patent_details,"#chr(10)#","<br>","all")#</cfif></td></tr>
                   </cfif>

                   <cfif #product_url# is not "">
                       <tr><td colspan=2 class="feed_sub_header">More Information</td></tr>
	                   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><a href="#product_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;"><u>#prod.product_url#</u></a></td></tr>
                   </cfif>

                 </table>

                 </cfoutput>

                 </td></tr>

				  <cfif count is not prod.recordcount>
				   <tr><td height=20></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfloop>

				</cfif>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 	   </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>