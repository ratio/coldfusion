<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Add Product or Service</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" width=20%><b>Name</b></td>
                    <td><input name="product_name" class="input_text" type="text" size=100 maxlength="100" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
                    <td><textarea name="product_desc" class="input_textarea" rows=6 cols=101 placeholder="Please provide a brief overview of your product or service."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Tags & Keywords</b></td>
                    <td><input name="product_keywords" class="input_text" type="text" size=100 maxlength="100" placeholder="Machine Learning, Intelligence, etc."></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Type</b></td>
                    <td class="feed_option">

                        <select name="product_type" class="input_select">
                         <option value="Product">Product
                         <option value="Service">Service
                         <option value="Product & Service">Both
                        </select>

                    </td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top><b>Why Choose This Product?</b></td>
                    <td><textarea name="product_pitch" class="input_textarea" rows=4 cols=101 placeholder="Describe why customers or partners would choose this product over others."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Key Differentiators?</b></td>
                    <td><textarea name="product_diff" class="input_textarea" rows=4 cols=101 placeholder="Describe the key features for why this product or service is better than others."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Pricing Model?</b></td>
                    <td><textarea name="product_pricing" class="input_textarea" rows=3 cols=101 placeholder="How do customers or partners buy your product or service (i.e., subscription, fixed price, licenses, etc.)"></textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>More Information (URL)</b></td>
                    <td><input name="product_url" class="input_text" type="url" size=100 maxlength="100" placeholder="http://www..."></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Patent Details</b></td>

					<td><textarea name="product_patent_details" class="input_textarea" rows=4 cols=101 placeholder="Describe details of patent"></textarea></td>
		        </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Product or Service Logo / Picture</td>
                    <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="product_attachment"></td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="product_order" required class="input_text" style="width: 75px;" required type="number" step=".1">&nbsp;&nbsp;Determines the order this product or service will appear in your profile.</td>                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="product_public" style="width: 20px; height: 20px;" value=1>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Product or Service"></td></tr>


                 </cfoutput>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>