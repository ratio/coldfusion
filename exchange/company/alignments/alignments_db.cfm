<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update Alignments">

<cftransaction>

	<cfquery name="delete_alignments" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete align
	 where (align_type_id in (2,3,4)) and
	       (align_company_id = #session.company_id#)
	       <cfif isdefined("session.hub")>
	        and align_hub_id = #session.hub#
	       <cfelse>
	        and align_hub_id is null
	       </cfif>
	</cfquery>

	<cfif isdefined("market_id")>

		<cfloop index="m_element" list=#market_id#>

			<cfquery name="insert_market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.company_id#, 2, #m_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("sector_id")>

		<cfloop index="s_element" list=#sector_id#>

			<cfquery name="insert_sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.company_id#, 3, #s_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

	<cfif isdefined("topic_id")>

		<cfloop index="t_element" list=#topic_id#>

			<cfquery name="insert_topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 Insert into align
			 (align_company_id, align_type_id, align_type_value, align_hub_id)
			 Values
			 (#session.company_id#, 4, #t_element#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
			</cfquery>

		</cfloop>

	</cfif>

</cftransaction>

</cfif>

<cflocation URL="/exchange/company/alignments/index.cfm?l=2&u=1" addtoken="no">