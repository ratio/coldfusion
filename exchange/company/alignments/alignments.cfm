<cfinclude template="/exchange/security/check.cfm">

  <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from market
   	 <cfif isdefined("session.hub")>
	  where market_hub_id = #session.hub#
	 <cfelse>
	  where market_hub_id is null
	 </cfif>
  </cfquery>

  <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from sector
   	 <cfif isdefined("session.hub")>
	  where sector_hub_id = #session.hub#
	 <cfelse>
	  where sector_hub_id is null
	 </cfif>
   order by sector_name
  </cfquery>

  <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from topic
   	 <cfif isdefined("session.hub")>
	  where topic_hub_id = #session.hub#
	 <cfelse>
	  where topic_hub_id is null
	 </cfif>
   order by topic_name
  </cfquery>

	  <cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_company_id = #session.company_id#) and
	          <cfif isdefined("session.hub")>
	           align_hub_id = #session.hub# and
	          <cfelse>
	           align_hub_id is null and
	          </cfif>
			 (align_type_id = 2)
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_company_id = #session.company_id#) and
	          <cfif isdefined("session.hub")>
	           align_hub_id = #session.hub# and
	          <cfelse>
	           align_hub_id is null and
	          </cfif>
			 (align_type_id = 3)
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_company_id = #session.company_id#) and
	          <cfif isdefined("session.hub")>
	           align_hub_id = #session.hub# and
	          <cfelse>
	           align_hub_id is null and
	          </cfif>
			 (align_type_id = 4)
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
	  <cfinclude template="/exchange/profile_company.cfm">

	  </td><td valign=top>

	  <div class="main_box">

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">Edit Alignments</td>
			        <td align=right><a href="/exchange/company/"><img src="/images/delete.png" border=0 width=20></a></td></tr>
			   <tr><td class="feed_option">Please align your company to the lists below.  Use the CTRL button to select multiple values.</td></tr>
			   <tr><td>&nbsp;</td></tr>
		      </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="alignments_db.cfm" method="post">

        <tr><td colspan=2>

			<table cellspacing=0 cellpadding=0 border=0>

			<tr><td class="feed_sub_header"><b>Markets Supported</b></td>
				<td class="feed_sub_header"><b>Sector Focus</b></td>
				<td class="feed_sub_header"><b>Capabilities & Services</b></td>
				</tr>
		    <tr><td height=10></td></tr>
			<tr>

				 <td width=180>
					  <select style="font-size: 12px; width: 160px; height: 260px; padding-left: 10px; padding-top: 5px;" name="market_id" multiple required>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=220>
					  <select style="font-size: 12px; width: 200px; height: 260px; padding-left: 10px; padding-top: 5px;" name="sector_id" multiple required>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select style="font-size: 12px; width: 275px; height: 260px; padding-left: 10px; padding-top: 5px;" name="topic_id" multiple required>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
				<tr><td colspan=3><input class="button_blue" type="submit" name="button" value="Update" vspace=10></td></tr>

              </form>

			  </table>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

