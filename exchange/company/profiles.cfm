<cfinclude template="/exchange/security/check.cfm">

<cfquery name="ass" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_comp_company_id from usr_comp
 where usr_comp_usr_id = #session.usr_id#
</cfquery>

<cfif ass.recordcount is 0>
 <cfset list = 0>
<cfelse>
 <cfset list = valuelist(ass.usr_comp_company_id)>
</cfif>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id in (#list#)
 order by company_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.comp_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
	  <td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td>

      <td valign=top width=100%>

	  <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_header">Company Associations</td>
		    <td align=right></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td class="feed_sub_header" style="font-weight: normal;">You are associated with mutliple Companies on the Exchange.  To change your association, please select the Company below.</td></tr>

        <cfif isdefined("u")>
         <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">You are now associated with this Company.</td></tr>
         <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: green;">You are not or no longer associated with the selected Company.</td></tr>
         </cfif>

         <tr><td height=20></td></tr>
        <cfelse>
         <tr><td height=20></td></tr>
        </cfif>

	   </table>

       <cfloop query="companies">

            <cfoutput>

			<div class="comp_badge">

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=10></td></tr>

                 <tr><td valign=top width=100>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

                      <tr><td>

                      <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">

                      <cfif companies.company_logo is "">
					   <img src="//logo.clearbit.com/#companies.company_website#" width=80 border=0 onerror="this.src='/images/no_logo.png'">
					  <cfelse>
                       <img src="#image_virtual#/#companies.company_logo#" width=80 border=0>
					  </cfif>

					  </a>

					  </td></tr>

					 </table>

				 <td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

			     <tr><td class="feed_sub_header"><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank">#companies.company_name#</a></td></tr>
				 <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">

				 <cfif companies.company_city is "" and companies.company_state is "">
				  Location Not Provided
				 <cfelse>
				 #companies.company_city# #companies.company_state#
				 </cfif>

				 </td></tr>
				 <tr><td class="feed_option" style="font-weight: normal; padding-top: 0px;">

				 <cfif companies.company_website is "">
				  Website Not Provided
				 <cfelse>
				 <a href="#companies.company_website#" target="_blank"><u>#companies.company_website#</u></a>
				 </cfif>
				 </td></tr>

				</table>

				</td></tr>

				<tr><td height=15></td></tr>
				<tr><td colspan=2 align=center>

				<cfif session.company_id is companies.company_id>
					<img src="/images/comp_selected.png" height=30>
				<cfelse>
					<a href="select.cfm?id=#companies.company_id#" onclick="return confirm('Associate with Company?\r\nAre you sure you want to associate with this Company?');"><img src="/images/comp_not_selected.png" height=30 alt="Select Company" title="Select Company"></a>
				</cfif>

				</td></tr>

				</table>

			</div>

	   	   </cfoutput>

	   	   </cfloop>

     </div>

     </td>

     </tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

