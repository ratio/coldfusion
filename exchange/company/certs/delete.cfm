<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select product_attachment from product
	  where (product_id = #product_id#) and
			(product_company_id = #session.company_id#)
	</cfquery>

	<cfif remove.product_attachment is not "">
	 <cffile action = "delete" file = "#media_path#\#remove.product_attachment#">
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete product
	  where (product_id = #product_id#) and
			(product_company_id = #session.company_id#)
	</cfquery>

</cftransaction>

<cflocation URL="/exchange/company/products/index.cfm?l=3&u=2" addtoken="no">