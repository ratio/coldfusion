<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from cert
 where cert_company_id = #session.company_id#
 order by cert_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Certifications<cfif #cert.recordcount# GT 0> (<cfoutput>#cert.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header">
			       <cfif session.update_access is 1>
                    <a href="/exchange/company/certs/add.cfm?l=11"><img src="/images/plus3.png" width=15 border=0 alt="Add Certification" title="Add Certification" valign=middle></a>&nbsp;&nbsp;<a href="/exchange/company/certs/add.cfm?l=11">Add Certification</a>
                   </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Certification has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Certification has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Certification has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif cert.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">No Certifications have been entered.</td></tr>

               <cfelse>

               <cfset count = 1>

			   <cfloop query="cert">

               <tr>
                   <td valign=top class="feed_sub_header" align=center width=175>

                   <cfoutput>

                   <cfif #cert.cert_attachment# is "">
                   <img src="/images/icon_product_stock.png" width=150 vspace=5 border=0>
                   <cfelse>
                   <img src="#media_virtual#/#cert.cert_attachment#" width=150 vspace=5 border=0>
                   </cfif>

                   </cfoutput>

                   </td>

				   <td width=40>&nbsp;</td><td valign=top>

                   <cfoutput>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=middle>#ucase(cert.cert_name)#</td>
				       <td align=right class="feed_sub_header">
				       <cfif session.update_access is 1>
				       <a href="edit.cfm?l=11&cert_id=#cert.cert_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit" title="Edit" valign=middle></a>&nbsp;&nbsp;<a href="edit.cfm?l=11&cert_id=#cert.cert_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <tr><td colspan=2><hr></td></tr>

				   <tr><td class="feed_sub_header" valign=top>Description</td></tr>
			       <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>#replace(cert.cert_desc,"#chr(10)#","<br>","all")#</td></tr>
                   <tr><td height=5></td></tr>
			       <tr><td class="link_small_gray" colspan=2 valign=top>
			       <cfif cert.cert_date_acquired is "">
			       No Certification Date Provided
			       <cfelse>
			       Date Certified - #dateformat(cert.cert_date_acquired,'mmmm dd, yyyy')#
			       </cfif></td></tr>

                 </table>

                 </cfoutput>

                 </td></tr>

				  <cfif count is not cert.recordcount>
				   <tr><td height=20></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfloop>

				</cfif>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 	   </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>