<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<cfquery name="capability" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from topic
 <cfif isdefined("session.hub")>where topic_hub_id = #session.hub#
 <cfelse>
 where topic_hub_id is null
 </cfif>
 order by topic_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Add Certification</td>
			       <td align=right class="feed_sub_header"><a href="index.cfm?l=11">Return</a>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header" width=20%><b>Certification</b></td>
                    <td><input name="cert_name" class="input_text" type="text" size=100 maxlength="100" required></td>
                </tr>

                <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
                    <td><textarea name="cert_desc" class="input_textarea" rows=6 cols=101 placeholder="Please provide a brief overview of this certification."></textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Tags & Keywords</b></td>
                    <td><input name="cert_keywords" class="input_text" type="text" size=100 maxlength="1000" placeholder="ISO9000, CMMI, etc."></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Certification Date</b></td>
                    <td><input name="cert_date_acquired" type="date" class="input_date" type="text"></td>
                </tr>

                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top>Certification Logo / Picture</td>
                    <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="cert_attachment"></td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="cert_order" required class="input_text" style="width: 75px;" required type="number" step=".1">&nbsp;&nbsp;Determines the order this product or service will appear in your profile.</td>                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="cert_public" style="width: 20px; height: 20px;" value=1>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Certification"></td></tr>

               </table>

			   </form>

               </td></tr>

          </table>


         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>