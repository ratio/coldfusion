<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Press or Media">

	<cfif #media_image# is not "">
		<cffile action = "upload"
		 fileField = "media_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert media
      (media_image, media_order, media_public, media_source, media_date, media_name, media_desc, media_desc_long, media_url, media_company_id, media_updated)
      values
      (<cfif #media_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,#media_order#,<cfif isdefined("media_public")>1<cfelse>null</cfif>,'#media_source#','#media_date#','#media_name#','#media_desc#', '#media_desc_long#', '#media_url#',#session.company_id#, #now()#)
    </cfquery>

    <cflocation URL="/exchange/company/media/index.cfm?l=7&u=3" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select media_image from media
	  where (media_id = #media_id#) and
	        (media_company_id = #session.company_id#)
	</cfquery>

    <cfif remove.media_image is not "">
     <cfif fileexists("#media_path#\#remove.media_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.media_image#">
     </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete media
	  where (media_id = #media_id#) and
	        (media_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/media/index.cfm?l=7&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select media_image from media
		  where media_id = #media_id#
		</cfquery>

	     <cfif fileexists("#media_path#\#remove.media_image#")>
			<cffile action = "delete" file = "#media_path#\#remove.media_image#">
		 </cfif>

	</cfif>

	<cfif #media_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select media_image from media
		  where media_id = #media_id#
		</cfquery>

		<cfif #getfile.media_image# is not "">
			<cfif fileexists("#media_path#\#getfile.media_image#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.media_image#">
			</cfif>
	    </cfif>

		<cffile action = "upload"
		 fileField = "media_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update media
	  set media_name = '#media_name#',
	      media_desc = '#media_desc#',

		  <cfif #media_image# is not "">
		   media_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   media_image = null,
		  </cfif>

	      media_order = #media_order#,
	      media_public = <cfif isdefined("media_public")>1<cfelse>null</cfif>,
	      media_date = '#media_date#',
	      media_source = '#media_source#',
	      media_desc_long = '#media_desc_long#',
	      media_url = '#media_url#',
	      media_updated = #now()#
	      where (media_id = #media_id#) and
	            (media_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/media/index.cfm?l=7&u=1" addtoken="no">

</cfif>