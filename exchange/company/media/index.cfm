<cfinclude template="/exchange/security/check.cfm">

<cfquery name="medias" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from media
 where media_company_id = #session.company_id#
 order by media_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Press & Media<cfif #medias.recordcount# GT 0> (<cfoutput>#medias.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header">
                    <a href="/exchange/company/media/add.cfm?l=4" valign=middle>
                    <cfif session.update_access is 1>
                    <img src="/images/plus3.png" width=15 valign=middle border=0 alt="Add Press or Media" title="Add Press or Media"></a>&nbsp;&nbsp;<a href="/exchange/company/media/add.cfm?l=7">Add Press or Media</a>
                    </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>Press or Media has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>Press or Media has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>Press or Media has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif medias.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">No Press or Media information has been entered.</td></tr>

               <cfelse>

               <cfset count = 1>

			   <cfoutput query="medias">

               <tr>
                   <td valign=top class="feed_sub_header" width=165 align=center>

                   <cfif #medias.media_image# is "">
                   <img src="/images/no_logo.png" width=150 vspace=5 border=0>
                   <cfelse>
                   <img src="#media_virtual#/#media_image#" width=150 vspace=5 border=0>
                   </cfif>

                   <br><br>

                   <font size=3>#media_source#</font>

                   </td>

				   <td width=40>&nbsp;</td><td valign=top>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=top colspan=2>#media_name#</td>
				       <td align=right class="feed_sub_header">
				       <cfif session.update_access is 1>
				       <a href="/exchange/company/media/edit.cfm?l=7&media_id=#media_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Press or Media" title="Edit Press or Media"></a>&nbsp;&nbsp;<a href="/exchange/company/media/edit.cfm?l=7&media_id=#media_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <tr><td class="feed_option" colspan=3 style="font-weight: normal;" valign=top><b>#dateformat(media_date,'mmmm dd, yyyy')#</b></td></tr>
				   <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;" valign=top>#replace(media_desc,"#chr(10)#","<br>","all")#</td></tr>

				   <cfif #media_url# is not "">
				   	<tr><td class="feed_option" colspan=3 style="font-weight: normal;" valign=top><i><a href="#media_url#" target="_blank" rel="noopener" rel="noreferrer">#media_url#</a></i></td></tr>
				   </cfif>

                 </table>

                 </td></tr>

				  <cfif count is not medias.recordcount>
				   <tr><td height=10></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

				</cfif>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>