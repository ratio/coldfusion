<cfquery name="cert_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(cert_id) as total from cert
 where cert_company_id = #session.company_id#
</cfquery>

<cfquery name="product_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(product_id) as total from product
 where product_company_id = #session.company_id#
</cfquery>

<cfquery name="customer_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(customer_id) as total from customer
 where customer_company_id = #session.company_id#
</cfquery>

<cfquery name="partner_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(partner_id) as total from partner
 where partner_company_id = #session.company_id#
</cfquery>

<cfquery name="media_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(media_id) as total from media
 where media_company_id = #session.company_id#
</cfquery>

<cfquery name="marketing_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(doc_id) as total from doc
 where doc_company_id = #session.company_id#
</cfquery>

<cfquery name="news_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(news_id) as total from news
 where news_company_id = #session.company_id#
</cfquery>

<cfquery name="users_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(usr_id) as total from usr
 where (usr_company_id = #session.company_id#) and
       (usr_id <> #session.usr_id#)
</cfquery>

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 left join size on size_id = company_size_id
 left join entity on entity_id = company_entity_id
 left join usr on usr_id = company_admin_id
 where company_id = #session.company_id#
</cfquery>

<cfset score_total = 37>
<cfset score = 0>

<!--- 1 --->

<cfif #company.company_keywords# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 2 --->

<cfif #company.company_about# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 3 --->

<cfif #company.company_tagline# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 4 --->

<cfif #company.company_city# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 5 --->

<cfif #company.company_state# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 6 --->

<cfif #company.company_zip# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 7 --->

<cfif #company.company_website# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 8 --->

<cfif #company.company_logo# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 9 --->

<cfif #company.company_history# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 10 --->

<cfif #company.company_leadership# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 11 --->

<cfif #company.company_founded# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 12 --->

<cfif #company.company_duns# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 13--->

<cfif #company.company_poc_phone# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 14 --->

<cfif #company.company_poc_email# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 15 --->

<cfif #company.company_twitter_url# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 16 --->

<cfif #company.company_linkedin_url# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 17 --->

<cfif #company.company_facebook_url# is not "">
 <cfset score = score + 1>
</cfif>

<!--- 18-22 --->

<cfif #product_count.total# is not 0>
 <cfset score = score + 5>
</cfif>

<!--- 23-27 --->

<cfif #customer_count.total# is not 0>
 <cfset score = score + 5>
</cfif>

<!--- 28-32 --->

<cfif #marketing_count.total# is not 0>
 <cfset score = score + 5>
</cfif>

<!--- 33-37 --->

<cfif #partner_count.total# is not 0>
 <cfset score = score + 5>
</cfif>

<!--- Set Percentage Complete --->

<cfset #score_percent# = #round(evaluate((score/score_total)*100))#>
