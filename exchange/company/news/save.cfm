<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add News">

	<cfif #news_image# is not "">
		<cffile action = "upload"
		 fileField = "news_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert news
      (news_image, news_order, news_public, news_date, news_title, news_desc, news_url, news_company_id, news_created, news_hub_id)
      values
      (<cfif #news_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,#news_order#,<cfif isdefined("news_public")>1<cfelse>null</cfif>,'#news_date#','#news_title#','#news_desc#', '#news_url#',#session.company_id#, #now()#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>)
    </cfquery>

    <cflocation URL="/exchange/company/news/index.cfm?l=10&u=3" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select news_image from news
	  where (news_id = #news_id#) and
	        (news_company_id = #session.company_id#)
	</cfquery>

    <cfif remove.news_image is not "">
	 <cffile action = "delete" file = "#media_path#\#remove.news_image#">
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete news
	  where (news_id = #news_id#) and
	        (news_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/news/index.cfm?l=10&u=2" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select news_image from news
		  where news_id = #news_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.news_image#">

	</cfif>

	<cfif #news_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select news_image from news
		  where news_id = #news_id#
		</cfquery>

		<cfif #getfile.news_image# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.news_image#">
		</cfif>

		<cffile action = "upload"
		 fileField = "news_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update news
	  set news_title = '#news_title#',
	      news_desc = '#news_desc#',

		  <cfif #news_image# is not "">
		   news_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   news_image = null,
		  </cfif>

	      news_order = #news_order#,
	      news_public = <cfif isdefined("news_public")>1<cfelse>null</cfif>,
	      news_date = '#news_date#',
	      news_url = '#news_url#',
	      news_updated = #now()#
	      where (news_id = #news_id#) and
	            (news_company_id = #session.company_id#)
	</cfquery>

    <cflocation URL="/exchange/company/news/index.cfm?l=10&u=1" addtoken="no">

</cfif>