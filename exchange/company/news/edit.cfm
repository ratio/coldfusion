<cfinclude template="/exchange/security/check.cfm">

<cfif session.update_access is not 1>
 <cflocation URL="/exchange/">
</cfif>

<cfquery name="news_profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from news
 where (news_company_id = #session.company_id#) and
       (news_id = #news_id#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;">Edit Profile News</td>
			       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>

			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td height=10></td></tr>

               <tr><td>

           <form action="save.cfm" method="post" enctype="multipart/form-data" >

               <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr><td class="feed_sub_header"><b>Title</b></td>
                    <td><input name="news_title" class="input_text" type="text" size=100 maxlength="100" value="#news_profile.news_title#" required></td>
                </tr>


                <tr><td class="feed_sub_header" valign=top>News Image / Banner</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #news_profile.news_image# is "">
					  <input type="file" name="news_image">
					<cfelse>
					  <img src="#media_virtual#/#news_profile.news_image#" width=150><br><br>
					  <input type="file" name="news_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo
					 </cfif>

					</td></tr>

			    <tr><td></td><td class="link_small_gray">Will be displayed to the left of the Title.</td></tr>
			    <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" valign=top><b>Summary</b></td>
                    <td><textarea name="news_desc" class="input_textarea" rows=4 cols=101 placeholder="Please provide an beief summary of your news.">#news_profile.news_desc#</textarea></td>
                </tr>

                <tr><td class="feed_sub_header"><b>Date</b></td>
                    <td><input name="news_date" class="input_date" type="date" size=15 value="#news_profile.news_date#" required></td>
                </tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header"><b>Reference URL</b></td>
                    <td><input name="news_url" class="input_text" type="url" size=100 maxlength="300" value="#news_profile.news_url#" placeholder="http://www..."></td>
                </tr>

                <input type="hidden" name="news_id" value=#news_id#>

                <tr><td class="feed_sub_header"><b>Display Order</b></td>
                    <td class="feed_option"><input name="news_order" required class="input_text" style="width: 75px;" required type="number" step=".1" value="#news_profile.news_order#">&nbsp;&nbsp;Determines the order this news will appear in your profile.

                    </td>
                </tr>

			    <tr><td class="feed_sub_header">Privacy</td>
			        <td class="feed_option">
			            <input type="checkbox" name="news_public" style="width: 20px; height: 20px;" value=1 <cfif #news_profile.news_public# is 1>checked</cfif>>

			            &nbsp;&nbsp;&nbsp;&nbsp;Display this information publicly?

			            </td></tr>

                <tr><td height=10></td></tr>
                <tr><td colspan=2><hr></td></tr>
                <tr><td height=10></td></tr>

				<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update">
				     &nbsp;&nbsp;<input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete News?\r\nAre you sure you want to delete this record?');">
				</td></tr>

                </cfoutput>

               </table>

			   </form>

               </td></tr>

             </table>

           </td></tr>

         </table>

 		</div>

	 </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>