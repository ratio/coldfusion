<cfinclude template="/exchange/security/check.cfm">

<cfquery name="news" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from news
 where news_company_id = #session.company_id#
 order by news_order
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/company/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/company/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header" style="font-size: 30;" valign=middle>Profile News<cfif #news.recordcount# GT 0> (<cfoutput>#news.recordcount#</cfoutput>)</cfif></td>
			       <td align=right class="feed_sub_header">
                    <a href="/exchange/company/media/add.cfm?l=4" valign=middle>
                    <cfif session.update_access is 1>
                    <img src="/images/plus3.png" width=15 valign=middle border=0 alt="Add News" title="Add News"></a>&nbsp;&nbsp;<a href="/exchange/company/news/add.cfm?l=10">Add News</a>
                    </cfif>
			       </td></tr>
			   <tr><td colspan=2><hr></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif isdefined("u")>
                <cfif u is 1>
                 <tr><td class="feed_sub_header"><font color="green"><b>News has been successfully updated.</b></font></td></tr>
                <cfelseif u is 2>
                 <tr><td class="feed_sub_header"><font color="green"><b>News has been successfully deleted.</b></font></td></tr>
                <cfelseif u is 3>
                 <tr><td class="feed_sub_header"><font color="green"><b>News has been successfully added.</b></font></td></tr>
                </cfif>
               </cfif>

			   <tr><td height=10></td></tr>

			   </table>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif news.recordcount is 0>

                <tr><td class="feed_sub_header" style="font-weight: normal;">No News has been entered.</td></tr>

               <cfelse>

               <cfset count = 1>

			   <cfoutput query="news">

               <tr>
                   <td valign=top class="feed_sub_header" width=165 align=center>

                   <cfif #news_image# is "">
                   <img src="/images/no_logo.png" width=150 vspace=5 border=0>
                   <cfelse>
                   <img src="#media_virtual#/#news_image#" width=150 vspace=5 border=0>
                   </cfif>

                   </td>

				   <td width=40>&nbsp;</td><td valign=top>

		           <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr><td height=10></td></tr>
				   <tr><td class="feed_header" valign=top colspan=2>#news_title#</td>
				       <td align=right class="feed_sub_header">
				       <cfif session.update_access is 1>
				       <a href="/exchange/company/news/edit.cfm?l=10&news_id=#news_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit News" title="Edit News"></a>&nbsp;&nbsp;<a href="/exchange/company/news/edit.cfm?l=10&news_id=#news_id#">Edit</a>
				       </cfif>
				       </td></tr>
				   <tr><td class="feed_option" colspan=3 style="font-weight: normal;" valign=top><b>#dateformat(news_date,'mmmm dd, yyyy')#</b></td></tr>
				   <tr><td class="feed_sub_header" colspan=3 style="font-weight: normal;" valign=top>#replace(news_desc,"#chr(10)#","<br>","all")#</td></tr>

				   <cfif #news_url# is not "">
				   	<tr><td class="feed_option" colspan=3 style="font-weight: normal;" valign=top><i><a href="#news_url#" target="_blank" rel="noopener" rel="noreferrer">#news_url#</a></i></td></tr>
				   </cfif>

                 </table>

                 </td></tr>

				  <cfif count is not news.recordcount>
				   <tr><td height=10></td></tr>
				   <tr><td colspan=3><hr></td></tr>
				   <tr><td height=10></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

				</cfif>

              </table>

          </td></tr>

          </table>

         </td></tr>

       </table>

 		  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>