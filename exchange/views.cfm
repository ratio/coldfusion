<cfinclude template="/exchange/security/check.cfm">

<cfquery name="company_views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	SELECT CAST(MONTH(cview_date) AS VARCHAR(2)) + '-' + CAST(YEAR(cview_date) AS VARCHAR(4)) AS view_date, month(cview_date) as month, year(cview_date) as date, count(cview_id) AS total
	FROM cview
	WHERE cview_company_id = #session.company_id# and
	      cview_hub_id = #session.hub#
	GROUP BY CAST(MONTH(cview_date) AS VARCHAR(2)) + '-' + CAST(YEAR(cview_date) AS VARCHAR(4)), month(cview_date), year(cview_date)
	order by year(cview_date) ASC, month(cview_date) ASC
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
	      <cfinclude template="/exchange/components/company_profile/index.cfm">

      </td><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Company Views</td><td align=right class="feed_option"></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

          <cfif company_views.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_sub_header" style="font-weight: normal;">You have no company views.</td></tr>
          </table>

          <cfelse>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=30></td></tr>

           <tr><td>

			 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">
				  google.charts.load('current', {'packages':['line']});
				  google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Month-Year', 'Views'],
						  <cfoutput query="company_views">
						   ['#view_date#',#round(total)#],
						  </cfoutput>
						]);


			  var options = {
					width: 1300,
					height: 175,
					legend: { position: 'none' }
				  };

				  var chart = new google.charts.Line(document.getElementById('line_top_x'));
				  chart.draw(data, google.charts.Line.convertOptions(options));
				}
			  </script>

			<div id="line_top_x"></div>

			</td></tr>

			</table>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=20></td></tr>

			<cfquery name="view_detail" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				select * from cview
				join usr on usr_id = cview_by_usr_id
				where cview_company_id = #session.company_id# and
				      cview_hub_id = #session.hub#
				order by cview_date DESC
			</cfquery>

            <tr><td valign=top>

		    <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr><td class="feed_header">View Detail</td></tr>
             <tr><td height=20></td></tr>

             <tr>
                 <td class="feed_sub_header">Member</td>
                 <td class="feed_sub_header">Company</td>
                 <td class="feed_sub_header" align=right>Date / Time</td>
             </tr>

             <cfset counter = 0>
             <cfoutput query="view_detail">
              <cfif counter is 0>
              <tr bgcolor="ffffff">
              <cfelse>
              <tr bgcolor="e0e0e0">
              </cfif>

                 <cfif usr_profile_display is 2>
                 <td class="feed_sub_header" width=250>Private Profile</td>
                 <cfelse>
                 <td class="feed_sub_header" width=250><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#usr_first_name# #usr_last_name#</a></td>
                 </cfif>

                 <td class="feed_sub_header" style="font-weight: normal;">#usr_company_name#</td>

                 <td class="feed_sub_header" style="font-weight: normal;" align=right>#dateformat(cview_date,'mm/dd/yyyy')# at #timeformat(cview_date)#</td>
              </tr>
              <cfif counter is 0>
               <cfset counter = 1>
              <cfelse>
               <cfset counter = 0>
              </cfif>
             </cfoutput>

            </table>

            </td></tr>

            </table>

            </cfif>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

