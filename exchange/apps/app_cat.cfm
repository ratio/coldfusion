<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.app_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

  <cfquery name="category" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from app_category
   where app_category_id = #i#
  </cfquery>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <cfoutput>

	   <tr><td class="feed_header">App Library - #category.app_category_name#</td>
	       <td class="feed_sub_header" align=right><a href="index.cfm">All Apps</a></td></tr>
	   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">#category.app_category_desc#</td></tr>
	   <tr><td colspan=2><hr></td></tr>

	   </cfoutput>

	   </table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td height=20></td></tr>

	  </table>

	  <cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from app
	   join app_category on app_category.app_category_id = app.app_category_id
	   where app_active = 1 and
	         app.app_category_id = #i#
	   order by app_name
	  </cfquery>

	  <cfif apps.recordcount is 0>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">No apps were found.</td></tr>
      </table>

	  <cfelse>

      <cfoutput query="apps">

	  <div class="app_badge">
	   <cfinclude template="app_badge.cfm">
	  </div>

	  </cfoutput>

	  </cfif>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>