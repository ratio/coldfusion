<cfinclude template="/exchange/security/check.cfm">

<cfquery name="installed_apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from app
  join hub_app on hub_app_app_id = app_id
  left join app_category on app_category.app_category_id = app.app_category_id
  where hub_app_hub_id = #session.hub#
  order by app_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/apps/menu.cfm">

	  </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">MY APPS</td>
	       <td class="feed_sub_header" align=right></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif #installed_apps.recordcount# is 0>

        <tr><td class="feed_sub_header" style="font-weight: normal;">You have not installed any Apps.  To find and install an App, please visit the <a href="apps.cfm">App Library</a>.</td></tr>

        <cfelse>

	   <tr><td colspan=4 class="feed_sub_header" style="font-weight: normal;">The following Apps are installed in your Exchange.</td></tr>
       <tr>

				<td>
				</td>
				<td class="feed_sub_header" width=300>App Name</b></td>
				<td class="feed_sub_header">Description</b></td>
				<td class="feed_sub_header">Category</b></td>
			</tr>

        </cfif>

        <cfset counter = 0>

         <cfoutput query="installed_apps">

          <cfif #counter# is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="f0f0f0">
          </cfif>

             <td width=100>
			 <cfif #app_image# is "">
			  <img src="/images/app.png" height=50 width=50 border=0 vspace=5>
			 <cfelse>
			  <img style="border-radius: 0px;" src="/images/#app_image#" vspace=5 height=50 width=50 border=0>
			 </cfif>
			 </td>

              <td class="feed_sub_header">#app_name#</a></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif app_desc_short is "">No description provided<cfelse>#app_desc_short#</cfif></td>
              <td class="feed_sub_header" valign=top style="font-weight: normal;">#app_category_name#</td>
          </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

         </cfoutput>

	    </table>

       </td></tr>
     </table>

	  </div>

	 </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

