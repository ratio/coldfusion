<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.app_badge {
    width: 30%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 250px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
</style>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header">App Library</td></tr>
	   <tr><td class="feed_sub_header" style="font-weight: normal;">
	       Welcome to the Exchange App Library.   The App Library contains dozens of pre-built applications that can be snapped into your Exchange.</td></tr>
	   <tr><td><hr></td></tr>
	   <tr><td height=20></td></tr>

       <cfif isdefined("u")>
        <cfif u is 1>
         <tr><td class="feed_sub_header" style="color: green;">App has been successfully installed.</td></tr>
        <cfelseif u is 2>
         <tr><td class="feed_sub_header" style="color: red;">App has been successfully removed.</td></tr>
        </cfif>
        <tr><td height=10></td></tr>
       </cfif>

	  </table>

	  <cfquery name="installed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select hub_app_app_id from hub_app
	   where hub_app_hub_id = #session.hub#
	  </cfquery>

	  <cfif installed.recordcount is 0>
	   <cfset install_list = 0>
	  <cfelse>
	   <cfset install_list = valuelist(installed.hub_app_app_id)>
	  </cfif>

	  <cfquery name="apps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from app
	   join app_category on app_category.app_category_id = app.app_category_id
	   where app_active = 1
	   order by app_name
	  </cfquery>

      <cfoutput query="apps">

	  <div class="app_badge">
	   <cfinclude template="app_badge.cfm">
	  </div>

	  </cfoutput>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>