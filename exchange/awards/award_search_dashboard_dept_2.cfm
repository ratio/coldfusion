<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Build Query --->

   <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
            federal_action_obligation > 0

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

   </cfquery>

  <!--- NAICS --->

  <cfquery name="naics_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) naics_code, naics_description, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
	         and federal_action_obligation > 0
     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by naics_code, naics_description
     order by amount DESC

   </cfquery>

  <!--- PSC --->

  <cfquery name="psc_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) product_or_service_code, product_or_service_code_description, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
            and federal_action_obligation > 0
     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by product_or_service_code, product_or_service_code_description
     order by amount DESC

   </cfquery>

  <!--- Sub Agency --->

  <cfquery name="sub_agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select awarding_agency_code, awarding_agency_name, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
            and federal_action_obligation > 0

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by awarding_agency_code, awarding_agency_name
     order by amount DESC

   </cfquery>

  <!--- Recipient --->

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
             and federal_action_obligation > 0
     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by recipient_duns, recipient_name
     order by amount DESC

   </cfquery>


  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("session.award_search_name")>

		  <cfoutput>

           <tr><td class="feed_header"><b>#session.award_search_name#</b></td>
                <td class="feed_option" align=right><a href="award_search.cfm">Return</td></tr>
           <tr><td height=5></td></tr>
           <tr><td class="feed_sub_header"><b>Total Addressable Market (TAM) Dashboard - By Actual Spend</b></td>
               <td class="feed_option" align=right></td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_option"><b><a href="award_search_dashboard_dept.cfm">By Actual Spend</a>&nbsp;|&nbsp;<a href="award_search_dashboard_value_dept.cfm">By Total Allocated</a></td></tr>

          </cfoutput>

          <cfelse>

           <tr><td class="feed_header"><b>Total Addressable Market (TAM) Dashboard - By Actual Spend</b></td>
               <td class="feed_option" align=right><a href="award_search.cfm">Return</td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_option"><b><a href="award_search_dashboard_dept.cfm">By Actual Spend</a>&nbsp;|&nbsp;<a href="award_search_dashboard_value_dept.cfm">By Total Allocated</a></td></tr>

          </cfif>

           <tr><td height=20></td></tr>
          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_sub_header">Total Awards: #numberformat(awards.total,'999,999')#</td></tr>
            <tr><td class="feed_sub_header">Award Total: #numberformat(awards.amount,'$999,999,999')#</td></tr>
            <tr><td height=10></td></tr>
            <tr><td><hr></td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td>&nbsp;</td></tr>

           <tr>
              <td class="feed_sub_header" align=center>By NAICS Code (Top 25)</td>
              <td class="feed_sub_header" align=center>By Product or Service Code (Top 25)</td>
           </tr>

           <tr><td valign=top width align=center>



 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="naics_total">
 						   ['#naics_description#',#round(amount)#,'/exchange/awards/award_search_detail_naics_dept.cfm?val=#naics_code#'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'bottom',
						title: '',
			            chartArea:{top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						width: 500,
						height: 425,
						fontSize: 9,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_blank');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="naics_graph"></div>

                </td><td valign=top align=center>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="psc_total">
 						   ['#product_or_service_code_description#',#round(amount)#,'/exchange/awards/award_search_detail_psc_dept.cfm?val=#product_or_service_code#'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'bottom',
						title: '',
			            chartArea:{top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						width: 500,
						height: 425,
						fontSize: 9,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_blank');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="psc_graph"></div>


				    </td></tr>

           <tr><td>&nbsp;</td></tr>

				    </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td>

			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript">
			  google.charts.load('current', {'packages':['treemap']});
			  google.charts.setOnLoadCallback(drawChart);
			  function drawChart() {
				var data = google.visualization.arrayToDataTable([

				  ['Location', 'Parent', 'Value', 'Number of Awards'],
				  ['Award Value', null, 0, 0],

				   <cfoutput query="psc_total">
					['#product_or_service_code_description#','Award Value', #amount#, #numberformat(evaluate(100*(psc_total.amount/awards.amount)),'99.99')#],
				   </cfoutput>
				]);

				tree = new google.visualization.TreeMap(document.getElementById('heatmap'));
				tree.draw(data, {
				highlightOnMouseOver: true,
				minColor: '#D7F4D2',
				maxColor: '#68A429',
				headerHeight: 15,
				height: 400,
				showScale: false,
				generateTooltip: showFullTooltip
				});


		  function showFullTooltip(row, size, value) {
			return '<div style="font-size: 12px; background:#fd9; padding:10px; border-style:solid">' +
				   '<span style="font-size: 12px; font-family:arial"><b>' + data.getValue(row, 0) +
				   '</b><br>'+ data.getValue(row, 1)+
				   ' - $' + data.getValue(row, 2) +
				   '<br>Market Share - ' + data.getValue(row,3) + '%' +
				   '</span><br>' + ' </div>';
		  }

			  }
			</script>

		   <div id="heatmap" style="width: 1300px; height: 500px;"></div>

				    </td></tr>

				    </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
           		<td class="feed_sub_header" align=center>By Department</td>
           		<td class="feed_sub_header" align=center>By Vendor (Top 25)</td>
           </tr>

           <tr><td>&nbsp;</td></tr>

				    <tr><td valign=top>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('dept_graph'));

 						data.addColumn('string','Department');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="sub_agency">
 						   ['#awarding_agency_name#',#round(amount)#,'/exchange/awards/award_search_detail_agency_dept.cfm?val=#awarding_agency_code#'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'bottom',
						title: '',
			            chartArea:{right: 30, left:80,top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						width: 500,
						height: 425,
						fontSize: 9,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_blank');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>


				    <div id="dept_graph"></div>



				    </td><td valign=top align=right>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Recipient', 'Award Value'],
						  <cfoutput query="duns">
						   ['#recipient_name#',#round(duns.amount)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: '',
			            chartArea:{right: 0, left:100,top:20,width:'83%',height:'75%'},
						width: 600,
						height: 400,
						fontSize: 9,
						};

					var chart = new google.visualization.ColumnChart(document.getElementById('duns'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="duns"></div>
				    </td></tr>
             </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=2 class="feed_sub_header"><b>Award Data</b></td></tr>
          <tr><td>&nbsp;</td></tr>

		  <tr><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>NAICS</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td></tr>

		  <cfoutput query="naics_total">
		   <tr><td class="feed_option" width=75 valign=top><a href="award_search_detail_naics_dept.cfm?val=#naics_code#" target="_blank" rel="noopener" rel="noreferrer">#naics_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_naics_dept.cfm?val=#naics_code#" target="_blank" rel="noopener" rel="noreferrer">#naics_description#</a></td>
		       <td class="feed_option" valign=top align=center>#total#</td>
		       <td class="feed_option" valign=top align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

		  <tr><td>&nbsp;</td></tr>

		   <tr><td class="feed_option" colspan=2><b>Department</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td></tr>

		  <cfoutput query="sub_agency">
		   <tr><td class="feed_option" colspan=2><a href="award_search_detail_agency_dept.cfm?val=#awarding_agency_code#" target="_blank" rel="noopener" rel="noreferrer">#awarding_agency_name#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

		  <tr><td>&nbsp;</td></tr>

		   <tr><td class="feed_option" colspan=2><b>Vendor</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td></tr>

		  <cfoutput query="duns">
		   <tr><td class="feed_option" colspan=2><a href="award_search_detail_vendor_dept.cfm?val=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

          </table>

		      </td><td width=50>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>PSC</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td></tr>

		  <cfoutput query="psc_total">
		   <tr><td class="feed_option" valign=top width=50><a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#" target="_blank" rel="noopener" rel="noreferrer">#product_or_service_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#" target="_blank" rel="noopener" rel="noreferrer">#product_or_service_code_description#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

		  </table>

		     </td><td width=30>&nbsp;</td></tr>

		  </table>

  	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>