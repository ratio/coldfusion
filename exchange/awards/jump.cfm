<cfif not isdefined("session.jump")>
 <cfset session.jump = 0>
</cfif>

<div class="left_box">

  <center>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header" valign=absmiddle>AWARD ANALYSIS</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>
  </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
  <form action="/exchange/awards/jump_set.cfm" method="post">
  <tr><td>

              <select name="jump" class="input_select" style="width: 220px;" onchange='if(this.value != 0) { this.form.submit(); }'>
               <option value=0 <cfif session.jump is 0>selected</cfif>>SELECT VIEW
               <option value=1 <cfif session.jump is 1>selected</cfif>>BY DEPARTMENT
               <option value=2 <cfif session.jump is 2>selected</cfif>>BY STATE
               <option value=3 <cfif session.jump is 3>selected</cfif>>BY NAICS CODE
               <option value=4 <cfif session.jump is 4>selected</cfif>>BY PSC CODE
               <option value=5 <cfif session.jump is 5>selected</cfif>>BY SET ASIDES
              </select>

  </td></tr>
  </form>

 </table>
  </center>

</div>