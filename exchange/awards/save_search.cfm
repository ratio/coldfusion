<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

		<cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert award_view
		  (
		  award_view_name,
		  award_view_keyword,
		  award_view_updated,
		  award_view_date_from,
		  award_view_date_to,
		  award_view_state,
		  award_view_naics,
		  award_view_psc,
		  award_view_set_aside,
		  award_view_agency,
		  award_view_location_type,
		  award_view_date_type,
		  award_view_usr_id)
		  values
		  (
		  '#award_view_name#',
		  '#session.award_keyword#',
		   #now()#,
		  '#session.award_from#',
		  '#session.award_to#',
		  '#session.award_state#',
		  '#session.award_naics#',
		  '#session.award_psc#',
		  '#session.award_setaside#',
		  '#session.award_dept#',
		   1,
		   1,
		   #session.usr_id#)
		</cfquery>

<cflocation URL="award_search.cfm?u=1" addtoken="no">

</cfif>