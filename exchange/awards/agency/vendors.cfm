<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"filter_keyword")>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.set_aside_code")>
 <cfset #session.set_aside_code# = 0>
</cfif>

<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 where department_code = '#department_code#'
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 where agency_code = '#agency_code#'
</cfquery>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

  		   <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select sum(base_and_all_options_value) as total from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
                   awarding_agency_code = '#department_code#' and
			       awarding_sub_agency_code = '#agency_code#'

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>

 		   </cfquery>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select recipient_name, recipient_duns, count(distinct(parent_award_id)) as awards, sum(federal_action_obligation) as obligated,
			sum(base_and_all_options_value) as total_value,
			(sum(base_and_all_options_value) - sum(federal_action_obligation)) as available from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>

 		    and (awarding_agency_code = '#department_code#' and
 		         awarding_sub_agency_code = '#agency_code#')

			group by recipient_duns, recipient_name

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by recipient_name DESC
		    <cfelseif #sv# is 10>
		     order by recipient_name ASC
		    <cfelseif #sv# is 2>
		     order by recipient_duns ASC
		    <cfelseif #sv# is 20>
		     order by recipient_duns DESC
		    <cfelseif #sv# is 3>
		     order by awards DESC
		    <cfelseif #sv# is 30>
		     order by awards ASC
		    <cfelseif #sv# is 4>
		     order by obligated DESC
		    <cfelseif #sv# is 40>
		     order by obligated ASC
		    <cfelseif #sv# is 5>
		     order by total_value DESC
		    <cfelseif #sv# is 50>
		     order by total_value ASC
		    <cfelseif #sv# is 6>
		     order by available DESC
		    <cfelseif #sv# is 60>
		     order by available ASC

		    </cfif>

		   <cfelse>
		     order by obligated DESC
		   </cfif>

		  </cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
            </cfif>

<cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">

              <cfif #department_code# is "97">
               #agency.agency_name#
              <cfelse>
               #department.department_name#&nbsp;:&nbsp;#agency.agency_name#
               </td>
              </cfif>

           </td>
               <td class="feed_sub_header" align=right><a href="sub_agency.cfm?department_code=#department_code#">Return</td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>

                       <form action="/exchange/awards/set.cfm" method="post">

           <td class="feed_sub_header" valign=top>Filter Options</td>
           <td align=right class="feed_option" valign=top>


				       <select name="selected_set_aside_code" class="input_select" style="width:250px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>NO SET ASIDE
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>#ucase(set_aside_name)#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

                       <cfoutput>

	                   <b>From:</b>&nbsp;&nbsp;<input type="date" name="from" class="input_date" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               &nbsp;<b>To:</b>&nbsp;&nbsp;<input type="date" name="to" class="input_date" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_3">
                       <input type="hidden" name="department_code" value=#department_code#>
                       <input type="hidden" name="agency_code" value=#agency_code#>
           </cfoutput>

          </td>                       </form>
</tr>

          <tr><td colspan=4><hr></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfparam name="URL.PageId" default="0">
		  <cfset RecordsPerPage = 100>
		  <cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
		  <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		  <cfset EndRow = StartRow+RecordsPerPage-1>

		  <cfset counter = 0>
		  <cfset tot = 0>
		  <cfset tot2 = 0>
		  <cfset tot3 = 0>

 		  <cfoutput>

          <tr>

             <td class="feed_option" colspan=10 valign=top>
              <cfif agencies.recordcount GT #RecordsPerPage#>
				  <b>Page: </b>&nbsp;|
				  <cfloop index="Pages" from="0" to="#TotalPages#">
				   <cfoutput>
				   <cfset DisplayPgNo = Pages+1>
					  <cfif URL.PageId eq pages>
						 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
					  <cfelse>
						 <a href="?pageid=#pages#&department_code=#department_code#&agency_code=#agency_code#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
					  </cfif>
				   </cfoutput>
				  </cfloop>
			   </cfif>

              </td>

             <td class="feed_sub_header" align=right>

              <a href="dashboard_vendors.cfm?department_code=#department_code#&agency_code=#agency_code#"><img src="/images/icon_dashboard.png" hspace=10 border=0 width=20 alt="Dashboard" title="Dashboard"></a><a href="dashboard_vendors.cfm?department_code=#department_code#&agency_code=#agency_code#">Dashboard</a>
              &nbsp;
              <a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" border=0 hspace=10 width=20 alt="Export to Excel" title="Export to Excel"></a><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>

             </td>

          </tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr height=40>
             <td class="feed_option"><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Contractor</b></a></td>
             <td class="feed_option" align=center><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DUNS</b></a></td>
             <td class="feed_option" align=center><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Contracts</b></a></td>
             <td class="feed_option" align=right width=100><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Total Awards</b></a></td>
             <td class="feed_option" align=right width=150><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Total Obligations</b></a></td>
             <td class="feed_option" align=right width=125><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Available</b></a></td>
             <td class="feed_option" align=right width=75><b>% Obligated</b></td>
             <td class="feed_option" align=right width=100><b>Market Share</b></a></td>
          </tr>

          </cfoutput>

          <cfloop query="agencies">

          <cfif CurrentRow gte StartRow >

            <cfif counter is 0>
              <tr bgcolor="ffffff">
            <cfelse>
              <tr bgcolor="e0e0e0">
            </cfif>

            <cfoutput>

               <td class="feed_sub_header"><a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#agencies.recipient_duns#"><b>#agencies.recipient_name#</b></a></td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#agencies.recipient_duns#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(agencies.awards,'999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(agencies.obligated,'$999,999,999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(agencies.total_value,'$999,999,999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>

               <cfif available LTE 0>
               $0
               <cfelse>
               #numberformat(agencies.available,'$999,999,999,999')#
               </cfif></td>

               <cfif available LTE 0>
                <cfset spend_percent = 100>
               <cfelse>
                <cfif agencies.obligated LT 0>
                 <cfset spend_percent = 100>
                <cfelse>
                <cfset spend_percent = #evaluate(agencies.obligated/agencies.total_value)#>
                </cfif>
               </cfif>

               <td class="feed_sub_header" style="font-weight: normal;" align=right width=85>

               <cfif available LTE 0>
                <a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#agencies.recipient_duns#"><progress value="10" max="10" alt="100%" title="100%" style="width:65px;"></progress></a>
               <cfelse>
                <a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#agencies.recipient_duns#"><progress value="#agencies.obligated#" max="#agencies.total_value#" alt="#round(spend_percent*100)#%" title="#round(spend_percent*100)#%" style="width:65px;"></progress></a>
               </cfif>
               </td>

               <cfif evaluate(agencies.obligated/total.total) LT 0>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>0.00%</td>
               <cfelse>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(evaluate((agencies.total_value/total.total)*100),'99.9999')#%</td>
               </cfif>
             </tr>

           </cfoutput>

           <cfset tot = tot + obligated>
           <cfset tot2 = tot2 + total_value>
           <cfset tot3 = tot3 + available>
             <cfif counter is 0>
             <cfset counter = 1>
           <cfelse>
             <cfset counter = 0>
           </cfif>

	      </cfif>

		  <cfif CurrentRow eq EndRow>
		   <cfbreak>
		  </cfif>

          </cfloop>

          <cfoutput>
          <tr><td colspan=8><hr></td></tr>
          <tr><td class="feed_option" colspan=3><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot2,'$999,999,999,999')#</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot3,'$999,999,999,999')#</b></td>
              <td></td></tr>
          </cfoutput>

          <tr><td>&nbsp;</td></tr>

		  </table>

        </td></tr>

       </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>