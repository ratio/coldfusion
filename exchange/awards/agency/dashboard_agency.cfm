<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select * from department
	where department_code = '#department_code#'
  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header"><cfoutput>#dept.department_name#</cfoutput></td>
         <cfoutput>
             <td class="feed_option" align=right>
             <a href="sub_agency.cfm?department_code=#department_code#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
         </cfoutput>
         <tr><td colspan=2><hr></td></tr>
        </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td height=10></td></tr>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(naics_code) as awards, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as obligations, awarding_sub_agency_code, awarding_sub_agency_name from award_data
            left  join agency on agency_code = awarding_sub_agency_code
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
			      and federal_action_obligation > 0
			      and awarding_agency_code = '#department_code#'
 			group by awarding_sub_agency_code, awarding_sub_agency_name
 			order by obligations DESC
		   </cfquery>

  	  </table>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Vendor', 'Award Value',{ role: 'link' }],
			  <cfoutput query="agencies">
			   ['#awarding_sub_agency_name#',#round(obligations)#, '/exchange/awards/agency/vendors.cfm?department_code=#department_code#&agency_code=#awarding_sub_agency_code#'],
			  </cfoutput>
			]);

			var options = {
			'legend'   : 'none',
			'title'    : '',
			'pieHole'  : 0.4,
			'fontSize' : 11,
        hAxis: {
          title: 'Obligations',
			'height'   : 600
			};

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));

 		google.visualization.events.addListener(chart, 'select', function (e) {
            var selection = chart.getSelection();
                if (selection.length) {
                    var row = selection[0].row;
                    let link =data.getValue(row, 2);
                    location.href = link;
                }
        });

        chart.draw(data, options);

      }

		</script>

    <div id="donutchart"></div>


 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Recipient', 'Award Value',{ role: 'link' }],
						  <cfoutput query="agencies">
						   ['#awarding_sub_agency_name#',#round(obligations)#, '/exchange/awards/agency/vendors.cfm?department_code=#department_code#&agency_code=#awarding_sub_agency_code#'],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: '',
			            chartArea:{right: 0, left:100,top:20,width:'83%',height:'75%'},
						width: 1250,
						height: 400,
						fontSize: 9,
						};

					var chart = new google.visualization.ColumnChart(document.getElementById('Awards'));

 		google.visualization.events.addListener(chart, 'select', function (e) {
            var selection = chart.getSelection();
                if (selection.length) {
                    var row = selection[0].row;
                    let link =data.getValue(row, 2);
                    location.href = link;
                }
        });


					chart.draw(data, options);

					  }
					</script>


    <div id="Awards"></div>









</div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>