<cfinclude template="/exchange/security/check.cfm">

<cfsetting requestTimeOut = "90000" />

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 	  <div class="exchange_filter_box">

	      <cfinclude template="/exchange/analytics/selection.cfm">

	  </div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Agency Analytics</td><td align=right class="feed_option"></td></tr>
          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfset counter = 0>

          <tr><td class="feed_option"><b>Department Name</b></a></td></tr>

			<cfquery name="departments" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select * from department
			 order by department_name
			</cfquery>

          <cfoutput query="departments">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
            <cfelse>
            <tr bgcolor="e0e0e0">
            </cfif>

               <td class="feed_option"><a href="sub_agency.cfm?department_code=#department_code#">#department_name#</a></td>

            </tr>

           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>