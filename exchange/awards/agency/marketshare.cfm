<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

  		   <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from agency
			where agency_code = '#agency_code#'
		   </cfquery>

  		   <cfquery name="tot_val" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select sum(federal_action_obligation) as total from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
			          awarding_agency_code = '#department_code#' and
			          awarding_sub_agency_code = '#agency_code#'
 		   </cfquery>

  		   <cfquery name="wins" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select top 100 recipient_name, recipient_duns, sum(federal_action_obligation) as total, count(id) as number from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
			      and awarding_agency_code = '#department_code#' and
			          awarding_sub_agency_code = '#agency_code#'
 			group by recipient_duns, recipient_name
			order by total DESC
 		   </cfquery>

           <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top><cfoutput>Marketshare <cfif #wins.recordcount# is 100>(Top 100 Vendors)</cfif> - #agency.agency_name#</cfoutput></td>
               <td class="feed_option" align=right><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#">Return</a></td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_option" colspan=2 valign=top>

                      <form action="/exchange/awards/set.cfm" method="post">
	                   From:&nbsp;<input type="date" name="from" size=8 maxlength=10 required value="#dateformat(session.award_from,'yyyy-mm-dd')#">
		               To:&nbsp;<input type="date" name="to" size=8 maxlength=10 required value="#dateformat(session.award_to,'yyyy-mm-dd')#">
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_5">
                       <input type="hidden" name="department_code" value="#department_code#">
                       <input type="hidden" name="agency_code" value="#agency_code#">
                       </form>
           </cfoutput>

          </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['treemap']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([

          ['DUNS', 'Vendor', 'Number of Awards','Value'],
          ['Award Value', null, 0, 0],

           <cfoutput query="wins">
            ['#wins.recipient_duns#','#replace(recipient_name,"'","","all")#',#round(total)#, #numberformat(evaluate(100*(wins.total/tot_val.total)),'99.99')#],
           </cfoutput>
        ]);

        tree = new google.visualization.TreeMap(document.getElementById('chart_div'));
        tree.draw(data, {
        highlightOnMouseOver: true,
        minColor: '#D7F4D2',
        maxColor: '#68A429',
        headerHeight: 15,
        showScale: false,
        generateTooltip: showFullTooltip
        });

  function showFullTooltip(row, size, value) {
    return '<div style="font-size: 12px; background:#fd9; padding:10px; border-style:solid">' +
           '<span style="font-size: 12px; font-family:arial"><b>' + data.getValue(row, 0) +
           '</b><br>'+ data.getValue(row, 1)+
           ' - $' + data.getValue(row, 2) +
           '<br>Market Share - ' + data.getValue(row,3) + '%' +
           '<div style="border-style:none">' +
           <cfoutput>
           '<br><a href="/exchange/awards/agency/contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&name=' + data.getValue(row, 0) + '">View Awards</a>' +
           </cfoutput>
           '</span><br>' + ' </div>';
  }

      }
    </script>

    <div id="chart_div" style="width: 1300px; height: 500px;"></div>

</td></tr>

</table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>