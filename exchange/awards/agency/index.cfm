<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.award_to") or not isdefined("session.award_from")>
 <cfset #session.award_to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.award_from# = #dateformat(dateadd("d",-730,now()),'yyyy-mm-dd')#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Federal Awards - By Department and Agency</b></td>
             <td align=right></td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from department
	        order by department_name ASC
		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>

          <tr><td class="feed_sub_header">DEPARTMENT</td></tr>

          <cfloop query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

			   <cfif #agencies.department_code# is '97' or #agencies.department_code# is '097'>

  		       <cfquery name="sub" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			    select distinct(agency_code), department_code, agency_name from sub_agency
			    where department_code = '097' or department_code = '97'
			    order by agency_name
			   </cfquery>

               <cfoutput>
				 <td class="feed_sub_header" style="font-weight: normal;"><a href="##" onclick="toggle_visibility('foo');"><b>#agencies.department_name#</b></a><br>
               </cfoutput>

               <div id="foo" style="display:none;">
                   <br>
                   <cfoutput query="sub">
                   &nbsp;&nbsp;-&nbsp;&nbsp;<a href="/exchange/awards/agency/vendors.cfm?department_code=#sub.department_code#&agency_code=#agency_code#">#agency_name#</a><br>
                   </cfoutput>
                   <br>
               </div>

               </td>

			   <cfelse>
           <cfoutput>

				   <td class="feed_sub_header" style="font-weight: normal;"><a href="/exchange/awards/agency/sub_agency.cfm?department_code=#agencies.department_code#"><b>#agencies.department_name#</b></a></td>
           </cfoutput>
			   </cfif>

           </tr>

           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfloop>

          <tr><td>&nbsp;</td></tr>

		  </table>

		  </td></tr>

		</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>