<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from department
 where department_code = '#department_code#'
</cfquery>

<cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from agency
 where agency_code = '#agency_code#'
</cfquery>

<cfquery name="vendor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select top(1) * from award_data
 where recipient_duns = '#duns#'
</cfquery>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select award_id_piid, count(id) as records, naics_description, product_or_service_code_description, sum(base_and_exercised_options_value) as obligations, sum(base_and_all_options_value) as total,
			(sum(base_and_all_options_value) - sum(base_and_exercised_options_value)) as available from award_data

            where (recipient_duns = '#duns#' and
                  action_date between '#session.award_from#' and '#session.award_to#' and
			      awarding_agency_code = '#department_code#' and
			      awarding_sub_agency_code = '#agency_code#')

		   <cfif isdefined("session.filter_keyword")>
		   and contains((award_description, product_or_service_code_description),'"#trim(session.filter_keyword)#"')
		   </cfif>

		   group by award_id_piid, naics_description, product_or_service_code_description

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by obligations DESC
		    <cfelseif #sv# is 10>
             order by obligations ASC
		    <cfelseif #sv# is 2>
             order by total DESC
		    <cfelseif #sv# is 20>
             order by total ASC
		    <cfelseif #sv# is 3>
             order by available DESC
		    <cfelseif #sv# is 30>
             order by available ASC

		    </cfif>

		   <cfelse>
		     order by available DESC
		   </cfif>


		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>
		  <cfset tot2 = 0>
		  <cfset tot3 = 0>

          <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
          </cfif>

<cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">#department.department_name# - #agency.agency_name#</td>
               <td align=right class="feed_sub_header"><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="/exchange/awards/set.cfm" method="post">

          <tr>

           <td class="feed_sub_header" valign=top>Filter Options</td>
           <td align=right class="feed_option">


            <b>Keyword:</b>&nbsp;
                       <input type="text" class="input_text" name="filter" size=20 <cfif isdefined("session.filter_keyword")> value="#session.filter_keyword#"</cfif>>&nbsp;
	                   <b>From:</b>&nbsp;&nbsp;<input type="date" name="from" class="input_date" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               &nbsp;<b>To:</b>&nbsp;&nbsp;<input type="date" name="to" class="input_date" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_4">
					   <input type="hidden" name="department_code" value=#department_code#>
					   <input type="hidden" name="agency_code" value=#agency_code#>
					   <input type="hidden" name="duns" value=#duns#>
           </cfoutput>

          </td></tr>

          </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		  <cfoutput>

          <tr><td class="feed_sub_header" valign=middle><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer">#vendor.recipient_name#</a> - CONTRACTS</td>
           <td class="feed_sub_header" align=right valign=middle>
              <cfoutput>

              <a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#"><img src="/images/icon_list.png" border=0 hspace=10 width=20 alt="Detailed Awards" title="Detailed Awards"></a>
              <a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#">Detailed Awards</a>&nbsp;
              &nbsp;&nbsp;&nbsp;<a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" hspace=10 border=0 width=20 alt="Export to Excel" title="Export to Excel"></a>
              <a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>

              </cfoutput>

              </td>

          </tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr height=40>

             <td class="feed_option"><b>Award Date</b></td>
             <td class="feed_option"><b>Contract ##</b></td>
             <td class="feed_option" align=center><b>Actions</b></td>
             <td class="feed_option"><b>Office</b></td>
             <td class="feed_option"><b>Description</b></td>
             <td class="feed_option"><b>NAICS</b></td>
             <td class="feed_option"><b>Product or Service</b></td>
             <td class="feed_option" align=right><a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award</b></a></td>
             <td class="feed_option" align=right><a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Obligated</b></a></td>
             <td class="feed_option" align=right><a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Available</b></a></td>
             <td class="feed_option" align=right width=75><b>% Obligated</b></td>

          </tr>

          </cfoutput>

          <cfloop query="agencies">

  		   <cfquery name="desc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  		    select top(1) award_description, awarding_office_name, action_date, recipient_duns from award_data
  		    where award_id_piid = '#agencies.award_id_piid#' and
  		          recipient_duns = '#duns#'
  		    order by action_date
  		   </cfquery>

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

           <cfoutput>

               <td class="text_xsmall" valign=top width=75>#dateformat(desc.action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/contract_history.cfm?contractid=#agencies.award_id_piid#&duns=#duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.award_id_piid#</b></a></td>
               <td class="text_xsmall" valign=top align=center width=75>#agencies.records#</td>
               <td class="text_xsmall" valign=top>#desc.awarding_office_name#</td>
               <td class="text_xsmall" valign=top>

               <cfif isdefined("session.filter_keyword")>
	               #ucase(replaceNoCase(desc.award_description,session.filter_keyword,"<span style='background:yellow'>#session.filter_keyword#</span>","all"))#
               <cfelse>
	               #desc.award_description#
               </cfif>
               </td>

               <td class="text_xsmall" valign=top>

               <cfif isdefined("session.filter_keyword")>
	               #ucase(replaceNoCase(agencies.naics_description,session.filter_keyword,"<span style='background:yellow'>#session.filter_keyword#</span>","all"))#
               <cfelse>
	               #agencies.naics_description#
               </cfif>
               </td>

               <td class="text_xsmall" valign=top>

               <cfif isdefined("session.filter_keyword")>
	               #ucase(replaceNoCase(agencies.product_or_service_code_description,session.filter_keyword,"<span style='background:yellow'>#session.filter_keyword#</span>","all"))#
               <cfelse>
	               #agencies.product_or_service_code_description#
               </cfif>
               </td>

               <td class="text_xsmall" valign=top width=100 align=right>#numberformat(agencies.obligations,'$999,999,999,999')#</td>
               <td class="text_xsmall" valign=top width=100 align=right>#numberformat(agencies.total,'$999,999,999,999')#</td>
               <td class="text_xsmall" valign=top width=100 align=right>#numberformat(agencies.available,'$999,999,999,999')#</td>

               <cfif available LTE 0>
                <cfset spend_percent = 100>
               <cfelse>
                <cfif agencies.obligations LT 0>
                 <cfset spend_percent = 100>
                <cfelse>
                 <cfset spend_percent = #evaluate(agencies.obligations/agencies.total)#>
                </cfif>
               </cfif>

               <td class="feed_option" align=right width=80 valign=top>

               <cfif spend_percent is 100>
                <progress value="10" max="10" alt="100%" title="100%" style="width:60px;"></progress></a>
               <cfelse>
               <cfif available LTE 0>
                <progress value="10" max="10" alt="100%" title="100%" style="width:60px;"></progress></a>
               <cfelse>
                <progress value="#agencies.obligations#" max="#agencies.total#" alt="#numberformat(evaluate(spend_percent*100),'999.99')#%" title="#numberformat(evaluate(spend_percent*100),'999.99')#%" style="width:60px;"></progress></a>
               </cfif>
               </cfif>
               </td>

          </cfoutput>

             </tr>

          <cfif agencies.obligations is "">
           <cfset obl = 0>
          <cfelse>
           <cfset obl = agencies.obligations>
          </cfif>

          <cfif agencies.total is "">
           <cfset agtot = 0>
          <cfelse>
           <cfset agtot = agencies.total>
          </cfif>

          <cfif agencies.available is "">
           <cfset avail = 0>
          <cfelse>
           <cfset avail = agencies.available>
          </cfif>

          <cfset tot = tot + #obl#>
          <cfset tot2 = tot2 + #agtot#>
          <cfset tot3 = tot3 + #avail#>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfloop>

          <cfoutput>
          <tr><td colspan=11><hr></td></tr>
          <tr><td class="feed_option" colspan=7><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot2,'$999,999,999,999')#</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot3,'$999,999,999,999')#</b></td>
              </tr>
          </cfoutput>


          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>