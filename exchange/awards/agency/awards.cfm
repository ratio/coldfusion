<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template="/exchange/include/header.cfm">

	   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select * from department
		where department_code = '#department_code#'
	   </cfquery>

	   <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select * from agency
		where agency_code = '#agency_code#'
	   </cfquery>

	   <cfquery name="vendor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select top(1) * from award_data
		where recipient_duns = '#duns#'
	   </cfquery>

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">FEDERAL AWARDS - #department.department_name#&nbsp;:&nbsp;#agency.agency_name#</td>
               <td align=right class="feed_sub_header"><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>

          <tr><td height=5></td></tr>

          <tr><td class="feed_sub_header" valign=top>
              <cfoutput>
              <a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=15 hspace=10 border=0>
              <a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><b>Export to Excel</b></a>
              </cfoutput>

              </td>

            <td class="feed_option" align=right>

                      <form action="/exchange/awards/set.cfm" method="post">

            Filter by Keyword:&nbsp;
                       <input type="text" class="input_text" name="filter" size=20 <cfif isdefined("session.filter_keyword")> value="#session.filter_keyword#"</cfif>>&nbsp;
	                   From:&nbsp;&nbsp;<input type="date" class="input_date" name="from" size=8 maxlength=10 required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               &nbsp;To:&nbsp;&nbsp;<input type="date" class="input_date" name="to" size=8 maxlength=10 required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue_large" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_8">
					   <input type="hidden" name="department_code" value=#department_code#>
					   <input type="hidden" name="agency_code" value=#agency_code#>
					   <input type="hidden" name="duns" value=#duns#>
                       </form>

          </td></tr>

          <tr>
             <td class="feed_header"><a href="/exchange/include/federal_profile.cfm?duns=#vendor.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#vendor.recipient_name#</a></td>

             <td class="feed_sub_header" align=right>
              <a href="contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#"><b>Contracts</b></a>
             </td>

             </tr>
          <tr><td height=10></td></tr>

          </table>

          </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select action_date, base_and_exercised_options_value, recipient_name, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, award_id_piid, modification_number, award_description, product_or_service_code_description, naics_code, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_potential_end_date, federal_action_obligation, id from award_data

            where (recipient_duns = '#duns#' and
                  action_date between '#session.award_from#' and '#session.award_to#' and
			      awarding_agency_code = '#department_code#' and
			      awarding_sub_agency_code = '#agency_code#')

		   <cfif isdefined("session.filter_keyword")>
		   and contains((award_description, product_or_service_code_description),'"#session.filter_keyword#"')
		   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by award_id_piid ASC
		    <cfelseif #sv# is 10>
             order by award_id_piid DESC
		    <cfelseif #sv# is 3>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 4>
             order by solicitation_identifier ASC
		    <cfelseif #sv# is 40>
             order by solicitation_identifier DESC
		    <cfelseif #sv# is 5>
             order by product_or_service_code_description ASC
		    <cfelseif #sv# is 50>
             order by product_or_service_code_description DESC
		    <cfelseif #sv# is 6>
             order by naics_code ASC
		    <cfelseif #sv# is 60>
             order by naics_code DESC
		    <cfelseif #sv# is 7>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 70>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 8>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 80>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 9>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 90>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 2>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 20>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 11>
             order by action_date DESC
		    <cfelseif #sv# is 110>
             order by action_date ASC
		    <cfelseif #sv# is 12>
             order by type_of_set_aside ASC
		    <cfelseif #sv# is 120>
             order by type_of_set_aside DESC

		    <cfelseif #sv# is 13>
             order by base_and_all_options_value DESC
		    <cfelseif #sv# is 130>
             order by base_and_all_options_value ASC


		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>
		  <cfset tot2 = 0>

          <cfif isdefined("export")>

             <cfinclude template="/exchange/include/export_to_excel.cfm">

            </cfif>

 		  <cfoutput>

          <tr><td>&nbsp;</td></tr>

          <tr>

             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Award Date</b></a></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Contract ##</b></a></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>MOD ##</b></a></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Office</b></a></td>
             <td class="text_xsmall"><b>Award Description</b></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Product or Service</b></a></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>NAICS</b></a></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>PoP Start</b></a></td>
             <td class="text_xsmall"><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PoP End</b></a></td>
             <td class="text_xsmall" align=right><a href="awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Award</b></a></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="text_xsmall" valign=top width=75>#dateformat(action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=125><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
               <td class="text_xsmall" valign=top width=50>#modification_number#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>
               <td class="text_xsmall" valign=top width=350>

               <cfif isdefined("session.filter_keyword")>
	               #ucase(replaceNoCase(award_description,session.filter_keyword,"<span style='background:yellow'>#session.filter_keyword#</span>","all"))#
               <cfelse>
	               #award_description#
               </cfif>
               </td>
               <td class="text_xsmall" valign=top>#product_or_service_code_description#</td>
               <td class="text_xsmall" valign=top width=75>#naics_code#</td>
               <td class="text_xsmall" valign=top width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=75>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             </tr>

          <cfset tot = tot + #federal_action_obligation#>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=12><hr></td></tr>
          <tr><td class="text_xsmall" colspan=9><b>Total:</b></td>
              <td class="text_xsmall" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td>

              </tr>
          </cfoutput>


          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>