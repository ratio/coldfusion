<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.set_aside_code")>
 <cfset #session.set_aside_code# = 0>
</cfif>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

  		   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from department
			where department_code = '#department_code#'
		   </cfquery>

  		   <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from agency
			where agency_code = '#agency_code#'
		   </cfquery>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select top(25) recipient_name, recipient_duns, count(naics_code) as awards, sum(federal_action_obligation) as amount from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
                   awarding_agency_code = '#department_code#' and
			       awarding_sub_agency_code = '#agency_code#'

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>
			group by recipient_name, recipient_duns
			order by amount DESC
		  </cfquery>


  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

 		  <cfoutput>

          <tr><td class="feed_header">#department.department_name# - #agency.agency_name# DASHBOARD</td>
              <td align=right><a href="/exchange/awards/agency/vendors.cfm?department_code=#department_code#&agency_code=#agency_code#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
          <tr><td colspan=2><hr></td></tr>

          </cfoutput>

		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_sub_header">Filter Options</td>
               <td align=right class="feed_option">

                       <form action="/exchange/awards/set.cfm" method="post">

				       <select name="selected_set_aside_code" class="input_select" style="width:250px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>NO SET ASIDE
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>#ucase(set_aside_name)#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

                       <cfoutput>

	                   <b>From:</b>&nbsp;&nbsp;<input type="date" name="from" class="input_date" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               &nbsp;<b>To:</b>&nbsp;&nbsp;<input type="date" name="to" class="input_date" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_7">
                       <input type="hidden" name="department_code" value=#department_code#>
                       <input type="hidden" name="agency_code" value=#agency_code#>
                       </form>
           </cfoutput>

          </td></tr>

          </table>

	  <cfif agencies.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_option">No records found.</td></tr>
		  </table>

		  <cfelse>

				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Recipient', 'Award Value',{ role: 'link' }],
						  <cfoutput query="agencies">
						   ['#recipient_name#',#round(amount)#, '/exchange/awards/agency/contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#recipient_duns#'],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: '',
			            chartArea:{right: 0, left:100,top:20,width:'83%',height:'75%'},
						height: 400,
						fontSize: 9,
						};

					var chart = new google.visualization.ColumnChart(document.getElementById('Awards'));

					google.visualization.events.addListener(chart, 'select', function (e) {
						var selection = chart.getSelection();
							if (selection.length) {
								var row = selection[0].row;
								let link =data.getValue(row, 2);
								location.href = link;
							}
					});

					chart.draw(data, options);

					  }
					</script>


    <div id="Awards" style="width: 100%"></div>

    </cfif>



	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>