<cfinclude template="/exchange/security/check.cfm">

<cfset session.set_aside_code = 0>

<cfif not isdefined("session.award_to")>
 <cfset #session.award_to# = #now()#>
 <cfset #session.award_from# = #dateadd("d",-740,now())#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

  		   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from department
			where department_code = '#department_code#'
			order by department_name
		   </cfquery>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top>#department.department_name#</td>
               <td class="feed_sub_header" align=right><a href="/exchange/awards/agency/">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="/exchange/awards/set.cfm" method="post">

           <tr>
           <td class="feed_sub_header" valign=top>Filter Options</td>
           <td valign=top class="feed_option" align=right>

	                   <b>From:</b>&nbsp;&nbsp;<input type="date" name="from" class="input_date" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               &nbsp;<b>To:</b>&nbsp;&nbsp;<input type="date" name="to" class="input_date" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_2">
                       <input type="hidden" name="department_code" value="#department_code#">

          </td></tr>

          </form>

          <tr><td colspan=4><hr></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header" align=right>
              <a href="dashboard_agency.cfm?department_code=#department_code#"><img src="/images/icon_dashboard.png" border=0 hspace=10 width=20 alt="Dashboard" title="Dashboard"></a><a href="dashboard_agency.cfm?department_code=#department_code#">Dashboard</a>
              &nbsp;
              <a href="sub_agency.cfm?department_code=#department_code#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" border=0 width=20 alt="Export to Excel" hspace=10 title="Export to Excel"></a><a href="sub_agency.cfm?department_code=#department_code#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
           </td></tr>

           </table>

           </cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select awarding_sub_agency_code, awarding_sub_agency_name, count(distinct(recipient_duns)) as vendors,
			count(distinct(parent_award_id)) as awards, sum(federal_action_obligation) as obligations, sum(base_and_all_options_value) as total_value,
			(sum(base_and_all_options_value) - sum(federal_action_obligation)) as available
			from award_data
			where action_date between '#dateformat(session.award_from,'mm/dd/yyyy')#' and '#dateformat(session.award_to,'mm/dd/yyyy')#' and
			      awarding_agency_code = '#department_code#'
			group by awarding_sub_agency_code, awarding_sub_agency_name

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 10>
		     order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 2>
		     order by awards DESC
		    <cfelseif #sv# is 20>
		     order by awards ASC
		    <cfelseif #sv# is 3>
		     order by vendors DESC
		    <cfelseif #sv# is 30>
		     order by vendors ASC
		    <cfelseif #sv# is 4>
		     order by obligations DESC
		    <cfelseif #sv# is 40>
		     order by obligations ASC
		    <cfelseif #sv# is 5>
		     order by total_value DESC
		    <cfelseif #sv# is 50>
		     order by total_value ASC
		    <cfelseif #sv# is 6>
		     order by available DESC
		    <cfelseif #sv# is 60>
		     order by available ASC
		    </cfif>

		   <cfelse>
		     order by obligations DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>
		  <cfset tot2 = 0>
		  <cfset tot3 = 0>

          <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
          </cfif>

 		  <cfoutput>

          <tr height=40>
             <td class="feed_option"><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Agency</b></a></td>
             <td class="feed_option" align=center><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contracts</b></a></td>
             <td class="feed_option" align=center><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Vendors</b></a></td>
             <td class="feed_option" align=right width=100><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Award Value</b></a></td>
             <td class="feed_option" align=right width=150><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Total Obligations</b></a></td>
             <td class="feed_option" align=right width=150><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Total Available</b></a></td>
             <td class="feed_option" align=right width=100><b>% Allocated</b></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="feed_sub_header"><a href="vendors.cfm?department_code=#department_code#&agency_code=#awarding_sub_agency_code#"><b>#awarding_sub_agency_name#</b></a></td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(awards,'999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(vendors,'999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(obligations,'$999,999,999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(total_value,'$999,999,999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(available,'$999,999,999,999')#</td>

               <cfif available LTE 0>
                <cfset spend_percent = 100>
               <cfelse>
                <cfset spend_percent = #evaluate(agencies.obligations/agencies.total_value)#>
               </cfif>

               <td class="feed_sub_header" style="font-weight: normal;" align=right width=80>

               <cfif available LTE 0>
                <a href="vendors.cfm?department_code=#department_code#&agency_code=#awarding_sub_agency_code#"><progress value="10" max="10" alt="100%" title="100%" style="width:60px;"></progress></a>
               <cfelse>
                <a href="vendors.cfm?department_code=#department_code#&agency_code=#awarding_sub_agency_code#"><progress value="#agencies.obligations#" max="#agencies.total_value#" alt="#numberformat(evaluate(spend_percent*100),'999.99')#%" title="#numberformat(evaluate(spend_percent*100),'999.99')#%" style="width:60px;"></progress></a>
               </cfif>
               </td>
             </tr>

           <cfset tot = tot + obligations>
           <cfset tot2 = tot2 + total_value>
           <cfset tot3 = tot3 + available>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=7><hr></td></tr>
          <tr><td class="feed_option" colspan=3><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot2,'$999,999,999,999')#</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot3,'$999,999,999,999')#</b></td>
              <td>&nbsp;</td>
              <tr><td></td></tr>
          </cfoutput>
          <tr><td>&nbsp;</td></tr>

		  </table>

         </td></tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>