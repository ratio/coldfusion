<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.set_aside_code")>
 <cfset #session.set_aside_code# = 0>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;<a href="/exchange/awards/agency/">By Department</a></td>
               <td align=right class="feed_option">

			   <cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select * from set_aside
				order by set_aside_name
			   </cfquery>

                       <form action="/exchange/awards/set.cfm" method="post">

				       <select name="selected_set_aside_code" style="width:200px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>All AWARDS
				       <option value=1 <cfif #session.set_aside_code# is 1>selected</cfif>>All SET ASIDES
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>&nbsp;-&nbsp;#set_aside_name#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

                       <cfoutput>

	                   From:&nbsp;<input type="date" name="from" size=8 maxlength=10 required value="#dateformat(session.award_from,'yyyy-mm-dd')#">
		               To:&nbsp;<input type="date" name="to" size=8 maxlength=10 required value="#dateformat(session.award_to,'yyyy-mm-dd')#">
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="20">
                       <input type="hidden" name="department_code" value=#department_code#>
                       <input type="hidden" name="agency_code" value=#agency_code#>
                       </form>
           </cfoutput>

          </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from department
			where department_code = '#department_code#'
		   </cfquery>

  		   <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from agency
			where agency_code = '#agency_code#'
		   </cfquery>

  		   <cfquery name="contracts" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data award_data
			where awarding_sub_agency_code = '#agency_code#'
 		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>

		     order by action_date DESC

		  </cfquery>

		  <cfparam name="URL.PageId" default="0">
		  <cfset RecordsPerPage = 250>
		  <cfset TotalPages = (contracts.Recordcount/RecordsPerPage)>
		  <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		  <cfset EndRow = StartRow+RecordsPerPage-1>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

          <tr><td class="feed_option"><b>

              <cfif #department_code# is "97">
               <a href="/exchange/awards/agency/">#agency.agency_name#</a>
              <cfelse>
               <a href="sub_agency.cfm?department_code=#department_code#">#department.department_name#</a>&nbsp;:&nbsp;<b>#agency.agency_name#</b>
               </td>
              </cfif>


              <td align=right class="feed_option" colspan=3>
              <cfif contracts.recordcount GT #RecordsPerPage#>
				  <b>Page: </b>&nbsp;|
				  <cfloop index="Pages" from="0" to="#TotalPages#">
				   <cfoutput>
				   <cfset DisplayPgNo = Pages+1>
					  <cfif URL.PageId eq pages>
						 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
					  <cfelse>
						 <a href="?pageid=#pages#&department_code=#department_code#&agency_code=#agency_code#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
					  </cfif>
				   </cfoutput>
				  </cfloop>
			   </cfif>

              </td>

          </tr>

          <cfoutput>
            <tr><td class="feed_option"><b><a href="vendors.cfm?department_code=#department_code#&agency_code=#agency_code#">By Vendor</a>&nbsp;|&nbsp;By Contract</b>

            &nbsp;|&nbsp;<a href="marketshare.cfm?department_code=#department_code#&agency_code=#agency_code#"><b>Marketshare</b></a>


            </td></tr>
          </cfoutput>

          <tr><td height=10></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>


          <tr>
             <td class="text_xsmall" width=75><b>Award Date</b></td>
             <td class="text_xsmall" width=100><b>Contract</b></td>
             <td class="text_xsmall" width=200><b>Vendor</b></td>
             <td class="text_xsmall"><b>Award Description</b></td>
             <td class="text_xsmall" align=center><b>NAICS</b></td>
             <td class="text_xsmall" align=center width=75><b>PoP Start</b></td>
             <td class="text_xsmall" align=center width=75><b>PoP End</b></td>
             <td class="text_xsmall" align=right width=100><b>Obligation</b></td>
             <td class="text_xsmall" align=right width=100><b>Options</b></td>
             <td class="text_xsmall" align=right width=100><b>Ceiling</b></td>
          </tr>

          </cfoutput>

          <cfloop query="contracts">

          <cfif CurrentRow gte StartRow >


            <cfif counter is 0>
              <tr bgcolor="ffffff">
            <cfelse>
              <tr bgcolor="e0e0e0">
            </cfif>

            <cfoutput>

             <td class="text_xsmall">#dateformat(action_date,'mm/dd/yyyy')#</td>
             <td class="text_xsmall"><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
             <td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
             <td class="text_xsmall">#left(award_description,200)#...</td>
             <td class="text_xsmall" align=center>#naics_code#</td>
             <td class="text_xsmall" align=center>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
             <td class="text_xsmall" align=center>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
             <td class="text_xsmall" align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             <td class="text_xsmall" align=right>#numberformat(base_and_exercised_options_value,'$999,999,999,999')#</td>
             <td class="text_xsmall" align=right>#numberformat(base_and_all_options_value,'$999,999,999,999')#</td>
             </tr>

           </cfoutput>

           <cfset tot = tot + federal_action_obligation>
             <cfif counter is 0>
             <cfset counter = 1>
           <cfelse>
             <cfset counter = 0>
           </cfif>

	      </cfif>

		  <cfif CurrentRow eq EndRow>
		   <cfbreak>
		  </cfif>

          </cfloop>

          <cfoutput>
          <tr><td class="feed_option" colspan=9><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>