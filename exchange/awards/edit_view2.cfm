<cfinclude template="/exchange/security/check.cfm">

<cfquery name="view" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_view
 where award_view_id = #award_view_id# and
       award_view_usr_id = #session.usr_id#
</cfquery>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="departments" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from department
  order by department_name
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from agency
  order by agency_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Edit Search</td><td align=right class="feed_option"><a href="index.cfm">Return</a></td></tr>
           <tr><td class="feed_option">Please choose the options below for how you want to filter Federal awards.</td></tr>

           <cfif isdefined("u")>
            <tr><td height=10></td></tr>
            <tr><td class="feed_option"><font color="red"><b>Date range must be less than 1 year (or 365 days)></b></font></td></tr>
           </cfif>

           <tr><td>&nbsp;</td></tr>
          </table>

          <form name="autoSelectForm" action="create_view_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <cfoutput>

			   <tr><td class="feed_option" width=150><b>View Name</b></td>
				   <td><input type="text" name="award_view_name" size=40 required value="#view.award_view_name#" style="width: 350px;">&nbsp;*</td></tr>

			   <tr><td class="feed_option" width=150 valign=top><b>Description</b></td>
				   <td><textarea name="award_view_desc" cols=38 rows=6 style="width: 350px;">#view.award_view_desc#</textarea></td></tr>

               </cfoutput>

		      <tr><td class="feed_option" valign=top><b>Set Aside</b></td>
		          <td class="feed_option">

				   <select name="award_view_set_aside" style="font-size: 12px; width: 350px;">
					   <option value=0 <cfif #view.award_view_set_aside# is 0>selected</cfif>>ALL
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #view.award_view_set_aside# is #set_aside_code#>selected</cfif>>#set_aside_name#
					   </cfoutput>
				   </select>&nbsp;**</td></tr>

		      <tr><td class="feed_option" valign=top><b>State</b></td>
		          <td class="feed_option">

				   <select name="award_view_state" style="font-size: 12px; width: 350px;">
					   <option value=0 <cfif #view.award_view_state# is 0>selected</cfif>>All
					   <cfoutput query="states">
					    <option value="#state_abbr#" <cfif #view.award_view_state# is #state_abbr#>selected</cfif>>#state_name#
					   </cfoutput>
				   </select>&nbsp;**</td></tr>

              <tr><td></td>
                  <td class="feed_option">
                  <input type="radio" name="award_view_location_type" value=1 <cfif #view.award_view_location_type# is 1>checked</cfif>>&nbsp;Contract State&nbsp;
                  <input type="radio" name="award_view_location_type" value=2 <cfif #view.award_view_location_type# is 2>checked</cfif>>&nbsp;Place of Performance&nbsp;
                  </td></tr>

		      <cfoutput>



			  <tr><td class="feed_option" width=100><b>NAICS Code(s)</b></td>
				   <td class="feed_option"><input type="text" name="award_view_naics" size=40 value="#view.award_view_naics#" maxlength="599" required>&nbsp;&nbsp;<a href="https://www.census.gov/cgi-bin/sssdaicsaicsrch?chart=2017" target="_blank" rel="noopener" rel="noreferrer">NAICS Code List</a></td></tr>

			  <tr><td class="feed_option" width=100><b>PSC Code(s)</b></td>
				   <td class="feed_option"><input type="text" name="award_view_psc" size=40 value="#view.award_view_psc#" maxlength="299">&nbsp;&nbsp;<a href="http://psctool.us/" target="_blank" rel="noopener" rel="noreferrer">PSC Codes List</a></td></tr>

              <tr><td height=10></td></tr>
			  <tr><td></td><td class="text_xsmall"><i>for multiple NAICS or PSC code, seperate with a comma (i.e., 544111,643122)</i></td></tr>

              </cfoutput>

			  </table>

          </td><td valign=top>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <cfoutput>

			  <tr><td class="feed_option" width=150><b>Vendor(s) DUNS</b></td>
				   <td><input type="text" name="award_view_duns" size=40 maxlength="599" style="width:400px;" value="#view.award_view_duns#"></td></tr>
			  <tr><td></td><td class="text_xsmall"><i>for multiple Vendors, seperate DUNS with a comma</i></td></tr>
              <tr><td height=10></td></tr>


			  <tr><td class="feed_option" width=100><b>Date Range</b></td>
				   <td class="text_xsmall">From:&nbsp;&nbsp;<input type="date" name="award_view_date_from" value="#view.award_view_date_from#" style="width: 130px;" required>&nbsp;To:&nbsp;<input type="date" name="award_view_date_to" value="#view.award_view_date_to#" style="width: 130px;" required>&nbsp;*</td></tr>

              <tr><td></td>
                  <td class="feed_option">
                  <input type="radio" name="award_view_date_type" value=1 <cfif #view.award_view_date_type# is 1>checked</cfif>>&nbsp;Award Date&nbsp;
                  <input type="radio" name="award_view_date_type" value=2 <cfif #view.award_view_date_type# is 2>checked</cfif>>&nbsp;Contract End Date&nbsp;
                  </td></tr>

			  <tr><td class="feed_option" width=100><b>Value Range</b></td>
				   <td class="text_xsmall">From:&nbsp;&nbsp;<input type="number" style="width: 130px;" name="award_view_amount_from" value=#view.award_view_amount_from#>&nbsp;To:&nbsp;<input type="number" name="award_view_amount_to" value=#view.award_view_amount_to# style="width: 130px;" ></td></tr>
              </cfoutput>

              <tr><td height=10></td></tr>

			  <tr><td class="feed_option" width=100 valign=top><b>Department</b></td>
				   <td class="feed_option">
				   <select name="award_view_agency" style="width: 400px;" required>
				   <cfoutput query="departments">
				    <option value="#department_code#" <cfif #view.award_view_agency# is #department_code#>selected</cfif>>#department_name#
				   </cfoutput>
				   </select>&nbsp;*
				   </td></tr>

			  <cfoutput>

			  <tr><td class="feed_option" width=150><b>Keyword</b></td>
				  <td><input type="text" name="award_view_keyword" size=40 maxlength="99" value="#view.award_view_keyword#" style="width: 350px;"></td></tr>

              </cfoutput>

			  <tr><td height=10>&nbsp;</td></tr>
			  <tr><td class="text_xsmall" colspan=2><i>* - required field<br>** - select no values to get all</i></td></tr>
			  <tr><td height=10>&nbsp;</td></tr>
			  <tr><td colspan=2>
			  <input class="button_blue" type="submit" name="button" value="Update Search">&nbsp;&nbsp;
			  <input class="button_blue" type="submit" name="button" value="Delete Search">&nbsp;&nbsp;
			  </td></tr>

			   </table>

          </td></tr>

          </table>

          <cfoutput>
          <input type="hidden" name="award_view_id" value=#award_view_id#>
          </cfoutput>

          </form>

      </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>