<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from search
  where search_usr_id = #session.usr_id# and
        search_id = #search_id#
  order by search_updated DESC
 </cfquery>

			 <cfset SB = #evaluate(
				detail.search_alaskan_native+
				detail.search_american_indian+
				detail.search_indian_tribe+
				detail.search_native_hawaiian,
				detail.search_tribally+
				detail.search_veteran_owned+
				detail.search_sd_veteran_owned+
				detail.search_woman_owned_b+
				detail.search_woman_owned_sb+
				detail.search_edvosb+
				detail.search_jv_woman_owned+
				detail.search_jv_edwosb+
				detail.search_minority+
				detail.search_saiaob+
				detail.search_apao+
				detail.search_black_american+
				detail.search_hispanic_american+
				detail.search_native_american+
				detail.search_other_minority)#
			 >

              <cfif #sb# GT 0>

              <cfset query = "and (">

				<cfif detail.search_alaskan_native is 1>
				 <cfset query = query & "alaskan_native_owned_corporation_or_firm = 't' or ">
				</cfif>

				<cfif detail.search_american_indian is 1>
				 <cfset query = query & "american_indian_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_indian_tribe is 1>
				 <cfset query = query & "indian_tribe_federally_recognized = 't' or ">
				</cfif>

				<cfif detail.search_native_hawaiian is 1>
				 <cfset query = query & "native_hawaiian_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_tribally is 1>
				 <cfset query = query & "tribally_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_veteran_owned is 1>
				 <cfset query = query & "veteran_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_sd_veteran_owned is 1>
				 <cfset query = query & "service_disabled_veteran_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_woman_owned_b is 1>
				 <cfset query = query & "woman_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_woman_owned_sb is 1>
				 <cfset query = query & "women_owned_small_business = 't' or ">
				</cfif>

				<cfif detail.search_edvosb is 1>
				 <cfset query = query & "economically_disadvantaged_women_owned_small_business = 't' or ">
				</cfif>

				<cfif detail.search_jv_woman_owned is 1>
				 <cfset query = query & "joint_venture_women_owned_small_business = 't' or ">
				</cfif>

				<cfif detail.search_jv_edwosb is 1>
				 <cfset query = query & "joint_venture_economic_disadvantaged_women_owned_small_bus = 't' or ">
				</cfif>

				<cfif detail.search_minority is 1>
				 <cfset query = query & "minority_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_saiaob is 1>
				 <cfset query = query & "subcontinent_asian_asian_indian_american_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_apao is 1>
				 <cfset query = query & "asian_pacific_american_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_black_american is 1>
				 <cfset query = query & "black_american_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_hispanic_american is 1>
				 <cfset query = query & "hispanic_american_owned_business = 't' or ">
				</cfif>

				<cfif detail.search_native_american is 1>
				 <cfset query = query & "native_american_owned_business = 't' or ">
			    </cfif>

				<cfif detail.search_other_minority is 1>
				 <cfset query = query & "other_minority_owned_business = 't' or ">
				</cfif>

		      <cfset query_length = len(query)>
		      <cfset final_query = left(query,evaluate(query_length-3))>
		      <cfset final_query = final_query & ")">

              </cfif>

 <cfquery name="summary" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from award_data
  where (action_date >= '#detail.search_from#'and action_date <= '#detail.search_to#')

<!--- NAICS Filter --->

   <cfif listlen(detail.search_naics_list) GT 0>
	<cfif listlen(detail.search_naics_list) is 1>
	 and naics_code = '#detail.search_naics_list#'
	<cfelse>
	and (
	 <cfset #ncounter# = 1>
	 <cfloop index="nelement" list=#detail.search_naics_list#>
	   naics_code = '#trim(nelement)#'
	   <cfif #ncounter# LT listlen(detail.search_naics_list)> or </cfif>
	   <cfset #ncounter# = #ncounter# + 1>
	 </cfloop>
	 )
  </cfif>
 </cfif>

 <!--- Agency Filter --->

	  <cfif listlen(detail.search_agency_list) GT 0>
	   <cfif listlen(detail.search_agency_list) is 1>
		and awarding_sub_agency_code = '#detail.search_agency_list#'
	   <cfelse>
	   and (
		<cfset #acounter# = 1>
		<cfloop index="aelement" list=#detail.search_agency_list#>
		  awarding_sub_agency_code = '#trim(aelement)#'
		  <cfif #acounter# LT listlen(detail.search_agency_list)> or </cfif>
		  <cfset #acounter# = #acounter# + 1>
		</cfloop>
		)
	 </cfif>
	 </cfif>

   <!--- State Filter --->

	  <cfif listlen(detail.search_state_list) GT 0>
	   <cfif listlen(detail.search_state_list) is 1>
		and recipient_state_code = '#detail.search_state_list#'
	   <cfelse>
		and (
		<cfset #scounter# = 1>
		<cfloop index="selement" list=#detail.search_state_list#>
		  recipient_state_code = '#trim(selement)#'
		  <cfif #scounter# LT listlen(detail.search_state_list)> or </cfif>
		  <cfset #scounter# = #scounter# + 1>
		</cfloop>
		)
	 </cfif>
	 </cfif>

   <!--- Zip Filter --->

	  <cfif listlen(detail.search_zip_list) GT 0>
	   <cfif listlen(detail.search_zip_list) is 1>
		and recipient_zip_4_code like '#detail.search_state_list#%'
	   <cfelse>
		and (
		<cfset #zcounter# = 1>
		<cfloop index="zelement" list=#detail.search_zip_list#>
		  recipient_zip_4_code like '#zelement#%'
		  <cfif #zcounter# LT listlen(detail.search_zip_list)> or </cfif>
		  <cfset #zcounter# = #zcounter# + 1>
		</cfloop>
		)
	 </cfif>
	 </cfif>

             <!--- Small Business Classification Filter --->

              <cfif #sb# GT 0>

               <cfoutput>#PreserveSingleQuotes(final_query)#</cfoutput>

              </cfif>

 order by action_date DESC

 </cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 	  <div class="exchange_filter_box">

	      <cfinclude template="selection.cfm">

	  </div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" colspan=2><cfoutput>#detail.search_name#</cfoutput></td>
               <td align=right class="feed_option"><a href="index.cfm">Return</a></td>
           </tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
               <td class="text_xsmall"><b>Action Date</b></td>
               <td class="text_xsmall"><b>Agency</b></td>
               <td class="text_xsmall"><b>Office</b></td>
               <td class="text_xsmall"><b>Vendor</b></td>
               <td class="text_xsmall"><b>DUNS</b></td>
               <td class="text_xsmall"><b>NAICS</b></td>
               <td class="text_xsmall"><b>Contract</b></td>
               <td class="text_xsmall"><b>Type of Set Aside</b></td>
               <td class="text_xsmall"><b>PoP Start</b></td>
               <td class="text_xsmall"><b>PoP End</b></td>
               <td class="text_xsmall" align=right><b>Amount</b></td>
           </tr>

		   <cfset counter = 0>

           <cfoutput query="summary">

			   <cfif #counter# is 0>
				<tr bgcolor="ffffff">
			   <cfelse>
				<tr bgcolor="e0e0e0">
			   </cfif>

               <td class="text_xsmall" width=75>#dateformat(action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall">#awarding_sub_agency_name#</td>
               <td class="text_xsmall">#awarding_office_name#</td>
               <td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
               <td class="text_xsmall" width=60>#recipient_duns#</td>
               <td class="text_xsmall" width=60>#naics_code#</td>
               <td class="text_xsmall">

               <cfif #type_of_contract_pricing# is "Firm Fixed Price">
                FFP
               <cfelseif #type_of_contract_pricing# is "ORDER DEPENDENT (IDV ALLOWS PRICING ARRANGEMENT TO BE DETERMINED SEPARATELY FOR EACH ORDER)">
                Order Dependent
               <cfelseif #type_of_contract_pricing# is "Cost Plus Fixed Fee">
                CPFF
               <cfelseif #type_of_contract_pricing# is "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT">
                FFP (EPA)
               <cfelse>
               #type_of_contract_pricing#
               </cfif>
               </td>
               <td class="text_xsmall">#type_of_set_aside#</td>
               <td class="text_xsmall" width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" width=75>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
            </tr>

           <cfif #counter# is 0>
            <cfset #counter# = 1>
           <cfelse>
            <cfset #counter# = 0>
           </cfif>

           </cfoutput>

          </table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>