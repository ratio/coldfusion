<cfinclude template="/exchange/security/check.cfm">

<cfquery name="view" datasource="#client_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_view
 where award_view_id = #award_view_id# and
       award_view_usr_id = #session.usr_id#
</cfquery>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="departments" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from department
  order by department_name
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from agency
  order by agency_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">EDIT SEARCH</td><td align=right class="feed_sub_header">
           <cfoutput>
           <cfif isdefined("loc")>
	         <a href="/exchange/awards/award_search_run.cfm?award_view_id=#award_view_id#"><img src="/images/delete.png" width=20 alt="Close" title="Close" border=0></a>
           <cfelse>
	         <a href="/exchange/awards/">Return</a>
           </cfif>
           </cfoutput>
           </td></tr>
           <tr><td class="feed_sub_header" style="font-weight: normal;">To modify your saved search, please fill out the below fields and click Update Search.</td></tr>
           <tr><td colspan=2><hr></td></tr>

           <cfif isdefined("u")>
            <tr><td class="feed_sub_header"><font color="red">Date range is more than 3 years.</font></td></tr>
           </cfif>

           <tr><td height=10></td></tr>
          </table>

          <form name="autoSelectForm" action="create_view_db.cfm" method="post">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <cfoutput>

			   <tr><td class="feed_sub_header" width=150>Search Name</td>
				   <td><input type="text" class="input_text" name="award_view_name" size=40 value="#view.award_view_name#" required style="width: 350px;">&nbsp;</td></tr>

			   <tr><td class="feed_sub_header" width=150 valign=top>Description</td>
				   <td><textarea class="input_textarea" name="award_view_desc" cols=38 rows=6 style="width: 350px;">#view.award_view_desc#</textarea></td></tr>

              </cfoutput>

		      <tr><td class="feed_sub_header" valign=top>Set Aside</td>
		          <td class="feed_option">

				   <select name="award_view_set_aside" class="input_select" style="width: 250px;">
					   <option value="0">All
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #view.award_view_set_aside# is #set_aside_code#>selected</cfif>>#set_aside_name#
					   </cfoutput>
				   </select></td></tr>

		      <tr><td class="feed_sub_header" valign=top>State</td>
		          <td class="feed_option">

				   <select name="award_view_state" class="input_select" style="width: 250px;">
				   <option value="0">All
					   <cfoutput query="states">
					    <option value="#state_abbr#" <cfif #view.award_view_state# is #state_abbr#>selected</cfif>>#state_name#
					   </cfoutput>
				   </select></td></tr>

              <tr><td></td>
                  <td class="feed_option">
                  <input type="radio" name="award_view_location_type" value=1 <cfif #view.award_view_location_type# is 1>checked</cfif>>&nbsp;Contractor State&nbsp;
                  <input type="radio" name="award_view_location_type" value=2 <cfif #view.award_view_location_type# is 2>checked</cfif>>&nbsp;Place of Performance&nbsp;
                  </td></tr>

              <cfoutput>

			  <tr><td class="feed_sub_header" width=100>NAICS Code(s)<sup>1</sup></td>
				   <td><input class="input_text" type="text" name="award_view_naics" value="#view.award_view_naics#" style="width: 250px;"><img src="/images/icon_search.png" style="cursor: pointer;" width=20 border=0 hspace=10 alt="NAICS Code Lookup" title="NAICS Code Lookup" onclick="window.open('/exchange/opps/naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;"></a></td></tr>

			  <tr><td class="feed_sub_header" width=100>PSC Code(s)<sup>1</sup></td>
				   <td><input class="input_text" type="text" name="award_view_psc" value="#view.award_view_psc#" style="width: 250px;"><img src="/images/icon_search.png" style="cursor: pointer;" hspace=10 width=20 border=0 alt="PSC Lookup" title="PSC Lookup"  onclick="window.open('/exchange/opps/psc_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;"></a></td></tr>

              </cfoutput>

			  </table>

          </td><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <cfoutput>

			  <tr><td class="feed_sub_header" width=150>Vendor(s) DUNS <sup>1</sup></td>
				   <td><input class="input_text" type="text" name="award_view_duns" value="#view.award_view_duns#" maxlength="599" style="width:382px;"></td></tr>

			  <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Date Range</b><br><span class="link_small_gray">Max of 3 years</span></td>
				   <td class="feed_option"><b>From:</b>&nbsp;&nbsp;<input class="input_date" style="width: 155px;" type="date" name="award_view_date_from" required value="#view.award_view_date_from#">&nbsp;<b>To:</b>&nbsp;&nbsp;<input class="input_date" type="date" name="award_view_date_to" style="width:155px;" required value="#view.award_view_date_to#"></td></tr>
		      </cfoutput>
              <tr><td></td>
                  <td class="feed_option">
                  <input type="radio" name="award_view_date_type" value=1 <cfif #view.award_view_date_type# is 1>checked</cfif>>&nbsp;Award Date&nbsp;
                  <input type="radio" name="award_view_date_type" value=2 <cfif #view.award_view_date_type# is 2>checked</cfif>>&nbsp;Contract End Date&nbsp;
                  </td></tr>
              <tr><td height=5></td></tr>

			  <tr><td class="feed_sub_header">Value Range</td>
				   <td class="feed_option"><b>From:</b>&nbsp;&nbsp;<input type="number" class="input_date" style="width: 155px;" name="award_view_amount_from" value=#view.award_view_amount_from#>&nbsp;<b>To:</b>&nbsp;&nbsp;<input type="number" class="input_date" name="award_view_amount_to" style="width: 155px;" value=#view.award_view_amount_to#></td></tr>

              <tr><td height=10></td></tr>

			  <tr><td class="feed_sub_header">Department</td>
				   <td class="feed_option">
				   <select name="award_view_agency" class="input_select" style="width: 382px;">
				    <option value=0>All Departments
				   <cfoutput query="departments">
				    <option value="#department_code#" <cfif #view.award_view_agency# is #department_code#>selected</cfif>>#department_name#
				   </cfoutput>
				   </select>
				   </td></tr>
              <cfoutput>
			  <tr><td class="feed_sub_header">Keyword</td>
				  <td><input type="text" class="input_text" value='#replaceNoCase(view.award_view_keyword,'"','',"all")#' name="award_view_keyword" maxlength="99" style="width: 383px;"></td></tr>

              <input type="hidden" name="award_view_id" value=#award_view_id#>

              <cfif isdefined("loc")>
               <input type="hidden" name="loc" value=#loc#>
              </cfif>

              </cfoutput>
			  <tr><td height=20></td></tr>
			  <tr><td colspan=2>
			  <input class="button_blue_large" type="submit" name="button" value="Update Search">&nbsp;&nbsp;
			  <input class="button_blue_large" type="submit" name="button" value="Delete Search"  onclick="return confirm('Delete Saved Search?\r\nAre you sure you want to delete the saved search?');">
			  </td></tr>

			   </table>

          </td></tr>

          </table>

          </form>

      <script type="text/javascript">
       resetForm(document.autoSelectForm);
      </script>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="link_small_gray"><sup>1</sup>&nbsp;&nbsp;For multiple DUNS Numbers or NAICS or PSC codes, seperate values with a comma (i.e., 544111,643122)</td></tr>
       </table>


       </td></tr>



       </table>

	  </div>

	  </td></tr>

 </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>