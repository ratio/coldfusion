<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 update award_view
 set award_view_last_ran = #now()#
 where award_view_id = #award_view_id# and
       award_view_usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

   <cfquery name="view" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select * from award_view
    where award_view_usr_id = #session.usr_id# and
          award_view_id = #award_view_id#
   </cfquery>

   <cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select * from set_aside
    where set_aside_code = '#view.award_view_set_aside#'
   </cfquery>

   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select * from department
    where department_code = '#view.award_view_agency#'
   </cfquery>

   <!--- Build Query --->

   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select action_date, id, award_id_piid, modification_number, recipient_duns, recipient_name, recipient_city_name, recipient_state_code, award_description,
	awarding_agency_name, awarding_sub_agency_name, awarding_office_name, naics_code, product_or_service_code, type_of_set_aside, type_of_contract_pricing, period_of_performance_start_date,
    period_of_performance_current_end_date, primary_place_of_performance_city_name, primary_place_of_performance_state_code, federal_action_obligation

	from award_data

    <cfif #view.award_view_date_type# is 1>
     where action_date between '#view.award_view_date_from#' and '#view.award_view_date_to#'
	<cfelse>
	 where period_of_performance_current_end_date between '#view.award_view_date_from#' and '#view.award_view_date_to#'
	</cfif>

	and awarding_agency_code in ('#view.award_view_agency#')

    <cfif #view.award_view_location_type# is 1>
     <cfif #view.award_view_state# is not 0>
      and recipient_state_code = '#view.award_view_state#'
     </cfif>
    <cfelse>
     <cfif #view.award_view_state# is not 0>
      and primary_place_of_performance_state_code = '#view.award_view_state#'
     </cfif>
    </cfif>

    <cfif #view.award_view_set_aside# is not 0>
     and type_of_set_aside_code = '#view.award_view_set_aside#'
    </cfif>

    <cfif #view.award_view_amount_from# is "" and #view.award_view_amount_to# is "">

    <cfelse>

     <cfif #view.award_view_amount_from# is "">
      and federal_action_obligation <= #view.award_view_amount_to#
     <cfelse>
      <cfif #view.award_view_amount_to# is "">
       and federal_action_obligation >= #view.award_view_amount_from#
      <cfelse>
       and ((federal_action_obligation >= #view.award_view_amount_from#) and (federal_action_obligation <= #view.award_view_amount_to#))
      </cfif>
     </cfif>

    </cfif>

    <cfif #listlen(view.award_view_naics)# GT 0>
		<cfif #listlen(view.award_view_naics)# GT 1>
		<cfset ncounter = 1>
		and (
         <cfloop index="nc" list="#view.award_view_naics#">
           (naics_code = '#nc#')
           <cfif ncounter LT #listlen(view.award_view_naics)#> or</cfif>
           <cfset ncounter = ncounter + 1>
         </cfloop>
         )
		<cfelse>
		 and naics_code = '#view.award_view_naics#'
		</cfif>
    </cfif>

    <cfif #listlen(view.award_view_psc)# GT 0>
		<cfif #listlen(view.award_view_psc)# GT 1>
		<cfset pcounter = 1>
		and (
         <cfloop index="pc" list="#view.award_view_psc#">
           (product_or_service_code = '#pc#')
           <cfif pcounter LT #listlen(view.award_view_psc)#> or</cfif>
           <cfset pcounter = pcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and product_or_service_code = '#view.award_view_psc#'
		</cfif>
    </cfif>

    <cfif #listlen(view.award_view_duns)# GT 0>
		<cfif #listlen(view.award_view_duns)# GT 1>
		<cfset dcounter = 1>
		and (
         <cfloop index="dc" list="#view.award_view_duns#">
           (recipient_duns = '#dc#')
           <cfif dcounter LT #listlen(view.award_view_duns)#> or</cfif>
           <cfset dcounter = dcounter + 1>
         </cfloop>
         )
		<cfelse>
		 and recipient_duns = '#view.award_view_duns#'
		</cfif>
    </cfif>

   <cfif #view.award_view_keyword# is not "">
    and award_description like '%#view.award_view_keyword#%'
   </cfif>

   <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by recipient_state_code ASC, recipient_city_name ASC
	<cfelseif #sv# is 50>
	 order by recipient_state_code ASC, recipient_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC

	</cfif>

   <cfelse>
	 order by action_date DESC
   </cfif>

   </cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
            </cfif>

<cfparam name="URL.PageId" default="0">
<cfset RecordsPerPage = 250>
<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
<cfset EndRow = StartRow+RecordsPerPage-1>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <cfoutput>
           <tr><td class="feed_header" colspan=2>#view.award_view_name#</td><td class="feed_option" align=right><a href="edit_view.cfm?award_view_id=#award_view_id#">Edit View</a>&nbsp;|&nbsp;<a href="index.cfm">Return</a></td></tr>
           <tr><td class="feed_option">#view.award_view_desc#</td></td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_option" colspan=2><b>Total Awards: </b><cfoutput>#numberformat(agencies.recordcount,'999,999')#</cfoutput></td>

             <td class="feed_option" align=right colspan=2 valign=top>

              <cfoutput>
              <a href="run.cfm?award_view_id=#award_view_id#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><b>Export to Excel</b></a>
              &nbsp;|&nbsp;
              <a href="tam_dashboard.cfm?award_view_id=#award_view_id#"><b>TAM Dashboard</b></a>
              </cfoutput>

              </td>

           </tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_option" colspan=4>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr>
                <td class="feed_option"><b>Department</b></td>
                <td class="feed_option"><b>Dates</b></td>
                <td class="feed_option"><b>Location</b></td>
                <td class="feed_option"><b>Set Aside</b></td>
                <td class="feed_option"><b>NAICS</b></td>
                <td class="feed_option"><b>PSC</b></td>
                <td class="feed_option"><b>Vendor DUNS</b></td>
                <td class="feed_option"><b>Keyword</b></td>
                <td class="feed_option" align=right><b>Amount</b></td>
            </tr>
            <tr>
                <td class="feed_option">#department.department_name#</td>
                <td class="feed_option">

                <cfif view.award_view_date_type is 1>
                Award Date -
                <cfelse>
                Contract End Date -
                </cfif>

                #dateformat(view.award_view_date_from,'mm/dd/yyyy')# to #dateformat(view.award_view_date_to,'mm/dd/yyyy')#

                <td class="feed_option">
                <cfif #view.award_view_location_type# is 1>
                 Contractor Location - <cfif #view.award_view_state# is 0>All<cfelse>#view.award_view_state#</cfif>
                <cfelse>
                 Place of Performance - <cfif #view.award_view_state# is 0>All<cfelse>#view.award_view_state#</cfif>
                </cfif>
                </td>

                <td class="feed_option"><cfif #view.award_view_set_aside# is 0>No Set Aside<cfelse>#set_aside.set_aside_name#</cfif></td>
                <td class="feed_option"><cfif #view.award_view_naics# is "">All<cfelse>#view.award_view_naics#</cfif></td>
                <td class="feed_option"><cfif #view.award_view_psc# is "">All<cfelse>#view.award_view_psc#</cfif></td>
                <td class="feed_option">
                <cfif #view.award_view_duns# is "">
                All
                <cfelse>
                <cfif listlen(view.award_view_duns) GT 3>
                 #listlen(view.award_view_duns)# DUNS Numbers Selected
                <cfelse>
                #view.award_view_duns#
                </cfif>
                </cfif></td>
                <td class="feed_option"><cfif #view.award_view_keyword# is "">No Keyword<cfelse>#view.award_view_keyword#</cfif></td>
                <td class="feed_option" align=right>
                <cfif #view.award_view_amount_from# is "">Undefined<cfelse>#numberformat(view.award_view_amount_from,'$999,999,999')#</cfif>
                &nbsp;-&nbsp;
                <cfif #view.award_view_amount_to# is "">&nbsp;Undefined<cfelse>#numberformat(view.award_view_amount_to,'$999,999,999')#</cfif>
            </tr>

           </table>

               </td></tr>

          </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td colspan=16><hr></td></tr>
          <tr><td height=10></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_option">No awards or mods were found with the information you entered.</td></tr>
          <cfelse>

           <tr><td class="feed_option" colspan=16 align=right>
              </b>

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&award_view_id=#award_view_id#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
           </td></tr>

           <tr><td>&nbsp;</td></tr>

          <cfoutput>

			  <tr>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract ##</b></a></td>
				 <td class="text_xsmall"><b>Mod</b></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Vendor</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>City</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>State</b></a></td>
				 <td class="text_xsmall"><b>Award Description</b></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=15<cfelse><cfif #sv# is 15>sv=150<cfelse>sv=15</cfif></cfif>"><b>Agency</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Office</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PSC</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Set Aside</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Pricing</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP Start</b></a></td>
				 <td class="text_xsmall"><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PoP End</b></a></td>
				 <td class="text_xsmall" align=right><a href="run.cfm?award_view_id=#award_view_id#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Obligation</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

           	   <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff">
			   <cfelse>
				<tr bgcolor="e0e0e0">
			   </cfif>

			   <cfoutput>

				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#agencies.award_id_piid#</a></td>
				   <td class="text_xsmall" valign=top>#modification_number#</td>
				   <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#agencies.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#agencies.recipient_name#</a></td>
                   <cfif view.award_view_location_type is 1>
					   <td class="text_xsmall" valign=top>#agencies.recipient_city_name#</td>
					   <td class="text_xsmall" valign=top>#agencies.recipient_state_code#</td>
				   <cfelse>
					   <td class="text_xsmall" valign=top>#agencies.primary_place_of_performance_city_name#</td>
					   <td class="text_xsmall" valign=top>#agencies.primary_place_of_performance_state_code#</td>
				   </cfif>
				   <td class="text_xsmall" valign=top width=300>#agencies.award_description#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_sub_agency_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_office_name#</td>
				   <td class="text_xsmall" valign=top width=75>#agencies.naics_code#</td>
				   <td class="text_xsmall" valign=top width=50>#agencies.product_or_service_code#</td>
				   <td class="text_xsmall" valign=top width=50>#agencies.type_of_set_aside#</td>
				   <td class="text_xsmall" valign=top width=100>#agencies.type_of_contract_pricing#</td>
				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.period_of_performance_start_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top align=right width=75>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>
				 </tr>

			  </cfoutput>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

		  <cfset tot = tot + #agencies.federal_action_obligation#>

          </cfif>

		  <cfif CurrentRow eq EndRow>
		   <cfbreak>
		  </cfif>

          </cfloop>

          <tr><td colspan=16><hr></td></tr>
          <tr>
             <td class="feed_option"><b>Total</b></td>
             <td class="feed_option" colspan=15 align=right><b><cfoutput>#numberformat(tot,'$999,999,999')#</cfoutput></b></td>
          </tr>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>