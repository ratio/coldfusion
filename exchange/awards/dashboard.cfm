<cfinclude template="/exchange/security/check.cfm">

<cfquery name="cities" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select top(10) recipient_city_name, count(distinct(naics_code)) as naics_codes, count(recipient_duns) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_name)) as vendors, sum(federal_action_obligation) as total from award_data
where recipient_state_code = 'VA'
  and recipient_country_code = 'USA'
and ((action_date >= '10/1/2017') and (action_date <= '1/1/2018'))

<cfif session.set_aside_code is not 0>

 <cfif session.set_aside_code is 1>
  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
 <cfelse>
  and (type_of_set_aside_code =  '#session.set_aside_code#')
 </cfif>

</cfif>
group by recipient_city_name
order by total DESC
</cfquery>

<b>Award Value by City</b>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="cities">
			   ['#recipient_city_name#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			legend: { position: "none" },
			fontSize: 9,
			};

			var chart = new google.visualization.BarChart(document.getElementById('state_1'));

			chart.draw(data, options);
		  }
		</script>

    <div id="state_1" style="width: 500px; height: 300px;"></div>

<hr>

<!---

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select top(10) awarding_agency_code, awarding_agency_name, sum(federal_action_obligation) as total from award_data
where ((action_date >= '10/1/2017') and (action_date <= '1/1/2018'))

<cfif session.set_aside_code is not 0>

 <cfif session.set_aside_code is 1>
  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
 <cfelse>
  and (type_of_set_aside_code =  '#session.set_aside_code#')
 </cfif>

</cfif>
group by awarding_agency_code, awarding_agency_name
order by total DESC
</cfquery>

<b>Top 10 Agencies Award Value</b>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="agencies">
			   ['#awarding_agency_name#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			'legend'   : 'right',
			'title'    : '',
			'fontSize' : 8,
			'height'   : 300
			};

			var chart = new google.visualization.PieChart(document.getElementById('agency_1'));

			chart.draw(data, options);
		  }
		</script>

    <div id="agency_1"></div>

<hr>

<br>PSC by Agency


<cfquery name="psc_1" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select top(10) product_or_service_code, product_or_service_code_description, sum(federal_action_obligation) as total from award_data
where ((action_date >= '10/1/2017') and (action_date <= '1/1/2018'))

<cfif session.set_aside_code is not 0>

 <cfif session.set_aside_code is 1>
  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
 <cfelse>
  and (type_of_set_aside_code =  '#session.set_aside_code#')
 </cfif>

</cfif>
group by product_or_service_code, product_or_service_code_description
order by total DESC
</cfquery>

<b>Top 10 PSC Value (all awards)</b>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="psc_1">
			   ['#product_or_service_code_description#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			'legend'   : 'left',
			'title'    : '',
			'pieHole'  : 0.4,
			'fontSize' : 8,
			'height'   : 300
			};

        var chart = new google.visualization.PieChart(document.getElementById('psc_1_graph'));
        chart.draw(data, options);

		  }
		</script>

    <div id="psc_1_graph"></div>

<hr>


<cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select top(25) naics_code, naics_description, sum(federal_action_obligation) as total from award_data
where ((action_date >= '10/1/2017') and (action_date <= '1/1/2018'))

<cfif session.set_aside_code is not 0>

 <cfif session.set_aside_code is 1>
  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
 <cfelse>
  and (type_of_set_aside_code =  '#session.set_aside_code#')
 </cfif>

</cfif>
group by naics_code, naics_description
order by total DESC
</cfquery>

<b>NAICS Codes (top 25)</b>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="naics">
			   ['#naics_description#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			legend: { position: "none" },
			fontSize: 9,
			};

			var chart = new google.visualization.BarChart(document.getElementById('naics_1'));

			chart.draw(data, options);
		  }
		</script>

    <div id="naics_1" style="width: 1000px; height: 500px;"></div>

<hr>


<br>PSC by Agency


<cfquery name="set_1" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select type_of_set_aside_code, type_of_set_aside, sum(federal_action_obligation) as total from award_data
where ((action_date >= '10/1/2017') and (action_date <= '1/1/2018'))

group by type_of_set_aside_code, type_of_set_aside
order by total DESC
</cfquery>

<b>Contract Types</b>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="set_1">
			   ['#type_of_set_aside#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			'legend'   : 'left',
			'title'    : '',
			'pieHole'  : 0.4,
			'fontSize' : 8,
			'height'   : 300
			};

        var chart = new google.visualization.PieChart(document.getElementById('set_1_graph'));
        chart.draw(data, options);

		  }
		</script>

    <div id="set_1_graph"></div>

<hr>
--->