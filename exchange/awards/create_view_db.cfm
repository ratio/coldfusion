<cfinclude template="/exchange/security/check.cfm">

<cfset #value# = #award_view_date_to# - #award_view_date_from#>

<cfif #button# is "Create New Search">

<cfset search_string = #replace(award_view_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

<cfif #value# GT 1096>
 <cflocation URL="create_view.cfm?u=1" addtoken="no">
</cfif>

	<cfquery name="create" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert award_view
	  (
	  award_view_name,
	  award_view_hub_id,
	  award_view_keyword,
	  award_view_desc,
	  award_view_updated,
	  award_view_date_from,
	  award_view_date_to,
	  award_view_amount_from,
	  award_view_amount_to,
	  award_view_duns,
	  award_view_state,
	  award_view_naics,
	  award_view_psc,
	  award_view_set_aside,
	  award_view_agency,
	  award_view_location_type,
	  award_view_date_type,
	  award_view_usr_id)
	  values
	  (
	  '#award_view_name#',
	   #session.hub#,
	  '#search_string#',
	  '#award_view_desc#',
	   #now()#,
	  '#award_view_date_from#',
	  '#award_view_date_to#',
	   <cfif #award_view_amount_from# is "">null<cfelse>#award_view_amount_from#</cfif>,
	   <cfif #award_view_amount_to# is "">null<cfelse>#award_view_amount_to#</cfif>,
	  '#award_view_duns#',
	  '#award_view_state#',
	  '#award_view_naics#',
	  '#award_view_psc#',
	  '#award_view_set_aside#',
	  '#award_view_agency#',
	   #award_view_location_type#,
	   #award_view_date_type#,
	   #session.usr_id#)
	</cfquery>

<cflocation URL="/exchange/awards/index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update Search">

<cfif #value# GT 1825>
 <cflocation URL="/exchange/awards/edit_view.cfm?award_view_id=#award_view_id#&u=1" addtoken="no">
</cfif>

<cfset search_string = #replace(award_view_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update award_view
	  set award_view_name = '#award_view_name#',
		  award_view_desc = '#award_view_desc#',
		  award_view_date_from = '#award_view_date_from#',
		  award_view_date_to = '#award_view_date_to#',
		  award_view_location_type = #award_view_location_type#,
		  award_view_duns = '#award_view_duns#',
		  award_view_date_type = #award_view_date_type#,
		  award_view_amount_from = <cfif #award_view_amount_from# is "">null<cfelse>#award_view_amount_from#</cfif>,
		  award_view_amount_to = <cfif #award_view_amount_to# is "">null<cfelse>#award_view_amount_to#</cfif>,
		  award_view_state = '#award_view_state#',
		  award_view_naics = '#award_view_naics#',
		  award_view_psc = '#award_view_psc#',
		  award_view_keyword = '#search_string#',
		  award_view_set_aside = '#award_view_set_aside#',
		  award_view_agency = '#award_view_agency#',
		  award_view_updated = #now()#
	  where award_view_id= #award_view_id# and
			award_view_usr_id = #session.usr_id#
	</cfquery>

<cfif isdefined("loc")>
 <cflocation URL="/exchange/awards/award_search_run.cfm?award_view_id=#award_view_id#" addtoken="no">
</cfif>

<cflocation URL="/exchange/awards/index.cfm?u=3" addtoken="no">

<cfelseif #button# is "Delete Search">

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete award_view
		 where award_view_id = #award_view_id# and
		       award_view_usr_id = #session.usr_id#
        </cfquery>

<cflocation URL="/exchange/awards/index.cfm?u=4" addtoken="no">

</cfif>