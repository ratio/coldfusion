<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("keywords")>
 <cfset #session.search_keywords# = #keywords#>
 <cfset search_string = #keywords#>
<cfelse>
 <cfset search_string = #session.search_keywords#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>

           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;

           </td>
           <td align=right class="feed_option">

           <a href="/exchange/awards/">Return</a>

           </cfoutput>

          </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
             select id, award_description, award_id_piid, awarding_office_name, funding_office_name, action_date, federal_action_obligation, awarding_agency_name, awarding_sub_agency_name, recipient_name, recipient_duns from award_data
             where contains((award_description, awarding_office_name, funding_office_name, solicitation_identifier, award_id_piid, parent_award_id),'"#search_string#"')

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by award_id_piid ASC, modification_number
		    <cfelseif #sv# is 10>
             order by award_id_piid DESC, modification_number
		    <cfelseif #sv# is 2>
             order by action_date DESC
		    <cfelseif #sv# is 20>
             order by action_date ASC
		    <cfelseif #sv# is 3>
             order by recipient_name ASC
		    <cfelseif #sv# is 30>
             order by recipient_name DESC
		    <cfelseif #sv# is 4>
             order by awarding_agency_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_agency_name DESC
		    <cfelseif #sv# is 5>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 50>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 6>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 60>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 7>
             order by funding_office_name ASC
		    <cfelseif #sv# is 70>
             order by funding_office_name DESC
		    <cfelseif #sv# is 8>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 80>
             order by federal_action_obligation ASC
		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		   </cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
            </cfif>


			<cfparam name="URL.PageId" default="0">
			<cfset RecordsPerPage = 500>
			<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
			<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
			<cfset EndRow = StartRow+RecordsPerPage-1>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		 <cfoutput>

         <tr><td class="feed_option" colspan=7><b>Total Awards: #numberformat(agencies.recordcount,'999,999')#</b></td>


             <td class="feed_option" align=right colspan=2 valign=top>

              <cfoutput>
              <a href="index.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><b>Export to Excel</b></a>
              </cfoutput>

              </td>

         </tr>
         <tr><td class="feed_option" colspan=9 align=right>

         </cfoutput>

   				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>

         </td></tr>

         <tr><td colspan=9><hr></td></tr>
         <tr><td height=10></td></tr>

         <cfoutput>

          <tr>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Action Date</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Contract ##</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Vendor</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Department</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Agency</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Awarding Office</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Funding Office</b></a></td>
             <td class="text_xsmall"><b>Award Description</b></a></td>
             <td class="text_xsmall" align=right><a href="index.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Obligation</b></a></td>
          </tr>

          </cfoutput>

          <cfloop query="agencies">

		  <cfif CurrentRow gte StartRow >

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

           <cfoutput>

               <td class="text_xsmall" valign=top width=75>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=100><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#agencies.award_id_piid#</a></td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#agencies.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#agencies.recipient_name#</a></td>
               <td class="text_xsmall" valign=top>#agencies.awarding_agency_name#</td>
               <td class="text_xsmall" valign=top>#agencies.awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#agencies.awarding_office_name#</td>
               <td class="text_xsmall" valign=top>#agencies.funding_office_name#</td>
               <td class="text_xsmall" valign=top width=350>#left(agencies.award_description,300)#...</td>
               <td class="text_xsmall" valign=top width=75 align=right>#numberformat(agencies.federal_action_obligation,'$999,999,999,999')#</td>

             </cfoutput>

             </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

		  </cfif>

		  <cfif CurrentRow eq EndRow>
		   <cfbreak>
		  </cfif>

          </cfloop>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>