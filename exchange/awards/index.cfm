<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"award_search_name")>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id#
 order by award_view_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

    <script>
        function validateAndSend() {
            if (myForm.search_keyword.value == '' && myForm.naics.value == '') {
                alert('You must enter either a NAICS code or keyword.');
                return false;
            }
        }
    </script>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/components/recent_boards/index.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/awards/search_awards.cfm">
		</div>

   <!--- Get Last 100 viewed --->

   <cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    select distinct top(25) recent_award_id, recent_date from recent
	where recent_usr_id = #session.usr_id# and
          recent_hub_id = #session.hub# and
          recent_award_id is not null
    order by recent_date DESC
   </cfquery>

   <cfif recent.recordcount is 0>
    <cfset recent_list = 0>
   <cfelse>
    <cfset recent_list = valuelist(recent.recent_award_id)>
   </cfif>

   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
    select action_date, award_id_piid, modification_number, recipient_name, recipient_duns, recipient_city_name, recipient_state_code, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, funding_office_name, award_description, primary_place_of_performance_city_name, primary_place_of_performance_state_code, naics_code, product_or_service_code, product_or_service_code_description, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_current_end_date, federal_action_obligation, orgname_logo, id from award_data
    left join orgname on orgname_name = awarding_sub_agency_name
    where id in (#recent_list#)
   </cfquery>

   <cfif isdefined("export")>
	 <cfinclude template="/exchange/include/export_to_excel.cfm">
	</cfif>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif views.recordcount is 0>

			<form action="/exchange/awards/set_view.cfm" method="post">
			 <tr><td class="feed_header" valign=middle>Recently Viewed Awards <cfif agencies.recordcount GT 0>(Last <cfoutput>#agencies.recordcount#</cfoutput>)</cfif></td>
				 <td class="feed_sub_header" valign=middle align=right><b>Create New Search</b>&nbsp;&nbsp;
				 &nbsp;<input class="button_blue" type="submit" name="button" value="New">
				 </td></tr>
			 <tr><td colspan=2><hr></td></tr>
			</form>

        <cfelse>

			<form action="/exchange/awards/set_view.cfm" method="post">
			 <tr><td class="feed_header" valign=middle>Recently Viewed Awards <cfif agencies.recordcount GT 0>(Last <cfoutput>#agencies.recordcount#</cfoutput>)</cfif></td>
				 <td class="feed_sub_header" style="font-weight: normal;" valign=middle align=right><b>Saved Searches</b>&nbsp;&nbsp;
				 <select name="award_view_id" class="input_select" style="width: 200px;">
				 <cfoutput query="views">
				  <option value=#award_view_id#>#award_view_name#
				 </cfoutput>
				 &nbsp;<input class="button_blue" type="submit" name="button" value="Go">&nbsp;&nbsp;
					   <input class="button_blue" type="submit" name="button" value="New">
				 </td></tr>
			 <tr><td colspan=2><hr></td></tr>
			</form>

        </cfif>

        </table>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" valign=top>

          <cfif isdefined("u")>
           <cfif u is 3>
            <font color="green">Saved search has been updated.</font>
           <cfelseif u is 4>
            <font color="red">Saved search has been deleted.</font>
           <cfelseif u is 1>
            <font color="green">Saved search has been created.</font>
           </cfif>
          </cfif>

          </td><td align=right class="feed_sub_header" valign=top>

          <cfif agencies.recordcount GT 0>
			  <a href="/exchange/awards/recent_expanded.cfm"><img src="/images/icon_expand.png" width=20 hspace=10 alt="Expand" title="Expand" border=0></a>
			  <a href="/exchange/awards/recent_expanded.cfm">Expand</a>
			  <a href="/exchange/awards/index.cfm?export=1"><img src="/images/icon_export_excel.png" width=20 alt="Export to Excel" title="Export to Excel" hspace=10 border=0></a>
			  <a href="/exchange/awards/index.cfm?export=1">Export to Excel</a>

          </cfif>
          </td></tr>
        </table>

        <cfif agencies.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" style="font-weight: normal;">You have not viewed any Federal awards.</td></tr>
        </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td height=15></td></tr>

          <cfoutput>

			  <tr>
				 <td></td>
			 	 <td class="text_xsmall"><b>Department / Agency</b></td>
				 <td class="text_xsmall"><b>Office / Funding Office</b></td>
				 <td class="text_xsmall"><b>Vendor / Location</b></td>
				 <td class="text_xsmall"><b>Contract ## / Mod ##</b></td>
				 <td class="text_xsmall" align=center><b>PoP Start</b></td>
				 <td class="text_xsmall" align=center><b>PoP End</b></td>
				 <td class="text_xsmall" align=right><b>Obligation</b></td>
				 <td class="text_xsmall" align=right><b>Award Date</b></td>
				 <td></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>

           <cfoutput query="agencies">

			   <cfif counter is 0>
				<cfset bgc = "ffffff">
			   <cfelse>
			    <cfset bgc = "e0e0e0">
			   </cfif>

			   <tr bgcolor="#bgc#" height=50>

                 <td width=60>
				 <a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer" alt="#agencies.award_description#" title="#agencies.award_description#">
				<cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0 vspace=10>
				<cfelse>
				  <img src="#image_virtual#/#orgname_logo#" valign=top align=top width=40 border=0 vspace=10>
				</cfif>
				 </a>
				 </td>

				   <td class="text_xsmall" valign=middle>
				   <a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">
				   <b>#agencies.awarding_agency_name#<br>#agencies.awarding_sub_agency_name#</b>
				   </a></td>
				   <td class="text_xsmall" valign=middle>#agencies.awarding_sub_agency_name#<br>#agencies.funding_office_name#</td>
				   <td class="text_xsmall" valign=middle><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.recipient_name#</b></a><br>#recipient_city_name#, #recipient_state_code#</td>

				   <td class="text_xsmall" valign=middle><a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer" alt="#agencies.award_description#" title="#agencies.award_description#"><b>#agencies.award_id_piid#</b></a><br>#agencies.modification_number#</td>
				   <td class="text_xsmall" valign=middle align=center width=75>#dateformat(agencies.period_of_performance_start_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=middle align=center width=75>#dateformat(agencies.period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=middle align=right width=65>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>
				   <td class="text_xsmall" valign=middle align=right width=100>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
                   <td align=right width=40>
					<img src="/images/icon_pin.png" style="cursor: pointer;" width=25 alt="Pin to Opportunity Board" title="Pin to Opportunity Board" onclick="window.open('/exchange/include/save_opp.cfm?id=#agencies.id#&t=award','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;">
			       </td>
				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

         </cfoutput>

	  </table>

	  </cfif>

       </td></tr>

       </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>