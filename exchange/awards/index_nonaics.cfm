<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.award_to")>
 <cfset #session.award_to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.award_from# = #dateformat(dateadd("d",-364,now()),'yyyy-mm-dd')#>
</cfif>

<html>

<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Federal Awards</td>
             <td class="feed_header" align=right></td></tr>
         <tr><td class="feed_option" colspan=2>The EXCHANGE contains over <b>23 million</b> Federal contract awards from <b>2013 - 2018</b>.  To view awards please create a custom view or select from one of our standard views or search options.</td></tr>
         <tr><td>&nbsp;</td></tr>
        </table>

		<cfset from = dateadd('yyyy',-1,now())>

		<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select * from department
		  order by department_name
		</cfquery>

		<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from state
		 order by state_name
		</cfquery>

		<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select * from set_aside
		 order by set_aside_name
		</cfquery>

         <form action="search_set.cfm" method="post">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td class="feed_header" colspan=5><b>Award Search</b></td></tr>
			 <tr><td height=10></td></tr>

          <tr>

              <td>
                  <td class="feed_option"><b>Department</b></td>
                  <td class="feed_option">
                  <select name="dept" style="width:200px;">
                   <cfoutput query="dept">
                   <option value="#department_code#" <cfif isdefined("session.award_dept") and session.award_dept is "#department_code#">selected</cfif>>#department_name#
                   </cfoutput>
                  </select>
              </td>

              <td>
                  <td class="feed_option"><b>NAICS Code(s) *</b></td>
                  <td class="feed_option"><input type="text" name="naics" style="width:150px;" maxlength="299" <cfif isdefined("session.award_naics")>value="<cfoutput>#session.award_naics#</cfoutput>"</cfif>></td>
              </td>

              <td>
                  <td class="feed_option"><b>From</b></td>
                  <td class="feed_option"><input type="date" name="from" required value="<cfoutput>#dateformat(session.award_from,'yyyy-mm-dd')#</cfoutput>" required></td>
              </td>

              <td>
                  <td class="feed_option"><b>Keyword / Soliciation #</b></td>
                  <td class="feed_option"><input type="text" name="search_keyword" style="width:222px;" maxlength="100" ></td>
              </td>

            </tr>

          <tr>

              <td>
                  <td class="feed_option"><b>Place of Performance</b></td>
                  <td class="feed_option">
                  <select name="state" style="width: 200px;">
                   <option value=0>All
                   <cfoutput query="states">
                    <option value="#state_abbr#" <cfif isdefined("session.award_state") and session.award_state is #state_abbr#>selected</cfif>>#state_name#
                   </cfoutput>
                  </select>
              </td>

              <td>
                  <td class="feed_option"><b>Product or Service Code(s) *</b></td>
                  <td class="feed_option"><input type="text" name="psc" style="width: 150px;" maxlength="299" <cfif isdefined("session.award_psc")>value="<cfoutput>#session.award_psc#</cfoutput>"</cfif>></td>
              </td>

              <td>
                  <td class="feed_option"><b>To</b></td>
                  <td class="feed_option"><input type="date" name="to" required value="<cfoutput>#dateformat(session.award_to,'yyyy-mm-dd')#</cfoutput>" required></td>
              </td>

              <td>
                  <td class="feed_option"><b>Small Business</b></td>
                  <td class="feed_option">
                                    <select name="setaside" style="width:150px;">
				                     <option value=0>No Preference
				                     <cfoutput query="setaside">
				                     <option value="#set_aside_code#" <cfif isdefined("session.award_setaside") and session.award_setaside is '#set_aside_code#'>selected</cfif>>#set_aside_name#
				                     </cfoutput>
                  </select>

               &nbsp;&nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Search">

               </td></tr>

         <tr><td height=5></td></tr>
         <tr><td colspan=14 class="feed_option">* - To search multiple values, seperate codes with a commma and no spaces.  For a complete code list, click here - <a href="https://www.census.gov/cgi-bin/sssdaicsaicsrch?chart=2017" target="_blank" rel="noopener" rel="noreferrer">NAICS Code list</a>, or <a href="https://psctool.us/" target="_blank" rel="noopener" rel="noreferrer">Product or Service Code list</a>.</td></tr>
         <tr><td height=15></td></tr>
         <tr><td colspan=14><hr></td></tr>
         </table>

        </form>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td valign=top width=50%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td class="feed_header"><b>Standard Views</b></td></tr>
			 <tr><td>&nbsp;</td></tr>

			 <tr><td class="feed_option"><a href="/exchange/awards/agency/"><b>By Department</b></a></td>
			 <tr><td class="feed_option"><a href="/exchange/awards/state/"><b>By State</b></a></td>
			 <tr><td class="feed_option"><a href="/exchange/awards/naics/"><b>By NAICS Code</b></a></td>
			 <tr><td class="feed_option"><a href="/exchange/awards/product/"><b>By Product or Service Code</b></a></td>
			 <tr><td class="feed_option"><a href="/exchange/awards/sb/"><b>By Set Asides</b></a></td>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td class="feed_header" height=10><b>By Contract, Award, or Solicitation Number</b></td>

			 <form action="/exchange/awardsumber/" method="post">
			 <tr><td class="feed_option">
			 <input name="number" class="input_white" style="width:200px;" type="text" size=30 required>&nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 25px; width: 60px;" type="submit" name="button" value="Search">
			 </td></tr>
			 </form>

			 <tr><td class="text_xsmall">Note - some contracts, awards, or solicitation identifiers do not include dashes.  Please remove if needed.</td></tr>
			 <tr><td>&nbsp;</td></tr>
			 <tr><td class="feed_header"><b>By Keyword</b></td></tr>

			 <form action="/exchange/awards/search/" method="post">
						<tr><td class="feed_option"><input class="input_white" style="width:300px;" name="keywords" type="text" required>&nbsp;&nbsp;<input class="button_blue" style="font-size: 11px; height: 25px; width: 60px;" type="submit" name="button" value="Search"></td></tr>
			 </form>

			 <tr><td>&nbsp;</td></tr>
			 <tr><td class="text_xsmall"><b>Note</b> - Due to the large number of awards the Exchange maintains some searches may be limited to 1 year.</td></tr>
			</table>

         <td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header"><b>Custom Views</b></td><td align=right class="feed_option"><a href="create_view.cfm">Create View</a></td></tr>
			 <tr><td>&nbsp;</td></tr>
	     </table>

			 <cfquery name="views" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select * from award_view
			  where award_view_usr_id = #session.usr_id#
			  order by award_view_last_ran DESC
			 </cfquery>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif views.recordcount is 0>
              <tr><td class="feed_option">No custom views have been created.</td></tr>
             <cfelse>

             <cfoutput query="views">
              <tr><td width=30><a href="edit_view.cfm?award_view_id=#award_view_id#"><img src="/images/pencil.png" width=20 border=0 alt="Edit View" title="Edit View"></a></td>
                  <td class="feed_option"><a href="run.cfm?award_view_id=#award_view_id#"><b>#award_view_name#</b></a></td>
                  <td class="feed_option" align=right><b>Last Ran: </b><cfif #award_view_last_ran# is "">Never<cfelse>#dateformat(award_view_last_ran,'mm/dd/yyyy')# at #timeformat(award_view_last_ran)#</cfif></tr>
              <tr><td></td><td class="feed_option" colspan=2>#award_view_desc#</td></tr>
              <tr><td></td><td colspan=2><hr></td></tr>
             </cfoutput>

             </cfif>

         </table>

           </td><td width=30>&nbsp;</td></tr>
        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>