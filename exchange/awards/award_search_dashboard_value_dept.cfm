<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head><div class="center">
<body class="body">

<cfset session.view = 2>

<!--- Build Query --->

   <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select count(id) as total, sum(base_and_all_options_value) as amount from award_data

	  where (((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

   </cfquery>

  <!--- NAICS --->

  <cfquery name="naics_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select naics_code, naics_description, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

	  where (((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by naics_code, naics_description
     order by amount DESC

   </cfquery>

  <!--- PSC --->

  <cfquery name="psc_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) product_or_service_code, product_or_service_code_description, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

	  where (((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by product_or_service_code, product_or_service_code_description
     order by amount DESC

   </cfquery>

  <!--- Sub Agency --->

  <cfquery name="sub_agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select awarding_agency_code, awarding_agency_name, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

	  where (((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by awarding_agency_code, awarding_agency_name
     order by amount DESC

   </cfquery>

  <!--- Recipient --->

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

	  where (((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by recipient_duns, recipient_name
     order by amount DESC

   </cfquery>


  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("session.award_search_name")>

		  <cfoutput>

           <tr><td class="feed_header"><b>#ucase(session.award_search_name)#</b></td>
                <td class="feed_option" align=right><a href="award_search.cfm">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header"><b>TOTAL ADDRESSABLE MARKET (TAM) DASHBOARD - By Total Allocations</b></td>
               <td class="feed_option" align=right></td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header"><b><a href="award_search_dashboard.cfm">By Actual Obligations</a>&nbsp;|&nbsp;<a href="award_search_dashboard_value.cfm"><u>By Total Allocations</u></a></td></tr>

          </cfoutput>

          <cfelse>

           <tr><td class="feed_header"><b>TOTAL ADDRESSABLE MARKET (TAM) DASHBOARD - By Total Allocations</b></td>
                <td class="feed_sub_header" align=right><a href="/exchange/awards/award_search.cfm">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header"><a href="/exchange/awards/award_search_dashboard_value.cfm"><u>By Total Allocations</u></a>&nbsp;|&nbsp;<a href="/exchange/awards/award_search_dashboard_dept.cfm">By Total Obligations</a></td></tr>

          </cfif>

          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_sub_header">Total Awards: #numberformat(awards.total,'999,999')#</td></tr>
            <tr><td class="feed_sub_header">Total Allocations: #numberformat(awards.amount,'$999,999,999')#</td></tr>
            <tr><td height=10></td></tr>
            <tr><td><hr></td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td>&nbsp;</td></tr>

           <tr>
              <td class="feed_sub_header" align=center>By NAICS Code (Top 25)</td>
              <td></td>
              <td class="feed_sub_header" align=center>By Product or Service Code (Top 25)</td>
           </tr>

           <tr><td valign=top width align=center width=50%>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="naics_total">
 						  <cfif amount GTE 0>
							  <cfif counter LTE 25>
							   ['#naics_description#',#round(amount)#,'/exchange/awards/award_search_detail_naics_dept.cfm?val=#naics_code#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="naics_graph" style="width: 100%;"></div>

                </td><td width=50>&nbsp;</td><td valign=top align=center width=50%>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="psc_total">
 						  <cfif counter LTE 25>
							  <cfif amount GTE 0>
							   ['#product_or_service_code_description#',#round(amount)#,'/exchange/awards/award_search_detail_psc_dept.cfm?val=#product_or_service_code#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="psc_graph" style="width: 100%;"></div>


				    </td></tr>

                    <tr><td colspan=3><hr></td></tr>

           <tr>
           		<td class="feed_sub_header" align=center>By Department</td>
           		<td></td>
           		<td class="feed_sub_header" align=center>By Vendor (Top 25)</td>
           </tr>

           <tr><td>&nbsp;</td></tr>

				    <tr><td valign=top align=center>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('dept_graph'));

 						data.addColumn('string','Department');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="sub_agency">
 						  <cfif amount GTE 0>
 						   ['#awarding_agency_name#',#round(amount)#,'/exchange/awards/award_search_detail_agency_dept.cfm?val=#awarding_agency_code#'],
 						  </cfif>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>


				    <div id="dept_graph" style="width: 100%;"></div>

				    </td><td></td><td valign=top align=center>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('duns'));

 						data.addColumn('string','PSC');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="duns">
 						  <cfif counter LTE 25>
							  <cfif amount GTE 0>
							   ['#recipient_name#',#round(amount)#,'/exchange/awards/award_search_detail_vendor_dept.cfm?val=#recipient_duns#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="duns" style="width: 100%;"></div>

				    </td></tr>
             </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=2 class="feed_sub_header">AWARD DATA</td></tr>
          <tr><td>&nbsp;</td></tr>

		  <tr><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr height=40><td class="feed_option"><b>NAICS</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>

		       </tr>

		  <cfoutput query="naics_total">
		   <tr height=30><td class="feed_option" width=75 valign=top><a href="award_search_detail_naics_dept.cfm?val=#naics_code#">#naics_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_naics_dept.cfm?val=#naics_code#"><b>#naics_description#</b></a></td>
		       <td class="feed_option" valign=top align=center>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>

               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_naics_dept.cfm?val=#naics_code#"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_naics_dept.cfm?val=#naics_code#"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>

		       </tr>

		  </cfoutput>

		  <tr><td colspan=6><hr></td></tr>

		   <tr height=40><td class="feed_option" colspan=2><b>Department</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>
		   </tr>

		  <cfoutput query="sub_agency">
		   <tr height=30><td class="feed_option" colspan=2><a href="award_search_detail_agency_dept.cfm?val=#awarding_agency_code#"><b>#awarding_agency_name#</b></a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>


               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_agency_dept.cfm?val=#awarding_agency_code#"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_agency_dept.cfm?val=#awarding_agency_code#"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>
           </tr>
		  </cfoutput>

		  <tr><td colspan=6><hr></td></tr>

		   <tr height=40><td class="feed_option" colspan=2><b>Vendor</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>
           </tr>

		  <cfoutput query="duns">
		   <tr height=30><td class="feed_option" colspan=2><a href="award_search_detail_vendor_dept.cfm?val=#recipient_duns#"><b>#recipient_name#</b></a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>


               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_vendor_dept.cfm?val=#recipient_duns#"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_vendor_dept.cfm?val=#recipient_duns#"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>


           </tr>
		  </cfoutput>

          </table>

		      </td><td width=50>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr height=40><td class="feed_option"><b>PSC</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Awarded</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>
           </tr>

		  <cfoutput query="psc_total">
		   <tr height=30><td class="feed_option" valign=top width=50 valign=top><a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#">#product_or_service_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#"><b>#product_or_service_code_description#</b></a></td>
		       <td class="feed_option" align=center valign=top>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>
               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#"><a href="award_search_detail_psc_dept.cfm?val=#product_or_service_code#"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>

           </tr>
		  </cfoutput>

		  </table>

		     </td><td width=30>&nbsp;</td></tr>

		  </table>

  	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>