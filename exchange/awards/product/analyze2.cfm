<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  		   <cfquery name="psc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from psc
			where psc_code = '#psc_code#'
		   </cfquery>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;<a href="/exchange/awards/product/">By Product or Service Code</a></td>

           <td align=right class="feed_option">

           <form action="/exchange/awards/set.cfm" method="post">

  		   <cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from set_aside
			order by set_aside_name
		   </cfquery>

       		       <select name="selected_set_aside_code" style="width:200px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>All AWARDS
				       <option value=1 <cfif #session.set_aside_code# is 1>selected</cfif>>All SET ASIDES
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>&nbsp;-&nbsp;#set_aside_name#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

           <cfoutput>

	                   From:&nbsp;<input type="date" name="from" size=8 maxlength=10 required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               To:&nbsp;<input type="date" name="to" size=8 maxlength=10 required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="psc_1">
                       <input type="hidden" name="psc_code" value=#psc_code#>
                       </form>

          </td></tr>

  		   <cfquery name="analyze" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(product_or_service_code) as count from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
			        product_or_service_code = '#psc_code#' and
			        recipient_country_code = 'USA'

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>

		   </cfquery>

          <tr><td class="feed_option"><b>#psc.psc_code# - #psc.psc_description#<br>Total Awards: #analyze.count#</b></td></tr>
          <tr><td>&nbsp;</td></tr>
          </cfoutput>

          </table>

          <!--- Awards by State --->

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>

           <td valign=top width=30%>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="analyze_state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select recipient_state_code, recipient_state_name, count(product_or_service_code) as state_count, sum(federal_action_obligation) as amount from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
			        product_or_service_code = '#psc_code#' and
			        recipient_country_code = 'USA' and
 		            recipient_state_code <> ''

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>

 		    group by recipient_state_code, recipient_state_name
 		    order by amount DESC
		   </cfquery>

		   <cfoutput>

			   <tr><td class="feed_header" colspan=2>Awards by State</td>
				   <td align=right colspan=2 class="feed_option"><a href="analyze.cfm?psc_code=#psc_code#&export=1&view=1">Export to Excel</a></td></tr>
			   </tr>

           </cfoutput>

           <tr><td height=10></td></tr>

           <cfif #analyze_state.recordcount# is 0>
           <tr><td class="feed_option">No awards were found.</td></tr>
           <cfelse>

           <tr><td class="feed_option"><b>State</b></td>
               <td class="feed_option" align=center><b>Awards</b></td>
               <td class="feed_option" align=right width=100><b>Value</b></td></tr>

           <cfset scounter = 0>
           <cfoutput query="analyze_state">
            <tr

            <cfif scounter is 0>
             bgcolor="ffffff"
            <cfelse>
             bgcolor="e0e0e0"
            </cfif>

            ><td class="feed_option"><a href="analyze_state.cfm"><a href="contracts_state.cfm?psc_code=#psc_code#&state=#recipient_state_code#">#recipient_state_name#</a></td>
                <td class="feed_option" align=center>#numberformat(state_count,'999,999')#</td>
                <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>

           <cfif scounter is 0>
            <cfset scounter = 1>
           <cfelse>
            <cfset scounter = 0>
           </cfif>

           </cfoutput>

          </cfif>

		  </table>

		  </td><td width=2%>&nbsp;</td>

		             <td valign=top width=30%>

		             <table cellspacing=0 cellpadding=0 border=0 width=100%>

		    		   <cfquery name="analyze_agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  			select awarding_sub_agency_name, count(product_or_service_code) as state_count, sum(federal_action_obligation) as amount, awarding_sub_agency_code from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
			        product_or_service_code = '#psc_code#' and
			        recipient_country_code = 'USA'

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>


		   		    group by awarding_sub_agency_code, awarding_sub_agency_name
		   		    order by amount DESC
		  		   </cfquery>

		  		   <cfoutput>

		             <tr><td class="feed_header">Awards by Agency</td>
                         <td align=right colspan=2 class="feed_option"><a href="analyze.cfm?psc_code=#psc_code#&export=1&view=2">Export to Excel</a></td></tr>

		             </tr>

		           </cfoutput>

		             <tr><td height=10></td></tr>

		             <cfif #analyze_agency.recordcount# is 0>
		             <tr><td class="feed_option">No awards were found.</td></tr>
		             <cfelse>

		             <tr><td class="feed_option"><b>Agency Name</b></td>
		                 <td class="feed_option" align=center><b>Awards</b></td>
		                 <td class="feed_option" align=right width=100><b>Value</b></td></tr>

		             <cfset scounter = 0>
		             <cfoutput query="analyze_agency">
		              <tr

		              <cfif scounter is 0>
		               bgcolor="ffffff"
		              <cfelse>
		               bgcolor="e0e0e0"
		              </cfif>

		              ><td class="feed_option"><a href="contracts_agency.cfm?psc_code=#psc_code#&awarding_sub_agency_code=#awarding_sub_agency_code#">#awarding_sub_agency_name#</a></td>
		                  <td class="feed_option" align=center>#numberformat(state_count,'999,999')#</td>
		                  <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>

		             <cfif scounter is 0>
		              <cfset scounter = 1>
		             <cfelse>
		              <cfset scounter = 0>
		             </cfif>

		             </cfoutput>

		            </cfif>

		  		  </table>

		  </td><td width=2%>&nbsp;</td>


		  <td valign=top width=30%>

          <!--- Awards by Vendor --->

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="analyze_vendor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select recipient_duns, recipient_name, recipient_state_code, count(product_or_service_code) as vendor_count, sum(federal_action_obligation) as vendor_total from award_data
			where product_or_service_code = '#psc_code#'
 		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>


 		    group by recipient_duns, recipient_name, recipient_state_code
 		    order by vendor_total DESC
		   </cfquery>

           <cfoutput>
           <tr><td class="feed_header">Awards by Vendor</td>
               <td align=right colspan=3 class="feed_option"><a href="analyze.cfm?psc_code=#psc_code#&export=1&view=3">Export to Excel</a></td></tr>

           </tr>
           </cfoutput>
           <tr><td height=10></td></tr>

           <cfif #analyze_vendor.recordcount# is 0>
           <tr><td class="feed_option">No awards were found.</td></tr>
           <cfelse>

           <tr><td class="feed_option"><b>Vendor Name</b></td>
               <td class="feed_option" align=center width=50><b>State</b></td>
               <td class="feed_option" align=center width=75><b>Awards</b></td>
               <td class="feed_option" align=right width=100><b>Value</b></td></tr>

           <cfset vcounter = 0>
           <cfoutput query="analyze_vendor">
            <tr
            <cfif vcounter is 0>
             bgcolor="ffffff"
            <cfelse>
             bgcolor="e0e0e0"
            </cfif>

            ><td class="feed_option"><a href="contracts_vendor.cfm?psc_code=#psc_code#&duns=#recipient_duns#">#recipient_name#</a></td>
                <td class="feed_option" align=center>#recipient_state_code#</td>
                <td class="feed_option" align=center>#numberformat(vendor_count,'999,999')#</td>
                <td class="feed_option" align=right>#numberformat(vendor_total,'$999,999,999')#</td></tr>

           <cfif vcounter is 0>
            <cfset vcounter = 1>
           <cfelse>
            <cfset vcounter = 0>
           </cfif>

           </cfoutput>

		 </cfif>

		 </table>

		  </td></tr>
		  </table>

	  </div>

<cfif isdefined("export")>
here

<cfif view is 1>

             <cfoutput>
                <cfset theFile = "exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
                <cfset fullFile = "#temp_path#\exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
             </cfoutput>

  			  <cfspreadsheet action="write" sheetname="EXCHANGE Report" query="analyze_state" filename="#fullfile#" overwrite="yes">

    		  <cfscript>

  				   a = SpreadSheetRead("#fullfile#","EXCHANGE Report");
                   SpreadsheetAddRow(a,"EXCHANGE Report",1,1);
                   SpreadsheetAddRow(a,"#psc.psc_code# - #psc.psc_description#",2,1);
                   SpreadsheetAddRow(a," ",3,2);
                   SpreadsheetAddRow(a,"Date / Time - #dateformat(now(),'mm/dd/yyyy')# at #timeformat(now())#",2,1);
                   SpreadsheetAddRow(a," ",3,2);

  			  </cfscript>

  			  <cfset info = StructNew()>
  			  <cfset info.title="EXCHANGE Report">
  			  <cfset info.category="Award Data">
  			  <cfset info.subject="Awards by State">
  			  <cfset info.creationdate="#dateformat(now(),'mm/dd/yyyy')#">
  			  <cfset info.author="EXCHANGE">
  			  <cfset info.comments="This information was exported from https://www.ratio.exchange and shall be used as is.">

              <cfset spreadsheetaddInfo(a,info)>

  			  <cfset format = StructNew()>
  			  <cfset format.font="Arial">
  		      <cfset format.size="30">
  			  <cfset format.color="black">
  			  <cfset format.bold="true">
  			  <cfset format.alignment="left">
  			  <cfset spreadsheetFormatCell(a,format,1,1)>

              <cfspreadsheet action="write" filename="#fullFile#" name="a" sheet=1 sheetname="By State" overwrite=true>

              <cfheader name="Content-Disposition" value="attachment; filename=#thefile#">
  			  <cfcontent type="application/vnd.ms-excel" file="#fullfile#" reset="yes">

<cfelseif view is 2>

             <cfoutput>
                <cfset theFile = "exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
                <cfset fullFile = "#temp_path#\exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
             </cfoutput>

  			  <cfspreadsheet action="write" sheetname="EXCHANGE Report" query="analyze_agency" filename="#fullfile#" overwrite="yes">

    		  <cfscript>

  				   a = SpreadSheetRead("#fullfile#","EXCHANGE Report");
                   SpreadsheetAddRow(a,"EXCHANGE Report",1,1);
                   SpreadsheetAddRow(a,"#psc.psc_code# - #psc.psc_description#",2,1);
                   SpreadsheetAddRow(a," ",3,2);
                   SpreadsheetAddRow(a,"Date / Time - #dateformat(now(),'mm/dd/yyyy')# at #timeformat(now())#",2,1);
                   SpreadsheetAddRow(a," ",3,2);

  			  </cfscript>

  			  <cfset info = StructNew()>
  			  <cfset info.title="EXCHANGE Report">
  			  <cfset info.category="Award Data">
  			  <cfset info.subject="Awards by Agency">
  			  <cfset info.creationdate="#dateformat(now(),'mm/dd/yyyy')#">
  			  <cfset info.author="EXCHANGE">
  			  <cfset info.comments="This information was exported from https://www.ratio.exchange and shall be used as is.">

              <cfset spreadsheetaddInfo(a,info)>

  			  <cfset format = StructNew()>
  			  <cfset format.font="Arial">
  		      <cfset format.size="30">
  			  <cfset format.color="black">
  			  <cfset format.bold="true">
  			  <cfset format.alignment="left">
  			  <cfset spreadsheetFormatCell(a,format,1,1)>

              <cfspreadsheet action="write" filename="#fullFile#" name="a" sheet=1 sheetname="By Agency" overwrite=true>

              <cfheader name="Content-Disposition" value="attachment; filename=#thefile#">
  			  <cfcontent type="application/vnd.ms-excel" file="#fullfile#" reset="yes">


<cfelseif view is 3>

             <cfoutput>
                <cfset theFile = "exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
                <cfset fullFile = "#temp_path#\exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
             </cfoutput>

  			  <cfspreadsheet action="write" sheetname="EXCHANGE Report" query="analyze_vendor" filename="#fullfile#" overwrite="yes">

    		  <cfscript>

  				   a = SpreadSheetRead("#fullfile#","EXCHANGE Report");
                   SpreadsheetAddRow(a,"EXCHANGE Report",1,1);
                   SpreadsheetAddRow(a,"#psc.psc_code# - #psc.psc_description#",2,1);
                   SpreadsheetAddRow(a," ",3,2);
                   SpreadsheetAddRow(a,"Date / Time - #dateformat(now(),'mm/dd/yyyy')# at #timeformat(now())#",2,1);
                   SpreadsheetAddRow(a," ",3,2);

  			  </cfscript>

  			  <cfset info = StructNew()>
  			  <cfset info.title="EXCHANGE Report">
  			  <cfset info.category="Award Data">
  			  <cfset info.subject="Awards by Agency">
  			  <cfset info.creationdate="#dateformat(now(),'mm/dd/yyyy')#">
  			  <cfset info.author="EXCHANGE">
  			  <cfset info.comments="This information was exported from https://www.ratio.exchange and shall be used as is.">

              <cfset spreadsheetaddInfo(a,info)>

  			  <cfset format = StructNew()>
  			  <cfset format.font="Arial">
  		      <cfset format.size="30">
  			  <cfset format.color="black">
  			  <cfset format.bold="true">
  			  <cfset format.alignment="left">
  			  <cfset spreadsheetFormatCell(a,format,1,1)>

              <cfspreadsheet action="write" filename="#fullFile#" name="a" sheet=1 sheetname="By Vendor" overwrite=true>

              <cfheader name="Content-Disposition" value="attachment; filename=#thefile#">
  			  <cfcontent type="application/vnd.ms-excel" file="#fullfile#" reset="yes">
</cfif>


</cfif>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>