<cfinclude template="/exchange/security/check.cfm">

<cfset session.award_from = #from#>
<cfset session.award_to = #to#>

<cfif location is "agency_1">
 <cflocation URL="/exchange/awards/agency/" addtoken="no">
<cfelseif location is "agency_2">
 <cflocation URL="/exchange/awards/agency/sub_agency.cfm?department_code=#department_code#" addtoken="no">
<cfelseif location is "agency_3">
 <cfset session.set_aside_code = #selected_set_aside_code#>
<cflocation URL="/exchange/awards/agency/vendors.cfm?department_code=#department_code#&agency_code=#agency_code#" addtoken="no">
<cfelseif location is "agency_4">
 <cfif filter is "">
  <cfset StructDelete(Session,"filter_keyword")>
 <cfelse>
 	<cfset session.filter_keyword = #filter#>
 </cfif>
 <cflocation URL="/exchange/awards/agency/contracts.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#" addtoken="no">
<cfelseif location is "agency_8">
 <cfif filter is "">
  <cfset StructDelete(Session,"filter_keyword")>
 <cfelse>
 	<cfset session.filter_keyword = #filter#>
 </cfif>
 <cflocation URL="/exchange/awards/agency/awards.cfm?department_code=#department_code#&agency_code=#agency_code#&duns=#duns#" addtoken="no">

<cfelseif location is "agency_5">
 <cflocation URL="/exchange/awards/agency/marketshare.cfm?department_code=#department_code#&agency_code=#agency_code#" addtoken="no">
<cfelseif location is "agency_7">
 <cfset session.set_aside_code = #selected_set_aside_code#>
<cflocation URL="/exchange/awards/agency/dashboard_vendors.cfm?department_code=#department_code#&agency_code=#agency_code#" addtoken="no">
<cfelseif location is "psc_1">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/analyze.cfm?psc_code=#psc_code#" addtoken="no">

<cfelseif location is "psc_1a">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/analyze.cfm?psc_code=#psc_code#" addtoken="no">

<cfelseif location is "psc_1b">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/analyze_department.cfm?psc_code=#psc_code#" addtoken="no">

<cfelseif location is "psc_1c">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/analyze_vendor.cfm?psc_code=#psc_code#" addtoken="no">


<cfelseif location is "psc_2">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/contracts_state.cfm?psc_code=#psc_code#&state=#state#" addtoken="no">
<cfelseif location is "psc_3">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/contracts_agency.cfm?psc_code=#psc_code#&awarding_sub_agency_code=#awarding_sub_agency_code#" addtoken="no">
<cfelseif location is "psc_4">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/product/contracts_vendor.cfm?psc_code=#psc_code#&duns=#duns#" addtoken="no">

<cfelseif location is "setaside_1a">
 <cflocation URL="/exchange/awards/sb/analyze.cfm?code=#code#" addtoken="no">

<cfelseif location is "setaside_1b">
 <cflocation URL="/exchange/awards/sb/analyze_department.cfm?code=#code#" addtoken="no">

<cfelseif location is "setaside_1c">
 <cflocation URL="/exchange/awards/sb/analyze_vendor.cfm?code=#code#" addtoken="no">

<cfelseif location is "setaside_1d">
 <cflocation URL="/exchange/awards/sb/analyze_city.cfm?code=#code#&state=#state#" addtoken="no">

<cfelseif location is "setaside_1e">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cflocation URL="/exchange/awards/sb/contracts_city.cfm?code=#code#&state=#state#&city=#city#" addtoken="no">

<cfelseif location is "setaside_1f">
 <cflocation URL="/exchange/awards/sb/analyze_vend.cfm?code=#code#&awarding_sub_agency_code=#awarding_sub_agency_code#" addtoken="no">

<cfelseif location is "setaside_1g">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cflocation URL="/exchange/awards/sb/contracts_vend.cfm?code=#code#&awarding_sub_agency_code=#awarding_sub_agency_code#&duns=#duns#" addtoken="no">


<cfelseif location is "setaside_1h">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cflocation URL="/exchange/awards/sb/contracts_vendor.cfm?code=#code#&duns=#duns#" addtoken="no">

<cfelseif location is "naics_1a">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/naics/analyze.cfm?naics_code=#naics_code#" addtoken="no">

<cfelseif location is "naics_1b">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/naics/analyze_department.cfm?naics_code=#naics_code#" addtoken="no">

<cfelseif location is "naics_1c">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/naics/analyze_vendor.cfm?naics_code=#naics_code#" addtoken="no">

<cfelseif location is "naics_2">
 <cfif filter is "">
  <cfset StructDelete(Session,"filter_keyword")>
 <cfelse>
 	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/naics/contracts_state.cfm?naics_code=#naics_code#&state=#state#" addtoken="no">
<cfelseif location is "naics_3">

 <cfif filter is "">
  <cfset StructDelete(Session,"filter_keyword")>
 <cfelse>
 	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/naics/contracts_agency.cfm?naics_code=#naics_code#&awarding_sub_agency_code=#awarding_sub_agency_code#" addtoken="no">
<cfelseif location is "naics_4">

 <cfif filter is "">
  <cfset StructDelete(Session,"filter_keyword")>
 <cfelse>
 	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/naics/contracts_vendor.cfm?naics_code=#naics_code#&duns=#duns#" addtoken="no">
<cfelseif location is "state_1">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cfset session.location_type = #location_type#>
 <cfif session.dashboard is 0>
  <cfset session.search_kw = #keyword#>
  <cflocation URL="/exchange/awards/state/city.cfm?state_abbr=#state_abbr#" addtoken="no">
 <cfelse>
  <cflocation URL="/exchange/awards/state/city_dashboard.cfm?state_abbr=#state_abbr#" addtoken="no">
 </cfif>
<cfelseif location is "state_2">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cfset session.location_type = #location_type#>
 <cfset session.search_kw = #keyword#>
 <cflocation URL="/exchange/awards/state/vendors.cfm?state_abbr=#state_abbr#&city=#city#" addtoken="no">
<cfelseif location is "state_3">

 <cfif filter is "">
  <cfset StructDelete(Session,"search_kw")>
 <cfelse>
 	<cfset session.search_kw = #filter#>
 </cfif>

 <cflocation URL="/exchange/awards/state/contracts.cfm?state_abbr=#state_abbr#&city=#city#&duns=#duns#&recipient_name=#recipient_name#" addtoken="no">
<cfelseif location is "state_4">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/stateaics_codes.cfm?state_abbr=#state_abbr#&city=#city#" addtoken="no">
<cfelseif location is "state_5">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/state/dashboard.cfm?state_abbr=#state_abbr#" addtoken="no">
<cfelseif location is "sb_1">

  <cfif filter is "">
   <cfset StructDelete(Session,"filter_keyword")>
  <cfelse>
  	<cfset session.filter_keyword = #filter#>
 </cfif>

 <cflocation URL="/exchange/awards/sb/contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#" addtoken="no">
<cfelseif location is "20">
 <cfset session.set_aside_code = #selected_set_aside_code#>
 <cflocation URL="/exchange/awards/agency/contract.cfm?department_code=#department_code#&agency_code=#agency_code#" addtoken="no">
</cfif>