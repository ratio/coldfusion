<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Search">
 <cflocation URL="/exchange/awards/naics/search_results.cfm?naics_search=#trim(naics_search)#" addtoken="no">
<cfelse>
 <cflocation URL="/exchange/awards/naics/analyze.cfm?naics_code=#trim(naics_search)#" addtoken="no">
</cfif>