<cfinclude template="/exchange/security/check.cfm">

<cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from naics
 where naics_code like '%#naics_search#%' or naics_code_description like '%#naics_search#%'

	   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by naics_code DESC
		    <cfelseif #sv# is 10>
		     order by naics_code ASC
		    <cfelseif #sv# is 2>
		     order by naics_code_description ASC
		    <cfelseif #sv# is 20>
		     order by naics_code_description DESC

		    </cfif>

		   <cfelse>
		     order by naics_code ASC
		   </cfif>

</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Federal Awards - By NAICS Code</td></td>
           <td class="feed_option" align=right class="feed_sub_header"><a href="/exchange/awards/naics/index.cfm">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif #naics.recordcount# is 0>
           <tr><td class="feed_sub_header">No NAICS codes were found.  <a href="/exchange/awards/naics/index.cfm">Please try a different search string</a>.</td></tr>
           <cfelse>

		   <cfoutput>

           <tr>
             <td class="feed_sub_header" width=100><a href="/exchange/awards/naics/search_results.cfm?naics_search=#naics_search#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>NAICS Code</b></a></td>
             <td class="feed_sub_header"><a href="/exchange/awards/naics/search_results.cfm?naics_search=#naics_search#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Description</b></a></td>
           </tr>

           </cfoutput>

		   <cfset counter = 0>

		   <cfoutput query="naics">
		    <tr

		    <cfif counter is 0>
		     bgcolor="ffffff"
		    <cfelse>
		     bgcolor="e0e0e0"
		    </cfif>

		    >

		    <td class="feed_sub_header"><a href="/exchange/awards/naics/analyze.cfm?naics_code=#naics_code#"><b>#naics_code#</b></a></td>
		    <td class="feed_sub_header" style="font-weight: normal;">#naics_code_description#</td></tr>

		    <cfif counter is 0>
		     <cfset counter = 1>
		    <cfelse>
		     <cfset counter = 0>
		    </cfif>

		   </cfoutput>

		   </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

        </td></tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>