<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.award_to") or not isdefined("session.award_from")>
 <cfset #session.award_to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.award_from# = #dateformat(dateadd("d",-730,now()),'yyyy-mm-dd')#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Federal Awards - By NAICS Code</b></td>
               <td align=right></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">The <a href="https://www.census.gov/eos/www/naics/" target="_blank" rel="noopener" rel="noreferrer"><b>North American Industry Classification System (NAICS)</b></a> is the standard used by Federal statistical agencies in classifying business establishments for the purpose of collecting, analyzing, and publishing statistical data related to the U.S. business economy.  The
           Federal government uses NAICS codes to describe / characterize the type of service being requested or delivered.</td></tr>
           <tr><td><hr></td></tr>
          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

                    <form action="/exchange/awards/naics/search.cfm" method="post">

					   <tr><td class="feed_sub_header"><b>SEARCH FOR NAICS CODE</b></td></tr>
					   <tr><td class="feed_option"><input type="text" class="input_text" name="naics_search" size=30 placeholder="Keyword or Number">&nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup" style="cursor: pointer;" onclick="window.open('/exchange/opps/naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;"></td></tr>
                       <tr><td height=10></td></tr>
					   <tr><td><input class="button_blue_large" type="submit" name="button" value="Search"></td></tr>
                       <tr><td height=20></td></tr>
                       <tr><td><hr></td></tr>
                    </form>

                    <form action="/exchange/awards/naics/search.cfm" method="post">

					   <tr><td height=10></td></tr>
					   <tr><td class="feed_sub_header"><b>OR, ENTER NAICS CODE DIRECTLY</b></td></tr>
					   <tr><td class="feed_option"><input class="input_text" type="text" placeholder="Code Number" name="naics_search" size=10 required>                   &nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup" style="cursor: pointer;" onclick="window.open('/exchange/opps/naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;"></td></tr>
                       <tr><td height=10></td></tr>
					   <tr><td><input class="button_blue_large" type="submit" name="button" value="Analyze"></td></tr>

                    </form>

          <tr><td>&nbsp;</td></tr>

		  </table>

       </td></tr>

    </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>