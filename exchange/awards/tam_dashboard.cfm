<cfinclude template="/exchange/security/check.cfm">

<cfquery name="view" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id# and
	   award_view_id = #award_view_id#
</cfquery>

<cfset #session.award_dept# = '#view.award_view_agency#'>
<cfset #session.award_from# = '#view.award_view_date_from#'>
<cfset #session.award_to# = '#view.award_view_date_to#'>
<cfset #session.award_state# = '#view.award_view_state#'>
<cfset #session.award_naics# = '#view.award_view_naics#'>
<cfset #session.award_psc# = '#view.award_view_psc#'>
<cfset #session.award_keyword# = '#view.award_view_keyword#'>
<cfset #session.award_setaside# = '#view.award_view_set_aside#'>
<cfset #session.duns# = '#view.award_view_duns#'>
<cfset #session.location_type# = #view.award_view_location_type#>
<cfset #session.date_type# = #view.award_view_date_type#>
<cfset #session.amount_from# = #view.award_view_amount_from#>
<cfset #session.amount_to# = #view.award_view_amount_to#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Build Query --->

   <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (awarding_agency_code = '#session.award_dept#' and
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
            federal_action_obligation > 0

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

   </cfquery>

  <!--- NAICS --->

  <cfquery name="naics_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select naics_code, naics_description, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (awarding_agency_code = '#session.award_dept#' and
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
	         and federal_action_obligation > 0
     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by naics_code, naics_description
     order by amount DESC

   </cfquery>

  <!--- PSC --->

  <cfquery name="psc_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) product_or_service_code, product_or_service_code_description, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (awarding_agency_code = '#session.award_dept#' and
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
            and federal_action_obligation > 0
     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by product_or_service_code, product_or_service_code_description
     order by amount DESC

   </cfquery>

  <!--- Sub Agency --->

  <cfquery name="sub_agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select awarding_sub_agency_code, awarding_sub_agency_name, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (awarding_agency_code = '#session.award_dept#' and
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
            and federal_action_obligation > 0

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by awarding_sub_agency_code, awarding_sub_agency_name
     order by amount DESC

   </cfquery>

  <!--- Recipient --->

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as amount from award_data

	  where (
            ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
             and awarding_agency_code = '#session.award_dept#'
             and federal_action_obligation > 0

     		<cfif #session.award_state# is not 0>
     			and primary_place_of_performance_state_code = '#session.award_state#'
     		</cfif>

		 <cfif #session.award_state# is not 0>
			and primary_place_of_performance_state_code = '#session.award_state#'
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif>
            )
      <cfif #session.award_keyword# is not "">
	  and contains((award_description),'#session.award_keyword#')
	  </cfif>

     group by recipient_duns, recipient_name
     order by amount DESC

   </cfquery>


  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><b>Total Addressable Market (TAM) Dashboard</b></td>
               <td class="feed_option" align=right><a href="index.cfm">Return</td></tr>
           <tr><td height=20></td></tr>
          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_sub_header">Total Awards: #numberformat(awards.total,'999,999')#</td></tr>
            <tr><td class="feed_sub_header">Award Total: #numberformat(awards.amount,'$999,999,999')#</td></tr>
            <tr><td>&nbsp;</td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td>&nbsp;</td></tr>

           <tr>
              <td class="feed_sub_header" align=center>TAM by NAICS Code</td>
              <td class="feed_sub_header" align=center>TAM by Product or Service Code (Top 25)</td>
           </tr>

           <tr><td valign=top width>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['NAICS', 'Award Value', 'Awards'],
						  <cfoutput query="naics_total">
						   ['#naics_description#',#round(amount)#,#total#],
						  </cfoutput>
						]);

						var options = {
						legend: 'bottom',
			            chartArea:{right: 30, left:50,top:20,width:'83%',height:'75%'},
						title: '',
						pieHole: 0.4,
						width: 600,
						height: 500,
						fontSize: 10,
						};

					var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="naics_graph"></div>

                 </td><td valign=top>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['PSC', 'Award Value', 'Awards'],
						  <cfoutput query="psc_total">
						   ['#product_or_service_code_description#',#round(amount)#,#total#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: '',
			            chartArea:{right: 30, left:150,top:20,width:'83%',height:'75%'},
						width: 600,
						height: 550,
						fontSize: 9,
						};

					var chart = new google.visualization.BarChart(document.getElementById('psc_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="psc_graph"></div>

				    </td></tr>

           <tr><td class="feed_sub_header" align=center colspan=2>Product or Service Market Share (Top 25)</td></tr>
           <tr><td>&nbsp;</td></tr>


				    </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				    <tr><td>

			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript">
			  google.charts.load('current', {'packages':['treemap']});
			  google.charts.setOnLoadCallback(drawChart);
			  function drawChart() {
				var data = google.visualization.arrayToDataTable([

				  ['Location', 'Parent', 'Value', 'Number of Awards'],
				  ['Award Value', null, 0, 0],

				   <cfoutput query="psc_total">
					['#product_or_service_code_description#','Award Value', #amount#, #numberformat(evaluate(100*(psc_total.amount/awards.amount)),'99.99')#],
				   </cfoutput>
				]);

				tree = new google.visualization.TreeMap(document.getElementById('heatmap'));
				tree.draw(data, {
				highlightOnMouseOver: true,
				minColor: '#D7F4D2',
				maxColor: '#68A429',
				headerHeight: 15,
				height: 400,
				showScale: false,
				generateTooltip: showFullTooltip
				});


		  function showFullTooltip(row, size, value) {
			return '<div style="font-size: 12px; background:#fd9; padding:10px; border-style:solid">' +
				   '<span style="font-size: 12px; font-family:arial"><b>' + data.getValue(row, 0) +
				   '</b><br>'+ data.getValue(row, 1)+
				   ' - $' + data.getValue(row, 2) +
				   '<br>Market Share - ' + data.getValue(row,3) + '%' +
				   '</span><br>' + ' </div>';
		  }

			  }
			</script>

		   <div id="heatmap" style="width: 1300px; height: 500px;"></div>

				    </td></tr>

				    </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
           		<td class="feed_sub_header" align=center>TAM by Agency</td>
           		<td class="feed_sub_header" align=center>TAM by Vendor (Top 25)</td>
           </tr>

           <tr><td>&nbsp;</td></tr>

				    <tr><td valign=top>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Agency', 'Award Value', 'Awards'],
						  <cfoutput query="sub_agency">
						   ['#awarding_sub_agency_name#',#round(amount)#,#total#],
						  </cfoutput>
						]);

						var options = {
						legend: 'bottom',
						title: '',
			            chartArea:{right: 30, left:50,top:20,width:'83%',height:'75%'},
						pieHole: 0.4,
						width: 500,
						height: 400,
						fontSize: 9,
						};

					var chart = new google.visualization.PieChart(document.getElementById('sub_agency_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="sub_agency_graph"></div>

				    </td><td valign=top align=right>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Recipient', 'Award Value'],
						  <cfoutput query="duns">
						   ['#recipient_name#',#round(duns.amount)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'none',
						title: '',
			            chartArea:{right: 0, left:100,top:20,width:'83%',height:'75%'},
						width: 600,
						height: 400,
						fontSize: 9,
						};

					var chart = new google.visualization.ColumnChart(document.getElementById('duns'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="duns"></div>
				    </td></tr>
             </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=2 class="feed_sub_header"><b>TAM Award Data</b></td></tr>
          <tr><td>&nbsp;</td></tr>

		  <tr><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>NAICS</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Amount</b></td></tr>

		  <cfoutput query="naics_total">
		   <tr><td class="feed_option"><a href="award_search_detail_naics.cfm?val=#naics_code#" target="_blank" rel="noopener" rel="noreferrer">#naics_code#</a></td>
		       <td class="feed_option"><a href="award_search_detail_naics.cfm?val=#naics_code#" target="_blank" rel="noopener" rel="noreferrer">#naics_description#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

		  <tr><td>&nbsp;</td></tr>

		   <tr><td class="feed_option" colspan=2><b>Agency</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Amount</b></td></tr>

		  <cfoutput query="sub_agency">
		   <tr><td class="feed_option" colspan=2><a href="award_search_detail_agency.cfm?val=#awarding_sub_agency_code#" target="_blank" rel="noopener" rel="noreferrer">#awarding_sub_agency_name#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

		  <tr><td>&nbsp;</td></tr>

		   <tr><td class="feed_option" colspan=2><b>Vendor</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Amount</b></td></tr>

		  <cfoutput query="duns">
		   <tr><td class="feed_option" colspan=2><a href="award_search_detail_vendor.cfm?val=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

          </table>

		      </td><td width=50>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>PSC</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards</b></td>
		       <td class="feed_option" align=right><b>Amount</b></td></tr>

		  <cfoutput query="psc_total">
		   <tr><td class="feed_option" valign=top width=50><a href="award_search_detail_psc.cfm?val=#product_or_service_code#" target="_blank" rel="noopener" rel="noreferrer">#product_or_service_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_psc.cfm?val=#product_or_service_code#" target="_blank" rel="noopener" rel="noreferrer">#product_or_service_code_description#</a></td>
		       <td class="feed_option" align=center>#total#</td>
		       <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td></tr>
		  </cfoutput>

		  </table>

		     </td><td width=30>&nbsp;</td></tr>

		  </table>

  	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>