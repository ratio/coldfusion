<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>

           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;

           Search Number - #number#
           </td>
           <td align=right class="feed_option">
            <a href="/exchange/awards/index.cfm">Return</a>
           </cfoutput>

          </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>


  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from award_data
			where (parent_award_id = '#number#' or award_id_piid = '#number#' or solicitation_identifier = '#number#')

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by action_date ASC
		    <cfelseif #sv# is 10>
             order by action_date DESC
		    <cfelseif #sv# is 2>
             order by recipient_name ASC
		    <cfelseif #sv# is 20>
             order by recipient_name DESC
		    <cfelseif #sv# is 3>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 4>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 5>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 50>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 6>
             order by product_or_service_code ASC
		    <cfelseif #sv# is 60>
             order by product_or_service_code DESC
		    <cfelseif #sv# is 7>
             order by type_of_set_aside ASC
		    <cfelseif #sv# is 70>
             order by type_of_set_aside DESC
		    <cfelseif #sv# is 8>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 80>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 9>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 90>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 10>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 100>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 11>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 110>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 12>
             order by naics_code ASC
		    <cfelseif #sv# is 120>
             order by naics_code DESC
		    <cfelseif #sv# is 14>
             order by award_id_piid ASC, modification_number
		    <cfelseif #sv# is 140>
             order by award_id_piid DESC, modification_number

		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

         <tr><td class="feed_option" colspan=7><b>Total Awards: #agencies.recordcount#</b></td></tr>
          <tr><td>&nbsp;</td></tr>

          <tr>

             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Contract ##</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Vendor Name</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Agency</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Office</b></a></td>
             <td class="text_xsmall"><b>Award Description</b></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Product or Service</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>NAICS</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Pricing</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Set Aside</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PoP Start</b></a></td>
             <td class="text_xsmall"><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP End</b></a></td>
             <td class="text_xsmall" align=right><a href="index.cfm?number=#number#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Obligation</b></a></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="text_xsmall" valign=top width=75>#dateformat(action_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
               <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>
               <td class="text_xsmall" valign=top width=350>#award_description#</td>
               <td class="text_xsmall" valign=top>#product_or_service_code_description#</td>
               <td class="text_xsmall" valign=top>#naics_code#</td>
               <td class="text_xsmall" valign=top>#left(type_of_contract_pricing,21)#</td>
               <td class="text_xsmall" valign=top>#type_of_set_aside#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_start_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_potential_end_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             </tr>

          <cfset tot = tot + #federal_action_obligation#>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=13><hr></td></tr>
          <tr><td class="text_xsmall" colspan=12><b>Total:</b></td>
              <td class="text_xsmall" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>


          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>