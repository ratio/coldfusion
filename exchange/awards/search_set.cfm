<cfinclude template="/exchange/security/check.cfm">

<cfset #session.award_dept# = '#dept#'>
<cfset #session.award_from# = '#from#'>
<cfset #session.award_to# = '#to#'>
<cfset #session.award_state# = '#state#'>
<cfset #session.award_naics# = '#naics#'>
<cfset #session.award_psc# = '#psc#'>
<cfset #session.award_setaside# = '#setaside#'>

<cfset search_string = #replace(search_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replaceNoCase(search_string,' or ','" or "',"all")#>
<cfset search_string = #replaceNoCase(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>

<cfset #session.award_keyword# = '#search_string#'>

 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into keyword_search
  (
   keyword_search_usr_id,
   keyword_search_company_id,
   keyword_search_hub_id,
   keyword_search_keyword,
   keyword_search_date,
   keyword_search_area
   )
   values
   (
   #session.usr_id#,
   #session.company_id#,
   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  '#session.award_keyword#',
   #now()#,
   2
   )
 </cfquery>

<cfset #value# = #session.award_to# - #session.award_from#>

<cfif #value# GT 1826>
 <cflocation URL="/exchange/awards/index.cfm?s=1" addtoken="no">
</cfif>

<cflocation URL = "award_search.cfm" addtoken="no">