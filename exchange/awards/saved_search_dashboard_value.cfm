<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.location_type")>
 <cfset session.location_type = 1>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="view" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id# and
       award_view_id = #award_view_id#
</cfquery>

<!--- Build Awards Query --->

   <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

    where (action_date between '#session.view_from#' and '#session.view_to#'

		 <cfif #session.view_dept# is not 0>
		   and awarding_agency_code = '#session.view_dept#'
		 </cfif>

         <cfif session.view_location_type is 1>
          <cfif #session.view_state# is not 0>
		   and recipient_state_code = '#session.view_state#'
		  </cfif>
		 <cfelse>
          <cfif #session.view_state# is not 0>
		   and primary_place_of_performance_state_code = '#session.view_state#'
		  </cfif>
		 </cfif>

		 <cfif #session.view_setaside# is not 0>
		   and type_of_set_aside_code = '#session.view_setaside#'
		 </cfif>

		 <cfif #listlen(session.view_duns)# GT 0>
			<cfif #listlen(session.view_duns)# GT 1>
			<cfset dcounter = 1>
			and (
			 <cfloop index="dc" list="#session.view_duns#">
			   (recipient_duns = '#dc#')
			   <cfif dcounter LT #listlen(session.view_duns)#> or</cfif>
			   <cfset dcounter = dcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and recipient_duns = '#session.view_duns#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_naics)# GT 0>
			<cfif #listlen(session.view_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.view_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.view_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.view_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_psc)# GT 0>
			<cfif #listlen(session.view_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.view_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.view_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.view_psc#'
			</cfif>
		 </cfif> )

      <cfif #session.view_keyword# is not "">
	   and contains((award_description, awarding_agency_name, awarding_office_name, funding_office_name, awarding_sub_agency_name, award_id_piid, parent_award_id, solicitation_identifier),'#trim(session.view_keyword)#')
	  </cfif>

   </cfquery>

  <!--- NAICS --->

  <cfquery name="naics_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select naics_code, naics_description, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

    where (action_date between '#session.view_from#' and '#session.view_to#'

		 <cfif #session.view_dept# is not 0>
		   and awarding_agency_code = '#session.view_dept#'
		 </cfif>

         <cfif session.view_location_type is 1>
          <cfif #session.view_state# is not 0>
		   and recipient_state_code = '#session.view_state#'
		  </cfif>
		 <cfelse>
          <cfif #session.view_state# is not 0>
		   and primary_place_of_performance_state_code = '#session.view_state#'
		  </cfif>
		 </cfif>

		 <cfif #session.view_setaside# is not 0>
		   and type_of_set_aside_code = '#session.view_setaside#'
		 </cfif>

		 <cfif #listlen(session.view_duns)# GT 0>
			<cfif #listlen(session.view_duns)# GT 1>
			<cfset dcounter = 1>
			and (
			 <cfloop index="dc" list="#session.view_duns#">
			   (recipient_duns = '#dc#')
			   <cfif dcounter LT #listlen(session.view_duns)#> or</cfif>
			   <cfset dcounter = dcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and recipient_duns = '#session.view_duns#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_naics)# GT 0>
			<cfif #listlen(session.view_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.view_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.view_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.view_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_psc)# GT 0>
			<cfif #listlen(session.view_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.view_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.view_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.view_psc#'
			</cfif>
		 </cfif> )

      <cfif #session.view_keyword# is not "">
	   and contains((award_description, awarding_agency_name, awarding_office_name, funding_office_name, awarding_sub_agency_name, award_id_piid, parent_award_id, solicitation_identifier),'#trim(session.view_keyword)#')
	  </cfif>

     group by naics_code, naics_description
     order by amount DESC

   </cfquery>

  <!--- PSC --->

  <cfquery name="psc_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) product_or_service_code, product_or_service_code_description, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

    where (action_date between '#session.view_from#' and '#session.view_to#'

		 <cfif #session.view_dept# is not 0>
		   and awarding_agency_code = '#session.view_dept#'
		 </cfif>

         <cfif session.view_location_type is 1>
          <cfif #session.view_state# is not 0>
		   and recipient_state_code = '#session.view_state#'
		  </cfif>
		 <cfelse>
          <cfif #session.view_state# is not 0>
		   and primary_place_of_performance_state_code = '#session.view_state#'
		  </cfif>
		 </cfif>

		 <cfif #session.view_setaside# is not 0>
		   and type_of_set_aside_code = '#session.view_setaside#'
		 </cfif>

		 <cfif #listlen(session.view_duns)# GT 0>
			<cfif #listlen(session.view_duns)# GT 1>
			<cfset dcounter = 1>
			and (
			 <cfloop index="dc" list="#session.view_duns#">
			   (recipient_duns = '#dc#')
			   <cfif dcounter LT #listlen(session.view_duns)#> or</cfif>
			   <cfset dcounter = dcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and recipient_duns = '#session.view_duns#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_naics)# GT 0>
			<cfif #listlen(session.view_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.view_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.view_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.view_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_psc)# GT 0>
			<cfif #listlen(session.view_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.view_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.view_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.view_psc#'
			</cfif>
		 </cfif> )

      <cfif #session.view_keyword# is not "">
	   and contains((award_description, awarding_agency_name, awarding_office_name, funding_office_name, awarding_sub_agency_name, award_id_piid, parent_award_id, solicitation_identifier),'#trim(session.view_keyword)#')
	  </cfif>

     group by product_or_service_code, product_or_service_code_description
     order by amount DESC

   </cfquery>

  <!--- Sub Agency --->

  <cfquery name="sub_agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select awarding_sub_agency_code, awarding_sub_agency_name, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

    where (action_date between '#session.view_from#' and '#session.view_to#'

		 <cfif #session.view_dept# is not 0>
		   and awarding_agency_code = '#session.view_dept#'
		 </cfif>

         <cfif session.view_location_type is 1>
          <cfif #session.view_state# is not 0>
		   and recipient_state_code = '#session.view_state#'
		  </cfif>
		 <cfelse>
          <cfif #session.view_state# is not 0>
		   and primary_place_of_performance_state_code = '#session.view_state#'
		  </cfif>
		 </cfif>

		 <cfif #session.view_setaside# is not 0>
		   and type_of_set_aside_code = '#session.view_setaside#'
		 </cfif>

		 <cfif #listlen(session.view_duns)# GT 0>
			<cfif #listlen(session.view_duns)# GT 1>
			<cfset dcounter = 1>
			and (
			 <cfloop index="dc" list="#session.view_duns#">
			   (recipient_duns = '#dc#')
			   <cfif dcounter LT #listlen(session.view_duns)#> or</cfif>
			   <cfset dcounter = dcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and recipient_duns = '#session.view_duns#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_naics)# GT 0>
			<cfif #listlen(session.view_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.view_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.view_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.view_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_psc)# GT 0>
			<cfif #listlen(session.view_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.view_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.view_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.view_psc#'
			</cfif>
		 </cfif> )

      <cfif #session.view_keyword# is not "">
	   and contains((award_description, awarding_agency_name, awarding_office_name, funding_office_name, awarding_sub_agency_name, award_id_piid, parent_award_id, solicitation_identifier),'#trim(session.view_keyword)#')
	  </cfif>

     group by awarding_sub_agency_code, awarding_sub_agency_name
     order by amount DESC

   </cfquery>

  <!--- Recipient --->

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	select top(25) recipient_duns, recipient_name, count(id) as total, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as amount from award_data

    where (action_date between '#session.view_from#' and '#session.view_to#'

		 <cfif #session.view_dept# is not 0>
		   and awarding_agency_code = '#session.view_dept#'
		 </cfif>

         <cfif session.view_location_type is 1>
          <cfif #session.view_state# is not 0>
		   and recipient_state_code = '#session.view_state#'
		  </cfif>
		 <cfelse>
          <cfif #session.view_state# is not 0>
		   and primary_place_of_performance_state_code = '#session.view_state#'
		  </cfif>
		 </cfif>

		 <cfif #session.view_setaside# is not 0>
		   and type_of_set_aside_code = '#session.view_setaside#'
		 </cfif>

		 <cfif #listlen(session.view_duns)# GT 0>
			<cfif #listlen(session.view_duns)# GT 1>
			<cfset dcounter = 1>
			and (
			 <cfloop index="dc" list="#session.view_duns#">
			   (recipient_duns = '#dc#')
			   <cfif dcounter LT #listlen(session.view_duns)#> or</cfif>
			   <cfset dcounter = dcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and recipient_duns = '#session.view_duns#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_naics)# GT 0>
			<cfif #listlen(session.view_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.view_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.view_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.view_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.view_psc)# GT 0>
			<cfif #listlen(session.view_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.view_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.view_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.view_psc#'
			</cfif>
		 </cfif> )

      <cfif #session.view_keyword# is not "">
	   and contains((award_description, awarding_agency_name, awarding_office_name, funding_office_name, awarding_sub_agency_name, award_id_piid, parent_award_id, solicitation_identifier),'#trim(session.view_keyword)#')
	  </cfif>

     group by recipient_duns, recipient_name
     order by amount DESC

   </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <cfoutput>
           <tr><td class="feed_header"><b>#ucase(session.view_name)# - DASHBOARD</b></td>
                <td class="feed_option" align=right><a href="award_search_run.cfm?award_view_id=#award_view_id#"><img src="/images/delete.png" width=20 border=0 alt="Close" value="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </cfoutput>
          </table>

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_sub_header">Total Awards: #numberformat(awards.total,'999,999')#</td></tr>
            <tr><td class="feed_sub_header">Total Obligations: #numberformat(awards.obligated,'$999,999,999')#</td></tr>
            <tr><td class="feed_sub_header">Total Allocations: #numberformat(awards.amount,'$999,999,999')#</td></tr>
            <tr><td><hr></td></tr>
          </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td>&nbsp;</td></tr>

           <tr>
              <td class="feed_sub_header" align=center>By NAICS Code (Top 25) - <cfoutput>#session.view_keyword#</cfoutput></td>
              <td></td>
              <td class="feed_sub_header" align=center>By Product or Service Code (Top 25)</td>
           </tr>

           <tr><td valign=top width=50%>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('naics'));

 						data.addColumn('string','NAICS');
 						data.addColumn('number','Amount');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="naics_total">
 						  <cfif counter LTE 25>
							  <cfif amount GTE 0>
							   <cfset description = #replace(naics_description,"'","","all")#>
							   ['#description#',#round(amount)#,'/exchange/awards/award_search_detail_naics.cfm?val=#naics_code#&award_view_id=#award_view_id#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="naics" style="width: 100%;"></div>

                 </td><td width=50>&nbsp;<td valign=top>


 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));

 						data.addColumn('string','NAICS');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="psc_total">
 						  <cfif counter LTE 25>
							  <cfif amount GTE 0>
							   ['#product_or_service_code_description#',#round(amount)#,'/exchange/awards/award_search_detail_psc.cfm?val=#product_or_service_code#&award_view_id=#award_view_id#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="psc_graph"></div>

				    </td></tr>

           <tr><td colspan=3><hr></td></tr>


           <tr>
           		<td class="feed_sub_header" align=center>By Agency</td>
           		<td></td>
           		<td class="feed_sub_header" align=center>By Vendor (Top 25)</td>
           </tr>

				    <tr><td valign=top>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('sub_agency'));

 						data.addColumn('string','Agency');
 						data.addColumn('number','Award Value');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="sub_agency">
 						  <cfif #amount# GTE 0>
							  <cfif counter LTE 25>
							   <cfset description = #replace(awarding_sub_agency_name,"'","","all")#>
							   ['#description#',#round(amount)#,'/exchange/awards/award_search_detail_agency.cfm?val=#awarding_sub_agency_code#&award_view_id=#award_view_id#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 12,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="sub_agency" style="width: 100%;"></div>

				    </td><td></td><td valign=top align=right>

 					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('duns'));

 						data.addColumn('string','Vendor');
 						data.addColumn('number','Award Value');
 						data.addColumn('string','Code');

                        <cfset counter = 0>
 						data.addRows([
 						  <cfoutput query="duns">
 						  <cfif #amount# GTE 0>
							  <cfif counter LTE 25>
							   <cfset description = #replace(recipient_name,"'","","all")#>
							   ['#description#',#round(amount)#,'/exchange/awards/award_search_detail_vendor.cfm?val=#recipient_duns#&award_view_id=#award_view_id#'],
							  </cfif>
 						  </cfif>
 						  <cfset counter = counter + 1>
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 12,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="duns" style="width: 100%;"></div>
				    </td></tr>
             </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td colspan=2 class="feed_sub_header"><b>Award Data</b></td></tr>
		  <tr><td colspan=6><hr></td></tr>

		  <tr><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>NAICS</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards *</b></td>
		       <td class="feed_option" align=right><b>Obligated</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>

		       </tr>

		  <cfoutput query="naics_total">
		   <tr><td class="feed_option" width=75 valign=top><a href="award_search_detail_naics.cfm?award_view_id=#award_view_id#&val=#naics_code#" target="_self">#naics_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_naics.cfm?award_view_id=#award_view_id#&val=#naics_code#" target="_self"><b>#naics_description#</b></a></td>
		       <td class="feed_option" valign=top align=center width=75>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>

               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_naics.cfm?award_view_id=#award_view_id#&val=#naics_code#" target="_self"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_naics.cfm?award_view_id=#award_view_id#&val=#naics_code#" target="_self"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>

		       </tr>

		  </cfoutput>

		  <tr><td colspan=6><hr></td></tr>

		   <tr><td class="feed_option" colspan=2><b>Agency</b></td>
		       <td class="feed_option" align=center><b>Awards *</b></td>
		       <td class="feed_option" align=right><b>Obligated</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>
		   </tr>

		  <cfoutput query="sub_agency">
		   <tr><td class="feed_option" colspan=2><a href="award_search_detail_agency.cfm?val=#awarding_sub_agency_code#&award_view_id=#award_view_id#" target="_self"><b>#awarding_sub_agency_name#</b></a></td>
		       <td class="feed_option" align=center width=75>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>


               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_agency.cfm?award_view_id=#award_view_id#&val=#awarding_sub_agency_code#" target="_self"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_agency.cfm?award_view_id=#award_view_id#&val=#awarding_sub_agency_code#" target="_self"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>
           </tr>
		  </cfoutput>

		  <tr><td colspan=6><hr></td></tr>

		   <tr><td class="feed_option" colspan=2><b>Vendor</b></td>
		       <td class="feed_option" align=center><b>Awards *</b></td>
		       <td class="feed_option" align=right><b>Obligated</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>
           </tr>

		  <cfoutput query="duns">
		   <tr><td class="feed_option" colspan=2><a href="award_search_detail_vendor.cfm?val=#recipient_duns#&award_view_id=#award_view_id#" target="_self"><b>#recipient_name#</b></a></td>
		       <td class="feed_option" align=center width=75>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>


               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_vendor.cfm?award_view_id=#award_view_id#&val=#recipient_duns#" target="_self"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_vendor.cfm?award_view_id=#award_view_id#&val=#recipient_duns#" target="_self"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>


           </tr>
		  </cfoutput>

          </table>

		      </td><td width=50>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>PSC</b></td>
		       <td class="feed_option"><b>Description</b></td>
		       <td class="feed_option" align=center><b>Awards *</b></td>
		       <td class="feed_option" align=right><b>Obligated</b></td>
		       <td class="feed_option" align=right><b>Allocated</b></td>
		       <td class="feed_option" align=right><b>Available</b></td>
           </tr>

		  <cfoutput query="psc_total">
		   <tr><td class="feed_option" valign=top width=50 valign=top><a href="award_search_detail_psc.cfm?award_view_id=#award_view_id#&val=#product_or_service_code#" target="_self">#product_or_service_code#</a></td>
		       <td class="feed_option" valign=top><a href="award_search_detail_psc.cfm?award_view_id=#award_view_id#&val=#product_or_service_code#" target="_self"><b>#product_or_service_code_description#</b></a></td>
		       <td class="feed_option" align=center valign=top width=75>#total#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(obligated,'$999,999,999')#</td>
		       <td class="feed_option" valign=top align=right width=100>#numberformat(amount,'$999,999,999')#</td>


               <td class="feed_option" align=right width=60 valign=top>

               <cfif evaluate(amount-obligated) LTE 0>
                <a href="award_search_detail_psc.cfm?award_view_id=#award_view_id#&val=#product_or_service_code#" target="_self"><progress value="10" max="10" alt="$0.00" title="$0.00" style="width:40px;"></progress></a>
               <cfelse>
                <a href="award_search_detail_psc.cfm?award_view_id=#award_view_id#&val=#product_or_service_code#" target="_self"><a href="award_search_detail_psc.cfm?val=#product_or_service_code#" target="_self"><progress value="#obligated#" max="#amount#" alt="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" title="#numberformat(evaluate(amount-obligated),'$999,999,999,999')#" style="width:40px;"></progress></a>
               </cfif>
               </td>

           </tr>
		  </cfoutput>

		  </table>

		     </td><td width=30>&nbsp;</td></tr>

          <tr><td>&nbsp;</td></tr>

		  </table>

  	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>