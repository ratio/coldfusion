<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 update award_view
 set award_view_last_ran = #now()#
 where award_view_id = #award_view_id# and
       award_view_usr_id = #session.usr_id#
</cfquery>

<cfquery name="view" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id# and
       award_view_id = #award_view_id#
</cfquery>

<cfset #session.award_search_name# = '#view.award_view_name#'>
<cfset #session.award_dept# = '#view.award_view_agency#'>
<cfset #session.award_from# = '#view.award_view_date_from#'>
<cfset #session.award_to# = '#view.award_view_date_to#'>
<cfset #session.award_state# = '#view.award_view_state#'>
<cfset #session.award_naics# = '#view.award_view_naics#'>
<cfset #session.award_psc# = '#view.award_view_psc#'>
<cfset #session.award_keyword# = '#view.award_view_keyword#'>
<cfset #session.award_setaside# = '#view.award_view_set_aside#'>
<cfset #session.duns# = '#view.award_view_duns#'>
<cfset #session.location_type# = #view.award_view_location_type#>
<cfset #session.date_type# = #view.award_view_date_type#>
<cfset #session.amount_from# = #view.award_view_amount_from#>
<cfset #session.amount_to# = #view.award_view_amount_to#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Build Query --->

   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select action_date, award_id_piid, modification_number, recipient_name, recipient_duns, recipient_city_name, recipient_state_code, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, award_description, primary_place_of_performance_city_name, primary_place_of_performance_state_code, naics_code, product_or_service_code, product_or_service_code_description, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_current_end_date, federal_action_obligation, id from award_data

    where (awarding_agency_code = '#session.award_dept#' and
          action_date between '#session.award_from#' and '#session.award_to#'

         <cfif session.location_type is 1>
          <cfif #session.award_state# is not 0>
		   and recipient_state_code = '#session.award_state#'
		  </cfif>
		 <cfelse>
          <cfif #session.award_state# is not 0>
		   and primary_place_of_performance_state_code = '#session.award_state#'
		  </cfif>
		 </cfif>

		 <cfif #session.award_setaside# is not 0>
		   and type_of_set_aside_code = '#session.award_setaside#'
		 </cfif>

		 <cfif #listlen(session.duns)# GT 0>
			<cfif #listlen(session.duns)# GT 1>
			<cfset dcounter = 1>
			and (
			 <cfloop index="dc" list="#session.duns#">
			   (recipient_duns = '#dc#')
			   <cfif dcounter LT #listlen(session.duns)#> or</cfif>
			   <cfset dcounter = dcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and recipient_duns = '#session.duns#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_naics)# GT 0>
			<cfif #listlen(session.award_naics)# GT 1>
			<cfset ncounter = 1>
			and (
			 <cfloop index="nc" list="#session.award_naics#">
			   (naics_code = '#nc#')
			   <cfif ncounter LT #listlen(session.award_naics)#> or</cfif>
			   <cfset ncounter = ncounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and naics_code = '#session.award_naics#'
			</cfif>
		 </cfif>

		 <cfif #listlen(session.award_psc)# GT 0>
			<cfif #listlen(session.award_psc)# GT 1>
			<cfset pcounter = 1>
			and (
			 <cfloop index="pc" list="#session.award_psc#">
			   (product_or_service_code = '#pc#')
			   <cfif pcounter LT #listlen(session.award_psc)#> or</cfif>
			   <cfset pcounter = pcounter + 1>
			 </cfloop>
			 )
			<cfelse>
			 and product_or_service_code = '#session.award_psc#'
			</cfif>
		 </cfif> )

      <cfif #session.award_keyword# is not "">
	   and contains((award_description),'#session.award_keyword#')
	  </cfif>

    <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by recipient_state_code ASC, recipient_city_name ASC
	<cfelseif #sv# is 50>
	 order by recipient_state_code ASC, recipient_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 10>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 100>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC

	</cfif>

   <cfelse>

	 order by action_date DESC

   </cfif>

   </cfquery>

   <cfif isdefined("export")>

	 <cfinclude template="/exchange/include/export_to_excel.cfm">

	</cfif>

<cfparam name="URL.PageId" default="0">
<cfset RecordsPerPage = 250>
<cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
<cfset StartRow = (URL.PageId*RecordsPerPage)+1>
<cfset EndRow = StartRow+RecordsPerPage-1>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><b><cfoutput>#ucase(view.award_view_name)# (#numberformat(agencies.recordcount,'999,999')#)</cfoutput></b></td>
               <td class="feed_option" align=right><a href="index.cfm"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

      </cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif agencies.recordcount is 0>
          <cfoutput>

           <tr><td class="feed_sub_header">No awards or mods from you saved search.</td>
               <td align=right><a href="/exchange/awards/edit_view.cfm?award_view_id=#award_view_id#"><img src="/images/icon_config.png" border=0 width=20 alt="Configure" title="Configure"></a></td></tr>
          </cfoutput>
          <cfelse>

           <form action="save_search.cfm" method="post">

           <tr><td class="feed_option" colspan=7>
            <b>Save Search&nbsp;&nbsp;</b><input type="text" name="award_view_name" required size=30>&nbsp;&nbsp;<input class="button_blue" type="submit" style="width: 50px; height: 22px;" name="button" value="Save"></td>
            <td class="feed_option" align=right colspan=9>
              <b>
              <a href="award_search.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>&nbsp;|&nbsp;
              <a href="award_search_dashboard.cfm">TAM Dashboard</a></td></tr>
           <td><td class="feed_option" colspan=15 align=right>
              </b>

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&award_view_id=#award_view_id#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
           </td></tr>

           </form>

           <tr><td height=20></td></tr>

          <cfoutput>

			  <tr>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract ##</b></a></td>
				 <td class="text_xsmall"><b>Mod</b></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Vendor</b></a></td>

                 <cfif session.location_type is 1>
				  <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>City</b></a></td>
				  <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>State</b></a></td>
				 <cfelse>
				  <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=16<cfelse><cfif #sv# is 16>sv=160<cfelse>sv=15</cfif></cfif>"><b>City</b></a></td>
				  <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=17<cfelse><cfif #sv# is 17>sv=170<cfelse>sv=16</cfif></cfif>"><b>State</b></a></td>

				 </cfif>
				 <td class="text_xsmall"><b>Award Description</b></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=15<cfelse><cfif #sv# is 15>sv=150<cfelse>sv=15</cfif></cfif>"><b>Agency</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Office</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PSC</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Set Aside</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Pricing</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP Start</b></a></td>
				 <td class="text_xsmall"><a href="award_search.cfm?<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PoP End</b></a></td>
				 <td class="text_xsmall" align=right><a href="award_search.cfm?<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Obligation</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff">
			   <cfelse>
				<tr bgcolor="e0e0e0">

			   </cfif>

			   <cfoutput>

				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener" rel="noreferrer">#agencies.award_id_piid#</a></td>
				   <td class="text_xsmall" valign=top>#agencies.modification_number#</td>
				   <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#agencies.recipient_name#</a></td>
					   <td class="text_xsmall" valign=top>#agencies.primary_place_of_performance_city_name#</td>
					   <td class="text_xsmall" valign=top>#agencies.primary_place_of_performance_state_code#</td>
				   <td class="text_xsmall" valign=top width=300>#agencies.award_description#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_sub_agency_name#</td>
				   <td class="text_xsmall" valign=top>#agencies.awarding_office_name#</td>
				   <td class="text_xsmall" valign=top width=75>#agencies.naics_code#</td>
				   <td class="text_xsmall" valign=top width=50>#agencies.product_or_service_code#</td>
				   <td class="text_xsmall" valign=top width=50>#agencies.type_of_set_aside#</td>
				   <td class="text_xsmall" valign=top width=100>#agencies.type_of_contract_pricing#</td>
				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.period_of_performance_start_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top width=75>#dateformat(agencies.period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top align=right width=75>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

		  <cfset tot = tot + #federal_action_obligation#>

 </cfif>

								  <cfif CurrentRow eq EndRow>
								   <cfbreak>
								  </cfif>

          </cfloop>

          <tr><td colspan=16><hr></td></tr>
          <tr>
             <td class="feed_option"><b>Total</b></td>
             <td class="feed_option" colspan=16 align=right><b><cfoutput>#numberformat(tot,'$999,999,999')#</cfoutput></b></td>
          </tr>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>