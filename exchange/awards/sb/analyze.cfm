<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 where set_aside_code = '#code#'
</cfquery>

<cfif not isdefined("session.award_from") or not isdefined("session.award_to")>
	<cfset #session.award_from# = #dateadd('d', -730, now())#>
	<cfset #session.award_to# = #now()#>
</cfif>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(recipient_duns)) as vendors, count(naics_code) as awards, sum(federal_action_obligation) as amount, recipient_state_code, recipient_state_name from award_data
 where action_date between '#dateformat(session.award_from,'mm/dd/yyyy')#' and '#dateformat(session.award_to,'mm/dd/yyyy')#'
 and type_of_set_aside_code =  '#code#'
 and recipient_state_code <> ''
 and recipient_country_code = 'USA'
 group by recipient_state_name, recipient_state_code
 order by amount DESC
 </cfquery>

<cfif isdefined("export")>
<cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top>Federal Awards - By Small Business</td>
               <td align=right valign=top class="feed_sub_header"><a href="/exchange/awards/sb/">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header" valign=top><cfoutput>#set_aside.set_aside_name#</cfoutput></td></tr>
           <tr><td height=5></td></tr>
           </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="/exchange/awards/set.cfm" method="post">

		  <tr>

           <td class="feed_sub_header" valign=middle>Filter Options<td>
           <td class="feed_option" valign=middle align=right></b>

		   <cfoutput>

                      <form action="/exchange/awards/set.cfm" method="post">
	                   <b>From:</b>&nbsp;&nbsp;<input type="date" class="input_date" name="from" style="width: 160px;" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               <b>To:</b>&nbsp;&nbsp;<input type="date" class="input_date" name="to" style="width: 160px;" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>

		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="setaside_1a">
                       <input type="hidden" name="code" value='#code#'>

          </td></tr>

          <tr><td colspan=4><hr></td></tr>
          <tr><td height=10></td></tr>

          </cfoutput>

          </form>

          </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>
           <tr><td class="feed_sub_header">
           <a href="/exchange/awards/sb/analyze.cfm?code=#code#"><u>Awards by State</u></a>&nbsp;|&nbsp;
           <a href="/exchange/awards/sb/analyze_department.cfm?code=#code#">Awards by Agency</a>&nbsp;|&nbsp;
           <a href="/exchange/awards/sb/analyze_vendor.cfm?code=#code#">Awards by Vendor</a>
           </td>
               <td align=right colspan=3 class="feed_sub_header"><a href="/exchange/awards/sb/analyze.cfm?code=<cfoutput>#code#</cfoutput>&export=1&view=1"><img src="/images/icon_export_excel.png" hspace=10 width=20 alt="Export to Excel" title="Export to Excel" border=0></a><a href="/exchange/awards/sb/analyze.cfm?code=<cfoutput>#code#</cfoutput>&export=1&view=1">Export to Excel</a></td></tr>
           <tr><td height=10></td></tr>
           </cfoutput>

           <cfif #agencies.recordcount# is 0>
           <tr><td class="feed_sub_header">No Set Asides awards were found.</td></tr>
           <cfelse>

           <tr><td class="feed_option"><b>STATE</b></td>
               <td class="feed_option" align=center><b>VENDORS</b></td>
               <td class="feed_option" align=center><b>AWARDS</b></td>
               <td class="feed_option" align=right width=100><b>AMOUNT</b></td></tr>

           <tr><td height=10></td></tr>

           <cfset scounter = 0>
           <cfoutput query="agencies">
            <tr height=30

            <cfif scounter is 0>
             bgcolor="ffffff"
            <cfelse>
             bgcolor="e0e0e0"
            </cfif>

            ><td class="feed_sub_header"><a href="/exchange/awards/sb/analyze_city.cfm?code=#code#&state=#recipient_state_code#"><b>#recipient_state_name#</b></a></td>
                <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(vendors,'999,999')#</td>
                <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(awards,'999,999')#</td>
                <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(amount,'$999,999,999')#</td></tr>

           <cfif scounter is 0>
            <cfset scounter = 1>
           <cfelse>
            <cfset scounter = 0>
           </cfif>

           </cfoutput>

          </cfif>

          <tr><td height=10></td></tr>
          <tr><td colspan=2 class="feed_option">Note - States that are not listed do not have any awards for the criteria you have chosen.</td></tr>
          <tr><td height=10></td></tr>

		  </table>

        </td></tr>

     </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>