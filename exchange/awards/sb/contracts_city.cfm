<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 where set_aside_code = '#code#'
 order by set_aside_name
</cfquery>

<cfquery name="city_state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 where state_abbr = '#state#'
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select action_date, award_id_piid, recipient_name, recipient_duns, recipient_city_name, recipient_state_code, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, award_description, naics_code, product_or_service_code_description, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_potential_end_date, federal_action_obligation, id from award_data
 where (type_of_set_aside_code = '#code#'
   and recipient_state_code = '#state#'
   and recipient_city_name = '#city#'
   and recipient_country_code = 'USA'
   and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

)

<cfif isdefined("session.filter_keyword")>
and contains((award_description),'"#trim(session.filter_keyword)#"')
</cfif>

<cfif isdefined("sv")>

<cfif #sv# is 1>
 order by action_date ASC
<cfelseif #sv# is 10>
 order by action_date DESC
<cfelseif #sv# is 2>
 order by awarding_agency_name ASC
<cfelseif #sv# is 20>
 order by awarding_agency_name DESC
<cfelseif #sv# is 3>
 order by awarding_sub_agency_name ASC
<cfelseif #sv# is 30>
 order by awarding_sub_agency_name DESC
<cfelseif #sv# is 4>
 order by awarding_office_name ASC
<cfelseif #sv# is 40>
 order by awarding_office_name DESC
<cfelseif #sv# is 5>
 order by awarding_office_name ASC
<cfelseif #sv# is 50>
 order by awarding_office_name DESC
<cfelseif #sv# is 6>
 order by naics_code ASC
<cfelseif #sv# is 60>
 order by naics_code DESC
<cfelseif #sv# is 7>
 order by type_of_set_aside ASC
<cfelseif #sv# is 70>
 order by type_of_set_aside DESC
<cfelseif #sv# is 8>
 order by type_of_contract_pricing ASC
<cfelseif #sv# is 80>
 order by type_of_contract_pricing DESC
<cfelseif #sv# is 9>
 order by period_of_performance_start_date ASC
<cfelseif #sv# is 90>
 order by period_of_performance_start_date DESC
<cfelseif #sv# is 10>
 order by period_of_performance_potential_end_date ASC
<cfelseif #sv# is 100>
 order by period_of_performance_potential_end_date DESC
<cfelseif #sv# is 11>
 order by federal_action_obligation DESC
<cfelseif #sv# is 110>
 order by federal_action_obligation ASC
<cfelseif #sv# is 12>
 order by award_id_piid ASC, modification_number
<cfelseif #sv# is 120>
 order by award_id_piid DESC, modification_number
</cfif>

<cfelse>
 order by action_date DESC
</cfif>

</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

           <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		    <tr><td class="feed_header" valign=top>#set_aside.set_aside_name#</td>
			    <td align=right valign=top class="feed_sub_header"><a href="/exchange/awards/sb/analyze_city.cfm?code=#code#&state=#state#">Return</a></td></tr>
		    <tr><td colspan=2><hr></td></tr>
		    <tr><td class="feed_sub_header" valign=top>#ucase(city)#, #state# (#trim(numberformat(agencies.recordcount,'999,999'))# Awards)</td>
		        <td align=right class="feed_sub_header"><a href="/exchange/awards/sb/contracts_city.cfm?code=#code#&state=#state#&city=#city#&export=1"><img src="/images/icon_export_excel.png" hspace=10 align=absmiddle border=0 width=20 alt="Export to Excel" title="Export to Excel"></a><a href="/exchange/awards/sb/contracts_city.cfm?code=#code#&state=#state#&city=#city#&export=1">Export to Excel</a></td></tr>
		   </table>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <form action="/exchange/awards/set.cfm" method="post">

           <tr><td class="feed_sub_header" valign=top><b>Filter Options</b></td>

           <td align=right class="feed_option" valign=top>

                       <b>Keyword:</b>&nbsp;
                       <input type="text" class="input_text" name="filter" size=20 <cfif isdefined("session.filter_keyword")> value="#session.filter_keyword#"</cfif>>&nbsp;

	                   &nbsp;&nbsp;<b>From:</b>&nbsp;&nbsp;<input type="date" name="from" class="input_date" style="width: 160px;" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               &nbsp;&nbsp;<b>To:</b>&nbsp;&nbsp;<input type="date" name="to" class="input_date" style="width: 160px;" <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="setaside_1e">
                       <input type="hidden" name="code" value='#code#'>
                       <input type="hidden" name="state" value='#state#'>
                       <input type="hidden" name="city" value='#city#'>
           </cfoutput>

          </td>
          </tr>

          </form>

          <tr><td colspan=2><hr></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

          <tr height=40>

             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Contract ##</b></a></td>

             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Vendor</b></a></td>


             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Department</b></a></td>
             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Agency</b></a></td>
             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Office</b></a></td>
             <td class="text_xsmall"><b>Award Description</b></td>
             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>NAICS</b></a></td>
             <td class="text_xsmall"><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Pricing</b></a></td>
             <td class="text_xsmall" width=75><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PoP Start</b></a></td>
             <td class="text_xsmall" width=75><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP End</b></a></td>
             <td class="text_xsmall" align=right><a href="contracts_city.cfm?code=#code#&state=#state#&city=#city#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Obligation</b></a></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="text_xsmall" valign=top width=75>#dateformat(action_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top width=125><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#recipient_name#</b></a></td>
               <td class="text_xsmall" valign=top>#awarding_agency_name#</a></td>
               <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>

               <td class="text_xsmall" valign=top width=400>

               <cfif isdefined("session.filter_keyword")>
               #replaceNoCase(award_description,session.filter_keyword,"<span style='background:yellow'>#ucase(session.filter_keyword)#</span>","all")#
               <cfelse>
               #award_description#
               </cfif>

               </td>

               <td class="text_xsmall" valign=top>#naics_code#</td>
               <td class="text_xsmall" valign=top>#left(type_of_contract_pricing,21)#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_start_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_potential_end_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             </tr>

          <cfset tot = tot + #federal_action_obligation#>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=12><hr></td></tr>
          <tr><td class="text_xsmall" colspan=11><b>Total:</b></td>
              <td class="text_xsmall" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>


          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>