<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

  		   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from department
			where department_code = '#department_code#'
			order by department_name
		   </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;<a href="/exchange/awards/agency/">By Department</a></td>

           <td align=right class="feed_option">

           <cfif not isdefined("session.award_from")>
            <cfset #session.award_from# = '1/1/2018'>
            <cfset #session.award_to# = '3/1/2018'>
           </cfif>

           <cfoutput>

                      <form action="/exchange/awards/set.cfm" method="post">
	                   From:&nbsp;<input type="text" name="from" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'mm/dd/yyyy')#"</cfif>>
		               To:&nbsp;<input type="text" name="to" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_to,'mm/dd/yyyy')#"</cfif>>
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="agency_2">
                       <input type="hidden" name="department_code" value="#department_code#">
                       </form>
           </cfoutput>

          </td></tr>

          <tr><td class="feed_option"><b><cfoutput>#department.department_name#</cfoutput></b></td></tr>


          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select awarding_sub_agency_code, awarding_sub_agency_name, sum(federal_action_obligation) as total, count(naics_code) as awards, count(distinct(recipient_duns)) as count from award_data
			where awarding_agency_code = '#department_code#'
 		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
			group by awarding_sub_agency_code, awarding_sub_agency_name

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 10>
		     order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 2>
		     order by awards DESC
		    <cfelseif #sv# is 20>
		     order by awards ASC
		    <cfelseif #sv# is 3>
		     order by count DESC
		    <cfelseif #sv# is 30>
		     order by count ASC
		    <cfelseif #sv# is 4>
		     order by total DESC
		    <cfelseif #sv# is 40>
		     order by total ASC
		    </cfif>

		   <cfelse>
		     order by total DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

          <tr>

             <td class="feed_option"><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Agency</b></a></td>
             <td class="feed_option" align=center><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Awards</b></a></td>
             <td class="feed_option" align=center><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Vendors</b></a></td>
             <td class="feed_option" align=right width=100><a href="sub_agency.cfm?department_code=#department_code#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Total</b></a></td>

          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="feed_option"><a href="vendors.cfm?department_code=#department_code#&agency_code=#awarding_sub_agency_code#">#awarding_sub_agency_name#</a></td>
               <td class="feed_option" align=center>#numberformat(awards,'999,999')#</td>
               <td class="feed_option" align=center>#numberformat(count,'999,999')#</td>
               <td class="feed_option" align=right>#numberformat(total,'$999,999,999,999')#</td>
             </tr>

           <cfset tot = tot + total>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=4><hr></td></tr>
          <tr><td class="feed_option" colspan=3><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>
          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>