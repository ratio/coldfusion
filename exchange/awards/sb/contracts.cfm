<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.award_from")>
 <cfset session.award_from = #from#>
</cfif>

<cfif not isdefined("session.award_to")>
 <cfset session.award_to = #to#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;<a href="/exchange/awards/sb/">By Small Business Type</a></td>
           <td align=right class="feed_option" valign=top>

           <cfoutput>

                      <form action="/exchange/awards/set.cfm" method="post">

                       Filter by Keyword:&nbsp;
                       <input type="text" name="filter" size=20 <cfif isdefined("session.filter_keyword")> value="#session.filter_keyword#"</cfif>>&nbsp;

	                   From:&nbsp;<input type="date" name="from" size=8 maxlength=10 value="#dateformat(session.award_from,'yyyy-mm-dd')#">
		               To:&nbsp;<input type="date" name="to" size=8 maxlength=10 value="#dateformat(session.award_to,'yyyy-mm-dd')#">
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="sb_1">
					   <input type="hidden" name="department_code" value=#department_code#>
					   <input type="hidden" name="set_aside_code" value=#set_aside_code#>
                       <input type="hidden" name="restrict" value=1>
                      </form>
           </cfoutput>

          </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="department" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from department
			where department_code = '#department_code#'
		   </cfquery>

  		   <cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from set_aside
			where set_aside_code = '#set_aside_code#'
		   </cfquery>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select action_date, award_id_piid, recipient_name, recipient_duns, recipient_city_name, recipient_state_code, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, award_description, product_or_service_code_description, naics_code, type_of_set_aside, type_of_contract_pricing, period_of_performance_start_date, period_of_performance_potential_end_date, federal_action_obligation, id from award_data
			where (((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#')) and
			      type_of_set_aside_code = '#set_aside_code#' and
				  awarding_agency_code = '#department_code#'

				  )

		   <cfif isdefined("session.filter_keyword")>
		   and contains((award_description, product_or_service_code_description),'"#session.filter_keyword#"')
		   </cfif>

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by action_date ASC
		    <cfelseif #sv# is 10>
             order by action_date DESC
		    <cfelseif #sv# is 2>
             order by recipient_name ASC
		    <cfelseif #sv# is 20>
             order by recipient_name DESC
		    <cfelseif #sv# is 3>
             order by recipient_state_code ASC
		    <cfelseif #sv# is 30>
             order by recipient_state_code DESC
		    <cfelseif #sv# is 4>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 5>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 50>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 6>
             order by product_or_service_code_description ASC
		    <cfelseif #sv# is 60>
             order by product_or_service_code_description DESC
		    <cfelseif #sv# is 7>
             order by naics_code ASC
		    <cfelseif #sv# is 70>
             order by naics_code DESC
		    <cfelseif #sv# is 8>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 80>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 9>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 90>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 10>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 100>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 11>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 110>
             order by federal_action_obligation ASC
		    <cfelseif #sv# is 12>
             order by award_id_piid ASC, modification_number DESC
		    <cfelseif #sv# is 120>
             order by award_id_piid DESC, modification_number
		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		  </cfquery>


          <cfif isdefined("export")>

             <cfinclude template="/exchange/include/export_to_excel.cfm">

          </cfif>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

          <tr><td class="feed_option" colspan=9><b>#department.department_name#<br>Set Aside Type - #set_aside.set_aside_name#<br>Awards - #numberformat(agencies.recordcount,'999,999,999')#</td>


           <td class="feed_option" align=right colspan=4 valign=top>
              <cfoutput>
              <a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><b>Export to Excel</b></a>
              </cfoutput>

           </td>

          </tr>
          <tr><td>&nbsp;</td></tr>

          <tr>

             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Contract ##</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Vendor</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Office</b></a></td>
             <td class="text_xsmall"><b>Award Description</b></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Product or Service</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>NAICS</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Pricing</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PoP Start</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP End</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?department_code=#department_code#&set_aside_code=#set_aside_code#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 10>sv=110<cfelse>sv=11</cfif></cfif>"><b>Obligation</b></a></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="text_xsmall" valign=top>#dateformat(action_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
               <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a></td>
               <td class="text_xsmall" valign=top>#recipient_state_code#</td>
               <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>
               <td class="text_xsmall" valign=top>#award_description#</td>
               <td class="text_xsmall" valign=top>#product_or_service_code_description#</td>
               <td class="text_xsmall" valign=top>#naics_code#</td>
               <td class="text_xsmall" valign=top>#left(type_of_contract_pricing,21)#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             </tr>

          <cfset tot = tot + #federal_action_obligation#>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=13><hr></td></tr>
          <tr><td class="text_xsmall" colspan=12><b>Total:</b></td>
              <td class="text_xsmall" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>