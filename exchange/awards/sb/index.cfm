<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfif not isdefined("session.award_to") or not isdefined("session.award_from")>
 <cfset #session.award_to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.award_from# = #dateformat(dateadd("d",-730,now()),'yyyy-mm-dd')#>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Federal Awards - By Small Business</b></td>
               <td align=right></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td class="feed_sub_header" style="font-weight: normal;">The Federal Government awards millions of dollars each year to qualified <a href="http://www.sba.gov" target="_blank" rel="noopener" rel="noreferrer"><b>Small Business Administration (SBA)</a></b> small businesses .   To analyze government spending for each classification, please choose one below.</td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header">SMALL BUSINESS CLASSIFICATION</td></tr>
           <cfset counter = 0>
           <cfoutput query="set_aside">
           <cfif counter is 0>
            <tr bgcolor="FFFFFF" height=30>
           <cfelse>
            <tr bgcolor="e0e0e0" height=30>
           </cfif>

            <td class="feed_sub_header"><a href="/exchange/awards/sb/analyze.cfm?code=#set_aside_code#"><b>#set_aside_name#</b></a></td></tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

           </cfoutput>

          </table>

       </td></tr>

    </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>