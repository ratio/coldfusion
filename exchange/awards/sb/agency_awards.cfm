<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

 	  <div class="exchange_filter_box">

	      <cfinclude template="/exchange/analytics/selection.cfm">

	  </div>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Agency Analytics</td>


           <td align=right class="feed_option">

           <a href="agency.cfm">Return</a>

           </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  		    select sum(federal_action_obligation) as total from award_data
  		    where awarding_agency_name = '#contracting_agency#'
  		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
 		   </cfquery>

  		   <cfquery name="count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  		    select count(naics_code) as total from award_data
  		    where awarding_agency_name = '#contracting_agency#'
  		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
            </cfquery>

		  <cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select recipient_name, recipient_state_code, count(naics_code) as awards, sum(federal_action_obligation) as total from award_data
           where awarding_agency_name = '#contracting_agency#'
           <cfif isdefined("session.award_from")>
           and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
           </cfif>

		   group by recipient_name, recipient_state_code

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by recipientr_name ASC
		    <cfelseif #sv# is 10>
		     order by recipient_name DESC
		    <cfelseif #sv# is 2>
		     order by awards DESC
		    <cfelseif #sv# is 20>
		     order by awards ASC
		    <cfelseif #sv# is 3>
		     order by recipient_state_code ASC
		    <cfelseif #sv# is 30>
		     order by recipient_state_code DESC
		    <cfelseif #sv# is 4>
		     order by total DESC
		    <cfelseif #sv# is 40>
		     order by total ASC
		    </cfif>

		   <cfelse>
		     order by total DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>

 		  <cfoutput>

 		  <tr><td class="feed_option" colspan=4 valign=top>
 		  <b>Contracting Agency: </b><a href="/exchange/include/company_profile.cfm">#contracting_agency#</a></br>
 		  <b>Total Awards: </b>#numberformat(count.total,'999,999,999')#</br>
 		  <b>Total Award Value: </b>#numberformat(total.total,'$999,999,999')#<br>
 		  </td>

 		  <td valign=top class="feed_option" align=right colspan=2>



            <cfoutput>

                       <form action="set.cfm" method="post">
 	                   From:&nbsp;<input type="text" name="from" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'mm/dd/yyyy')#"</cfif>>
 		               To:&nbsp;<input type="text" name="to" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_to,'mm/dd/yyyy')#"</cfif>>
 		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 50px;" type="submit" name="button" value="Filter">
                        <input type="hidden" name="location" value="agency_2">
                        <input type="hidden" name="contracting_agency" value="#contracting_agency#">
                        </form>
           </cfoutput>



 		  </td></tr>

          </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>


          <tr>

             <td class="feed_option"><a href="agency_awards.cfm?contracting_agency=#contracting_agency#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Vendor Name</b></a></td>
             <td class="feed_option" align=center><a href="agency_awards.cfm?contracting_agency=#contracting_agency#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>State</b></a></td>
             <td class="feed_option" align=center><a href="agency_awards.cfm?contracting_agency=#contracting_agency#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Awards</b></a></td>
             <td class="feed_option" align=right><a href="agency_awards.cfm?contracting_agency=#contracting_agency#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Total</b></a></td>

          </tr>

          </cfoutput>

          <cfloop query="info">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

           <cfoutput>

               <td class="feed_option"><a href="agency_awards_details.cfm?contracting_agency=#contracting_agency#&vendor_name=#info.recipient_name#">#info.recipient_name#</a></td>
               <td class="feed_option" align=center>#info.recipient_state_code#</a></td>
               <td class="feed_option" align=center>#numberformat(info.awards,'99,999')#</a></td>
               <td class="feed_option" align=right>#numberformat(info.total,'$999,999,999')#</td>

           </cfoutput>

            </tr>

           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfloop>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>