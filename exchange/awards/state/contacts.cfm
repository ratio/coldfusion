<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.set_aside_code")>
 <cfset #session.set_aside_code# = 0>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 where state_abbr = '#state_abbr#'
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><a href="/exchange/awards/"><cfoutput>#ucase(city)#, #ucase(state.state_name)# - CONTACTS</cfoutput></td>
           <td align=right class="feed_option" valign=top>

           <cfoutput>

            <a href="vendors.cfm?state_abbr=#state_abbr#&city=#city#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>

           </cfoutput>
          <tr><td colspan=2><hr></td></tr>
          </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif session.location_type is 1>

			   <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select sum(federal_action_obligation) as total from award_data
				where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
				  and recipient_state_code = '#state_abbr#'
				  and recipient_country_code = 'USA'
				  and recipient_city_name = '#city#'

				<cfif session.set_aside_code is not 0>

				 <cfif session.set_aside_code is 1>
				  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
				 <cfelse>
				  and (type_of_set_aside_code =  '#session.set_aside_code#')
				 </cfif>

				</cfif>
			   </cfquery>

			   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select distinct(recipient_duns), recipient_name, sum(federal_action_obligation) as total, count(naics_code) as awards from award_data
				where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
				  and recipient_state_code = '#state_abbr#'
				  and recipient_country_code = 'USA'
				  and recipient_city_name = '#city#'

				<cfif session.set_aside_code is not 0>

				 <cfif session.set_aside_code is 1>
				  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
				 <cfelse>
				  and (type_of_set_aside_code =  '#session.set_aside_code#')
				 </cfif>

				</cfif>

				group by recipient_duns, recipient_name

			   <cfif isdefined("sv")>

				<cfif #sv# is 1>
				 order by recipient_name DESC
				<cfelseif #sv# is 10>
				 order by recipient_name ASC
				<cfelseif #sv# is 2>
				 order by recipient_duns ASC
				<cfelseif #sv# is 20>
				 order by recipient_duns DESC
				<cfelseif #sv# is 3>
				 order by awards DESC
				<cfelseif #sv# is 30>
				 order by awards ASC
				<cfelseif #sv# is 5>
				 order by total DESC
				<cfelseif #sv# is 50>
				 order by total ASC
				</cfif>

			   <cfelse>
				 order by total DESC
			   </cfif>

			  </cfquery>

          <cfelse>

			   <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select sum(federal_action_obligation) as total from award_data
				where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
				  and primary_place_of_performance_country_code = 'USA'
				  and primary_place_of_performance_state_code = '#state_abbr#'
				  and primary_place_of_performance_city_name = '#city#'

				<cfif session.set_aside_code is not 0>

				 <cfif session.set_aside_code is 1>
				  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
				 <cfelse>
				  and (type_of_set_aside_code =  '#session.set_aside_code#')
				 </cfif>

				</cfif>

			   </cfquery>

			   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select distinct(recipient_duns), recipient_name, sum(federal_action_obligation) as total, count(naics_code) as awards from award_data
				where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
				  and primary_place_of_performance_country_code = 'USA'
				  and primary_place_of_performance_state_code = '#state_abbr#'
				  and primary_place_of_performance_city_name = '#city#'

				<cfif session.set_aside_code is not 0>

				 <cfif session.set_aside_code is 1>
				  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
				 <cfelse>
				  and (type_of_set_aside_code =  '#session.set_aside_code#')
				 </cfif>

				</cfif>

				group by recipient_duns, recipient_name

			   <cfif isdefined("sv")>

				<cfif #sv# is 1>
				 order by recipient_name DESC
				<cfelseif #sv# is 10>
				 order by recipient_name ASC
				<cfelseif #sv# is 2>
				 order by recipient_duns ASC
				<cfelseif #sv# is 20>
				 order by recipient_duns DESC
				<cfelseif #sv# is 3>
				 order by awards DESC
				<cfelseif #sv# is 30>
				 order by awards ASC
				<cfelseif #sv# is 5>
				 order by total DESC
				<cfelseif #sv# is 50>
				 order by total ASC
				</cfif>

			   <cfelse>
				 order by total DESC
			   </cfif>

			  </cfquery>

          </cfif>

		  <cfset counter = 0>

		  <tr>
		     <td class="feed_sub_header"><b>VENDOR</b></td>
		     <td class="feed_sub_header"><b>DUNS</b></td>
		     <td class="feed_sub_header" align=center><b>AWARDS</b></td>
		     <td class="feed_sub_header" align=right><b>AMOUNT</b></td>
		     <td class="feed_sub_header" align=right><b>SHARE</b></td>
		  </tr>

          <cfloop query="agencies">

      		<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
      		 select * from sams
      		 where duns = '#agencies.recipient_duns#'
      		</cfquery>

             <tr bgcolor="ffffff">

           <cfoutput>
           <tr bgcolor="e0e0e0">
               <td class="feed_option"><b>#recipient_name#</b></td>
               <td class="feed_option"><b>#recipient_duns#</a></b></td>
               <td class="feed_option" align=center><b>#numberformat(awards,'999,999')#</b></td>
               <td class="feed_option" align=right><b>#numberformat(total,'$999,999,999,999')#</b></td>

               <cfif evaluate(agencies.total/total.total) LT 0>
	               <td class="feed_option" align=right><b>0.00%</b></td>
               <cfelse>
	               <td class="feed_option" align=right><b>#numberformat(evaluate((agencies.total/total.total)*100),'99.999')#%</b></td>
               </cfif>

           </tr>

           <tr><td colspan=5>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				    <tr><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Primary Point of Contact</b></td></tr>
						<tr><td class="feed_option">#sams.poc_fnme# #sams.poc_lname#<br>
						                            <cfif #sams.poc_title# is not "">#sams.poc_title#<br></cfif>
						                            (#left(sams.poc_us_phone,3)#) #mid(sams.poc_us_phone,4,3)#-#right(sams.poc_us_phone,4)#<br>
						                            <a href="mailto:#sams.poc_email#">#sams.poc_email#</a></td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Alternate Point of Contact</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.alt_poc_lname# is not "">#sams.alt_poc_fname# #sams.alt_poc_lname#<br><cfelse>Not Provided</cfif>
						                            <cfif #sams.alt_poc_title# is not "">#sams.alt_poc_title#<br></cfif>
						                            <cfif #sams.akt_poc_phone# is not "">(#left(sams.akt_poc_phone,3)#) #mid(sams.akt_poc_phone,4,3)#-#right(sams.akt_poc_phone,4)#<br></cfif>
						                            <cfif #sams.alt_poc_email# is not ""><a href="mailto:#sams.alt_poc_email#">#sams.alt_poc_email#</a></cfif></td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Past Performance Point of Contact</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.pp_poc_lname# is not "">#sams.pp_poc_fname# #sams.pp_poc_lname#<br><cfelse>Not Provided</cfif>
						                            <cfif #sams.pp_poc_title# is not "">#sams.pp_poc_title#<br></cfif>
						                            <cfif #sams.pp_poc_phone# is not "">(#left(sams.pp_poc_phone,3)#) #mid(sams.pp_poc_phone,4,3)#-#right(sams.pp_poc_phone,4)#<br></cfif>
						                            <cfif #sams.pp_poc_email# is not ""><a href="mailto:#sams.pp_poc_email#">#sams.pp_poc_email#</a></cfif></td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Electronic Point of Contact</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.elec_bus_poc_lnmae# is not "">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#<br><cfelse>Not Provided</cfif>
						                            <cfif #sams.elec_bus_poc_title# is not "">#sams.elec_bus_poc_title#<br></cfif>
						                            <cfif #sams.elec_bus_poc_us_phone# is not "">(#left(sams.elec_bus_poc_us_phone,3)#) #mid(sams.elec_bus_poc_us_phone,4,3)#-#right(sams.elec_bus_poc_us_phone,4)#<br></cfif>
						                            <cfif #sams.elec_bus_poc_email# is not ""><a href="mailto:#sams.elec_bus_poc_email#">#sams.elec_bus_poc_email#</a></cfif></td></tr>
						</table>

                    </td></tr>
                  </table>

              </td></tr>

           </cfoutput>

          </cfloop>


		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>