<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.location_type")>
 <cfset #session.location_type# = 1>
</cfif>

<cfif not isdefined("session.set_aside_code")>
 <cfset session.set_aside_code = 0>
</cfif>

<cfset #session.dashboard# = 1>

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 where state_abbr = '#state_abbr#'
</cfquery>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header" valign=top>#ucase(state.state_name)# DASHBOARD</td>
				   <td align=right valign=top><a href="city.cfm?state_abbr=#state_abbr#&state_type=#session.location_type#"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close" border=0></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			  </table>

          </cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="/exchange/awards/set.cfm" method="post">

           <tr><td class="feed_sub_header" valign=top>Filter Options

           <td align=right class="feed_option" valign=top>

                       <input type="radio" name="location_type" value=1 <cfif #session.location_type# is 1>checked</cfif>>&nbsp;Contractor Location&nbsp;&nbsp;
                       <input type="radio" name="location_type" value=2 <cfif #session.location_type# is 2>checked</cfif>>&nbsp;Place of Performance&nbsp;&nbsp;&nbsp;&nbsp;

				       <select name="selected_set_aside_code" class="input_select" style="width:250px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>NO SET ASIDE
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>#set_aside_name#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

          <cfoutput>

	                   <b>From:</b>&nbsp;&nbsp;<input type="date" class="input_date" name="from" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               <b>To:</b>&nbsp;&nbsp;<input type="date" class="input_date" name="to" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>
		               <input class="button_blue" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="state_1">
                       <input type="hidden" name="state_abbr" value=#state_abbr#>

          </td></tr>

          </form>

          <tr><td colspan=2><hr></td></tr>

          </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif session.location_type is 1>

			   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select top(25) count(recipient_duns) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_name)) as vendors, sum(federal_action_obligation) as total, recipient_city_name as city from award_data
				where action_date between '#session.award_from#' and '#session.award_to#'
					  and recipient_state_code = '#state_abbr#'
			          and recipient_country_code = 'USA'

				<cfif session.set_aside_code is not 0>

					 <cfif session.set_aside_code is 1>
					  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
					 <cfelse>
					  and (type_of_set_aside_code =  '#session.set_aside_code#')
					 </cfif>

					</cfif>

					group by recipient_city_name

					 order by total DESC

				  </cfquery>

          <cfelse>

 		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select top(25) count(recipient_duns) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_name)) as vendors, sum(federal_action_obligation) as total, primary_place_of_performance_city_name as city from award_data
			where ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))
			      and primary_place_of_performance_state_code = '#state_abbr#'
			      and recipient_country_code = 'USA'

 		    <cfif session.set_aside_code is not 0>

				 <cfif session.set_aside_code is 1>
				  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
				 <cfelse>
				  and (type_of_set_aside_code =  '#session.set_aside_code#')
				 </cfif>

				</cfif>

				group by primary_place_of_performance_city_name

				order by total DESC

			  </cfquery>

          </cfif>

 	  </table>

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['City', 'Awards'],
        <cfoutput query="agencies">
        ['#city#', #total#],

        </cfoutput>
      ]);

      var options = {
        title: 'Top 25 Awardees',
        chartArea: {width: '95%'},
        legend: 'none',
        height: 400,
        hAxis: {
          textStyle: {color: 'black', fontSize: 10},
          format: 'currency',
          title: '',
          minValue: 0
        },
        vAxis: {
          title: 'Award Value'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_1'));

      chart.draw(data, options);
    }

    </script>

  <div id="chart_div_1"></div>

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['City', 'Vendors','Agencies'],
        <cfoutput query="agencies">
        ['#city#', #vendors#, #agencies#],

        </cfoutput>
      ]);

      var options = {
        title: 'Vendors by Agencies',
        chartArea: {width: '95%'},
        legend: 'bottom',
        height: 400,
        hAxis: {
          textStyle: {color: 'black', fontSize: 10},
          format: 'currency',
          title: '',
          minValue: 0
        },
        vAxis: {
          title: 'Vendors & Agencies'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_2'));

      chart.draw(data, options);
    }

    </script>

  <div id="chart_div_2"></div>







	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>