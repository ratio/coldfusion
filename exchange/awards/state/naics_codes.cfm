	<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  		   <cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from state
			where state_abbr = '#state_abbr#'
		   </cfquery>

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>
           &nbsp;:&nbsp;<a href="/exchange/awards/state/">By State</a>
           &nbsp;:&nbsp;<a href="city.cfm?state_abbr=#state_abbr#">#state.state_name#</a>
           &nbsp;:&nbsp;#city#

          </cfoutput>

           </b></td>

  		   <cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from set_aside
			order by set_aside_name
		   </cfquery>

           <td align=right class="feed_option">

                      <form action="/exchange/awards/set.cfm" method="post">

				       <select name="selected_set_aside_code" style="width:200px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>All AWARDS
				       <option value=1 <cfif #session.set_aside_code# is 1>selected</cfif>>All SET ASIDES
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>&nbsp;-&nbsp;#set_aside_name#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

          <cfoutput>


	                   From:&nbsp;<input type="text" name="from" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'mm/dd/yyyy')#"</cfif>>
		               To:&nbsp;<input type="text" name="to" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_to,'mm/dd/yyyy')#"</cfif>>
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="state_4">
                       <input type="hidden" name="state_abbr" value=#state_abbr#>
                       <input type="hidden" name="city" value="#city#">
                       </form>

          </td></tr>

          </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select naics_code, naics_description, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_sub_agency_code)) as agencies, sum(federal_action_obligation) as total from award_data
			where recipient_state_code = '#state_abbr#'
			  and recipient_city_name = '#city#'
 		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>


			group by naics_code, naics_description

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by naics_code ASC
		    <cfelseif #sv# is 10>
		     order by naics_code DESC
		    <cfelseif #sv# is 2>
		     order by naics_description ASC
		    <cfelseif #sv# is 20>
		     order by naics_description DESC
		    <cfelseif #sv# is 3>
		     order by agencies DESC
		    <cfelseif #sv# is 30>
		     order by agencies ASC
		    <cfelseif #sv# is 4>
		     order by vendors DESC
		    <cfelseif #sv# is 40>
		     order by vendors ASC
		    <cfelseif #sv# is 5>
		     order by total DESC
		    <cfelseif #sv# is 50>
		     order by total ASC
		    </cfif>

		   <cfelse>
		     order by total DESC
		   </cfif>

		  </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

          <tr>
              <td class="feed_option" colspan=2><b>By NAICS Code</b></td>
          </tr>
          <tr><td height=5></td></tr>


          <tr>

             <td class="feed_option" width=100><a href="naics_codes.cfm?state_abbr=#state_abbr#&city=#city#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>NAICS Code</b></a></td>
             <td class="feed_option"><a href="naics_codes.cfm?state_abbr=#state_abbr#&city=#city#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Description</b></a></td>
             <td class="feed_option" align=center><a href="naics_codes.cfm?state_abbr=#state_abbr#&city=#city#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Agencies</b></a></td>
             <td class="feed_option" align=center><a href="naics_codes.cfm?state_abbr=#state_abbr#&city=#city#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Vendors</b></a></td>
             <td class="feed_option" align=right width=100><a href="naics_codes.cfm?state_abbr=#state_abbr#&city=#city#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Total</b></a></td>

          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="feed_option">#naics_code#</a></td>
               <td class="feed_option">#naics_description#</a></td>
               <td class="feed_option" align=center width=75>#numberformat(agencies,'999,999')#</td>
               <td class="feed_option" align=center width=75>#numberformat(vendors,'999,999')#</td>
               <td class="feed_option" align=right>#numberformat(total,'$999,999,999,999')#</td>
             </tr>

           <cfset tot = tot + total>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=5><hr></td></tr>
          <tr><td class="feed_option" colspan=4><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>
          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>