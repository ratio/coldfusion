<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.award_to") or not isdefined("session.award_from")>
 <cfset #session.award_to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.award_from# = #dateformat(dateadd("d",-730,now()),'yyyy-mm-dd')#>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header">Federal Awards - By State</b></td>
               <td align=right></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header" align=center>Please select a State to continue...</td></tr>
           <tr><td height=20></td></tr>
          </table>

		  <center>
		   <iframe src="https://createaclickablemap.com/map.php?id=81840&maplocation=&online=true" width="90%" height="600" style="border: none;"></iframe>
		   <script>if (window.addEventListener){ window.addEventListener("message", function(event) { if(event.data.length >= 22) { if( event.data.substr(0, 22) == "__MM-LOCATION.REDIRECT") location = event.data.substr(22); } }, false); } else if (window.attachEvent){ window.attachEvent("message", function(event) { if( event.data.length >= 22) { if ( event.data.substr(0, 22) == "__MM-LOCATION.REDIRECT") location = event.data.substr(22); } }, false); } </script>
		  </center>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td height=30>&nbsp;</td></tr>
          </table>

       </td></tr>

    </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>