<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  		   <cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from state
			where state_abbr = '#state_abbr#'
		   </cfquery>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><a href="/exchange/awards/">Federal Awards</a>&nbsp;:&nbsp;<a href="/exchange/awards/state/">By State</a></b></td>

  		   <cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select * from set_aside
			order by set_aside_name
		   </cfquery>

           <td align=right class="feed_option">

                      <form action="/exchange/awards/set.cfm" method="post">

				       <select name="selected_set_aside_code" style="width:200px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>All AWARDS
				       <option value=1 <cfif #session.set_aside_code# is 1>selected</cfif>>All SET ASIDES
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>&nbsp;-&nbsp;#set_aside_name#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

          <cfoutput>


	                   From:&nbsp;<input type="text" name="from" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'mm/dd/yyyy')#"</cfif>>
		               To:&nbsp;<input type="text" name="to" size=8 maxlength=10 <cfif isdefined("session.award_from")>value="#dateformat(session.award_to,'mm/dd/yyyy')#"</cfif>>
		               <input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="submit" name="button" value="Refresh">
                       <input type="hidden" name="location" value="state_5">
                       <input type="hidden" name="state_abbr" value=#state_abbr#>
                       </form>

          </td></tr>

          <tr><td class="feed_option"><b>#state.state_name#</td><td align=right class="feed_option"><a href="city.cfm?state_abbr=<cfoutput>#state_abbr#</cfoutput>">Return</a></tr>
          <tr><td height=10></td></tr>

          </cfoutput>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="cities" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select top(10) recipient_city_name, count(distinct(naics_code)) as naics_codes, count(recipient_duns) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_name)) as vendors, sum(federal_action_obligation) as total from award_data
			where recipient_state_code = '#state_abbr#'
			  and recipient_country_code = 'USA'
 		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>
			group by recipient_city_name
			order by total DESC
		  </cfquery>

    <tr><td class="feed_header" valign=top>
        <b>Total Value By City</b>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Total Value by City'],
			  <cfoutput query="cities">
			   ['#recipient_city_name#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			legend: { position: "none" },
			fontSize: 9,
			};

			var chart = new google.visualization.BarChart(document.getElementById('piechart'));

			chart.draw(data, options);
		  }
		</script>

    <div id="piechart" style="width: 500px; height: 300px;"></div>

    </td><td class="feed_header" valign=top>
           <b>NAICS Codes & Vendors</b>

  		   <cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select top(10) recipient_city_name, count(distinct(naics_code)) as naics_codes, count(recipient_duns) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_name)) as vendors, sum(federal_action_obligation) as total from award_data
			where recipient_state_code = '#state_abbr#'
			  and recipient_country_code = 'USA'
 		    and ((action_date >= '#session.award_from#') and (action_date <= '#session.award_to#'))

 		    <cfif session.set_aside_code is not 0>

             <cfif session.set_aside_code is 1>
              and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
             <cfelse>
              and (type_of_set_aside_code =  '#session.set_aside_code#')
             </cfif>

 		    </cfif>
			group by recipient_city_name
			order by vendors DESC
		  </cfquery>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'NAICS Codes','Vendors'],
			  <cfoutput query="vendors">
			   ['#recipient_city_name#', #naics_codes#,#vendors#],
			  </cfoutput>
			]);

			var options = {
			legend: { position: "none" },
			fontSize: 9,
			};

			var chart = new google.visualization.BarChart(document.getElementById('graph2'));

			chart.draw(data, options);
		  }
		</script>

    <div id="graph2" style="width: 500px; height: 300px;"></div>

     </td></tr>

     <tr><td valign=top class="feed_header">

Buying Patterns - Products and Services

<cfquery name="psc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select top(15) product_or_service_code, product_or_service_code_description, sum(federal_action_obligation) as total from award_data
			where recipient_state_code = '#state_abbr#'
			  and recipient_country_code = 'USA'
			  and federal_action_obligation > 0

<cfif session.set_aside_code is not 0>

 <cfif session.set_aside_code is 1>
  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
 <cfelse>
  and (type_of_set_aside_code =  '#session.set_aside_code#')
 </cfif>

</cfif>
group by product_or_service_code, product_or_service_code_description
order by total DESC
</cfquery>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="psc">
			   ['#product_or_service_code_description#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			'legend'   : 'left',
			'title'    : '',
			'pieHole'  : 0.4,
			'fontSize' : 8,
			'height'   : 300
			};

        var chart = new google.visualization.PieChart(document.getElementById('set_1_graph'));
        chart.draw(data, options);

		  }
		</script>

    <div id="set_1_graph"></div>
     </td><td valign=top class="feed_header">

Buying Patterns - Federal NAICS Codes

<cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select top(10) naics_code, naics_description, sum(federal_action_obligation) as total from award_data
			where recipient_state_code = '#state_abbr#'
			  and recipient_country_code = 'USA'
			  and federal_action_obligation > 0

<cfif session.set_aside_code is not 0>

 <cfif session.set_aside_code is 1>
  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
 <cfelse>
  and (type_of_set_aside_code =  '#session.set_aside_code#')
 </cfif>

</cfif>
group by naics_code, naics_description
order by total DESC
</cfquery>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load('current', {'packages':['corechart']});
		  google.charts.setOnLoadCallback(drawChart);

		  function drawChart() {

			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Award Value'],
			  <cfoutput query="naics">
			   ['#naics_description#',#round(total)#],
			  </cfoutput>
			]);

			var options = {
			legend: { position: "none" },
			fontSize: 7,
			};

			var chart = new google.visualization.ColumnChart(document.getElementById('naics_1'));

			chart.draw(data, options);
		  }
		</script>

    <div id="naics_1" style="width: 500px; height: 500px;"></div>

     </td></tr>



































  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>