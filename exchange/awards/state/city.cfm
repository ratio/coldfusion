<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif not isdefined("session.location_type")>
 <cfset #session.location_type# = 1>
</cfif>

<cfif not isdefined("session.set_aside_code")>
 <cfset session.set_aside_code = 0>
</cfif>

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 where state_abbr = '#state_abbr#'
</cfquery>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfset #session.dashboard# = 0>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top valign=top>

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top>Federal Awards - By State</td>
               <td align=right valign=top class="feed_sub_header"><a href="/exchange/awards/state/">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header" valign=top><cfoutput>#ucase(state.state_name)#</cfoutput></td>
               </tr>
           <tr><td height=5></td></tr>
           </table>


		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <form action="/exchange/awards/set.cfm" method="post">

		  <tr>

           <td class="feed_sub_header" valign=middle width=50>Filter<td>
           <td class="feed_option" valign=middle align=right>

                       <cfoutput>
                       <input type="text" name="keyword" class="input_text" style="width: 175px;" <cfif isdefined("session.search_kw")> value="#session.search_kw#"</cfif> placeholder="keyword">&nbsp;&nbsp;

	                   <b>From:</b>&nbsp;&nbsp;<input type="date" class="input_date" name="from" style="width: 160px;" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
		               <b>To:</b>&nbsp;&nbsp;<input type="date" class="input_date" name="to" style="width: 160px;" required <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>


                       </cfoutput>

				       <select name="location_type" class="input_select" style="width: 175px;">
				        <option value=1 <cfif #session.location_type# is 1>selected</cfif>>Contractor Location
				        <option value=2 <cfif #session.location_type# is 2>selected</cfif>>Place of Performance
                       </select>

				       <select name="selected_set_aside_code" class="input_select" style="width: 150px;">
				       <option value=0 <cfif #session.set_aside_code# is 0>selected</cfif>>NO SETASIDE
					   <cfoutput query="set_aside">
					    <option value="#set_aside_code#" <cfif #session.set_aside_code# is #set_aside_code#>selected</cfif>>#set_aside_name#
					   </cfoutput>
					   </select>&nbsp;&nbsp;

		               <cfoutput>

		               <input class="button_blue" type="submit" name="button" value="Go">
                       <input type="hidden" name="location" value="state_1">
                       <input type="hidden" name="state_abbr" value=#state_abbr#>

                       </cfoutput>

          </td></tr>

          <tr><td colspan=4><hr></td></tr>
          <tr><td height=5></td></tr>

          </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td align=right class="feed_sub_header">
              <cfoutput>
              <a href="/exchange/awards/state/city_dashboard.cfm?state_abbr=#state_abbr#&state_type=#session.location_type#"><img src="/images/icon_dashboard.png" width=20 hspace=10 border=0 alt="Dashboard" title="Dashboard"></a>
              <a href="/exchange/awards/state/city_dashboard.cfm?state_abbr=#state_abbr#&state_type=#session.location_type#">Dashboard</a>
              <a href="/exchange/awards/state/city.cfm?state_abbr=#state_abbr#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 hspace=10 border=0 alt="Export to Excel" title="Export to Excel"></a>
              <a href="/exchange/awards/state/city.cfm?state_abbr=#state_abbr#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
              </cfoutput>

           </td>

          </tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif session.location_type is 1>
			   <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select sum(federal_action_obligation) as total from award_data
				where recipient_state_code = '#state_abbr#' and
                      action_date between '#session.award_from#' and '#session.award_to#'

				<cfif session.set_aside_code is not 0>
					 <cfif session.set_aside_code is 1>
					  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
					 <cfelse>
					  and (type_of_set_aside_code =  '#session.set_aside_code#')
					 </cfif>
				</cfif>

                <cfif isdefined("session.search_kw") and trim(session.search_kw) is not "">
	             and contains((*),'"#trim(session.search_kw)#"')
                </cfif>

			   </cfquery>

			   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select count(distinct(award_id_piid)) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as total, recipient_city_name as city from award_data
				where recipient_state_code = '#state_abbr#' and
				      action_date between '#session.award_from#' and '#session.award_to#'

				<cfif session.set_aside_code is not 0>

					 <cfif session.set_aside_code is 1>
					  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
					 <cfelse>
					  and (type_of_set_aside_code =  '#session.set_aside_code#')
					 </cfif>

				</cfif>

                <cfif isdefined("session.search_kw") and trim(session.search_kw) is not "">
	             and contains((*),'"#trim(session.search_kw)#"')
                </cfif>

					group by recipient_city_name

				   <cfif isdefined("sv")>

					<cfif #sv# is 1>
					 order by recipient_city_name DESC
					<cfelseif #sv# is 10>
					 order by recipient_city_name ASC
					<cfelseif #sv# is 2>
					 order by awards DESC
					<cfelseif #sv# is 20>
					 order by awards ASC
					<cfelseif #sv# is 3>
					 order by vendors DESC
					<cfelseif #sv# is 30>
					 order by vendors ASC
					<cfelseif #sv# is 4>
					 order by total DESC
					<cfelseif #sv# is 40>
					 order by total ASC
					<cfelseif #sv# is 5>
					 order by agencies DESC
					<cfelseif #sv# is 50>
					 order by agencies ASC
					<cfelseif #sv# is 6>
					 order by naics_codes DESC
					<cfelseif #sv# is 60>
					 order by naics_codes ASC

					</cfif>

				   <cfelse>
					 order by total DESC
				   </cfif>

				  </cfquery>

          <cfelse>

			   <cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select sum(federal_action_obligation) as total from award_data
				where primary_place_of_performance_state_code = '#state_abbr#' and
				      action_date between '#session.award_from#' and '#session.award_to#'

				<cfif session.set_aside_code is not 0>

					 <cfif session.set_aside_code is 1>
					  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
					 <cfelse>
					  and (type_of_set_aside_code =  '#session.set_aside_code#')
					 </cfif>

				</cfif>

                <cfif isdefined("session.search_kw") and trim(session.search_kw) is not "">
	             and contains((*),'"#trim(session.search_kw)#"')
                </cfif>

			   </cfquery>

			   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select count(id) as awards, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as total, primary_place_of_performance_city_name as city from award_data
				where primary_place_of_performance_state_code = '#state_abbr#' and
				      action_date between '#session.award_from#' and '#session.award_to#'

				<cfif session.set_aside_code is not 0>
					 <cfif session.set_aside_code is 1>
					  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
					 <cfelse>
					  and (type_of_set_aside_code =  '#session.set_aside_code#')
					 </cfif>
				</cfif>

                <cfif isdefined("session.search_kw") and trim(session.search_kw) is not "">
	             and contains((*),'"#trim(session.search_kw)#"')
                </cfif>

					group by primary_place_of_performance_city_name

				   <cfif isdefined("sv")>

					<cfif #sv# is 1>
					 order by primary_place_of_performance_city_name DESC
					<cfelseif #sv# is 10>
					 order by primary_place_of_performance_city_name ASC
					<cfelseif #sv# is 2>
					 order by awards DESC
					<cfelseif #sv# is 20>
					 order by awards ASC
					<cfelseif #sv# is 3>
					 order by vendors DESC
					<cfelseif #sv# is 30>
					 order by vendors ASC
					<cfelseif #sv# is 4>
					 order by total DESC
					<cfelseif #sv# is 40>
					 order by total ASC
					<cfelseif #sv# is 5>
					 order by agencies DESC
					<cfelseif #sv# is 50>
					 order by agencies ASC
					<cfelseif #sv# is 6>
					 order by naics_codes DESC
					<cfelseif #sv# is 60>
					 order by naics_codes ASC

					</cfif>

				   <cfelse>
					 order by total DESC
				   </cfif>

				  </cfquery>
          </cfif>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
            </cfif>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

          <tr height=40>

             <td class="feed_option"><a href="city.cfm?state_abbr=#state_abbr#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>CITY</b></a></td>
             <td class="feed_option" align=center><a href="city.cfm?state_abbr=#state_abbr#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARDS</b></a></td>
             <td class="feed_option" align=center><a href="city.cfm?state_abbr=#state_abbr#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AGENCIES</b></a></td>
             <td class="feed_option" align=center><a href="city.cfm?state_abbr=#state_abbr#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>VENDORS</b></a></td>
             <td class="feed_option" align=right width=100><a href="city.cfm?state_abbr=#state_abbr#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AMOUNT</b></a></td>
             <td class="feed_option" align=right width=100><b>SHARE</b></td>

          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="feed_sub_header"><a href="vendors.cfm?state_abbr=#state_abbr#&city=#city#"><b>#ucase(city)#</b></a></td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(awards,'9,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(agencies,'999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(vendors,'999,999')#</td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(total,'$999,999,999,999')#</td>

               <cfif agencies.total GT 0>
               <cfif evaluate(agencies.total/total.total) LT 0>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>0.00%</td>
               <cfelse>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(evaluate((agencies.total/total.total)*100),'99.999')#%</td>
               </cfif>
               <cfelse>
	               <td class="feed_sub_header" style="font-weight: normal;" align=right>0.00%</td>
               </cfif>

             </tr>

           <cfset tot = tot + total>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=6><hr></td></tr>
          <tr><td class="feed_option" colspan=4><b>Total:</b></td>
              <td class="feed_option" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>
          <tr><td>&nbsp;</td></tr>

		  </table>

        </td></tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>