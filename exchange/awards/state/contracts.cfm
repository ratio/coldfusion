<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title><link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 where state_abbr = '#state_abbr#'
</cfquery>

<cfquery name="set_aside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfquery name="vendor" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select top(1) * from award_data
 where recipient_duns = '#duns#'
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top>Federal Awards - By State</td>
               <td align=right valign=top class="feed_sub_header"><a href="/exchange/awards/state/vendors.cfm?state_abbr=<cfoutput>#state_abbr#&city=#city#</cfoutput>">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header" valign=top><cfoutput>#ucase(city)#, #ucase(state.state_name)# - <a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer"><u>#ucase(vendor.recipient_name)#</u></a></td></tr>
           <tr><td height=5></td></tr>
           </table>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <form action="/exchange/awards/set.cfm" method="post">

           <tr><td class="feed_sub_header" valign=top><b>Filter Options</b></td>

           <td align=right class="feed_option" valign=top>

			   <b>Keyword:</b>&nbsp;
			   <input type="text" class="input_text" name="filter" size=20 <cfif isdefined("session.search_kw")> value="#session.search_kw#"</cfif>>&nbsp;

			   &nbsp;&nbsp;<b>From:</b>&nbsp;&nbsp;<input type="date" name="from" class="input_date" style="width: 160px;" required <cfif isdefined("session.award_from")>value="#dateformat(session.award_from,'yyyy-mm-dd')#"</cfif>>
			   &nbsp;&nbsp;<b>To:</b>&nbsp;&nbsp;<input type="date" name="to" class="input_date" style="width: 160px;" <cfif isdefined("session.award_to")>value="#dateformat(session.award_to,'yyyy-mm-dd')#"</cfif>>

			   <input class="button_blue" type="submit" name="button" value="Refresh">
			   <input type="hidden" name="location" value="state_3">
			   <input type="hidden" name="state_abbr" value="#state_abbr#">
			   <input type="hidden" name="city" value="#city#">
			   <input type="hidden" name="recipient_name" value="#recipient_name#">
			   <input type="hidden" name="duns" value="#duns#">

			   </cfoutput>

          </td>
          </tr>

          </form>

          <tr><td colspan=2><hr></td></tr>

          </table>
          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select action_date, recipient_name, award_id_piid, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, award_description, product_or_service_code_description, naics_code, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_potential_end_date, federal_action_obligation, id from award_data

	        where action_date between '#session.award_from#' and '#session.award_to#'
			and recipient_duns = '#duns#' and

           <cfif session.location_type is 1>
		      recipient_state_code = '#state_abbr#' and
			  recipient_city_name = '#city#'
		   <cfelse>
		      primary_place_of_performance_state_code = '#state_abbr#' and
		      primary_place_of_performance_city_name = '#city#'
		   </cfif>

				<cfif session.set_aside_code is not 0>
					 <cfif session.set_aside_code is 1>
					  and (type_of_set_aside_code is not null and type_of_set_aside_code <> 'NONE')
					 <cfelse>
					  and (type_of_set_aside_code =  '#session.set_aside_code#')
					 </cfif>
				</cfif>

                <cfif isdefined("session.search_kw") and trim(session.search_kw) is not "">
	             and contains((*),'"#trim(session.search_kw)#"')
                </cfif>


		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by action_date ASC
		    <cfelseif #sv# is 10>
             order by action_date DESC
		    <cfelseif #sv# is 2>
             order by award_id_piid ASC
		    <cfelseif #sv# is 20>
             order by award_id_piid DESC
		    <cfelseif #sv# is 3>
             order by awarding_agency_name ASC
		    <cfelseif #sv# is 30>
             order by awarding_agency_name DESC
		    <cfelseif #sv# is 4>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 5>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 50>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 6>
             order by product_or_service_code_description ASC
		    <cfelseif #sv# is 60>
             order by product_or_service_code_description DESC
		    <cfelseif #sv# is 7>
             order by naics_code ASC
		    <cfelseif #sv# is 70>
             order by naics_code DESC
		    <cfelseif #sv# is 8>
             order by type_of_contract_pricing ASC
		    <cfelseif #sv# is 80>
             order by type_of_contract_pricing DESC
		    <cfelseif #sv# is 9>
             order by type_of_set_aside ASC
		    <cfelseif #sv# is 90>
             order by type_of_set_aside DESC
		    <cfelseif #sv# is 10>
             order by period_of_performance_start_date DESC
		    <cfelseif #sv# is 100>
             order by period_of_performance_start_date ASC
		    <cfelseif #sv# is 11>
             order by period_of_performance_potential_end_date DESC
		    <cfelseif #sv# is 110>
             order by period_of_performance_potential_end_date ASC
		    <cfelseif #sv# is 12>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 120>
             order by federal_action_obligation ASC
		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		  </cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
            </cfif>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>


          <tr><td colspan=5 class="feed_sub_header">VENDOR CONTACTS</td></tr>

          <tr><td colspan=13>

  		   <cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  		    select * from sams
  		    where duns = '#duns#'
  		   </cfquery>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				    <tr><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Primary Point of Contact</b></td></tr>
						<tr><td class="feed_option">#sams.poc_fnme# #sams.poc_lname#<br>
						                            <cfif #sams.poc_title# is not "">#sams.poc_title#<br></cfif>
						                            (#left(sams.poc_us_phone,3)#) #mid(sams.poc_us_phone,4,3)#-#right(sams.poc_us_phone,4)#<br>
						                            <a href="mailto:#sams.poc_email#">#sams.poc_email#</a></td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Alternate Point of Contact</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.alt_poc_lname# is not "">#sams.alt_poc_fname# #sams.alt_poc_lname#<br><cfelse>Not Provided</cfif>
						                            <cfif #sams.alt_poc_title# is not "">#sams.alt_poc_title#<br></cfif>
						                            <cfif #sams.akt_poc_phone# is not "">(#left(sams.akt_poc_phone,3)#) #mid(sams.akt_poc_phone,4,3)#-#right(sams.akt_poc_phone,4)#<br></cfif>
						                            <cfif #sams.alt_poc_email# is not ""><a href="mailto:#sams.alt_poc_email#">#sams.alt_poc_email#</a></cfif></td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Past Performance Point of Contact</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.pp_poc_lname# is not "">#sams.pp_poc_fname# #sams.pp_poc_lname#<br><cfelse>Not Provided</cfif>
						                            <cfif #sams.pp_poc_title# is not "">#sams.pp_poc_title#<br></cfif>
						                            <cfif #sams.pp_poc_phone# is not "">(#left(sams.pp_poc_phone,3)#) #mid(sams.pp_poc_phone,4,3)#-#right(sams.pp_poc_phone,4)#<br></cfif>
						                            <cfif #sams.pp_poc_email# is not ""><a href="mailto:#sams.pp_poc_email#">#sams.pp_poc_email#</a></cfif></td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_option"><b>Electronic Point of Contact</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.elec_bus_poc_lnmae# is not "">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#<br><cfelse>Not Provided</cfif>
						                            <cfif #sams.elec_bus_poc_title# is not "">#sams.elec_bus_poc_title#<br></cfif>
						                            <cfif #sams.elec_bus_poc_us_phone# is not "">(#left(sams.elec_bus_poc_us_phone,3)#) #mid(sams.elec_bus_poc_us_phone,4,3)#-#right(sams.elec_bus_poc_us_phone,4)#<br></cfif>
						                            <cfif #sams.elec_bus_poc_email# is not ""><a href="mailto:#sams.elec_bus_poc_email#">#sams.elec_bus_poc_email#</a></cfif></td></tr>
						</table>

                    </td></tr>

                   <tr><td height=10></td></tr>
                   <tr><td colspan=4><hr></td></tr>
                   <tr><td height=10></td></tr>
                   </table>

                   </td></tr>

           <tr><td colspan=15 align=right class="feed_sub_header">
              <cfoutput>
              <a href="contracts.cfm?state_abbr=#state_abbr#&city=#city#&duns=#duns#&recipient_name=#recipient_name#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" hspace=10 width=20 border=0 alt="Export to Excel" title="Export to Excel"></a>
              <a href="contracts.cfm?state_abbr=#state_abbr#&city=#city#&duns=#duns#&recipient_name=#recipient_name#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>

              </cfoutput>

              </td></tr>

          <tr height=40>
             <td class="text_xsmall" width=75><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Action Date</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract ##</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Department</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Office</b></a></td>
             <td class="text_xsmall"><b>Award Description</b></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Product or Service</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>NAICS</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>Pricing</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>Set Aside</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PoP Start</b></a></td>
             <td class="text_xsmall"><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PoP End</b></a></td>
             <td class="text_xsmall" align=right><a href="contracts.cfm?recipient_name=#recipient_name#&state_abbr=#state_abbr#&city=#city#&duns=#duns#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Obligation</b></a></td>
          </tr>

          </cfoutput>

          <cfoutput query="agencies">

            <cfif counter is 0>
            <tr bgcolor="ffffff" height=40>
           <cfelse>
            <tr bgcolor="e0e0e0" height=40>

           </cfif>

               <td class="text_xsmall" valign=top>#dateformat(action_date,'mm/dd/yy')#</td>
               <td class="text_xsmall" valign=top width=125><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
               <td class="text_xsmall" valign=top>#awarding_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>

               <td class="text_xsmall" valign=top width=400>

               <cfif isdefined("session.filter_keyword")>
               #replaceNoCase(award_description,session.filter_keyword,"<span style='background:yellow'>#ucase(session.filter_keyword)#</span>","all")#
               <cfelse>
               #award_description#
               </cfif>

               </td>

               <td class="text_xsmall" valign=top>

               <cfif isdefined("session.filter_keyword")>
               #replaceNoCase(product_or_service_code_description,session.filter_keyword,"<span style='background:yellow'>#ucase(session.filter_keyword)#</span>","all")#
               <cfelse>
               #product_or_service_code_description#
               </cfif>

               </td>

               <td class="text_xsmall" valign=top>#naics_code#</td>
               <td class="text_xsmall" valign=top>#left(type_of_contract_pricing,21)#</td>
               <td class="text_xsmall" valign=top>#type_of_set_aside#</td>
               <td class="text_xsmall" valign=top width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=75>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             </tr>

          <cfset tot = tot + #federal_action_obligation#>
           <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          </cfoutput>

          <cfoutput>
          <tr><td colspan=13><hr></td></tr>
          <tr><td class="text_xsmall" colspan=12><b>Total:</b></td>
              <td class="text_xsmall" align=right><b>#numberformat(tot,'$999,999,999,999')#</b></td></tr>
          </cfoutput>


          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>