<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from department
  order by department_name
</cfquery>

<cfquery name="states" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="setaside" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from set_aside
 order by set_aside_name
</cfquery>

<cfif not isdefined("session.award_to") or not isdefined("session.award_from")>
 <cfset #session.award_to# = #dateformat(now(),'yyyy-mm-dd')#>
 <cfset #session.award_from# = #dateformat(dateadd("d",-730,now()),'yyyy-mm-dd')#>
</cfif>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header"><img src="/images/icon_fed.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;Search Federal Awards</td>
              <td align=right></td></tr>
          <tr><td><hr></td></tr>
         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("s")>
            <tr><td colspan=3 class="feed_sub_header"><font color="red">Search dates cannot exceed 5 years.</font></td></tr>
          <cfelse>
            <tr><td height=10></td></tr>

          </cfif>

          <form name="myForm" action="/exchange/awards/search_set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

           <tr>

                   <td class="feed_option"><b>Keyword</b></td>
                   <td class="feed_option"><input type="text" name="search_keyword" class="input_text" placeholder="i.e., sensors and clinical" style="width: 235px;" <cfif isdefined("session.award_keyword")>value="<cfoutput>#session.award_keyword#</cfoutput>"</cfif>></td>

                   <td class="feed_option"><b>NAICS *</b></td>
                   <td class="feed_option"><input type="text" name="naics" class="input_text" style="width: 150px;" maxlength="299" placeholder="All Codes" <cfif isdefined("session.award_naics")>value="<cfoutput>#session.award_naics#</cfoutput>"</cfif>>

                   &nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="NAICS Code Lookup" title="NAICS Code Lookup" style="cursor: pointer;" onclick="window.open('/exchange/opps/naics_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">

               </td>

                   <td class="feed_option"><b>From</b></td>
                   <td class="feed_option"><input type="date" class="input_date" style="width: 160px;" name="from" required value="<cfoutput>#dateformat(session.award_from,'yyyy-mm-dd')#</cfoutput>" required>

                   <td class="feed_option"><b>State</b></td>
                   <td class="feed_option">
                   <select name="state" class="input_select" style="width: 175px;">
                    <option value=0>All States
                    <cfoutput query="states">
                     <option value="#state_abbr#" <cfif isdefined("session.award_state") and session.award_state is #state_abbr#>selected</cfif>>#ucase(state_name)#
                    </cfoutput>
                   </select>
               </td>

             </tr>

           <tr>

                   <td class="feed_option"><b>Department</b></td>
                   <td class="feed_option">
                   <select name="dept" class="input_select" style="width: 235px;">
                   <option value=0>All Departments
                    <cfoutput query="dept">
                    <option value="#department_code#" <cfif isdefined("session.award_dept") and session.award_dept is "#department_code#">selected</cfif>>#department_name#
                    </cfoutput>
                   </select>
               </td>



                   <td class="feed_option"><b>PSC *</b></td>
                   <td class="feed_option"><input type="text" name="psc" class="input_text" style="width:150px;" maxlength="299" placeholder="All Codes" <cfif isdefined("session.award_psc")>value="<cfoutput>#session.award_psc#</cfoutput>"</cfif>>

                   &nbsp;&nbsp;<img src="/images/icon_search.png" height=18 alt="PSC Lookup" title="PSC Lookup" style="cursor: pointer;" onclick="window.open('/exchange/opps/psc_lookup.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">


               </td>


                   <td class="feed_option"><b>To</b></td>
                   <td class="feed_option"><input type="date" class="input_date" name="to" style="width: 160px;" required value="<cfoutput>#dateformat(session.award_to,'yyyy-mm-dd')#</cfoutput>" required>



                   <td class="feed_option"><b>Set Aside</b></td>
                   <td class="feed_option">
                                     <select name="setaside" class="input_select" style="width: 175px;">
 				                     <option value=0>No Preference
 				                     <cfoutput query="setaside">
 				                     <option value="#set_aside_code#" <cfif isdefined("session.award_setaside") and session.award_setaside is '#set_aside_code#'>selected</cfif>>#set_aside_name#
 				                     </cfoutput>
                   </select>

                &nbsp;&nbsp;&nbsp;<input class="button_blue" onclick="

                if (myForm.search_keyword.value == '' && myForm.naics.value == '') {
                    alert('You must enter either a NAICS code or keyword.');
                    return false;}


                " type="submit" name="button" value="Search">

                </td></tr>

         <tr><td height=5></td></tr>
         <tr><td colspan=14 class="link_small_gray">* - To search multiple values, seperate NAICS or PSC codes with a commma and no spaces.  For a complete code list, click here - <a href="https://www.census.gov/eos/www/naics/" target="_blank" rel="noopener" rel="noreferrer"><b>NAICS Code</b></a> list, or <a href="https://psctool.us/" target="_blank" rel="noopener" rel="noreferrer"><b>Product or Service Code (PSC)</b></a> list.</td></tr>

         </form>

          </table>
