<cfquery name="updates" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from exchange_update
 left join eupdate on eupdate_update_id = exchange_update_id
 where eupdate_usr_id is null
       and exchange_update_active = 1 and
       <cfif isdefined("session.hub")>
        exchange_update_hub_id = #session.hub#
       <cfelse>
        exchange_update_hub_id is null
       </cfif>
 order by exchange_update_order ASC
</cfquery>

<cfif isdefined("session.hub")>
<cfquery name="hi" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>
</cfif>

<cfloop query="updates">

<cfset mdiff = #datediff("n",updates.exchange_update_date,now())#>

<div class="feed_box">

<cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td height=10></td></tr>
	 <tr><td class="feed_option" width=100 valign=middle>
	     <cfif isdefined("session.hub")>
			<cfif #hi.hub_logo# is "">
			  <img src="/images/no_logo.png" width=60 border=0>
			<cfelse>
			  <img src="#media_virtual#/#hi.hub_logo#" width=100 border=0>
			</cfif>
	     <cfelse>
	       <img src="/images/exchange_logo_black.png" border=0 width=80>
	     </cfif>
	     </td>
	     <td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_title"><a href="/exchange/update_detail.cfm?update_id=#updates.exchange_update_id#">#updates.exchange_update_name#</a></td>
               <td align=right><a href="/exchange/remove_update.cfm?update_id=#updates.exchange_update_id#" onclick="return confirm('Clear Update?\r\nAre you sure you want to remove this update?');"><img src="/images/icon_trash.png" height=20 valign=middle alt="Remove" title="Remove"></a></td></tr>
           <tr><td class="feed_option"><b><cfif isdefined("session.hub")>#hi.hub_name#<cfelse>Exchange</cfif></b></td></tr>
           <tr><td class="text_xsmall">

			 <font size=2 color="gray">

					   <cfif #mdiff# LT 1>
						 < minute ago
					   <cfelseif #mdiff# GTE 1 and #mdiff# LT 2>
						#mdiff# minute ago
					   <cfelseif #mdiff# GTE 2 and #mdiff# LT 60>
						#mdiff# minutes ago
					   <cfelseif #mdiff# GT 60>
						<cfif #mdiff# LT 1440>
						 #round(evaluate(mdiff/60))# hours ago
						<cfelse>
						 <cfif #round(evaluate(mdiff/1440))# GT 1>
						   #round(evaluate(mdiff/1440))# days ago
						 <cfelse>
						   #round(evaluate(mdiff/1440))# days ago
						 </cfif>
						</cfif>
					   </cfif>
			 </b>
			 </font>
	     </td></tr>

         </table>

         </td></tr>

     <tr><td height=10></td></tr>

	 <tr><td colspan=2 class="feed_option">#(replace(updates.exchange_update_desc_short,"#chr(10)#","<br>","all"))#</td></tr>

	  <tr><td height=10></td></tr>
	  <cfif #updates.exchange_update_url# is not "">
	  	<tr><td colspan=2 class="link_small_gray">More information...  <a href="#updates.exchange_update_url#" target="_blank" rel="noopener" rel="noreferrer"><u>#updates.exchange_update_url#</u></a></td></tr>
	  </cfif>

	  <cfif #updates.exchange_update_attachment# is not "">
        <tr><td height=5></td></tr>
	  	<tr><td colspan=2 class="link_small_gray"><a href="#media_virtual#/#updates.exchange_update_attachment#" target="_blank" rel="noopener" rel="noreferrer"><u>Download Attachment</u></a></td></tr>
	  </cfif>

	  <tr><td height=5></td></tr>


  </table>

 </cfoutput>

</div>

</cfloop>