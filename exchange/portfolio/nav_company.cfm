<!--- Portfolios --->

<div class="right_box">

	<center>

		  <!--- Company Portfolios --->

		  <cfquery name="c_portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select portfolio_image, portfolio_usr_id, portfolio_id, portfolio_name from portfolio
			 where portfolio_type_id = 1 and
				   ((portfolio_usr_id = #session.usr_id#) or (portfolio_company_id = #session.company_id# and portfolio_access_id = 2))
			 order by portfolio_name
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td colspan=3 class="feed_header">COMPANY PORTFOLIOS</td></tr>
          <tr><td><hr></td></tr>
          </table>

          <cfset count = 1>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfif c_portfolios.recordcount is 0>

		  <tr><td class="feed_option">You have not created any company portfolios.</td></tr>

		  <cfelse>

		  <tr><td height=5></td></tr>

		  <cfloop query = "c_portfolios">

		  <cfquery name="item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(portfolio_item_id) as total from portfolio_item
		   where portfolio_item_portfolio_id = #c_portfolios.portfolio_id#
		  </cfquery>

		  <cfoutput>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#c_portfolios.portfolio_id#">

	                   <cfif #c_portfolios.portfolio_image# is "">
				        <img src="/images/icon_portfolio_company.png" width=18 border=0 alt="Company Portfolio" title="Company Porfolio">
				       <cfelse>
		                <img src="#media_virtual#/#c_portfolios.portfolio_image#" width=18 alt="Company Portfolio" title="Company Porfolio">
				       </cfif>

					   </a></td>
					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#c_portfolios.portfolio_id#"><cfif len(c_portfolios.portfolio_name) GT 22>#left(c_portfolios.portfolio_name,'22')#...<cfelse>#c_portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
					</tr>

			  <cfif count is not c_portfolios.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

	    </cfoutput>

		  </td></tr>

		  </cfloop>

		</cfif>

	</center>

	</table>

</div>

