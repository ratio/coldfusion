<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Update">

    <cfif #portfolio_item_schedule_start_date# GTE #portfolio_item_schedule_end_date#>
     <cflocation URL="activity_opp_edit.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&activity_id=#portfolio_item_schedule_id#&u=1" addtoken="no">
    </cfif>

    <cftransaction>

		<cfquery name="port_update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update portfolio
		 set portfolio_updated = #now()#
		 where portfolio_id = #portfolio_id# and
			   portfolio_usr_id = #session.usr_id#
		</cfquery>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 update portfolio_item_schedule
		 set portfolio_item_schedule_name = '#portfolio_item_schedule_name#',
			 portfolio_item_schedule_desc = '#portfolio_item_schedule_desc#',
			 portfolio_item_schedule_start_date = '#portfolio_item_schedule_start_date#',
			 portfolio_item_schedule_end_date = '#portfolio_item_schedule_end_date#',
			 portfolio_item_schedule_updated = #now()#
		 where portfolio_item_schedule_id = #portfolio_item_schedule_id# and
			   portfolio_item_schedule_usr_id = #session.usr_id#
		</cfquery>

 	</cftransaction>

<script>
    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload();
    }
</script>

    <cflocation URL="manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&u=2" addtoken="no">

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="port_update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio
	 set portfolio_updated = #now()#
	 where portfolio_id = #portfolio_id# and
		   portfolio_usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete portfolio_item_schedule
	 where portfolio_item_schedule_id = #portfolio_item_schedule_id# and
	       portfolio_item_schedule_usr_id = #session.usr_id#
	</cfquery>

</cftransaction>

<script>
    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload();
    }
</script>

    <cflocation URL="manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&u=1" addtoken="no">

<cfelseif button is "Save Activity">

    <cfif #portfolio_item_schedule_start_date# GTE #portfolio_item_schedule_end_date#>
     <cflocation URL="activity_opp_add.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&u=1" addtoken="no">
    </cfif>

<cftransaction>

	<cfquery name="port_update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update portfolio
	 set portfolio_updated = #now()#
	 where portfolio_id = #portfolio_id# and
		   portfolio_usr_id = #session.usr_id#
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into portfolio_item_schedule
	 (
	 portfolio_item_schedule_name,
	 portfolio_item_schedule_desc,
	 portfolio_item_schedule_start_date,
	 portfolio_item_schedule_end_date,
	 portfolio_item_schedule_created,
	 portfolio_item_schedule_updated,
	 portfolio_item_schedule_usr_id,
	 portfolio_item_schedule_portfolio_id,
	 portfolio_item_schedule_item_id
	 )
	 values
	 (
	 '#portfolio_item_schedule_name#',
	 '#portfolio_item_schedule_desc#',
	 '#portfolio_item_schedule_start_date#',
	 '#portfolio_item_schedule_end_date#',
	  #now()#,
	  #now()#,
	  #session.usr_id#,
	  #portfolio_id#,
	  #portfolio_item_id#
	  )
	</cfquery>

</cftransaction>

<script>
    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload();
    }
</script>

    <cflocation URL="manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&u=1" addtoken="no">

</cfif>
