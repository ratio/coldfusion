<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="schedule" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item_schedule
 where portfolio_item_schedule_id = #activity_id# and
       portfolio_item_schedule_usr_id = #session.usr_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfif #session.portfolio_location# is "Home">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

       <cfelseif #session.portfolio_location# is "Navigator">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/nav_company.cfm">
		   <cfinclude template="/exchange/portfolio/nav_award.cfm">

       </cfif>

      <td valign=top>


      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">EDIT ACTIVITY</td>
                 <td class="feed_option" align=right>

                                  <a href="/exchange/portfolio/manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
		     <tr><td colspan=2><hr></td></tr>
		  </table>

      </cfoutput>

        <form action="activity_opp_db.cfm" method="post">

		<table cellspacing=0 cellpadding=0 border=0 width=95%>

        <cfif isdefined("u")>
         <tr><td colspan=2 class="feed_sub_header"><font color="red"><b>Error.  The start date cannot be greater or equal to the end date</b></font></td></tr>
        </cfif>

		<cfoutput>

           <tr><td class="feed_sub_header"><b>Activity Name</b></td></tr>
           <tr><td class="feed_option"><input class="input_text" type="text" name="portfolio_item_schedule_name" size=50 required value="#schedule.portfolio_item_schedule_name#"></td></tr>

           <tr><td class="feed_sub_header"><b>Description</b></td></tr>
           <tr><td class="feed_option"><textarea class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  name="portfolio_item_schedule_desc" cols=80 rows=5>#schedule.portfolio_item_schedule_desc#</textarea></td></tr>

           <tr><td class="feed_sub_header"><b>Start Date</b></td></tr>
           <tr><td class="feed_option"><input class="input_date" type="date" min="2000-01-01" max="2100-12-31" name="portfolio_item_schedule_start_date" required value="#schedule.portfolio_item_schedule_start_date#"></td></tr>

           <tr><td class="feed_sub_headaer"><b>End Date</b></td></tr>
           <tr><td class="feed_option"><input class="input_date" type="date" min="2000-01-01" max="2100-12-31" name="portfolio_item_schedule_end_date" required value="#schedule.portfolio_item_schedule_end_date#"></td></tr>

           <tr><td height=10></td></tr>
           <tr><td><input type="submit" name="button" class="button_blue_large" value="Update">
           &nbsp;&nbsp;
           <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Activity?\r\nAre you sure you want to delete this activity?');">

           <input type="hidden" name="portfolio_item_schedule_id" value=#activity_id#>
           <input type="hidden" name="portfolio_item_id" value=#portfolio_item_id#>
           <input type="hidden" name="portfolio_id" value=#portfolio_id#>

        </cfoutput>

		</table>

		</form>

		</td></tr>

      </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>