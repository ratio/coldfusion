<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.portfolio_view")>
<cfset session.portfolio_view = 0>
</cfif>

<cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(portfolio_id) as total from portfolio
 where portfolio_hub_id = #session.hub#
</cfquery>

<cfif session.portfolio_view is 0>

	<cfquery name="port_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_portfolio_id from sharing
	 where sharing_hub_id = #session.hub# and
	       sharing_portfolio_id is not null and
		   (sharing_to_usr_id = #session.usr_id#)
	</cfquery>

	<cfif port_access.recordcount is 0>
	 <cfset port_list = 0>
	<cfelse>
	 <cfset port_list = valuelist(port_access.sharing_portfolio_id)>
	</cfif>

    <cfquery name="myportfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select portfolio_usr_id, portfolio_image, portfolio_type_id, portfolio_access_id, portfolio_id, portfolio_name, usr_first_name, usr_last_name, portfolio_updated from portfolio
	  left join usr on usr_id = portfolio_usr_id
	  where portfolio_hub_id = #session.hub# and
		    (portfolio_id in (#port_list#) or portfolio_usr_id = #session.usr_id# or portfolio_company_access = 1)
	  order by portfolio_updated DESC
    </cfquery>

<cfelseif session.portfolio_view is 1>

    <cfquery name="myportfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select portfolio_usr_id, portfolio_image, portfolio_type_id, portfolio_access_id, portfolio_id, portfolio_name, usr_first_name, usr_last_name, portfolio_updated from portfolio
	  left join usr on usr_id = portfolio_usr_id
	  where portfolio_hub_id = #session.hub# and
		    portfolio_usr_id = #session.usr_id#
	  order by portfolio_updated DESC
    </cfquery>

<cfelseif session.portfolio_view is 2>

	<cfquery name="port_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_portfolio_id from sharing
	 where sharing_hub_id = #session.hub# and
	       sharing_portfolio_id is not null and
		   (sharing_to_usr_id = #session.usr_id#)
	</cfquery>

	<cfif port_access.recordcount is 0>
	 <cfset port_list = 0>
	<cfelse>
	 <cfset port_list = valuelist(port_access.sharing_portfolio_id)>
	</cfif>

    <cfquery name="myportfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select portfolio_usr_id, portfolio_image, portfolio_type_id, portfolio_access_id, portfolio_id, portfolio_name, usr_first_name, usr_last_name, portfolio_updated from portfolio
	  left join usr on usr_id = portfolio_usr_id
	  where portfolio_hub_id = #session.hub# and
		    (portfolio_id in (#port_list#) or portfolio_usr_id = #session.usr_id# or portfolio_company_access = 1) and
		    portfolio_usr_id <> #session.usr_id#
	  order by portfolio_updated DESC
    </cfquery>



</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset session.portfolio_location = "Home">

<style>
.portfolio_badge_block {
    width: 47%;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 410px;
    padding-top: 20px;
    padding-bottom: 30px;
    padding-left: 20px;
    padding-right: 20px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 8px;
    background-color: #ffffff;
}
</style>

  <cfinclude template="/exchange/include/header.cfm">


      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">
       <cfinclude template="recent.cfm">

       </td><td valign=top>

       <cfinclude template="port_header.cfm">

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <form action="refresh_portfolios.cfm" method="post">
        <cfoutput>
        <tr><td class="feed_header" valign=bottom>Company Portfolio Snapshots <cfif #myportfolio.recordcount# GT 0>(#myportfolio.recordcount#)</cfif></td>
        </cfoutput>
            <td align=right><span class="feed_sub_header">Filter Portfolios</span>&nbsp;&nbsp;

            <select name="portfolio_view" class="input_select" onchange="form.submit()">
              <option value=0 <cfif #session.portfolio_view# is 0>selected</cfif>>All Portfolios
              <option value=1 <cfif #session.portfolio_view# is 1>selected</cfif>>My Portfolios
              <option value=2 <cfif #session.portfolio_view# is 2>selected</cfif>>Shared with Me
            </select>

            </td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><hr></td></tr>
        </form>
       </table>

       <center>

       <cfif #myportfolio.recordcount# is 0>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header" style="font-weight: normal;">No Portfolio's have been created.  To create a Portfolio, <a href="/exchange/portfolio/create_portfolio.cfm"><b>click here</b></a>.</td></tr>
		   </table>

       <cfelse>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td height=20></td></tr>
		   </table>

			<cfloop query="myportfolio">
				<div class="portfolio_badge_block">
					<cfinclude template="portfolio_badge.cfm">
				</div>
			</cfloop>

            <cfif myportfolio.recordcount mod 2 is 0>
            <cfelse>
				<div style="width: 47%; display: inline-block;">


                </div>
            </cfif>


	    </cfif>

	   </center>

      </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>