<cfoutput>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header">

		<cfif #port_info.portfolio_image# is "">
		  <cfif port_info.portfolio_type_id is 1>
			<img src="/images/icon_portfolio_company.png" height=30>
		  <cfelseif port_info.portfolio_type_id is 2>
			<img src="/images/icon_portfolio_award.png" height=30>
		  <cfelseif port_info.portfolio_type_id is 3>
			<img src="/images/icon_portfolio_opp.png" height=30>
		  </cfif>
		<cfelse>
		  <img src="#media_virtual#/#port_info.portfolio_image#" height=30>
		</cfif>

     &nbsp;&nbsp;<a href="portfolio_company.cfm">#port_info.portfolio_name#</a></td>
	 <td class="feed_sub_header" align=right>

    	&nbsp;&nbsp;<a href="/exchange/portfolio/portfolio_dashboard.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/icon_dashboard.png" width=20 alt="Portfolio Dashboard" title="Portfolio Dashboard" hspace=10></a><a href="/exchange/portfolio/portfolio_dashboard.cfm" onclick="javascript:document.getElementById('page-loader').style.display='block';">Federal Award Dashboard</a>

       <cfif port_info.portfolio_type_id is 1>
    	 &nbsp;&nbsp;<a href="/exchange/portfolio/portfolio_contacts.cfm"><img src="/images/group.png" width=20 alt="Portfolio Contacts" title="Portfolio Contacts" hspace=10></a><a href="/exchange/portfolio/portfolio_contacts.cfm">Portfolio Contacts</a>
       </cfif>

       <cfif session.portfolio_access_level GTE 2>
    	 <a href="portfolio_sharing.cfm"><img src="/images/icon_sharing.png" width=20 alt="Share Portfolio" title="Share Portfolio" hspace=10></a><a href="/exchange/portfolio/portfolio_sharing.cfm">Share Portfolio</a>
         <a href="/exchange/portfolio/edit_portfolio.cfm"><img src="/images/icon_edit.png" width=20 alt="Edit Portfolio" title="Edit Portfolio" hspace=10></a><a href="/exchange/portfolio/edit_portfolio.cfm">Edit Portfolio</a>
       </cfif>

	 </td></tr>

     <cfif #port_info.portfolio_desc# is not "">
      <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top colspan=2>#port_info.portfolio_desc#</td></tr>
     </cfif>

     <tr><td colspan=2><hr></td></tr>

     <cfif isdefined("u")>
      <tr><td class="feed_sub_header" style="color: green;">Company Portfolio has been successfully updated.</td></tr>
     </cfif>

</table>
</cfoutput>