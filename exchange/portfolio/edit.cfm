<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_usr_id = #session.usr_id# and
       portfolio_id = #portfolio_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">Edit Portfolio</td>
                 <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option" colspan=2>Portfolios allow you to select and group EXCHANGE awards, opportunities, grants, and opportunities.</td></tr>
             <tr><td height=10></td></tr>
          </table>

          <cfoutput>

          <form action="db.cfm" method="post">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_option"><b>Portfolio Name</b></td></tr>
             <tr><td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  name="portfolio_name" size=60 value="#info.portfolio_name#"></td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option"><b>Description</b></td></tr>
             <tr><td class="feed_option"><textarea name="portfolio_desc" rows=5 cols=60>#info.portfolio_desc#</textarea></td></tr>
             <tr><td height=5></td></tr>
             <tr><td class="feed_option"><b>Sharing</b></td></tr>
             <tr><td class="feed_option">
             <select name="portfolio_access_id">
              <option value=1 <cfif #info.portfolio_access_id# is 1>selected</cfif>>Private (me only)
              <option value=2 <cfif #info.portfolio_access_id# is 2>selected</cfif>>Me, and Company Users (view)
             </td></tr>
             <tr><td height=10></td></tr>
             <tr><td>
             <input type="submit" name="button" class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" value="Update">
             &nbsp;&nbsp;<input type="submit" name="button" class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" value="Delete" onclick="return confirm('Are you sure you want to delete this portfolio?');">

          </table>

          <input type="hidden" name="portfolio_id" value=#portfolio_id#>

          </form>

          </cfoutput>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>