<cfinclude template="/exchange/security/check.cfm">

 <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  update sharing
  set sharing_access = #a#
  where sharing_hub_id = #session.hub# and
        sharing_portfolio_id = #session.portfolio_id# and
        sharing_to_usr_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
 </cfquery>

<cflocation URL="portfolio_sharing.cfm?u=3" addtoken="no">