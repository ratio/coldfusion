<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("share_id")>

 <cftransaction>

 <cfloop index="d" list="#share_id#">

  <cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select sharing_id from sharing
   where sharing_hub_id = #session.hub# and
		 sharing_portfolio_id = #session.portfolio_id# and
		 sharing_to_usr_id = #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#
  </cfquery>

  <cfif check.recordcount is 0>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into sharing
	 (
	  sharing_hub_id,
	  sharing_portfolio_id,
	  sharing_from_usr_id,
	  sharing_to_usr_id,
	  sharing_access
	 )
	 values
	 (
	 #session.hub#,
	 #session.portfolio_id#,
	 #session.usr_id#,
	 #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	 1
	 )
	</cfquery>

	</cfif>

  </cfloop>

 </cftransaction>

 	<cflocation URL="portfolio_sharing.cfm?u=4" addtoken="no">

<cfelse>

	<cflocation URL="portfolio_sharing.cfm?u=5" addtoken="no">

</cfif>

