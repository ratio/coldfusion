<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save Portfolio">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select portfolio_image from portfolio
		  where portfolio_id = #session.portfolio_id# and
		        portfolio_usr_id = #session.usr_id#
		</cfquery>

		<cfif fileexists("#media_path#\#remove.portfolio_image#")>
		 <cffile action = "delete" file = "#media_path#\#remove.portfolio_image#">
		</cfif>

	</cfif>

	<cfif #portfolio_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select portfolio_image from portfolio
		  where portfolio_id = #session.portfolio_id#
		</cfquery>

		<cfif #getfile.portfolio_image# is not "">

		 <cfif fileexists("#media_path#\#getfile.portfolio_image#")>
		   <cffile action = "delete" file = "#media_path#\#getfile.portfolio_image#">
		 </cfif>

		</cfif>

		<cffile action = "upload"
		 fileField = "portfolio_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      update portfolio
      set portfolio_name = '#portfolio_name#',
		  portfolio_desc = '#portfolio_desc#',
		  portfolio_keywords = '#portfolio_keywords#',

		  <cfif #portfolio_image# is not "">
		   portfolio_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   portfolio_image = null,
		  </cfif>

		  portfolio_goals = '#portfolio_goals#',
		  portfolio_updated = #now()#
      where portfolio_id = #session.portfolio_id# and
            portfolio_usr_id = #session.usr_id#
	</cfquery>

	<cflocation URL="/exchange/portfolio/portfolio_company.cfm?u=1" addtoken="no">

<cfelseif #button# is "Create Portfolio">

	<cfif #portfolio_image# is not "">
		<cffile action = "upload"
		 fileField = "portfolio_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into portfolio
	 (
      portfolio_name,
      portfolio_keywords,
      portfolio_image,
      portfolio_desc,
      portfolio_goals,
      portfolio_usr_id,
      portfolio_company_id,
      portfolio_hub_id,
      portfolio_updated,
      portfolio_type_id
      )
      values
      (
      '#portfolio_name#',
      '#portfolio_keywords#',
       <cfif #portfolio_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
      '#portfolio_desc#',
      '#portfolio_goals#',
       #session.usr_id#,
       <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
       <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
       #now()#,
       1
      )
	</cfquery>

	<cflocation URL="/exchange/portfolio/index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Delete Portfolio">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select portfolio_image from portfolio
	  where portfolio_id = #session.portfolio_id# and
	        portfolio_usr_id = #session.usr_id#
	</cfquery>

    <cfif remove.portfolio_image is not "">

     <cfif fileexists("#media_path#\#remove.portfolio_image#")>
	 	<cffile action = "delete" file = "#media_path#\#remove.portfolio_image#">
	 </cfif>

	</cfif>

	<cftransaction>

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete portfolio
		 where portfolio_id = #session.portfolio_id# and
			   portfolio_usr_id = #session.usr_id#
		</cfquery>

		<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete portfolio_item
		 where portfolio_item_portfolio_id = #session.portfolio_id#
		</cfquery>

		<cfquery name="delete3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete portfolio_item_schedule
		 where portfolio_item_schedule_portfolio_id = #session.portfolio_id#
		</cfquery>

		<cfquery name="delete4" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete sharing
		 where sharing_portfolio_id = #session.portfolio_id#
		</cfquery>

		<cfquery name="delete5" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 delete recent
		 where recent_portfolio_id = #session.portfolio_id#
		</cfquery>

	</cftransaction>

	<cflocation URL="/exchange/portfolio/index.cfm?u=3" addtoken="no">

</cfif>
