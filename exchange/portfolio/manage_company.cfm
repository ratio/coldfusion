<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="get_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select portfolio_item_id from portfolio_item
 where portfolio_item_portfolio_id = #session.portfolio_id# and
       portfolio_item_company_id = #comp_id#
</cfquery>

<cfquery name="company_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #comp_id#
</cfquery>

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item
 where portfolio_item_id = #get_info.portfolio_item_id#
</cfquery>

<cfquery name="port_schedule" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item_schedule
 where portfolio_item_schedule_item_id = #get_info.portfolio_item_id# and
       portfolio_item_schedule_usr_id = #session.usr_id#
 order by portfolio_item_schedule_end_date ASC
</cfquery>

<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns = '#port_info.portfolio_item_duns#'
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="recent.cfm">

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">

			<cfif company_info.company_logo is "">
			  <img src="//logo.clearbit.com/#company_info.company_website#" height=50 onerror="this.src='/images/no_logo.png'" border=0>
			<cfelse>
			  <img src="#media_virtual#/#company_info.company_logo#" height=50 border=0>
			</cfif>
			</a>&nbsp;&nbsp;

            <cfif sams.recordcount is 0>
            <a href="/exchange/include/company_profile.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#company_info.company_name#</a>
            <cfelse>
            <a href="/exchange/include/federal_profile.cfm?duns=#sams.duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">#company_info.company_name#</a>
            </cfif>

            </td>

                 <td class="feed_sub_header" align=right class="feed_sub_header">

				 <a href="/exchange/include/save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#company_info.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><img src="/images/plus3.png" width=15 border=0 hspace=5></a>
				 <a href="/exchange/include/save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#company_info.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;">Add to Model</a>
				 &nbsp;|&nbsp;

                 <a href="/exchange/include/company_profile.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><img src="/images/icon_company4.png" height=20 hspace=10 border=0 alt="Company Profile" title="Company Profile"></a>

                 <a href="/exchange/include/company_profile.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">Company Profile</a>
                 &nbsp;|&nbsp;

                 <cfif session.portfolio_access_level GT 1>

                 <a href="remove_comp.cfm?company_id=#company_info.company_id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this company from your portfolio?');"><img src="/images/delete.png" height=20 hspace=5 border=0 alt="Remove Company" title="Remove Company"></a>
                 <a href="remove_comp.cfm?company_id=#company_info.company_id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this company from your portfolio?');">Remove Company</a>

                 &nbsp;|&nbsp;

                 </cfif>


                 <a href="/exchange/portfolio/portfolio_company.cfm">Return</a>

                 </td></tr>
             <tr><td colspan=2><hr></td></tr>

           <cfif isdefined("u")>
            <tr><td height=5></td></tr>
            <cfif u is 1>
             <tr><td class="feed_sub_header"><font color="green"><b>Update successful.</b></font></td></tr>
            </cfif>
           </cfif>
          </table>

       </cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

          <cfoutput>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td>

				       <cfif sams.recordcount is 0>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
                             	<tr><td class="feed_sub_header">Company Information</td></tr>
								<tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #company_info.company_website# is not ""><b>Website: </b><a href="#company_info.company_website#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>#company_info.company_website#</u></a><br></cfif>
								                            <b>Location: </b>#company_info.company_city# #company_info.company_state#, #company_info.company_zip#

					            </td></tr>
					            <tr><td height=10></td></tr>
							</table>

				       <cfelse>

						<cfquery name="certs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						 select * from sba
						 where duns = '#company_info.company_duns#'
						</cfquery>

						<tr><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							<tr><td class="feed_option"><b>Corporate Information</b></td></tr>
							<tr><td class="feed_option"><b>Website: </b>

							<cfoutput>
							<cfif left("#sams.corp_url#",4) is "http" or left("#sams.corp_url#",5) is "https">
							  <a href="#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>#sams.corp_url#</u></a>
							<cfelse>
							 <a href="http://#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>#sams.corp_url#</u></a>
							</cfif>
							</cfoutput><br>

														<b>DUNS: </b>#sams.duns#<cfif #sams.duns_4# is not "">-#sams.duns_4#</cfif><br>
														<b>State of Incorporation: </b>#sams.state_of_inc#<br>
														<b>Cage Code: </b>#sams.cage_code#

														</td></tr>


							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							<tr><td class="feed_option"><b>Address</b></td></tr>
							<tr><td class="feed_option"><cfif #sams.phys_address_l1# is not "">#sams.phys_address_l1#<br></cfif>
														<cfif #sams.phys_address_line_2# is not "">#sams.phys_address_line_2#<br></cfif>
														#sams.city_1# #sams.state_1#, #sams.zip_1#<br>
														</td></tr>
							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							<tr><td class="feed_option"><b>Important Dates</b></td></tr>
							<tr><td class="feed_option">
														<b>Founded: </b>#mid(sams.biz_start_date,5,2)#/#right(sams.biz_start_date,2)#/#left(sams.biz_start_date,4)#<br>
														<b>Registration Date: </b>#mid(sams.init_reg_date,5,2)#/#right(sams.init_reg_date,2)#/#left(sams.init_reg_date,4)#<br>
														<b>Activation Date: </b>#mid(sams.activation_date,5,2)#/#right(sams.activation_date,2)#/#left(sams.activation_date,4)#<br>
														<b>Expiration Date: </b>#mid(sams.reg_exp_date,5,2)#/#right(sams.reg_exp_date,2)#/#left(sams.reg_exp_date,4)#<br>
														<b>Last Updated: </b>#mid(sams.last_update,5,2)#/#right(sams.last_update,2)#/#left(sams.last_update,4)#<br>

														 <cfif certs.eight_start is not "">
														 <b>8(a) Certification:</b> #dateformat(certs.eight_start,'mm/dd/yyyy')# - #dateformat(certs.eight_end,'mm/dd/yyyy')#<br>
														 </cfif>

														 <cfif certs.hub_start is not "">
														 <b>HubZone Certification:</b> #dateformat(certs.hub_start,'mm/dd/yyyy')# - #dateformat(certs.hub_end,'mm/dd/yyyy')#<br>
														 </cfif>

														 <cfif certs.sdb_start is not "">
														 <b>SDB Certification: </b> #dateformat(certs.sdb_start,'mm/dd/yyyy')# - #dateformat(certs.sdb_end,'mm/dd/yyyy')#<br>
														 </cfif>

														</td></tr>

							</table>

						</td><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>

					   <tr>
					       <td class="feed_option"><b>Business Type</b></td>

					   <tr><td class="feed_option" valign=top>

                       <cfif sams.biz_type_string is "">
						   Unknown
                       <cfelse>

						   <cfloop index="element" list="#listsort(sams.biz_type_string,'Text','ASC','~')#" delimiters="~">

							<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
							 select biz_code_name from biz_code
							 where biz_code = '#element#'
							</cfquery>

							<cfoutput>
							#code.biz_code_name#<br>
							</cfoutput>
						   </cfloop>

					   </cfif>

						   </td>
						   <td class="feed_option" valign=top></td></tr>


							</table>

						</td></tr>

                       </cfif>

					   <tr><td colspan=4><hr></td></tr>

					   </table>

        </cfoutput>

		<cfquery name="annual" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				SELECT sum(federal_action_obligation) as total, year(action_date) as year1 from award_data
				where recipient_duns = '#company_info.company_duns#'
				group by year(action_date)
				order by year(action_date)
		</cfquery>

		<cfquery name="award_summary_1" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select count(id) as total, avg(CAST(annual_revenue AS numeric)) as revenue, avg(CAST(number_of_employees AS int)) as employees, datepart(yyyy, [action_date]) as [year], count(distinct(id)) as awards, count(distinct(award_id_piid)) as contracts, count(distinct(awarding_sub_agency_code)) as sub_agencies, count(distinct(awarding_agency_code)) as agencies, sum(base_and_all_options_value) as all_options, sum(base_and_exercised_options_value) as options, sum(federal_action_obligation) as federal_action_obligation from award_data
	       where recipient_duns = '#company_info.company_duns#'
           group by datepart(yyyy, [action_date])
           order by [year]
		</cfquery>

       <cfif award_summary_1.total GT 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
              <td class="feed_sub_header" align=center>Award Value</td>
              <td width=50>&nbsp;</td>
              <td class="feed_sub_header" align=center>Reported Revenue</td>
              <td width=50>&nbsp;</td>
              <td class="feed_sub_header" align=center>Award Diversity</td>
         </tr>

        <tr>

        <td valign=top width=33% align=center>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Awards'],
				<cfoutput query="annual">
				 ["#year1#", #round(total)#],
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 100, right: 0, width: '100%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Award Value',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual" style="width: 100%;"></div>

        </td><td width=30>&nbsp;</td><td valign=top width=33% align=center>

		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Revenue'],
				<cfoutput query="award_summary_1">
				<cfif revenue is "">
				 ["#year#", 0],
				<cfelse>
				 ["#year#", #round(revenue)#],
				</cfif>
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 100, right: 0, width: '100%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Revenue',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual_2'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual_2" style="width: 100%;"></div>

        </td><td width=30>&nbsp;</td><td valign=top width=33% align=center>

		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Agencies', 'Contracts','Awards'],
				<cfoutput query="award_summary_1">
				 ["#year#", #agencies#, #contracts#, #awards#],
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 80, right: 0, width: '100%'},
				legend: 'bottom',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Number',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual_3'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual_3" style="width: 100%;"></div>

          </td></tr>

        <tr><td colspan=5><hr></td></tr>

        </table>

      </cfif>

<!--- Summary Performance --->

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <cfoutput>

         <tr><td colspan=6 class="feed_sub_header">Summary Performance</td>
             <td align=right class="feed_sub_header" colspan=3>

             <cfif award_summary_1.total GT 0>

             <a href="/exchange/include/award_dashboard.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><img src="/images/icon_dashboard.png" hspace=10 height=20 border=0 alt="Dashboard" title="Dashboard"></a>
             <a href="/exchange/include/award_dashboard.cfm?id=#company_info.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer">Company Dashboard</a>

             </cfif>

             </td></tr>

      </cfoutput>

      <cfif award_summary_1.total GT 0>

        <cfset pcounter = 1>

         <tr>
              <td class="feed_option"><b>Year</b></td>
              <td class="feed_option" align=center><b>Departments</b></td>
              <td class="feed_option" align=center><b>Agencies</b></td>
              <td class="feed_option" align=center><b>Contracts</b></td>
              <td class="feed_option" align=center><b>Awards</b></td>
              <td class="feed_option" align=right><b>Obligated</b></td>
              <td class="feed_option" align=right><b>With Options</b></td>
              <td class="feed_option" align=center><b>Avg # of Employees</b></td>
              <td class="feed_option" align=right><b>Avg Revenue</b></td>
         </tr>

        <cfset counter = 0>
        <cfoutput query="award_summary_1">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="e0e0e0">
         </cfif>

             <td class="feed_option">#year#</td>
             <td class="feed_option" align=center>#agencies#</td>
             <td class="feed_option" align=center>#sub_agencies#</td>
             <td class="feed_option" align=center>#contracts#</td>
             <td class="feed_option" align=center>#awards#</td>
             <td class="feed_option" align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             <td class="feed_option" align=right>#numberformat(options,'$999,999,999,999')#</td>
             <td class="feed_option" align=center>#employees#</td>
             <td class="feed_option" align=right>#numberformat(revenue,'$999,999,999')#</td>

             <cfif counter is 0>
              <cfset counter = 1>
             <cfelse>
              <cfset counter = 0>
             </cfif>


         </tr>
        </cfoutput>

       <cfelse>

         <tr><td class="feed_sub_header" style="font-weight: normal;">No performance information could be found.</td></tr>

       </cfif>


      </table>

<!--- Comments --->

<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company_intel
 left join usr on usr_id = company_intel.company_intel_created_by_usr_id
 where company_intel_company_id = #comp_id# and
       company_intel_hub_id = #session.hub#
 order by company_intel_created_date DESC
</cfquery>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>

         <tr><td class="feed_sub_header">Company Comments</td>
             <td align=right class="feed_sub_header">

             <cfoutput>
             <img src="/images/plus3.png" width=15 hspace=10 border=0><a href="add_comment.cfm?comp_id=#comp_id#"></a>
             <a href="add_comment.cfm?comp_id=#comp_id#">Add Comment</a>

             </cfoutput>

             </td></tr>
         <tr><td height=10></td></tr>

      </table>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfif intel.recordcount is 0>
	<tr><td class="feed_sub_header" style="font-weight: normal;">No comments have been created.</td></tr>
	<cfelse>
	<tr><td height=10></td></tr>

	<cfset count = 1>

	<cfloop query="intel">

	<cfoutput>

	<tr><td valign=top width=110>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <cfif #intel.usr_photo# is "">
		  <tr><td align=center><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(intel.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" style="border-radius: 180px;" width=50 height=50 border=0 alt="#intel.usr_first_name#'s Profile" title="#intel.usr_first_name#'s Profile"></a></td></tr>
		 <cfelse>
		  <tr><td align=center><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(intel.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 180px;" src="#media_virtual#/#intel.usr_photo#" width=50 height=50 border=0 title="#intel.usr_first_name#'s Profile" alt="#intel.usr_first_name#'s Profile"></a></td></tr>
		 </cfif>
		 <tr><td  align=center class="feed_option"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(intel.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#usr_first_name# #usr_last_name#</b></a></td></tr>


	</table>

	</td><td width=30>&nbsp;</td><td valign=top>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>
		   <td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">

		   <b>#intel.company_intel_context#</b>

		   </td>
		   <td class="feed_sub_header" align=right style="padding-top: 5px;">

		   <cfif session.usr_id is intel.company_intel_created_by_usr_id>
            <a href="edit_comment.cfm?cid=#intel.company_intel_id#&comp_id=#comp_id#"><img src="/images/icon_edit.png" width=20 hspace=10 alt="Edit Comment" title="Edit Comment"></a>
           </cfif>


           <!---
			<cfif intel.comments_rating_value is 0 or intel.comments_rating_value is "">
			 <img src="/images/star_0.png" height=16>
			<cfelse>
			 <img src="/images/star_#round(intel.comments_rating_value)#.png" height=16>
			</cfif> --->

		   </td></tr>
	   <tr>
		   <td colspan=2 class="feed_sub_header" style="font-weight: normal;">

		   #replace(intel.company_intel_comments,"#chr(10)#","<br>","all")#

		   </td></tr>
	   </tr>

	   <tr><td class="link_small_gray">

		   <!---

		   <cfif #intel.company_intel_attachment# is not "">
			<a href="#media_virtual#/#intel.company_intel_attachment#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>Download Attachment</u></a>&nbsp;|&nbsp;
		   </cfif> --->

		   <cfif #intel.company_intel_url# is not "">
			Reference URL: <a href="#intel.company_intel_url#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>#intel.company_intel_url#</u></a>
		   </cfif>


		   </td>
		   <td align=right class="link_small_gray">#dateformat(intel.company_intel_created_date,'mmm dd, yyyy')# at #timeformat(intel.company_intel_created_date)#</td>
	   </tr>

	 </cfoutput>

	  <cfset count = count + 1>

	</table>

	</td></tr>

    <cfif count LTE intel.recordcount>
		<tr><td height=5></td></tr>
		<tr><td colspan=3><hr></td></tr>
		<tr><td height=5></td></tr>
	</cfif>

	</cfloop>

</cfif>

</table>

    </td></tr>

</table>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>