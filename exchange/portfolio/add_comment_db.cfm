<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Comment">

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert company_intel
	  (company_intel_context,
	   company_intel_portfolio_id,
	   company_intel_company_id,
	   company_intel_comments,
	   company_intel_url,
	   company_intel_created_date,
	   company_intel_hub_id,
	   company_intel_created_by_usr_id,
	   company_intel_created_by_company_id
       )
	  values
	  (
	   '#company_intel_context#',
	    #session.portfolio_id#,
	    #comp_id#,
	   '#company_intel_comments#',
	   '#company_intel_url#',
	    #now()#,
	   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	    #session.usr_id#,
	    <cfif session.company_id is 0 or not isdefined("session.company_id")>null<cfelse>#session.company_id#</cfif>
	   )
	</cfquery>

	<cflocation URL="manage_company.cfm?comp_id=#comp_id#&u=11" addtoken="no">

<cfelseif #button# is "Delete Comment">

	<cftransaction>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete company_intel
		  where (company_intel_id = #company_intel_id#) and
				(company_intel_created_by_usr_id = #session.usr_id#)
		</cfquery>

	</cftransaction>

	<cflocation URL="manage_company.cfm?comp_id=#comp_id#&u=13" addtoken="no">

<cfelseif #button# is "Save Comment">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update company_intel
	  set company_intel_context = '#company_intel_context#',

		  <cfif isdefined("remove_attachment")>
		   company_intel_attachment = null,
		  </cfif>

	      company_intel_comments = '#company_intel_comments#',
	      company_intel_url = '#company_intel_url#'

	  where (company_intel_id = #company_intel_id# ) and
	        (company_intel_created_by_usr_id = #session.usr_id#)
	</cfquery>

	<cflocation URL="manage_company.cfm?comp_id=#comp_id#&u=12" addtoken="no">

</cfif>