<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfif #session.portfolio_location# is "Home">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

       <cfelseif #session.portfolio_location# is "Navigator">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/nav_company.cfm">
		   <cfinclude template="/exchange/portfolio/nav_award.cfm">
           <cfinclude template="/exchange/portfolio/nav_opp.cfm">

       </cfif>

      </td><td valign=top>

      <cfif #session.portfolio_location# is "Home">
	      <cfinclude template="/exchange/portfolio/jump.cfm">
      </cfif>

		  <cfquery name="opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio_item
		   left join fbo on fbo_id = portfolio_item_opp_id
                  where portfolio_item_portfolio_id = #session.portfolio_id# and
                        portfolio_item_type_id = 3
		  </cfquery>

      <div class="main_box">

          <cfinclude template="/exchange/portfolio/portfolio_header.cfm">

		  <cfif opps.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	       <tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunities have been added to this portfolio.  To add Opportunities, visit <a href="/exchange/opps/"><b>Federal Opportunities</b></a> and select <img src="/images/icon_green_add.png" width=20 hspace=5 align=absmiddle>&nbsp;<u>Add to Portfolio</u> option when viewing Opportunity details.</td></tr>
          </table>

		  <cfelse>

      <cfoutput>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_sub_header" colspan=3>PORTFOLIO DETAIL <cfif #opps.recordcount# GT 0>( <cfoutput>#trim(opps.recordcount)#</cfoutput> Opportunities )</cfif></td>
		      <td align=right class="feed_option"><a href="/exchange/portfolio/portfolio_opp_timeline.cfm?portfolio_id=#session.portfolio_id#"><img src="/images/icon_gannt.png" height=20 border=0 alt="Schedule & Timeline" title="Schedule & Timeline"></a>
		      <a href="/exchange/portfolio/portfolio_opp_timeline.cfm?portfolio_id=#session.portfolio_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>Integrated Schedule & Timeline</b></a></td></tr>
		  <tr><td height=10></td></tr>
      </table>
      </cfoutput>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr height=30>
		     <td class="feed_option"><b>Name</b></td>
		     <td class="feed_option"><b>Title</b></td>
		     <td class="feed_option"><b>Department</b></td>
		     <td class="feed_option"><b>Agency</b></td>
		     <td class="feed_option"><b>Notice</b></td>
		     <td class="feed_option"><b>Set Aside</b></td>
		     <td class="feed_option"><b>NAICS</b></td>
		     <td>&nbsp;</td>
		  </tr>

		  <cfset counter = 0>

			  <cfoutput query="opps">

                <cfif counter is 0>
                 <tr bgcolor="ffffff" height=25>
                <cfelse>
                 <tr bgcolor="e0e0e0" height=25>
                </cfif>

                    <td class="text_xsmall" valign=middle><a href="/exchange/portfolio/manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#session.portfolio_id#"><b>#ucase(portfolio_item_name)#</b></a></td>
                    <td class="text_xsmall" valign=middle><a href="/exchange/portfolio/manage_opp.cfm?portfolio_item_id=#portfolio_item_id#&portfolio_id=#session.portfolio_id#">#fbo_opp_name#</a></td>
                    <td class="text_xsmall" valign=middle>#fbo_agency#</td>
                    <td class="text_xsmall" valign=middle>#fbo_notice_type#</td>
                    <td class="text_xsmall" valign=middle>#fbo_setaside_original#</td>
                    <td class="text_xsmall" valign=middle>#fbo_naics_code#</td>

                    <td class="text_xsmall" valign=middle align=right>

                    <cfif opps.portfolio_item_usr_id is #session.usr_id#>

                    &nbsp;&nbsp;&nbsp;&nbsp;<a href="remove_opp.cfm?id=#opps.portfolio_item_id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this Opportunity from your portfolio?');"><img src="/images/delete.png" width=12 border=0 alt="Remove" title="Remove">
                    <cfelse>
                    &nbsp;
                    </cfif>

                    </td>
                </tr>

                <cfif counter is 0>
                 <cfset counter = 1>
                <cfelse>
                 <cfset counter = 0>
                </cfif>

			  </cfoutput>

		    </table>

		  </cfif>

        </td></tr>

     </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>