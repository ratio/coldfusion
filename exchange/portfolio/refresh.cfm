<cfquery name="get" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select portfolio_id from portfolio
 where portfolio_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfset session.portfolio_id = #get.portfolio_id#>

<cfif not isdefined("session.portfolio_location")>
 <cfset session.portfolio_location = "Home">
</cfif>

<!--- Update Recent --->

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_portfolio_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#get.portfolio_id#,#session.usr_id#,#session.company_id#,#session.hub#,#now()#)
</cfquery>

<cfif #session.portfolio_id# is 0>
 <cflocation URL="/exchange/portfolio/" addtoken="no">
</cfif>

<cfquery name="go" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from portfolio
   where portfolio_id = #session.portfolio_id#
</cfquery>

<cfif go.portfolio_type_id is 1>
 <cfset session.portfolio_type = 1>
 <cflocation URL="/exchange/portfolio/portfolio_company.cfm" addtoken="no">
<cfelseif go.portfolio_type_id is 2>
 <cfset session.portfolio_type = 2>
 <cflocation URL="/exchange/portfolio/portfolio_award.cfm" addtoken="no">
<cfelseif go.portfolio_type_id is 3>
 <cfset session.portfolio_type = 3>
 <cflocation URL="/exchange/portfolio/portfolio_opp.cfm" addtoken="no">
</cfif>

