<cfinclude template="/exchange/security/check.cfm">

<cfif session.portfolio_access_level is not 3>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update portfolio
 set portfolio_company_access = #l#
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfif l is 1>
 <cflocation URL="portfolio_sharing.cfm?u=6" addtoken="no">
<cfelse>
 <cflocation URL="portfolio_sharing.cfm?u=7" addtoken="no">
</cfif>
