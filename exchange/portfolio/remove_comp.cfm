<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("session.portfolio_id")>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete portfolio_item
	 where portfolio_item_portfolio_id = #session.portfolio_id# and
		   portfolio_item_company_id = #company_id# and
		   portfolio_item_type_id = 1
	</cfquery>

</cfif>

<cflocation URL="portfolio_company.cfm?portfolio_id=#session.portfolio_id#" addtoken="no">