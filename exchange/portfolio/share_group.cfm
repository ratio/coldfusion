<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfquery name="existing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_group_id from sharing
 where sharing_portfolio_id = #session.portfolio_id# and
       sharing_group_id is not null and
       sharing_hub_id = #session.hub#
</cfquery>

<cfif existing.recordcount is 0>
 <cfset group_list = 0>
<cfelse>
 <cfset group_list = valuelist(existing.sharing_group_id)>
</cfif>

<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group
 where sharing_group_usr_id = #session.usr_id# and
       sharing_group_hub_id = #session.hub# and
       sharing_group_id not in (#group_list#)
 order by sharing_group_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">
        <cfoutput>
        #ucase(port_info.portfolio_name)#
        </cfoutput>

        </td>
            <td align=right class="feed_sub_header"><a href="/exchange/portfolio/portfolio_sharing_groups.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

        <cfif groups.recordcount is 0>
	       <table cellspacing=0 cellpadding=0 border=0 width=100%>
	         <tr><td class="feed_sub_header" style="font-weight: normal;">No Groups were found.<br><br>Note, Groups which have already been granted share access will not appear in search results.</td></tr>
	       </table>
        <cfelse>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
             <td colspan=4 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">
              Please select the Group, or Groups, that you'd like to share your Portfolio with.  By default, Groups will receive Read access when Shared.</td></tr>

          <tr>
             <td class="feed_sub_header">SELECT</td>
             <td></td>
             <td class="feed_sub_header">NAME</td>
             <td class="feed_sub_header">DESCRIPTION</td>
             <td class="feed_sub_header">USERS</td>
          </tr>

          <form action="share_update_group.cfm" method="post">

          <cfloop query="groups">

			<cfquery name="users" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from sharing_group_usr
			 join usr on usr_id = sharing_group_usr_usr_id
			 where sharing_group_usr_group_id = #groups.sharing_group_id#
			 order by usr_full_name
			</cfquery>

			<cfif users.recordcount is 0>
			 <cfset group_users = "No users have been added to this Group">
			<cfelse>
			 <cfset group_users = valuelist(users.usr_full_name)>
			</cfif>

             <cfoutput>

             <tr>
                 <td width=75>&nbsp;&nbsp;<input type="checkbox" class="input_text" name="share_id" value=#encrypt(groups.sharing_group_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;"></td>
                 <td width=40>&nbsp;&nbsp;</td>
                 <td width=200 valign=top class="feed_sub_header">#ucase(groups.sharing_group_name)#</td>
                 <td class="feed_sub_header" style="font-weight: normal;">#groups.sharing_group_desc#</td>
                 <td class="feed_sub_header" style="font-weight: normal;">#group_users#</td>
             </tr>
		     <tr><td colspan=5><hr></td></tr>

           </cfoutput>

           </cfloop>

           <tr><td height=10></td></tr>
           <tr>
              <td colspan=3><input type="submit" name="button" class="button_blue_large" value="Grant Sharing" onclick="return confirm('Grant Sharing?\r\nAre you sure you want to grant sharing access to the selected Groups?');"></td></tr>

           </form>

	       </table>

          </cfif>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>