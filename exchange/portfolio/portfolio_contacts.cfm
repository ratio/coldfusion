<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfif session.portfolio_access_level is 0>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfinclude template="/exchange/portfolio/check_access.cfm">

 <cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from portfolio
  where portfolio_id = #session.portfolio_id#
 </cfquery>

 <cfquery name="port_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select portfolio_item_company_id from portfolio_item
   where portfolio_item_portfolio_id = #session.portfolio_id# and
 		portfolio_item_type_id = 1
 </cfquery>

 <cfif port_list.recordcount is 0>
 	<cfset plist = 0>
 <cfelse>
 	<cfset plist = valuelist(port_list.portfolio_item_company_id)>
</cfif>

 <cfquery name="pinfo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from company
  where company_id in (#plist#)
  order by company_name
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

		   <cfinclude template="/exchange/components/my_profile/profile.cfm">
		   <cfinclude template="recent.cfm">

       </td><td valign=top>

       <div class="main_box">

		  <cfif plist is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfif session.portfolio_access_level is 1>
			  <tr><td class="feed_header">Portfolio Contacts</td>
			      <td class="feed_sub_header" align=right><a href="portfolio_company.cfm">Return</a></td></tr>
			  <tr><td colspan=2><hr></td></tr>


	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.</td></tr>
           <cfelse>
	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.<br><br>To add Companies, go to <a href="/exchange/marketplace/partners/network_in.cfm"><b>In Network</b></a> or <a href="/exchange/marketplace/partners/network_out.cfm"><b>Out of Network</b></a> Partners and choose the <u>Add to Portfolio</u> option when viewing the Company profile.</td></tr>
	       </cfif>
          </table>

		  <cfelse>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_header">Portfolio Contacts</td>
			      <td class="feed_sub_header" align=right><a href="portfolio_company.cfm">Return</a></td></tr>
			  <tr><td colspan=2><hr></td></tr>
  	      </table>

  	     <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=10></td></tr>

		 <tr>
			<td class="feed_sub_header" align=center>Company</td>
			<td></td>
			<td class="feed_sub_header">Points of Contact</td>
		 </tr>

		 <tr><td height=10></td></tr>

         <tr><td>

          <cfset row_counter = 1>
          <cfset email_list = "">

          <cfloop query="pinfo">

                <cfoutput>
                <tr>

					<td width=225 class="feed_sub_header" align=center>

	                <table cellspacing=0 cellpadding=0 border=0 width=100%>

	                <tr><td align=center>

						<cfif pinfo.company_logo is "">
						  <a href="/exchange/include/company_profile.cfm?id=#pinfo.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><img src="//logo.clearbit.com/#pinfo.company_website#" width=80 border=0 onerror="this.src='/images/no_logo.png'"></a>
						<cfelse>
						  <a href="/exchange/include/company_profile.cfm?id=#pinfo.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><img src="#media_virtual#/#pinfo.company_logo#" width=80 border=0></a>
						</cfif>

						</td></tr>

				    <tr><td height=10></td></tr>
				    <tr><td class="feed_sub_header" align=center><a href="/exchange/include/company_profile.cfm?id=#pinfo.company_id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>#ucase(pinfo.company_name)#</b></a></td></tr>
				    <tr><td height=10></td></tr>
				    <tr><td align=center>

					</td></tr>

					</table>

           </cfoutput>

		   </td><td width=50>&nbsp;</td><td valign=top>

		   <cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="100">
			 select * from sams
			 where duns = '#pinfo.company_duns#'
		   </cfquery>

		   <cfquery name="bah" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			  select * from company_contact
			  where company_contact_company_id = #pinfo.company_id# and
					company_contact_hub_id = #session.hub#
			  order by company_contact_name
		   </cfquery>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td height=10></td></tr>
			<tr>
				<td class="feed_sub_header" width=200><b>NAME</b></td>
				<td class="feed_sub_header" width=200><b>TITLE / ROLE</b></td>
				<td class="feed_sub_header" width=200><b>PHONE</b></td>
				<td class="feed_sub_header" width=200><b>EMAIL</b></td>
				<td></td>
			</tr>

			<tr><td colspan=5 class="feed_sub_header" bgcolor="e0e0e0">Exchange / Network Created Point of Contacts</td></tr>

            <cfif bah.recordcount is 0>
             <tr><td colspan=4 class="feed_sub_header" style="font-weight: normal;">No contacts were found.</td></tr>

            <cfelse>

             <cfoutput query="bah">
               <tr>
                  <td class="feed_sub_header" style="font-weight: normal;">#company_contact_name#</td>
                  <td class="feed_sub_header" style="font-weight: normal;">#company_contact_title#</td>
                  <td class="feed_sub_header" style="font-weight: normal;">#company_contact_phone#</td>
                  <td class="feed_sub_header" style="font-weight: normal;">#company_contact_email#</td>
               </tr>
				<cfset email_list = #listappend(email_list,bah.company_contact_email,';')#>
             </cfoutput>

            </cfif>

			<tr><td colspan=5 bgcolor="e0e0e0" class="feed_sub_header"><a href="http://www.sam.gov" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>SAM.Gov</u></a> Point of Contacts</td></tr>

			<cfoutput>

			<cfset poc = 0>

			<cfif pinfo.company_poc_email is not "">
				<tr height=35>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;"><cfif #pinfo.company_poc_first_name# is "" and #pinfo.company_poc_last_name# is "">Unknown<cfelse>#pinfo.company_poc_first_name# #pinfo.company_poc_last_name#</cfif></td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;"><cfif #pinfo.company_poc_title# is "">Unknown<cfelse>#pinfo.company_poc_title#</cfif></td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;"><cfif #pinfo.company_poc_phone# is "">Unknown<cfelse>#pinfo.company_poc_phone#</cfif></td>
				   <td class="feed_sub_header" style="font-weight: normal;">#pinfo.company_poc_email#</td>
				</tr>
				<cfset poc = 1>
				<cfset email_list = #listappend(email_list,pinfo.company_poc_email,';')#>
			</cfif>

			<cfif sams.poc_fnme is not "">
				<tr height=35>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.poc_fnme# #sams.poc_lname#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.poc_title#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.poc_us_phone#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_email#</td>
				</tr>
				<cfset email_list = #listappend(email_list,sams.poc_email,';')#>
				<cfset poc = 1>
			</cfif>

			<cfif sams.pp_poc_fname is not "">
				<tr height=35>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.pp_poc_fname# #sams.pp_poc_lname#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.pp_poc_title#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.pp_poc_phone#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_email#</td>
				</tr>
				<cfset email_list = #listappend(email_list,sams.pp_poc_email,';')#>
				<cfset poc = 1>
			</cfif>

			<cfif sams.alt_poc_fname is not "">
				<tr height=35>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.alt_poc_fname# #sams.alt_poc_lname#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.alt_poc_title#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.akt_poc_phone#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_email#</td>
				</tr>
				<cfset email_list = #listappend(email_list,sams.alt_poc_email,';')#>
				<cfset poc = 1>
			</cfif>

			<cfif sams.elec_bus_poc_fname is not "">
				<tr height=35>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.elec_bus_poc_title#</td>
				   <td class="feed_sub_header" width=200 style="font-weight: normal;">#sams.elec_bus_poc_us_phone#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_email#</td>
				</tr>
				<cfset email_list = #listappend(email_list,sams.elec_bus_poc_email,';')#>
				<cfset poc = 1>
			</cfif>

			<cfif poc is 0>
			 <tr><td colspan=4 class="feed_sub_header" style="font-weight: normal;">No point of contact information could be found the Company.</td></tr>
			</cfif>

            </cfoutput>

		   </table>

		   </td></tr>

        <tr><td colspan=3><hr></td></tr>
        <tr><td height=10></td></tr>

		</cfloop>

		<script>
		function myFunction() {
		  var copyText = document.getElementById("emaillist");
		  copyText.select();
		  copyText.setSelectionRange(0, 99999)
		  document.execCommand("copy");
		  alert("Copied to Clipboard: " + copyText.value);
		}
		</script>

        <cfif email_list is not "">
		<tr><td height=10></td></tr>
		<cfoutput>
		<tr><td colspan=2 class="feed_sub_header">Copy Email List to Clipboard</td></tr>
        <tr><td colspan=5><textarea id="emaillist" name="emaillist" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  style="width: 1000px; height: 70px;">#email_list#</textarea></td></tr>
        <tr><td height=10></td></tr>
        <tr><td colspan=2><button class="button_blue_large" onclick="myFunction()">Copy</button></td></tr>
        </cfoutput>
		</cfif>

	  </table>

	  </cfif>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>