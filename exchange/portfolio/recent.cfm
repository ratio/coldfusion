<div class="left_box">

	<cfquery name="p_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select sharing_portfolio_id from sharing
	 where sharing_hub_id = #session.hub# and
	       sharing_portfolio_id is not null and
		   (sharing_to_usr_id = #session.usr_id#)
	</cfquery>

	<cfif p_access.recordcount is 0>
	 <cfset pf_list = 0>
	<cfelse>
	 <cfset pf_list = valuelist(p_access.sharing_portfolio_id)>
	</cfif>

	<cfquery name="p_own" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select portfolio_id from portfolio
	 where portfolio_usr_id = #session.usr_id# and
	       portfolio_hub_id = #session.hub#
	</cfquery>

	<cfif p_own.recordcount is 0>
	 <cfset po_list = 0>
	<cfelse>
	 <cfset po_list = valuelist(p_own.portfolio_id)>
	</cfif>

	<center>

		  <cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="10">
		   select distinct(portfolio_id), portfolio_type_id, max(recent_date), portfolio_name, portfolio_image from recent
		   join portfolio on portfolio_id = recent_portfolio_id
		   where (recent_portfolio_id in (#pf_list#) or recent_portfolio_id in (#po_list#))
		   group by portfolio_id, portfolio_type_id, portfolio_name, portfolio_image
		   order by max(recent_date) DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><a href="/exchange/portfolio"><b>Recent Portfolios</b></a></td>
		      <td align=right valign=top></td></tr>
		  <tr><td><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfset count = 1>

          <cfif portfolios.recordcount is 0>

           <tr><td class="feed_sub_header" style="font-weight: normal;">No Portfolio's have been viewed.</td></tr>

         <cfelse>

		  <cfloop query="portfolios">

			  <cfquery name="item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select count(portfolio_item_id) as total from portfolio_item
			   where portfolio_item_portfolio_id = #portfolios.portfolio_id#
			  </cfquery>

			  <cfquery name="desc" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select portfolio_desc from portfolio
			   where portfolio_id = #portfolios.portfolio_id#
			  </cfquery>

			  <cfoutput>

				<cfif portfolios.portfolio_type_id is 1>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(portfolios.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

					   <cfif #portfolios.portfolio_image# is "">
						 <img src="/images/icon_portfolio_company.png" width=18 border=0 alt="Company Portfolio" title="Company Porfolio">
					   <cfelse>
						 <img src="#media_virtual#/#portfolios.portfolio_image#" width=18 alt="Company Portfolio" title="Company Porfolio">
					   </cfif>
					   </a></td>

					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(portfolios.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(portfolios.portfolio_name) GT 20>#left(portfolios.portfolio_name,'20')#...<cfelse>#portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
					</tr>

			   <cfelseif portfolios.portfolio_type_id is 2>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#">

					   <cfif #portfolios.portfolio_image# is "">
						 <img src="/images/icon_portfolio_award.png" width=18 border=0 alt="Award Portfolio" title="Award Porfolio">
					   <cfelse>
						 <img src="#media_virtual#/#portfolios.portfolio_image#" width=18 alt="Award Portfolio" title="Award Portfolio">
					   </cfif>
					   </a></td>

					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#"><cfif len(portfolios.portfolio_name) GT 22>#left(portfolios.portfolio_name,'22')#...<cfelse>#portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
				   </tr>

			   <cfelseif portfolios.portfolio_type_id is 3>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#">

					   <cfif #portfolios.portfolio_image# is "">
						 <img src="/images/icon_portfolio_opp.png" width=18 border=0 alt="Opportunity Portfolio" title="Opportunity Porfolio">
					   <cfelse>
						 <img src="#media_virtual#/#portfolios.portfolio_image#" width=18 alt="Opportunity Portfolio" title="Opportunity Portfolio">
					   </cfif>
					   </a></td>

					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#"><cfif len(portfolios.portfolio_name) GT 22>#left(portfolios.portfolio_name,'22')#...<cfelse>#portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
				   </tr>

			   </cfif>

			   <tr><td></td><td class="link_small_gray" colspan=2>

			   <cfif len(desc.portfolio_desc) GT 65>
				#left(desc.portfolio_desc,'65')# ...
			   <cfelse>
				#desc.portfolio_desc#
			   </cfif>

			   </td></tr>

			  <cfif count is not portfolios.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

			  </cfoutput>

			  </td></tr>

			  </cfloop>

          </cfif>

	</center>

	</table>

</div>