<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfset session.portfolio_id = #portfolio_id#>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td class="feed_header">INTEGRATED SCHEDULE & TIMELINE</td>
                 <td class="feed_option" align=right><img src="/images/delete.png" align=absmiddle border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
             <tr><td colspan=2><hr></td></tr>
          </table>

		  <cfquery name="opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio_item
		   left join award_data on portfolio_item_value_id = id
                  where portfolio_item_portfolio_id = #portfolio_id# and
                        portfolio_item_usr_id = #session.usr_id# and
                        portfolio_item_type_id = 3
		  </cfquery>


          <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			      <tr><td height=20></td></tr>
				  <tr><td class="feed_header">#access.portfolio_name#</td></tr>
				  <cfif access.portfolio_desc is not "">
				  <tr><td class="feed_option">#access.portfolio_desc#</td></tr>
				  </cfif>
				  <tr><td height=10></td></tr>
			  </table>
		  </cfoutput>

		  <cfif opps.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header" style="font-weigiht: normal;">No Opportunities have been added to this portfolio.</td></tr>
          </table>

		  <cfelse>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfquery name="port_schedule" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from portfolio_item_schedule
			 left join portfolio_item on portfolio_item_id = portfolio_item_schedule_item_id
			 where portfolio_item_portfolio_id = #portfolio_id# and
				   portfolio_item_schedule_usr_id = #session.usr_id#
			 order by portfolio_item_schedule_end_date ASC
			</cfquery>

<cfif port_schedule.recordcount is 0>
	<tr><td class="feed_sub_header" style="font-weight: normal;">No activities have been recorded for this portfolio.</td></tr>
<cfelse>

<tr><td>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['timeline']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'Name' });
        dataTable.addColumn({ type: 'string', id: 'Activity' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addRows([

        <cfoutput query="port_schedule">

          [ '#portfolio_item_name#', '#portfolio_item_schedule_name#', new Date(#year(portfolio_item_schedule_start_date)#, #evaluate(month(portfolio_item_schedule_start_date)-1)#, #day(portfolio_item_schedule_start_date)#), new Date(#year(portfolio_item_schedule_end_date)#, #evaluate(month(portfolio_item_schedule_end_date)-1)#, #day(portfolio_item_schedule_end_date)#)],

        </cfoutput>

          ]);

    var options = {
      timeline: { colorByRowLabel: true, rowLabelStyle: {fontName: 'Arial', fontSize: 10, color: '#603913' },
                     barLabelStyle: { fontName: 'Arial', fontSize: 10 } }
    };

  function drawChart1() {
    chart.draw(dataTable, options);
  }
  drawChart1();

      }
    </script>

    <div id="timeline" style="width: 100%; height: 500;"></div>

</td></tr>

</cfif>

</table>


		  </cfif>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>