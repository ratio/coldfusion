<cfinclude template="/exchange/security/check.cfm">

<cfset session.portfolio_id = #portfolio_id#>

<cfif id is "">
 <cflocation URL="not_found.cfm?portfolio_id=#portfolio_id#&portfolio_item_id=#portfolio_item_id#" addtoken="no">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfquery name="award_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_data
 where id = #id#
</cfquery>

<cfquery name="award_total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(id) as awards, sum(federal_action_obligation) as obligated, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as total from award_data
 where award_id_piid = '#award_info.award_id_piid#' and
       recipient_duns = '#award_info.recipient_duns#'
</cfquery>

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item
 where portfolio_item_id = #portfolio_item_id#
</cfquery>

<cfquery name="port_schedule" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio_item_schedule
 where portfolio_item_schedule_item_id = #portfolio_item_id# and
       portfolio_item_schedule_usr_id = #session.usr_id#
 order by portfolio_item_schedule_end_date ASC
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfif #session.portfolio_location# is "Home">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

       <cfelseif #session.portfolio_location# is "Navigator">

		   <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
		   <cfinclude template="/exchange/portfolio/nav_company.cfm">
		   <cfinclude template="/exchange/portfolio/nav_award.cfm">
           <cfinclude template="/exchange/portfolio/nav_opp.cfm">

       </cfif>

      <td valign=top>

      <div class="main_box">

      <cfoutput>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
             <tr><td class="feed_header">#port_info.portfolio_item_name#</td>
                 <td class="feed_option" align=right>

				 <cfif #port_info.portfolio_item_usr_id# is #session.usr_id#>

                 <a href="/exchange/portfolio/portfolio_award_edit.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/icon_config.png" width=20 border=0 alt="Edit" title="Edit"></a>

                 &nbsp;&nbsp;

                 </cfif>

                 <a href="/exchange/portfolio/portfolio_award.cfm?portfolio_id=#portfolio_id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
             <tr><td colspan=2><hr></td></tr>
          </cfoutput>

           <cfif isdefined("u")>
            <tr><td height=5></td></tr>
            <cfif u is 1>
             <tr><td class="feed_sub_header"><font color="green"><b>Update successful.</b></font></td></tr>
            </cfif>
           </cfif>
          </table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td class="feed_sub_header">AWARD SUMMARY</td><td class="feed_sub_header" align=right></td>
			    <td class="feed_sub_header" align=right>

                <a href="/exchange/include/award_history.cfm?id=#id#&award_id=#award_info.award_id_piid#&duns=#award_info.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><img src="/images/icon_history.png" border=0 width=25 alt="Contract History" title="Contract History"></a>
                &nbsp;&nbsp;<a href="/exchange/include/award_history.cfm?id=#id#&award_id=#award_info.award_id_piid#&duns=#award_info.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>View Contract History</b></a>

                &nbsp;&nbsp;
                <a href="/exchange/compete/analyze.cfm?id=#award_info.id#&number=#award_info.award_id_piid#" target=_"blank"><img src="/images/icon_analyze_1.png" border=0 width=20 alt="Analyze Competition" title="Analyze Competition"></a>
                &nbsp;&nbsp;<a href="/exchange/compete/analyze.cfm?id=#award_info.id#&number=#award_info.award_id_piid#" target=_"blank"><b>Analyze Competition</b></a>

			    </td></tr>
			<tr><td height=10></td></tr>
		</table>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

					<tr>

					<td class="feed_option" width=200 valign=top><b>
											Recipient Name<br>
											DUNS<br>
											Department<br>
											Agency<br>
											Office<br>
											Funding Office<br>
											Contract Number<br>
											Parent Contract Number<br>
											</b></td>

					<td class="feed_option" valign=top>
											<a href="/exchange/include/federal_profile.cfm?duns=#award_info.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>#award_info.recipient_name#</b></a><br>
											#award_info.recipient_duns#<br>
											#award_info.awarding_agency_name#<br>
											#award_info.awarding_sub_agency_name#<br>
											#award_info.awarding_office_name#<br>
											#award_info.funding_office_name#<br>
											<a href="/exchange/include/award_information.cfm?id=#award_info.id#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><b>#award_info.award_id_piid#</b></a><br>
											#award_info.parent_award_id#<br>

											</td>
					</tr>


				</table>

		</td><td valign=top>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

					<tr>

					<td class="feed_option" width=200 valign=top><b>
									Federal Action Obligation<br>
									Base & Exercised Options Value<br>
									Base & All Options Value<br>
									Award Date<br>
									PoP Start Date<br>
									PoP Current End Date<br>
									PoP Potential End Date<br>
									</b></td>

					<td class="feed_option" valign=top>
									#numberformat(award_info.federal_action_obligation,'$999,999,999,999')#</a><br>
									#numberformat(award_info.base_and_exercised_options_value,'$999,999,999,999')#<br>
									#numberformat(award_info.base_and_all_options_value,'$999,999,999,999')#<br>
									#dateformat(award_info.action_date,'mm/dd/yyyy')#<br>
									#dateformat(award_info.period_of_performance_start_date,'mm/dd/yyyy')#<br>
									#dateformat(award_info.period_of_performance_current_end_date,'mm/dd/yyyy')#<br>
									#dateformat(award_info.period_of_performance_potential_end_date,'mm/dd/yyyy')#<br>
					</td>
					</tr>


				</table>

		        </td><td valign=top>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>
						  <tr><td class="feed_option" colspan=3><b>Total Contract Value</b></td></tr>
						  <tr>
						      <td class="feed_option" valign=top width=50>$0.00</td>
						      <td class="feed_option" wideth=170><a href="/exchange/include/award_history.cfm?id=#id#&award_id=#award_info.award_id_piid#&duns=#award_info.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><progress value="#award_total.obligated#" max="#award_total.total#" alt="#numberformat(evaluate(award_total.total-award_total.obligated),'$999,999,999,999')# Remaining" title="#numberformat(evaluate(award_total.total-award_total.obligated),'$999,999,999,999')# Remaining" style="width:150px; height: 20px;"></progress></a></td>
						      <td class="feed_option">#trim(numberformat(award_total.total,'$999,999,999,999'))#</td>
						  </tr>

                          <tr><td class="feed_option" colspan=2><b>Total Spent:</b> #numberformat(award_total.obligated,'$999,999,999')#</td></tr>
                          <tr><td class="feed_option" colspan=2><b>Total Remaining:</b>  #numberformat(evaluate(award_total.total-award_total.obligated),'$999,999,999,999')#</td></tr>

						</table>

				</td></tr>

		<tr><td class="feed_option" colspan=4><b>Award Description</b><br>#award_info.award_description#</td></tr>

		<tr><td height=15></td></tr>
		<tr><td colspan=3><hr></td></tr>

		</table>

      </cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfoutput>
			 <tr>
			     <td class="feed_sub_header">TIMELINE / SCHEDULE</td>
			     <td class="feed_sub_header" align=right colspan=2>

			     <cfif #port_info.portfolio_item_usr_id# is #session.usr_id#>
			      <a href="/exchange/portfolio/activity_add.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/icon_gannt.png" width=20 border=0 alt="Add Activity" title="Add Activity"></a>
                  &nbsp;
			      <a href="/exchange/portfolio/activity_add.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><b>Add Activity</b></a>
			     </cfif></td>

			 </tr>
			 <tr><td height=4></td></tr>
			 </cfoutput>

			 <cfif port_schedule.recordcount is 0>
			   <tr><td class="feed_option">No activities have been added.</td></tr>
			 <cfelse>

			 <tr><td colspan=2>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['timeline']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'Name' });
        dataTable.addColumn({ type: 'string', id: 'Activity' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addColumn({ type: 'string', role: 'tooltip', id: 'link', 'p': {'html': true} });
        dataTable.addRows([

        <cfoutput query="port_schedule">
          [ 'Activities', '#portfolio_item_schedule_name#', new Date(#year(portfolio_item_schedule_start_date)#, #evaluate(month(portfolio_item_schedule_start_date)-1)#, #day(portfolio_item_schedule_start_date)#), new Date(#year(portfolio_item_schedule_end_date)#, #evaluate(month(portfolio_item_schedule_end_date)-1)#, #day(portfolio_item_schedule_end_date)#),'activity_edit.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#&activity_id=#portfolio_item_schedule_id#'],
        </cfoutput>

          ]);

    var options = {
      timeline: { showRowLabels: false, colorByRowLabel: true }
    };

 google.visualization.events.addListener(chart, 'select', function () {
    var selection = chart.getSelection();
    if (selection.length > 0) {
      window.open(dataTable.getValue(selection[0].row, 4), '_blank');
      console.log(dataTable.getValue(selection[0].row, 4));
    }
  });

  function drawChart1() {
    chart.draw(dataTable, options);
  }
  drawChart1();

      }
    </script>

    <div id="timeline" style="width: 100%; height: 100%;"></div>

   </td></tr>

   </cfif>

 <tr><td height=10></td></tr>
 <tr><td colspan=2><hr></td></tr>

 </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td class="feed_sub_header">NOTES & COMMMENTS</td>
			     <td align=right class="feed_sub_header">

				 <cfif #port_info.portfolio_item_usr_id# is #session.usr_id#>

                 <cfoutput>
                 <a href="/exchange/portfolio/portfolio_award_edit.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><img src="/images/icon_edit.png" width=20 border=0 alt="Edit Notes & Comments" title="Edit Notes & Comments"></a>
                 &nbsp;&nbsp;<a href="/exchange/portfolio/portfolio_award_edit.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_id#"><b>Update</b></a>
                 </cfoutput>


                 </cfif>

			     </td></tr>

             <cfif port_info.portfolio_item_comments is "">

             <tr><td class="feed_sub_header" style="font-weight: normal;">No notes or comments have been added.</td></tr>

             <cfelse>

             <tr><td class="feed_option">

                 <cfoutput>
                 #(replace(port_info.portfolio_item_comments,"#chr(10)#","<br>","all"))#
                 </cfoutput>

                 </td></tr>

             </cfif>

            </table>





    </td></tr>

</table>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>