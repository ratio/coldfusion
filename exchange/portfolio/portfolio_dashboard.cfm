<cfinclude template="/exchange/security/check.cfm">

<cfsetting RequestTimeout = "900000">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

/***********************************************
* Limit number of checked checkboxes script- by JavaScript Kit (www.javascriptkit.com)
* This notice must stay intact for usage
* Visit JavaScript Kit at http://www.javascriptkit.com/ for this script and 100s more
***********************************************/

function checkboxlimit(checkgroup, limit){
	var checkgroup=checkgroup
	var limit=limit
	for (var i=0; i<checkgroup.length; i++){
		checkgroup[i].onclick=function(){
		var checkedcount=0
		for (var i=0; i<checkgroup.length; i++)
			checkedcount+=(checkgroup[i].checked)? 1 : 0
		if (checkedcount>limit){
			alert("You can only select a maximum of "+limit+" companies")
			this.checked=false
			}
		}
	}
}

</script>

</head><div class="center">
<body class="body">

<cfif not isdefined("sv")>
 <cfset sv = 5>
</cfif>

<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

<cfset page = "Open">

<cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

<cfquery name="port_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select portfolio_item_company_id from portfolio_item
  where portfolio_item_portfolio_id = #session.portfolio_id# and
		portfolio_item_type_id = 1
</cfquery>

<cfif port_list.recordcount is 0>
	<cfset plist = 0>
<cfelse>
	<cfset plist = valuelist(port_list.portfolio_item_company_id)>
</cfif>

<cfset end_date = #dateformat(now(),'mm/dd/yyyy')#>
<cfset start_date = dateformat(dateadd("yyyy",-3,end_date),'mm/dd/yyyy')>

<!--- Check for Access --->

 <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select company_id, company_name, company_duns, company_logo, company_website,
         count(distinct(awarding_sub_agency_code)) as customers,
         sum(federal_action_obligation) as amount,
	     count(distinct(award_id_piid)) as contracts from company
  left join award_data on recipient_duns = company_duns
  where company_id in (#plist#) and action_date between '#start_date#' and '#end_date#'
  group by company_id, company_duns, company_name, company_logo, company_website

	<cfif #sv# is 1>
	 order by company_name ASC
	<cfelseif #sv# is 10>
	 order by company_name DESC
	<cfelseif #sv# is 2>
	 order by company_duns ASC
	<cfelseif #sv# is 20>
	 order by company_duns DESC
	<cfelseif #sv# is 3>
	 order by customers DESC
	<cfelseif #sv# is 30>
	 order by customers ASC
	<cfelseif #sv# is 4>
	 order by contracts DESC
	<cfelseif #sv# is 40>
	 order by contracts ASC
	<cfelseif #sv# is 5>
	 order by amount ASC
	<cfelseif #sv# is 50>
	 order by amount DESC
	</cfif>
  OPTION (HASH GROUP);

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="recent.cfm">

       </td><td valign=top>

       <cfinclude template="port_header.cfm">

       <div class="main_box">

          <cfinclude template="portfolio_header.cfm">

		  <cfif companies.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	        <tr><td class="feed_sub_header" style="font-weight: normal;">No Federal award information could be found.</td></tr>
          </table>

		  <cfelse>

		  <!--- Insert Graphs --->

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td height=10></td></tr>

		  <tr><td class="feed_header" colspan=3>Federal Award Dashboard</td></tr>
		  <tr><td height=12></td></tr>

          <tr>
              <td class="feed_sub_header" align=center><b>Total Value of Awards</b></td>
              <td class="feed_sub_header" align=center><b>Contracts</b></td>
              <td class="feed_sub_header" align=center><b>Customers</b></td>
          </tr>
          <tr><td width=33% align=center>

		  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		  <script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Company', 'Total Award Value'],
					<cfoutput query="companies">
				<cfif amount is "">
				["'#company_name#'", 0],
				<cfelse>
				["'#company_name#'", #amount#],
				</cfif>

				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {width: '85%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 10},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: '',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_1'));

			  chart.draw(data, options);
			}

			</script>

		  <div id="chart_div_1"></div>


</td><td width=33% align=center>

		  <script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Company', 'Contracts'],
				<cfoutput query="companies">
				["'#company_name#'", #contracts#],

				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {width: '85%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 10},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: '',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_2'));

			  chart.draw(data, options);
			}

			</script>

		  <div id="chart_div_2"></div>

</td>


</td><td width=33% align=center>

		  <script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Company', 'Customers'],
				<cfoutput query="companies">
				["'#company_name#'", #customers#],

				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {width: '85%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 10},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: '',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_3'));

			  chart.draw(data, options);
			}

			</script>

		  <div id="chart_div_3"></div>

</td>

</tr>

<tr><td height=10></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td class="feed_sub_header" colspan=3>FEDERAL AWARD SUMMARY
		  <cfoutput>
			  <cfif #companies.recordcount# is 0>
			   ( No Companies Selected )
			  <cfelseif #companies.recordcount# is 1>
			   ( 1 Company )
			  <cfelse>
			   ( #companies.recordcount# Companies )
			  </cfif>
		  </cfoutput>

		  </td></tr>

		  <tr><td class="link_small_gray" colspan=3>Companies who do not have any Federal award data are not included in the dashboard.</td></tr>


		  <tr><td height=10></td></tr>

		   <tr>
			   <td class="feed_sub_header">Compare</td>
               <td colspan=2 class="feed_sub_header"><a href="portfolio_dashboard.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Company</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 1><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 10><img src="/images/icon_sort_down.png" width=10></cfif></td>
			   <td></td>
               <td class="feed_sub_header"><a href="portfolio_dashboard.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>DUNS</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 2><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 20><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" align=center><a href="portfolio_dashboard.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Customers</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 3><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 30><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" align=center><a href="portfolio_dashboard.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Contracts</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 4><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 40><img src="/images/icon_sort_down.png" width=10></cfif></td>
               <td class="feed_sub_header" align=right><a href="portfolio_dashboard.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Total Awards</b></a>&nbsp;&nbsp;<cfif isdefined("sv") and sv is 5><img src="/images/icon_sort_up.png" width=10><cfelseif isdefined("sv") and sv is 50><img src="/images/icon_sort_down.png" width=10></cfif></td>

		   </tr>

		  <cfset counter = 0>

		  <form name="compare" id="compare" action="portfolio_compare_set.cfm" method="post">

			  <cfloop query="companies">

			    <cfquery name="innet" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			     select hub_comp_id from hub_comp
			     where hub_comp_hub_id = #session.hub# and
			           hub_comp_company_id = #companies.company_id#
			    </cfquery>

			    <cfoutput>

					   <cfif counter is 0>
					    <tr bgcolor="ffffff" height=20>
					   <cfelse>
					    <tr bgcolor="e0e0e0" height=20>
					   </cfif>

					        <td width=100>
					        <input type="checkbox" name="company_id" value=#companies.company_id# style="margin-left: 20px; width: 20px; height: 20px;"></td>
					        <td width=50>

							<cfif companies.company_logo is "">
							  <img src="//logo.clearbit.com/#companies.company_website#" width=30 border=0 onerror="this.src='/images/no_logo.png'">
							<cfelse>
							  <img src="#media_virtual#/#companies.company_logo#" width=30 border=0>
							</cfif>

                            </td>

                             <td class="feed_sub_header" width=450><a href="/exchange/portfolio/manage_company.cfm?comp_id=#companies.company_id#"><b>#ucase(companies.company_name)#</b></a></td>
                             <td width=100>

                             <cfif innet.recordcount GT 0>
                              <img src="/images/in_network.png" width=25 border=0 alt="In Network Company" title="In Network Company">
                             </cfif>


                             </td>
                             <td class="feed_sub_header" width=200 style="font-weight: normal;"><cfif #companies.company_duns# is "">Not Listed<cfelse>#companies.company_duns#</cfif></td>


                             <td class="feed_sub_header" width=150 style="font-weight: normal;" align=center>#numberformat(companies.customers,'999,999')#</td>
                             <td class="feed_sub_header" width=150 style="font-weight: normal;" align=center>#numberformat(companies.contracts,'999,999')#</td>
                             <td class="feed_sub_header" style="font-weight: normal;" align=right width=150>#numberformat(companies.amount,'$999,999,999,999')#</td>

                             <!---
                             <td class="feed_sub_header" width=100 style="font-weight: normal;" align=center>#numberformat(companies.customers,'999,999')#</td>
                             <td class="feed_sub_header" width=100 style="font-weight: normal;" align=center>#numberformat(companies.amount,'999,999')#</td>
                             --->

                             <td class="text_xsmall" align=right width=50>

                             <cfif session.portfolio_access_level GT 1>
                             &nbsp;&nbsp;&nbsp;&nbsp;<a href="remove_comp.cfm?company_id=#company_id#&l=1" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this company from your portfolio?');"><img src="/images/delete.png" width=10 border=0 alt="Remove" title="Remove">
                             <cfelse>
                             &nbsp;
                             </cfif></td>

                         </tr>

                       <cfif counter is 0>
                        <cfset counter = 1>
                       <cfelse>
                        <cfset counter = 0>
                       </cfif>

			  </cfoutput>

			  </cfloop>

			  </td></tr>

			  </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr><td height=20></td></tr>
              <tr><td><input type="submit" name="button" class="button_blue_large" value="Compare Companies" onclick="javascript:document.getElementById('page-loader').style.display='block';"></td></tr>

		      <tr><td height=10></td></tr>
		      <tr><td><hr></td></tr>

		      <tr><td class="link_small_gray">- Maximum of 5 companies can be selected for comparison.</td></tr>
		      <tr><td class="link_small_gray">- Customers, contracts and award amounts are based on the last 3 years of performance.</td></tr>

              <input type="hidden" name="d" value=1>

              </form>

			<script type="text/javascript">

			//Syntax: checkboxlimit(checkbox_reference, limit)
			checkboxlimit(document.forms.compare.company_id, 5)

			</script>

 		    </table>

		    </td></tr>

		    </table>

		  </cfif>

</td></tr>

</table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>