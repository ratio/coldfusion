<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfif session.portfolio_access_level is 1>
 <cflocation URL="index.cfm" addtoken="no">
</cfif>

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_id = #session.portfolio_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <form action="save.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Portfolio</td>
            <td align=right class="feed_sub_header"><a href="/exchange/portfolio/portfolio_company.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfoutput>

         <tr><td class="feed_sub_header" width=150><b>Portfolio Name</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  class="input_text"  style="width: 534px;" value="#edit.portfolio_name#" name="portfolio_name" style="width:300px;" required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="portfolio_desc" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  cols=70 rows=5>#edit.portfolio_desc#</textarea></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Goals</b></td>
             <td class="feed_option"><textarea name="portfolio_goals" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  cols=70 rows=5>#edit.portfolio_goals#</textarea></td>
             </td></tr>

         <tr><td class="feed_sub_header" width=150><b>Focus Area(s)</b></td>
             <td class="feed_option"><input type="text" onkeypress="isAlphaNum(event);"  onchange="this.value = this.value.replace(/[<>]/g, ' ')"  class="input_text"  style="width: 534px;" name="portfolio_keywords" value="#edit.portfolio_keywords#" style="width:300px;" placeholder="Tags and keywords that frame the focus of this portfolio.">
             &nbsp;&nbsp;seperated with commas (i.e., Energy, Machine Learning, etc.)
             </td></tr>

		 <tr><td height=5></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Portfolio Image</td>
		 	 <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #edit.portfolio_image# is "">
					  <input type="file" id="image" onchange="validate_img()" name="portfolio_image">
					<cfelse>
					  <img src="#media_virtual#/#edit.portfolio_image#" width=150><br><br>
					  <input type="file" id="image" onchange="validate_img()" name="portfolio_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove image
					 </cfif>
		  </td></tr>

          </cfoutput>

          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
          <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Save Portfolio">

          <cfif edit.portfolio_usr_id is session.usr_id>
          &nbsp;&nbsp;
          <input class="button_blue_large" type="submit" name="button" value="Delete Portfolio" onclick="return confirm('Are you sure you want to delete this portfolio?');">
          </cfif>
          </td></tr>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>