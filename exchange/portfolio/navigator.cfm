<cfinclude template="/exchange/security/check.cfm">

<cfquery name="zero" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(portfolio_id) as total from portfolio
 where portfolio_usr_id = #session.usr_id#
</cfquery>

<cfif zero.total is 0>
 <cflocation URL="/exchange/portfolio/index.cfm?u=3" addtoken="no">
</cfif>

<cfif not isdefined("session.nav_start") or not isdefined("portfolio_id")>
 <cfset session.nav_start = 1>

 <cfquery name="start" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select top(1) portfolio_id from portfolio
  where portfolio_usr_id = #session.usr_id#
  order by portfolio_name
 </cfquery>
 <cfset session.portfolio_id = #start.portfolio_id#>
<cfelse>
 <cfset session.portfolio_id = #portfolio_id#>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset session.portfolio_location = "Navigator">

	<!--- Check for Access --->

<cfinclude template="/exchange/portfolio/check_access.cfm">

	 <cfquery name="port_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from portfolio
	  where portfolio_id = #session.portfolio_id#
	 </cfquery>

	 <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select  company_id, portfolio_item_id, company_duns, company_name, max(period_of_performance_current_end_date) as last, count(distinct(awarding_sub_agency_code)) as agencies, count(distinct(awarding_agency_code)) as dept, count(id) as awards, max(federal_action_obligation) as max, count(distinct(award_id_piid)) as contracts, sum(federal_action_obligation) as obligated from portfolio_item
	  left join company on company_id = portfolio_item_company_id
	  left join award_data on recipient_duns = company_duns
	  where portfolio_item_portfolio_id = #session.portfolio_id# and
			portfolio_item_type_id = 1
	  group by company_duns, company_name, company_id, portfolio_item_id
	  order by company_name
	 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/portfolio/portfolio_menu.cfm">
       <cfinclude template="/exchange/portfolio/nav_company.cfm">
       <cfinclude template="/exchange/portfolio/nav_award.cfm">
       <cfinclude template="/exchange/portfolio/nav_opp.cfm">

       </td><td valign=top>


      <div class="main_box">

          <cfinclude template="/exchange/portfolio/portfolio_header.cfm">

		  <cfif companies.recordcount is 0>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header">
		   <cfif port_info.portfolio_type_id is 1>
		    No companies have been added to this portfolio.
		   <cfelseif port_info.portfolio_type_id is 2>
		    No awards have been added to this portfolio.
		   <cfelse>
		    No opportunities have been added to this portfolio.
		   </cfif>

		   </td></tr>
          </table>

		  <cfelse>

		  <!--- Insert Graphs --->

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td width=48% align=center>

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['Company', 'Departments', 'Agencies'],
	        <cfoutput query="companies">
        ["'#company_name#'", #dept#, #agencies#],

        </cfoutput>
      ]);

      var options = {
        title: 'Footprint Diversity',
        chartArea: {width: '85%'},
        legend: 'none',
        height: 200,
        hAxis: {
          textStyle: {color: 'black', fontSize: 10},
          format: 'currency',
          title: '',
          minValue: 0
        },
        vAxis: {
          title: 'Dept & Agencies',
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_1'));

      chart.draw(data, options);
    }

    </script>

  <div id="chart_div_1"></div>


</td><td width=48% align=center>

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['Company', 'Contracts', 'Awards'],
        <cfoutput query="companies">
        ["'#company_name#'", #contracts#, #awards#],

        </cfoutput>
      ]);

      var options = {
        title: 'Contract & Award Diversity',
        chartArea: {width: '85%'},
        legend: 'none',
        height: 200,
        hAxis: {
          textStyle: {color: 'black', fontSize: 10},
          format: 'currency',
          title: '',
          minValue: 0
        },
        vAxis: {
          title: 'Contracts & Awards',
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_2'));

      chart.draw(data, options);
    }

    </script>

  <div id="chart_div_2"></div>

</td></tr>

<tr><td height=10></td></tr>

          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

					   <tr>
					       <td class="feed_option"><b>Company</b></td>
					       <td class="feed_option"><b>DUNS</b></td>
					       <td class="feed_option" align=center><b>Departments</b></td>
					       <td class="feed_option" align=center><b>Agencies</b></td>
					       <td class="feed_option" align=center><b>Contracts</b></td>
					       <td class="feed_option" align=center><b>Awards</b></td>
					       <td class="feed_option" align=right><b>Largest Obligation</b></td>
					       <td class="feed_option" align=right><b>Last Contract Expires</b></td>
					       <td class="feed_option" align=right><b>Federal Awards</b></td>
					   </tr>

		  <cfset counter = 0>

			  <cfoutput query="companies">

					   <cfif counter is 0>
					    <tr bgcolor="ffffff" height=20>
					   <cfelse>
					    <tr bgcolor="e0e0e0" height=20>
					   </cfif>

                             <td class="feed_option"><a href="/exchange/portfolio/manage_company.cfm?company_id=#company_id#&portfolio_id=#session.portfolio_id#&portfolio_item_id=#portfolio_item_id#"><b>#ucase(company_name)#</b></a></td>
                             <td class="feed_option">#company_duns#</td>
                             <td class="feed_option" align=center>#numberformat(dept,'999,999')#</td>
                             <td class="feed_option" align=center>#numberformat(agencies,'999,999')#</td>
                             <td class="feed_option" align=center>#numberformat(contracts,'999,999')#</td>
                             <td class="feed_option" align=center>#numberformat(awards,'999,999')#</td>
                             <td class="feed_option" align=right>#numberformat(max,'$999,999,999')#</td>
                             <td class="feed_option" align=right>#dateformat(last,'mm/dd/yyyy')#</td>
                             <td class="feed_option" align=right>#numberformat(obligated,'$999,999,999')#</td>
                             <td class="text_xsmall" align=right>

                             <cfif port_info.portfolio_usr_id is #session.usr_id#>

                             &nbsp;&nbsp;&nbsp;&nbsp;<a href="remove_comp.cfm?company_id=#company_id#" onclick="return confirm('Remove from Portfolio.\r\nAre you sure you want to remove this company from your portfolio?');"><img src="/images/delete.png" width=12 border=0 alt="Remove" title="Remove">
                             <cfelse>
                             &nbsp;
                             </cfif></td>

                         </tr>

                       <cfif counter is 0>
                        <cfset counter = 1>
                       <cfelse>
                        <cfset counter = 0>
                       </cfif>

			  </cfoutput>

			  </td></tr>

		    </table>

		    </td></tr>

		    </table>

		  </cfif>

        </div>

		</td></tr>

		</table>



<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>