<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td valign=middle width=70>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<cfif #myportfolio.portfolio_image# is "">
			  <tr><td><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(myportfolio.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="/images/icon_portfolio_company.png" height=60 border=0 alt="Open" title="Open"></a>
			  </td></tr>
			<cfelse>
			  <tr><td><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(myportfolio.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><img src="#media_virtual#/#myportfolio.portfolio_image#" height=60 border=0 alt="Open" title="Open"></a></td></tr>
			</cfif>
		 </table>

     <td width=20>&nbsp;</td><td valign=middle>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_title" valign=top><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(myportfolio.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><cfif len(myportfolio.portfolio_name) GT 40>#left(myportfolio.portfolio_name,'40')#...<cfelse>#myportfolio.portfolio_name#</cfif></a></td></tr>
		   <tr><td class="feed_option"><b>Portfolio Manager - #myportfolio.usr_first_name# #myportfolio.usr_last_name#</b></td></tr>

		 </table>

     </td></tr>

     <tr><td colspan=3><hr></td></tr>

     <tr><td colspan=3>

	  <cfif #myportfolio.portfolio_type_id# is 1>

          <!--- Display Company Portfolio --->

		  <cfquery name="port_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select portfolio_item_company_id from portfolio_item
			  where portfolio_item_portfolio_id = #myportfolio.portfolio_id# and
					portfolio_item_type_id = 1
		  </cfquery>

          <cfif port_list.recordcount is 0>
           <cfset plist = 0>
          <cfelse>
           <cfset plist = valuelist(port_list.portfolio_item_company_id)>
          </cfif>

 		  <cfquery name="port_comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  select company_name, company_id, company_duns, company_city, company_state, company_website from company
			  where company_id in (#plist#)
			  order by company_name
		  </cfquery>

         <div class="scroll_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif port_comp.recordcount is 0>
			   <tr><td height=5></td></tr>

		       <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.</td></tr>

		       <!--- <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been added to this Portfolio.<br><br>To add Companies, go to <a href="/exchange/marketplace/partners/network_in.cfm"><b>In Network</b></a> or <a href="/exchange/marketplace/partners/network_out.cfm"><b>Out of Network</b></a> Partners and choose the <u>Add to Portfolio</u> option when viewing the Company profile.</td></tr> --->
			 <cfelse>
			   <tr><td height=5></td></tr>
			   <tr>
				   <td class="text_xsmall" width=150><b>Company</b></td>
				   <td align=center></td>
				   <td class="text_xsmall"><b>Location</b></td>
				   <td class="text_xsmall"><b>Website</b></td>
				   <td class="text_xsmall" align=right stype="padding-right: 5px;"><b>DUNS&nbsp;</b></td>
			   </tr>

			   <tr><td height=5></td></tr>

			   <cfset counter = 0>

			   <cfloop query="port_comp">

		 		  <cfquery name="innet" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 		   select hub_comp_id from hub_comp
		 		   where hub_comp_hub_id = #session.hub# and
		 		         hub_comp_company_id = #port_comp.company_id#
		 		  </cfquery>

				   <cfoutput>

					   <cfif counter is 0>
						<tr bgcolor="ffffff" height=22>
					   <cfelse>
						<tr bgcolor="e0e0e0" height=22>
					   </cfif>

							 <td class="text_xsmall"><a href="/exchange/portfolio/set.cfm?comp_id=#company_id#&i=#encrypt(myportfolio.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#ucase(company_name)#</b></a></td>

					         <td align=center width=50>
					         <cfif innet.recordcount is 1>
					          <img src="/images/in_network.png" width=15 alt="In Network Company" title="In Network Company">
					         </cfif>
					         </td>

					         <td class="text_xsmall"><cfif #company_city# is not "">#company_city# ,</cfif><cfif #company_state# is not "">#company_state#</cfif></td>
							 <td class="text_xsmall"><cfif #company_website# is "">Unknown<cfelse><a href="#company_website#" target="_blank" rel="noopener" rel="noreferrer" rel="noopener noreferrer"><u>#company_website#</u></a></cfif></td>
							 <td class="text_xsmall" align=right style="padding-right: 5px;"><cfif #company_duns# is "">Unknown<cfelse>#company_duns#</cfif>&nbsp;</td>

					   </tr>

					   <cfif counter is 0>
						<cfset counter = 1>
					   <cfelse>
						<cfset counter = 0>
					   </cfif>

				   </cfoutput>

			   </cfloop>

			 </cfif>

 	     </table>

        </div>

        <cfoutput>

    	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
    	  <tr><td height=10></td></tr>
    	  <tr><td colspan=2><hr></td></tr>
    	  <tr><td class="feed_option">Companies <b>#port_comp.recordcount#</b></td>
    	      <td class="feed_option" align=right>Updated on <b>#dateformat(myportfolio.portfolio_updated,'mmm dd, yyyy')#</td></tr>
    	 </table>

       </cfoutput>

	  <cfelseif #myportfolio.portfolio_type_id# is 2>

          <!--- Display Award Portfolio --->

		 <cfquery name="port_awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select id, portfolio_item_id, portfolio_item_portfolio_id, portfolio_item_name, awarding_sub_agency_name, period_of_performance_start_date, period_of_performance_current_end_date, federal_action_obligation from portfolio_item
		  left join award_data on id = portfolio_item_value_id
		  where portfolio_item_portfolio_id = #myportfolio.portfolio_id# and
				portfolio_item_type_id = 2
		 </cfquery>

         <div class="scroll_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		     <cfset ptotal = 0>

             <cfif port_awards.recordcount is 0>
			   <tr><td height=5></td></tr>
		       <tr><td class="feed_sub_header" style="font-weight: normal;">No Awards have been added to this portfolio.<br><br>To add Awards, visit <a href="/exchange/awards/"><b>Federal Awards</b></a> and select <img src="/images/icon_green_add.png" width=20 hspace=5 align=absmiddle>&nbsp;<u>Save to Award Portfolio</u> option when viewing an Award profile.</td></tr>
			 <cfelse>
			   <tr><td height=5></td></tr>

			  <tr>
				 <td class="text_xsmall" width=150><b>Opportunity Name</b></td>
				 <td class="text_xsmall"><b>Agency</b></td>
				 <td class="text_xsmall" align=center><b>PoP Start</b></td>
				 <td class="text_xsmall" align=center><b>PoP End</b></td>
				 <td class="text_xsmall" align=right><b>Award</b></td>
			  </tr>

			   <tr><td height=10></td></tr>

			   <cfset counter = 0>

			   <cfloop query="port_awards">

				   <cfoutput>

					    <cfif counter is 0>
					     <tr bgcolor="ffffff" height=20>
					    <cfelse>
					     <tr bgcolor="e0e0e0" height=20>
					    </cfif>

					          <td class="text_xsmall" valign=top><a href="/exchange/portfolio/manage_award.cfm?id=#id#&portfolio_item_id=#portfolio_item_id#&portfolio_id=#portfolio_item_portfolio_id#"><b>#ucase(portfolio_item_name)#</b></a></td>
					          <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
					          <td class="text_xsmall" valign=top align=center width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
					          <td class="text_xsmall" valign=top align=center width=75>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
					          <td class="text_xsmall" valign=top align=right width=75>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
					          </tr>

					          <cfif federal_action_obligation is "">
					            <cfset ptotal = ptotal + 0>
					          <cfelse>
					            <cfset ptotal = ptotal + federal_action_obligation>
					          </cfif>

					          <cfif counter is 0>
					           <cfset counter = 1>
					          <cfelse>
					           <cfset counter = 0>
					          </cfif>

				   </cfoutput>

			   </cfloop>

			 </cfif>

 	     </table>

        </div>

        <cfoutput>

    	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
    	  <tr><td height=10></td></tr>
    	  <tr><td colspan=2><hr></td></tr>
    	  <tr><td class="feed_option">Awards <b>#port_awards.recordcount#</b>&nbsp;|&nbsp;Total Value <b>#trim(numberformat(ptotal,'$999,999,999'))#</td>
    	      <td class="feed_option" align=right>Updated on <b>#dateformat(myportfolio.portfolio_updated,'mmmm dd, yyyy')#</td></tr>
    	 </table>

       </cfoutput>

	  <cfelseif #myportfolio.portfolio_type_id# is 3>

          <!--- Display Opportunity Portfolio --->

		 <cfquery name="port_opps" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select portfolio_item_id, fbo_opp_name, fbo_id, fbo_office, fbo_notice_type, fbo_solicitation_number, portfolio_item_portfolio_id, portfolio_item_name from portfolio_item
		  left join fbo on fbo_id = portfolio_item_opp_id
		  where portfolio_item_portfolio_id = #myportfolio.portfolio_id# and
				portfolio_item_type_id = 3
		 </cfquery>

         <div class="scroll_box">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		     <cfset ptotal = 0>

             <cfif port_opps.recordcount is 0>
			   <tr><td height=5></td></tr>
		       <tr><td class="feed_sub_header" style="font-weight: normal;">No Opportunities have been added to this portfolio.<br><br>To add Opportunities, visit <a href="/exchange/opps/"><b>Federal Opportunities</b></a> and select <img src="/images/icon_green_add.png" width=20 hspace=5 align=absmiddle>&nbsp;<u>Add to Portfolio</u> option when viewing Opportunity details.</td></tr>
			 <cfelse>
			   <tr><td height=5></td></tr>

			  <tr>
				 <td class="text_xsmall" width=150><b>Opportunity</b></td>
				 <td class="text_xsmall"><b>Title</b></td>
				 <td class="text_xsmall"><b>Office</b></td>
				 <td class="text_xsmall"><b>Solicitation ##</b></td>
				 <td class="text_xsmall"><b>Notice</b></td>
			  </tr>

			   <tr><td height=10></td></tr>

			   <cfset counter = 0>

			   <cfloop query="port_opps">

				   <cfoutput>

					    <cfif counter is 0>
					     <tr bgcolor="ffffff" height=20>
					    <cfelse>
					     <tr bgcolor="e0e0e0" height=20>
					    </cfif>

					          <td class="text_xsmall" valign=middle><a href="/exchange/portfolio/manage_opp.cfm?portfolio_item_id=#port_opps.portfolio_item_id#&portfolio_id=#port_opps.portfolio_item_portfolio_id#"><b>#ucase(port_opps.portfolio_item_name)#</b></a></td>
					          <td class="text_xsmall" valign=middle>#port_opps.fbo_opp_name#</td>
					          <td class="text_xsmall" valign=middle>#port_opps.fbo_office#</td>
					          <td class="text_xsmall" valign=middle><cfif #port_opps.fbo_solicitation_number# is "">Not provided<cfelse>#port_opps.fbo_solicitation_number#</cfif></td>
					          <td class="text_xsmall" valign=middle>#port_opps.fbo_notice_type#</td>
					          </tr>

					          <cfset ptotal = ptotal + 0>

					          <cfif counter is 0>
					           <cfset counter = 1>
					          <cfelse>
					           <cfset counter = 0>
					          </cfif>

				   </cfoutput>

			   </cfloop>

			 </cfif>

 	     </table>

        </div>

        <cfoutput>

    	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
    	  <tr><td height=10></td></tr>
    	  <tr><td colspan=2><hr></td></tr>
    	  <tr><td class="feed_option">Opportunities <b>#port_opps.recordcount#</b></td>
    	      <td class="feed_option" align=right>Updated on <b>#dateformat(myportfolio.portfolio_updated,'mmmm dd, yyyy')#</td></tr>
    	 </table>

       </cfoutput>

 	   </cfif>

       </td></tr>

    </table>

</cfoutput>