<!--- Portfolios --->

<div class="left_box">

	<center>

			<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select sharing.sharing_group_id from sharing
			 join sharing_group on sharing_group.sharing_group_id = sharing.sharing_group_id
			 join sharing_group_usr on sharing_group_usr_group_id = sharing.sharing_group_id
			 where sharing_hub_id = #session.hub# and
				   sharing_group_usr_usr_id = #session.usr_id#
			</cfquery>

			<cfif groups.recordcount is 0>
			 <cfset group_list = 0>
			<cfelse>
			 <cfset group_list = valuelist(groups.sharing_group_id)>
			</cfif>

			<cfquery name="port_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select sharing_portfolio_id from sharing
			 where sharing_hub_id = #session.hub# and
				   (sharing_to_usr_id = #session.usr_id# or sharing_group_id in (#group_list#))
			</cfquery>

			<cfif port_access.recordcount is 0>
			 <cfset port_list = 0>
			<cfelse>
			 <cfset port_list = valuelist(port_access.sharing_portfolio_id)>
			</cfif>

		  <cfquery name="portfolios" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from portfolio
		   where portfolio_hub_id = #session.hub# and
			    (portfolio_id in (#port_list#) or portfolio_usr_id = #session.usr_id# or portfolio_company_access = 1)
		   order by portfolio_updated DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=bottom><a href="/exchange/portfolio/"><b>PORTFOLIOS</b></a></td>
		      <td align=right valign=top><a href="/exchange/portfolio/create_portfolio.cfm"><img src="/images/plus3.png" border=0 vspace=3 width=15 alt="Add Portfolio" title="Add Portfolio"></a></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		  </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif portfolios.recordcount is 0>

          	  <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any <a href="/exchange/portfolio/"><b>Company Portfolios</b></a>.</td></tr>

          <cfelse>

			  <cfset count = 1>

			  <cfloop query="portfolios">

			  <cfquery name="item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   select count(portfolio_item_id) as total from portfolio_item
			   where portfolio_item_portfolio_id = #portfolios.portfolio_id#
			  </cfquery>

			  <cfoutput>

			   <cfif portfolios.portfolio_type_id is 1>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(portfolios.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">

					   <cfif #portfolios.portfolio_image# is "">
						 <img src="/images/icon_portfolio_company.png" width=18 border=0 alt="Open Portfolio" title="Open Porfolio">
					   <cfelse>
						 <img src="#media_virtual#/#portfolios.portfolio_image#" width=18 alt="Open Portfolio" title="Open Porfolio">
					   </cfif>

					   </a></td>
					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?i=#encrypt(portfolios.portfolio_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif #len(portfolios.portfolio_name)# GT 20>#left(portfolios.portfolio_name,'20')#...<cfelse>#portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
					</tr>

			   <cfelseif portfolios.portfolio_type_id is 2>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#">

					   <cfif #portfolios.portfolio_image# is "">
						 <img src="/images/icon_portfolio_award.png" width=18 border=0 alt="Award Portfolio" title="Award Porfolio">
					   <cfelse>
						 <img src="#media_virtual#/#portfolios.portfolio_image#" width=18 alt="Award Portfolio" title="Award Portfolio">
					   </cfif>

					   </a></td>
					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#"><cfif #len(portfolios.portfolio_name)# GT 22>#left(portfolios.portfolio_name,'22')#...<cfelse>#portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
				   </tr>

			   <cfelseif portfolios.portfolio_type_id is 3>

				   <tr>
					   <td width=30><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#">

					   <cfif #portfolios.portfolio_image# is "">
						 <img src="/images/icon_portfolio_opp.png" width=18 border=0 alt="Opportunity Portfolio" title="Opportunity Porfolio">
					   <cfelse>
						 <img src="#media_virtual#/#portfolios.portfolio_image#" width=18 alt="Opportunity Portfolio" title="Opportunity Portfolio">
					   </cfif>

					   </a></td>
					   <td class="link_med_blue"><a href="/exchange/portfolio/refresh.cfm?portfolio_id=#portfolios.portfolio_id#"><cfif #len(portfolios.portfolio_name)# GT 22>#left(portfolios.portfolio_name,'22')#...<cfelse>#portfolios.portfolio_name#</cfif></a></td>
					   <td class="link_med_blue" align=right>#item.total#</td>
				   </tr>

			   </cfif>

			   <tr><td></td><td class="link_small_gray" colspan=2>

			   <cfif len(portfolios.portfolio_desc) GT 65>
				#left(portfolios.portfolio_desc,'65')# ...
			   <cfelse>
				#portfolios.portfolio_desc#
			   </cfif>

			   </td></tr>

			  <cfif count is not portfolios.recordcount>
			   <tr><td colspan=3><hr></td></tr>
			  </cfif>

			  <cfset count = count + 1>

			  </cfoutput>

			  </td></tr>

			  </cfloop>

		  </cfif>

	</center>

	</table>

</div>