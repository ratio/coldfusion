<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

  <cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from market
  </cfquery>

  <cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from sector
   order by sector_name
  </cfquery>

  <cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from topic
   order by topic_name
  </cfquery>

  <cfquery name="stage" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   select * from stage
   order by stage_order
  </cfquery>

	  <cfquery name="market_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
			 (align_type_id = 2)
	  </cfquery>

	  <cfset #market_list# = #valuelist(market_alignment.align_type_value)#>

	  <cfquery name="sector_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
			 (align_type_id = 3)
	  </cfquery>

	  <cfset #sector_list# = #valuelist(sector_alignment.align_type_value)#>

	  <cfquery name="topic_alignment" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select align_type_value from align
	   where (align_usr_id = #session.usr_id#) and
			 (align_type_id = 4)
	  </cfquery>

	  <cfset #topic_list# = #valuelist(topic_alignment.align_type_value)#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td class="feed_header"><b>EXCHANGE Alignments</b></td></tr>
			   <tr><td class="feed_option">Please select the markets, sectors, and capabilities and services that you are interested in.  These alignments are used to send you specific opportuities that are targeted to your personal preferences.</td></tr>
			   <tr><td>&nbsp;</td></tr>

           </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <form action="alignments_db.cfm" method="post">

        <tr><td colspan=2>

			<table cellspacing=0 cellpadding=0 border=0>

			<tr><td><b>Markets you support</b></td>
				<td><b>Sectors you're focused on</b></td>
				<td><b>Capabilities & Services you deliver</b></td>
				</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>

				 <td width=180>
					  <select style="font-size: 12px; width: 160px; height: 260px; padding-left: 10px; padding-top: 5px;" name="market_id" multiple>
					   <cfoutput query="market">
						<option value=#market_id# <cfif listfind(market_list,market_id)>selected</cfif>>#market_name#
					   </cfoutput>
					  </select>

				</td>

				 <td width=220>
					  <select style="font-size: 12px; width: 200px; height: 260px; padding-left: 10px; padding-top: 5px;" name="sector_id" multiple>
					   <cfoutput query="sector">
						<option value=#sector_id# <cfif listfind(sector_list,sector_id)>selected</cfif>>#sector_name#
					   </cfoutput>
					  </select>

				</td>

				 <td>
					  <select style="font-size: 12px; width: 275px; height: 260px; padding-left: 10px; padding-top: 5px;" name="topic_id" multiple>
					   <cfoutput query="topic">
						<option value=#topic_id# <cfif listfind(topic_list,topic_id)>selected</cfif>>#topic_name#
					   </cfoutput>
					  </select>

				</td>

				</tr>

				<tr><td>&nbsp;</td></tr>
				<tr><td colspan=3><input class="button_blue" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Cancel" vspace=10>&nbsp;&nbsp;</td></tr>

              </form>

			  </table>

		   </td></tr>

 		  </table>

        </td></tr>
       </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

