<cfif not isdefined("session.feed_time")>
 <cfset #session.feed_time# = 0>
</cfif>

<cfif not isdefined("session.feed_category_id")>
 <cfset #session.feed_category_id# = 0>
</cfif>

<cfif not isdefined("session.hub_filter")>
 <cfset #session.hub_filter# = 1>
</cfif>

<!--- Get Children --->

<cfquery name="hub_children" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_parent_hub_id = #session.hub#
</cfquery>

<cfif hub_children.recordcount is 0>
 <cfset hub_list = 0>
<cfelse>
 <cfset hub_list = valuelist(hub_children.hub_id)>
</cfif>

<cfquery name="feed" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from feed
 join usr on usr_id = feed_created_usr_id
 join lens on lens_id = feed_category_id
 left join hub on hub_id = feed_hub_id
 join hub_xref on hub_xref_hub_id = hub_id
 where (hub_xref_usr_id = #session.usr_id#) and
       (hub_xref_active = 1) and
       (hub_xref_hub_id = #session.hub# or hub_xref_hub_id in (#hub_list#))

<cfif isdefined("session.feed_keyword")>
  <cfif trim(session.feed_keyword) is not "">
	 and contains((feed_desc, feed_title, feed_keywords, feed_company_name, feed_user_name),'"#session.feed_keyword#"')
  </cfif>
</cfif>

<cfif isdefined("session.feed_category_id")>
 <cfif session.feed_category_id is not 0>
 	and (feed.feed_category_id = #session.feed_category_id#)
</cfif>
</cfif>

 <cfif #session.feed_time# is not 0>

	   <cfif #session.feed_time# is 1>

	   <cfset start_date = dateformat(now(),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
	   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

	  <cfelseif #session.feed_time# is 2>

		   <cfset start_date = dateformat(dateadd('d','-7',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

		  <cfelseif #session.feed_time# is 3>
		   <cfset start_date = dateformat(dateadd('d','-30',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

		  <cfelseif #session.feed_time# is 4>
		   <cfset start_date = dateformat(dateadd('d','-90',now()),'mm/dd/yyyy') & ' 12:00:00' & ' AM'>
		   <cfset end_date = dateformat(now(),'mm/dd/yyyy') & ' 11:59:59' & ' PM'>

	 </cfif>

	  and (feed.feed_created >= '#start_date#' and feed.feed_created <= '#end_date#')

 </cfif>

 order by feed.feed_created DESC

</cfquery>

<cfset perpage = 50>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt feed.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(feed.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <form action="/exchange/search.cfm" method="post">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Posts</b> are messages to everyone in this Exchange. Subscribe to categories by <a href="/exchange/feed/categories.cfm">clicking here</a>.</td>
           <td class="feed_sub_header" align=right>
		   <a href="/exchange/post.cfm"><img src="/images/pencil.png" height=20 border=0 align=middle hspace=5></a>
				<a href="post.cfm">New Post</a>&nbsp;

           </td></tr>

       <tr><td colspan=2>

			<input style="width: 200px;" class="input_text" type="text" name="keyword" placeholder="search posts..." <cfif isdefined("session.feed_keyword")>value="<cfoutput>#session.feed_keyword#</cfoutput>"</cfif>>&nbsp;&nbsp;

			<cfquery name="lens" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select lens_id, lens_name from lens
			 join align on align_type_value = lens_id
			 where (align_usr_id = #session.usr_id#) and
				   (align_hub_id = #session.hub#) and
				   (align_type_id = 1)
				   order by lens_order
			</cfquery>

			<select name="feed_category_id" class="input_select">
			 <option value=0>All Categories
			 <cfoutput query="lens">
			  <option value=#lens_id# <cfif #lens_id# is #session.feed_category_id#>selected</cfif>>#lens_name#
			 </cfoutput>
			</select>

			<a href="/exchange/feed/categories.cfm"><img src="/images/icon_config.png" width=20 align=absmiddle alt="Category Subscriptions" title="Category Subscriptions"></a>&nbsp;

			<select name="time" class="input_select" style="width:125px">
				<option value=0 <cfif session.feed_time is 0>selected</cfif>>Show All
				<option value=1 <cfif session.feed_time is 1>selected</cfif>>Today
				<option value=2 <cfif session.feed_time is 2>selected</cfif>>This Week
				<option value=3 <cfif session.feed_time is 3>selected</cfif>>This Month
				<option value=4 <cfif session.feed_time is 4>selected</cfif>>This Quarter
			</select>


			<input class="button_blue" type="submit" name="button" value="Filter">&nbsp;
			<input class="button_blue" type="submit" name="button" value="Reset">&nbsp;
		   </td>

		   <td align=right class="feed_sub_header">

			<cfoutput>
				<cfif feed.recordcount GT #perpage#>
					<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage)>
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt feed.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage)>
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>
			</cfoutput>

		   </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>

           <tr><td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">
           <cfif feed.recordcount is 0>
             No posts were found.
           <cfelseif feed.recordcount is 1>
            1 Post was found.
           <cfelse>
            <cfoutput>#feed.recordcount# Posts were found.</cfoutput>
           </cfif>
           </td></tr>
       </table>

       </form>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td>

<!--- Start Post Updates --->

<cfinclude template="updates.cfm">

<!--- End Post Updates --->

<cfif isdefined("u")>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <cfif u is 1>
   <tr><td class="feed_sub_header" style="color: green;">Your post has been saved.</td></tr>
   <cfelseif u is 3>
   <tr><td class="feed_sub_header" style="color: green;">Your post has been successfully updated.</td></tr>
   <cfelseif u is 4>
   <tr><td class="feed_sub_header" style="color: red;">Your post has been successfully removed.</td></tr>
   <cfelseif u is 7>
   <tr><td class="feed_sub_header" style="color: green;">Your categories have been updated.</td></tr>
   </cfif>
  </table>

</cfif>

<cfif feed.recordcount is 0>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_sub_header" style="font-weight: normal;">To start receiving posts please ensure you have subscribed to one or more <a href="/exchange/feed/categories.cfm"><b>Categories</b>.</a></td></tr>
  </table>

<cfelse>

<cfoutput query="feed" startrow="#url.start#" maxrows="#perpage#">

  <cfset mdiff = #datediff("n",feed.feed_created,now())#>

	   <div class="feed_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=100>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <cfif #feed.usr_photo# is "">
				  <tr><td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(feed.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" width=75 border=0></a></td></tr>
				 <cfelse>
				  <tr><td><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(feed.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 0px;" src="#media_virtual#/#feed.usr_photo#" width=75 border=0></a></td></tr>
				 </cfif>
			   </table>

		   </td><td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   	<tr><td class="feed_header" style="padding-top: 0px; padding-bottom: 0px; vertical-align: top;" ><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(feed.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#feed.usr_first_name# #feed.usr_last_name#</a>

			   	</td>

	      <td align=right valign=top>

                       <span class="link_small_gray">
                       <b>
		  			   <cfif #mdiff# LT 1>
		  				 < Minute
		  			   <cfelseif #mdiff# GTE 1 and #mdiff# LT 2>
		  				#mdiff# Minute
		  			   <cfelseif #mdiff# GTE 2 and #mdiff# LT 60>
		  				#mdiff# Minutes
		  			   <cfelseif #mdiff# GT 60>
		  				<cfif #mdiff# LT 1440>
		  				 #round(evaluate(mdiff/60))# Hours
		  				<cfelse>
		  				 <cfif #round(evaluate(mdiff/1440))# is 1>
		  				   #round(evaluate(mdiff/1440))# Day
		  				 <cfelse>
		  				   #round(evaluate(mdiff/1440))# Days
		  				 </cfif>
		  				</cfif>
		  			   </cfif>
		  			   </b>
		  			   </span>

		  	<cfif #feed.feed_created_usr_id# is #session.usr_id#>

				<div class="dropdown" style="cursor: pointer;">
				  <img src="/images/3dots2.png" style="cursor: pointer; padding-left: 10px;" height=7>
                  <div class="dropdown-content" style="top: 5; width: 150px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px; padding-top: 0px; margin-top: 2px;">
                    <a href="/exchange/edit_post.cfm?i=#encrypt(feed.feed_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><i class="fa fa-fw fa-pencil" style="padding-right: 10px;"></i>Edit Post</a>
					<a href="/exchange/delete_post.cfm?i=#encrypt(feed.feed_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="return confirm('Delete Post?\r\nAre you sure you want to delete this post?');"><i class="fa fa-fw fa-trash" style="padding-right: 10px;"></i>Delete Post</a>
				  </div>
				</div>

			</cfif>

			</span>

			   	    </td></tr>
			   	<tr><td class="feed_option" style="font-weight: normal; padding-top: 2px; padding-bottom: 0px;">#ucase(feed.usr_company_name)#</td>
			   	    <td class="feed_option" style="font-weight: normal; padding-top: 2px; padding-bottom: 0px;" align=right><b>#ucase(lens_name)#</b></td></tr>
			   	<tr><td class="feed_option" style="font-weight: normal; padding-top: 3px; padding-bottom: 0px;">#feed.usr_title#</td>
			   	    </tr>

			   </table>

		   </td></tr>

           <tr><td height=10></td></tr>
           <tr><td colspan=2>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   	<cfif feed_title is not "">
			   		<tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 5px;" colspan=2>

			   		<cfif feed_url is not "">
			   		<a href="#feed_url#" target="_blank" rel="noopener" rel="noreferrer">#ucase(feed_title)#</a>
			   		<cfelse>

			   		<b>#ucase(feed_title)#</b>
			   		</cfif>

			   		</td></tr>
			   	</cfif>

				<style>
				.image {
				  height: 250px;
				  width: 100%;
				}
				</style>

                <cfif feed_pic is not "">
                	<tr><td colspan=2>

                	<cfif feed_url is not "">
                	<a href="#feed_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#feed.feed_pic#" border=0 class="image"></a>
                	<cfelse>
                	<img src="#media_virtual#/#feed.feed_pic#" class="image" border=0>
                	</cfif>

                	</td></tr>
                </cfif>

			   	<tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 5px;" colspan=2>#replace(feed_desc,"#chr(10)#","<br>","all")#</td></tr>

			   		<tr><td class="link_small_gray" style="font-weight: normal; padding-top: 5px;">#feed_keywords#</a></td>

                     <cfif feed.hub_id is #session.hub#>
                      <td class="link_small_gray" style="font-weight: normal; padding-top: 5px;" align=right></td>
                     <cfelse>
                      <td class="link_small_gray" style="font-weight: normal; padding-top: 5px;" align=right><a href="/exchange/marketplace/communities/s.cfm?i=#encrypt(hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#hub_name# Community</b></a></td>
                     </cfif>

			   		</tr>


			   </table>

           </td></tr>

		  </table>

   </div>

</cfoutput>

</cfif>

</td></tr>

</table>