	  <cfquery name="hp_left" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		select * from home_section
		left join component on component_id = home_section_component_id
		where home_section_profile_id = #usr.hub_xref_role_id# and
			  home_section_hub_id = #session.hub# and
			  home_section_location = 'Left' and
			  home_section_active = 1
			  order by home_section_order
	  </cfquery>

	  <cfif hp_left.recordcount is 0>

	  <div class="left_box">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">No home page left sections exist.</td></tr>
		</table>

	  </div>

	  <cfelse>

      <cfoutput query="hp_left">

           <cfif hp_left.home_section_component_id is not 0>

              <cfinclude template="#hp_left.component_path#">

           <cfelse>

           <div class="left_box">

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #hp_left.home_section_display# is 1>

				<tr><td class="feed_header">#hp_left.home_section_name#</td></tr>
				<tr><td><hr></td></tr>


				<cfif hp_left.home_section_desc is not "">

					<tr>
					   <td class="feed_sub_header" style="font-weight: normal;">#hp_left.home_section_desc#</td>
					</tr>

				</cfif>

            </cfif>

            <tr>
               <td class="feed_sub_header" style="font-weight: normal;">#hp_left.home_section_content#</td>
            </tr>

           </table>

           </cfif>

          </cfoutput>

	  </cfif>
