<cfinclude template="/exchange/security/check.cfm">

<cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_data
 where id = #id#
</cfquery>

<cfquery name="value" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select sum(federal_action_obligation) as total from award_data
 where award_id_piid = '#info.award_id_piid#'
</cfquery>

<cfquery name="subs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(subawardee_duns)) as total from award_data_sub
 where prime_award_piid = '#info.award_id_piid#'
 and prime_awardee_duns = '#info.recipient_duns#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">ADD TO OPPORTUNITY</td>
             <td class="feed_header" align=right><img src="/images/delete.png" width=20 border=0 style="cursor: pointer;" onclick="windowClose();"></td></tr>
         <tr><td colspan=2><hr></td></tr>

</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td valign=top>

 <cfoutput query="info">

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>

			<td class="feed_option" width=200 valign=top><b>
                            		Recipient Name<br>
                            		DUNS<br>
									Department<br>
									Agency<br>
									Office<br>
									Contract Number<br>
									Parent Contract Number<br>
									</b></td>

			<td class="feed_option" valign=top>
									#recipient_name#<br>
                                    #recipient_duns#<br>
			                        #awarding_agency_name#<br>
									#awarding_sub_agency_name#<br>
									#awarding_office_name#<br>
									#award_id_piid#<br>
									#parent_award_id#<br>

									</td>
			</tr>


		</table>

</td><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr>

			<td class="feed_option" width=200 valign=top><b>
                            Total Value<br>
                            This Action / Mod<br>
                            Award Date<br>
                            PoP Start Date<br>
                            PoP Current End Date<br>
                            PoP Potential End Date<br>
                            </b></td>

            <td class="feed_option" valign=top>

							#numberformat(value.total,'$999,999,999,999')#<br>
                            #numberformat(federal_action_obligation,'$999,999,999,999')#<br>
                            #dateformat(action_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_start_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#<br>
            </td>
			</tr>


		</table>

</td></tr>

<tr><td class="feed_option" colspan=4><b>Award Description</b><br>
<cfif len(award_description) GT 400>
	#left(award_description,400)#...
<cfelse>
	#award_description#
</cfif></td></tr>

<tr><td colspan=2><hr></td></tr>

</table>

</cfoutput>

<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pipeline
 where pipeline_usr_id = #session.usr_id# and
       pipeline_hub_id = #session.hub#
 order by pipeline_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="/exchange/include/save_pipeline_db.cfm" method="post">

 <cfif pipeline.recordcount is 0>

     <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created any Opportunities.  To add this Award to an Opportunity, please create one now.</td></tr>
     <tr><td class="feed_sub_header">Opportunity Name</td></tr>
     <tr><td><input type="text" name="pipeline_name" size=50 class="input_text" required placeholder="Please provide a name for this Opportunity."></td></tr>
     <tr><td height=10></td></tr>
     <tr><td><hr></td></tr>
     <tr><td class="feed_sub_header"><b>Model Name</b></td></tr>
     <tr><td><input type="text" name="deal_name" size=50 class="input_text" required placeholder="Please provide a name for this Model."></td></tr>
     <tr><td class="feed_sub_header" valign=top><b>Description</td></tr>
     <tr><td class="feed_option"><textarea name="deal_desc" rows=3 cols=120 class="input_textarea" placeholder="Please add any comments you'd like about this Model."></textarea></td></tr>
     <tr><td class="feed_sub_header"><b>Keywords</b></td></tr>
     <tr><td><input type="text" name="deal_keywords" size=50 class="input_text" required placeholder="Please provide tags or keywords for this Model."></td></tr>
     <tr><td class="feed_sub_header"><b>Subcontractors</b></td></tr>
     <cfif subs.total is 0>
	     <tr><td class="feed_sub_header" style="font-weight: normal;">No Subcontractors were found for this Award.</td></tr>
     <cfelse>
	     <tr><td class="feed_sub_header" style="font-weight: normal;"><input type="checkbox" name="add_subs" checked style="width: 22px; height: 22px;">Add the <cfoutput>#subs.total#</cfoutput> Subcontractors as Potential Partners to the Deal?</td></tr>
     </cfif>


     <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Create and Save">

 <cfelse>

     <tr>
	     <td class="feed_sub_header">Select Existing Opportunity</td>
         <td class="feed_sub_header">Or, Create New Opportunity</td>
	 </tr>

     <tr>
         <td>
			 <select name="deal_pipeline_id" class="input_select" style="width: 400px;">
			 <cfoutput query="pipeline">
			  <option value=#pipeline_id#>#pipeline_name#
			 </cfoutput>
			 <select></td>
     <td><input type="text" name="pipeline_name" size=40 class="input_text" placeholder="Please provide a name for this Opportunity."></td>
			 </tr>

     <tr><td colspan=2><hr></td></tr>

     <tr><td class="feed_sub_header" colspan=2>Model Name</td>

	 <tr><td colspan=2><input type="text" name="deal_name" size=50 class="input_text" required placeholder="Please provide a name for this Model."></td></tr>

	 <tr><td height=5></td></tr>
	 <tr><td colspan=2 class="feed_sub_header" valign=top><b>Description</td></tr>
	 <tr><td colspan=2 class="feed_option"><textarea name="deal_desc" rows=3 cols=120 class="input_textarea"></textarea></td></tr>
     <tr><td class="feed_sub_header"><b>Keywords</b></td></tr>
     <tr><td><input type="text" name="deal_keywords" size=50 class="input_text" required placeholder="Please provide tags or keywords for this Model."></td></tr>
     <tr><td class="feed_sub_header"><b>Subcontractors</b></td></tr>
     <cfif subs.total is 0>
	     <tr><td class="feed_sub_header" style="font-weight: normal;">No Subcontractors were found for this Award.</td></tr>
     <cfelse>
	     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><input type="checkbox" name="add_subs" checked style="width: 22px; height: 22px;">&nbsp;&nbsp;Add the <b><cfoutput>#subs.total#</cfoutput> Subcontractors</b> as Potential Partners to the Deal?</td></tr>
     </cfif>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Save Deal to Pipeline">
     <tr><td height=20></td></tr>
 </cfif>

<cfoutput>
<input type="hidden" name="deal_award_id" value=#id#>
</cfoutput>

</form>

</table>

</center>

</body>
</html>

