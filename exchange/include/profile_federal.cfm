<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("s")>

 <cfif s is "t">

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select company_duns from company
   where company_id = #id#
  </cfquery>

  <cfset session.company_profile_duns = #duns.company_duns#>
  <cfset session.company_profile_id = #id#>

 </cfif>
</cfif>

<cfif isdefined("ky")>
 <cfset session.profile_filter = #trim(ky)#>
</cfif>

<!--- Insert Recent --->

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_usr_id, recent_company_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#session.usr_id#,#session.company_profile_id#,#session.company_id#,#session.hub#,#now()#)
</cfquery>

<cfquery name="name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns = '#name.company_duns#'
</cfquery>

<cfset duns = #name.company_duns#>
<cfset id = #name.company_id#>



<html>
<head>
	<title><cfif isdefined("session.company_profile_name")><cfoutput>#session.company_profile_name#</cfoutput><cfelse>Exchange</cfif></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<cfinclude template = "/exchange/include/header.cfm">

<div class="main_box">

	<cfinclude template="/exchange/include/company_header.cfm">
</div>


<cfoutput>

  <div class="tab_not_active" style="margin-left: 20px;">
   <span class="feed_sub_header"><img src="/images/icon_blocks.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="profile.cfm">Snapshot</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Company Details</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Products & Services</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">People</a></span>
  </div>

  <div class="tab_active">
   <span class="feed_header"><img src="/images/icon_fed.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="profile_federal.cfm">Federal Profile</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Customers</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Marketing</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Funding</a></span>
  </div>

</cfoutput>

<div class="main_box_2">

  <cfoutput>
  <table cellspacing=0 cellpadding=0 border=0 width=100%>

   <tr><td height=10></td></tr>

   <tr><td class="feed_header">#name.company_name#</td><td colspan=2 align=right class="feed_sub_header" style="font-weight: normal;"><b>Apply Filter</b>&nbsp;&nbsp;

   <input type="text" name="filter" class="input_text" style="width: 200px;" placeholder="keyword">

   &nbsp;<b>from</b>&nbsp;
   <input type="date" type="date" class="input_date" name="from" style="width: 175px;">
   &nbsp;<b>to</b>&nbsp;
   <input type="date" type="date" class="input_date" name="to" style="width: 175px;">

   <input type="submit" name="button" class="button_blue" value="Apply">

   </td></tr>

   </table>
  </cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  </table>

 <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td colspan=5><hr></td></tr>
			<tr><td height=20></td></tr>
			<tr><td class="feed_header">SAMS.Gov Information</td></tr>
			<tr><td colspan=5 class="feed_sub_header" style="font-weight: normal;">The following information was extracted from SAMS.Gov and is updated every 30 days.</td></tr>
			<tr><td height=20></td></tr>


			<tr><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_option"><b>Company Information</b></td></tr>

				<tr><td class="feed_option"><b>Website: </b>

				<cfoutput>
				<cfif left("#sams.corp_url#",4) is "http" or left("#sams.corp_url#",5) is "https">
				  <a href="#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer"><u>#sams.corp_url#</u></a>
				<cfelse>
				 <a href="http://#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer"><u>#sams.corp_url#</u></a>
				</cfif>
				</cfoutput><br>

											<b>DUNS: </b>#sams.duns#<cfif #sams.duns_4# is not "">-#sams.duns_4#</cfif><br>
											<b>State of Incorporation: </b>#sams.state_of_inc#<br>
											<b>Cage Code: </b>#sams.cage_code#

											</td></tr>


				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_option"><b>Address</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.phys_address_l1# is not "">#sams.phys_address_l1#<br></cfif>
											<cfif #sams.phys_address_line_2# is not "">#sams.phys_address_line_2#<br></cfif>
											#sams.city_1# #sams.state_1#, #sams.zip_1#<br>
											</td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_option"><b>Important Dates</b></td></tr>
				<tr><td class="feed_option">
											<b>Founded: </b>#mid(sams.biz_start_date,5,2)#/#right(sams.biz_start_date,2)#/#left(sams.biz_start_date,4)#<br>
											<b>Registration Date: </b>#mid(sams.init_reg_date,5,2)#/#right(sams.init_reg_date,2)#/#left(sams.init_reg_date,4)#<br>
											<b>Activation Date: </b>#mid(sams.activation_date,5,2)#/#right(sams.activation_date,2)#/#left(sams.activation_date,4)#<br>
											<b>Expiration Date: </b>#mid(sams.reg_exp_date,5,2)#/#right(sams.reg_exp_date,2)#/#left(sams.reg_exp_date,4)#<br>
											<b>Last Updated: </b>#mid(sams.last_update,5,2)#/#right(sams.last_update,2)#/#left(sams.last_update,4)#<br>


											</td></tr>

				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr>
			   <td class="feed_option"><b>Business Type</b></td>

		   <tr><td class="feed_option" valign=top>

		   <cfif sams.biz_type_string is "">
			   Unknown
		   <cfelse>

			   <cfloop index="element" list="#listsort(sams.biz_type_string,'Text','ASC','~')#" delimiters="~">

				<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select biz_code_name from biz_code
				 where biz_code = '#element#'
				</cfquery>

				<cfoutput>
				#code.biz_code_name#<br>
				</cfoutput>
			   </cfloop>

		   </cfif>

			   </td>
			   <td class="feed_option" valign=top></td></tr>


				</table>

			</td></tr>

		   <tr><td colspan=4><hr></td></tr>

		   </table>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Primary Point of Contact</b></td></tr>
				<tr><td class="feed_option">#sams.poc_fnme# #sams.poc_lname#<br>
											<cfif #sams.poc_title# is not "">#sams.poc_title#<br></cfif>
											(#left(sams.poc_us_phone,3)#) #mid(sams.poc_us_phone,4,3)#-#right(sams.poc_us_phone,4)#<br>
											<a href="mailto:#sams.poc_email#">#sams.poc_email#</a></td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Alternate Point of Contact</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.alt_poc_lname# is not "">#sams.alt_poc_fname# #sams.alt_poc_lname#<br><cfelse>Not Provided</cfif>
											<cfif #sams.alt_poc_title# is not "">#sams.alt_poc_title#<br></cfif>
											<cfif #sams.akt_poc_phone# is not "">(#left(sams.akt_poc_phone,3)#) #mid(sams.akt_poc_phone,4,3)#-#right(sams.akt_poc_phone,4)#<br></cfif>
											<cfif #sams.alt_poc_email# is not ""><a href="mailto:#sams.alt_poc_email#">#sams.alt_poc_email#</a></cfif></td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Past Performance Point of Contact</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.pp_poc_lname# is not "">#sams.pp_poc_fname# #sams.pp_poc_lname#<br><cfelse>Not Provided</cfif>
											<cfif #sams.pp_poc_title# is not "">#sams.pp_poc_title#<br></cfif>
											<cfif #sams.pp_poc_phone# is not "">(#left(sams.pp_poc_phone,3)#) #mid(sams.pp_poc_phone,4,3)#-#right(sams.pp_poc_phone,4)#<br></cfif>
											<cfif #sams.pp_poc_email# is not ""><a href="mailto:#sams.pp_poc_email#">#sams.pp_poc_email#</a></cfif></td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Electronic Point of Contact</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.elec_bus_poc_lnmae# is not "">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#<br><cfelse>Not Provided</cfif>
											<cfif #sams.elec_bus_poc_title# is not "">#sams.elec_bus_poc_title#<br></cfif>
											<cfif #sams.elec_bus_poc_us_phone# is not "">(#left(sams.elec_bus_poc_us_phone,3)#) #mid(sams.elec_bus_poc_us_phone,4,3)#-#right(sams.elec_bus_poc_us_phone,4)#<br></cfif>
											<cfif #sams.elec_bus_poc_email# is not ""><a href="mailto:#sams.elec_bus_poc_email#">#sams.elec_bus_poc_email#</a></cfif></td></tr>
			</table>

			</td></tr>

		   <tr><td colspan=4><hr></td></tr>

		   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td valign=top width=50%>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>NAICS Codes</b></td></tr>
		   <tr><td class="feed_option" valign=top>

		   <cfif #sams.naisc_code_string# is "" or #sams.pri_naics# is "">

				No NAICS codes were found.

		   <cfelse>

			   <cfloop index="element" list="#listsort(sams.naisc_code_string,'Text','ASC','~')#" delimiters="~">

				<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select naics_code, naics_code_description from naics
				 where naics_code = '#left(element,'6')#'
				</cfquery>

				<cfoutput>
				<cfif #sams.pri_naics# is #left(element,'6')#>
				 <b>#left(element,'6')# - #code.naics_code_description#</b><br>
				<cfelse>
				 #left(element,'6')# - #code.naics_code_description#<br>
				</cfif>
				</cfoutput>
			   </cfloop>

			   <br>

			   <b><i>Bold - Indicates primary NAICS code</i></b>

		   </cfif>

		   </td></tr>

		   </table>

	   </td><td valign=top width=50%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_option"><b>Product or Service Codes</b></td></tr>
		<tr><td class="feed_option">

		   <cfif #sams.psc_code_string# is "">
			No Product or Service codes found.

		   <cfelse>

		   <cfloop index="psc_element" list="#listsort(sams.psc_code_string,'Text','ASC','~')#" delimiters="~">

			<cfquery name="psc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select psc_code, psc_description from psc
			 where psc_code = '#psc_element#'
			</cfquery>

			<cfoutput>
			 <b>#psc.psc_code#</b> - #psc.psc_description#<br>
			</cfoutput>

		   </cfloop>

	   </cfif>

	   </td></tr>

	  </table>

	</cfoutput>

</td></tr>

</table>

		<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select top(1) * from award_data
		 where recipient_duns = '#duns#'
		</cfquery>

		<cfif awards.recordcount is 0>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header">No Federal prime awards found.</td></tr>
        </table>

        <cfelse>

        <!--- Start Graphs --->

		<cfquery name="annual" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select sum(federal_action_obligation) as total, year(action_date) as year1 from award_data
				where recipient_duns = '#duns#'
				group by year(action_date)
				order by year(action_date)
		</cfquery>

		<cfquery name="award_summary_1" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select count(id) as total, avg(CAST(annual_revenue AS numeric)) as revenue, avg(CAST(number_of_employees AS int)) as employees, datepart(yyyy, [action_date]) as [year], count(distinct(id)) as awards, count(distinct(award_id_piid)) as contracts, count(distinct(awarding_sub_agency_code)) as sub_agencies, count(distinct(awarding_agency_code)) as agencies, sum(base_and_all_options_value) as all_options, sum(base_and_exercised_options_value) as options, sum(federal_action_obligation) as federal_action_obligation from award_data
	       where recipient_duns = '#duns#'
           group by datepart(yyyy, [action_date])
           order by [year]
		</cfquery>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td height=20></td></tr>
        <tr><td colspan=6><hr></td></tr>
        <tr><td height=20></td></tr>

        <tr><td colspan=3 class="feed_header">Federal Award Performance</td></tr>
        <tr><td height=20></td></tr>

         <tr>
              <td class="feed_sub_header" align=center>Annual Awards</td>
              <td width=50>&nbsp;</td>
              <td class="feed_sub_header" align=center>Reported Revenue</td>
              <td width=50>&nbsp;</td>
              <td class="feed_sub_header" align=center>Award Footprint / Diversity</td>
         </tr>

        <tr>

        <td valign=top width=33% align=center>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Awards'],
				<cfoutput query="annual">
				 ["#year1#", #round(total)#],
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 100, right: 0, width: '100%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Award Value',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual" style="width: 100%;"></div>

        </td><td width=30>&nbsp;</td><td valign=top width=33% align=center>

		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Revenue'],
				<cfoutput query="award_summary_1">
				<cfif revenue is "">
				 ["#year#", 0],
				<cfelse>
				 ["#year#", #round(revenue)#],
				</cfif>
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 100, right: 0, width: '100%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Revenue',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual_2'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual_2" style="width: 100%;"></div>

        </td><td width=30>&nbsp;</td><td valign=top width=33% align=center>

		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Agencies', 'Contracts','Awards'],
				<cfoutput query="award_summary_1">
				 ["#year#", #agencies#, #contracts#, #awards#],
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 80, right: 0, width: '100%'},
				legend: 'bottom',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Number',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual_3'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual_3" style="width: 100%;"></div>

          </td></tr>

        <tr><td colspan=5><hr></td></tr>

        </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td height=20></td></tr>
<tr><td class="feed_header">Federal Awards (Cummulative)</td></tr>
<tr><td height=20></td></tr>

<tr><td>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([

		<cfquery name="money" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select action_date, sum(federal_action_obligation) as obligated, sum(base_and_exercised_options_value) as total from award_data
		where recipient_duns = '#duns#'
		group by action_date
		order by action_date ASC
		</cfquery>

          ['Date', 'Obligated', 'Total Contract Value' ],

          <cfset column1 = 0>
          <cfset column2 = 0>

          <cfloop query="money">

          <cfif #money.obligated# is "">
           <cfset obg = 0>
          <cfelse>
           <cfset obg = #money.obligated#>
          </cfif>

          <cfif #money.total# is "">
           <cfset tot = 0>
          <cfelse>
           <cfset tot = #money.total#>
          </cfif>

           <cfset column1 = column1 + #obg#>
           <cfset column2 = column2 + #tot#>

          <cfoutput>
           ['#dateformat(money.action_date,'mm/dd/yyyy')#',
          </cfoutput>

           <cfoutput>
           #column1#,
           #column2#
           </cfoutput>

          ],

          </cfloop>

        ]);

        var options = {
          title: '',
          chartArea:{right: 30, left:70,top:20,width:'83%',height:'75%'},
          hAxis: { textStyle: {color: 'black', fontSize: 11}},
          vAxis: { textStyle: {color: 'black', fontSize: 11}},
          aggregationTarget: 'Obligated',
          legend: { position: 'bottom', textStyle: {fontSize: 10}}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('award_timeline'));
        chart.draw(data, options);
      }
    </script>

    <div id="award_timeline" style="width: 100%; height: 250px"></div>

          <tr><td><hr></td></tr>
        </table>



	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
	      <tr><td height=20></td></tr>
          <tr><td class="feed_header">Federal Award Timeline</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>

			<cfquery name="wins" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				SELECT sum(federal_action_obligation) as total, year(action_date), month(action_date), CAST(MONTH(action_date) AS VARCHAR(2)) + '-' + CAST(YEAR(action_date) AS VARCHAR(4)) AS win_date from award_data
				where recipient_duns = '#duns#'
				group by year(action_date), month(action_date), CAST(MONTH(action_date) AS VARCHAR(2)) + '-' + CAST(YEAR(action_date) AS VARCHAR(4))
				order by year(action_date), month(action_date)
			</cfquery>

				<script type="text/javascript">
				  google.charts.load('current', {'packages':['line']});
				  google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Year', 'Award Value'],
						  <cfoutput query="wins">
						   ['#dateformat(win_date,'yyyy/mm/dd')#',#round(total)#],
						  </cfoutput>
						]);


			  var options = {
					legend: { position: 'none' }
				  };

				  var chart = new google.charts.Line(document.getElementById('line_top_x'));
				  chart.draw(data, google.charts.Line.convertOptions(options));
				}
			  </script>

			<div id="line_top_x"></div>

          </td></tr>


          <tr><td><hr></td></tr>
        </table>


	    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td height=20></td></tr>
			<tr><td class="feed_header">Federal Award Analyzer</td></tr>
			<tr><td height=20></td></tr>

			   <tr><td valign=top class="feed_header" width=49% align=center>

			       <b>Department Footprint</b>

				   <!--- Department Footprint --->

				   <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					select top(15) awarding_agency_code, awarding_agency_name, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#'
					and federal_action_obligation > 0
					group by awarding_agency_code, awarding_agency_name
					order by total DESC
				   </cfquery>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('dept_graph'));

 						data.addColumn('string','Department');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="dept">
 						   ['#awarding_agency_name#',#round(total)#,'/exchange/include/award_detail.cfm?val=#awarding_agency_code#&id=#id#&t=1'],
 						  </cfoutput>
 						]);


						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="dept_graph" style="width: 90%;"></div>

                   </td><td width=50>&nbsp;</td><td class="feed_header" width=51% align=center>

                       <b>Agency Footprint</b>

					   <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						select top(15) awarding_sub_agency_code, awarding_sub_agency_name, sum(federal_action_obligation) as total from award_data
						where recipient_duns = '#duns#'
 					    and federal_action_obligation > 0
						group by awarding_sub_agency_code, awarding_sub_agency_name
						order by total DESC
					   </cfquery>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('agency_graph'));

 						data.addColumn('string','Department');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="agency">
 						   ['#awarding_sub_agency_name#',#round(total)#,'/exchange/include/award_detail.cfm?val=#awarding_sub_agency_code#&id=#id#&t=2'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="agency_graph" style="width: 90%;"></div>

                    </td>

                  </tr>

               <tr><td>&nbsp;</td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td>&nbsp;</td></tr>

			   <tr><td valign=top class="feed_header" width=49% align=center>

			       <b>Product or Services (Top 25)</b>

				   <!--- PSC --->

				   <cfquery name="psc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					select top(25) product_or_service_code, product_or_service_code_description, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#'
					and federal_action_obligation > 0
					group by product_or_service_code, product_or_service_code_description
					order by total DESC
				   </cfquery>


 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));

 						data.addColumn('string','Product or Service');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="psc">
 						   ['#product_or_service_code_description#',#round(total)#,'/exchange/include/award_detail.cfm?val=#product_or_service_code#&id=#id#&t=3'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="psc_graph" style="width: 90%;"></div>

                   </td><td width=50>&nbsp;</td><td class="feed_header" width=51% align=center>

                       <b>NAICS Code (Top 25)</b>

					   <cfquery name="naics" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
						select top(25) naics_code, naics_description, sum(federal_action_obligation) as total from award_data
						where recipient_duns = '#duns#'
					    and federal_action_obligation > 0
						group by naics_code, naics_description
						order by total DESC
					   </cfquery>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));

 						data.addColumn('string','NAICS Code');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="naics">
 						   ['#naics_description#',#round(total)#,'/exchange/include/award_detail.cfm?val=#naics_code#&id=#id#&t=4'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="naics_graph" style="width: 90%;"></div>

                    </td>

                  </tr>

               <tr><td>&nbsp;</td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td>&nbsp;</td></tr>

			   <tr><td valign=top class="feed_header" width=49% align=center>

			       <b>Place of Performance</b>

				   <cfquery name="place" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					select primary_place_of_performance_state_name, primary_place_of_performance_state_code, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#'
					and federal_action_obligation > 0
					group by primary_place_of_performance_state_name, primary_place_of_performance_state_code
					order by total DESC
				   </cfquery>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('place_graph'));

 						data.addColumn('string','State');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="place">
 						   ['#primary_place_of_performance_state_name#',#round(total)#,'/exchange/include/award_detail.cfm?val=#primary_place_of_performance_state_code#&id=#id#&t=5'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="place_graph" style="width: 90%;"></div>

	                </td><td width=50>&nbsp;</td><td class="feed_header" width=51% align=center>

			       <b>Contract Pricing</b>

				   <cfquery name="price" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					select type_of_contract_pricing, type_of_contract_pricing_code, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#'
					and federal_action_obligation > 0
					group by type_of_contract_pricing, type_of_contract_pricing_code
					order by total DESC
				   </cfquery>

 					<script type="text/javascript">
 					  google.charts.load('current', {'packages':['corechart']});
 					  google.charts.setOnLoadCallback(drawChart);

 					  function drawChart() {

 						var data = new google.visualization.DataTable();
 					    var chart = new google.visualization.PieChart(document.getElementById('price_graph'));

 						data.addColumn('string','Type of Pricing');
 						data.addColumn('number','Awards');
 						data.addColumn('string','Code');

 						data.addRows([
 						  <cfoutput query="price">
 						   ['#type_of_contract_pricing#',#round(total)#,'/exchange/include/award_detail.cfm?val=#type_of_contract_pricing_code#&id=#id#&t=6'],
 						  </cfoutput>
 						]);

						var options = {
						legend: 'labeled',
						title: '',
			            chartArea:{left: 0, right: 0, top:20,width:'93%',height:'75%'},
						pieHole: 0.4,
						height: 425,
						fontSize: 11,
						};

 				 google.visualization.events.addListener(chart, 'select', function () {
 					var selection = chart.getSelection();
 					if (selection.length > 0) {
 					  window.open(data.getValue(selection[0].row, 2), '_self');
 					  console.log(data.getValue(selection[0].row, 2));
 					}
 				  });

 				  function drawChart1() {
 					chart.draw(data, options);
 				  }
 				  drawChart1();

 					  }

 					</script>

				    <div id="price_graph" style="width: 90%;"></div>

       </cfif>

                    </td>

                  </tr>

        </table>

        <!--- End Graphs --->


























</div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>


