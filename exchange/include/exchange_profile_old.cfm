<cfquery name="view" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into cview
 (cview_by_usr_id, cview_company_id, cview_date)
 values
 (#session.usr_id#,#id#,#now()#)
</cfquery>

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="size" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from size
</cfquery>

<cfquery name="media" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from media
 where media_company_id = #id# and
       media_public = 1
 order by media_updated DESC
</cfquery>

<cfquery name="partners" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner
 where partner_company_id = #id# and
       partner_public = 1
</cfquery>

<cfquery name="customers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from customer
 where customer_company_id = #id# and
       customer_public = 1
 order by customer_end DESC
</cfquery>

<cfquery name="products" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from product
 where product_company_id = #id# and
       product_public = 1
</cfquery>

<cfquery name="documents" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from doc
 where doc_company_id = #id# and
       doc_public = 1
</cfquery>

<cfquery name="stage" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select stage.stage_name from company
 left join stage on company.company_stage_id = stage.stage_id
 where company.company_id = #id#
</cfquery>

<cfquery name="size" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select size.size_name from company
 left join size on company.company_size_id = size.size_id
 where company.company_id = #id#
</cfquery>

<cfquery name="registered" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = <cfif #company.company_admin_id# is "">0<cfelse>#company.company_admin_id#</cfif>
</cfquery>

<cfquery name="market" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select market_name from market, align
  where (market.market_id = align.align_type_value) and
		(align.align_company_id = #id#) and
		(align.align_type_id = 2)
  order by market.market_name
</cfquery>

<cfset market_list = #valuelist(market.market_name)#>

<cfquery name="sector" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select sector_name from sector, align
  where (sector.sector_id = align.align_type_value) and
		(align.align_company_id = #id#) and
		(align.align_type_id = 3)
  order by sector.sector_name
</cfquery>

<cfset sector_list = #valuelist(sector.sector_name)#>

<cfquery name="topic" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select topic_name from topic, align
  where (topic.topic_id = align.align_type_value) and
		(align.align_company_id = #id#) and
		(align.align_type_id = 4)
  order by topic.topic_name
</cfquery>

<cfset topic_list = #valuelist(topic.topic_name)#>

		   <cfoutput>

			<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from follow
			  where follow_company_id = #id# and
			        follow_by_usr_id = #session.usr_id#
			</cfquery>

		   <form action="action.cfm" method="post">

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header"><cfoutput>#company.company_name#</cfoutput></td>
               <td align=right>

               <cfif #follow.recordcount# is 0>
                <input class="button_blue" style="font-size: 11px; height: 25px; width: 80px;" type="submit" name="button" value="Follow">
               <cfelse>
                <input class="button_blue" style="font-size: 11px; height: 25px; width: 80px;" type="submit" name="button" value="Unfollow">
               </cfif>

                </td></tr>

           <input type="hidden" name="id" value=#id#>

           </form>

           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td>

	             <table cellspacing=0 cellpadding=0 border=0 width=100%>
                  <tr><td height=10></td></tr>

                  <cfif isdefined("u")>
                   <cfif u is 0>
                     <tr><td class="feed_option"><font color="red"><b>You have stopped following this company.</b></font></td></tr>
                   <cfelseif u is 1>
                     <tr><td class="feed_option"><font color="green"><b>You are now following this company.</b></font></td></tr>
                   </cfif>
                  </cfif>

                  <cfif #company.company_about# is not "">
                  <tr><td class="feed_option">#replace(company.company_about,"#chr(10)#","<br>","all")#</td></tr>
                  <cfelse>
                  <tr><td class="feed_option">Company description not provided.</td></tr>
                  </cfif>
                  <tr><td><hr></td></tr>
                  <tr><td>

                  <table cellspacing=0 cellpadding=0 border=0 width=100%>

					  <tr><td valign=top width=25%>

                      <table cellspacing=0 cellpadding=0 border=0 width=100%>

						  <tr><td class="feed_option"><b>Address</b></td></tr>
						  <tr><td class="feed_option">
						  <cfif #company.company_address_l1# is not "">#company.company_address_l1#<br></cfif>
						  <cfif #company.company_address_l2# is not "">#company.company_address_l2#<br></cfif>
						  #company.company_city#, #company.company_state#.  #company.company_zip#
						  </td></tr>

					  </table>

					  </td><td valign=top width=25%>

                      <table cellspacing=0 cellpadding=0 border=0 width=100%>

						  <tr><td class="feed_option"><b>Corporate Information</b></td></tr>
						  <tr><td class="feed_option">
						  <b>DUNS: </b><cfif #company.company_duns# is "">Unknown<cfelse>#company.company_duns#</cfif><br>
						  <b>Founded: </b><cfif #company.company_founded# is "">Unknown<cfelse>#company.company_founded#</cfif></td></tr>
					  </table>

					  </td><td valign=top width=25%>

                      <table cellspacing=0 cellpadding=0 border=0 width=100%>

						  <tr><td class="feed_option">&nbsp;</td></tr>

						  <tr><td class="feed_option">
						  <b>Size: </b><cfif #size.size_name# is "">Unknown<cfelse>#size.size_name#</cfif><br>
						  <b>Type: </b>

						  <cfif #company.company_entity_id# is "">
						  Unknown
						  <cfelse>

							<cfquery name="entity" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
							 select * from entity
							 where entity_id = #company.company_entity_id#
							</cfquery>

						     #entity.entity_name#

						  </cfif><br>

						  <b>Website: </b><a href="#company.company_website#" target="_blank" rel="noopener" rel="noreferrer">#company.company_website#</a></b>

                          </td></tr>

					  </table>

					  </td><td valign=top width=25%>


                      <table cellspacing=0 cellpadding=0 border=0 width=100%>

						  <tr><td class="feed_option">&nbsp;</td></tr>

						  <tr><td class="feed_option">
						  <b>Last Updated: </b>#dateformat(company.company_updated,'mm/dd/yyyy')# at #timeformat(company.company_updated)#<br>
						  <b>Registered: </b>#dateformat(company.company_registered,'mm/dd/yyyy')#<br>
						  <b>Registered By: </b>#registered.usr_first_name# #registered.usr_last_name#<br>
						  <b>Email: </b>#registered.usr_email#</b>

                          </td></tr>

					  </table>

					  </td></tr>

                  </table>

                  </td></tr>

                  <tr><td>

                  <table cellspacing=0 cellpadding=0 border=0 width=100%>

                  <tr>
                      <td class="feed_option" width=25%><b>Company Point of Contact</b></td>
                      <td class="feed_option" width=25%><b>Company History</b></td>
                      <td class="feed_option" width=25%><b>Leadership Team</b></td>
                      <td class="feed_option" width=25%><b>Revenue</b></td></tr>

                  <tr>
                      <td class="feed_option" valign=top>

                      <cfif #company.company_poc_first_name# is "" and #company.company_poc_last_name# is "">
                       Unknown
                      <cfelse>
                      #company.company_poc_first_name# #company.company_poc_last_name#<br>
                                                         #company.company_poc_title#<br>
                                                         #company.company_poc_phone#<br>
                                                         #company.company_poc_email#<br>
                      </cfif>
                      </td>
                      <td class="feed_option" valign=top><cfif #company.company_history# is "">Not Disclosed<cfelse>#replace(company.company_history,"#chr(10)#","<br>","all")#</cfif></td>
                      <td class="feed_option" valign=top><cfif #company.company_leadership# is "">Not Disclosed<cfelse>#replace(company.company_leadership,"#chr(10)#","<br>","all")#</cfif></td>
                      <td class="feed_option" valign=top>
                      <b>FY18: &nbsp;</b><cfif #company.company_fy18_revenue# is "">Not Disclosed<cfelse>#numberformat(company.company_fy18_revenue,'$999,999,999')#</cfif><br>
                      <b>FY17: &nbsp;</b><cfif #company.company_fy17_revenue# is "">Not Disclosed<cfelse>#numberformat(company.company_fy17_revenue,'$999,999,999')#</cfif><br>
                      <b>FY16: &nbsp;</b><cfif #company.company_fy16_revenue# is "">Not Disclosed<cfelse>#numberformat(company.company_fy16_revenue,'$999,999,999')#</cfif>

                      </td></tr>

                  <tr><td colspan=3 class="feed_option"><b>Company Keywords</b></td></tr>
                  <tr><td colspan=3 class="feed_option"><cfif #company.company_keywords# is "">Not provided.<cfelse>#company.company_keywords#</cfif></td></tr>

                  </table>

                  </td></tr>

                  <tr><td><hr></td></tr>

				</table>

           </td></tr>

          </cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Markets, Sectors & Capabilities</a></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>


			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr>
                    <td valign=top width=25% class="feed_option"><b>Markets</b></td>
                    <td valign=top width=25% class="feed_option"><b>Sectors</b></td>
                    <td valign=top width=25% class="feed_option"><b>Capabilities & Services</b></td>
                </tr>
                <tr>
                    <cfoutput>
						<td valign=top class="text_small"><cfif #market_list# is "">Unknown<cfelse><cfloop index="melement" list="#market_list#"><cfoutput>#melement#</cfoutput><br></cfloop></cfif></td>
						<td valign=top class="text_small"><cfif #sector_list# is "">Unknown<cfelse><cfloop index="selement" list="#sector_list#"><cfoutput>#selement#</cfoutput><br></cfloop></cfif></td>
						<td valign=top class="text_small"><cfif #topic_list# is "">Unknown<cfelse><cfloop index="telement" list="#topic_list#"><cfoutput>#telement#</cfoutput><br></cfloop></cfif></td>
                    </cfoutput>
                 </tr>

                 </table>

            </td></tr>
            <tr><td><hr></td></tr>
           </table>

         <!--- Custom Fields - Start --->

          <cfif isdefined("session.hub")>

		  <cfquery name="check_company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
            select hub_xref_usr_id from hub_xref
            join company on company_admin_id = hub_xref_usr_id
            where company_id = #id# and
                  hub_xref_hub_id = #session.hub#
          </cfquery>

          <cfif check_company.recordcount GT 0>

		  <cfquery name="sections" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from section
			where section_hub_id = #session.hub#
			and section_access = 0
			order by section_order
		  </cfquery>

 		  <cfif sections.recordcount GT 0>

 		  <cfloop query="sections">

		  <cfquery name="fields" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from section_field
			 left join section_field_value on section_field_value_field_id = section_field_id
			 where section_field_section_id = #sections.section_id# and
			       section_field_value_company_id = #id# and
				   section_field_access = 0
			 order by section_field_order
		  </cfquery>

 		  <cfoutput>

 			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
 				<tr><td class="feed_header">#sections.section_name#</a></td></tr>

 				<tr><td>

 			    <table cellspacing=0 cellpadding=0 border=0 width=100%>
 			     <tr><td height=10></td></tr>

			     <cfif fields.recordcount is 0>
			      <tr><td class="feed_option">No information has been added.</td></tr>
			     </cfif>

 			     <cfloop query="fields">
 			     <cfoutput>
 			      <tr><td class="feed_option"><b>#fields.section_field_name#</b></td></tr>

                   <!--- Text Field --->

                   <cfif fields.section_field_type is 1>
                    <cfif fields.section_field_value_text is "">
                     <tr><td class="feed_option">Not provided.</td></tr>
                    <cfelse>
                     <tr><td class="feed_option">#fields.section_field_value_text#</td></tr>
                    </cfif>

                   <!-- Memo Field --->

                   <cfelseif fields.section_field_type is 2>
                    <cfif fields.section_field_value_textarea is "">
                     <tr><td class="feed_option">Not provided.</td></tr>
                    <cfelse>
                     <tr><td class="feed_option">#fields.section_field_value_textarea#</td></tr>
                    </cfif>

                   </cfif>

 			     </cfoutput>

 			     <tr><td height=10></td></tr>
 			     </cfloop>

 			    </table>

 				</td>
 				</tr>

 				<tr><td><hr></td></tr>
 			   </table>

                </cfoutput>

           </cfloop>

           </cfif>

           </cfif>

           </cfif>

           <!--- Custom Fields - End --->

           <!--- Documents --->

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Marketing Material</a></td></tr>
            <tr><td>

            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #documents.recordcount# is 0>
             <tr><td class="feed_option">No documents have been uploaded.</td></tr>
            </cfif>
            <tr><td height=5></td></tr>

            <cfoutput query="documents">
             <tr><td class="feed_option" valign=top><b>#documents.doc_name#</b></td></tr>

             <tr><td colspan=2 class="text_xsmall" valign=top>#doc_description#</td></tr>

             <cfif #doc_attachment# is not "">
              <tr><td height=5></td></tr>
              <tr><td colspan=2 class="text_xsmall"><a href="#media_virtual#/#doc_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a></td></tr>
             </cfif>

             <tr><td>&nbsp;</td></tr>
            </cfoutput>
            </table>
            </td></tr>
            <tr><td><hr></td></tr>
           </table>


           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Customer Snapshots</a></td></tr>
            <tr><td>
            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #customers.recordcount# is 0>
             <tr><td class="feed_option">No customer snapshots have been entered.</td></tr>
            </cfif>
            <tr><td height=5></td></tr>

            <cfoutput query="customers">
             <tr>
                <td class="feed_option" valign=top><b>#customers.customer_name#</b>

                <cfif #customers.customer_deal# is not "">
                 <i>&nbsp;( #numberformat(customers.customer_deal,'$999,999,999,999')# )</i>
                </cfif>


                </td>
                <td valign=top class="feed_option" align=right><b>#dateformat(customers.customer_start,'mm/yyyy')# - #dateformat(customers.customer_end,'mm/yyyy')#</b></td></tr>

             <tr><td colspan=2 class="text_xsmall" valign=top>#replace(customers.customer_work,"#chr(10)#","<br>","all")#</td></tr>
            </cfoutput>
            </table>
            </td></tr>
            <tr><td><hr></td></tr>
           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Products & Services</a></td></tr>
            <tr><td>
            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #products.recordcount# is 0>
             <tr><td class="feed_option">No products have been entered.</td></tr>
            </cfif>
            <tr><td height=5></td></tr>

            <cfoutput query="products">
             <tr><td class="feed_option" valign=top width=200><b>#products.product_name#</b></td>
                 <td class="feed_option" align=right><b>#products.product_type#</b></td></tr>
             <tr><td class="text_xsmall" valign=top>#replace(products.product_desc,"#chr(10)#","<br>","all")#</td></tr>
             <tr><td class="text_xsmall"><a href="#products.product_url#" target="_blank" rel="noopener" rel="noreferrer"><i>#products.product_url#</i></a></td></tr>
            </cfoutput>
            </table>
            </td></tr>
            <tr><td><hr></td></tr>
           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Partners & Alliances</a></td></tr>
            <tr><td>
            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #partners.recordcount# is 0>
             <tr><td class="feed_option">No partners have been entered.</td></tr>
            </cfif>
            <tr><td height=5></td></tr>
            <cfoutput query="partners">
             <tr><td class="feed_option"><b>#partners.partner_name#</b></td>
                 <td class="feed_option" align=right><b>
                 <cfif #partners.partner_type_id# is 1>Strategic
                 <cfelseif #partners.partner_type_id# is 2>Business
                 <cfelseif #partners.partner_type_id# is 3>Technical
                 <cfelseif #partners.partner_type_id# is 4>Operational
                 </cfif>
                 </b></td></tr>
            </cfoutput>
            </table>
            </td></tr>
            <tr><td><hr></td></tr>
           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header">Press & Media Coverage</a></td></tr>
            <tr><td>
            <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfif #media.recordcount# is 0>
             <tr><td class="feed_option">No press or media information has been entered.</td></tr>
            </cfif>
            <tr><td height=5></td></tr>

            <cfoutput query="media">
             <tr><td class="feed_option"><b>#media.media_name#</b></td>
                 <td class="feed_option" align=right><cfif #media.media_date# is not ""><b>#dateformat(media.media_date,'mm/dd/yyyy')#</b></cfif></td></tr>
             <tr><td class="text_xsmall" colspan=2>#replace(media.media_desc,"#chr(10)#","<br>","all")#</td></tr>
             <tr><td class="text_xsmall"><cfif #media.media_source# is not ""><b>#media.media_source#</b><br></cfif><a href="#media.media_url#" target="_blank" rel="noopener" rel="noreferrer"><i>#media.media_url#</i></a></td></tr>
             <tr><td height=5></td></tr>
           </cfoutput>
            </table>
            </td></tr>
            <tr><td><hr></td></tr>
           </table>

			<cfif #company.company_duns# is not "">
			 <cfset #duns# = #company.company_duns#>
			 <cfinclude template="sams_profile.cfm">
			</cfif>

              </td></tr>

           </table>

            </td></tr>
          </table>

        </td></tr>
       </table>

      </td></tr>
     </table>
