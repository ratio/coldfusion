<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from comments_rating
 where comments_rating_hub_id = #session.hub#
 order by comments_rating_value DESC
</cfquery>

<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company_intel
 where company_intel_id = #company_intel_id# and
 company_intel_created_by_usr_id = #session.usr_id#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td class="feed_header" style="font-size: 30;" valign=middle>Edit Company Comments</td>
   <td align=right class="feed_sub_header"><a href="profile.cfm?l=30">Return</a></td></tr>
<tr><td colspan=2><hr></td></tr>
<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>Company comments allows you to add information about this company and choose who you share it with.</td></tr>
<tr><td height=5></td></tr>

</table>

<cfoutput>

   <form action="company_intel_save.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td class="feed_sub_header" width=200><b>Context</b></td>
			<td><input name="company_intel_context" class="input_text" value="#intel.company_intel_context#" type="text" size=100 maxlength="1000" required placeholder="What's the context of writing these comments?"></td>
		</tr>

		<tr><td class="feed_sub_header" valign=top><b>Description</b></td>
			<td><textarea name="company_intel_comments" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  rows=6 cols=101 placeholder="Please provide any comments related to this intel.">#intel.company_intel_comments#</textarea></td>
		</tr>

		<tr><td class="feed_sub_header"><b>Reference URL</b></td>
			<td><input name="company_intel_url" class="input_text" type="url" size=100 maxlength="1000" value="#intel.company_intel_url#"></td>
		</tr>

		<tr><td height=10></td></tr>

		<tr><td class="feed_sub_header" valign=top>Attachment</td>
			<td class="feed_sub_header" style="font-weight: normal;">

			<cfif #intel.company_intel_attachment# is "">
			  <input type="file" id="image" onchange="validate_img()" name="company_intel_attachment">
			<cfelse>
			  <a href="#media_virtual#/#intel.company_intel_attachment#" width=150>#intel.company_intel_attachment#</a><br><br>
			  <input type="file" id="image" onchange="validate_img()" name="company_intel_attachment"><br><br>
			  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove Attachment
			 </cfif>

			 </td></tr>

		<tr>

		<input type="hidden" name="company_intel_id" value=#intel.company_intel_id#>

</cfoutput>

		<td class="feed_sub_header">Rating</td>
		<td><select name="company_intel_rating" class="input_select">
		<cfoutput query="rating">
		 <option value=#comments_rating_id# <cfif #intel.company_intel_rating# is #comments_rating_value#>selected</cfif>>#comments_rating_name#
		</cfoutput>
		</select>

		</td></tr>

		<tr>
		<td class="feed_sub_header">Sharing</td>
		<td><select name="company_intel_sharing" class="input_select">
		 <option value=0 <cfif intel.company_intel_sharing is 0>selected</cfif>>Keep private, only me
		 <option value=1 <cfif intel.company_intel_sharing is 1>selected</cfif>>Share with my Company
		</select>

		</td></tr>

		<tr><td height=10></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>

		<tr><td></td><td>

              <input type="submit" class="button_blue_large" name="button" value="Update Comment">&nbsp;&nbsp;
		      <input class="button_blue_large" type="submit" name="button" value="Delete Comment" vspace=10 onclick="return confirm('Delete Comment?\r\nAre you sure you want to delete this comment?');">

		</td></tr>

	   </table>

	   </form>

	   </td></tr>

  </table>




