
		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr><td colspan=10><hr></td></tr>

			   <tr><td>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=8></td></tr>

			   <tr><td class="feed_header" colspan=10><a name=grants><b>Federal Grants</b></a></td></tr>
			   <tr><td height=5></td></tr>

			   <tr><td colspan=10>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<!--- SBIR --->

					<cfquery name="grants" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from grants
					 where recipient_duns = '#company.company_duns#'
					 order by action_date DESC
					</cfquery>

			        <cfif subscription.premium_grants is 1 or (isdefined("session.hub") and hub_subscription.premium_grants is 1)>

					<cfif grants.recordcount is 0>

					 <tr><td class="feed_option">No Federal Grants were found.</td></tr>

					<cfelse>

					<cfoutput>

								<tr>
								   <td class="text_xsmall" width=100><b>Award ID</b></td>
								   <td class="text_xsmall"><b>Department</b></td>
								   <td class="text_xsmall"><b>Agency</b></td>
								   <td class="text_xsmall"><b>Title</b></td>
								   <td class="text_xsmall"><b>Start Date</b></td>
								   <td class="text_xsmall"><b>End Date</b></td>
								   <td class="text_xsmall" align=right><b>Award Amount</b></td>
								</tr>

					</cfoutput>

								<tr><td height=5></td></tr>

								<cfset #counter# = 0>
								<cfset #sc_total# = 0>

								<cfoutput query="grants">

								 <cfif counter is 0>
								  <tr bgcolor="ffffff">
								 <cfelse>
								  <tr bgcolor="e0e0e0">
								 </cfif>

									 <td class="text_xsmall"><a href="/exchange/include/grant_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_fain#</a></td>
								     <td class="text_xsmall">#awarding_agency_name#</td>
									 <td class="text_xsmall">#awarding_sub_agency_name#</td>
									 <td class="text_xsmall">#cfda_title#</td>
									 <td class="text_xsmall" width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" width=75>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" align=right width=100>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>

								 </tr>


								 <cfif counter is 0>
								  <cfset counter = 1>
								 <cfelse>
								  <cfset counter = 0>
								 </cfif>
								 <cfset #sc_total# = #sc_total# + #federal_action_obligation#>

								</cfoutput>

								<tr><td height=5></td></tr>
								<tr><td class="feed_option" colspan=6><b>Total</b></td>
									<td class="feed_option" align=right><b><cfoutput>#numberformat(sc_total,'$999,999,999,999')#</cfoutput></b></td></tr>

				   </cfif>

				   <cfelse>

					   <tr><td class="feed_option" colspan=7><b>Total Records Found - <cfoutput>#grants.recordcount#</cfoutput> Contracts</b></td>
						   <td class="feed_option" align=right></tr>
					   <tr><td class="feed_option">Requires premium access.</td></tr>

				   </cfif>

				   </table>

			   </td></tr>
</td></tr>
</table>

</td></tr>
</table>