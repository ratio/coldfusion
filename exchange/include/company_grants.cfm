<table cellspacing=0 cellpadding=0 border=0 width=100%>

   <cfoutput>
   <tr><td class="feed_header" style="font-size: 30;">Grants</td>
       <td class="feed_sub_header" style="font-weight: normal;" align=right>
       	<form action="filter.cfm" method="post">
	   		<td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter</b>&nbsp;&nbsp;
	   		<input class="input_text" type="text" size=30 name="filter" placeholder="enter keyword" <cfif isdefined("session.profile_filter")>value='#replace(session.profile_filter,'"','','all')#'</cfif>>&nbsp;&nbsp;
	   		<input class="button_blue" type="submit" name="button" value="Apply">&nbsp;
	   		<input class="button_blue" type="submit" name="button" value="Clear">
	   		<input type="hidden" name="location" value="Grants">
	   		</td>
	    </form>
        </td>
        </tr>
   </cfoutput>
   <tr><td colspan=3><hr></td></tr>

</table>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from grants
	 where (recipient_duns = '#session.company_profile_duns#' or recipient_parent_duns = '#session.company_profile_duns#')

	<cfif isdefined("session.profile_filter")>
	 and contains((cfda_title, awarding_agency_name, awarding_sub_agency_name, award_description),'#trim(session.profile_filter)#')
	</cfif>

	 order by action_date DESC
	</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
           </cfif>

					<cfif agencies.recordcount is 0>

					 <tr><td class="feed_sub_header">No Grants were found.</td></tr>

					<cfelse>

					<cfoutput>

					 <tr><td class="feed_sub_header" colspan=5>Grants Found - #numberformat(agencies.recordcount,'999,999')#</td>
					     <td class="feed_sub_header" colspan=3 align=right>
					     <a href="/exchange/include/profile.cfm?l=13&export=1"><img src="/images/icon_export_excel.png" alt="Export to Excel" title="Export to Excel" width=23 hspace=10 border=0 align=absmiddle></a>
					     <a href="/exchange/include/profile.cfm?l=13&export=1">Export to Excel</a>

					</td>

					 </tr>

								<tr height=60>
								   <td class="feed_option" width=100><b>AWARD ##</b></td>
								   <td class="feed_option"><b>TITLE</b></td>
								   <td class="feed_option"><b>DEPARTMENT / AGENCY</b></td>
								   <td class="feed_option"><b>GRANTEE</b></td>
								   <td class="feed_option"><b>START DATE</b></td>
								   <td class="feed_option"><b>END DATE</b></td>
								   <td class="feed_option" align=right><b>AMOUNT</b></td>
								</tr>

					</cfoutput>

								<tr><td height=5></td></tr>

								<cfset #counter# = 0>
								<cfset #sc_total# = 0>

								<cfoutput query="agencies">

								 <cfif counter is 0>
								  <tr bgcolor="ffffff" height=30>
								 <cfelse>
								  <tr bgcolor="f0f0f0" height=30>
								 </cfif>

									 <td class="text_xsmall" style="padding-right: 20px;"><a href="/exchange/include/grant_information.cfm?id=#id#" target="_blank" rel="noopener"><b>#award_id_fain#</b></a></td>
									 <td class="text_xsmall" style="padding-right: 20px;">#cfda_title#</td>
								     <td class="text_xsmall" style="padding-right: 20px;">#awarding_agency_name#<br>#awarding_sub_agency_name#</td>
								     <td class="text_xsmall" style="padding-right: 20px;" width=250>#recipient_name#</td>
									 <td class="text_xsmall" style="padding-right: 20px;" width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" style="padding-right: 20px;" width=75>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" align=right width=100>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>

								 </tr>


								 <cfif counter is 0>
								  <cfset counter = 1>
								 <cfelse>
								  <cfset counter = 0>
								 </cfif>

								</cfoutput>


				   </cfif>


				   </table>

			   </td></tr>
	</td></tr>
</table>