<cfinclude template="/exchange/security/check.cfm">

<cfquery name="dept" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select top(1) awarding_agency_name from award_data
 where awarding_agency_code = '#val#'
</cfquery>

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select company_duns, company_name from company
 where company_id = #id#
</cfquery>

<cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sams
 where duns = '#company.company_duns#'
</cfquery>

<cfset duns = #company.company_duns#>

<html>
<head>
	<title><cfoutput>#sams.legal_business_name# - #dept.awarding_agency_name# Dashboard</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body" bgcolor="afafaf">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		   <cfquery name="stats" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select count(id) as awards, min(action_date) as min, max(action_date) as max, count(distinct(award_id_piid)) as contracts, sum(base_and_all_options_value) as all_options, sum(base_and_exercised_options_value) as options, sum(federal_action_obligation) as obligated from award_data
			 where recipient_duns = '#duns#' and
				   awarding_agency_code = '#val#'
		   </cfquery>

		   <cfquery name="agency" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select top(15) awarding_sub_agency_code, awarding_sub_agency_name, sum(federal_action_obligation) as total from award_data
			where recipient_duns = '#duns#' and
				  awarding_agency_code = '#val#'
			and federal_action_obligation > 0
			group by awarding_sub_agency_code, awarding_sub_agency_name
			order by total DESC
		   </cfquery>

		   <cfquery name="active" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select count(id) as total from award_data
			 where recipient_duns = '#duns#' and
				   awarding_agency_code = '#val#' and
				   period_of_performance_current_end_date >= #now()#
		   </cfquery>

					   <cfquery name="naics" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						select naics_code, naics_description, sum(federal_action_obligation) as total from award_data
						where recipient_duns = '#duns#' and
						      awarding_agency_code = '#val#'
					    and federal_action_obligation > 0
						group by naics_code, naics_description
						order by total DESC
					   </cfquery>

				   <cfquery name="psc" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					select product_or_service_code, product_or_service_code_description, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#' and
					      awarding_agency_code = '#val#'
					and federal_action_obligation > 0
					group by product_or_service_code, product_or_service_code_description
					order by total DESC
				   </cfquery>

				   <cfquery name="price" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					select type_of_contract_pricing, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#' and
					      awarding_agency_code = '#val#'
					and federal_action_obligation > 0
					group by type_of_contract_pricing
					order by total DESC
				   </cfquery>


           <cfoutput>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_header" valign=top>#company.company_name#</a></td>
					<td class="feed_option" align=right><input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="button" value="Close" onclick="windowClose();"></td></tr>
				<tr><td height=5></td></tr>
				<tr><td colspan=2><hr></td></tr>
			   </table>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td height=5></td></tr>
				<tr><td class="feed_header" colspan=2>#dept.awarding_agency_name# - Dashboard</td></tr>
				<cfoutput>
				<tr><td height=10></td></tr>
					<tr>
						<td class="feed_option">
			            <table cellspacing=0 cellpadding=0 border=0 width=100%>

				         <tr>
				            <td class="feed_option" width=200><b>Total Contracts:</b></td>
				            <td class="feed_option">#numberformat(stats.contracts,'999,999')# (#active.total# Active Now)</td
				         </tr>

				         <tr>
				            <td class="feed_option" width=175><b>Total Contract Awards & Mods:</b></td>
				            <td class="feed_option">#numberformat(stats.awards,'999,999')#</td
				         </tr>
				         <tr>
				             <td class="feed_option"><b>Total Obligations:</b></td>
				             <td class="feed_option">#numberformat(stats.obligated,'$999,999,999,999')#</td>
				         </tr>

				         <tr>
				             <td class="feed_option"><b>Total Base & Options:</b></td>
				             <td class="feed_option">#numberformat(stats.options,'$999,999,999,999')#</td>
				         </tr>

				         <tr>
				             <td class="feed_option"><b>Total Base & All Options:</b></td>
				             <td class="feed_option">#numberformat(evaluate(stats.obligated+stats.options),'$999,999,999,999')#</td>
				         </tr>

				        </table>
						</td><td valign=top>


			            <table cellspacing=0 cellpadding=0 border=0 width=100%>

				         <tr>
				            <td class="feed_option" width=175><b>Total Agencies Supported:</b></td>
				            <td class="feed_option">#numberformat(agency.recordcount,'999,999')#</td
				         </tr>

				         <tr>
				            <td class="feed_option" width=175><b>Total NAICS Code Delivered:</b></td>
				            <td class="feed_option">#numberformat(naics.recordcount,'999,999')#</td
				         </tr>
				         <tr>
				             <td class="feed_option"><b>Total PSC Codes Delivered:</b></td>
				             <td class="feed_option">#numberformat(psc.recordcount,'999,999')#</td>
				         </tr>

				         <tr>
				            <td class="feed_option" width=175><b>First Award:</b></td>
				            <td class="feed_option">#dateformat(stats.min,'mm/dd/yyyy')#</td
				         </tr>
				         <tr>
				             <td class="feed_option"><b>Last Award:</b></td>
				            <td class="feed_option">#dateformat(stats.max,'mm/dd/yyyy')#</td
				         </tr>

				        </table>

						</td>

					</tr>
                </cfoutput>

				<tr><td height=20></td></tr>

				<tr><td colspan=4><hr></td></tr>
				</table>
        </cfoutput>

        <!--- Start Graphs --->

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header">Award Timeline</td></tr>
          <tr><td>&nbsp;</td></tr>
          <tr><td>

			<cfquery name="wins" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				SELECT sum(federal_action_obligation) as total, year(action_date), month(action_date), CAST(MONTH(action_date) AS VARCHAR(2)) + '-' + CAST(YEAR(action_date) AS VARCHAR(4)) AS win_date from award_data
				where recipient_duns = '#duns#' and
				      awarding_agency_code = '#val#'
				group by year(action_date), month(action_date), CAST(MONTH(action_date) AS VARCHAR(2)) + '-' + CAST(YEAR(action_date) AS VARCHAR(4))
				order by year(action_date), month(action_date)
			</cfquery>

			 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">
				  google.charts.load('current', {'packages':['line']});
				  google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Year', 'Award Value'],
						  <cfoutput query="wins">
						   ['#win_date#',#round(total)#],
						  </cfoutput>
						]);


			  var options = {
					width: 1300,
					height: 200,
					legend: { position: 'none' }
				  };

				  var chart = new google.charts.Line(document.getElementById('line_top_x'));
				  chart.draw(data, google.charts.Line.convertOptions(options));
				}
			  </script>

			<div id="line_top_x"></div>

          </td></tr>


          <tr><td><hr></td></tr>
        </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td class="feed_sub_header">Award Value Timeline</td></tr>

<tr><td>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([

		<cfquery name="money" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		select action_date, sum(federal_action_obligation + base_and_exercised_options_value) as obligated, sum(base_and_all_options_value) as total from award_data
		where recipient_duns = '#duns#' and
		      awarding_agency_code = '#val#'
		group by action_date
		order by action_date ASC
		</cfquery>

          ['Date', 'Obligated', 'Total Contract Value' ],

          <cfset column1 = 0>
          <cfset column2 = 0>

          <cfloop query="money">

           <cfset column1 = column1 + #money.obligated#>
           <cfset column2 = column2 + #money.total#>

          <cfoutput>
           ['#dateformat(money.action_date,'mm/dd/yyyy')#',
          </cfoutput>

           <cfoutput>
           #column1#,
           #column2#
           </cfoutput>

          ],

          </cfloop>

        ]);

        var options = {
          title: '',
          chartArea:{right: 30, left:70,top:20,width:'83%',height:'75%'},
          hAxis: { textStyle: {color: 'black', fontSize: 11}},
          vAxis: { textStyle: {color: 'black', fontSize: 11}},
          aggregationTarget: 'Obligated',
          legend: { position: 'bottom', textStyle: {fontSize: 10}}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('award_timeline'));
        chart.draw(data, options);
      }
    </script>

    <div id="award_timeline" style="width: 1300px; height: 300px"></div>

          <tr><td><hr></td></tr>
        </table>


	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
	           <tr><td>&nbsp;</td></tr>

	               <tr><td class="feed_sub_header" align=center valign=top>

                       <b>Agency Footprint</b>

						<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
						<script type="text/javascript">
						  google.charts.load('current', {'packages':['corechart']});
						  google.charts.setOnLoadCallback(drawChart);

						  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Agency', 'Award Value'],
						  <cfoutput query="agency">
						   ['#awarding_sub_agency_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'left',
						title: '',
			            chartArea:{top:20,width:'100%',height:'100%'},
						pieHole: 0.4,
						width: 400,
						height: 300,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('agency_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="agency_graph"></div>

                    </td>

                   <td valign=top class="feed_sub_header" align=center>

			       <b>Product or Services</b>

				   <!--- PSC --->

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="psc">
						   ['#product_or_service_code_description#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'left',
						title: '',
			            chartArea:{top:20,width:'100%',height:'100%'},
						pieHole: 0.4,
						width: 400,
						height: 300,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('psc_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="psc_graph"></div>

                   </td><td class="feed_sub_header" align=center valign=top>

                       <b>NAICS Code</b>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Department', 'Award Value'],
						  <cfoutput query="naics">
						   ['#naics_description#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'left',
						title: '',
			            chartArea:{top:20,width:'100%',height:'100%'},
						pieHole: 0.4,
						width: 400,
						height: 300,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('naics_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="naics_graph"></div>

                    </td>

                  </tr>

               <tr><td colspan=3><hr></td></tr>

			   <tr><td valign=top class="feed_sub_header" align=center>

			       <b>Place of Performance</b>

				   <cfquery name="place" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					select primary_place_of_performance_state_name, sum(federal_action_obligation) as total from award_data
					where recipient_duns = '#duns#' and
					      awarding_agency_code = '#val#'
					and federal_action_obligation > 0
					group by primary_place_of_performance_state_name
					order by total DESC
				   </cfquery>


					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['State', 'Award Value'],
						  <cfoutput query="place">
						   ['#primary_place_of_performance_state_name#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'left',
						title: '',
			            chartArea:{top:20,width:'100%',height:'100%'},
						pieHole: 0.4,
						width: 400,
						height: 300,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('place_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="place_graph"></div>

                   </td><td class="feed_sub_header" align=center>

			       <b>Contract Pricing</b>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Type of Pricing', 'Award Value'],
						  <cfoutput query="price">
						   ['#type_of_contract_pricing#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'left',
						title: '',
			            chartArea:{top:20,width:'100%',height:'100%'},
						pieHole: 0.4,
						width: 400,
						height: 300,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('price_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="price_graph"></div>


                    </td>

                    <td valign=top class="feed_sub_header" align=center>

			       <b>Subcontractors (Top 25)</b>

				   <cfquery name="sub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					select top(25) subawardee_duns, subawardee_name, sum(subaward_amount) as total from award_data_sub
					where prime_awardee_duns = '#duns#' and
					      prime_awarding_agency_code = '#val#'
					group by subawardee_duns, subawardee_name
					order by total DESC
				   </cfquery>

					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<script type="text/javascript">
					  google.charts.load('current', {'packages':['corechart']});
					  google.charts.setOnLoadCallback(drawChart);

					  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Subcontractor', 'Award Value'],
						  <cfoutput query="sub">
						   ['#replaceNoCase(subawardee_name,"'","","all")#',#round(total)#],
						  </cfoutput>
						]);

						var options = {
						legend: 'left',
						title: '',
			            chartArea:{top:20,width:'100%',height:'100%'},
						pieHole: 0.4,
						width: 400,
						height: 300,
						fontSize: 8,
						};

					var chart = new google.visualization.PieChart(document.getElementById('sub_graph'));
					chart.draw(data, options);

					  }
					</script>

				    <div id="sub_graph"></div>

                    </td>

                  </tr>

        </table>

        <!--- End Graphs --->

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

