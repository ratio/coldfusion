function validate_img() {
 var fileName = document.getElementById("image").value;
 var dot = fileName.lastIndexOf(".") + 1;
 var extFile = fileName.substr(dot, fileName.length).toLowerCase();
 if (extFile == "jpg" ||
     extFile == "jpeg" ||
     extFile == "svg" ||
     extFile == "png" ||
     extFile == "gif"
      ){
  //accepted
  return true;
 } else {
  alert("Only image files are accepted (jpg, svg, png, gif).");
  document.getElementById("image").value = null;
  return false;
 }
}

function validate_doc() {
 var fileName = document.getElementById("document").value;
 var dot = fileName.lastIndexOf(".") + 1;
 var extFile = fileName.substr(dot, fileName.length).toLowerCase();
 if (extFile == "doc" ||
     extFile == "ppt" ||
     extFile == "jpg" ||
     extFile == "jpeg" ||
     extFile == "svg" ||
     extFile == "txt" ||
     extFile == "png" ||
     extFile == "gif" ||
     extFile == "xls" ||
     extFile == "pdf" ||
     extFile == "xlsx"
      ){
  //accepted
 } else {
  alert("Only image and document files are accepted (jpg, svg, png, gif, doc, ppt, xls, pdf).");
  document.getElementById("document").value = null;
  return false;
 }
}

function isAlphaNum(event) {
var regex = new RegExp("^[\"a-zA-Z0-9\\s,,.,!,?,,_,:,;,@,$,%,&,(,),-]+$");
var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
if (!regex.test(key)) {
event.preventDefault();
return false;
}
}



