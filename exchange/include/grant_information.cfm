<cfinclude template="/exchange/security/check.cfm">

<cfquery name="grant_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from grants
 where id = #id#
</cfquery>

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_grant_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>


<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">GRANT AWARD INFORMATION</td>
             <td class="feed_header" align=right>
             <img src="/images/delete.png" width=20 border=0 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td>

             </tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

    <cfoutput query="grant_info">

    <table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr>

    <td class="feed_option" width=300 valign=top><b>
                            Recipient Name<br>
                            DUNS<br>
                            Contact Information
                            </b></td>

    <td class="feed_option" valign=top><a href="/exchange/include/company_profile.cfm?id=0&duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#recipient_name#</a><br>
                            #recipient_duns#<br>
                            <cfif #recipient_address_line_1# is not "">#recipient_address_line_1#<br></cfif>
                            <cfif #recipient_address_line_2# is not "">#recipient_address_line_2#<br></cfif>
                            #recipient_city_name# #recipient_state_code#. #recipient_zip_code# #recipient_country_code#<br>
                            </td>

    <td class="feed_option" valign=top><b>
                            Doing Business As<br>
                            DUNS<br>
                            Congressional District
                            </b></td>

    <td class="feed_option" valign=top>#recipient_parent_name#<br>
                            #recipient_parent_duns#<br>
                            #recipient_congressional_district#
                            </td>
    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>

    <td class="feed_option" width=200 valign=top><b>
                            Department<br>
                            Agency<br>
                            Office
                            </b></td>

    <td class="feed_option" valign=top>#awarding_agency_name#<br>
                            #awarding_sub_agency_name#<br>
                            #awarding_office_name#
                            </td>

    <td class="feed_option" valign=top><b>
                            Funding Department<br>
                            Funding Agency<br>
                            Funding Office
                            </b></td>

    <td class="feed_option" valign=top>#funding_agency_name#<br>
                            #funding_sub_agency_name#<br>
                            #funding_office_name#
                            </td>
    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>
    <td class="feed_option" valign=top><b>
                            Award ID<br>
                            Modification Number<br>
                            Award ID URI
                            </b></td>

    <td class="feed_option" valign=top>
                            #award_id_fain#</a><br>
                            #modification_number#<br>
                            #award_id_uri#
                            </td>

    <td class="feed_option" valign=top><b>
                            Federal Action Obligation<br>
                            Non Federal Funding Amount<br>
                            Total Funding Amount<br>
                            Face Value of Loan<br>
                            Total Loan Value<br>
                            Original Subsidy Cost<br>
                            Total Subsidy Cost
                            </b></td>

    <td class="feed_option" valign=top>#numberformat(federal_action_obligation,'$999,999,999,999')#<br>
                            #numberformat(non_federal_funding_amount,'$999,999,999,999')#<br>
                            #numberformat(total_funding_amount,'$999,999,999,999')#<br>
                            #numberformat(face_value_of_loan,'$999,999,999,999')#<br>
                            #numberformat(total_loan_value,'$999,999,999,999')#<br>
                            #numberformat(original_subsidy_cost,'$999,999,999,999')#<br>
                            #numberformat(total_subsidy_cost,'$999,999,999,999')#


                            </td>
    </tr>


    <tr><td colspan=4><hr></td></tr>

    <tr><td class="feed_option" valign=top><b>Award Title / Description</b></td>
        <td class="feed_option" colspan=3><b>#cfda_title#</b><br><br>#award_description#</td></tr>
    <tr><td colspan=4><hr></td></tr>


    <tr>

    <td class="feed_option" valign=top><b>
                            Action / Award Date<br>
                            Period of Performance Start Date<br>
                            Period of Performance Current End Date<br>


                            </b></td>

    <td class="feed_option" valign=top>#dateformat(action_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_start_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#<br>


    <td class="feed_option" valign=top><b>
                            Assistance Type Code<br>
                            Business Funds Indicator Code<br>
                            Business Types Code<br>
                            Action Type Code<br>
                            Record Type Code<br>
                            </b></td>

    <td class="feed_option" valign=top>#assistance_type_code#<br>
                            #business_funds_indicator_code#<br>
                            #business_types_code#<br>
                            #action_type_code#<br>
                            #record_type_code#<br>
                            </td>

    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>

    <td class="feed_option" valign=top><b>
                            Primary Place of Performance<br>
                            City, State<br>
                            County<br>
                            Congressional District
                            </b></td>

    <td class="feed_option" valign=top><br>
                            #primary_place_of_performance_city_name#, #primary_place_of_performance_state_name#<br>
                            #left(primary_place_of_performance_zip_4,5)#-#right(primary_place_of_performance_zip_4,4)#
                            #primary_place_of_performance_county_name#<br>
                            #primary_place_of_performance_congressional_district#
                            </b>
                            </td>
    </tr>

</table>

</cfoutput>


	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

