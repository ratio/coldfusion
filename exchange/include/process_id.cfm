<cfif isdefined("id")>

    <!--- Process the ID --->

	<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     select company_name, company_duns from company
     where company_id = #id#
	</cfquery>

    <cfset session.company_profile_id = #id#>
    <cfset session.company_profile_duns = #company.company_duns#>
    <cfset session.company_profile_name = #company.company_name#>

<cfelseif isdefined("duns")>

    <!--- Process DUNS --->

	<cfif left(duns,'3') is "000">
		<cfset duns_length = len(duns)>
		<cfset new_length = #evaluate(duns_length - 3)#>
		<cfset new_duns = right(duns,new_length)>
	<cfelseif left(duns,'2') is "00">
		<cfset duns_length = len(duns)>
		<cfset new_length = #evaluate(duns_length - 2)#>
		<cfset new_duns = right(duns,new_length)>
	<cfelseif left(duns,'1') is "0">
		<cfset duns_length = len(duns)>
		<cfset new_length = #evaluate(duns_length - 1)#>
		<cfset new_duns = right(duns,new_length)>
	<cfelse>
		<cfset new_duns = '#duns#'>
	</cfif>

    <!--- Check to see if we have the company --->

	<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  select company_website, company_registered, company_logo, company_id, company_duns, company_name from company
	  where company_duns = '#new_duns#'
	</cfquery>

	<cfif company.recordcount is 1>

	    <cfset session.company_profile_id = #company.company_id#>
	    <cfset session.company_profile_duns = #company.company_duns#>
	    <cfset session.company_profile_name = #company.company_name#>

	<cfelse>

		<!--- Check to see if it's in SAMS --->

		<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select * from sams
		  where duns = '#new_duns#'
		</cfquery>

		<cfif sams.recordcount is 1>

			<!--- If found, add to Companies --->

			<cftransaction>

			<cfquery name="insert_company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  insert into company
			  (
			   company_name,
			   company_website,
			   company_duns,
			   company_city,
			   company_state,
			   company_zip
			  )
			  values
			  (
			   '#sams.legal_business_name#',
			   '#sams.corp_url#',
			   '#sams.duns#',
			   '#sams.city_1#',
			   '#sams.state_1#',
			   '#sams.zip_1#'
			   )
			</cfquery>

			<cfquery name="max" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select max(company_id) as id from company
			</cfquery>

				<cfset session.company_profile_id = #max.id#>
				<cfset session.company_profile_duns = #sams.duns#>
				<cfset session.company_profile_name = #sams.legal_business_name#>

			</cftransaction>

        <cfelse>

		    <cfset session.company_profile_id = 0>
		    <cfset session.company_profile_duns = #new_duns#>

        </cfif>

    </cfif>

<cfelseif isdefined("cb")>

	<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     select top(1) company_id, company_duns from company
     where company_cb_id = '#i#'
	</cfquery>

	<cfset session.company_profile_id = #company.company_id#>
	<cfset session.company_profile_duns = #company.company_duns#>

	<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     insert into recent
     (
      recent_usr_id,
      recent_hub_id,
      recent_investor_id,
      recent_date
     )
     values
     (
     #session.usr_id#,
     #session.hub#,
     #session.company_profile_id#,
     #now()#
     )
	</cfquery>

	<cflocation URL="/exchange/include/profile.cfm?l=35" addtoken="no">

</cfif>

<cfquery name="cview" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into cview
 (
  cview_company_id,
  cview_by_usr_id,
  cview_date,
  cview_hub_id
  )
  values
  (
  #session.company_profile_id#,
  #session.usr_id#,
  #now()#,
  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>
  )
</cfquery>