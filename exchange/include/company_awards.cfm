<table cellspacing=0 cellpadding=0 border=0 width=100%>
<cfoutput>
   <tr>
    <td class="feed_header" style="font-size: 30;">Federal Awards</td>
	<td class="feed_header" align=right>
	<form action="filter.cfm" method="post">
		<td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter</b>&nbsp;&nbsp;
   		<input class="input_text" type="text" size=30 name="filter" placeholder="enter keyword" <cfif isdefined("session.profile_filter")>value='#replace(session.profile_filter,'"','','all')#'</cfif>>&nbsp;&nbsp;
		<input class="button_blue" type="submit" name="button" value="Apply">&nbsp;
		<input class="button_blue" type="submit" name="button" value="Clear">
		<input type="hidden" name="location" value="Awards">
		</td>
	</form>
	</td>
   </tr>
</cfoutput>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<cfquery name="total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(id) as total from award_data
 where (recipient_duns = '#session.company_profile_duns#' or
       recipient_parent_duns = '#session.company_profile_duns#')
</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="250">
 select id, recipient_name, recipient_duns, product_or_service_code_description, award_description, awarding_sub_agency_name, action_date,award_id_piid,awarding_agency_name,awarding_office_name,naics_code,type_of_contract_pricing,type_of_set_aside,period_of_performance_start_date,period_of_performance_potential_end_date, period_of_performance_current_end_date, federal_action_obligation from award_data
 where (recipient_duns = '#session.company_profile_duns#' or
       recipient_parent_duns = '#session.company_profile_duns#')

  <cfif isdefined("session.profile_filter")>
   and contains((*),'#trim(session.profile_filter)#')
  </cfif>

   <cfif isdefined("sv")>

	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC
	<cfelseif #sv# is 3>
	 order by product_or_service_code_description ASC
	<cfelseif #sv# is 30>
	 order by product_or_service_code_description DESC
	<cfelseif #sv# is 4>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 40>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 5>
	 order by naics_code ASC
	<cfelseif #sv# is 50>
	 order by naics_code DESC
	<cfelseif #sv# is 6>
	 order by recipient_name ASC
	<cfelseif #sv# is 60>
	 order by recipient_name DESC
	<cfelseif #sv# is 7>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 70>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 8>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 80>
	 order by period_of_performance_start_date DESC
	<cfelseif #sv# is 9>
	 order by period_of_performance_potential_end_date ASC
	<cfelseif #sv# is 90>
	 order by period_of_performance_potential_end_date DESC
	<cfelseif #sv# is 11>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 110>
	 order by federal_action_obligation DESC
	</cfif>

   <cfelse>
	 order by action_date DESC
   </cfif>

</cfquery>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
           </cfif>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<cfif #agencies.recordcount# is 0>
			  <tr><td class="feed_sub_header">No Federal awards were found.</td></tr>
			<cfelse>

			   <tr><td class="feed_sub_header" colspan=3><b>Prime Contracts<cfif #agencies.recordcount# GT 0>&nbsp;-&nbsp;Last <cfoutput>#agencies.recordcount#</cfoutput> Awards</cfif></b></td>
			       <td class="feed_sub_header" align=right colspan=7>

               <cfoutput>

				   <cfif #agencies.recordcount# LT #total.total#>
				   <a href="/exchange/include/extended_awards.cfm?id=0&duns=#session.company_profile_duns#"><b>See other #numberformat(evaluate(total.total - agencies.recordcount),'999,999')# awards...</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				   </cfif>

           		<a href="/exchange/include/award_dashboard.cfm?id=#session.company_profile_id#" target="_blank" rel="noopener"><img src="/images/icon_dashboard.png" border=0 hspace=5 alt="Award Dashboard" title="Award Dashboard" width=23 border=0 align=absmiddle></a>
                <a href="/exchange/include/award_dashboard.cfm?id=#session.company_profile_id#" target="_blank" rel="noopener">Company Dashboard</a>

           		&nbsp;&nbsp;&nbsp;<a href="/exchange/include/profile.cfm?l=10&export=1&sv=#sv#"><img src="/images/icon_export_excel.png" border=0 hspace=5 alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>
                                  <a href="/exchange/include/profile.cfm?l=10&export=1&sv=#sv#">Export to Excel</a>
			   </cfoutput>

			   </td>

			   </tr>

				<cfoutput>
					<tr height=30>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATE</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>COMPANY</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CONTRACT ##</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AGENCY</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>DESCRIPTION</b></a></td>
					   <td class="text_xsmall" align=center><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>POP START</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>POP END</b></a></td>
					   <td class="text_xsmall" align=right><a href="/exchange/include/profile.cfm?l=10&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>AMOUNT</b></a></td>
					</tr>
				</cfoutput>

				<cfset #total_value# = 0>

				<cfset #counter# = 0>

				<cfoutput query="agencies">

				  <tr height=40

				  <cfif #counter# is 0>
				   bgcolor="ffffff"
				  <cfelse>
				   bgcolor="f0f0f0"
				  </cfif>


				  >
					 <td class="text_xsmall" style="padding-right: 20px;">#dateformat(action_date,'mm/dd/yyyy')#</td>
					 <td class="text_xsmall" style="padding-right: 20px;" width=300>#recipient_name#</td>
					 <td class="text_xsmall" style="padding-right: 20px;"><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener"><b>#award_id_piid#</b></a></td>
					 <td class="text_xsmall" style="padding-right: 20px;"width=175 style="padding-left: 5px; padding-right: 5px;">#awarding_sub_agency_name#</td>
					 <td class="text_xsmall" style="padding-right: 20px;"width=600 style="padding-top: 10px; padding-bottom: 10px;">#award_description#</td>
					 <td class="text_xsmall" align=center width=75>#naics_code#</td>
					 <td class="text_xsmall" style="padding-right: 20px;"width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
					 <td class="text_xsmall" style="padding-right: 20px;"width=75>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
					 <td class="text_xsmall" align=right width=75>#numberformat(federal_action_obligation,'$999,999,999')#</td>
				  </tr>

				  <cfif #counter# is 0>
				   <cfset #counter# = 1>
				  <cfelse>
				   <cfset #counter# = 0>
				  </cfif>

				</cfoutput>

			</cfif>
	   </table>