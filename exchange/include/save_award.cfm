<cfinclude template="/exchange/security/check.cfm">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_data
 where id = #id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">SAVE AWARD TO PORTFOLIO</td>
             <td class="feed_header" align=right><img src="/images/delete.png" width=20 border=0 style="cursor: pointer;" onclick="windowClose();"></td></tr>
         <tr><td colspan=2><hr></td></tr>

</table>

<table cellspacing=0 cellpadding=0 border=0 width=95%>

 <tr><td valign=top>

 <cfoutput query="info">

		<table cellspacing=0 cellpadding=0 border=0 width=95%>

			<tr>

			<td class="feed_option" width=200 valign=top><b>
                            		Recipient Name<br>
                            		DUNS<br>
									Department<br>
									Agency<br>
									Office<br>
									Contract Number<br>
									Parent Contract Number<br>
									</b></td>

			<td class="feed_option" valign=top>
									#recipient_name#<br>
                                    #recipient_duns#<br>
			                        #awarding_agency_name#<br>
									#awarding_sub_agency_name#<br>
									#awarding_office_name#<br>
									#award_id_piid#<br>
									#parent_award_id#<br>

									</td>
			</tr>


		</table>

</td><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=95%>

			<tr>

			<td class="feed_option" width=200 valign=top><b>
                            Federal Action Obligation<br>
                            Base & Exercised Options Value<br>
                            Base & All Options Value<br>
                            Award Date<br>
                            PoP Start Date<br>
                            PoP Current End Date<br>
                            PoP Potential End Date<br>
                            </b></td>

            <td class="feed_option" valign=top>

                            #numberformat(federal_action_obligation,'$999,999,999,999')#<br>
                            #numberformat(base_and_exercised_options_value,'$999,999,999,999')#<br>
                            #numberformat(base_and_all_options_value,'$999,999,999,999')#<br>
                            #dateformat(action_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_start_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#<br>
            </td>
			</tr>


		</table>

</td></tr>

<tr><td class="feed_option" colspan=4><b>Award Description</b><br>
<cfif len(award_description) GT 400>
	#left(award_description,400)#...
<cfelse>
	#award_description#
</cfif></td></tr>

<tr><td colspan=2><hr></td></tr>

</table>

</cfoutput>

<cfquery name="portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where portfolio_usr_id = #session.usr_id#
   <!---
       and portfolio_company_id = #session.company_id# and --->
       and portfolio_type_id = 2
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=95%>

 <form action="/exchange/include/save_portfolio.cfm" method="post">

 <cfif portfolio.recordcount is 0>

     <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created an Award Portfolio.  To add this Award to a Portfolio, please create one now.</td></tr>
     <tr><td class="feed_sub_header">New Award Portfolio Name</td></tr>
     <tr><td><input type="text" name="portfolio_name" size=50 class="input_text" required placeholder="Please provide a name for this Portfolio."></td></tr>
     <tr><td height=10></td></tr>
     <tr><td><hr></td></tr>
     <tr><td class="feed_sub_header"><b>Opportunity Name</b></td></tr>
     <tr><td><input type="text" name="portfolio_item_name" size=50 class="input_text" required placeholder="Please provide a name for this award."></td></tr>
     <tr><td class="feed_sub_header" valign=top><b>Comments or Notes</td></tr>
     <tr><td class="feed_option"><textarea name="portfolio_item_comments" rows=3 cols=120 class="input_textarea" placeholder="Please add any comments you'd like about this award."></textarea></td></tr>
     <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Create and Save">

 <cfelse>

     <tr><td class="feed_sub_header">Create New Award Portfolio</td>
	     <td class="feed_sub_header">Or, Select Existing Award Portfolio</td></tr>

     <tr><td><input type="text" name="portfolio_name" size=40 class="input_text" placeholder="Please provide a name for this Portfolio."></td>
         <td>
			 <select name="portfolio_item_portfolio_id" class="input_select" style="width: 400px;">
			 <cfoutput query="portfolio">
			  <option value=#portfolio_id#>#portfolio_name#
			 </cfoutput>
			 <select></td></tr>

     <tr><td colspan=2><hr></td></tr>

     <tr><td class="feed_sub_header" colspan=2>Award Name</td>

	 <tr><td colspan=2><input type="text" name="portfolio_item_name" size=50 class="input_text" required placeholder="Please provide a name for this award."></td></tr>

	 <tr><td height=5></td></tr>
	 <tr><td colspan=2 class="feed_sub_header" valign=top><b>Comments or Notes</td></tr>
	 <tr><td colspan=2 class="feed_option"><textarea name="portfolio_item_comments" rows=3 cols=120 class="input_textarea"></textarea></td></tr>
	 <tr><td height=5></td></tr>
	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Save Award to Portfolio">

 </cfif>

<cfoutput>
<input type="hidden" name="portfolio_item_type_id" value=2>
<input type="hidden" name="portfolio_type_id" value=2>
<input type="hidden" name="portfolio_item_award_id" value=#id#>
</cfoutput>

</form>

</table>

</center>

</body>
</html>

