<cfinclude template="/exchange/security/check.cfm">

<cfquery name="patent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from patent
 left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
 left join patent_cpc on patent_cpc.patent_id = patent.patent_id
 left join patent_cat on patent_cat.patent_cat = patent_cpc.section_id
 left join patent_catsubsection on patent_catsubsection.id = patent_cpc.subsection_id
 left join patent_catgroup on patent_catgroup.patent_catgroup = patent_cpc.group_id
 left join patent_catsubgroup on patent_catsubgroup.id = patent_cpc.category
 left join patent_sumtext on patent_sumtext.patent_id = patent.patent_id
 where patent.patent_id = '#patent_id#'
</cfquery>

<cfif isnumeric(patent_id)>

	<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into recent(recent_patent_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
	 values(#patent_id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
	</cfquery>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=top>PATENT INFORMATION</td>
             <td class="feed_header" align=right class="feed_sub_header" valign=top>
             <cfoutput>
             <img src="/images/delete.png" align=absmiddle border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
             </cfoutput></td></tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
             <td class="feed_sub_header" valign=top>PATENT ##</td>
             <td class="feed_sub_header" valign=top>GRANTEE</td>
             <td class="feed_sub_header" valign=top>TYPE</td>
             <td class="feed_sub_header" valign=top align=right>DATE GRANTED</td>

             </tr>

         <tr>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top>#patent.patent_id#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top>#ucase(patent.organization)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top>#ucase(patent.type)#</td>
             <td class="feed_sub_header" style="font-weight: normal;" valign=top align=right>#dateformat(patent.date,'mm/dd/yyyy')#</td>
         </tr>

         <tr><td colspan=4><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" valign=top>PATENT TITLE</td>
             <td class="feed_sub_header" align=right><img src="/images/uspto_logo.png" height=18 hspace=10 align=absmiddle><a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=1&f=G&l=50&co1=AND&d=PTXT&s1=#patent_id#.PN.&OS=PN/#patent_id#&RS=PN/#patent_id#" target="_blank" rel="noopener" rel="noreferrer"><u>Patent and Trademark Office Lookup</u></a></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top colspan=2>#patent.title#</td></tr>
         <tr><td class="feed_sub_header" valign=top colspan=2>ABSTRACT</td></tr>
         <tr><td class="feed_option" valign=top colspan=2>#patent.abstract#</td></tr>
         <tr><td colspan=2><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr>
             <td class="feed_sub_header">CATEGORY</td>
             <td class="feed_sub_header">SUB CATEGORY</td>
             <td class="feed_sub_header">GROUP</td>
             <td class="feed_sub_header">SUB GROUP</td>
         </tr>


         <tr>
             <td class="feed_option" valign=top width=150>#patent.patent_cat_name#</td>
             <td class="feed_option" valign=top width=250>#patent.subsection#</td>
             <td class="feed_option" valign=top width=300>#patent.patent_catgroup_name#</td>
             <td class="feed_option" valign=top>#patent.subgroup#</td></tr>

         <tr><td colspan=4><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" valign=top>SUMMARY PATENT INFORMATION</td></tr>
         <tr><td class="feed_option" style="font-weight: normal;" valign=top>
         <cfif patent.sumtext is "">
          No summary information was found.
         <cfelse>
         #patent.sumtext#
         </cfif>
         </td></tr>


       </table>

       </cfoutput>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

