 </div>

 <div class="footer">

 <cfif isdefined("session.hub")>
  <cfif hub.hub_header_bgcolor is "">
   <cfset #backcolor# = "##000000">
  <cfelse>
   <cfset #backcolor# = "#hub.hub_header_bgcolor#">
  </cfif>
 <cfelse>
   <cfset #backcolor# = "##2d2d2d">
 </cfif>

  <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="<cfoutput>#backcolor#</cfoutput>">

	  <tr><td valign=top>

      <cfif hub.hub_parent_hub_id is not "">

		  <cfquery name="getparent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select * from hub
			where hub_id = #hub.hub_parent_hub_id#
		  </cfquery>

		  <cfset #parent_name# = #getparent.hub_name#>
		  <cfset #val# = 2>

      <cfelse>

          <cfset #parent_name# = #hub.hub_name#>
          <cfset #val# = 1>

	  </cfif>

      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="#backcolor#">

			  <tr><td height=10></td></tr>
			  <tr><td width=25>&nbsp;</td><td class="hub_header"><a href="/exchange/hub.cfm?val=#val#"><font color="ffffff">#parent_name#</font></a></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td width=25>&nbsp;</td><td><font size=2 color="ffffff"><b>#hub.hub_support_name#&nbsp;|&nbsp;#hub.hub_support_phone#&nbsp;|&nbsp;#hub.hub_support_email#</b></font></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td></td><td valign=top><font size=1 color="ffffff">Powered by the <a href="https://www.ratio.exchange" target="_blank" rel="noopener" rel="noreferrer"><font color="white">Exchange</font></a>,  Fall 2020 Release, &copy; 2020, All Rights Reserved</font></td></tr>
			  <tr><td height=15></td></tr>
			  <tr><td width=25>&nbsp;</td><td valign=top>

			  <font size=1 color="ffffff">

				  <a href="https://www.ratio.exchange/terms" target="_blank" rel="noopener" rel="noreferrer" style="color:ffffff">Terms of Service</a> |
				  <a href="https://www.ratio.exchange/acceptableuse" target="_blank" rel="noopener" rel="noreferrer" style="color:ffffff">Acceptable Use</a> |
				  <a href="https://www.ratio.exchange/privacy" target="_blank" rel="noopener" rel="noreferrer" style="color:ffffff">Privacy Policy</a> |
				  <a href="https://www.ratio.exchange/cookies" target="_blank" rel="noopener" rel="noreferrer" style="color:ffffff">Cookie Policy</a>

			  </font>

			  </td></tr>

			  <tr><td height=15></td></tr>

		  </table>

	  </cfoutput>

	  </td><td valign=top>

      <cfif hub.hub_library_id is 1>

	  <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="<cfoutput>#backcolor#</cfoutput>">
		  <tr><td>&nbsp;</td></tr>
		  <tr><td align=right>

	   <input class="button_white" type="submit" name="button" value="Help & Support" style="width:150px;" onclick="window.open('https://exchangeanswers.zendesk.com/hc/en-us', '_blank');">

		  <td width=40>&nbsp;</td></td></tr>
	  </table>

      <cfelse>

	  <form action="/exchange/support/" method="post">

	  <table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="<cfoutput>#backcolor#</cfoutput>">
		  <tr><td>&nbsp;</td></tr>
		  <tr><td align=right><input class="button_white" type="submit" name="button" value="Help & Support" style="width:150px;"></td><td width=40>&nbsp;</td></tr>
	  </table>

	  </form>

	  </cfif>

  </td></tr>

</table>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from page
  where page_url = '#cgi.script_name#'
</cfquery>

<cfif check.recordcount is 0>

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into page (page_url, page_added)
  values('#cgi.script_name#',#now()#)
</cfquery>

<cfset credit = 0>

<cfelse>

 <cfif check.page_credits is "">
  <cfset credit = 0>
 <cfelse>
  <cfset credit = #check.page_credits#>
 </cfif>

</cfif>

 <cfset page_time_end = #gettickcount()#>
 <cfset log_page_time = #evaluate(page_time_end-page_time_start)#>

 <cfquery name="log_insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into log
  (log_hub_id, log_usr_id, log_company_id, log_date,log_page,log_ip,log_credit, log_query_string, log_page_time)
  values(#session.hub#,#session.usr_id#,#session.company_id#,#now()#,'#cgi.script_name#','#cgi.remote_addr#',#credit#,'#cgi.query_string#',#log_page_time#)
 </cfquery>

</div>