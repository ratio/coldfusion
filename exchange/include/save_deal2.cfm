<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif isdefined("comp_id")>
 <cfset session.company_profile_id = #comp_id#>
</cfif>

<cfif isdefined("id")>
 <cfset session.company_profile_id = #id#>
</cfif>

<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="pipe_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_pipeline_id from sharing
 where sharing_hub_id = #session.hub# and
	   sharing_pipeline_id is not null and
	   (sharing_to_usr_id = #session.usr_id#)
</cfquery>

<cfif pipe_access.recordcount is 0>
 <cfset pipe_list = 0>
<cfelse>
 <cfset pipe_list = valuelist(pipe_access.sharing_pipeline_id)>
</cfif>

<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select pipeline_name, pipeline_image, pipeline_id, usr_first_name, usr_last_name, pipeline_updated from pipeline
  left join usr on usr_id = pipeline_usr_id
  where pipeline_hub_id = #session.hub# and
		(pipeline_id in (#pipe_list#) or pipeline_usr_id = #session.usr_id#)
  order by pipeline_name
</cfquery>


<!---
<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from pipeline
 where pipeline_hub_id = #session.hub#
 order by pipeline_name
</cfquery> --->

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">Add to Opportunity</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<cfoutput query="comp">
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td valign=top width=130>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=10></td></tr>
			   <tr><td>
					<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif comp.company_logo is "">
					  <img src="//logo.clearbit.com/#comp.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
					  <img src="#media_virtual#/#comp.company_logo#" width=100 border=0>
					</cfif>
					</a>
			   </td></tr>
			   </table>
             </td><td valign=top>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr><td class="feed_title"><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">#company_name#</a></td>
			   <td align=right valign=top width=100></td></tr>
               <tr><td class="feed_option">
				<cfif #comp.company_city# is "" or #comp.company_state# is "">
				 <cfif #comp.company_city# is "">
				  #comp.company_state#
				 <cfelse>
				  #comp.company_state#
				 </cfif>
				<cfelse>
				 <b>#comp.company_city#, #comp.company_state#</b>
				</cfif>
			   </td></tr>

		       <tr><td colspan=3 class="feed_option" valign=top><cfif comp.company_long_desc is "">Company description not provided.<cfelse><cfif len(comp.company_long_desc GT 525)>#left(comp.company_long_desc,'525')#...<cfelse>#comp.company_long_desc#</cfif></cfif></td></tr>
		   </table>
		   </td></tr>

		   <tr><td colspan=2><hr></td></tr>
     </table>
</cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="/exchange/include/save_deal_db.cfm" method="post">

     <tr>
	     <td class="feed_sub_header">Select Existing Opportunity</td>
         <td class="feed_sub_header">Role on Opportunity</td>
     </tr>

     <tr>
         <td width=450>
			 <select name="deal_id" class="input_select" style="width: 400px;">

			  <cfloop query="pipeline">

				  <cfquery name="deals" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				   select * from deal
				   where deal_pipeline_id = #pipeline.pipeline_id#
				   order by deal_name
				  </cfquery>

				  <cfif deals.recordcount GT 0>
				  	<cfoutput>
				  	 <optgroup label="#ucase(pipeline.pipeline_name)#">
				    </cfoutput>

				    <cfoutput query="deals">
				     <option value=#deal_id# <cfif deal_id is #session.deal_id#>selected</cfif>>#deal_name# (#numberformat(deal_value_total,'$999,999,999')#)
				    </cfoutput>

				  </cfif>

			  </cfloop>
			 <select></td>

        <td>
           <select name="deal_role" class="input_select">
            <option value=1>Partner
            <option value=2>Competitor
            <option value=3>Both
           </select></td></tr>

	 <tr><td height=10></td></tr>

     <tr><td colspan=2><hr></td></tr>

	 <tr><td colspan=2 class="feed_sub_header" valign=top><b>Comments or Notes</td></tr>
	 <tr><td colspan=2 class="feed_option"><textarea name="deal_comment_text" style="width: 800px; height: 150px;" placeholder="Please add any comments about this company." class="input_textarea"></textarea></td></tr>
	 <tr><td height=5></td></tr>
	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Add">

</form>

</table>

</center>

</body>
</html>