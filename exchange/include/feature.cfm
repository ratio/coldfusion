<cfquery name="save" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into featured
 (
  featured_company_id,
  featured_hub_id,
  featured_added_usr_id,
  featured_added,
  featured_active
  )
  values
  (
  #session.company_profile_id#,
  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  #session.usr_id#,
  #now()#,
  0
  )
 </cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body" style="background-color: ffffff">

<center>

  <table cellspacing=0 cellpadding=0 border=0 width=98%>
   <tr><td height=20></td></tr>
   <tr><td class="feed_sub_header">Company has been added as a Featured Company.</td></tr>
   <tr><td height=20></td></tr>
   <tr><td class="feed_option">The company will NOT appear in the Marketplace Featured Companies until you make it active and add the featured content in Admin.</td></tr>
  </table>
</center>

</body>
</html>