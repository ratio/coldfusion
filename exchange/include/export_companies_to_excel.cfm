 <cfoutput>
	<cfset theFile = "exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
	<cfset fullFile = "#temp_path#\exchange_export#dateFormat(now(),'yyyy-mm-dd')#.xls">
 </cfoutput>

  <cfspreadsheet action="write" sheetname="EXCHANGE Export" query="companies" filename="#fullfile#" overwrite="yes">
  <cfscript>
	 a = SpreadSheetRead("#fullfile#","EXCHANGE Export");
	   SpreadsheetAddRow(a,"EXCHANGE Report",1,1);
	   SpreadsheetAddRow(a,"Date / Time - #dateformat(now(),'mm/dd/yyyy')# at #timeformat(now())#",2,1);
	   SpreadsheetAddRow(a," ",3,2);
  </cfscript>
  <cfset info = StructNew()>
  <cfset info.title="EXCHANGE Report">
  <cfset info.category="Award Data">
  <cfset info.subject="By Agency">
  <cfset info.creationdate="#dateformat(now(),'mm/dd/yyyy')#">
  <cfset info.author="EXCHANGE">
  <cfset info.comments="This information was exported from https://www.ratio.exchange and shall be used as is.">
  <cfset spreadsheetaddInfo(a,info)>
  <cfset format = StructNew()>
  <cfset format.font="Arial">
  <cfset format.size="30">
  <cfset format.color="black">
  <cfset format.bold="true">
  <cfset format.alignment="left">
  <cfset spreadsheetFormatCell(a,format,1,1)>
  <cfspreadsheet action="write" filename="#fullFile#" name="a" sheet=1 sheetname="Exchange Export" overwrite=true>
  <cfheader name="Content-Disposition" value="attachment; filename=#thefile#">
  <cfcontent type="application/vnd.ms-excel" file="#fullfile#" reset="yes">