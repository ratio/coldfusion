<cfinclude template="/exchange/security/check.cfm">

<cfquery name="tech_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from lab_tech
 where id = #id#
</cfquery>

<cfquery name="lab_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from labs
 where federallab = '#tech_info.federallab#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">LAB TECHNOLOGY INFORMATION</td>
             <td class="feed_header" align=right>
             <img src="/images/delete.png" width=20 border=0 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td>

             </tr>
         <tr><td colspan=2><hr></td></tr>
       </table>


       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td class="feed_header" colspan=2>LAB INFORMATION</td></tr>
         <tr><td height=10></td></tr>

         <cfoutput>

          <tr><td width=150>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td><img src="#lab_info.imageurl#" width=120></td></tr>
               </table>

              </td><td valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header">#ucase(lab_info.department)#</td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">#ucase(lab_info.federallab)#<br>#ucase(lab_info.addressstreet)#<br>#ucase(lab_info.city)#, #ucase(lab_info.state)#  #ucase(lab_info.zipcode)#</td></tr>
               </table>

              </td><td valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header">REPRESENTATIVE</td></tr>
                <tr><td class="feed_sub_header" style="font-weight: normal;">
                #ucase(lab_info.LaboratoryRepresentative)#
                <br>
                #ucase(lab_info.RepresentativeEmail)#
                <br>
                #lab_info.phone#<br>
                </td></tr>
               </table>

              </td></tr>

          <tr><td height=10></td></tr>
		  <tr><td class="feed_sub_header" colspan=2><a href="##" onclick="toggle_visibility('foo');">More Lab Information ...</a></td></tr>
          <tr><td height=10></td></tr>

         </table>

        <div id="foo" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>
       	  <tr><td colspan=3><hr></td></tr>
       	  <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3><b>DESCRIPTION</b><br>#lab_info.fulldescription#</td></tr>
       	  <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3><b>MISSION</b><br>#lab_info.mission#</td></tr>
       	  <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3><b>TECHNOLOGY AREAS</b><br><cfif #lab_info.techareas# is "">Not Provided<cfelse>#lab_info.techareas#</cfif></td></tr>
       	  <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3><b>SECURITY LAB</b><br><cfif #lab_info.securitylab# is "">UNKNOWN<cfelse>#ucase(lab_info.securitylab)#</cfif></b></td></tr>
         </table>
       </div>

         </cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td colspan=3><hr></td></tr>
         <tr><td class="feed_header">TECHNOLOGY INFORMATION</td></tr>
       </table>

       <cfoutput>


       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header">#ucase(tech_info.availabletechnology)#</td></tr>

         <tr><td class="feed_sub_header">Short Description</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.shortdescription#</td></tr>

         <tr><td class="feed_sub_header">Full Description</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.fulldescription#</td></tr>


        <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header"><b>Abstract</b></td></tr>

         <cfset abs = 0>

         <cfif tech_info.abstract is not "">
         <cfset abs = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.abstract#</td></tr>
         </cfif>

         <cfif tech_info.abstract2 is not "">
         <cfset abs = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.abstract2#</td></tr>
         </cfif>

         <cfif tech_info.abstract3 is not "">
         <cfset abs = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.abstract3#</td></tr>
         </cfif>

         <cfif abs is 0>
         <tr><td class="feed_sub_header" style="font-weight: normal;">No Abstract information available.</td></tr>
         </cfif>



         <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header"><b>Patent Information</b></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">Patent Number - <cfif tech_info.patentnumber is "">Unknown<cfelse>#tech_info.patentnumber#</cfif></td></tr>
         <cfif tech_info.patenturl is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal;">Patent URL - <a href="#tech_info.patenturl#" target="_blank" rel="noopener" rel="noreferrer"><u>#tech_info.patenturl#</u></a></td></tr>
         </cfif>

         <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header"><b>Contact Information</b></td></tr>


         <cfif tech_info.LaboratoryRepresentitive is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Lab Representative</b> - #tech_info.LaboratoryRepresentitive#</td></tr>
         </cfif>


         <cfif tech_info.RepresentativeTitle is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Title</b> - #tech_info.RepresentativeTitle#</td></tr>
         </cfif>

         <cfif tech_info.RepresentativeEmail is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Email</b> - #tech_info.RepresentativeEmail#</td></tr>
         </cfif>

         <cfif tech_info.phone is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal;"><b>Phone</b> - #tech_info.phone#</td></tr>
         </cfif>

         <cfset inv = 0>
         <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header"><b>Inventor Information</b></td></tr>

         <cfif tech_info.inventors1 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors1#</td></tr>
         </cfif>

         <cfif tech_info.inventors2 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors2#</td></tr>
         </cfif>

         <cfif tech_info.inventors3 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors3#</td></tr>
         </cfif>

         <cfif tech_info.inventors4 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors4#</td></tr>
         </cfif>

         <cfif tech_info.inventors5 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors5#</td></tr>
         </cfif>

         <cfif tech_info.inventors6 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors6#</td></tr>
         </cfif>

         <cfif tech_info.inventors is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors#</td></tr>
         </cfif>

         <cfif tech_info.inventors9 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors9#</td></tr>
         </cfif>

         <cfif tech_info.inventors10 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors10#</td></tr>
         </cfif>

         <cfif tech_info.inventors11 is not "">
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors11#</td></tr>
         <cfset inv = 1>
         </cfif>

         <cfif tech_info.inventors12 is not "">
         <cfset inv = 1>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#tech_info.inventors12#</td></tr>
         </cfif>

         <cfif inv is 0>
         <tr><td class="feed_sub_header" style="font-weight: normal;">No Inventor information available.</td></tr>
         </cfif>

       </table>

       </cfoutput>


	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

