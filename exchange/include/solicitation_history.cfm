<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfhttp method="get" url="https://api.amaforge.com/fbo/archive?SOLNBR=#soliciation#" result="sol_data">
  <cfhttpparam type="header" name="key" value="d7e4ee3b297344d6acb83c6d421eac08">
</cfhttp>

<cfset soliciation_data=DeserializeJSON(sol_data.filecontent)>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Solicitation History</td>
             <td class="feed_option" align=right><input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="button" value="Close" onclick="windowClose();"></td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>
        </table>

        <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header"></td><td align=right></td></tr>
			 <tr><td height=5></td></tr>
            </table>
			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr><td>

			        <table cellspacing=0 cellpadding=0 border=0 width=100%>

			         <tr><td class="feed_header" colspan=2><b>#soliciation_data.data.subject#</td></tr>

			         <tr><td height=15></td></tr>

			         <tr>
			             <td class="feed_option"><b>Solicitation ##</td>
			             <td class="feed_option">#soliciation_data.data.solnbr#</td>

			             <td class="feed_option"><b>Date Posted</td>
			             <td class="feed_option">#left(soliciation_data.data.date,2)#/#mid(soliciation_data.data.date,3,2)#/#right(soliciation_data.data.date,4)#</td>

			         </tr>

			         <tr>
			             <td class="feed_option">&nbsp;</td>
			             <td class="feed_option">&nbsp;</td>

			             <td class="feed_option"><b>Response Date</td>
			             <td class="feed_option">

			             <cfif isdefined("soliciation_data.data.respdate")>
			              #left(soliciation_data.data.respdate,2)#/#mid(soliciation_data.data.respdate,3,2)#/#right(soliciation_data.data.respdate,4)#
			             </cfif>
			             </td>

			         </tr>

			         <tr>
			             <td class="feed_option"><b>Agency</td>
			             <td class="feed_option">#soliciation_data.data.agency#</td>


			             <td class="feed_option"><b>Small Business</td>
			             <td class="feed_option">#soliciation_data.data.setaside#</td>


			         </tr>

			         <tr>
			             <td class="feed_option"><b>Office</td>
			             <td class="feed_option">#soliciation_data.data.office#</td>

			             <td class="feed_option" valign=top><b>NAICS</td>
			             <td class="feed_option" valign=top>#soliciation_data.data.naics#</td>


			         </tr>

			         <tr>
			             <td class="feed_option"><b>Location</td>
			             <td class="feed_option">#soliciation_data.data.location#</td>

			             <td class="feed_option"><b>Product or Service</td>
			             <td class="feed_option">#soliciation_data.data.classcod#</td>

			         </tr>

			         <tr>
			             <td class="feed_option">&nbsp;</td>
			             <td class="feed_option">&nbsp;</td>

			             <td class="feed_option"><b>Place of Performance</td>
			             <td class="feed_option">

			             <cfif isdefined("soliciation_data.data.popaddress")>#soliciation_data.data.popaddress#<cfelse>&nbsp;</cfif>
			             <cfif isdefined("soliciation_data.data.popzip")>#soliciation_data.data.popzip#<cfelse>&nbsp;</cfif>


			             </td>

			         </tr>

			        <tr><td class="feed_option"><b>Opportunity URL</b></td>
			            <td colspan=3 class="feed_option"><a href="#soliciation_data.data.link#" target="_blank" rel="noopener" rel="noreferrer">#soliciation_data.data.link#</a></td></tr>


			        </table>

			     </td></tr>
			 <tr><td height=10></td></tr>

			 <tr><td><hr></td></tr>
			 <tr><td class="feed_header"><b>Opportunity Description</b></td></tr>
			 <tr><td height=10></td></tr>
			 <tr><td class="feed_option">#replace(soliciation_data.data.desc,"#chr(46)##chr(32)#",".<br><br>","all")#</td></tr>
			 <tr><td height=10></td></tr>
			 <tr><td class="feed_header"><b>Contact</b></td></tr>
			 <tr><td height=10></td></tr>
			 <tr><td class="feed_option">#replace(soliciation_data.data.contact,"#chr(46)##chr(32)#",".<br><br>","all")#</td></tr>

			</table>

        </cfoutput>
 	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>