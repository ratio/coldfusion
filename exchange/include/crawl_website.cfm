<cfsetting RequestTimeout = "90000">

<cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.crawl_company_id#
</cfquery>

<cfloop query="company">

    <cfset counter = 0>

    <cfif len(company.company_website) GT 0>

		<cfoutput>

			<cfhttp url="#company.company_website#" method="get" />

			<cfset site_content = cfhttp.filecontent>
			<cfset metaKeywords = '<meta name="keywords" content="'>
			<cfset metaDescription = '<meta name="description" content="'>

                <!--- Get Meta Keywords --->

				<cfif #findnocase(metakeywords,site_content)# GT 0>

					<cfset #key_start# = #evaluate(findnocase(metakeywords,site_content)+31)#>
					<cfset #key_end# = #findnocase('"', site_content, key_start)#>
					<cfset #key_count# = #evaluate(key_end - key_start)#>
					<cfset #keywords# = #mid(site_content, key_start, key_count)#>

				<cfelse>

					<cfset #keywords# = "nope">

				</cfif>

				<!--- Get Meta Descriptions --->

				<cfif #findnocase(metaDescription,site_content)# GT 0>

					<cfset #desc_start# = #evaluate(findnocase(metaDescription,site_content)+34)#>
					<cfset #desc_end# = #findnocase('"', site_content, desc_start)#>
					<cfset #desc_count# = #evaluate(desc_end - desc_start)#>
					<cfset #description# = #mid(site_content, desc_start, desc_count)#>

				<cfelse>

				   <cfset #description# = "nope">

				</cfif>

				 Company Name - #company.company_name#<br>
				 Company Website - #company.company_website#, #len(company.company_website)#<br>
				 Keywords - #htmleditformat(keywords)#<br>
				 Description - #htmleditformat(description)#<br><br><br>

		</cfoutput>

        <cfquery name="update" datasource="#datasource#" username="#username#" password="#password#">
         update company
         set company_meta_keywords =  <cfif #keywords# is not "nope">'#keywords#'<cfelse>null</cfif>,
             company_meta_description =  <cfif #description# is not "nope">'#description#'<cfelse>null</cfif>,
             company_homepage_text = '#site_content#',
             company_homepage_updated = #now()#
         where company_id = #company.company_id#
        </cfquery>

        <cfset counter = counter + 1>

	</cfif>

</cfloop>