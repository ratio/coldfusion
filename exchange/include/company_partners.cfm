<cfquery name="private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from partner
 where partner_company_id = #session.company_profile_id# and
       partner_public = 1
 order by partner_order
</cfquery>

<cfquery name="public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from partner
 where partner_company_id = #session.company_profile_id# and
       partner_public = 1
 order by partner_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Partners & Alliances</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Local Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been approved by Companies for use on the <cfoutput>#session.network_name#</cfoutput>.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <cfif private.recordcount is 0>
   <tr><td class="feed_sub_header">No information was found.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="private">

      <tr><td valign=top width=120>
		   <cfif #private.partner_logo# is "">
		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#private.partner_logo#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(private.partner_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(private.partner_services,"#chr(10)#","<br>","all")#</td></tr>

			   </table>

           </td>

           <td align=right>

            <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('private#private.partner_id#');">

           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="private#private.partner_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td><hr></td></tr>
		   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Summary of Relationship</td></tr>
		   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>


		   <cfif private.partner_type_id is 1>
		    <b>Strategic.</b>
		   <cfelseif private.partner_type_id is 2>
		    <b>Business.</b>
		   <cfelseif private.partner_type_id is 3>
		    <b>Technical.</b>
		   <cfelse>
		    <b>Operational.</b>
		   </cfif>

		   &nbsp;&nbsp;



		   <cfif #private.partner_relationship# is "">Not described<cfelse>#replace(private.partner_relationship,"#chr(10)#","<br>","all")#</cfif></td></tr>

		   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px; font-weight: normal;"><b>Year Established</b> - #private.partner_year_established#</td></tr>

		   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px;">Partner / Alliance Website</td></tr>
		   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

		   <cfif private.partner_website is "">
		    Not provided
		   <cfelse>
		     <a href="#private.partner_website#" target="_blank" rel="noopener" style="font-weight: normal;"><u>#private.partner_website#</u></a>
		   </cfif>
		   </td></tr>

         </table>
       </div>
       </td></tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

  <tr><td height=10></td></tr>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Public Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been sourced from the Ratio Exchange.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <tr><td height=5></td></tr>

  <cfif public.recordcount is 0>
   <tr><td class="feed_sub_header">No information has been sourced.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="public">

      <tr><td valign=top width=120>
		   <cfif #public.partner_logo# is "">
		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#public.partner_logo#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(public.partner_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(public.partner_services,"#chr(10)#","<br>","all")#</td></tr>

			   </table>

           </td>

           <td align=right>

            <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('public#public.partner_id#');">

           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="public#public.partner_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td><hr></td></tr>
		   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Summary of Relationship</td></tr>
		   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>

		   <cfif public.partner_type_id is 1>
		    <b>Strategic.</b>
		   <cfelseif public.partner_type_id is 2>
		    <b>Business.</b>
		   <cfelseif public.partner_type_id is 3>
		    <b>Technical.</b>
		   <cfelse>
		    <b>Operational.</b>
		   </cfif>

		   &nbsp;&nbsp;

		   <cfif #public.partner_relationship# is "">Not described<cfelse>#replace(public.partner_relationship,"#chr(10)#","<br>","all")#</cfif></td></tr>

		   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px; font-weight: normal;"><b>Year Established</b> - #public.partner_year_established#</td></tr>

		   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px;">Partner / Alliance Website</td></tr>
		   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">

		   <cfif public.partner_website is "">
		    Not provided
		   <cfelse>
		     <a href="#public.partner_website#" target="_blank" rel="noopener" style="font-weight: normal;"><u>#public.partner_website#</u></a>
		   </cfif>
		   </td></tr>

         </table>
       </div>
       </td></tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

</table>
