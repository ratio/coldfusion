
		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr><td colspan=10><hr></td></tr>

			   <tr><td>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=8></td></tr>

			   <tr><td class="feed_header" colspan=10><b><a name=sub2>Federal Subcontractor Awards</td></tr>
			   <tr><td height=5></td></tr>


					<!--- Subcontracts --->

				<cfquery name="sub_total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(id) as total from award_data_sub
				 where prime_awardee_duns = '#company.company_duns#'
				</cfquery>

				<cfquery name="sub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select top(50) subaward_number, subaward_description, id, prime_awardee_duns, subawardee_name, prime_awarding_agency_name, subawardee_duns, subaward_action_date, prime_award_parent_piid, prime_award_piid, prime_awarding_sub_agency_name, prime_awarding_office_name, subaward_amount from award_data_sub
				 where prime_awardee_duns = '#company.company_duns#'
				 order by subaward_action_date DESC
				</cfquery>

			   <cfif #sub.recordcount# GT 0>
				   <cfoutput>
				   <tr><td class="feed_option" colspan=9><b>Last #sub.recordcount# Awards</b></td>
					   <td class="feed_option" align=right colspan=2><a href="extended_subcontrator.cfm"><b>See All #numberformat(sub_total.total,'999,999')# Awards</b></a></td></tr>
				   </cfoutput>
               </cfif>

               <tr><td height=10></td></tr>

			   <tr><td colspan=10>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			        <cfif subscription.premium_awards is 1 or (isdefined("session.hub") and hub_subscription.premium_awards is 1)>

					<cfif sub.recordcount is 0>

					 <tr><td class="feed_option">No subcontractors were found.</td></tr>

					<cfelse>

					<cfoutput>

								<tr>
								   <td class="text_xsmall" width=75><b>Award Date</b></td>
								   <td class="text_xsmall" width=100><b>Subcontractor</b></td>
								   <td class="text_xsmall" width=100><b>Subcontract ##</b></td>
								   <td class="text_xsmall"><b>Prime Contract ##</b></td>
								   <td class="text_xsmall"><b>Department</b></td>
								   <td class="text_xsmall"><b>Agency</b></td>
								   <td class="text_xsmall"><b>Award Description</b></td>
								   <td class="text_xsmall" align=right width=100><b>Amount</b></td>
								</tr>

					</cfoutput>

								<tr><td height=5></td></tr>

								<cfset #counter# = 0>
								<cfset #sc_total# = 0>

								<cfoutput query="sub">

								 <cfif counter is 0>
								  <tr bgcolor="ffffff">
								 <cfelse>
								  <tr bgcolor="e0e0e0">
								 </cfif>

									 <td class="text_xsmall" valign=top>#dateformat(subaward_action_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#subawardee_duns#" target="_blank" rel="noopener" rel="noreferrer">#subawardee_name#</a></td>
									 <td class="text_xsmall" valign=top><a href="/exchange/include/sub_award_information.cfm?id=#sub.id#" target="_blank" rel="noopener" rel="noreferrer">#subaward_number#</a></td>
									 <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?prime_award_id=#prime_award_piid#&duns=#prime_awardee_duns#" target="_blank" rel="noopener" rel="noreferrer">#prime_award_piid#</a></td>
									 <td class="text_xsmall" valign=top>#prime_awarding_agency_name#</td>
									 <td class="text_xsmall" valign=top>#prime_awarding_sub_agency_name#</td>
									 <td class="text_xsmall" valign=top width=600>#subaward_description#</td>
									 <td class="text_xsmall" valign=top align=right>#numberformat(subaward_amount,'$999,999,999,999')#</td>

								 </tr>


								 <cfif counter is 0>
								  <cfset counter = 1>
								 <cfelse>
								  <cfset counter = 0>
								 </cfif>
								 <cfset #sc_total# = #sc_total# + #subaward_amount#>

								</cfoutput>

								<tr><td height=5></td></tr>
								<tr><td class="feed_option"><b>Total</b></td>
									<td class="feed_option" align=right colspan=7><b><cfoutput>#numberformat(sc_total,'$999,999,999,999')#</cfoutput></b></td></tr>
			   					<tr><td>&nbsp;</td></tr>
			   					<tr><td class="text_xsmall" colspan=7><b>Note -</b> this may not include information from subcontractors where the Prime was not legally obligated to report them.</td></tr>

				   </cfif>

				   <cfelse>

					   <tr><td class="feed_option" colspan=7><b>Total Records Found - <cfoutput>#sub.recordcount#</cfoutput> Contracts</b></td>
						   <td class="feed_option" align=right colspan=4></tr>
					   <tr><td class="feed_option">Requires premium access.</td></tr>

				   </cfif>

				   </table>

			   </td></tr>
</td></tr>
</table>