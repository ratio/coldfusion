				<!--- Prime Contracts --->

				<cfquery name="total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(id) as total from award_data
				 where recipient_duns = '#company.company_duns#'
				</cfquery>

				<cfquery name="awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select top(50) id, modification_number, product_or_service_code_description, award_description, awarding_sub_agency_name, action_date,award_id_piid,awarding_agency_name,awarding_office_name,naics_code,type_of_contract_pricing,type_of_set_aside,period_of_performance_start_date,period_of_performance_potential_end_date,federal_action_obligation from award_data
				 where recipient_duns = '#company.company_duns#'

				   <cfif isdefined("sv")>
					<cfif #sv# is 1>
					 order by action_date DESC
					<cfelseif #sv# is 10>
					 order by action_date ASC
					<cfelseif #sv# is 2>
					 order by award_id_piid, modification_number ASC
					<cfelseif #sv# is 20>
					 order by award_id_piid, modification_number DESC
					<cfelseif #sv# is 3>
					 order by product_or_service_code_description ASC
					<cfelseif #sv# is 30>
					 order by product_or_service_code_description DESC
					<cfelseif #sv# is 4>
					 order by awarding_sub_agency_name ASC
					<cfelseif #sv# is 40>
					 order by awarding_sub_agency_name DESC
					<cfelseif #sv# is 5>
					 order by naics_code ASC
					<cfelseif #sv# is 50>
					 order by naics_code DESC
					<cfelseif #sv# is 6>
					 order by type_of_contract_pricing ASC
					<cfelseif #sv# is 60>
					 order by type_of_contract_pricing DESC
					<cfelseif #sv# is 7>
					 order by type_of_set_aside ASC
					<cfelseif #sv# is 70>
					 order by type_of_set_aside DESC
					<cfelseif #sv# is 8>
					 order by period_of_performance_start_date ASC
					<cfelseif #sv# is 80>
					 order by period_of_performance_start_date DESC
					<cfelseif #sv# is 9>
					 order by period_of_performance_potential_end_date ASC
					<cfelseif #sv# is 90>
					 order by period_of_performance_potential_end_date DESC
					<cfelseif #sv# is 11>
					 order by federal_action_obligation ASC
					<cfelseif #sv# is 110>
					 order by federal_action_obligation DESC
					</cfif>
				   <cfelse>
					 order by action_date DESC
				   </cfif>
				</cfquery>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=8></td></tr>
			   <tr><td colspan=11><hr></td></tr>
			   <tr><td class="feed_header" colspan=4><a name=awards>Federal Prime Awards (Contracts & Mods)</a></td>
			       <td class="feed_option" align=right colspan=7>

			       <cfif subscription.premium_awards is 1>
						<cfif awards.recordcount GT 0>
							<cfoutput>
							<b><a href="award_dashboard.cfm?id=#id#">Award Dashboard</a>&nbsp;|&nbsp;<a href="/exchange/include/award_summary.cfm?duns=#company.company_duns#" target="_blank" rel="noopener" rel="noreferrer">Award Summary</a></b>
							</cfoutput>
						</cfif>

						<cfoutput>
							<cfif #awards.recordcount# LT #total.total# and #awards.recordcount# GT 0>
							 &nbsp;|&nbsp;<a href="extended_awards.cfm?id=#id#"><b>See All #numberformat(total.total,'999,999')# Awards</b></a>
							</cfif>
						</cfoutput>
			      </cfif>

			       </td></tr>

			   <tr><td height=5></td></tr>

			   <cfif subscription.premium_awards is 1 or (isdefined("session.hub") and hub_subscription.premium_awards is 1)>

			     <cfif #awards.recordcount# GT 0>

			       <tr><td class="feed_option" colspan=3><b><cfif #awards.recordcount# GT 0>Last <cfoutput>#awards.recordcount#</cfoutput> Awards</cfif></b></td>
			           <td class="feed_option" align=right colspan=8>

					   <form action="search.cfm" method="post">
						<b>Search Contracts: </b>&nbsp;<input class="input_text" name="keywords" type="text" required>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Search">
					   <cfoutput><input type="hidden" name="id" value="#id#"></cfoutput>
					   </form>
					   </td>

                  </cfif>

			    </tr>
				<tr><td>

						<cfif #awards.recordcount# is 0>
							<tr><td class="feed_option">No Federal prime contract awards were found.</td></tr>
						<cfelse>
								<tr><td height=10></td></tr>
                                <cfoutput>
									<tr>
									   <td class="text_xsmall" width=75><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
									   <td class="text_xsmall" width=100><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract Number</b></a></td>
									   <td class="text_xsmall" width=100><b>Mod</b></td>
									   <td class="text_xsmall" width=275><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
									   <td class="text_xsmall"><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Award Description</b></a></td>
									   <td class="text_xsmall"><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Product or Service</b></a></td>
									   <td class="text_xsmall" align=center><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a></td>
									   <td class="text_xsmall" align=center><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Pricing</b></a></td>
									   <td class="text_xsmall"><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>POP Start</b></a></td>
									   <td class="text_xsmall"><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>POP End</b></a></td>
									   <td class="text_xsmall" align=right><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>Obligation</b></a></td>
									</tr>
								</cfoutput>

								<cfset #total_value# = 0>
								<cfset #counter# = 0>
								<cfoutput query="awards">

								  <tr
								  <cfif #counter# is 0>
								   bgcolor="ffffff"
								  <cfelse>
								   bgcolor="e0e0e0"
								  </cfif>
								  >
									 <td class="text_xsmall" valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
									 <td class="text_xsmall" valign=top>#modification_number#</td>
									 <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
									 <td class="text_xsmall" valign=top>#award_description#</td>
									 <td class="text_xsmall" valign=top>#product_or_service_code_description#</td>
									 <td class="text_xsmall" valign=top align=center width=75>#naics_code#</td>
									 <td class="text_xsmall" valign=top width=75 align=center>
									 <cfif type_of_contract_pricing is "Firm Fixed Price">
									  FFP
									 <cfelseif type_of_contract_pricing is "Time and Materials">
									  T&M
									 <cfelseif type_of_contract_pricing is "Cost Plus Fixed Fee">
									  CPFF
									 <cfelseif type_of_contract_pricing is "Labor Hours">
									  LH
									 <cfelseif type_of_contract_pricing is "Fixed Price Incentive">
									  FPI
									 <cfelse>
									 #left(type_of_contract_pricing,21)#
									 </cfif>
									 </td>
									 <td class="text_xsmall" valign=top width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=top width=75>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" valign=top align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
								  </tr>

								  <cfset #total_value# = #total_value# + #federal_action_obligation#>

								  <cfif #counter# is 0>
								   <cfset #counter# = 1>
								  <cfelse>
								   <cfset #counter# = 0>
								  </cfif>

								</cfoutput>

								<tr><td class="feed_option" colspan=10><b>Total:</b></td>
								<cfoutput>
									<td class="feed_option" align=right><b>#numberformat(total_value,'$999,999,999')#</b></td>
								</cfoutput>
							</cfif>

               <cfelse>
				   <tr><td class="feed_option" colspan=7><b>Total Records Found - <cfoutput>#total.total#</cfoutput> Awards and Mods</b></td>
					   <td class="feed_option" align=right colspan=4></tr>
				   <tr><td class="feed_option">Requires premium access.</td></tr>
               </cfif>

				<cfquery name="subtotal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select count(id) as total from award_data_sub
				 where subawardee_duns = '#company.company_duns#'
				</cfquery>

					<cfquery name="sub" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="50">
					 select top(50) subaward_report_year, subaward_report_month, subaward_number, id, prime_awarding_agency_name, subaward_description, subaward_action_date, prime_awardee_duns, prime_award_parent_piid, prime_award_piid, prime_awarding_sub_agency_name, prime_awarding_office_name, prime_awardee_name, subaward_amount from award_data_sub
					 where subawardee_duns = '#company.company_duns#'

					   <cfif isdefined("s")>
						<cfif #s# is 1>
						 order by subaward_action_date DESC
						<cfelseif #s# is 10>
						 order by subaward_action_date ASC
						<cfelseif #s# is 2>
						 order by prime_award_parent_piid ASC
						<cfelseif #s# is 20>
						 order by prime_award_parent_piid DESC
						<cfelseif #s# is 3>
						 order by prime_award_piid ASC
						<cfelseif #s# is 30>
						 order by prime_award_piid DESC
						<cfelseif #s# is 4>
						 order by subaward_number ASC
						<cfelseif #s# is 40>
						 order by subaward_number DESC
						<cfelseif #s# is 5>
						 order by prime_awarding_agency_name ASC
						<cfelseif #s# is 50>
						 order by prime_awarding_agency_name DESC
						<cfelseif #s# is 6>
						 order by prime_awarding_sub_agency_name ASC
						<cfelseif #s# is 60>
						 order by prime_awarding_sub_agency_name DESC
						<cfelseif #s# is 7>
						 order by prime_awardee_name ASC
						<cfelseif #s# is 70>
						 order by prime_awardee_name DESC
						<cfelseif #s# is 8>
						 order by subaward_amount ASC
						<cfelseif #s# is 80>
						 order by subaward_amount DESC
						<cfelseif #s# is 9>
						 order by subaward_report_year DESC
						<cfelseif #s# is 90>
						 order by subaward_report_year ASC
						<cfelseif #s# is 10>
						 order by subaward_report_month DESC
						<cfelseif #s# is 100>
						 order by subaward_report_month ASC
					   <cfelse>
						 order by subaward_action_date DESC
					   </cfif>
					  <cfelse>
						 order by subaward_action_date DESC
					  </cfif>
					</cfquery>

			   <tr><td colspan=11><hr></td></tr>
			   <tr><td class="feed_header" colspan=10><a name=sub1>Federal Subcontract Awards</a></td></tr>
			   <tr><td height=5></td></tr>

			   <cfif #sub.recordcount# GT 0>
				   <cfoutput>
				   <tr><td class="feed_option" colspan=9><b>Last #sub.recordcount# Awards</b></td>
					   <td class="feed_option" align=right colspan=2><cfif #subtotal.total# GT 50><a href="extended_subawards.cfm"><b>See All #subtotal.total# Awards</b></a></cfif></td></tr>
				   </cfoutput>
               </cfif>

               <tr><td height=10></td></tr>

			   <tr><td colspan=11>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<!--- Sub Contract Awards --->

			        <cfif subscription.premium_awards is 1 or (isdefined("session.hub") and hub_subscription.premium_awards is 1)>
					<cfif sub.recordcount is 0>
					 	<tr><td class="feed_option">No Federal subcontract awards were found.</td></tr>
					<cfelse>
						<cfoutput>
									<tr>
									   <td class="text_xsmall" width=75 valign=top><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=1<cfelse><cfif #s# is 1>s=10<cfelse>s=1</cfif></cfif>"><b>Award Date</b></a></td>
									   <td class="text_xsmall" valign=top><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=4<cfelse><cfif #s# is 4>s=40<cfelse>s=4</cfif></cfif>"><b>Sub Contract ##</b></a></td>
									   <td class="text_xsmall" valign=top><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=7<cfelse><cfif #s# is 7>s=70<cfelse>s=7</cfif></cfif>"><b>Prime</b></a></td>
									   <td class="text_xsmall" valign=top><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=3<cfelse><cfif #s# is 3>s=30<cfelse>s=3</cfif></cfif>"><b>Prime Contract ##</b></a></td>

									   <td class="text_xsmall" valign=top align=center><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=9<cfelse><cfif #s# is 9>s=90<cfelse>s=9</cfif></cfif>"><b>Year *</b></a></td>
									   <td class="text_xsmall" valign=top align=center><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=10<cfelse><cfif #s# is 10>s=100<cfelse>s=10</cfif></cfif>"><b>Month *</b></a/></td>

									   <td class="text_xsmall" valign=top><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=5<cfelse><cfif #s# is 5>s=50<cfelse>s=5</cfif></cfif>"><b>Department</b></a></td>
									   <td class="text_xsmall" valign=top><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=6<cfelse><cfif #s# is 6>s=60<cfelse>s=6</cfif></cfif>"><b>Agency</b></a></td>
									   <td class="text_xsmall" valign=top><b>Award Description</b></td>
									   <td class="text_xsmall" valign=top align=right><a href="federal_profile.cfm?id=#id#&<cfif not isdefined("s")>s=8<cfelse><cfif #s# is 8>s=80<cfelse>s=8</cfif></cfif>"><b>Amount</b></a></td>
									</tr>
						</cfoutput>

									<tr><td height=5></td></tr>

									<cfset #counter# = 0>
									<cfset #sub_total# = 0>

									<cfoutput query="sub">

									 <cfif counter is 0>
									  <tr bgcolor="ffffff">
									 <cfelse>
									  <tr bgcolor="e0e0e0">
									 </cfif>

										 <td class="text_xsmall" valign=top>#dateformat(subaward_action_date,'mm/dd/yyyy')#</td>
										 <td class="text_xsmall" valign=top width=100><a href="/exchange/include/sub_award_information.cfm?id=#sub.id#" target="_blank" rel="noopener" rel="noreferrer">#subaward_number#</a></td>
										 <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#prime_awardee_duns#" target="_blank" rel="noopener" rel="noreferrer">#prime_awardee_name#</a></td>
										 <td class="text_xsmall" valign=top width=100><a href="/exchange/include/award_information.cfm?prime_award_id=#prime_award_piid#&duns=#prime_awardee_duns#" target="_blank" rel="noopener" rel="noreferrer">#prime_award_piid#</a></td>
										 <td class="text_xsmall" valign=top width=50 align=center>#subaward_report_year#</td>
										 <td class="text_xsmall" valign=top width=50 align=center>#subaward_report_month#</td>
										 <td class="text_xsmall" valign=top>#prime_awarding_agency_name#</td>
										 <td class="text_xsmall" valign=top>#prime_awarding_sub_agency_name#</td>
										 <td class="text_xsmall" valign=top>#subaward_description#</td>
										 <td class="text_xsmall" valign=top align=right>#numberformat(subaward_amount,'$999,999,999,999')#</td>

									 </tr>

									 <cfif counter is 0>
									  <cfset counter = 1>
									 <cfelse>
									  <cfset counter = 0>
									 </cfif>
									 <cfset #sub_total# = #sub_total# + #subaward_amount#>

									</cfoutput>

									<tr><td height=5></td></tr>
									<tr><td class="feed_option" colspan=9><b>Total</b></td>
										<td class="feed_option" align=right><b><cfoutput>#numberformat(sub_total,'$999,999,999,999')#</cfoutput></b></td></tr>
									<tr><td height=5></td></tr>
									<tr><td class="feed_option" colspan=9><b>*</b> - Year and month values represent the timeframe that the Prime contractor reported this information.</td></tr>
					   </cfif>

				   <cfelse>

					   <tr><td class="feed_option" colspan=7><b>Total Records Found - <cfoutput>#sub.recordcount#</cfoutput> Contracts</b></td>
						   <td class="feed_option" align=right colspan=4></tr>
					   <tr><td class="feed_option">Requires premium access.</td></tr>

				   </cfif>

				   </table>

			   </td></tr>