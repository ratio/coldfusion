<cfquery name="company_profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 left join cb_organizations on uuid = company_cb_id
 where company_id = #session.company_profile_id#
</cfquery>

<!--- Check to see if this company is being followed or not --->

<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from follow
  where follow_company_id = #session.company_profile_id# and
        follow_hub_id = #session.hub# and
		follow_by_usr_id = #session.usr_id#
</cfquery>

<cfif isdefined("session.hub")>

	<cfquery name="network" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select hub_comp_id from hub_comp
	 where hub_comp_company_id = #session.company_profile_id# and
		   hub_comp_hub_id = #session.hub#
	</cfquery>

	<cfquery name="profile_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub_xref
	 where hub_xref_usr_id = #session.usr_id# and
	       hub_xref_hub_id = #session.hub#
	</cfquery>

</cfif>

<cfoutput>
<style>
.banner {
    <cfif #company_profile.company_banner# is "">
    background-image: url('/images/company_stock_banner.png');
    <cfelse>
    background-image: url('#media_virtual#/#company_profile.company_banner#');
    </cfif>
    max-width: 100%;
    background-size: 100%;
    height:200px;
}
</style>
</cfoutput>

<cfoutput>
   <table cellspacing=0 cellpadding=0 border=0 width=100% class="banner">
   <tr><td height=30></td></tr>

	   <tr><td align=middle valign=middle width=175>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfif #company_profile.company_logo# is "">
				<img src="//logo.clearbit.com/#company_profile.company_website#" width=150 border=0 onerror="this.src='/images/no_logo.png'">
			 <cfelse>
			    <img src="#media_virtual#/#company_profile.company_logo#" width=150 style="border: 2px solid ##ffffff;">
			 </cfif>

		  </table>

	   </td><td width=30>&nbsp;</td><td valign=top align=left>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" style="font-size: 45px; color: ffffff;">
		 <cfif network.recordcount is 1>
		 <img src="/images/in_network.png" height=43 alt="In Network Partner" title="In Network Partner">&nbsp;
		 </cfif>#company_profile.company_name#</td>
		     <td align=right>

				<div class="dropdown">
				  <img src="/images/profile_options.png" width=125 style="padding-right: 20px;">
				  <div class="dropdown-content" style="width: 225px; text-align: left; right: 20;">

					<a href="/exchange/include/save_company.cfm" style="font-size: 16px;" onclick="window.open('/exchange/include/save_company.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
					<a href="/exchange/include/save_deal.cfm" style="font-size: 16px;" onclick="window.open('/exchange/include/save_deal.cfm','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-dollar"></i>&nbsp;&nbsp;Add to Opportunity</a>

					 <cfif #follow.recordcount# is 0>
					  <a href="/exchange/marketplace/follow.cfm?l=3" style="font-size: 16px;"><i class="fa fa-fw fa-thumbs-up"></i>&nbsp;&nbsp;Follow Company</a>
					 <cfelse>
					  <a href="/exchange/marketplace/unfollow.cfm?l=3" style="font-size: 16px;"><i class="fa fa-fw fa-thumbs-down"></i>&nbsp;&nbsp;Unfollow Company</a>
					 </cfif>

					 <cfif profile_access.hub_xref_company_add is 1>

						 <cfif network.recordcount is 0>
						   <a href="network_add.cfm?a=1&id=#session.company_profile_id#" style="font-size: 16px;"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;&nbsp;Add to Network</a>
						 <cfelse>
						   <a href="network_add.cfm?a=2&id=#session.company_profile_id#" style="font-size: 16px;" onclick="return confirm('Confirm Removal\r\nAre you sure you want to remove this Company from this Exchange?');"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;&nbsp;Remove from Network</a>
						 </cfif>

					 </cfif>

					<a href="/exchange/include/profile.cfm?l=31" style="font-size: 16px;"><i class="fa fa-fw fa-comment"></i>&nbsp;&nbsp;Add Comments</a>

				  </div>

			    </div>

		     </td></tr>
		 </tr>

		 <tr><td height=10></td></tr>
		 <tr><td class="feed_header" style="font-size: 20px; color: ffffff;">

		 <cfif #company_profile.company_city# is "">CITY<cfelse>#ucase(company_profile.company_city)#</cfif>, <cfif #company_profile.company_state# is 0 or #company_profile.company_state# is "">STATE<cfelse>#ucase(company_profile.company_state)#</cfif>


         <cfif #company_profile.company_website# is not "">
		 	<br><br><a class="feed_header" style="font-size: 20px; color: ffffff;" href="#company_profile.company_website#" target="_blank" rel="noopener" rel="noreferrer">#company_profile.company_website#
		 </cfif>

		 </td>

		     <td class="feed_sub_header" style="color: ffffff;" align=right>

		     <cfif isdefined("u")>
				 <cfif u is 1>
				  The Company has been added to the Network
				 <cfelseif u is 2>
				  The Company has been removed from the Network
				 <cfelseif u is 3>
				  You are now following this Company
				 <cfelseif u is 4>
				  You have stopped following this Company
				 </cfif>
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		     </cfif>

		     </td></tr>
	   </table>

	   </td></tr>

   <tr><td height=30></td></tr>

   </table>
</cfoutput>