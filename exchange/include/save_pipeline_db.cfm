<cfinclude template="/exchange/security/check.cfm">

<cfquery name="deal" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_data
 where id = #deal_award_id#
</cfquery>

<cfquery name="value" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select sum(federal_action_obligation) as total from award_data
 where award_id_piid = '#deal.award_id_piid#'
</cfquery>

<cfset deal_value_total = #value.total#>
<cfset deal_customer_name = #deal.awarding_sub_agency_name#>
<cfset deal_contract_id = #deal.award_id_piid#>
<cfset deal_dept_code = #deal.awarding_agency_code#>
<cfset deal_agency_code = #deal.awarding_sub_agency_code#>
<cfset deal_pop_city = #deal.primary_place_of_performance_city_name#>
<cfset deal_pop_state = #deal.primary_place_of_performance_state_code#>
<cfset deal_naics = #deal.naics_code#>
<cfset deal_psc = #deal.product_or_service_code#>
<cfset deal_past_sol = #deal.solicitation_identifier#>

<cfif #button# is "Save">

 <cftransaction>

	<cfquery name="insert_pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into pipeline
	 (pipeline_name, pipeline_usr_id, pipeline_access_id, pipeline_created, pipeline_updated, pipeline_company_id, pipeline_hub_id)
	 values
	 ('#pipeline_name#',#session.usr_id#,1,#now()#, #now()#, #default_company_id#, #session.hub#)
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(pipeline_id) as id from pipeline
	</cfquery>

	<cfquery name="insert_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      insert into deal
      (deal_value_total, deal_customer_name, deal_contract_id, deal_dept_code, deal_agency_code, deal_pop_city, deal_pop_state, deal_naics, deal_psc, deal_past_sol, deal_name, deal_desc, deal_created, deal_updated, deal_owner_id, deal_access_id, deal_pipeline_id, deal_hub_id, deal_keywords, deal_stage_id, deal_award_id)
      values
      (#deal_value_total#,'#deal_customer_name#','#deal_contract_id#','#deal_dept_code#','#deal_agency_code#','#deal_pop_city#','#deal_pop_state#','#deal_naics#','#deal_psc#','#deal_past_sol#','#deal_name#','#deal_desc#',#now()#, #now()#, #session.usr_id#, 1, #max.id#, #session.hub#, '#deal_keywords#',1, #deal_award_id#)
    </cfquery>

	<cfquery name="max_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(deal_id) as id from deal
	</cfquery>

 </cftransaction>

<cfelse>

    <cfif pipeline_name is "">

    <cftransaction>

		<cfquery name="insert_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into deal
		  (deal_value_total, deal_customer_name, deal_contract_id, deal_dept_code, deal_agency_code, deal_pop_city, deal_pop_state, deal_naics, deal_psc, deal_past_sol, deal_name, deal_desc, deal_created, deal_updated, deal_owner_id, deal_access_id, deal_pipeline_id, deal_hub_id, deal_keywords, deal_stage_id, deal_award_id)
		  values
		  (#deal_value_total#,'#deal_customer_name#','#deal_contract_id#','#deal_dept_code#','#deal_agency_code#','#deal_pop_city#','#deal_pop_state#','#deal_naics#','#deal_psc#','#deal_past_sol#','#deal_name#','#deal_desc#',#now()#, #now()#, #session.usr_id#, 1, #deal_pipeline_id#, #session.hub#, '#deal_keywords#',1, #deal_award_id#)
		</cfquery>

		<cfquery name="max_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select max(deal_id) as id from deal
		</cfquery>

		<cfif #deal_keywords# is not "">

			<cfloop index="k" list="#deal_keywords#">

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert into partner_snapshot
				 (
				  partner_snapshot_keyword,
				  partner_snapshot_deal_id
				  )
				  values
				  (
				  '#k#',
				   #max_deal.id#
				   )
				</cfquery>

			</cfloop>

		</cfif>

	</cftransaction>

    <cfelse>

		 <cftransaction>

			<cfquery name="insert_pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into pipeline
			 (pipeline_name, pipeline_usr_id, pipeline_access_id, pipeline_created, pipeline_updated, pipeline_company_id, pipeline_hub_id)
			 values
			 ('#pipeline_name#',#session.usr_id#,1,#now()#, #now()#, #default_company_id#, #session.hub#)
			</cfquery>

			<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select max(pipeline_id) as id from pipeline
			</cfquery>

			<cfquery name="insert_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into deal
			  (deal_value_total, deal_customer_name, deal_contract_id, deal_dept_code, deal_agency_code, deal_pop_city, deal_pop_state, deal_naics, deal_psc, deal_past_sol, deal_name, deal_desc, deal_created, deal_updated, deal_owner_id, deal_access_id, deal_pipeline_id, deal_hub_id, deal_keywords, deal_stage_id, deal_award_id)
			  values
			  (#deal_value_total#,'#deal_customer_name#','#deal_contract_id#','#deal_dept_code#','#deal_agency_code#','#deal_pop_city#','#deal_pop_state#','#deal_naics#','#deal_psc#','#deal_past_sol#','#deal_name#','#deal_desc#',#now()#, #now()#, #session.usr_id#, 1, #max.id#, #session.hub#, '#deal_keywords#',1, #deal_award_id#)
			</cfquery>

			<cfquery name="max_deal" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select max(deal_id) as id from deal
			</cfquery>

			<cfif #deal_keywords# is not "">

				<cfloop index="k" list="#deal_keywords#">

					<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 insert into partner_snapshot
					 (
					  partner_snapshot_keyword,
					  partner_snapshot_deal_id
					  )
					  values
					  (
					  '#k#',
					   #max_deal.id#
					   )
					</cfquery>

				</cfloop>

			</cfif>

		 </cftransaction>

    </cfif>

</cfif>

<cfif isdefined("add_subs")>

		<cfquery name="info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select award_id_piid, recipient_duns from award_data
		 where id = #deal_award_id#
		</cfquery>

		<cfquery name="subs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select distinct(company_id) from award_data_sub
		 join company on company_duns = subawardee_duns
 		 where prime_award_piid = '#info.award_id_piid#'
 		 and prime_awardee_duns = '#info.recipient_duns#'
		</cfquery>

		<cfloop query="subs">

		 <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into deal_comp
		  (deal_comp_deal_id, deal_comp_partner_id, deal_comp_added, deal_comp_added_by)
		  values
		  (#max_deal.id#,#subs.company_id#,#now()#,#session.usr_id#)
		 </cfquery>

		</cfloop>

 </cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: ffffff">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">Deal has been saved to the Pipeline</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>


