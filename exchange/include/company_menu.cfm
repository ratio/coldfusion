<cfquery name="agreements" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(company_agreement_id) as total from company_agreement
 where company_agreement_company_id = #session.company_profile_id# and
       company_agreement_hub_id = #session.hub#
</cfquery>

<cfquery name="university_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select count(id) as total from autm
where exchange_company_id = #session.company_profile_id#
</cfquery>

<cfquery name="labs_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(id) as total from lab_tech
 where lab_company_id = #session.company_profile_id#
</cfquery>

<cfset innovation_count = university_count.total + labs_count.total>

<!--- Products --->

<cfquery name="product_count_private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(product_id) as total from product
 where product_company_id = #session.company_profile_id# and
       product_public = 1
</cfquery>

<cfquery name="product_count_public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(product_id) as total from product
 where product_company_id = #session.company_profile_id# and
       product_public = 1
</cfquery>

<cfset product_count = product_count_private.total + product_count_public.total>

<!--- People Count --->

<cfquery name="comp_people" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_cb_id from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="people" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(cb_people.uuid) as total from cb_people
 left join cb_people_descriptions on cb_people_descriptions.uuid = cb_people.uuid
 where cb_people.featured_job_organization_uuid = '#comp_people.company_cb_id#'
</cfquery>

<!--- Certs --->

<cfquery name="certs_count_private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(cert_id) as total from cert
 where cert_company_id = #session.company_profile_id# and
       cert_public = 1
</cfquery>

<cfquery name="certs_count_public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(cert_id) as total from cert
 where cert_company_id = #session.company_profile_id# and
       cert_public = 1
</cfquery>

<cfset certs_count = certs_count_private.total + certs_count_public.total>

<cfquery name="funding" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 left join cb_organizations on cb_organizations.uuid = company_cb_id
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="comp_duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_duns from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfif comp_duns.company_duns is "">
 <cfset c_duns = 0>
<cfelse>
 <cfset c_duns = '#comp_duns.company_duns#'>
</cfif>

<cfquery name="child_duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(recipient_duns) from award_data
 where recipient_parent_duns = '#session.company_profile_duns#'
</cfquery>

<cfif child_duns.recordcount is 0>
 <cfset child_duns_list = 0>
<cfelse>
 <cfset child_duns_list = valuelist(child_duns.recipient_duns)>
</cfif>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(recipient_duns)) as total from award_data where recipient_parent_duns = '#c_duns#'
</cfquery>

<cfquery name="contacts" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(company_contact_id) as total from company_contact
 where company_contact_company_id = #session.company_profile_id# and
       company_contact_hub_id = #session.hub#
</cfquery>

<!--- Patents --->

<cfquery name="patent_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(patent_id)) as total from patent_rawassignee
 where organization = '#name.company_name#'
</cfquery>

<!--- Media --->

<cfquery name="media_count_private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(media_id) as total from media
 where media_company_id = #session.company_profile_id# and
       media_public = 1
</cfquery>

<cfquery name="media_count_public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(media_id) as total from media
 where media_company_id = #session.company_profile_id# and
       media_public = 1
</cfquery>

<cfset media_count = media_count_private.total + media_count_public.total>

<!--- Customers --->

<cfquery name="customer_count_private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(customer_id) as total from customer
 where customer_company_id = #session.company_profile_id# and
       customer_public = 1
</cfquery>

<cfquery name="customer_count_public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(customer_id) as total from customer
 where customer_company_id = #session.company_profile_id# and
       customer_public = 1
</cfquery>

<cfset customer_count = customer_count_private.total + customer_count_public.total>

<!--- Company Comments --->

<cfquery name="intel_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(company_intel_id) as total from company_intel
 where company_intel_company_id = #session.company_profile_id# and
       company_intel_hub_id = #session.hub# and
       (company_intel_created_by_usr_id = #session.usr_id# or (company_intel_sharing = 1 and company_intel_created_by_company_id = #session.company_id#))
</cfquery>

<!--- Partners --->

<cfquery name="partner_count_private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(partner_id) as total from partner
 where partner_company_id = #session.company_profile_id# and
       partner_public = 1
</cfquery>

<cfquery name="partner_count_public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(partner_id) as total from partner
 where partner_company_id = #session.company_profile_id# and
       partner_public = 1
</cfquery>

<cfset partner_count = partner_count_private.total + partner_count_public.total>

<!--- Marketing --->

<cfquery name="marketing_count_private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(doc_id) as total from doc
 where doc_company_id = #session.company_profile_id# and
       doc_public = 1
</cfquery>

<cfquery name="marketing_count_public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(doc_id) as total from doc
 where doc_company_id = #session.company_profile_id# and
       doc_public = 1
</cfquery>

<cfset marketing_count = marketing_count_private.total + marketing_count_public.total>

<!--- Contract Vehicles --->

<cfquery name="cvehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cv
 order by cv_name
</cfquery>

<cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_duns from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="lake_vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(parent_award_id) from award_data
 where recipient_duns = '#duns.company_duns#' or
       recipient_parent_duns = '#duns.company_duns#'
</cfquery>

<cfset vehicles = 0>

<cfloop query="cvehicles">

	<cfquery name="company_vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
      select count(id) as total from award_data
      where (recipient_duns = '#duns.company_duns#' or recipient_parent_duns = '#duns.company_duns#') and
            parent_award_id like '#cvehicles.cv_pattern#%'
	</cfquery>

	<cfif company_vehicles.total GT 0>
	 <cfset vehicles = vehicles + 1>
	</cfif>

</cfloop>

<cfif session.company_profile_duns is not "">

	<cfquery name="award_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select count(id) as total from award_data
	 where (recipient_duns = '#session.company_profile_duns#' or
	       recipient_parent_duns = '#session.company_profile_duns#')
	</cfquery>

	<cfquery name="subcontracts_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select count(distinct(prime_awardee_duns)) as total from award_data_sub
	 where (subawardee_duns = '#session.company_profile_duns#' or
	       subawardee_parent_duns = '#session.company_profile_duns#')
	</cfquery>

	<cfquery name="sub_parent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select distinct(recipient_duns) from award_data
	 where recipient_parent_duns = '#session.company_profile_duns#'
	</cfquery>

	<cfif sub_parent.recordcount is 0>
	 <cfset p_list = 0>
	<cfelse>
	 <cfset p_list = valuelist(sub_parent.recipient_duns)>
	</cfif>

	<cfquery name="sub_data" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select distinct(subawardee_duns) from award_data_sub
	 where (prime_awardee_duns = '#session.company_profile_duns#'

	 <cfif p_list is not 0>
	  <cfloop index="p" list="#p_list#">
	   or prime_awardee_duns = '#p#'
	  </cfloop>
	</cfif>

	)
	</cfquery>

	<cfquery name="grants_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select count(id) as total from grants
	 where (recipient_duns = '#session.company_profile_duns#' or
	       recipient_parent_duns = '#session.company_profile_duns#')
	</cfquery>

	<cfif #session.company_profile_duns# is "">
	 <cfset sbir_count.total = 0>
	<cfelse>
	 <cfquery name="sbir_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  select count(id) as total from sbir
	  where duns = '#session.company_profile_duns#'

	  <cfif child_duns_list is not 0>
	   <cfloop index="i" list="#child_duns_list#">
         or duns = '#i#'
	   </cfloop>
	 </cfif>


	 </cfquery>
	</cfif>

</cfif>

<style>
.tooltip2 {
  position: relative;
  display: inline-block;
}
.tooltip2 .tooltiptext {
  visibility: hidden;
  width: 220px;
  font-size: 12;
  background-color: black;
  color: #fff;
  text-align: left;
  border-radius: 6px;
  padding-left: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 10px;
  /* Position the tooltip */
  position: absolute;
  z-index: 1;
  top: -5px;
  left: 105%;
}
.tooltip2:hover .tooltiptext {
  visibility: visible;
}
</style>

<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Profile</td></tr>
		   <tr><td colspan=3><hr></td></tr>
           <tr><td height=20></td></tr>

		   <tr><td width=20><cfif not isdefined("l")><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" height=25 style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" colspan=2><a href="/exchange/include/profile.cfm">Overview</a></td></tr>
		   <tr><td height=15></td></tr>
    </table>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=20></td></tr>
		   <tr><td class="feed_header" style="font-size: 20;">Local Information</td>
		   <td align=right>

			 <div class="tooltip2" align=right><img src="/images/question.png" width=30>
			  <span class="tooltiptext">Information in this section is proprietary and specific to this Exchange only.</span>
			 </div>

		   </td></tr>

    </table>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td colspan=3><hr></td></tr>
		   <tr><td height=10></td></tr>
           <tr><td width=20><cfif isdefined("l") and (l is 30 or l is 31 or l is 32)><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=30">Company Comments</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ffffff; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #intel_count.total# GT 0>#intel_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 8><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=8">Company Contacts</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##ffffff; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #contacts.total# GT 0>#contacts.total#<cfelse>0</cfif></td></tr>
	   	   <tr><td height=15></td></tr>
	       <tr><td width=20><cfif isdefined("l") and l is 19><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=19">Agreements</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##ffffff; font-size: 14; border-radius: 100px;" height=25 width=35 align=center><cfif #agreements.total# GT 0>#agreements.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>

           <!---
		   <tr><td width=20><cfif isdefined("l") and (l is 30 or l is 31 or l is 32)><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=30">Company Comments</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##d0d0d0; color: ##000000; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #intel_count.total# GT 0>#intel_count.total#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr> --->

     </table>
               <!--- Custom Fields Start --->

	           <cfif isdefined("session.hub")>

	           <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<cfquery name="sections" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select * from section
					 where section_hub_id = #session.hub#
					 order by section_order
					</cfquery>

					<cfif sections.recordcount GT 0>

						<cfloop query="sections">

					    <cfquery name="section_count" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					     select count(distinct(section_field_value_field_id)) as total from section_field_value
					     where section_field_value_section_id = #sections.section_id# and
					           section_field_value_company_id = #session.company_profile_id#
					    </cfquery>

						<cfoutput>

			                <tr><td width=20><cfif isdefined("l") and l is 100><cfif isdefined("session.section_selected") and session.section_selected is #sections.section_id#><img src="/images/icon_selected.png" width=6 height=25></cfif><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/set_other.cfm?l=100&section_id=#sections.section_id#">#sections.section_name#</a></td><td height=25 class="feed_sub_header"><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: ##025870; color: ##ffffff; font-size: 14; border-radius: 100px;" height=25 width=25 align=center><cfif #section_count.total# GT 0>#section_count.total#<cfelse>0</cfif></td></tr>
				            <tr><td height=15></td></tr>

						</cfoutput>

					   </cfloop>

				   </cfif>

	           </table>

	           </cfif>

		       <!--- Custom Fields End --->

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=20></td></tr>
		   <tr><td class="feed_header" style="font-size: 20;">General Information</td>
		       <td align=right>

			 <div class="tooltip2" align=right><img src="/images/question.png" width=30>
			  <span class="tooltiptext">Information in this section is collected from public and subscription based sources and is managed and maintained externally.</span>
			 </div>


		       </td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=10></td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>


		   <tr><td width=20><cfif isdefined("l") and l is 45><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=45" onclick="javascript:document.getElementById('page-loader').style.display='block';">Companies</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 width=35 align=center><cfif #companies.recordcount# is "">0<cfelse>#companies.total#</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 3><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=3">Products & Services</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #product_count# GT 0>#product_count#<cfelse>0</cfif></td></tr>
           <tr><td height=15></td></tr>

		   <tr><td width=20><cfif isdefined("l") and l is 16><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=16">Patents</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px; padding-left: 2px; padding-right: 2px;" height=25 align=center>

		   <cfif #patent_count.total# is 0>
		    0
		   <cfelseif #patent_count.total# LT 1000>
			#trim(numberformat(patent_count.total,'999'))#

		   <cfelseif #patent_count.total# GTE 1000>
             <cfset value = #numberformat(evaluate(patent_count.total/1000),'99.9')#>
			   #value#k
		   </cfif>

		   </td></tr>

		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 50><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=50">Innovations</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #innovation_count# GT 0>#innovation_count#<cfelse>0</cfif></td></tr>

		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 35><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=35" onclick="javascript:document.getElementById('page-loader').style.display='block';">Funding Rounds</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #funding.num_funding_rounds# is "">0<cfelse>#funding.num_funding_rounds#</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and (l is 46 or l is 33)><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=33">People</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #people.total# is "">0<cfelse>#people.total#</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 40><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=40">Certifications</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #certs_count# GT 0>#certs_count#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>

		   <tr><td width=20><cfif isdefined("l") and l is 4><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=4">Customer Snapshots</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #customer_count# GT 0>#customer_count#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 5><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=5">Partners & Alliances</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #partner_count# GT 0>#partner_count#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 7><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=7">Press & Media</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #media_count# GT 0>#media_count#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
		   <tr><td width=20><cfif isdefined("l") and l is 6><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=6">Marketing Material</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 100px;" height=25 align=center><cfif #marketing_count# GT 0>#marketing_count#<cfelse>0</cfif></td></tr>
		   <tr><td height=15></td></tr>
    </table>


	<table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td height=20></td></tr>
		   <tr><td class="feed_header" style="font-size: 20;">Federal Information</td>
		   <td align=right>

			 <div class="tooltip2" align=right><img src="/images/question.png" width=30>
			  <span class="tooltiptext">Information in this section is extracted from public sources of information and managed and maintained externally.</span>
			 </div>

		   </td></tr>

		   <tr><td colspan=2><hr></td></tr>

    </table>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td height=15></td></tr>

					   <cfif session.company_profile_duns is not "">

						   <tr><td width=20><cfif isdefined("l") and l is 10><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=10" onclick="javascript:document.getElementById('page-loader').style.display='block';">Federal Awards</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 1000px;" width=35 height=25 align=center>

						   <cfif award_count.total GT 1000>

                            <cfset value = #numberformat(evaluate(award_count.total/1000),'99.9')#>

						   #value#k

						   <cfelse>
						   #trim(numberformat(award_count.total,'999'))#
						   </cfif>


						   </td></tr>

						   <tr><td height=15></td></tr>
						   <tr><td width=20><cfif isdefined("l") and l is 20><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=20" onclick="javascript:document.getElementById('page-loader').style.display='block';">Contract Vehicles</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 1000px;" width=35 height=25 align=center>#vehicles#</td></tr>
						   <tr><td height=15></td></tr>
						   <tr><td width=20><cfif isdefined("l") and (l is 11 or l is 15)><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><cfif isdefined("l") and (l is 11 or l is 15)><a href="/exchange/include/profile.cfm?l=#l#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><cfelse><a href="/exchange/include/profile.cfm?l=11" onclick="javascript:document.getElementById('page-loader').style.display='block';"></cfif>Subcontracts</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 1000px;" width=35 height=25 align=center>#trim(numberformat(subcontracts_count.total,'999,999'))#</td></tr>
						   <tr><td height=15></td></tr>
						   <tr><td width=20><cfif isdefined("l") and l is 12><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=12" onclick="javascript:document.getElementById('page-loader').style.display='block';">Subcontractors</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 1000px;" width=35 height=25 align=center>

						   <cfif #sub_data.recordcount# is 0>
							0
						   <cfelseif sub_data.recordcount LT 1000>
							#trim(numberformat(sub_data.recordcount,'999'))#

						   <cfelseif sub_data.recordcount GTE 1000>
							 <cfset value = #numberformat(evaluate(sub_data.recordcount/1000),'99.9')#>
							   #value#k
						   </cfif>

						   </td></tr>
						   <tr><td height=15></td></tr>
						   <tr><td width=20><cfif isdefined("l") and l is 13><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=13" onclick="javascript:document.getElementById('page-loader').style.display='block';">Grants</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 1000px;" width=35 height=25 align=center>#trim(numberformat(grants_count.total,'999,999'))#</td></tr>
						   <tr><td height=15></td></tr>
						   <tr><td width=20><cfif isdefined("l") and l is 14><img src="/images/icon_selected.png" width=6 height=25><cfelse>&nbsp;</cfif></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;" height=25><a href="/exchange/include/profile.cfm?l=14" onclick="javascript:document.getElementById('page-loader').style.display='block';">SBIR/STTRs</a></td><td class="feed_sub_header" style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px; background-color: 253746; color: ##FFFFFF; font-size: 14; border-radius: 1000px;" width=35 height=25 align=center>#trim(numberformat(sbir_count.total,'999,999'))#</td></tr>

					   <cfelse>

						   <tr><td colspan=2></td><td colspan=2 class="feed_option">No Federal information was found on the selected Company.</td></tr>

					   </cfif>

				</table>

</cfoutput>