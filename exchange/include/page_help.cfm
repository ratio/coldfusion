<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_exchange_admin from usr
 where usr_id = #session.usr_id#
</cfquery>

<center>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">HELP</td>
             <td class="feed_header" align=right><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
         <tr><td colspan=2><hr></td></tr>
         <tr><td height=10></td></tr>

         <cfif isdefined("u")>
          <tr><td class="feed_option"><font color="green"><b>Page help saved.</b></font></td></tr>
          <tr><td height=10></td></tr>
         </cfif>

</table>

<cfquery name="help" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from page
 where page_url = '#page#'
</cfquery>

<div class="white">

<cfif #usr.usr_exchange_admin# is 1>

    <cfif isdefined("ev")>

        <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header"><b>#ucase(help.page_name)#</b></td>
			     <td class="feed_option" align=right><a href="page_help.cfm?page=#page#">Edit View</a>&nbsp;|&nbsp;<a href="page_help.cfm?page=#page#&ev=1">Public View</a></td></tr>
			 <tr><td height=5></td></tr>
			 <cfif help.page_help is "">

			 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>No help has been written for this feature.</td></tr>
			 <cfelse>
			 <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#replace(help.page_help,"#chr(10)#","<br>","all")#</td></tr>

			 </cfif>
			</table>

		</cfoutput>

    <cfelse>

	<form action="page_help_db.cfm" method="post">

    <cfoutput>

  	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_sub_header">Page Name</td>
	         <td class="feed_option" align=right><a href="page_help.cfm?page=#page#">Edit View</a>&nbsp;|&nbsp;<a href="page_help.cfm?page=#page#&ev=1">Public View</a></td></tr>
    	 <tr><td class="feed_option" colspan=2><input type="text" class="input_text" name="page_name" size=50 value="#help.page_name#"></td></tr>
		 <tr><td class="feed_sub_header" colspan=2 valign=top>Help Content</td></tr>
		 <tr><td class="feed_option" colspan=2><textarea name="page_help" class="input_textarea" rows=16 cols=130>#help.page_help#</textarea></td></tr>
		 <tr><td height=5></td></tr>
		 <tr><td><input type="submit" name="button" class="button_blue_large" value="Save">
		 <input type="hidden" name="page_url" value="#page#">
	   </table>

	</cfoutput>

	</form>

    </cfif>

<cfelse>

        <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header"><cfif #help.page_name# is "">Page Name<cfelse><b>#help.page_name#</b></cfif></td></tr>
			 <tr><td height=5></td></tr>
			 <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #help.page_help# is "">No help has been created for this page.<cfelse>#replace(help.page_help,"#chr(10)#","<br>","all")#</cfif></td></tr>
			</table>

		</cfoutput>

</cfif>

</center>

</div>

</body>
</html>

