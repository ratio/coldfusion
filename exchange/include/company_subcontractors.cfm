<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header" style="font-size: 30;">Subcontractors</td>

		<form action="sub_filter.cfm" method="post">
			<td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter</b>&nbsp;&nbsp;
			<input class="input_text" type="text" size=30 name="filter" <cfif isdefined("session.sub_profile_filter")><cfif session.sub_profile_filter is not 0>value="<cfoutput>#session.sub_profile_filter#</cfoutput>"</cfif></cfif> placeholder="i.e., Machine Learning, Finance, etc.">&nbsp;&nbsp;
			<input class="button_blue" type="submit" name="button" value="Apply">&nbsp;
			<input class="button_blue" type="submit" name="button" value="Clear">
			</td>
		</form>

        </tr>

   <tr><td colspan=2><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<cfif not isdefined("session.sub_profile_filter")>
 <cfset session.sub_profile_filter = 0>
</cfif>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfset perpage = 200>

<cfquery name="parent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(recipient_duns) from award_data
 where recipient_parent_duns = '#session.company_profile_duns#'
</cfquery>

<cfif parent.recordcount is 0>
 <cfset parent_list = 0>
<cfelse>
 <cfset parent_list = valuelist(parent.recipient_duns)>
</cfif>

<cfquery name="sub" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select subaward_number, prime_awardee_name, subawardee_city_name, subawardee_state_code, subaward_description, id, prime_awardee_duns, subawardee_name, prime_awarding_agency_name, subawardee_duns, subaward_action_date, prime_award_parent_piid, prime_award_piid, prime_awarding_sub_agency_name, prime_awarding_office_name, subaward_amount from award_data_sub
 where (prime_awardee_duns = '#session.company_profile_duns#'

 <cfif parent_list is not 0>
  <cfloop index="p" list="#parent_list#">
   or prime_awardee_duns = '#p#'
  </cfloop>
</cfif>

 )

 <cfif session.sub_profile_filter is not 0>
  and contains((*),'"#trim(session.sub_profile_filter)#"')
 </cfif>

	<cfif #sv# is 1>
	 order by subaward_action_date DESC
	<cfelseif #sv# is 10>
	 order by subaward_action_date ASC
	<cfelseif #sv# is 2>
	 order by subawardee_name ASC
	<cfelseif #sv# is 20>
	 order by subawardee_name DESC
	<cfelseif #sv# is 3>
	 order by subaward_number ASC
	<cfelseif #sv# is 30>
	 order by subaward_number DESC
	<cfelseif #sv# is 4>
	 order by prime_award_piid ASC
	<cfelseif #sv# is 40>
	 order by prime_award_piid DESC
	<cfelseif #sv# is 5>
	 order by prime_awarding_agency_name ASC
	<cfelseif #sv# is 50>
	 order by prime_awarding_agency_name DESC
	<cfelseif #sv# is 6>
	 order by prime_awarding_sub_agency_name ASC
	<cfelseif #sv# is 60>
	 order by prime_awarding_sub_agency_name DESC
	<cfelseif #sv# is 7>
	 order by subaward_amount DESC
	<cfelseif #sv# is 70>
	 order by subaward_amount ASC

	<cfelseif #sv# is 8>
	 order by subawardee_city_name ASC
	<cfelseif #sv# is 80>
	 order by subawardee_city_name DESC
	<cfelseif #sv# is 9>
	 order by subawardee_state_code ASC
	<cfelseif #sv# is 90>
	 order by subawardee_state_code DESC

	<cfelseif #sv# is 11>
	 order by prime_awardee_name ASC
	<cfelseif #sv# is 110>
	 order by prime_awardee_name DESC


	</cfif>

</cfquery>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt sub.recordCount or round(url.start) neq url.start>
    <cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(sub.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif sub.recordcount is 0>
		 <tr><td class="feed_sub_header">No subcontractors were found.</td></tr>
		<cfelse>

	    <cfoutput>
	    <tr><td class="feed_sub_header"><b>Total Subcontractor Awards Found - #numberformat(sub.recordcount,'99,999')#</b></td>
		    <td class="feed_option" align=right colspan=2>

		   <td align=right class="feed_sub_header">

			<cfoutput>
				<cfif sub.recordcount GT #perpage#>
					<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

					<cfif url.start gt 1>
					    <cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&l=12&sv=#sv#">
						<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>

					<cfif (url.start + perpage - 1) lt sub.recordCount>
						<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&l=12&sv=#sv#">
						<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 align=absmiddle></a>
					<cfelse>
					</cfif>
				</cfif>
			</cfoutput>

		    </td></tr>
	    </cfoutput>

        </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td>

					<tr height=50>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATE</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PRIME</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>SUBCONTRACTOR</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>CITY</b></a></td>
					   <td class="text_xsmall" align=center><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>STATE</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>SUBCONTRACT #</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>PRIME CONTRACT #</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>DEPARTMENT</b></a></td>
					   <td class="text_xsmall"><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>AGENCY</b></a></td>
					   <td class="text_xsmall"><b>DESCRIPTION</b></td>
					   <td class="text_xsmall" align=right><a href="/exchange/include/profile.cfm?l=12&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>AWARD</b></a></td>
					</tr>

					<tr><td height=5></td></tr>

					<cfset #counter# = 0>
					<cfset #sc_total# = 0>

					<cfoutput query="sub" startrow="#url.start#" maxrows="#perpage#">

					 <cfif counter is 0>
					  <tr bgcolor="ffffff" height=30>
					 <cfelse>
					  <tr bgcolor="f0f0f0" height=30>
					 </cfif>

						 <td class="text_xsmall" valign=top width=75 style="padding-right: 20px;">#dateformat(subaward_action_date,'mm/dd/yyyy')#</td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=200><b>#prime_awardee_name#</b></a></td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;"><a href="/exchange/include/federal_profile.cfm?duns=#subawardee_duns#" target="_blank" rel="noopener"><b>#subawardee_name#</b></a></td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;">#subawardee_city_name#</td>
						 <td class="text_xsmall" valign=top align=center>#subawardee_state_code#</td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=125><a href="/exchange/include/sub_award_information.cfm?id=#sub.id#" target="_blank" rel="noopener"><b>#subaward_number#</b></a></td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=125><a href="/exchange/include/award_information.cfm?prime_award_id=#prime_award_piid#&duns=#prime_awardee_duns#" target="_blank" rel="noopener"><b>#prime_award_piid#</b></a></td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=200>#prime_awarding_agency_name#</td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=200>#prime_awarding_sub_agency_name#</td>
						 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=600>

                         <cfif session.sub_profile_filter is not 0>
                          #replaceNoCase(subaward_description,session.sub_profile_filter,"<span style='background:yellow'>#ucase(session.sub_profile_filter)#</span>","all")#
                         <cfelse>
						  #subaward_description#
						 </cfif>

						 </td>
						 <td class="text_xsmall" valign=top align=right>#numberformat(subaward_amount,'$999,999,999,999')#</td>

					 </tr>

					 <cfif counter is 0>
					  <cfset counter = 1>
					 <cfelse>
					  <cfset counter = 0>
					 </cfif>

					</cfoutput>

					<tr><td height=10></td></tr>
					<tr><td class="text_xsmall" colspan=7><b>Note -</b> this may not include information from subcontractors where the Prime was not legally obligated to report them.</td></tr>

	   </cfif>

</table>
