<cfquery name="profile" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_cb_id = '#i#'
</cfquery>

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_person_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#profile.person_id#,#session.usr_id#, #session.company_id#,#session.hub#,#now()#)
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=2 class="feed_header" style="font-size: 30;">Company People</td>
       <td align=right class="feed_sub_header"><a href="profile.cfm?l=33">Return</a>
       </td></tr>

   <tr><td colspan=3><hr></td></tr>
   <tr><td height=20></td></tr>
</table>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr><td height=20></td></tr>

		   <tr><td valign=top width=18%>

		   <cfinclude template="company_people_detail_left.cfm">

           </td><td width=40>&nbsp;</td><td valign=top width=100%>

           <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				  <tr>
					  <td class="feed_header" style="font-size: 40px; padding-bottom: 0px;">#profile.person_first_name# #profile.person_last_name#</td>
					  <td class="feed_header" align=right>
				  </tr>

              </table>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_sub_header" colspan=2 style="font-size: 24px;">#profile.person_company#</td></tr>
				<tr><td class="feed_sub_header">#profile.person_title#</td></tr>
				<tr><td height=10></td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><td height=10></td></tr>
				<tr><td class="feed_header">Snapshot</td></tr>
				<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>#profile.person_desc#</td></tr>
			  </table>

		   </cfoutput>

			  <cfquery name="jobs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select * from cb_jobs
			   left join cb_organizations on cb_organizations.uuid = org_uuid
			   where person_uuid = '#profile.person_cb_id#'
			   order by started_on DESC
			  </cfquery>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td height=10></td></tr>
				<tr><td class="feed_header">History</td></tr>
				<tr><td height=10></td></tr>

				<cfif jobs.recordcount is 0>
					<tr><td class="feed_sub_header" style="font-weight: normal;">No history found.</td></tr>
			    <cfelse>

			      <cfset count = 1>

                  <cfoutput query="jobs">
                   <tr>
                   <td width=70 valign=top>

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td class="feed_sub_header"><img src="#logo_url#" width=50 onerror="this.onerror=null; this.src='/images/stock_company.png'"></td></tr>
				      </table>

                   </td><td valign=top>

					  <table cellspacing=0 cellpadding=0 border=0 width=100%>
                        <tr><td class="feed_sub_header" style="padding-bottom: 5px;">#jobs.org_name# <cfif jobs.is_current is 'true'>( Current )</cfif></td>
                            <td class="feed_sub_header" style="padding-bottom: 5px;" align=right>#dateformat(jobs.started_on,'mmm dd, yyyy')# - <cfif jobs.ended_on is "">Current<cfelse>#dateformat(jobs.ended_on,'mmm dd, yyyy')#</cfif></td></tr>
                        <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#jobs.title#</td>
                            <td class="link_small_gray" style="font-weight: normal; padding-top: 0px;" align=right>

                            </td></tr>

                        <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;">#jobs.short_description#</td></tr>
                        <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;">#jobs.category_groups_list#</td></tr>
                        <tr><td height=10></td></tr>


                        </tr>
                      </table>

                      </td></tr>

                  <cfif count LT jobs.recordcount>
                  <tr><td colspan=2><hr></td></tr>
                  </cfif>

                  <cfset count = count + 1>

                  </cfoutput>

			    </cfif>
			  </table>

            </td></tr>

 		  </table>