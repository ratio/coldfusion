<cfinclude template="/exchange/security/check.cfm">

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_opp_sbir_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_sbir
 where id = #id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=top>SBIR/STTR INFORMATION</td>
             <td class="feed_header" align=right class="feed_sub_header" valign=top>
             <cfoutput>
              <img src="/images/delete.png" align=absmiddle border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
             </cfoutput></td></tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header" width=250>DEPARTMENT</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#ucase(sbir.department)#</td></tr>
			 <tr><td class="feed_sub_header" width=250>AGENCY</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#ucase(sbir.agency)#</td></tr>
			 <tr><td class="feed_sub_header" width=250>ACQUISTION PROGRAM</td>
			     <td class="feed_sub_header" style="font-weight: normal;"><cfif #sbir.acquisitionprogram# is "">NOT PROVIDED<cfelse>#ucase(sbir.acquisitionprogram)#</cfif></td></tr>
			 <tr><td class="feed_sub_header">SOLICIATION NUMBER</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#sbir.solicitationnumber#</td></tr>
             <tr><td class="feed_sub_header">TECHNOLOGY AREA</td>
                 <td class="feed_sub_header" style="font-weight: normal;">#ucase(sbir.technologyarea)#</td></tr>
         </table>

         </td><td width=50>&nbsp;</td><td valign=top width=30%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header">PROGRAM</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#ucase(sbir.program)#</td></tr>
				 <tr><td class="feed_sub_header">RELEASE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(sbir.releasedate,'mm/dd/yyyy')#</td></tr>
				 <tr><td class="feed_sub_header">OPEN DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(sbir.opendate,'mm/dd/yyyy')#</td></tr>
				 <tr><td class="feed_sub_header">CLOSE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(sbir.closedate,'mm/dd/yyyy')#</td></tr>
			 </table>

         </td></tr>

         <tr><td colspan=3><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_sub_header">TOPIC</td></tr>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.title,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.title#
			 </cfif>

		 </td></tr>

		 <tr><td class="feed_sub_header">OBJECTIVE</td></tr>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.objective,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.objective#
			 </cfif>

		 </td></tr>
         <tr><td class="feed_sub_header">DESCRIPTION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.description,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.description#
			 </cfif>

         </td></tr>
         <tr><td class="feed_sub_header">KEYWORDS</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.keywords,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.keywords#
			 </cfif>

         </td></tr>

         <tr><td><hr></td></tr>
         <tr><td class="feed_sub_header">PHASE I</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.phasei,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.phasei#
			 </cfif>

         </td></tr>

         <tr><td class="feed_sub_header">PHASE II</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.phaseii,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.phaseii#
			 </cfif>

         </td></tr>

         <tr><td class="feed_sub_header">PHASE III</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.sbir_keyword")>
			  #replaceNoCase(sbir.phaseiii,session.sbir_keyword,"<span style='background:yellow'>#session.sbir_keyword#</span>","all")#
			 <cfelse>
			 #sbir.phaseiii#
			 </cfif>

         </td></tr>

         <tr><td><hr></td></tr>
         <tr><td class="feed_sub_header">CONTACT INFORMATION</td></tr>

         <cfif #sbir.name1# is not "">

			 <tr><td class="feed_sub_header" style="font-weight: normal;">
			 <b>#sbir.name1#</b><br>
			 Phone - #sbir.phone1#<br>
			 Email - <cfif #sbir.email1# is "">Not provided<cfelse>#sbir.email1#</cfif>
			</td></tr>

        </cfif>

        <cfif #sbir.name2# is not "">

			 <tr><td class="feed_sub_header" style="font-weight: normal;">
			 <b>#sbir.name2#</b><br>
			 Phone - #sbir.phone2#<br>
			 Email - <cfif #sbir.email2# is "">Not provided<cfelse>#sbir.email2#</cfif>
			</td></tr>

        </cfif>

        <tr><td><hr></td></tr>
        <tr><td class="feed_sub_header">REFERENCING URL</td></tr>
        <tr><td class="feed_sub_header"><a href="#sbir.url#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer">#sbir.url#</a></td></tr>

        <tr><td><hr></td></tr>
        <tr><td class="feed_sub_header">REFERENCES</td></tr>
        <tr><td class="feed_sub_header" style="font-weight: normal;">
        <cfif #sbir.sbirreferences# is "">
          None references are provided.
        <cfelse>
          #(replace(sbir.sbirreferences,"#chr(10)#","<br><br>","all"))#
        </cfif>
        </a></td></tr>

        <tr><td><hr></td></tr>
        <tr><td class="feed_sub_header">QUESTIONS & ANSWERS</td></tr>
        <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif #sbir.questionsandanswers# is "">No questions have been received.<cfelse>#sbir.questionsandanswers#</cfif></a></td></tr>


       </table>

       </cfoutput>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

