<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td class="feed_header" style="font-size: 30;">SBIR/STTR Awards</td>
   <cfoutput>
       <td class="feed_sub_header" align=right>

       	<form action="filter.cfm" method="post">
	   		<td class="feed_sub_header" style="font-weight: normal;" align=right><b>Filter</b>&nbsp;&nbsp;
    		<input class="input_text" type="text" size=30 name="filter" placeholder="enter keyword" <cfif isdefined("session.profile_filter")>value='#replace(session.profile_filter,'"','','all')#'</cfif>>&nbsp;&nbsp;
	   		<input class="button_blue" type="submit" name="button" value="Apply">&nbsp;
	   		<input class="button_blue" type="submit" name="button" value="Clear">
	   		<input type="hidden" name="location" value="SBIR">
	   		</td>
	    </form>
	  </cfoutput>

       </td>

   </tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td colspan=10>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<cfquery name="sub_parent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					 select distinct(recipient_duns) from award_data
					 where recipient_parent_duns = '#session.company_profile_duns#'
					</cfquery>

					<cfif sub_parent.recordcount is 0>
					 <cfset p_list = 0>
					<cfelse>
					 <cfset p_list = valuelist(sub_parent.recipient_duns)>
					</cfif>

					<cfquery name="sub_data" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					 select distinct(subawardee_duns) from award_data_sub
					 where (prime_awardee_duns = '#session.company_profile_duns#'

					 <cfif p_list is not 0>
					  <cfloop index="p" list="#p_list#">
					   or prime_awardee_duns = '#p#'
					  </cfloop>
					</cfif>

					)
					</cfquery>


					<cfif #session.company_profile_duns# is "">
						<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
							 select * from sbir
							 where duns = '-9399383'
							 order by award_start_date DESC
						</cfquery>
					<cfelse>
						<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
							 select * from sbir
							 where (duns = '#session.company_profile_duns#'


                             <cfif p_list is not 0>
                              <cfloop index="p" list="#p_list#">
                               or duns = '#p#'
                              </cfloop>
                             </cfif>


                             )


							<cfif isdefined("session.profile_filter")>
								and contains((award_title, department, research_keywords, abstract),'#trim(session.profile_filter)#')
							</cfif>

							 order by award_start_date DESC
						</cfquery>
					</cfif>

           <cfif isdefined("export")>
             <cfinclude template="/exchange/include/export_to_excel.cfm">
           </cfif>

					<cfif agencies.recordcount is 0>

					 <tr><td class="feed_sub_header">No SBIR or STTR's were found.</td></tr>

					<cfelse>

	                <cfoutput>

					 <tr><td class="feed_sub_header" colspan=8>SBIR/STTRs Found - #numberformat(agencies.recordcount,'999,999')#</td>
					     <td class="feed_sub_headaer" align=right><a href="/exchange/include/profile.cfm?l=14&export=1"><img src="/images/icon_export_excel.png" alt="Export to Excel" title="Export to Excel" width=23 border=0 align=absmiddle></a>


								<tr height=50>
								   <td class="feed_option"><b>CONTRACT ##</b></td>
								   <td class="feed_option"><b>DEPARTMENT</b></td>
								   <td class="feed_option"><b>TITLE</b></td>
								   <td class="feed_option" align=center><b>YEAR</b></td>
								   <td class="feed_option"><b>PROGRAM</b></td>
								   <td class="feed_option"><b>PHASE</b></td>
								   <td class="feed_option"><b>START DATE</b></td>
								   <td class="feed_option"><b>END DATE</b></td>
								   <td class="feed_option" align=right><b>AMOUNT</b></td>
								</tr>

					</cfoutput>

								<cfset #counter# = 0>
								<cfset #sc_total# = 0>

								<cfoutput query="agencies">

								 <cfif counter is 0>
								  <tr bgcolor="ffffff" height=30>
								 <cfelse>
								  <tr bgcolor="f0f0f0" height=30>
								 </cfif>

									 <td class="text_xsmall" width=125><a href="/exchange/sbir/detail.cfm?id=#id#" target="_blank" rel="noopener"><b>#contract#</b></a></td>
								     <td class="text_xsmall">#department#</td>
									 <td class="text_xsmall">#award_title#</td>
									 <td class="text_xsmall" width=75 align=center>#award_year#</td>
									 <td class="text_xsmall" width=75>#program#</td>
									 <td class="text_xsmall" width=75>#phase#</td>
									 <td class="text_xsmall" width=75>#dateformat(award_start_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" width=75>#dateformat(award_close_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall" align=right>#numberformat(award_amount,'$999,999,999,999')#</td>

								 </tr>

								 <cfif counter is 0>
								  <cfset counter = 1>
								 <cfelse>
								  <cfset counter = 0>
								 </cfif>

								 <cfif isnumeric(award_amount)>
								 <cfset #sc_total# = #sc_total# + #award_amount#>
								 </cfif>

								</cfoutput>

								<tr><td height=5></td></tr>
								<tr><td colspan=9><hr></td></tr>
								<tr><td height=5></td></tr>
								<tr><td class="feed_option" colspan=8><b>TOTAL</b></td>
									<td class="feed_option" align=right><b><cfoutput>#numberformat(sc_total,'$999,999,999,999')#</cfoutput></b></td></tr>

				   </cfif>

				   </table>

			   </td></tr>
</td></tr>
</table>
