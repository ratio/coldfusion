<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Funding</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 left join cb_organizations on cb_organizations.uuid = company_cb_id
 where company_id = #session.company_profile_id#
</cfquery>

		       <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <cfif #comp.num_funding_rounds# is "">

                <tr><td class="feed_sub_header">No Funding information could be found.</td></tr>

               <cfelse>

                <tr><td class="feed_sub_header" style="font-weight: normal;">Funding information is provided by <a href="www.crunchbase.com" target="_blank" rel="noopener"><b><u>Crunchbase</u></b></a>.</td></tr>
                <tr><td><hr></td></tr>

                <tr><td class="feed_sub_header">FUNDING SUMMARY</td></tr>

                <tr><td class="feed_sub_header" style="font-weight: normal;">

				  <cfoutput>

				  #comp.num_funding_rounds# Rounds<br>
				  Total Funds Received - <cfif comp.total_funding_usd is "">Unknown<cfelse>#numberformat(comp.total_funding_usd,'$999,999,999')#</cfif>
				  <cfif #comp.last_funding_on# is not ""><br>Last Funding On #dateformat(comp.last_funding_on,'mmmm dd, yyyy')#</cfif>

				  </cfoutput>

                </td></tr>

                <tr><td height=10></td></tr>
                <tr><td><hr></td></tr>

				<cfquery name="rounds" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select * from cb_funding_rounds
				 where org_uuid = '#comp.company_cb_id#'
				 order by announced_on ASC
				</cfquery>

                <tr><td class="feed_sub_header">FUNDING ROUNDS</td></tr>

                <tr><td>

		        <table cellspacing=0 cellpadding=0 border=0 width=100%>

                <tr>
                   <td class="feed_sub_header">Lead Investor(s)</td>
                   <td class="feed_sub_header">Type</td>
                   <td class="feed_sub_header">Description</td>
                   <td class="feed_sub_header" align=center>Announced</td>
                   <td class="feed_sub_header" align=center>Investors</td>
                   <td class="feed_sub_header" align=right>Raised (USD)</td>
                   <td class="feed_sub_header" align=right>Post Money Eval (USD)</td>
                </tr>

                <cfset counter = 0>

                <cfloop query="rounds">

                 <cfif counter is 0>
                  <tr bgcolor="FFFFFF" height=40>
                 <cfelse>
                  <tr bgcolor="FFFFFF" height=40>
                 </cfif>

                 <td class="feed_sub_header">

                <cfif #rounds.lead_investor_uuids# is "">
                 <b>UNKNOWN INVESTOR</b>
                <cfelse>

				<cfloop index="i" list="#rounds.lead_investor_uuids#">

                <cfoutput>

					<cfquery name="investors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					 select name, cb_url, domain from cb_investors
					 where uuid = '#i#'
					</cfquery>

					<cfif #investors.name# is "">
					UNKNOWN INVESTOR
					<cfelse>

					<a href="#investors.cb_url#" target="_blank" rel="noopener"><img src="//logo.clearbit.com/#investors.domain#" width=40 border=0 onerror="this.src='/images/no_logo.png'" style="padding-right: 10px;"></a>
					<a href="#investors.cb_url#" target="_blank" rel="noopener"><b>#ucase(investors.name)#</b></a>

					</cfif>

				</cfoutput>

				<br>

				</cfloop>

				</cfif>

                 </td>

                 <cfoutput>

                 <td class="feed_sub_header" style="font-weight: normal; font-size: 15px;">#ucase(replace(rounds.investment_type,"_"," ","all"))#</td>
                 <td class="feed_sub_header" style="font-weight: normal; font-size: 15px;">#ucase(rounds.name)#</td>
                 <td class="feed_sub_header" style="font-weight: normal; font-size: 15px;" align=center>#dateformat(rounds.announced_on,'mm/dd/yyyy')#</td>
                 <td class="feed_sub_header" style="font-weight: normal; font-size: 15px;" align=center>#rounds.investor_count#</td>
                 <td class="feed_sub_header" style="font-weight: normal; font-size: 15px;" align=right>#numberformat(rounds.raised_amount_usd,'$999,999,999,999')#</td>
                 <td class="feed_sub_header" style="font-weight: normal; font-size: 15px;" align=right>#numberformat(rounds.post_money_valuation_usd,'$999,999,999,999')#</td>
                 </tr>

                 <cfif counter is 0>
                  <cfset counter = 1>
                 <cfelse>
                  <cfset counter = 0>
                 </cfif>

                 <tr><td colspan=7><hr></td></tr>

                </cfoutput>

                </cfloop>


                </table>

                </td></tr>


               </cfif>

               </table>


