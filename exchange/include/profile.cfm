<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("s")>

 <cfif s is "t">

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select company_duns from company
   where company_id = #id#
  </cfquery>

  <cfset session.company_profile_duns = #duns.company_duns#>
  <cfset session.company_profile_id = #id#>

 </cfif>
</cfif>

<cfif isdefined("ky")>
 <cfset session.profile_filter = #trim(ky)#>
</cfif>

<!--- Insert Recent --->

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_usr_id, recent_company_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#session.usr_id#,#session.company_profile_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<cfquery name="name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_name from company
 where company_id = #session.company_profile_id#
</cfquery>

<html>
<head>
	<title><cfif isdefined("session.company_profile_name")><cfoutput>#session.company_profile_name#</cfoutput><cfelse>Exchange</cfif></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/include/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top width=225>

		   <cfinclude template="/exchange/include/company_menu.cfm">

           </td><td valign=top width=70>&nbsp;</td><td valign=top>

              <cfif not isdefined("l")>
               <cfinclude template="/exchange/include/company_overview.cfm">
              <cfelse>
                <cfif l is 9>
                 <cfinclude template="/exchange/include/company_federal.cfm">
                <cfelseif l is 45>
                 <cfinclude template="/exchange/include/company_companies.cfm">
                <cfelseif l is 30>
                 <cfinclude template="/exchange/include/company_intel.cfm">
                <cfelseif l is 40>
                 <cfinclude template="/exchange/include/company_certs.cfm">
                <cfelseif l is 31>
                 <cfinclude template="/exchange/include/company_intel_add.cfm">
                <cfelseif l is 32>
                 <cfinclude template="/exchange/include/company_intel_edit.cfm">
                <cfelseif l is 3>
                 <cfinclude template="/exchange/include/company_products.cfm">
                <cfelseif l is 4>
                 <cfinclude template="/exchange/include/company_customers.cfm">
                <cfelseif l is 5>
                 <cfinclude template="/exchange/include/company_partners.cfm">
                <cfelseif l is 6>
                 <cfinclude template="/exchange/include/company_marketing.cfm">
                <cfelseif l is 7>
                 <cfinclude template="/exchange/include/company_media.cfm">
                <cfelseif l is 8>
                 <cfinclude template="/exchange/include/company_contacts.cfm">
                <cfelseif l is 35>
                 <cfinclude template="/exchange/include/company_funding.cfm">
                <cfelseif l is 10>
                 <cfinclude template="/exchange/include/company_awards.cfm">
                <cfelseif l is 11>
                 <cfinclude template="/exchange/include/company_subcontracts.cfm">
                <cfelseif l is 12>
                 <cfinclude template="/exchange/include/company_subcontractors.cfm">
                <cfelseif l is 13>
                 <cfinclude template="/exchange/include/company_grants.cfm">
                <cfelseif l is 14>
                 <cfinclude template="/exchange/include/company_sbir.cfm">
                <cfelseif l is 15>
                 <cfinclude template="/exchange/include/company_subcontracts_agency.cfm">
                <cfelseif l is 16>
                 <cfinclude template="/exchange/include/company_patents.cfm">
                <cfelseif l is 20>
                 <cfinclude template="/exchange/include/company_vehicles.cfm">
                <cfelseif l is 100>
                 <cfinclude template="/exchange/include/company_other.cfm">
                <cfelseif l is 19>
                 <cfinclude template="/exchange/include/company_agreements.cfm">
                <cfelseif l is 33>
                 <cfinclude template="/exchange/include/company_people.cfm">
                <cfelseif l is 46>
                 <cfinclude template="/exchange/include/company_people_detail.cfm">
                <cfelseif l is 50>
                 <cfinclude template="/exchange/include/company_innovations.cfm">
                </cfif>
              </cfif>

           </td></tr>

           <tr><td height=20></td></tr>

           </table>

           </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>


