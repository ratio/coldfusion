
		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr><td colspan=10><hr></td></tr>

			   <tr><td>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=8></td></tr>

			   <tr><td class="feed_header" colspan=10><a name=sbir><b>SBIR/STTR Awards</b></a></td></tr>
			   <tr><td height=5></td></tr>

			   <tr><td colspan=10>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

					<!--- SBIR --->

					<cfif #company.company_duns# is "">

						<cfquery name="sbir" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select * from sbir
						 where duns = '-999'
						 order by award_start_date
						</cfquery>

					<cfelse>

						<cfquery name="sbir" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						 select * from sbir
						 where (duns = '#company.company_duns#' or duns='0#company.company_duns#' or duns='00#company.company_duns#')
						 order by award_start_date
						</cfquery>

					</cfif>

			        <cfif subscription.premium_sbir is 1  or (isdefined("session.hub") and hub_subscription.premium_sbir is 1)>

					<cfif sbir.recordcount is 0>

					 <tr><td class="feed_option">No SBIR or STTR's were found.</td></tr>

					<cfelse>

					<cfoutput>

								<tr>
								   <td class="text_xsmall" width=75><b>Contract</b></td>
								   <td class="text_xsmall"><b>Start Date</b></td>
								   <td class="text_xsmall"><b>End Date</b></td>
								   <td class="text_xsmall"><b>Department</b></td>
								   <td class="text_xsmall"><b>Award Title</b></td>
								   <td class="text_xsmall"><b>Year</b></td>
								   <td class="text_xsmall"><b>Program</b></td>
								   <td class="text_xsmall"><b>Phase</b></td>
								   <td class="text_xsmall" align=right><b>Award Amount</b></td>
								</tr>

					</cfoutput>

								<tr><td height=5></td></tr>

								<cfset #counter# = 0>
								<cfset #sc_total# = 0>

								<cfoutput query="sbir">

								 <cfif counter is 0>
								  <tr bgcolor="ffffff">
								 <cfelse>
								  <tr bgcolor="e0e0e0">
								 </cfif>

									 <td class="text_xsmall"><a href="/exchange/sbir/detail.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#contract#</a></td>
									 <td class="text_xsmall">#dateformat(award_start_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall">#dateformat(award_close_date,'mm/dd/yyyy')#</td>
								     <td class="text_xsmall">#department#</td>
									 <td class="text_xsmall">#award_title#</td>
									 <td class="text_xsmall">#award_year#</td>
									 <td class="text_xsmall">#program#</td>
									 <td class="text_xsmall">#phase#</td>
									 <td class="text_xsmall" align=right>#numberformat(award_amount,'$999,999,999,999')#</td>

								 </tr>


								 <cfif counter is 0>
								  <cfset counter = 1>
								 <cfelse>
								  <cfset counter = 0>
								 </cfif>
								 <cfset #sc_total# = #sc_total# + #award_amount#>

								</cfoutput>

								<tr><td height=5></td></tr>
								<tr><td class="feed_option" colspan=8><b>Total</b></td>
									<td class="feed_option" align=right><b><cfoutput>#numberformat(sc_total,'$999,999,999,999')#</cfoutput></b></td></tr>

				   </cfif>

				   <cfelse>

					   <tr><td class="feed_option" colspan=8><b>Total Records Found - <cfoutput>#sbir.recordcount#</cfoutput> Contracts</b></td>
						   <td class="feed_option" align=right></tr>
					   <tr><td class="feed_option">Requires premium access.</td></tr>

				   </cfif>

				   </table>

			   </td></tr>
</td></tr>
</table>

</td></tr>
</table>