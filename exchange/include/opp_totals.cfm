<!--- FBO Count --->

<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(fbo_solicitation_number)) as total from fbo
 where (fbo_response_date_updated > #now()# or fbo_response_date_original > #now()#)
</cfquery>
<cfset session.fbo_total = #fbo.total#>

<!--- Future Needs --->

<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(need_id) as total from need
 where need_public > 0 and
       need_status = 'Open'
</cfquery>
<cfset session.needs_total = #needs.total#>

<!--- Market Challengess --->

<cfquery name="set" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_parent_hub_id from hub
 where hub_id = #session.hub#
</cfquery>

<cfif set.hub_parent_hub_id is "">
 <cfset hub_list = 0>
<cfelse>

 <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select hub_id from hub
  where hub_id = #set.hub_parent_hub_id#
 </cfquery>

 <cfif list.recordcount is 0>
  <cfset hub_list = 0>
 <cfelse>
  <cfset hub_list = valuelist(list.hub_id)>
 </cfif>

</cfif>

<cfquery name="in" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select challenge_id from challenge
 join hub on hub_id = challenge_hub_id
 where (challenge_hub_id = #session.hub# or challenge_hub_id in (#hub_list#)) and
        challenge_public = 1
 union
 select challenge_id from challenge
 join hub on hub_id = challenge_hub_id
 where challenge_public = 2
</cfquery>

<cfquery name="out" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(challenge_id) as total from challenge
 where challenge_type_id = 1
</cfquery>

<cfset session.challenges_total = #evaluate(in.recordcount + out.total)#>

<!--- SBIR --->

<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(id) as total from opp_sbir
</cfquery>
<cfset session.sbir_total = #sbir.total#>

<!--- Grants --->

<cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(opp_grant_id) as total from opp_grant
</cfquery>
<cfset session.grants_total = #grants.total#>