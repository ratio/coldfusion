<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from comments_rating
 where comments_rating_hub_id = #session.hub#
 order by comments_rating_value DESC
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td class="feed_header" style="font-size: 30;" valign=middle>Add Company Comments</td>
   <td align=right class="feed_sub_header"></td></tr>
<tr><td colspan=2><hr></td></tr>
<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>Company comments allow you to add information about this company and choose who you share it with.</td></tr>
<tr><td height=5></td></tr>

</table>

   <form action="company_intel_save.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<tr><td class="feed_sub_header" width=200><b>Context</b></td>
			<td><input name="company_intel_context" class="input_text" type="text" size=100 maxlength="1000" required placeholder="What's the context of writing these comments?"></td>
		</tr>

		<tr><td class="feed_sub_header" valign=top><b>Description</b></td>
			<td><textarea name="company_intel_comments" class="input_textarea" onkeypress="isAlphaNum(event);"  onchange="this.value=this.value.replace(/[<+>=\[$\]/\\]/g,' ');"  rows=6 cols=101 placeholder="Please provide any comments related to this intel."></textarea></td>
		</tr>

		<tr><td class="feed_sub_header"><b>Reference URL</b></td>
			<td><input name="company_intel_url" class="input_text" type="url" size=100 maxlength="1000"></td>
		</tr>

		<tr><td height=10></td></tr>

		<tr><td class="feed_sub_header" valign=top>Attachment</td>
			<td class="feed_sub_header" style="font-weight: normal;"><input type="file" id="image" onchange="validate_img()" name="company_intel_attachment"></td></tr>

		<tr>

		<td class="feed_sub_header">Rating</td>
		<td><select name="company_intel_rating" class="input_select">
		<cfoutput query="rating">
		 <option value=#comments_rating_id#>#comments_rating_name#
		</cfoutput>
		</select>

		</td></tr>

		<tr>
		<td class="feed_sub_header">Sharing</td>
		<td><select name="company_intel_sharing" class="input_select">
		 <option value=0>Keep private, only me
		 <option value=1>Share with my Company

		</select>

		</td></tr>

		<tr><td height=10></td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td height=10></td></tr>

		<tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Add Comments"></td></tr>

	   </table>

	   </form>

	   </td></tr>

  </table>




