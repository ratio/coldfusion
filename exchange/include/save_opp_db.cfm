<cfif t is "Contract">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from fbo
  where fbo_id = #id#
 </cfquery>

 <cfset deal_url = #opp_detail.fbo_url#>

<cfelseif t is "Grant">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from opp_grant
  where opp_grant_id = #id#
 </cfquery>

 <cfset deal_url = #opp_detail.linktoadditionalinformation#>

<cfelseif t is "SBIR">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from opp_sbir
  where id = #id#
 </cfquery>

 <cfset deal_url = #opp_detail.url#>

<cfelseif t is "Challenge">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from challenge
  where challenge_id = #id#
 </cfquery>

 <cfset deal_url = #opp_detail.challenge_url#>

<cfelseif t is "Award">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) * from award_data
  where id = '#id#'
 </cfquery>

 <cfset deal_url = "#session.site_url#/exchange/include/award_information.cfm?id=#opp_detail.id#">

</cfif>

<cfif option is 3>

 <cfset notify_type = 1>
 <cfinclude template="save_opp_community.cfm">

<cfelse>

 <cfset notify_type = 2>

<cfif option is 1>

 <cfset pipeline_id = #decrypt(deal_pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

<cfelse>

<cfquery name="insert_pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into pipeline
 (
  pipeline_name,
  pipeline_usr_id,
  pipeline_hub_id,
  pipeline_created,
  pipeline_updated
 )
 values
 (
 '#pipeline_name#',
  #session.usr_id#,
  #session.hub#,
  #now()#,
  #now()#
 )
</cfquery>

<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select max(pipeline_id) as id from pipeline
</cfquery>

<cfset pipeline_id = #max.id#>

</cfif>

<cfif t is "Contract">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_pipeline_id = #pipeline_id# and
	       deal_hub_id = #session.hub# and
	       deal_contract_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_url,
	  deal_desc,
	  deal_contract_id,
	  deal_pipeline_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_current_sol,
	  deal_naics,
	  deal_pop_city,
	  deal_pop_state,
	  deal_psc,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_url#',
	 '#deal_desc#',
	  #id#,
	  #pipeline_id#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.fbo_office#',
	 '#opp_detail.fbo_solicitation_number#',
	 <cfif opp_detail.fbo_naics_code is "">null<cfelse>'#opp_detail.fbo_naics_code#'</cfif>,
	 <cfif opp_detail.fbo_pop_city is "">null<cfelse>'#opp_detail.fbo_pop_city#'</cfif>,
	 <cfif opp_detail.fbo_pop_state is "">null<cfelse>'#opp_detail.fbo_pop_state#'</cfif>,
	 <cfif opp_detail.fbo_class_code is "">null<cfelse>'#opp_detail.fbo_class_code#'</cfif>,
	 'Contract'
	  )
	</cfquery>

	</cfif>

<cfelseif t is "Grant">

    <!--- Grants --->

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_pipeline_id = #pipeline_id# and
	       deal_hub_id = #session.hub# and
	       deal_grant_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_url,
	  deal_desc,
	  deal_grant_id,
	  deal_pipeline_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_url#',
	 '#deal_desc#',
	  #id#,
	  #pipeline_id#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.agencyname#',
	 'Grant'
	  )
	</cfquery>

	</cfif>

    <!--- SBIR --->

<cfelseif t is "SBIR">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_pipeline_id = #pipeline_id# and
	       deal_hub_id = #session.hub# and
	       deal_sbir_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_url,
	  deal_desc,
	  deal_sbir_id,
	  deal_pipeline_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_url#',
	 '#deal_desc#',
	  #id#,
	  #pipeline_id#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.agency#',
	 'SBIR/STTR'
	  )
	</cfquery>

	</cfif>

<cfelseif t is "Challenge">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_pipeline_id = #pipeline_id# and
	       deal_hub_id = #session.hub# and
	       deal_challenge_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_url,
	  deal_desc,
	  deal_challenge_id,
	  deal_pipeline_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_url#',
	 '#deal_desc#',
	  #id#,
	  #pipeline_id#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.challenge_organization#',
	 'Challenge'
	  )
	</cfquery>

	</cfif>

<cfelseif t is "Award">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_pipeline_id = #pipeline_id# and
	       deal_hub_id = #session.hub# and
	       deal_award_id = '#id#'
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_url,
	  deal_desc,
	  deal_award_id,
	  deal_pipeline_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_dept_code,
	  deal_agency_code,
	  deal_naics,
	  deal_pop_city,
	  deal_pop_state,
	  deal_psc,
	  deal_contract_number,
	  deal_past_sol,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_url#',
	 '#deal_desc#',
	  '#id#',
	  #pipeline_id#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.awarding_office_name#',
	 <cfif opp_detail.awarding_agency_code is "">null<cfelse>'#opp_detail.awarding_agency_code#'</cfif>,
	 <cfif opp_detail.awarding_sub_agency_code is "">null<cfelse>'#opp_detail.awarding_sub_agency_code#'</cfif>,
	 <cfif opp_detail.naics_code is "">null<cfelse>'#opp_detail.naics_code#'</cfif>,
	 <cfif opp_detail.primary_place_of_performance_city_name is "">null<cfelse>'#opp_detail.primary_place_of_performance_city_name#'</cfif>,
	 <cfif opp_detail.primary_place_of_performance_state_code is "">null<cfelse>'#opp_detail.primary_place_of_performance_state_code#'</cfif>,
	 <cfif opp_detail.product_or_service_code is "">null<cfelse>'#opp_detail.product_or_service_code#'</cfif>,

	 <cfif opp_detail.parent_award_id is "" or opp_detail.parent_award_id is 0>
	  '#opp_detail.award_id_piid#',
	 <cfelse>
	  '#opp_detail.parent_award_id#',
	 </cfif>

	 <cfif opp_detail.solicitation_identifier is "">null<cfelse>'#opp_detail.solicitation_identifier#'</cfif>,


	 'Award'
	  )
	</cfquery>

	</cfif>

</cfif>

</cfif>

<cfif isdefined("send_notification")>

	<cfquery name="h_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from hub
	 where hub_id = #session.hub#
	</cfquery>

	<cfquery name="from" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_id = #session.usr_id#
	</cfquery>

    <cfif notify_type is 1>

		<cfquery name="to" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from comm
		 join usr on usr_id = comm_owner_id
		 where comm_id = #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	    </cfquery>

    <cfelse>

		<cfquery name="to" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from pipeline
		 join usr on usr_id = pipeline_usr_id
		 where pipeline_id = #pipeline_id#
		</cfquery>

    </cfif>

	<cfmail from="#from.usr_first_name# #from.usr_last_name# <noreply@ratio.exchange>"
			  to="#tostring(tobinary(to.usr_email))#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#from.usr_first_name# #from.usr_last_name# posted an opportunity to your Opportunity Board">
	<html>
	<head><cfoutput>
	<title>#h_info.hub_name#</title>
	</head></cfoutput>
	<body class="body">

	<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
	 <tr><td style="feed_sub_header"><b>Hi #to.usr_first_name#,</b></td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;">#from.usr_first_name# #from.usr_last_name# posted an opportunity to your <cfif notify_type is 1>Community</cfif> Opportunity Board.</td></tr>
	 <tr><td height=20></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;"><b>Opportunity Details</b></td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;">#deal_name#</td></tr>
	 <tr><td style="feed_sub_header" style="font-weight: normal;">#deal_desc#</td></tr>
	</table>

	</cfoutput>

	</body>
	</html>

	</cfmail>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: ffffff;">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">The Opportunity has been successfully saved.</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>