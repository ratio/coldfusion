<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

<cftransaction>

	<cfif deal_role is 1>

	  <!--- Partner Role --->

	  <cfquery name="check_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from deal_comp
	   where deal_comp_deal_id = #deal_id# and
			 deal_comp_partner_id = #session.company_profile_id#
	  </cfquery>

	  <cfif check_1.recordcount is 0>

		  <cfquery name="insert_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   insert into deal_comp(deal_comp_deal_id, deal_comp_partner_id, deal_comp_added, deal_comp_added_by)
		   values
		   (#deal_id#, #session.company_profile_id#, #now()#, #session.usr_id#)
		  </cfquery>

	  </cfif>

	<cfelseif deal_role is 2>

	  <!--- Competitor Role --->

	  <cfquery name="check_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from deal_comp
	   where deal_comp_deal_id = #deal_id# and
			 deal_comp_competitor_id = #session.company_profile_id#
	  </cfquery>

	  <cfif check_2.recordcount is 0>

		  <cfquery name="insert_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   insert into deal_comp(deal_comp_deal_id, deal_comp_competitor_id, deal_comp_added, deal_comp_added_by)
		   values
		   (#deal_id#, #session.company_profile_id#, #now()#, #session.usr_id#)
		  </cfquery>

	  </cfif>

	<cfelseif deal_role is 3>

	  <!--- Partner Role --->

	  <cfquery name="check_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from deal_comp
	   where deal_comp_deal_id = #deal_id# and
			 deal_comp_partner_id = #session.company_profile_id#
	  </cfquery>

	  <cfif check_1.recordcount is 0>

		  <cfquery name="insert_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   insert into deal_comp(deal_comp_deal_id, deal_comp_partner_id, deal_comp_added, deal_comp_added_by)
		   values
		   (#deal_id#, #session.company_profile_id#, #now()#, #session.usr_id#)
		  </cfquery>

	  </cfif>

	  <!--- Competitor Role --->

	  <cfquery name="check_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select * from deal_comp
	   where deal_comp_deal_id = #deal_id# and
			 deal_comp_competitor_id = #session.company_profile_id#
	  </cfquery>

	  <cfif check_2.recordcount is 0>

		  <cfquery name="insert_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   insert into deal_comp(deal_comp_deal_id, deal_comp_competitor_id, deal_comp_added, deal_comp_added_by)
		   values
		   (#deal_id#, #session.company_profile_id#, #now()#, #session.usr_id#)
		  </cfquery>

	  </cfif>

	</cfif>

</cftransaction>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: ffffff;">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">The Company has been saved to the Opportunity.</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>