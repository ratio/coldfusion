<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("s")>

 <cfif s is "t">

  <cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select company_duns from company
   where company_id = #id#
  </cfquery>

  <cfset session.company_profile_duns = #duns.company_duns#>
  <cfset session.company_profile_id = #id#>

 </cfif>
</cfif>

<cfif isdefined("ky")>
 <cfset session.profile_filter = #trim(ky)#>
</cfif>

<!--- Insert Recent --->

<cfquery name="recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into recent
 (recent_usr_id, recent_company_id, recent_usr_company_id, recent_hub_id, recent_date)
 values
 (#session.usr_id#,#session.company_profile_id#,#session.company_id#,#session.hub#,#now()#)
</cfquery>

<cfquery name="name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_profile_id#
</cfquery>

<html>
<head>
	<title><cfif isdefined("session.company_profile_name")><cfoutput>#session.company_profile_name#</cfoutput><cfelse>Exchange</cfif></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 20px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    display: inline-block;
    margin-left: -4px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    padding-right: 20px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}

.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<cfinclude template = "/exchange/include/header.cfm">

<div class="main_box">

	<cfinclude template="/exchange/include/company_header.cfm">
</div>


<cfoutput>

  <div class="tab_active">
   <span class="feed_header"><img src="/images/icon_blocks.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="profile.cfm">Snapshot</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Company Details</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Products & Services</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">People</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="profile_federal.cfm">Federal Profile</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Customers</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Marketing</a></span>
  </div>

  <div class="tab_not_active">
   <span class="feed_sub_header"><img src="/images/icon_fed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/opps/index.cfm">Funding</a></span>
  </div>

</cfoutput>

<div class="main_box_2">

<cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

   <tr><td height=10></td></tr>

   <tr><td class="feed_header">#name.company_name#</td><td colspan=2 align=right class="feed_sub_header" style="font-weight: normal;">
   </td></tr>

   </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>


   <tr><td colspan=3><hr></td></tr>

   <tr><td height=20></td></tr>

   <tr><td class="feed_header" width=50%>About this Company</td>
       <td width=30>&nbsp;</td>
       <td class="feed_header" align=center>Performance Stats</td></tr>
   <tr><td class="feed_sub_header" style="font-weight: normal;"><br>#name.company_long_desc#</td>
       <td width=30>&nbsp;</td>
       <td>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr>
			 <td class="feed_sub_header" align=center>Revenue</td>
			 <td class="feed_sub_header" align=center>Funding</td>
			 <td class="feed_sub_header" align=center>Contracts</td>
			 <td class="feed_sub_header" align=center>Customers</td>
			 <td class="feed_sub_header" align=center>Grants</td>
			 <td class="feed_sub_header" align=center>SBIR/STTRs</td>
			 <td class="feed_sub_header" align=center>Patents</td>
		   </tr>


		   <tr>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">$24m</a></td>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">$3m</a></td>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">384</a></td>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">18</a></td>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">8</a></td>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">23</a></td>
			<td class="big_number" align=center style="background-color: 3366CC; font-size: 20px; color: FFFFFF; width: 150px;"><a href="/exchange/components/home_page/snapshot_company/cview_01.cfm" style="font-size: 20px; color: ffffff;" onclick="javascript:document.getElementById('page-loader').style.display='block';">110</a></td>
		   </tr>
		  </table>

       </td></tr>
   <tr><td height=20></td></tr>

   <tr><td colspan=3><hr></td></tr>

  </table>

  </cfoutput>

		<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select top(1) * from award_data
		 where recipient_duns = '#name.company_duns#'
		</cfquery>

		<cfif awards.recordcount is 0>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header">No Federal prime awards found.</td></tr>
        </table>

        <cfelse>

        <!--- Start Graphs --->

		<cfquery name="annual" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				select sum(federal_action_obligation) as total, year(action_date) as year1 from award_data
				where recipient_duns = '#name.company_duns#'
				group by year(action_date)
				order by year(action_date)
		</cfquery>

		<cfquery name="award_summary_1" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select count(id) as total, avg(CAST(annual_revenue AS numeric)) as revenue, avg(CAST(number_of_employees AS int)) as employees, datepart(yyyy, [action_date]) as [year], count(distinct(id)) as awards, count(distinct(award_id_piid)) as contracts, count(distinct(awarding_sub_agency_code)) as sub_agencies, count(distinct(awarding_agency_code)) as agencies, sum(base_and_all_options_value) as all_options, sum(base_and_exercised_options_value) as options, sum(federal_action_obligation) as federal_action_obligation from award_data
	       where recipient_duns = '#name.company_duns#'
           group by datepart(yyyy, [action_date])
           order by [year]
		</cfquery>


        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr>
              <td class="feed_header" align=center>Federal Awards</td>
              <td width=50>&nbsp;</td>
              <td class="feed_header" align=center>Reported Revenue</td>
              <td width=50>&nbsp;</td>
              <td class="feed_header" align=center>Federal Award Diversity</td>
         </tr>

        <tr>

        <td valign=top width=33% align=center>

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Awards'],
				<cfoutput query="annual">
				 ["#year1#", #round(total)#],
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 100, right: 0, width: '100%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Award Value',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual" style="width: 100%;"></div>

        </td><td width=30>&nbsp;</td><td valign=top width=33% align=center>

		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Revenue'],
				<cfoutput query="award_summary_1">
				<cfif revenue is "">
				 ["#year#", 0],
				<cfelse>
				 ["#year#", #round(revenue)#],
				</cfif>
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 100, right: 0, width: '100%'},
				legend: 'none',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Revenue',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual_2'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual_2" style="width: 100%;"></div>

        </td><td width=30>&nbsp;</td><td valign=top width=33% align=center>

		<script type="text/javascript">

		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			  var data = google.visualization.arrayToDataTable([
				['Year', 'Agencies', 'Contracts','Awards'],
				<cfoutput query="award_summary_1">
				 ["#year#", #agencies#, #contracts#, #awards#],
				</cfoutput>
			  ]);

			  var options = {
				title: '',
				chartArea: {left: 80, right: 0, width: '100%'},
				legend: 'bottom',
				height: 200,
				hAxis: {
				  textStyle: {color: 'black', fontSize: 12},
				  format: 'currency',
				  title: '',
				  minValue: 0
				},
				vAxis: {
				  title: 'Number',
				}
			  };

			  var chart = new google.visualization.ColumnChart(document.getElementById('annual_3'));

			  chart.draw(data, options);
			}

		  </script>

		  <div id="annual_3" style="width: 100%;"></div>

          </td></tr>

        <tr><td colspan=5><hr></td></tr>

        </table>

	    <table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td height=20></td></tr>
<tr><td class="feed_header" align=center>Federal Awards Timeline (Cummulative)</td></tr>

<tr><td>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([

		<cfquery name="money" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select action_date, sum(federal_action_obligation) as obligated, sum(base_and_exercised_options_value) as total from award_data
		where recipient_duns = '#name.company_duns#'
		group by action_date
		order by action_date ASC
		</cfquery>

          ['Date', 'Obligated', 'Total Contract Value' ],

          <cfset column1 = 0>
          <cfset column2 = 0>

          <cfloop query="money">

          <cfif #money.obligated# is "">
           <cfset obg = 0>
          <cfelse>
           <cfset obg = #money.obligated#>
          </cfif>

          <cfif #money.total# is "">
           <cfset tot = 0>
          <cfelse>
           <cfset tot = #money.total#>
          </cfif>

           <cfset column1 = column1 + #obg#>
           <cfset column2 = column2 + #tot#>

          <cfoutput>
           ['#dateformat(money.action_date,'mm/dd/yyyy')#',
          </cfoutput>

           <cfoutput>
           #column1#,
           #column2#
           </cfoutput>

          ],

          </cfloop>

        ]);

        var options = {
          title: '',
          chartArea:{right: 30, left:70,top:20,width:'83%',height:'75%'},
          hAxis: { textStyle: {color: 'black', fontSize: 11}},
          vAxis: { textStyle: {color: 'black', fontSize: 11}},
          aggregationTarget: 'Obligated',
          legend: { position: 'bottom', textStyle: {fontSize: 10}}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('award_timeline'));
        chart.draw(data, options);
      }
    </script>

    <div id="award_timeline" style="width: 100%; height: 250px"></div>

    <tr><td><hr></td></tr>
    </table>

   </cfif>

</div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>


