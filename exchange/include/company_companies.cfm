<style>
.dropbtn2 {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropdown2 {
  position: relative;
  display: inline-block;
}

.dropdown2-content {
  display: none;
  right: 0;
  font-size: 13px;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown2-content a {
  color: b	lack;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown2-content a:hover {background-color: #f1f1f1}

.dropdown2:hover .dropdown2-content {
  display: block;
}

.dropdown2:hover .dropbtn2 {
  background-color: #3e8e41;
}
</style>

<cfquery name="comps" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(recipient_duns), sum(federal_action_obligation) as revenue from award_data
 where recipient_parent_duns = '#session.company_profile_duns#'
 group by recipient_duns
 order by revenue DESC
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td class="feed_header" style="font-size: 30;" valign=middle>Companies</td>
    <td align=right class="feed_sub_header"></td></tr>
<tr><td colspan=2><hr></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfif comps.recordcount is 0>
 <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies exist that full under this Companies DUNS number.</td></tr>

<cfelse>

<cfset count = 1>

<cfoutput>

				<tr>
                    <td class="feed_sub_header">Company Name</td>
                    <td class="feed_sub_header" align=center>DUNS</td>
                    <td class="feed_sub_header">Website</td>
                    <td class="feed_sub_header">City</td>
                    <td class="feed_sub_header" align=center>State</td>
                    <td class="feed_sub_header" align=right>Awards</td>
                    <td></td>
				</tr>

				<tr><td>&nbsp;</td></tr>
</cfoutput>

<cfloop query="comps">

<cfquery name="c_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select top(1) recipient_name, recipient_city_name, recipient_state_code, recipient_duns, company_name, company_id, company_website from award_data
 left join company on company_duns = recipient_duns
 where recipient_duns = '#comps.recipient_duns#'
</cfquery>

   <cfoutput>
				<tr>

					<td class="feed_sub_header" width=500 style="padding-right: 20px;">

					<cfif c_info.company_name is "">
					<a href="/exchange/include/federal_profile.cfm?duns=#comps.recipient_duns#">#ucase(c_info.recipient_name)#</a>
					<cfelse>
					<a href="/exchange/include/company_profile.cfm?id=#c_info.company_id#">#ucase(c_info.company_name)#</a>

					</cfif>

					</td>
					<td class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#ucase(comps.recipient_duns)#</td>
					<td class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#ucase(c_info.company_website)#</td>
					<td class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#ucase(c_info.recipient_city_name)#</td>
					<td class="feed_sub_header" style="font-weight: normal; padding-right: 20px;">#ucase(c_info.recipient_state_code)#</td>
					<td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(comps.revenue,'$999,999,999')#</td>

					<td width=50 align=right>

                    <!---

					<div class="dropdown2">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=8>
					  <div class="dropdown2-content" style="width: 175px; text-align: left;">
						<a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener"><i class="fa fa-fw fa-pie-chart"></i>&nbsp;&nbsp;Company Profile</a>
						<a href="/exchange/include/save_deal.cfm" onclick="window.open('/exchange/include/save_deal.cfm?comp_id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-dollar"></i>&nbsp;&nbsp;Add to Deal</a>
						<a href="/exchange/include/save_company.cfm" onclick="window.open('/exchange/include/save_company.cfm?comp_id=#companies.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-fw fa-briefcase"></i>&nbsp;&nbsp;Add to Portfolio</a>
					  </div>
					</div> --->
					</td>

				</tr>

				  <cfif count is not comps.recordcount>
				   <tr><td colspan=10><hr></td></tr>
				  <cfelse>
				   <tr><td height=5></td></tr>
				  </cfif>

				  <cfset count = count + 1>

				</cfoutput>

 </cfloop>

 </cfif>

</table>

