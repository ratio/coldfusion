<cfquery name="private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from cert
 where cert_company_id = #session.company_profile_id# and
       cert_public = 1
 order by cert_order
</cfquery>

<cfquery name="public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cert
 where cert_company_id = #session.company_profile_id# and
       cert_public = 1
 order by cert_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Certifications</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Local Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been approved by Companies for use on the <cfoutput>#session.network_name#</cfoutput>.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <cfif private.recordcount is 0>
   <tr><td class="feed_sub_header">No information was found.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="private">

      <tr><td valign=top width=120>
		   <cfif #private.cert_attachment# is "">
		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#private.cert_attachment#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr>
			         <td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(private.cert_name)#</td>
			         <td class="feed_sub_header" align=right valign=top style="padding-top: 5px; padding-bottom: 0px;">
			        <cfif private.cert_date_acquired is "">
						Date Certified:  Not Provided
						<cfelse>
						Date Certified:  #dateformat(private.cert_date_acquired,'mmm dd, yyyy')#
						</cfif>
			         </td>

		         </tr>
			     <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(private.cert_desc,"#chr(10)#","<br>","all")#</td></tr>
			     <tr><td colspan=2 class="link_small_gray" style="font-weight: normal;" valign=top>#private.cert_keywords#</td></tr>

			   </table>

           </td>

           <td align=right>

           </td>

      </tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

  <tr><td height=10></td></tr>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Public Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been sourced from the Ratio Exchange.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <tr><td height=5></td></tr>

  <cfif public.recordcount is 0>
   <tr><td class="feed_sub_header">No information has been sourced.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>


      <cfoutput query="public">

       <tr><td valign=top width=120>
 		   <cfif #public.cert_attachment# is "">
 		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
 		   <cfelse>
 		   <img src="#media_virtual#/#cert.cert_attachment#" width=90 vspace=5 border=0>
 		   </cfif>
            </td>

            <td valign=top>

 			   <table cellspacing=0 cellpadding=0 border=0 width=100%>


			     <tr>
			         <td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(public.cert_name)#</td>
			         <td class="feed_sub_header" align=right valign=top style="padding-top: 5px; padding-bottom: 0px;">
			        <cfif public.cert_date_acquired is "">
						Date Certified:  Not Provided
						<cfelse>
						Date Certified:  #dateformat(public.cert_date_acquired,'mmm dd, yyyy')#
						</cfif>
			         </td>

		         </tr>

 			     <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>#replace(public.cert_desc,"#chr(10)#","<br>","all")#</td></tr>

 			     <tr><td class="link_small_gray" colspan=2 style="font-weight: normal;" valign=top>#public.cert_keywords#</td></tr>

 			   </table>

            </td>

            <td align=right>

            </td>

       </tr>

       <tr><td colspan=3><hr></td></tr>
     </cfoutput>


   </table>

   </td></tr>

  </cfif>

</table>