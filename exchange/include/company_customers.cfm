<cfquery name="private" datasource="#client_datasource#" username="#client_username#" password="#lake_password#">
 select * from customer
 where customer_company_id = #session.company_profile_id# and
       customer_public = 1
 order by customer_order
</cfquery>

<cfquery name="public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from customer
 where customer_company_id = #session.company_profile_id# and
       customer_public = 1
 order by customer_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Customer Snapshots</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Local Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been approved by Companies for use on the <cfoutput>#session.network_name#</cfoutput>.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <cfif private.recordcount is 0>
   <tr><td class="feed_sub_header">No information was found.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="private">

      <tr><td valign=top width=120>
		   <cfif #private.customer_logo# is "">
		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#private.customer_logo#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(private.customer_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(private.customer_work,"#chr(10)#","<br>","all")#</td></tr>

			   </table>

           </td>

           <td align=right>
            <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('private#private.customer_id#');">
           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="private#private.customer_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Results</td></tr>
	       <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(private.customer_results,"#chr(10)#","<br>","all")#</td></tr>
		   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Case Study</td></tr>
	       <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>
	       <cfif private.customer_case_study_url is "">
	        Not Provided
	       <cfelse>
	        <a href="#private.customer_case_study_url#"><u>#private.customer_case_study_url#</u></a>
	       </cfif>
	       </td></tr>
         </table>
       </div>
       </td></tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

  <tr><td height=10></td></tr>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Public Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been sourced from the Ratio Exchange.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <tr><td height=5></td></tr>

  <cfif public.recordcount is 0>
   <tr><td class="feed_sub_header">No information has been sourced.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>


     <cfoutput query="public">

      <tr><td valign=top width=120>
		   <cfif #public.customer_logo# is "">
		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#public.customer_logo#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(public.customer_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(public.customer_work,"#chr(10)#","<br>","all")#</td></tr>

			   </table>

           </td>

           <td align=right>
            <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('public#public.customer_id#');">
           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="public#public.customer_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Results</td></tr>
	       <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(public.customer_results,"#chr(10)#","<br>","all")#</td></tr>
		   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Case Study</td></tr>
	       <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>
	       <cfif public.customer_case_study_url is "">
	       	Not Provided
	       <cfelse>
	       	<a href="#public.customer_case_study_url#"><u>#public.customer_case_study_url#</u></a>
	       </cfif>
	       </td></tr>
         </table>
       </div>
       </td></tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>



   </table>

   </td></tr>

  </cfif>

</table>