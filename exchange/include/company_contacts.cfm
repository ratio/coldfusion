<cfquery name="contacts" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company_contact
 where company_contact_company_id = #session.company_profile_id# and
       company_contact_hub_id = #session.hub#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=2 class="feed_header" style="font-size: 30;">Partner Contacts</td>
       <td align=right class="feed_sub_header">

       </td></tr>

   <tr><td colspan=3><hr></td></tr>
   <tr><td height=20></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td valign=top>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif #contacts.recordcount# is 0>
		 <tr><td class="feed_sub_header">No Partner Contacts have been created.</td></tr>
		<cfelse>

         <cfset counter = 0>

          <tr>
             <td class="feed_sub_header">Name</td>
             <td class="feed_sub_header">Title</td>
             <td class="feed_sub_header">Email</td>
             <td class="feed_sub_header">Phone</td>
             <td class="feed_sub_header">Cell</td>
          </tr>

          <cfoutput query="contacts">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

              <td class="feed_sub_header" style="font-weight: normal;"><b>#company_contact_name#</b></td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_title#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_email#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_phone#</td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_contact_cell#</td>
            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
          </cfoutput>

		</cfif>

     </table>

</td></tr>

</table>
