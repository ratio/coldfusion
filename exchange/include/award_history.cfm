<cfinclude template="/exchange/security/check.cfm">

<cfquery name="history" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_data
 where award_id_piid = '#award_id#' and
 recipient_duns = '#duns#'
 order by action_date ASC, modification_number
</cfquery>

<cfquery name="award_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(id) as awards, sum(federal_action_obligation) as obligated, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as total from award_data
 where award_id_piid = '#award_id#' and
       recipient_duns = '#duns#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box" style="margin-left: 10px; margin-right: 10px; margin-top: 30px;">

	   <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header">Award Information - Historical Awards and Modifications</td>
				 <td class="feed_option" align=right><img src="/images/delete.png" align=absmiddle border=0 width=20 style="cursor: pointer;" alt="Close" title="Close" onclick="windowClose();"></td></tr>
			 <tr><td colspan=2><hr></td></tr>
			 <tr><td colspan=2 class="feed_option"><b>Note</b> - information included in this report is based only on information we have.  Contracts and awards that may have been issued prior to the award dates listed below are not included in this report.</td></tr>
			 <tr><td height=10></td></tr>
		   </table>

       </cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td class="feed_sub_header">SUMMARY OF AWARDS</td>
           <td class="feed_sub_header" align=right>Contract Spend Summary: &nbsp;&nbsp;&nbsp;

            <cfoutput>
            $0&nbsp;&nbsp;
            <progress value="#award_total.obligated#" max="#award_total.total#" alt="Spent - #numberformat(evaluate(award_total.obligated),'$999,999,999')#" title="Spent - #numberformat(evaluate(award_total.obligated),'$999,999,999')#" style="width:100px; height: 20px;"></progress></a>
            &nbsp;&nbsp;#numberformat(award_total.total,'$999,999,999,999')#
            </cfoutput>

           </td></tr>
       <tr><td>&nbsp;</td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr height=40>
             <td class="feed_option"><b>Award Date</b></td>
             <td class="feed_option"><b>Award ID</b></td>
             <td class="feed_option"><b>Modification</b></td>
             <td class="feed_option"><b>Action Type</b></td>
             <td class="feed_option"><b>Description</b></td>
             <td class="feed_option" align=right><b>Obligated</b></td>
             <td class="feed_option" align=right><b>Base & Exercised Options</b></td>
             <td class="feed_option" align=right><b>Base & All Options</b></td>
         </tr>

       <cfset counter = 0>

       <cfset total_1 = 0>
       <cfset total_2 = 0>
       <cfset total_3 = 0>

       <cfoutput query="history">

       <cfif counter is 0>
        <tr bgcolor="ffffff" height=30>
       <cfelse>
        <tr bgcolor="e0e0e0" height=30>
       </cfif>


            <td class="feed_option">#dateformat(action_date,'mm/dd/yyyy')#</td>
            <td class="feed_option"><a href="award_information.cfm?id=#id#&duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
            <td class="feed_option">#modification_number#</td>
            <td class="feed_option">#action_type#</td>
            <td class="feed_option" width=600>#award_description#</td>
            <td class="feed_option" align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
            <td class="feed_option" align=right>#numberformat(base_and_exercised_options_value,'$999,999,999')#</td>
            <td class="feed_option" align=right>#numberformat(base_and_all_options_value,'$999,999,999')#</td>
        </tr>

        <cfif counter is 0>
         <cfset counter = 1>
        <cfelse>
         <cfset counter = 0>
        </cfif>

        <cfif #federal_action_obligation# is not "">
          <cfset total_1 = total_1 + #federal_action_obligation#>
        </cfif>

        <cfif #base_and_exercised_options_value# is not "">
        <cfset total_2 = total_2 + #base_and_exercised_options_value#>
        </cfif>

        <cfif #base_and_all_options_value# is not "">
        <cfset total_3 = total_3 + #base_and_all_options_value#>
        </cfif>

       </cfoutput>

       <cfoutput>
       <tr><td class="feed_option" colspan=5><b>Total</b></td>
           <td class="feed_option" align=right><b>#numberformat(total_1,'$999,999,999')#</b></td>
           <td class="feed_option" align=right><b>#numberformat(total_2,'$999,999,999')#</b></td>
           <td class="feed_option" align=right><b>#numberformat(total_3,'$999,999,999')#</b></td>
       </tr>
       </cfoutput>

       </td></tr>

       </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td height=10></td></tr>
    <tr><td class="feed_sub_header">AWARD TIMELINE</td></tr>
    <tr><td>&nbsp;</td></tr>

    <tr><td valign=top>


    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['timeline']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'Amount' });
        dataTable.addColumn({ type: 'string', id: 'Amount' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addColumn({ type: 'string', role: 'tooltip', id: 'link', 'p': {'html': true} });
        dataTable.addRows([

        <cfoutput query="history">

        <cfif period_of_performance_current_end_date LT period_of_performance_start_date>

          [ '#action_type#','Dates Invalid, Review Data.  Mod #modification_number# - (#numberformat(federal_action_obligation,'$999,999,999')#)', new Date(#year(period_of_performance_start_date)#, #evaluate(month(period_of_performance_start_date)-1)#, #day(period_of_performance_start_date)#), new Date(#year(period_of_performance_start_date)#, #evaluate(month(period_of_performance_start_date)+1)#, #day(period_of_performance_start_date)#),'/exchange/include/award_information.cfm?id=#id#'],

        <cfelse>

          [ '#action_type#','Mod #modification_number# - (#numberformat(federal_action_obligation,'$999,999,999')#)', new Date(#year(period_of_performance_start_date)#, #evaluate(month(period_of_performance_start_date)-1)#, #day(period_of_performance_start_date)#), new Date(#year(period_of_performance_current_end_date)#, #evaluate(month(period_of_performance_current_end_date)-1)#, #day(period_of_performance_current_end_date)#),'/exchange/include/award_information.cfm?id=#id#'],

        </cfif>

        </cfoutput>

          ]);

    var options = {
          chartArea:{right: 30, left:70,top:20,width:'83%',height:'75%'},
          hAxis: { textStyle: {color: 'black', fontSize: 11}},
          vAxis: { textStyle: {color: 'black', fontSize: 11}},
      timeline: { singleColor: '#022447', colorByRowLabel: true, rowLabelStyle: {fontName: 'Arial', fontSize: 10, color: '#603913' },
                     barLabelStyle: { fontName: 'Arial', fontSize: 10 } }
    };


 google.visualization.events.addListener(chart, 'select', function () {
    var selection = chart.getSelection();
    if (selection.length > 0) {
      window.open(dataTable.getValue(selection[0].row, 4), '_self');
      console.log(dataTable.getValue(selection[0].row, 4));
    }
  });

  function drawChart1() {
    chart.draw(dataTable, options);
  }
  drawChart1();

      }
    </script>

    <div id="timeline" style="width: 100%; height: 300;"></div>

</td></tr>

</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td class="feed_sub_header">AWARD VALUE TIMELINE</td></tr>

<tr><td>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([

		<cfquery name="money" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		select action_date, sum(federal_action_obligation) as obligated, sum(base_and_all_options_value) as total from award_data
		where award_id_piid = '#award_id#' and
		recipient_duns = '#duns#'
		group by action_date
		order by action_date ASC
		</cfquery>

          ['Date', 'Obligated', 'Total Contract Value' ],

          <cfset column1 = 0>
          <cfset column2 = 0>

          <cfloop query="money">

           <cfset column1 = column1 + #money.obligated#>
           <cfset column2 = column2 + #money.total#>

          <cfoutput>
           ['#dateformat(money.action_date,'mm/dd/yyyy')#',
          </cfoutput>

           <cfoutput>
           #column1#,
           #column2#
           </cfoutput>

          ],

          </cfloop>

        ]);

        var options = {
          title: '',
          chartArea:{right: 30, left:70,top:20,width:'83%',height:'75%'},
          hAxis: { textStyle: {color: 'black', fontSize: 11}},
          vAxis: { textStyle: {color: 'black', fontSize: 11}},
          aggregationTarget: 'Obligated',
          legend: { position: 'bottom', textStyle: {fontSize: 10}}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('award_timeline'));
        chart.draw(data, options);
      }
    </script>

    <div id="award_timeline" style="width: 100%; height: 300px"></div>

</td></tr>
</table>

	  </div>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

