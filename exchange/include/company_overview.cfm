<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns = '#session.company_profile_duns#'
</cfquery>

<cfquery name="certs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sba
 where duns = '#session.company_profile_duns#'
</cfquery>

<cfquery name="contacts" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_contact
  where company_contact_company_id = #session.company_profile_id# and
        company_contact_hub_id = #session.hub#
  order by company_contact_name
</cfquery>

<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 left join size on size_id = company_size_id
 left join broker on broker_company_id = company_id
 left join entity on entity_id = company_entity_id
 left join cb_organizations on cb_organizations.uuid = company_cb_id
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="rel_managers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from company_rm
  join usr on usr_id = company_rm_usr_id
  join hub_xref on hub_xref_usr_id = company_rm_usr_id
  where hub_xref_hub_id = #session.hub# and
        company_rm_company_id = #session.company_profile_id#
  order by company_rm_order
</cfquery>

<style>
.people_scroll {
    height: 150px;
    background-color: ffffff;
    overflow:auto;
}
.people_block {
    width: 200;
    border: 1px solid #f0f0f0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 320px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 3px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 280px;
  background-color: #ffffff;
  color: #000000;
  text-align: left;
  font-weight: normal;
  padding: 10px;
  position: absolute;
  font-size: 16;
  z-index: 1;
  top: 150%;
  left: 50%;
  margin-left: -140px;
  border-radius: 3px;
  border-color: #b0b0b0;
  border-width: thin;
  border-style: solid;
  border-radius: 2px;
  box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  bottom: 100%;
  left: 50%;
  margin-left: 0px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent black transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=2 class="feed_header" style="font-size: 30;">Overview</td>
       <td align=right class="feed_sub_header">
       <!---
       <cfoutput>
       <a href="profile.cfm?l=31"><img src="/images/plus3.png" width=15 border=0 alt="Add Comments" title="Add Comments" hspace=10 valign=middle></a><a href="profile.cfm?l=31">Add Comments</a>

       </cfoutput> --->
       </td></tr>

   <tr><td colspan=3><hr></td></tr>
   <tr><td height=20></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td valign=top>

<cfoutput>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <cfif comp.company_tagline is not "">
	  <tr>
		  <td class="feed_sub_header" width=175>Tagline</td>
		  <td class="feed_sub_header" style="font-weight: normal;">#comp.company_tagline#</td></tr>
      </cfif>

	  <tr>
		  <td class="feed_sub_header" valign=top width=175>Overview</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
		   <cfif #company_profile.company_about# is not "">
			#replace(comp.company_about,"#chr(10)#","<br>","all")#
		   <cfelse>
			Summary description not available
		   </cfif>
		   </td></tr>

	  <tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Full Description</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
		   <cfif #company_profile.company_long_desc# is not "">
			#replace(comp.company_long_desc,"#chr(10)#","<br>","all")#
		   <cfelse>
			Full description not available
		   </cfif>
		   </td></tr>

	  <tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Company Keywords</td>
		  <td class="feed_sub_header" valign=top style="font-weight: normal;">
		   <cfif #comp.company_keywords# is not "">
		   #comp.company_keywords#
		   <cfelse>
		   Not provided
		   </cfif>
		   </td></tr>

       </cfoutput>

	   <cfif network.recordcount is 1>

			<cfquery name="rm" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from company_extend
			 join partner_tier on partner_tier_id = company_extend_tier_id
			 where company_extend_hub_id = #session.hub# and
			       company_extend_company_id = #session.company_profile_id#
			</cfquery>

			<cfquery name="programs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select * from partner_program
			  join partner_program_xref on partner_program_xref_program_id = partner_program_id
			  where partner_program_xref_partner_id = #session.company_profile_id# and
					partner_program_xref_hub_id = #session.hub#
			  order by partner_program_order
			</cfquery>

		   <cfoutput>

		   <tr bgcolor="f0f0f0">
			  <td class="feed_sub_header" valign=top width=200>Company Tier</td>
			  <td class="feed_sub_header" style="font-weight: normal;" valign=top>
               <cfif #rm.company_extend_tier_id# is "">
                Not Identified
               <cfelse>
                #rm.partner_tier_name#
               </cfif>
              </td>
           </tr>
           </cfoutput>

		   <tr bgcolor="f0f0f0">
			  <td class="feed_sub_header" valign=top width=200>Company Programs</td>
			  <td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;" valign=top bgcolor="f0f0f0">
			 <cfif programs.recordcount is 0>
			  Not Identified
			 <cfelse>
				<cfoutput query="programs">
				 <li>#programs.partner_program_name#</li>
				</cfoutput>
			 </cfif>
             </td></tr>

          </cfif>

		   <tr>

			  <td class="feed_sub_header" valign=top width=225>Relationship Managers</td>
			  <td class="feed_sub_header" style="font-weight: normal;" valign=top>

              <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfif rel_managers.recordcount is 0>
				 <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" valign=top>No relationship managers have been identified</td></tr>
				<cfelse>

				 <cfoutput query="rel_managers">

				 <tr height=50>

					 <td width=70>

					 <a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">
					 <cfif #usr_photo# is "">
					  <img src="/images/headshot.png" height=40 width=40 border=0>
					 <cfelse>
					  <img style="border-radius: 2px;" src="#media_virtual#/#usr_photo#" height=40 width=40>
					 </cfif>
					 </a>

					 </td>

					 <td class="feed_sub_header" width=200><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#usr_first_name# #usr_last_name#</a></td>

					 <td class="feed_sub_header" width=200 style="font-weight: normal;">#usr_title#</td>
					 <td class="feed_sub_header" width=300 style="font-weight: normal;">#company_rm_title#</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(usr_email))#</td>
				 </tr>

				 </cfoutput>

				 </cfif>

				</table>

              </td>
		   </tr>

		   <tr>

			  <td class="feed_sub_header" valign=top width=200>Points of Contact</td>
			  <td class="feed_sub_header" valign=top>

              <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfif contacts.recordcount is 0>
				 <tr><td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" valign=top>No points of contact were found</td></tr>
				<cfelse>

				 <tr>
				    <td class="feed_sub_header" style="padding-top: 0px;">Name</td>
				    <td class="feed_sub_header" style="padding-top: 0px;">Title</td>
				    <td class="feed_sub_header" style="padding-top: 0px;">Phone</td>
				    <td class="feed_sub_header" style="padding-top: 0px;">Mobile</td>
				    <td class="feed_sub_header" style="padding-top: 0px;">Email</td>
                 </tr>

				 <cfoutput query="contacts">

				 <tr>
				    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-bottom: 5px;">#company_contact_name#</td>
				    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-bottom: 5px;">#company_contact_title#</td>
				    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-bottom: 5px;">#company_contact_phone#</td>
				    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-bottom: 5px;">#company_contact_cell#</td>
				    <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px; padding-bottom: 5px;">#company_contact_email#</td>
				 </tr>

                 </cfoutput>

                 </cfif>

				 </table>

				 </td></tr>

       <cfoutput>

	   <tr>
		  <td class="feed_sub_header" valign=top>Primary<br>Point of Contact</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
		  <cfif #comp.company_poc_first_name# is "" and #comp.company_poc_last_name# is "">
			  Not found
		  <cfelse>
			  #comp.company_poc_first_name# #comp.company_poc_last_name#<br>
			  <cfif #comp.company_poc_title# is not "">#comp.company_poc_title#<br></cfif>
			  #comp.company_poc_phone#<br>
			  #comp.company_poc_email#<br>
		  </cfif>
		   </td>
	   </tr>

	   <tr>
		  <td class="feed_sub_header" valign=top>Address</td>
		  <td class="feed_sub_header" style="font-weight: normal;">

			  <cfif #company_profile.company_address_l1# is not "">#company_profile.company_address_l1#<br></cfif>
			  <cfif #company_profile.company_address_l2# is not "">#company_profile.company_address_l2#<br></cfif>
			  <cfif #company_profile.company_city# is "" and (#company_profile.company_state# is 0 or #company_profile.company_state# is "")>
			  Unknown
			  <cfelse>
			  #company_profile.company_city# #company_profile.company_state#  #company_profile.company_zip#
			  </cfif>

		   </td>
	   </tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Corporate URLs</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <img src="/images/icon_home.png" width=25 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #comp.company_website# is "">Not provided<cfelse><a href="#comp.company_website#" target="_blank" rel="noopener" style="font-weight: normal;">#comp.company_website#</a></cfif><br>
			  <img src="/images/icon_linkedin.png" width=25 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #comp.company_linkedin_url# is "">Not provided<cfelse><a href="#comp.company_linkedin_url#" target="_blank" rel="noopener" style="font-weight: normal;">#comp.company_linkedin_url#</a></cfif><br>
			  <img src="/images/icon_twitter.png" width=25 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #comp.company_twitter_url# is "">Not provided<cfelse><a href="#comp.company_twitter_url#" target="_blank" rel="noopener" style="font-weight: normal;">#comp.company_twitter_url#</a></cfif><br>
			  <img src="/images/icon_facebook.png" width=25 vspace=10 valign=middle>&nbsp;&nbsp;&nbsp;&nbsp;<cfif #comp.company_facebook_url# is "">Not provided<cfelse><a href="#comp.company_facebook_url#" target="_blank" rel="noopener" style="font-weight: normal;">#comp.company_facebook_url#</a></cfif>
		   </td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

  </cfoutput>

<cfset comp_team = 0>

	  <tr>
		  <td class="feed_sub_header" valign=top>Company Team</td>
		  <td class="feed_sub_header" style="font-weight: normal;">

           <cfif company_profile.company_leadership is "">
            Company team information not provided
           <cfelse>

		   <cfif #trim(company_profile.company_leadership)# is not "">
			<cfset comp_team = 1>
			<cfoutput>#replace(comp.company_leadership,"#chr(10)#","<br>","all")#</cfoutput>
           </cfif>

           </cfif>

          </td>
      </tr>

<cfoutput>

	  <tr>
		  <td class="feed_sub_header" valign=top>Company History</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
		   <cfif #company_profile.company_history# is not "">
			#replace(comp.company_history,"#chr(10)#","<br>","all")#
		   <cfelse>
			Company history not provided
		   </cfif>
		   </td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Founded</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <cfif #company_profile.company_founded# is "">
			   <cfif #comp.founded_on# is "">
			    Unknown
			   <cfelse>
			    #dateformat(comp.founded_on,'mmmm dd, yyyy')#
			   </cfif>
			  <cfelse>
			    #company_profile.company_founded#
			  </cfif>
		   </td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Funding</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
          <cfif #comp.num_funding_rounds# is "">
          Unknown
          <cfelse>
          <a href="/exchange/include/profile.cfm?l=35"><b>#comp.num_funding_rounds# Rounds</b></a><br>
          Total Funds Received - <cfif comp.total_funding_usd is "">Unknown<cfelse>#numberformat(comp.total_funding_usd,'$999,999,999')#</cfif>
          <cfif #comp.last_funding_on# is not ""><br>Last Funding On #dateformat(comp.last_funding_on,'mmmm dd, yyyy')#</cfif>
          </cfif>
		   </td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>DUNS</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <cfif #company_profile.company_duns# is "">Unknown<cfelse>#company_profile.company_duns#</cfif>
		   </td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Stock Symbol</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <cfif #company_profile.company_stock_symbol# is "">Not provided<cfelse>#company_profile.company_stock_symbol#</cfif>
		   </td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Revenue</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
		  2018 - &nbsp;<cfif #comp.company_fy18_revenue# is "">Not disclosed<cfelse>#numberformat(comp.company_fy18_revenue,'$999,999,999')#</cfif><br>
		  2017 - &nbsp;<cfif #comp.company_fy17_revenue# is "">Not disclosed<cfelse>#numberformat(comp.company_fy17_revenue,'$999,999,999')#</cfif><br>
		  2016 - &nbsp;<cfif #comp.company_fy16_revenue# is "">Not disclosed<cfelse>#numberformat(comp.company_fy16_revenue,'$999,999,999')#</cfif>
		   </td></tr>

	   <tr><td height=10></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	   <tr><td height=10></td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Last Updated</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <cfif #comp.company_updated# is "">Unknown<cfelse>#dateformat(comp.company_updated,'mmmm dd, yyyy')#</cfif>
		   </td></tr>


	  <tr>
		  <td class="feed_sub_header" valign=top>Type</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <cfif #comp.entity_name# is "">Unknown<cfelse>#comp.entity_name#</cfif>
		   </td></tr>

	  <tr>
		  <td class="feed_sub_header" valign=top>Size</td>
		  <td class="feed_sub_header" style="font-weight: normal;">
			  <cfif #comp.size_name# is "">
			   <cfif #comp.employee_count# is "" or #comp.employee_count# is "unknown">
			    Unknown
			   <cfelse>
			    #comp.employee_count# Employees
			  </cfif>
			  <cfelse>
			   #comp.size_name#
			  </cfif>
		   </td></tr>

	  <tr><td height=10></td></tr>

</cfoutput>

</table>

<!--- Display SAMS Data Start --->

<cfif session.company_profile_duns is "">

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td colspan=3><hr></td></tr>
			   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Federal Profile</td></tr>
			   <tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;">No Federal information is available.   This Company is not registered with <a href="https://sam.gov" target="_blank" rel="noopener"><b><u>SAM.Gov</u></b></a> or the Company's DUNS number could not be linked to <a href="https://sam.gov" target="_blank" rel="noopener"><b><u>SAM.Gov</u></b></a>.</td></tr>
			   <tr><td height=10></td></tr>
			</table>

<cfelse>

 <cfoutput>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td colspan=3><hr></td></tr>
			   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Federal Profile</td></tr>
			   <tr><td colspan=3 class="feed_sub_header" style="font-weight: normal;">Federal information was linked from <a href="https://sam.gov" target="_blank" rel="noopener"><b><u>SAM.Gov</u></b></a> through the company's DUNS number.</td></tr>
			   <tr><td height=10></td></tr>
			</table>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_option"><b>Corporate Information</b></td></tr>

				<tr><td class="feed_option"><b>Website: </b>

				<cfoutput>
				<cfif left("#sams.corp_url#",4) is "http" or left("#sams.corp_url#",5) is "https">
				  <a href="#sams.corp_url#" target="_blank" rel="noopener"><u>#sams.corp_url#</u></a>
				<cfelse>
				 <a href="http://#sams.corp_url#" target="_blank" rel="noopener"><u>#sams.corp_url#</u></a>
				</cfif>
				</cfoutput><br>

											<b>DUNS: </b>#sams.duns#<cfif #sams.duns_4# is not "">-#sams.duns_4#</cfif><br>
											<b>State of Incorporation: </b>#sams.state_of_inc#<br>
											<b>Cage Code: </b>#sams.cage_code#

											</td></tr>


				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_option"><b>Address</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.phys_address_l1# is not "">#sams.phys_address_l1#<br></cfif>
											<cfif #sams.phys_address_line_2# is not "">#sams.phys_address_line_2#<br></cfif>
											#sams.city_1# #sams.state_1#, #sams.zip_1#<br>
											</td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<tr><td class="feed_option"><b>Important Dates</b></td></tr>
				<tr><td class="feed_option">
											<b>Founded: </b>#mid(sams.biz_start_date,5,2)#/#right(sams.biz_start_date,2)#/#left(sams.biz_start_date,4)#<br>
											<b>Registration Date: </b>#mid(sams.init_reg_date,5,2)#/#right(sams.init_reg_date,2)#/#left(sams.init_reg_date,4)#<br>
											<b>Activation Date: </b>#mid(sams.activation_date,5,2)#/#right(sams.activation_date,2)#/#left(sams.activation_date,4)#<br>
											<b>Expiration Date: </b>#mid(sams.reg_exp_date,5,2)#/#right(sams.reg_exp_date,2)#/#left(sams.reg_exp_date,4)#<br>
											<b>Last Updated: </b>#mid(sams.last_update,5,2)#/#right(sams.last_update,2)#/#left(sams.last_update,4)#<br>

											 <cfif certs.eight_start is not "">
											 <b>8(a) Certification:</b> #dateformat(certs.eight_start,'mm/dd/yyyy')# - #dateformat(certs.eight_end,'mm/dd/yyyy')#<br>
											 </cfif>

											 <cfif certs.hub_start is not "">
											 <b>HubZone Certification:</b> #dateformat(certs.hub_start,'mm/dd/yyyy')# - #dateformat(certs.hub_end,'mm/dd/yyyy')#<br>
											 </cfif>

											 <cfif certs.sdb_start is not "">
											 <b>SDB Certification: </b> #dateformat(certs.sdb_start,'mm/dd/yyyy')# - #dateformat(certs.sdb_end,'mm/dd/yyyy')#<br>
											 </cfif>

											</td></tr>

				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr>
			   <td class="feed_option"><b>Business Type</b></td>

		   <tr><td class="feed_option" valign=top>

		   <cfif sams.biz_type_string is "">
			   Unknown
		   <cfelse>

			   <cfloop index="element" list="#listsort(sams.biz_type_string,'Text','ASC','~')#" delimiters="~">

				<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select biz_code_name from biz_code
				 where biz_code = '#element#'
				</cfquery>

				<cfoutput>
				#code.biz_code_name#<br>
				</cfoutput>
			   </cfloop>

		   </cfif>

			   </td>
			   <td class="feed_option" valign=top></td></tr>


				</table>

			</td></tr>

		   <tr><td colspan=4><hr></td></tr>

		   </table>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<tr><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Primary Point of Contact</b></td></tr>
				<tr><td class="feed_option">#sams.poc_fnme# #sams.poc_lname#<br>
											<cfif #sams.poc_title# is not "">#sams.poc_title#<br></cfif>
											(#left(sams.poc_us_phone,3)#) #mid(sams.poc_us_phone,4,3)#-#right(sams.poc_us_phone,4)#<br>
											<a href="mailto:#sams.poc_email#">#sams.poc_email#</a></td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Alternate Point of Contact</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.alt_poc_lname# is not "">#sams.alt_poc_fname# #sams.alt_poc_lname#<br><cfelse>Not Provided</cfif>
											<cfif #sams.alt_poc_title# is not "">#sams.alt_poc_title#<br></cfif>
											<cfif #sams.akt_poc_phone# is not "">(#left(sams.akt_poc_phone,3)#) #mid(sams.akt_poc_phone,4,3)#-#right(sams.akt_poc_phone,4)#<br></cfif>
											<cfif #sams.alt_poc_email# is not ""><a href="mailto:#sams.alt_poc_email#">#sams.alt_poc_email#</a></cfif></td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Past Performance Point of Contact</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.pp_poc_lname# is not "">#sams.pp_poc_fname# #sams.pp_poc_lname#<br><cfelse>Not Provided</cfif>
											<cfif #sams.pp_poc_title# is not "">#sams.pp_poc_title#<br></cfif>
											<cfif #sams.pp_poc_phone# is not "">(#left(sams.pp_poc_phone,3)#) #mid(sams.pp_poc_phone,4,3)#-#right(sams.pp_poc_phone,4)#<br></cfif>
											<cfif #sams.pp_poc_email# is not ""><a href="mailto:#sams.pp_poc_email#">#sams.pp_poc_email#</a></cfif></td></tr>
				</table>

			</td><td valign=top width=25%>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_option"><b>Electronic Point of Contact</b></td></tr>
				<tr><td class="feed_option"><cfif #sams.elec_bus_poc_lnmae# is not "">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#<br><cfelse>Not Provided</cfif>
											<cfif #sams.elec_bus_poc_title# is not "">#sams.elec_bus_poc_title#<br></cfif>
											<cfif #sams.elec_bus_poc_us_phone# is not "">(#left(sams.elec_bus_poc_us_phone,3)#) #mid(sams.elec_bus_poc_us_phone,4,3)#-#right(sams.elec_bus_poc_us_phone,4)#<br></cfif>
											<cfif #sams.elec_bus_poc_email# is not ""><a href="mailto:#sams.elec_bus_poc_email#">#sams.elec_bus_poc_email#</a></cfif></td></tr>
			</table>

			</td></tr>

		   <tr><td colspan=4><hr></td></tr>

		   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td valign=top width=50%>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td class="feed_option"><b>NAICS Codes</b></td></tr>
		   <tr><td class="feed_option" valign=top>

		   <cfif #sams.naisc_code_string# is "" or #sams.pri_naics# is "">

				No NAICS codes were found.

		   <cfelse>

			   <cfloop index="element" list="#listsort(sams.naisc_code_string,'Text','ASC','~')#" delimiters="~">

				<cfquery name="code" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select naics_code, naics_code_description from naics
				 where naics_code = '#left(element,'6')#'
				</cfquery>

				<cfoutput>
				<cfif #sams.pri_naics# is #left(element,'6')#>
				 <b>#left(element,'6')# - #code.naics_code_description#</b><br>
				<cfelse>
				 #left(element,'6')# - #code.naics_code_description#<br>
				</cfif>
				</cfoutput>
			   </cfloop>

			   <br>

			   <b><i>Bold - Indicates primary NAICS code</i></b>

		   </cfif>

		   </td></tr>

		   </table>

	   </td><td valign=top width=50%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_option"><b>Product or Service Codes</b></td></tr>
		<tr><td class="feed_option">

		   <cfif #sams.psc_code_string# is "">
			No Product or Service codes found.

		   <cfelse>

		   <cfloop index="psc_element" list="#listsort(sams.psc_code_string,'Text','ASC','~')#" delimiters="~">

			<cfquery name="psc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select psc_code, psc_description from psc
			 where psc_code = '#psc_element#'
			</cfquery>

			<cfoutput>
			 <b>#psc.psc_code#</b> - #psc.psc_description#<br>
			</cfoutput>

		   </cfloop>

	   </cfif>

	   </td></tr>

	  </table>

	</cfoutput>

</td></tr>

</table>

</cfif>

<!--- Display SAMS Data End --->

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=10></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
  <tr><td height=10></td></tr>
  <tr><td width=100>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener"><img src="/images/cb_logo.png" width=75 alt="Crunchbase" title="Crunchbase"></a></td></tr>
      </table>

      </td><td>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_sub_header"><a href="http://www.crunchbase.com" target="_blank" rel="noopener"><b>Crunchbase</b></a></td><td align=right class="feed_sub_header"><a href="http://www.crunchbase.com" target="_blank" rel="noopener"><u>http://www.crunchbase.com</u></a></td></tr>
       <tr><td class="feed_option" style="font-weight: normal;" colspan=2>The Exchange uses Crunchbase information to strengthen and complement a company's profile with information related to funding, investments, founders and staff, biography's of people, and other information related to a company's background and corporate structure.</td></tr>
      </table>

      </td></tr>

</table>

<cfquery name="news" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from news
 where news_company_id = '#session.company_profile_id#' and
       news_public = 1
 order by news_order ASC
</cfquery>

<cfif news.recordcount GT 0>

	<td width=50>&nbsp;</td><td valign=top width=300>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_option"><b>Latest Company News</b></td></tr>
	  <tr><td><hr></td></tr>
	  <tr><td height=10></td></tr>
	</table>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfoutput query="news">

	<tr><td valign=top>

	   <cfif #news.news_image# is "">

			<cfif #news.news_url# is not "">
			 <a href="#news_url#" target="_blank" rel="noopener"><img src="/images/update.png" width=40 vspace=5 border=0></a>
			<cfelse>
			 <img src="#image_virtual#/update.png" width=40 vspace=5 border=0>
			</cfif>

	   <cfelse>

			<cfif #news.news_url# is not "">
			  <a href="#news_url#" target="_blank" rel="noopener"><img src="#media_virtual#/#news_image#" width=75 vspace=5 border=0></a>
			<cfelse>
			  <img src="#media_virtual#/#news_image#" width=75 vspace=5 border=0>
			</cfif>

	   </cfif>

	   </td><td width=15>&nbsp;</td><td valign=top><span class="feed_sub_header">

	   <cfif #news.news_url# is not "">
		   <a href="#news_url#" target="_blank" rel="noopener">#news.news_title#</a></span><br><span class="link_small_gray">#dateformat(news_date,'mmmm dd, yyyy')#</span>
	   <cfelse>
		   #news.news_title#</span><br><span class="link_small_gray">#dateformat(news_date,'mmmm dd, yyyy')#</span>
	   </cfif>

	   </td></tr>

	<tr><td colspan=3 class="feed_option"><cfif len(news_desc) GT 300>#left(news_desc,'300')#...<cfelse>#news_desc#</cfif></td></tr>

    <tr><td height=10></td></tr>
    <tr><td colspan=3><hr></td></tr>
    <tr><td height=10></td></tr>

    </cfoutput>

   </table>

</cfif>

</td></tr>

</table>
