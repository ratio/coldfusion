<!-- MenuMaker Plugin -->

<script src="https://s3.amazonaws.com/menumaker/menumaker.min.js"></script>

<!-- Icon Library -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="/include/menu.css" rel="stylesheet" type="text/css">

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_xref
 join usr on usr_id = hub_xref_usr_id
 where hub_xref_usr_id = #session.usr_id# and
       hub_xref_hub_id = #session.hub#
</cfquery>

<cfif usr.hub_xref_usr_role is 1>
 <cfset hub_admin = 1>
<cfelse>
 <cfset hub_admin = 0>
</cfif>

	 <cfquery name="hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select * from hub
	  where hub_id = #session.hub#
	 </cfquery>

     <div class="head">

	 <cfoutput>
	   <table style="position: fixed;" cellspacing=0 cellpadding=0 border=0 width=100% height=50 bgcolor="<cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>">
	 </cfoutput>

 		  <tr>
				  <td width=10>&nbsp;</td>

				  <cfoutput>

					  <td width=15>&nbsp;</td>
                      <td class="hub_header"><a href="/exchange/"><font color="ffffff">#hub.hub_name#</font></a></td>

				  </cfoutput>

                  <td align=right>

				  <!--- Header Menu --->

					<cfoutput><div id="cssmenu" style="background: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>"></cfoutput>

					  <ul>
						 <li><a href="/exchange/"><i class="fa fa-fw fa-home"></i> &nbsp;Home</a></li>

                      <cfif usr.hub_xref_role_id is 1>

						 <li><a href="/exchange/marketplace/partners/"><i class="fa fa-share-alt"></i> &nbsp;Network</a>
								<ul style="background-color: 000000;">
								   <li><a href="/exchange/marketplace/partners/" style="background-color: 000000;"><i class="fa fa-fw fa-briefcase" style="padding-right: 17px;"></i>My Partners</a></li>
								   <li><a href="/exchange/marketplace/partners/network_in.cfm" style="background-color: 000000;"><i class="fa fa-fw fa-thumbs-up" style="padding-right: 17px;"></i>In Network Partners</a></li>
								   <li><a href="/exchange/marketplace/partners/network_out.cfm" style="background-color: 000000;"><i class="fa fa-fw fa-industry" style="padding-right: 17px;"></i>Out of Network Companies</a></li>
								   <li><a href="/exchange/marketplace/people/set.cfm?v=3" style="background-color: 000000;"><i class="fa fa-user" style="padding-right: 8px;"></i>People</a></li>
							       <cfif session.role is 1>
								   	<li><a href="/exchange/messages/" style="background-color: 000000;"><i class="fa fa-comment" style="padding-right: 6px;"></i>Messaging</a></li>
								   	<li><a href="/exchange/marketplace/communities/" style="background-color: 000000;"><i class="fa fa-fw fa-users" style="padding-right: 6px;"></i>&nbsp;Communities</a></li>
								   </cfif>
								</ul>
							 </li>
						 </li>

						 <li><a href="#"><i class="fa fa-fw fa-institution"></i> &nbsp;Tools</a>
								<ul style="background-color: 000000;">
								   <li><a href="/exchange/pipeline/" style="background-color: 000000;"><i class="fa fa-fw fa-pie-chart"></i>  &nbsp;Opportunity Modeling</a></li>
								   <li><a href="/exchange/portfolio/" style="background-color: 000000;"><i class="fa fa-fw fa-suitcase"></i>  &nbsp;Company Portfolios</a></li>

							       <cfif session.role is 1>
									   <li><a href="/exchange/sourcing/needs/" style="background-color: 000000;"><i class="fa fa-info-circle"></i>  &nbsp;&nbsp;&nbsp;Partner Needs</a></li>
								   </cfif>

								   <li><a href="/exchange/market/" style="background-color: 000000;"><i class="fa fa-fw fa-cubes"></i>  &nbsp;Market Reports</a></li>
								   <li><a href="/exchange/groups/" style="background-color: 000000;"><i class="fa fa-users"></i>  &nbsp;&nbsp;Groups</a></li>

								</ul>
							 </li>
						 </li>

						 <li><a href="/exchange/awards/"><i class="fa fa-fw fa-search-plus"></i> Research</a>
								<ul style="background-color: 000000;">
								   <li><a href="/exchange/awards/" style="background-color: 000000;"><i class="fa fa-fw fa-pie-chart"></i> &nbsp;Federal Awards&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
									  <ul style="background-color: 000000;">
										 <li><a href="/exchange/awards/agency/" style="background-color: 000000;">By Department</a></li>
										 <li><a href="/exchange/awards/state/" style="background-color: 000000;">By State</a></li>
										 <li><a href="/exchange/awards/naics/" style="background-color: 000000;">By NAICS Code</a></li>
										 <li><a href="/exchange/awards/product/" style="background-color: 000000;">By Product or Service Code</a></li>
										 <li><a href="/exchange/awards/sb/" style="background-color: 000000;">By Set Aside</a></li>
									  </ul>
								   </li>

								   <li><a href="/exchange/grants/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-dollar"></i> &nbsp;Grants</a></li>
								   <li><a href="/exchange/sbir/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-map"></i> &nbsp;SBIR/STTRs</a></li>
								   <li><a href="/exchange/vehicles/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-cab"></i> &nbsp;Contract Vehicles</a></li>
								   <li><a href="/exchange/opps/poc/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-user"></i>  &nbsp;Contracting POC's</a></li>
								   <li><a href="/exchange/source/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-diamond"></i> &nbsp;Sourcing</a></li>
								   <li><a href="/exchange/patents/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-flask"></i>  &nbsp;Patents</a></li>
								   <li><a href="/exchange/research/labs/" style="background-color: 000000;" onclick="javascript:document.getElementById('page-loader').style.display='block';"><i class="fa fa-fw fa-cubes"></i>  &nbsp;Lab Technologies</a></li>
								   <li><a href="/exchange/compete/" style="background-color: 000000;"><i class="fa fa-fw fa-bar-chart"></i>  &nbsp;Competitor Analysis</a></li>

								   <li><a href="/exchange/research/investors/" style="background-color: 000000;"><i class="fa fa-usd" aria-hidden="true" style="padding-left: 3px;"></i>&nbsp;&nbsp;&nbsp;&nbsp;Investors & Investments</a></li>
								   <li><a href="/exchange/research/sb/" style="background-color: 000000;"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Socio-Economic Analysis</a></li>


								</ul>
						 </li>

						 <li><a href="/exchange/profile/"><i class="fa fa-fw fa-cog"></i> &nbsp;Profile</a>


						 </li>


                      <cfelse>

						 <li><a href="#"><i class="fa fa-share-alt"></i> &nbsp;Network</a>
						 	<ul>
							   <li style="background-color: 000000;"><a href="/exchange/employees.cfm" style="background-color: 000000;"><i class="fa fa-fw fa-dollar"></i>  &nbsp;Booz Allen Employees</a></li>
<!---							   <li style="background-color: 000000;"><a href="/exchange/marketplace/communities/" style="background-color: 000000;"><i class="fa fa-fw fa-dollar"></i>  &nbsp;Communities</a></li> --->
						 	</ul>
						 </li>
						 <li><a href="/exchange/opps/needs.cfm"><i class="fa fa-fw fa-line-chart"></i> &nbsp;Opportunities</a>
						 	<ul>
							   <li><a href="/exchange/opps/needs.cfm" style="background-color: 000000;"><i class="fa fa-th-large"></i>  &nbsp;Booz Allen Needs</a></li>
							   <li><a href="/exchange/enablement/" style="background-color: 000000;"><i class="fa fa-info-circle"></i>  &nbsp;Enablement Services</a></li>
						 	</ul>
						 </li>

						 <li><a href="#"><i class="fa fa-fw fa-cog"></i> &nbsp;Profile</a>

						 	<ul>
							   <li style="background-color: 000000;"><a href="/exchange/profile/" style="background-color: 000000;"><i class="fa fa-user"></i>  &nbsp;My Profile</a></li>
						       <li style="background-color: 000000;"><a href="/exchange/company/" style="background-color: 000000;"><i class="fa fa-industry"></i>  &nbsp;My Company</a></li>
						 	</ul>

						 </li>

	                  </cfif>


					 <li><a href="#" onclick="window.open('/exchange/include/page_help.cfm?page=<cfoutput>#cgi.script_name#</cfoutput>','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=575'); return false;"><i class="fa fa-question-circle"></i> &nbsp;Help</a>

					 <cfif hub.hub_owner_id is #session.usr_id# or #hub_admin# is 1>
						 <li><a href="/exchange/admin/"><i class="fa fa-fw fa-cog"></i> Admin</a></li>
					 </cfif>

					 <li><a href="/exchange/logout.cfm"><i class="fa fa-fw fa-paper-plane"></i> Logout</a></li>
					  </ul>
					</div>

                  </td><td width=20>&nbsp;</td>
          </tr>

</table>

</div>

<div class="space">&nbsp;</div>

