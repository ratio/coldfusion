<cfinclude template="/exchange/security/check.cfm">

 <cfif trim(person_portfolio_name) is "">

	<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into person_portfolio_item
	  (
	   person_portfolio_item_portfolio_id,
	   person_portfolio_item_person_id,
	   person_portfolio_item_usr_id,
	   person_portfolio_item_updated,
	   person_portfolio_item_hub_id,
	   person_portfolio_item_company_id
	   )
	  values
	  (#person_portfolio_item_portfolio_id#,
	   #person_portfolio_item_person_id#,
	   #session.usr_id#,
	   #now()#,
	   #session.hub#,
	   <cfif #session.company_id# is 0>null<cfelse>#session.company_id#</cfif>
	  )
	</cfquery>

 <cfelse>

	<cftransaction>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into person_portfolio
		 (
		  person_portfolio_name,
		  person_portfolio_usr_id,
		  person_portfolio_company_id,
		  person_portfolio_hub_id,
		  person_portfolio_updated,
		  person_portfolio_access_id
		  )
		  values
		  (
		  '#person_portfolio_name#',
		   #session.usr_id#,
		   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
		   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
		   #now()#,
		   1
		  )
		 </cfquery>

		 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select max(person_portfolio_id) as id from person_portfolio
		 </cfquery>

	<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into person_portfolio_item
	  (
	   person_portfolio_item_portfolio_id,
	   person_portfolio_item_person_id,
	   person_portfolio_item_usr_id,
	   person_portfolio_item_updated,
	   person_portfolio_item_hub_id,
	   person_portfolio_item_company_id
	   )
	  values
	  (#max.id#,
	   #person_portfolio_item_person_id#,
	   #session.usr_id#,
	   #now()#,
	   #session.hub#,
	   <cfif #session.company_id# is 0>null<cfelse>#session.company_id#</cfif>
	  )
	</cfquery>

	</cftransaction>

	<cfset #portfolio_item_portfolio_id# = #max.id#>

</cfif>

<!--- Update Portfolio --->

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update person_portfolio
   set person_portfolio_updated = #now()#
   where person_portfolio_id = #person_portfolio_item_portfolio_id# and
		 person_portfolio_usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>

         <tr><td class="feed_header">Person has been saved to your Portfolio</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>