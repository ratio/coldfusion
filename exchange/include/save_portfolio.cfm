<cfinclude template="/exchange/security/check.cfm">

	<cfif #portfolio_item_type_id# is 1>

	<!--- Company --->

    <cfif button is "Create and Save">

		<cftransaction>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into portfolio
			 (
			  portfolio_name,
			  portfolio_usr_id,
			  portfolio_company_id,
			  portfolio_hub_id,
			  portfolio_updated,
			  portfolio_access_id,
			  portfolio_type_id
			  )
			  values
			  (
			  '#portfolio_name#',
			   #session.usr_id#,
			   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
			   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			   #now()#,
			   1,
			   1
			  )
			 </cfquery>

			 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select max(portfolio_id) as id from portfolio
			 </cfquery>

			 <cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			   insert into portfolio_item
			   (portfolio_item_type_id, portfolio_item_company_id, portfolio_item_duns, portfolio_item_portfolio_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
			   values
			   (#portfolio_item_type_id#, #portfolio_item_company_id#, '#portfolio_item_duns#', #max.id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
			 </cfquery>

        </cftransaction>

        <cfset #portfolio_item_portfolio_id# = #max.id#>

    <cfelse>

		 <cfif trim(portfolio_name) is "">

			<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into portfolio_item
			  (portfolio_item_type_id, portfolio_item_company_id, portfolio_item_duns, portfolio_item_portfolio_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
			  values
			  (#portfolio_item_type_id#, #portfolio_item_company_id#, '#portfolio_item_duns#', #portfolio_item_portfolio_id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
			</cfquery>

		 <cfelse>

			<cftransaction>

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert into portfolio
				 (
				  portfolio_name,
				  portfolio_usr_id,
				  portfolio_company_id,
				  portfolio_hub_id,
				  portfolio_updated,
				  portfolio_access_id,
				  portfolio_type_id
				  )
				  values
				  (
				  '#portfolio_name#',
				   #session.usr_id#,
				   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
				   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
				   #now()#,
				   1,
				   1
				  )
				 </cfquery>

				 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select max(portfolio_id) as id from portfolio
				 </cfquery>

				<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into portfolio_item
				  (portfolio_item_type_id, portfolio_item_company_id, portfolio_item_duns, portfolio_item_portfolio_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
				  values
				  (#portfolio_item_type_id#, #portfolio_item_company_id#, '#portfolio_item_duns#', #max.id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
				</cfquery>

			</cftransaction>

			<cfset #portfolio_item_portfolio_id# = #max.id#>

		</cfif>

    </cfif>

		<cfif trim(portfolio_item_comments) is not "">

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert company_intel
			  (company_intel_context,
			   company_intel_portfolio_id,
			   company_intel_company_id,
			   company_intel_comments,
			   company_intel_created_date,
			   company_intel_hub_id,
			   company_intel_created_by_usr_id,
			   company_intel_created_by_company_id
			   )
			  values
			  (
			   'Save Company to Portfolio Comment',
				#portfolio_item_portfolio_id#,
				#portfolio_item_company_id#,
			   '#portfolio_item_comments#',
				#now()#,
			    #session.hub#,
				#session.usr_id#,
				<cfif session.company_id is 0 or not isdefined("session.company_id")>null<cfelse>#session.company_id#</cfif>
			   )
			</cfquery>

		</cfif>

    <cfelseif #portfolio_item_type_id# is 2>

    <!--- Award --->

    <cfif button is "Create and Save">

		<cftransaction>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into portfolio
			 (
			  portfolio_name,
			  portfolio_usr_id,
			  portfolio_company_id,
			  portfolio_hub_id,
			  portfolio_updated,
			  portfolio_access_id,
			  portfolio_type_id
			  )
			  values
			  (
			  '#portfolio_name#',
			   #session.usr_id#,
			   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
			   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			   #now()#,
			   1,
			   2
			  )
			 </cfquery>

			 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select max(portfolio_id) as id from portfolio
			 </cfquery>

			 <cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into portfolio_item
			  (portfolio_item_name, portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_value_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
			  values
			  ('#portfolio_item_name#',#portfolio_item_type_id#, #max.id#,#portfolio_item_award_id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
			 </cfquery>

        </cftransaction>

        <cfset #portfolio_item_portfolio_id# = #max.id#>

    <cfelseif button is "Save Award to Portfolio">

		 <cfif trim(portfolio_name) is "">

			<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into portfolio_item
			  (portfolio_item_name, portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_value_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
			  values
			  ('#portfolio_item_name#',#portfolio_item_type_id#, #portfolio_item_portfolio_id#,#portfolio_item_award_id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
			</cfquery>

		 <cfelse>

			<cftransaction>

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert into portfolio
				 (
				  portfolio_name,
				  portfolio_usr_id,
				  portfolio_company_id,
				  portfolio_hub_id,
				  portfolio_updated,
				  portfolio_access_id,
				  portfolio_type_id
				  )
				  values
				  (
				  '#portfolio_name#',
				   #session.usr_id#,
				   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
				   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
				   #now()#,
				   1,
				   2
				  )
				 </cfquery>

				 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select max(portfolio_id) as id from portfolio
				 </cfquery>

				 <cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into portfolio_item
				  (portfolio_item_name, portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_value_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
				  values
				  ('#portfolio_item_name#',#portfolio_item_type_id#, #max.id#,#portfolio_item_award_id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
				 </cfquery>

			</cftransaction>

			<cfset #portfolio_item_portfolio_id# = #max.id#>

		 </cfif>

    </cfif>

    <cfelseif #portfolio_item_type_id# is 3>

		<cfif button is "Create and Save">

		<cftransaction>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into portfolio
			 (
			  portfolio_name,
			  portfolio_usr_id,
			  portfolio_company_id,
			  portfolio_hub_id,
			  portfolio_updated,
			  portfolio_access_id,
			  portfolio_type_id
			  )
			  values
			  (
			  '#portfolio_name#',
			   #session.usr_id#,
			   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
			   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
			   #now()#,
			   1,
			   3
			  )
			 </cfquery>

			 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select max(portfolio_id) as id from portfolio
			 </cfquery>

			 <cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into portfolio_item
			  (portfolio_item_name, portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_opp_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
			   values
			  ('#portfolio_item_name#',#portfolio_item_type_id#, #max.id#, #fbo_id#, #session.usr_id#, #now()#, '#portfolio_item_comments#')
			 </cfquery>

        </cftransaction>

        <cfset #portfolio_item_portfolio_id# = #max.id#>

		<cfelseif button is "Save Opportunity to Portfolio">


		 <cfif trim(portfolio_name) is "">

			<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into portfolio_item
			  (portfolio_item_name, portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_opp_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
			  values
			  ('#portfolio_item_name#',#portfolio_item_type_id#, #portfolio_item_portfolio_id#, #fbo_id#, #session.usr_id#, #now()#, '#portfolio_item_comments#')
			</cfquery>

		 <cfelse>

			<cftransaction>

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 insert into portfolio
				 (
				  portfolio_name,
				  portfolio_usr_id,
				  portfolio_company_id,
				  portfolio_hub_id,
				  portfolio_updated,
				  portfolio_access_id,
				  portfolio_type_id
				  )
				  values
				  (
				  '#portfolio_name#',
				   #session.usr_id#,
				   <cfif #session.company_id# is not 0>#session.company_id#<cfelse>null</cfif>,
				   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
				   #now()#,
				   1,
				   3
				  )
				 </cfquery>

				 <cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  select max(portfolio_id) as id from portfolio
				 </cfquery>

				<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into portfolio_item
				  (portfolio_item_name, portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_opp_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
				  values
				  ('#portfolio_item_name#',#portfolio_item_type_id#, #max.id#, #fbo_id#, #session.usr_id#, #now()#, '#portfolio_item_comments#')
				</cfquery>

			</cftransaction>

			<cfset #portfolio_item_portfolio_id# = #max.id#>

		 </cfif>

		</cfif>

    </cfif>

 	<!--- Update Portfolio --->

 	<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       update portfolio
       set portfolio_updated = #now()#
       where portfolio_id = #portfolio_item_portfolio_id# and
             portfolio_usr_id = #session.usr_id#
    </cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>

         <cfif #portfolio_item_type_id# is 1>
         <tr><td class="feed_header">Company has been saved to your Portfolio</td></tr>
         <cfelseif #portfolio_item_type_id# is 2>
         <tr><td class="feed_header">Award has been saved to your Portfolio</td></tr>
         </cfif>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>