<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfquery name="comp" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 left join portfolio_item on portfolio_item_portfolio_id = portfolio_id
 left join usr on usr_id = portfolio_usr_id
 where portfolio_usr_id = #session.usr_id# and
	   portfolio_company_id = #session.company_id# and
	   portfolio_type_id = 1 and
	   portfolio_item_company_id = #id#
</cfquery>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">REMOVE FROM PORTFOLIO</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<cfoutput query="comp">
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td valign=top width=130>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=10></td></tr>
			   <tr><td>
					<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif comp.company_logo is "">
					  <img src="//logo.clearbit.com/#comp.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
					  <img src="#media_virtual#/#comp.company_logo#" width=100 border=0>
					</cfif>
					</a>
			   </td></tr>
			   </table>
             </td><td valign=top>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr><td class="feed_title"><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">#company_name#</a></td>
			   <td align=right valign=top width=100></td></tr>
               <tr><td class="feed_option">
				<cfif #comp.company_city# is "" or #comp.company_state# is "">
				 <cfif #comp.company_city# is "">
				  #comp.company_state#
				 <cfelse>
				  #comp.company_state#
				 </cfif>
				<cfelse>
				 <b>#comp.company_city#, #comp.company_state#</b>
				</cfif>
			   </td></tr>
		       <tr><td colspan=3 class="feed_option" valign=top><cfif comp.company_about is "">Company description not provided.<cfelse><cfif len(comp.company_about GT 400)>#left(comp.company_about,'400')#...<cfelse>#comp.company_about#</cfif></cfif></td></tr>
		   </table>
		   </td></tr>

		   <tr><td colspan=2><hr></td></tr>
     </table>
</cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="/exchange/include/remove_portfolio.cfm" method="post">

     <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfoutput>#comp.company_name#</cfoutput> has been added to the below Portfolio(s).  Please choose the Portfolio you'd like to remove this Company from.</td></tr>

     <cfoutput query="portfolio">
      <tr>
          <td class="feed_sub_header" width=40><input type="checkbox" name="remove_id" value=#portfolio_id# required style="width: 22px; height: 22px;"></td>
          <td class="feed_sub_header">#portfolio_name#</td>
      </tr>

      <input type="hidden" name="id" value=#id#>

     </cfoutput>

	 <tr><td height=10></td></tr>
	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Remove from Portfolio(s)">

 </form>

</table>

</center>

</body>
</html>

