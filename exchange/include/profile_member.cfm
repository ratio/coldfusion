<cfinclude template="/exchange/security/check.cfm">

<cfquery name="profile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #id#
</cfquery>

<cfif profile.usr_company_id is not "">

	<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from company
	 left join size on size_id = company_size_id
	 left join entity on entity_id = company_entity_id
	 where company_id = #profile.usr_company_id#
	</cfquery>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">MEMBER PROFILE</td>
		       <td align=right><img src="/images/delete.png" align=absmiddle border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td height=20></td></tr>
	      </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td valign=top width=160>

		   <cfinclude template="profile_member_left.cfm">

           </td><td valign=top width=40%>

           <cfoutput>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">#profile.usr_first_name# #profile.usr_last_name#</td></tr>
				   <cfif profile.usr_title is not "">
				   <tr><td class="feed_sub_header">#profile.usr_title#</td></tr>
				   </cfif>
                   <tr><td><hr></td></tr>
				   <tr><td class="feed_option"><b>Email</b>: #profile.usr_email#</td></tr>
				   <cfif #profile.usr_phone# is not "">
				   	<tr><td class="feed_option"><b>Phone</b>: #profile.usr_phone#</td></tr>
				   </cfif>
				   <tr><td class="feed_option"><b>Address</b></td></tr>
				   <tr><td class="feed_option">
				   <cfif #profile.usr_address_line_1# is "" and #profile.usr_address_line_2# is "">
				   <cfelse>
				   	#profile.usr_address_line_1#<br><cfif #profile.usr_address_line_2# is not "">#profile.usr_address_line_2#<br>
				   </cfif>
				   </cfif>#profile.usr_city# #profile.usr_state#.  #profile.usr_zip#</td></tr>

                   <tr><td><hr></td></tr>

                   <cfif profile.usr_about is not "">
					   <tr><td class="feed_sub_header" colspan=3><b>About Me</b></td></tr>
					   <tr><td class="feed_option" colspan=3>#replace(profile.usr_about,"#chr(10)#","<br>","all")#</td></tr>
				   </cfif>

                   <cfif profile.usr_experience is not "">
					   <tr><td class="feed_sub_header" colspan=3><b>Experience</b></td></tr>
					   <tr><td class="feed_option" colspan=3>#replace(profile.usr_experience,"#chr(10)#","<br>","all")#</td></tr>
				   </cfif>

                   <cfif profile.usr_education is not "">
					   <tr><td class="feed_sub_header" colspan=3><b>Education</b></td></tr>
					   <tr><td class="feed_option" colspan=3>#replace(profile.usr_education,"#chr(10)#","<br>","all")#</td></tr>
				   </cfif>

				</table>

            </td><td valign=top><td width=50>&nbsp;</td><td valign=top>

             <cfif profile.usr_company_id is not "">

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				   <tr><td class="feed_header">#company.company_name#</td></tr>
                   <tr><td height=10></td></tr>
                   <cfif #company.company_about# is not "">
				    <tr><td class="feed_option">#company.company_about#</td></tr>
				    <tr><td><hr></td></tr>
                   </cfif>
				   <tr><td class="feed_option"><b>Location</b></td></tr>
				   <tr><td class="feed_option">#company.company_address_l1#</td></tr>
				   <tr><td class="feed_option">#company.company_city#, #company.company_state#  #company.company_zip#</td></tr>
				   <cfif #company.company_website# is not "">
				   <tr><td class="feed_option"><b>Website: </b><a href="#company.company_website#" target="_blank" rel="noopener" rel="noreferrer"><u>#company.company_website#</u></a></td></tr>
				   </cfif>

				   <tr><td class="feed_option"><b>Founded: </b>#company.company_founded#</td></tr>
				   <tr><td class="feed_option"><b>Size: </b><cfif #company.size_name# is "">Unknown<cfelse>#company.size_name#</cfif><br>
				   <tr><td class="feed_option"><b>Type: </b><cfif #company.entity_name# is "">Unknown<cfelse>#company.entity_name#</cfif><br>

               </table>

             </cfif>

           </td></tr>

 		  </table>

		   </cfoutput>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

