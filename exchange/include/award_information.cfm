<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif isdefined("prime_award_id")>
 <cfquery name="getid" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) id from award_data
  where award_id_piid = '#prime_award_id#' and
        recipient_duns = '#duns#'
  order by action_date ASC
 </cfquery>

 <cfset #id# = #getid.id#>

</cfif>

<cfif id is "">
 Contract could not be found.
 <cfabort>
</cfif>

<cfquery name="award" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_data
 where id = #id#
</cfquery>

<cfquery name="totals" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select sum(federal_action_obligation) as obligation, sum(base_and_exercised_options_value) as options, sum(base_and_all_options_value) as totalvalue from award_data
 where award_id_piid = '#award.award_id_piid#' and
       recipient_duns = '#award.recipient_duns#'
</cfquery>

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_award_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#, #session.hub#,#now()#)
</cfquery>

<div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box" style="margin-left: 0px; margin-right: 0px; margin-top: 30px;">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=top>Award Information</td>

             <td align=right>

					<cfoutput>
					<div class="dropdown">
					  <img src="/images/3dots2.png" style="cursor: pointer;" height=10 hspace=10>
					  <div class="dropdown-content" style="width: 250px; text-align: left; padding-left: 0px;">
                        <a href="##" onclick="window.open('/exchange/include/save_opp.cfm?id=#award.id#&t=award','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"><i class="fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Pin to Opportunity Board</a>
                        <!--- <a href="##" onclick="window.open('/exchange/include/save_experience.cfm?id=#award.id#&t=award','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Mark My Experience</a> --->
						<a href="award_history.cfm?id=#id#&award_id=#award.award_id_piid#&duns=#award.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><i class="fa fa-history" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Award History</a>
                        <a href="##" onclick="windowClose();"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Close</a>
					  </div>
					</div>
					</cfoutput>

             </td></tr>

         <tr><td colspan=2><hr></td></tr>

       </table>

    <cfoutput query="award">

    <table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr>

    <td class="feed_option" width=300 valign=top><b>
                            Recipient Name<br>
                            DUNS<br>
                            Contact Information
                            </b></td>

    <td class="feed_option" valign=top>#recipient_name#<br>
                            #recipient_duns#<br>
                            <cfif #recipient_address_line_1# is not "">#recipient_address_line_1#<br></cfif>
                            <cfif #recipient_address_line_2# is not "">#recipient_address_line_2#<br></cfif>
                            #recipient_city_name# #recipient_state_code#. #recipient_zip_4_code# #recipient_country_code#<br>
                            <cfif #recipient_phone_number# is not "">Phone:  (#left(recipient_phone_number,3)#) #mid(recipient_phone_number,4,3)#-#right(recipient_phone_number,4)#<br></cfif>
                            <cfif #recipient_fax_number# is not "">Fax:  (#left(recipient_fax_number,3)#) #mid(recipient_fax_number,4,3)#-#right(recipient_fax_number,4)#</cfif>
                            </td>

    <td class="feed_option" valign=top><b>
                            Number of Employees<br>
                            Annual Revenue<br>
                            Doing Business As<br>
                            Parent DUNS<br>
                            Congressional District
                            </b></td>

    <td class="feed_option" valign=top>
                            <cfif #number_of_employees# is "">Not recorded<cfelse>#number_of_employees#</cfif><br>
                            <cfif #annual_revenue# is "">Not recorded<cfelse>#numberformat(annual_revenue,'$999,999,999,999')#</cfif><br>
                            #recipient_parent_name#<br>
                            #recipient_parent_duns#<br>
                            #recipient_congressional_district#
                            </td>
    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>

    <td class="feed_option" width=200 valign=top><b>
                            Department<br>
                            Agency<br>
                            Office
                            </b></td>

    <td class="feed_option" valign=top>#awarding_agency_name#<br>
                            #awarding_sub_agency_name#<br>
                            #awarding_office_name#
                            </td>

    <td class="feed_option" valign=top><b>
                            Funding Department<br>
                            Funding Agency<br>
                            Funding Office
                            </b></td>

    <td class="feed_option" valign=top>#funding_agency_name#<br>
                            #funding_sub_agency_name#<br>
                            #funding_office_name#
                            </td>
    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>
    <td class="feed_option" valign=top><b>
                            Parent Contract Number<br>
                            Contract Number<br>
                            Modification Number<br>
                            Transaction Number
                            </b></td>

    <td class="feed_option" valign=top>
                            #parent_award_id#<br>
                            #award_id_piid#&nbsp;&nbsp;(&nbsp;<b><a href="award_history.cfm?id=#id#&award_id=#award_id_piid#&duns=#recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">View Contract History</a></b>&nbsp;)<br>
                            #modification_number#<br>
                            #transaction_number#
                            </td>

    <td class="feed_option" valign=top><b>
                            <br>
                            Obligation<br>
                            Base & Exercised Options<br>
                            Base & All Options Value
                            </b></td>

    <td class="feed_option" valign=top>

    <table cellspacing=0 cellpadding=0 border=0 width=100%>
     <tr><td class="feed_option">
          <b>This Award / Mod</b>
          <br>
                            #numberformat(federal_action_obligation,'$999,999,999,999')#<br>
                            #numberformat(base_and_exercised_options_value,'$999,999,999,999')#<br>
                            #numberformat(base_and_all_options_value,'$999,999,999,999')#<br>
                            <b>Spend Analysis</b>
          </td>
         <td class="feed_option"><b>Total Contract Value</b>
         <br>
                            <a href="award_history.cfm?id=#id#&award_id=#award_id_piid#&duns=#recipient_duns#">#numberformat(totals.obligation,'$999,999,999,999')#</a><br>
                            <a href="award_history.cfm?id=#id#&award_id=#award_id_piid#&duns=#recipient_duns#">#numberformat(totals.options,'$999,999,999,999')#</a><br>
                            <a href="award_history.cfm?id=#id#&award_id=#award_id_piid#&duns=#recipient_duns#">#numberformat(totals.totalvalue,'$999,999,999,999')#</a><br>

                            <progress value="#evaluate(totals.obligation)#" max="#totals.totalvalue#" alt="Spent - #numberformat(evaluate(totals.obligation),'$999,999,999')#" title="Spent - #numberformat(evaluate(totals.obligation),'$999,999,999')#" style="width:100px; height: 20px;"></progress></a>

         </td>
         </tr>
    </table>


                            </td>
    </tr>


    <tr><td colspan=4><hr></td></tr>

    <tr><td class="feed_option" valign=top><b>Award Description</b></td>
        <td class="feed_option" colspan=3>#award_description#</td></tr>
    <tr><td colspan=4><hr></td></tr>


    <tr>

    <td class="feed_option" valign=top><b>
                            Action / Award Date<br>
                            Period of Performance Start Date<br>
                            Period of Performance Current End Date<br>
                            Period of Performance Potential End Date<br>
                            Ordering Period End Date<br>
                            Parent Award Type<br>
                            Parent Award (Single or Multiple)

                            </b></td>

    <td class="feed_option" valign=top>#dateformat(action_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_start_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#<br>
                            #dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#<br>
                            #dateformat(ordering_period_end_date,'mm/dd/yyyy')#<br>
							#parent_award_type#<br>
							#parent_award_single_or_multiple#

    <td class="feed_option" valign=top><b>
                            Contract Pricing<br>
                            Type of Set Aside<br>
                            Extend Competed<br>
                            Award Type<br>
                            Action Type<br>
                            IDV Type<br>
                            IDC Type
                            </b></td>

    <td class="feed_option" valign=top>#type_of_contract_pricing#<br>
                            #type_of_set_aside#<br>
                            #extent_competed#<br>
                            #award_type#<br>
                            #action_type#<br>
                            #idv_type#<br>
                            #type_of_idc#</td>

    </tr>


    <tr><td colspan=4><hr></td></tr>

    </cfoutput>

    <tr><td class="feed_option" valign=top><b>Sub Contractors</b></td></tr>

	<cfquery name="subs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select distinct(subaward_number), subaward_action_date, subawardee_name, subawardee_duns, subaward_naics_code, subaward_amount from award_data_sub
	 where prime_award_piid = '#award.award_id_piid#' and
	       prime_awardee_duns = '#award.recipient_duns#'
	       order by subaward_action_date DESC
	</cfquery>

	<cfif #subs.recordcount# is 0>
	 <tr><td class="feed_option" colspan=2>No sub contractors were found or reported for this award.</td></tr>
    <cfelse>

		<tr><td colspan=4>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

		    <tr height=40>
		       <td class="feed_option"><b>Action Date</b></td>
		       <td class="feed_option"><b>Sub Award Number</b></td>
		       <td class="feed_option"><b>Name</b></td>
		       <td class="feed_option"><b>DUNS</b></td>
		       <td class="feed_option"><b>NAICS</b></td>
		       <td class="feed_option" align=right><b>Amount</b></td>
		       <td class="feed_option" align=right><b>% of Awards</b></td>
		    </tr>

            <cfset counter = 0>
            <cfset total = 0>

            <cfoutput query="subs">

            <cfif counter is 0>
             <tr bgcolor="ffffff" height=30>
            <cfelse>
             <tr bgcolor="e0e0e0" height=30>
            </cfif>

		       <td class="feed_option">#dateformat(subaward_action_date,'mm/dd/yyyy')#</td>
		       <td class="feed_option">#subaward_number#</td>
		       <td class="feed_option"><a href="/exchange/include/federal_profile.cfm?duns=#subawardee_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#subawardee_name#</b></a></td>
		       <td class="feed_option">#subawardee_duns#</td>
		       <td class="feed_option">#subaward_naics_code#</td>
		       <td class="feed_option" width=100 align=right>#numberformat(subaward_amount,'$999,999,999,999.99')#</td>
		       <td class="feed_option" width=100 align=right>#numberformat(evaluate((subaward_amount / totals.obligation)*100),99.99)#%</td>
		    </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>

            <cfset total = total + subaward_amount>

            </cfoutput>

            <tr><td class="feed_option"><b>Total Sub Contracts</b></td>
                <td class="feed_option" colspan=5 align=right><b><cfoutput>#numberformat(total,'$999,999,999,999.99')#</cfoutput></td>
                <td class="feed_option" align=right><b><cfoutput>#numberformat(evaluate((total / totals.obligation)*100),99.99)#%</cfoutput></td></tr>
          </table>

		</td></tr>

	</cfif>

    <tr><td colspan=4><hr></td></tr>

    <cfoutput query="award">

    <tr>

    <td class="feed_option" valign=top><b>
                            Solicitation Identifier<br>
                            Solicitation Procedures<br>
                            Single or Multiple Award<br>
                            Number of Offers Received<br>
                            Number of Actions<br>
                            Fed Biz Ops
                            </b></td>

    <td class="feed_option" valign=top>


						   <cfif #solicitation_identifier# is "">
						    Not provied.<br>

						   <cfelse>

						    <!--- Call API to see if it exists --->

							<cfhttp method="get" url="https://api.amaforge.com/fbo/archive?SOLNBR=#solicitation_identifier#" result="sol_data">
							  <cfhttpparam type="header" name="key" value="d7e4ee3b297344d6acb83c6d421eac08">
							</cfhttp>

							<cfset soliciation_data=DeserializeJSON(sol_data.filecontent)>

							<cfif isdefined("soliciation_data.data.naics")>

                            <cfif #usr.usr_exchange_admin# is 1>
							 #solicitation_identifier# - <a href="solicitation_history.cfm?soliciation=#solicitation_identifier#">( Show History )</a>
							<cfelse>
							 #solicitation_identifier#
							</cfif>

							<cfelse>
							 #solicitation_identifier#
							</cfif>

					       </cfif>
							#multiple_or_single_award_idv#<br>
							#number_of_offers_received#<br>
							#number_of_actions#<br>
							#fed_biz_opps#


                            </td>



    <td class="feed_option" valign=top><b>
                            Contract Bundling<br>
                            Multi Year Contract<br>
                            Consolidated Contract<br>
                            Major Program<br>
                            SAM Exception<br>
                            Foreign Funding

                            </b></td>

    <td class="feed_option" valign=top>
  							#contract_bundling#<br>
							#multi_year_contract#<br>
							#consolidated_contract#<br>
							#major_program#<br>
							#sam_exception#<br>
                            #foreign_funding#
    </td>
    </tr>


    <tr><td colspan=4><hr></td></tr>

















    <tr>

    <td class="feed_option" valign=top><b>
                            Primary Place of Performance<br>
                            City, State<br>
                            County<br>
                            Congressional District<br>
                            Location Code
                            </b></td>

    <td class="feed_option" valign=top><br>
                            #primary_place_of_performance_city_name#, #primary_place_of_performance_state_name#<br>
                            #left(primary_place_of_performance_zip_4,5)#-#right(primary_place_of_performance_zip_4,4)#<br>
                            #primary_place_of_performance_county_name#<br>
                            #primary_place_of_performance_congressional_district#<br>
                            </td>

    <td class="feed_option" valign=top><b>
                            NAICS Code<br>
                            Product or Service<br>
                            IT Commercial Category<br>
                            Place of Manufacture<br>
                            </b></td>

    <td class="feed_option" valign=top>#naics_code# - #naics_description#<br>
                                       #product_or_service_code# - #product_or_service_code_description#<br>
                                       #information_technology_commercial_item_category#<br>
                                       #place_of_manufacture#
                                       </td>
    </tr>


    <tr><td colspan=4><hr></td></tr>


    <tr>

    <td class="feed_option" valign=top><b>
		DOD Claimant Program Description<br>
		Recovered Materials Sustainability<br>
		Domestic or Foreign Entity<br>
		DOE Acquisition Program Description<br>
		EPA Designated Product<br>
		Country of Product or Service Origin<br>
		Subcontracting Plan<br>
		Evaluated Preference<br>
		Research<br>
		Fair Opportunity Limited Sources<br>
		Other Than Full and Open Competition<br>
		Commercial Item Acquisition Procedures<br>
		SB Competiveness Demonstration Program<br>
		Commercial Item Test Program<br>
		Local Area Set Aside
       </b></td>

    <td class="feed_option" valign=top>


#dod_claimant_program_description#<br>
#recovered_materials_sustainability#<br>
#domestic_or_foreign_entity#<br>
#dod_acquisition_program_description#<br>
#epa_designated_product#<br>
#country_of_product_or_service_origin#<br>
#subcontracting_plan#<br>
#evaluated_preference#<br>
#research#<br>
#fair_opportunity_limited_sources#<br>
#other_than_full_and_open_competition#<br>
#commercial_item_acquisition_procedures#<br>
<cfif #small_business_competitiveness_demonstration_program# is "f">NO<cfelseif #small_business_competitiveness_demonstration_program# is "t">YES</cfif><br>
#commercial_item_test_program#<br>
#local_area_set_aside#

                            </td>

    <td class="feed_option" valign=top><b>

				Price Evaludation Adjustment<br>
				Materials Supplies Articles Equipment<br>
				Labor Standards<br>
				Construction Wage Rate Requirements<br>
				Interagency Contracting Authority<br>
				Other Statutory Authority<br>
				Program Acronym<br>
				National Interest Action<br>
				Cost or Pricing Data<br>
				Cost Accounting Standards Clause<br>
				GFT GFP<br>
				Sea Transportation<br>
				Undefinitized Action<br>
				Performance Based Service Acquisition<br>
				Contract Financing

                            </b></td>

    <td class="feed_option" valign=top>

			#price_evaluation_adjustment_preference_percent_difference#<br>
			#materials_supplies_articles_equipment#<br>
			#labor_standards#<br>
			#construction_wage_rate_requirements#<br>
			#interagency_contracting_authority#<br>
			#other_statutory_authority#<br>
			#program_acronym#<br>
			#national_interest_action#<br>
			#cost_or_pricing_data#<br>
			#cost_accounting_standards_clause#<br>
			#gfe_gfp#<br>
			#sea_transportation#<br>
			#undefinitized_action#<br>
			#performance_based_service_acquisition#<br>
			#contract_financing#

                            </td>
    </tr>

</table>



</cfoutput>


	  </div>

	  </td></tr>

  </table>

 </div>

  </td></tr>
 </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

