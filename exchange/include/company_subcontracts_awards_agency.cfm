<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

	<cfquery name="name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select top(1) prime_awarding_sub_agency_code, prime_awarding_sub_agency_name from award_data_sub
	 where prime_awarding_sub_agency_code = '#prime_awarding_sub_agency_code#'
    </cfquery>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select distinct(subaward_number) as subaward_number, id, prime_awardee_name, prime_awardee_duns, subawardee_name, subaward_description, subaward_naics_code, subawardee_business_type_description, subaward_action_date, prime_award_parent_piid, prime_award_piid, prime_awarding_sub_agency_name, prime_awarding_office_name, subaward_amount from award_data_sub
     where (subawardee_duns = '#session.company_profile_duns#' or subawardee_parent_duns = '#session.company_profile_duns#') and
	       prime_awarding_sub_agency_code = '#prime_awarding_sub_agency_code#'

	   <cfif isdefined("sv")>

		<cfif #sv# is 1>
		 order by subaward_action_date DESC
		<cfelseif #sv# is 10>
		 order by subaward_action_date ASC
		<cfelseif #sv# is 2>
		 order by prime_award_parent_piid ASC
		<cfelseif #sv# is 20>
		 order by prime_award_parent_piid DESC
		<cfelseif #sv# is 3>
		 order by prime_award_piid ASC
		<cfelseif #sv# is 30>
		 order by prime_award_piid DESC
		<cfelseif #sv# is 4>
		 order by subaward_number ASC
		<cfelseif #sv# is 40>
		 order by subaward_number DESC
		<cfelseif #sv# is 5>
		 order by prime_awarding_sub_agency_name ASC
		<cfelseif #sv# is 50>
		 order by prime_awarding_sub_agency_name DESC
		<cfelseif #sv# is 6>
		 order by prime_awarding_office_name ASC
		<cfelseif #sv# is 60>
		 order by prime_awarding_office_name DESC
		<cfelseif #sv# is 7>
		 order by prime_awardee_name ASC
		<cfelseif #sv# is 70>
		 order by prime_awardee_name DESC
		<cfelseif #sv# is 8>
		 order by subaward_amount ASC
		<cfelseif #sv# is 80>
		 order by subaward_amount DESC
		<cfelseif #sv# is 9>
		 order by subaward_naics_code ASC
		<cfelseif #sv# is 90>
		 order by subaward_naics_code DESC
		<cfelseif #sv# is 9>
		 order by prime_awardee_name ASC
		<cfelseif #sv# is 90>
		 order by prime_awardee_name DESC
		<cfelseif #sv# is 10>
		 order by subawardee_business_type_description ASC
		<cfelseif #sv# is 110>
		 order by subawardee_business_type_description DESC
		<cfelseif #sv# is 11>
		 order by subawardee_business_type_description ASC
		<cfelseif #sv# is 110>
		 order by subawardee_business_type_description DESC

	   <cfelse>
		 order by subaward_action_date DESC
	   </cfif>
	  </cfif>
	</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

 <div class="main_box">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=top>SUBCONTRACT AWARDS</td>
             <td class="feed_header" align=right class="feed_sub_header" valign=top>
             <cfoutput>
             <img src="/images/delete.png" align=absmiddle border=0 width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();">
             </cfoutput></td></tr>
         <tr><td colspan=2><hr></td></tr>

      <cfoutput>

       <tr><td height=10></td></tr>
       <tr><td class="feed_sub_header"><u>#ucase(name.prime_awarding_sub_agency_name)#</u></a></td>
           <td align=right><a href="/exchange/include/company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&sv=#sv#&export=1"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" title="Export to Excel"></a></td></tr>
       <tr><td height=10></td></tr>

      </cfoutput>

      </table>

      </td></tr>

   </table>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	<cfif agencies.recordcount is 0>

	 <tr><td class="feed_sub_header" style="font-weight: normal;">No subcontractors were found.</td></tr>

	<cfelse>

	<cfoutput>

				<tr height=50>
				   <td class="text_xsmall" width=75><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATE</b></a></td>
				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PRIME CONTRACTOR</b></a></td>
				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>PARENT CONTRACT ##</b></a></td>
				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PRIME CONTRACT ##</b></a></td>
				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>SUBCONTRACT ##</b></a></td>

				   <td class="text_xsmall"><b>SUBCONTRACTOR</b></a></td>


				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>OFFICE</b></a></td>
				   <td class="text_xsmall"><b>DESCRIPTION</b></td>
				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>NAICS</b></a></td>
				   <td class="text_xsmall"><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>BUSINESS TYPE</b></a></td>
			       <td class="text_xsmall" align=right><a href="company_subcontracts_awards_agency.cfm?prime_awarding_sub_agency_code=#prime_awarding_sub_agency_code#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>OBLIGATED</b></a></td>
				</tr>

	</cfoutput>

				<cfset #counter# = 0>
				<cfset #sub_total# = 0>

				<cfoutput query="agencies">

				 <cfif counter is 0>
				  <tr bgcolor="ffffff" height=30>
				 <cfelse>
				  <tr bgcolor="f0f0f0" height=30>
				 </cfif>

					 <td class="text_xsmall" valign=top style="padding-right: 20px;">#dateformat(subaward_action_date,'mm/dd/yyyy')#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=150>#prime_awardee_name#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=150>#prime_award_parent_piid#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=100>#prime_award_piid#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=100><a href="/exchange/include/sub_award_information.cfm?id=#id#" target="_blank" rel="noopener"><b>#subaward_number#</b></a></td>

					 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=150>#subawardee_name#</td>

					 <td class="text_xsmall" valign=top style="padding-right: 20px;" width=150>#prime_awarding_office_name#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;">#subaward_description#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;">#subaward_naics_code#</td>
					 <td class="text_xsmall" valign=top style="padding-right: 20px;">#subawardee_business_type_description#</td>
					 <td class="text_xsmall" valign=top align=right>#numberformat(subaward_amount,'$999,999,999,999')#</td>

				 </tr>


				 <cfif counter is 0>
				  <cfset counter = 1>
				 <cfelse>
				  <cfset counter = 0>
				 </cfif>
				 <cfset #sub_total# = #sub_total# + #subaward_amount#>

				</cfoutput>

				<tr><td height=5></td></tr>
				<tr><td class="feed_option"><b>Total</b></td>
					<td class="feed_option" align=right colspan=9><b><cfoutput>#numberformat(sub_total,'$999,999,999,999')#</cfoutput></b></td></tr>

   </cfif>

   </table>

      </div>


<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>