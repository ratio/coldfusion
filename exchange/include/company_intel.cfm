<cfquery name="intel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company_intel
 left join comments_rating on comments_rating_id = company_intel_rating
 left join usr on usr_id = company_intel.company_intel_created_by_usr_id
 where company_intel_company_id = #session.company_profile_id# and
       company_intel_hub_id = #session.hub# and
       (company_intel_created_by_usr_id = #session.usr_id# or (company_intel_sharing = 1 and company_intel_created_by_company_id = #session.company_id#))
 order by company_intel_created_date DESC
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td class="feed_header" style="font-size: 30;" valign=middle>Company Comments<cfif #intel.recordcount# GT 0> (<cfoutput>#intel.recordcount#</cfoutput>)</cfif></td>
   <td align=right class="feed_sub_header"><a href="profile.cfm?l=31"><img src="/images/plus3.png" width=15 border=0 alt="Add Comments" title="Add Comments" valign=middle></a>&nbsp;&nbsp;<a href="profile.cfm?l=31">Add Comments</a></td></tr>
<tr><td colspan=2><hr></td></tr>

<cfif isdefined("cu")>
 <cfif cu is 1>
 	<tr><td colspan=2 class="feed_sub_header" style="color: green;">Company comment has been successfully added.</td></tr>
 <cfelseif cu is 2>
 	<tr><td colspan=2 class="feed_sub_header" style="color: green;">Company comment has been successfully updated.</td></tr>
 <cfelseif cu is 3>
 	<tr><td colspan=2 class="feed_sub_header" style="color: green;">Company comment has been successfully deleted.</td></tr>
 </cfif>
 <tr><td height=10></td></tr>
</cfif>

</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfif intel.recordcount is 0>
<tr><td class="feed_sub_header">No Company comments have been created.</td></tr>
<cfelse>
<tr><td height=10></td></tr>
<cfset count = 1>

<cfloop query="intel">

<cfoutput>

<tr><td valign=top width=110>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <cfif #intel.usr_photo# is "">
	  <tr><td align=center><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(intel.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/headshot.png" width=100 border=0 alt="#intel.usr_first_name#'s Profile" title="#intel.usr_first_name#'s Profile"></a></td></tr>
	 <cfelse>
	  <tr><td align=center><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(intel.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img style="border-radius: 0px;" src="#media_virtual#/#intel.usr_photo#" width=100 border=0 title="#intel.usr_first_name#'s Profile" alt="#intel.usr_first_name#'s Profile"></a></td></tr>
	 </cfif>
	 <tr><td  align=center class="feed_option"><a href="/exchange/marketplace/people/profile.cfm?i=#encrypt(intel.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><b>#usr_first_name# #usr_last_name#</b></a></td></tr>

</table>

</td><td width=30>&nbsp;</td><td valign=top>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

   <tr>
	   <td class="feed_sub_header" style="padding-bottom: 0px; margin-bottom: 0px;">

	   <cfif session.usr_id is intel.company_intel_created_by_usr_id>
		   <a href="profile.cfm?l=32&company_intel_id=#intel.company_intel_id#">#intel.company_intel_context#</a>
	   <cfelse>
		   #intel.company_intel_context#
	   </cfif>

	   </td>
	   <td class="feed_sub_header" align=right style="padding-top: 5px;"><img src="#media_virtual#/#comments_rating_image#" height=16> </td></tr>
   <tr>
       <td colspan=2 class="feed_sub_header" style="font-weight: normal;">#intel.company_intel_comments#</td></tr>
   </tr>

   <tr><td class="link_small_gray">
       <cfif #intel.company_intel_attachment# is not "">
        <a href="#media_virtual#/#intel.company_intel_attachment#" target="_blank" rel="noopener"><u>Download Attachment</u></a>&nbsp;|&nbsp;
       </cfif>

       <cfif #intel.company_intel_url# is not "">
        Reference URL: <a href="#intel.company_intel_url#" target="_blank" rel="noopener"><u>#intel.company_intel_url#</u></a>
       </cfif>


       </td>
       <td align=right class="link_small_gray">#dateformat(intel.company_intel_created_date,'mmm dd, yyyy')# at #timeformat(intel.company_intel_created_date)#</td>
   </tr>

 </cfoutput>

  <cfset count = count + 1>

</table>

</td></tr>

<tr><td height=10></td></tr>
<tr><td colspan=3><hr></td></tr>
<tr><td height=10></td></tr>

</cfloop>

</cfif>

</table>



