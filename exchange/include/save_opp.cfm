<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif t is "Contract">

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select fbo_opp_name as name from fbo
	 where fbo_id = #id#
	</cfquery>

<cfelseif t is "Grant">

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select opportunitytitle as name from opp_grant
	 where opp_grant_id = #id#
	</cfquery>

<cfelseif t is "SBIR">

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select objective as name from opp_sbir
	 where id = #id#
	</cfquery>

<cfelseif t is "Challenge">

	<cfquery name="opp_name" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select challenge_name as name from challenge
	 where challenge_id = #id#
	</cfquery>

<cfelseif t is "Award">

	<cfquery name="opp_name" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select top(1) award_description as name from award_data
	 where award_id_piid = '#id#'
	</cfquery>

<cfelseif t is "Need">

	<cfquery name="opp_name" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select need_name as name from need
	 where need_id = #id#
	</cfquery>

</cfif>

<cfquery name="pipe_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_pipeline_id from sharing
 where sharing_hub_id = #session.hub# and
	   sharing_pipeline_id is not null and
	   (sharing_to_usr_id = #session.usr_id#)
</cfquery>

<cfif pipe_access.recordcount is 0>
 <cfset pipe_list = 0>
<cfelse>
 <cfset pipe_list = valuelist(pipe_access.sharing_pipeline_id)>
</cfif>

<cfquery name="pipelines" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select pipeline_name, pipeline_image, pipeline_id, usr_first_name, usr_last_name, pipeline_updated from pipeline
  left join usr on usr_id = pipeline_usr_id
  where pipeline_hub_id = #session.hub# and
		(pipeline_id in (#pipe_list#) or pipeline_usr_id = #session.usr_id#)
  order by pipeline_updated DESC
</cfquery>

<cfquery name="comms" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from comm
  join comm_xref on comm_xref_comm_id = comm_id
  where comm_hub_id = #session.hub# and
        comm_xref_usr_id = #session.usr_id# and
        comm_xref_usr_role = 1
  order by comm_name
</cfquery>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">Pin to Opportunity Board</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

 <form action="/exchange/include/save_opp_db.cfm" method="post">

 <cfif pipelines.recordcount is 0>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created or have access to any Opportunity Boards.  To add this Opportunity to a Board, please create one now.</td></tr>
     <tr><td class="feed_sub_header">Create Opportunity Board</td></tr>
     <tr><td><input type="text" name="pipeline_name" size=50 class="input_text" required placeholder="Please provide a name for this Opportunity Board."></td></tr>
     <tr><td height=10></td></tr>
     <input type="hidden" name="option" value=2>
     <tr><td><hr></td></tr>

 </table>

 <cfelse>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td width=30><input type="radio" name="option" style="height: 20px; width: 20px;" value=1 checked></td>
	     <td class="feed_sub_header">Select existing Opportunity Board</td></tr>
     <tr><td></td><td>

			 <select name="deal_pipeline_id" class="input_select" style="width: 400px;">
			 <cfoutput query="pipelines">
			  <option value=#encrypt(pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#pipeline_name# (#usr_last_name#, #usr_first_name#)
			 </cfoutput>
			 <select>

			 </td></tr>

	 <tr><td height=5></td></tr>
	 <tr><td width=40><input type="radio" name="option" style="height: 20px; width: 20px;" value=2></td>
         <td class="feed_sub_header">Or, create a new Opportunity Board</td></tr>

     <tr><td></td><td><input type="text" name="pipeline_name" style="width: 400px;" class="input_text" placeholder="Please provide a name for this Opportunity Board."></td>
     <tr><td height=10></td></tr>

     <cfif comms.recordcount GT 0>

	 <tr><td width=30><input type="radio" name="option" style="height: 20px; width: 20px;" value=3></td>
	     <td class="feed_sub_header">Or, post to Community Board</td></tr>
     <tr><td></td><td>

			 <select name="deal_comm_id" class="input_select" style="width: 400px;">
			 <cfoutput query="comms">
			  <option value=#encrypt(comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#>#comm_name#
			 </cfoutput>
			 <select>

			 </td></tr>

	 </cfif>

     <tr><td height=10></td></tr>

     <tr><td colspan=2><hr></td></tr>

   </table>

</cfif>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput>
     <tr><td class="feed_sub_header" colspan=2>

     <cfif t is "Contract">
      Contract
     <cfelseif t is "Grant">
      Grant
     <cfelseif t is "SBIR">
      SBIR/STTR
     <cfelseif t is "Challenge">
      Challenge
     <cfelseif t is "Award">
      Award
     <cfelseif t is "Need">
      Need
     </cfif>

     Name</td>
	 <tr><td colspan=2><input type="text" name="deal_name" style="width: 600px;" class="input_text" value="#opp_name.name#" required placeholder="Please provide a name for this Opportunity."></td></tr>
     </cfoutput>

     <tr><td colspan=2 class="feed_sub_header">Description</td>
	 <tr><td colspan=2><textarea name="deal_desc" class="input_textarea" style="width: 600px; height: 100px;" placeholder="Please add a description for this Opportunity."></textarea></td></tr>

     <tr>
        <td width=40><input type="checkbox" name="send_notification" style="width: 20px; height: 20px;"></td>
        <td class="feed_sub_header" style="font-weight: normal;">Send notification to Board Owner?</td>
     </tr>

     <tr><td colspan=2><hr></td></tr>
	 <tr><td height=10></td></tr>
	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Pin Opportunity">

     <cfoutput>
     <input type="hidden" name="id" value=#id#>
     <input type="hidden" name="t" value="#t#">
     </cfoutput>

</form>

</table>