<cfif t is "Contract">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from fbo
  where fbo_id = #id#
 </cfquery>

<cfelseif t is "Grant">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from opp_grant
  where opp_grant_id = #id#
 </cfquery>

<cfelseif t is "SBIR">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from opp_sbir
  where id = #id#
 </cfquery>

<cfelseif t is "Challenge">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from challenge
  where challenge_id = #id#
 </cfquery>

<cfelseif t is "Award">

 <cfquery name="opp_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) * from award_data
  where id = '#id#'
 </cfquery>

</cfif>

<cfif t is "Contract">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_comm_id = #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       deal_hub_id = #session.hub# and
	       deal_contract_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_desc,
	  deal_contract_id,
	  deal_comm_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type,
	  deal_url,
	  deal_current_sol,
	  deal_pop_city,
	  deal_pop_state,
	  deal_psc,
	  deal_naics

	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_desc#',
	  #id#,
	  #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.fbo_office#',
	 'Contract',
	 '#opp_detail.fbo_url#',
	 '#opp_detail.fbo_solicitation_number#',
	 '#opp_detail.fbo_pop_city#',
	 '#opp_detail.fbo_pop_state#',
	 '#opp_detail.fbo_class_code#',
	 '#opp_detail.fbo_naics_code#'
	  )
	</cfquery>

	</cfif>

<cfelseif t is "Grant">

    <!--- Grants --->

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_comm_id = #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       deal_hub_id = #session.hub# and
	       deal_grant_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_desc,
	  deal_grant_id,
	  deal_comm_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_desc#',
	  #id#,
	  #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.agencyname#',
	 'Grant'
	  )
	</cfquery>

	</cfif>

    <!--- SBIR --->

<cfelseif t is "SBIR">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_comm_id = #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       deal_hub_id = #session.hub# and
	       deal_sbir_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_desc,
	  deal_sbir_id,
	  deal_comm_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_desc#',
	  #id#,
	  #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.agency#',
	 'SBIR/STTR'
	  )
	</cfquery>

	</cfif>

<cfelseif t is "Challenge">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_comm_id = #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       deal_hub_id = #session.hub# and
	       deal_challenge_id = #id#
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_desc,
	  deal_challenge_id,
	  deal_comm_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 '#deal_desc#',
	  #id#,
	  #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.challenge_organization#',
	 'Challenge'
	  )
	</cfquery>

	</cfif>

<cfelseif t is "Award">

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select deal_id from deal
	 where deal_comm_id = #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# and
	       deal_hub_id = #session.hub# and
	       deal_award_id = '#id#'
    </cfquery>

	<cfif check.recordcount is 0>

	<cfquery name="getval" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select max(potential_total_value_of_award) as potential from award_data
	 where id = '#id#'
	</cfquery>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into deal
	 (
	  deal_name,
	  deal_value_total,
	  deal_pop_city,
	  deal_pop_state,
	  deal_past_sol,
	  deal_contract_number,
	  deal_psc,
	  deal_naics,
	  deal_url,
	  deal_desc,
	  deal_award_id,
	  deal_comm_id,
	  deal_hub_id,
	  deal_owner_id,
	  deal_created,
	  deal_updated,
	  deal_customer_name,
	  deal_type
	 )
	 values
	 (
	 '#deal_name#',
	 <cfif getval.potential is "">null<cfelse>#getval.potential#</cfif>,
	 '#opp_detail.primary_place_of_performance_city_name#',
	 '#opp_detail.primary_place_of_performance_state_code#',
	 '#opp_detail.solicitation_identifier#',
	 '#opp_detail.award_id_piid#',
	 '#opp_detail.product_or_service_code#',
	 '#opp_detail.naics_code#',
	 '#session.site_url#/exchange/include/award_information.cfm?id=#id#',
	 '#deal_desc#',
	  '#id#',
	  #decrypt(deal_comm_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
	  #session.hub#,
	  #session.usr_id#,
	  #now()#,
	  #now()#,
	 '#opp_detail.awarding_office_name#',
	 'Award'
	  )
	</cfquery>

	</cfif>

</cfif>

