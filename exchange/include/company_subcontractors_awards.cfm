<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Federal Subcontractors</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

	<cfquery name="sub_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select count(id) as total from award_data_sub
	 where prime_awardee_duns = '#session.company_profile_duns#'
	</cfquery>

	<cfquery name="sub" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select top(200) subaward_number, subaward_description, id, prime_awardee_duns, subawardee_name, prime_awarding_agency_name, subawardee_duns, subaward_action_date, prime_award_parent_piid, prime_award_piid, prime_awarding_sub_agency_name, prime_awarding_office_name, subaward_amount from award_data_sub
	 where prime_awardee_duns = '#session.company_profile_duns#'
	 order by subaward_action_date DESC
	</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif sub.recordcount is 0>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">No subcontractors were found.</td></tr>
		<cfelse>

	    <cfoutput>
	    <tr><td class="feed_option"><b>Last #sub.recordcount# Awards</b></td>
		    <td class="feed_option" align=right colspan=2><a href="extended_subcontrator.cfm"><b>See All #numberformat(sub_total.total,'999,999')# Awards</b></a></td></tr>
	    </cfoutput>

        </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr><td>

					<tr height=50>
					   <td class="text_xsmall" width=75><b>AWARD DATE</b></td>
					   <td class="text_xsmall" width=100><b>SUBCONTRACTOR</b></td>
					   <td class="text_xsmall" width=100><b>SUBCONTRACT #</b></td>
					   <td class="text_xsmall"><b>PRIME CONTRACT #</b></td>
					   <td class="text_xsmall"><b>DEPARTMENT</b></td>
					   <td class="text_xsmall"><b>AGENCY</b></td>
					   <td class="text_xsmall"><b>DESCRIPTION</b></td>
					   <td class="text_xsmall" align=right width=100><b>OBLIGATION</b></td>
					</tr>


					<tr><td height=5></td></tr>

					<cfset #counter# = 0>
					<cfset #sc_total# = 0>

					<cfoutput query="sub">

					 <cfif counter is 0>
					  <tr bgcolor="ffffff" height=30>
					 <cfelse>
					  <tr bgcolor="f0f0f0" height=30>
					 </cfif>

						 <td class="text_xsmall" valign=top>#dateformat(subaward_action_date,'mm/dd/yyyy')#</td>
						 <td class="text_xsmall" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#subawardee_duns#" target="_blank" rel="noopener"><b>#subawardee_name#</b></a></td>
						 <td class="text_xsmall" valign=top><a href="/exchange/include/sub_award_information.cfm?id=#sub.id#" target="_blank" rel="noopener"><b>#subaward_number#</b></a></td>
						 <td class="text_xsmall" valign=top><a href="/exchange/include/award_information.cfm?prime_award_id=#prime_award_piid#&duns=#prime_awardee_duns#" target="_blank" rel="noopener"><b>#prime_award_piid#</b></a></td>
						 <td class="text_xsmall" valign=top>#prime_awarding_agency_name#</td>
						 <td class="text_xsmall" valign=top>#prime_awarding_sub_agency_name#</td>
						 <td class="text_xsmall" valign=top width=600>#subaward_description#</td>
						 <td class="text_xsmall" valign=top align=right>#numberformat(subaward_amount,'$999,999,999,999')#</td>

					 </tr>


					 <cfif counter is 0>
					  <cfset counter = 1>
					 <cfelse>
					  <cfset counter = 0>
					 </cfif>
					 <cfset #sc_total# = #sc_total# + #subaward_amount#>

					</cfoutput>

					<tr><td height=5></td></tr>
					<tr><td class="feed_option"><b>Total</b></td>
						<td class="feed_option" align=right colspan=7><b><cfoutput>#numberformat(sc_total,'$999,999,999,999')#</cfoutput></b></td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td class="text_xsmall" colspan=7><b>Note -</b> this may not include information from subcontractors where the Prime was not legally obligated to report them.</td></tr>

	   </cfif>

	   </table>
