<cfinclude template="/exchange/security/check.cfm">

<cfquery name="award" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_data_sub
 where id = #id#
</cfquery>

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_subaward_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<body class="body" bgcolor="afafaf">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">SUBCONTRACT AWARD INFORMATION</td>
             <td class="feed_header" align=right>
             <img src="/images/delete.png" width=20 border=0 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td>
             </tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

    <cfoutput query="award">

    <table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr>

    <td class="feed_option" width=300 valign=top><b>
                            Recipient Name / Subcontractor<br>
                            DUNS<br>
                            Contact Information
                            </b></td>

    <td class="feed_option" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#subawardee_duns#" target="_blank" rel="noopener" rel="noreferrer"><u>#subawardee_name#</u></a><br>
                            #subawardee_duns#<br>
                            #subawardee_address_line_1#<br>
                            #subawardee_city_name# #subawardee_state_code#. #subawardee_zip_4# #subawardee_country_code#
                            </td>

    <td class="feed_option" valign=top><b>Prime Contractor<br>
                                          DUNS<br>
                                          Office / Location</b></td>

    <td class="feed_option" valign=top><a href="/exchange/include/federal_profile.cfm?duns=#prime_awardee_duns#" target="_blank" rel="noopener" rel="noreferrer"><u>#prime_awardee_name#</u></a><br>
                                       #prime_awardee_duns#<br>
                                       #prime_award_principal_place_city#, #prime_award_principal_place_state#. #prime_award_principal_place_zip# #prime_award_principal_place_country#
                            </td>
    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>

    <td class="feed_option" width=200 valign=top><b>
                            Department<br>
                            Agency<br>
                            Office
                            </b></td>

    <td class="feed_option" valign=top>#prime_awarding_agency_name#<br>
                            #prime_awarding_sub_agency_name#<br>
                            #prime_awarding_office_name#
                            </td>

    <td class="feed_option" valign=top><b>

                            </b></td>

    <td class="feed_option" valign=top>
                            </td>
    </tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>
    <td class="feed_option" valign=top><b>
                            Prime Parent Award Contract ##<br>
                            Prime Contract Number<br>
                            Subcontract Number<br>
                            Award Amount<br>
                            Reporting Year<br>
                            Reporting Month
                            </b></td>

    <td class="feed_option" valign=top>
                            #prime_award_parent_piid#<br>
                            <a href="/exchange/include/award_information.cfm?duns=#prime_awardee_duns#&prime_award_id=#prime_award_piid#" target="_blank" rel="noopener" rel="noreferrer">#prime_award_piid#</a><br>
                            #subaward_number#<br>
                            #numberformat(subaward_amount,'$999,999,999.99')#<br>
                            #subaward_report_year#<br>
                            #subaward_report_month#
                            </td>

    <td class="feed_option" valign=top></td>

    <td class="feed_option" valign=top></td></tr>

    <tr><td colspan=4><hr></td></tr>

    <tr>
    <td class="feed_option" valign=top><b>
                            Primary Place of Performance<br>
                            Congressional District<br>
                            </b></td>

    <td class="feed_option" valign=top>
                            #subaward_primary_place_of_performance_city_name#, #subaward_primary_place_of_performance_state_code#.  #subaward_primary_place_of_performance_address_zip_4#  #subaward_primary_place_of_performance_country_code#<br>
                            #subaward_primary_place_of_performance_congressional_district#
                            </td>

    <td class="feed_option" valign=top></td>

    <td class="feed_option" valign=top></td></tr>


    <tr><td colspan=4><hr></td></tr>

    <tr><td class="feed_option" valign=top><b>Award Description</b></td>
        <td class="feed_option" colspan=3>#subaward_description#</td></tr>

    <tr><td class="feed_option" valign=top><b>NAICS</b></td>
        <td class="feed_option" colspan=3>#subaward_naics_code# - #subaward_naics_description#</td></tr>


    </cfoutput>


    </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

