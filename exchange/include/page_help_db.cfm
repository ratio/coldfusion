<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update_page_help" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 update page
 set page_name = '#page_name#',
     page_help = '#page_help#',
     page_help_updated = #now()#
 where page_url = '#page_url#'
</cfquery>

<cflocation URL="page_help.cfm?page=#page_url#&u=1" addtoken="no">