<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 left join size on size_id = company_size_id
 left join broker on broker_company_id = company_id
 left join entity on entity_id = company_entity_id
 left join cb_organizations on cb_organizations.uuid = company_cb_id
 where company_id = #session.company_profile_id#
</cfquery>

<style>
.people_scroll {
    height: 150px;
    background-color: ffffff;
    overflow:auto;
}
.people_block {
    width: 200;
    border: 1px solid #e0e0e0;
    display: inline-block;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    height: 320px;
    padding-top: 10px;
    padding-bottom: 30px;
    padding-left: 18px;
    padding-right: 18px;
    margin-left: 10px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 30px;
    border-radius: 3px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
}
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 280px;
  background-color: #ffffff;
  color: #000000;
  text-align: left;
  font-weight: normal;
  padding: 10px;
  position: absolute;
  font-size: 16;
  z-index: 1;
  top: 150%;
  left: 50%;
  margin-left: -140px;
  border-radius: 3px;
  border-color: #b0b0b0;
  border-width: thin;
  border-style: solid;
  border-radius: 2px;
  box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  bottom: 100%;
  left: 50%;
  margin-left: 0px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent black transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

<cfquery name="people" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cb_people
 left join cb_people_descriptions on cb_people_descriptions.uuid = cb_people.uuid
 where cb_people.featured_job_organization_uuid = '#comp.company_cb_id#'
 order by cb_people.last_name
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=2 class="feed_header" style="font-size: 30;">Company People</td>
       <td align=right class="feed_sub_header">
       </td></tr>

   <tr><td colspan=3><hr></td></tr>
   <tr><td height=20></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td valign=top>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <!--- Check Crunchbase Data --->

				<cfif people.recordcount is 0>

				<tr><td class="feed_sub_header" style="font-weight: normal;">No People were found that are/were associated with this Company.</td></tr>

				<cfelse>

                <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>The following people were found that have, or have had an association with this Company.  This content is sourced externally and may not be up-to-date.</td></tr>

                <tr><td height=20></td></tr>
                <tr><td></td><td>

					<cfoutput query="people">

					<cfset comp_team = 1>

						<div class="people_block">

                          <table cellspacing=0 cellpadding=0 border=0 width=100%>
                           <tr><td height=10></td></tr>
						   <tr><td align=center>

						   <a href="profile.cfm?i=#people.uuid#&l=46">
						   	<img src="#people.logo_url#" width=150 border=0 style="border-radius: 180px;" >
						   </a>

						   </td></tr>
						   <tr><td class="feed_sub_header" align=center>

						   <cfif #description# is "">
						   #people.first_name# #people.last_name#
						   <cfelse>
						   <div class="tooltip">#people.first_name# #people.last_name#<span class="tooltiptext">#description#</span></div>
						   </cfif>

						   </td></tr>
						   <tr><td class="feed_option" align=center height=65 valign=top>#people.featured_job_title#</td></tr>
						   <tr><td align=center>

						    <cfif #people.linkedin_url# is not "">
						     <a href="#people.linkedin_url#" target="_blank" rel="noopener"><img src="/images/icon_linkedin.png" width=20 hspace=5></a>
						    </cfif>

						    <cfif #people.facebook_url# is not "">
						     <a href="#people.facebook_url#" target="_blank" rel="noopener"><img src="/images/icon_facebook.png" width=20 hspace=5></a>
						    </cfif>

						    <cfif #people.twitter_url# is not "">
						     <a href="#people.twitter_url#" target="_blank" rel="noopener"><img src="/images/icon_twitter.png" width=20 hspace=5></a>
						    </cfif>

						   </td></tr>
						  </table>
						</div>

					</cfoutput>

                </td></tr>

            </table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=10></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td class="feed_sub_header" colspan=2><b>Content Partners & Attribution</b></td></tr>
  <tr><td height=10></td></tr>
  <tr><td width=100>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td width=150><a href="http://www.crunchbase.com" target="_blank" rel="noopener"><img src="/images/cb_logo.png" width=75 alt="Crunchbase" title="Crunchbase"></a></td></tr>
      </table>

      </td><td>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_sub_header"><a href="http://www.crunchbase.com" target="_blank" rel="noopener"><b>Crunchbase</b></a></td><td align=right class="feed_sub_header"><a href="http://www.crunchbase.com" target="_blank" rel="noopener"><u>http://www.crunchbase.com</u></a></td></tr>
       <tr><td class="feed_option" style="font-weight: normal;" colspan=2>The Exchange uses Crunchbase information to strengthen and complement a company's profile with information related to funding, investments, founders and staff, biography's of people, and other information related to a company's background and corporate structure.</td></tr>
      </table>

      </td></tr>

</table>

</cfif>

</td></tr>

</table>

</td></tr>

</table>
