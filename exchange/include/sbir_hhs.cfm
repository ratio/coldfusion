<cfinclude template="/exchange/security/check.cfm">

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_opp_sbir_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_sbir
 where id = #id#
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td height=10></td></tr>
         <tr><td class="feed_header">SBIR/STTR INFORMATION</td>
             <td align=right>
             <cfoutput>

			<div class="dropdown" style="cursor: pointer;">
			  <img src="/images/3dots2.png" style="cursor: pointer; padding-left: 10px;" height=10>
			  <div class="dropdown-content" style="top: 5; width: 250px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px; padding-top: 0px; margin-top: 2px;">
				<a href="##" onclick="window.open('/exchange/include/save_opp.cfm?id=#sbir.id#&t=sbir','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"><i class="fa fa-thumb-tack" aria-hidden="true" style="padding-right: 10px;"></i>Pin to Opportunity Board</a>
				<a href="##" onclick="windowClose();" alt="Close" title="Close" style="cursor: pointer;"><i class="fa fa-times" aria-hidden="true" style="padding-right: 10px;"></i>Close</a>
			  </div>
			</div>

             </cfoutput></td></tr>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header" width=250>DEPARTMENT</td>
			     <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.department is "">Not Specified<cfelse>#ucase(sbir.department)#</cfif></td></tr>
			 <tr><td class="feed_sub_header" width=250>AGENCY / OFFICE</td>
			     <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.agency is "">Not Specified<cfelse>#ucase(sbir.agency)#</cfif></td></tr>
			 <tr><td class="feed_sub_header">PROGRAM / PHASE / YEAR</td>
			     <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.programphaseyear is "">Not Specified<cfelse>#sbir.programphaseyear#</cfif></td></tr>
			 <tr><td class="feed_sub_header">SOLICIATION NUMBER</td>
			     <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.solicitationnumber is "">Not Specified<cfelse>#sbir.solicitationnumber#</cfif></td></tr>
         </table>

         </td><td width=50>&nbsp;</td><td valign=top width=35%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <tr><td class="feed_sub_header" style="padding-right: 20px;">RELEASE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.releasedate is "" or sbir.releasedate is "01/01/1900">Not Specified<cfelse>#dateformat(sbir.releasedate,'mm/dd/yyyy')#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-right: 20px;">OPEN DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.opendate is "" or sbir.opendate is "01/01/1900">Not Specified<cfelse>#dateformat(sbir.opendate,'mm/dd/yyyy')#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-right: 20px;" width=250 valign=top>APPLICATION DUE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.applicationduedate is "" or sbir.applicationduedate is "01/01/1900">Not Specified<cfelse>#sbir.applicationduedate#</cfif></td></tr>
				 <tr><td class="feed_sub_header" style="padding-right: 20px;">CLOSE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.closedate is "" or sbir.closedate is "01/01/1900">Not Specified<cfelse>#dateformat(sbir.closedate,'mm/dd/yyyy')#</cfif></td></tr>
			 </table>

         </td></tr>

         <tr><td colspan=3><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_sub_header">TOPIC / OBJECTIVE</td></tr>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">#sbir.objective#</td></tr>
		 <tr><td height=10></td></tr>
         <tr><td class="feed_sub_header">DESCRIPTION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;"><cfif sbir.description is "">Not Specified<cfelse>#sbir.description#</cfif></td></tr>
         <tr><td><hr></td></tr>

         <tr><td class="feed_sub_header">REFERENCING URL</td></tr>
         <tr><td class="feed_sub_header"><a href="#sbir.url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;"><u>#sbir.url#</u></a></td></tr>
       </table>

       </cfoutput>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

