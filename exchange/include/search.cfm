<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("keywords")>
 <cfset #session.search_keywords# = #keywords#>
 <cfset sstring = #keywords#>
</cfif>

<cfset search_string = "">
<cfset length = listlen(session.search_keywords,' ')>
<cfset counter = 1>

<cfloop list="#session.search_keywords#" index="element" delimiters=" ">
<cfif counter LT length>
 <cfset search_string = search_string & element & " and ">
<cfelseif counter is length>
 <cfset search_string = search_string & element>
</cfif>
<cfset counter = counter + 1>
</cfloop>

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select company_duns from company
 where company_id = #id#
</cfquery>

<cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sams
 where duns = '#company.company_duns#'
</cfquery>

<cfset duns = #company.company_duns#>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfoutput>

           <tr><td class="feed_header">Search Results for "<cfoutput>#sstring#</cfoutput>"

           </td>
           <td align=right class="feed_option">

           <a href="/exchange/include/federal_profile.cfm?id=#id#">Return</a>

           </cfoutput>

          </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		   <cfquery name="awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
             select id, award_description, period_of_performance_start_date, period_of_performance_current_end_date, modification_number, award_id_piid, awarding_office_name, funding_office_name, action_date, federal_action_obligation, awarding_agency_name, awarding_sub_agency_name, recipient_name, recipient_duns from award_data
             where contains((award_description, awarding_office_name, funding_office_name),'#search_string#') and
                   recipient_duns = '#duns#'

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by award_id_piid, modification_number ASC
		    <cfelseif #sv# is 10>
             order by award_id_piid, modification_number DESC
		    <cfelseif #sv# is 2>
             order by action_date DESC
		    <cfelseif #sv# is 20>
             order by action_date ASC
		    <cfelseif #sv# is 3>
             order by recipient_name ASC
		    <cfelseif #sv# is 30>
             order by recipient_name DESC
		    <cfelseif #sv# is 4>
             order by awarding_agency_name ASC
		    <cfelseif #sv# is 40>
             order by awarding_agency_name DESC
		    <cfelseif #sv# is 5>
             order by awarding_sub_agency_name ASC
		    <cfelseif #sv# is 50>
             order by awarding_sub_agency_name DESC
		    <cfelseif #sv# is 6>
             order by awarding_office_name ASC
		    <cfelseif #sv# is 60>
             order by awarding_office_name DESC
		    <cfelseif #sv# is 7>
             order by funding_office_name ASC
		    <cfelseif #sv# is 70>
             order by funding_office_name DESC
		    <cfelseif #sv# is 8>
             order by federal_action_obligation DESC
		    <cfelseif #sv# is 80>
             order by federal_action_obligation ASC
		    </cfif>

		   <cfelse>
		     order by action_date DESC
		   </cfif>

		   </cfquery>

		  <cfset counter = 0>
		  <cfset tot = 0>

 		  <cfoutput>

         <tr><td class="feed_option" colspan=7><b>Total Awards: #awards.recordcount#</b></td></tr>
          <tr><td>&nbsp;</td></tr>

          <tr>
             <td class="text_xsmall"><b>Award Date</b></a></td>
             <td class="text_xsmall"><b>Award ID</b></a></td>
             <td class="text_xsmall"><b>Department</b></a></td>
             <td class="text_xsmall"><b>Agency</b></a></td>
             <td class="text_xsmall"><b>Office</b></a></td>
             <td class="text_xsmall" width=350><b>Award Description</b></td>
             <td class="text_xsmall" width=75><b>PoP Start</b></td>
             <td class="text_xsmall" width=75><b>PoP End</b></td>
             <td class="text_xsmall" align=right><b>Obligation</b></td>
          </tr>

          </cfoutput>

          <cfset total = 0>

          <cfoutput query="awards">

            <cfif counter is 0>
            <tr bgcolor="ffffff">
           <cfelse>
            <tr bgcolor="e0e0e0">

           </cfif>

               <td class="text_xsmall" width=75 valign=top>#dateformat(action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" width=100 valign=top><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
               <td class="text_xsmall" valign=top>#awarding_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#awarding_office_name#</td>
               <td class="text_xsmall" valign=top>#replaceNoCase(award_description,search_string,"<span style='background:yellow'>#ucase(search_string)#</span>","all")#</td>

               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top>#dateformat(period_of_performance_current_end_date,'mm/dd/yyyy')#</td>

               <td class="text_xsmall" width=75 align=right valign=top>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             </tr>

          <cfif counter is 0>
           <cfset counter = 1>
          <cfelse>
           <cfset counter = 0>
          </cfif>

          <cfset total = total + #federal_action_obligation#>

          </cfoutput>

          <cfoutput>

          <tr><td class="feed_option" colspan=8><b>Total</b></td>
              <td class="feed_option" align=right><b>#numberformat(total,'$999,999,999')#</b></td></tr>

          </cfoutput>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>