<cfif not isdefined("sv")>
 <cfset sv = 3>
</cfif>

	<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
     select prime_awardee_duns as prime, sum(subaward_amount) as value, count(distinct(prime_award_piid)) as contracts, prime_awardee_name, prime_awardee_duns from award_data_sub
     where (subawardee_duns = '#session.company_profile_duns#' or subawardee_parent_duns = '#session.company_profile_duns#')
     group by prime_awardee_duns, prime_awardee_name

		<cfif #sv# is 1>
		 order by prime_awardee_name DESC
		<cfelseif #sv# is 10>
		 order by prime_awardee_name ASC
		<cfelseif #sv# is 2>
		 order by contracts DESC
		<cfelseif #sv# is 20>
		 order by contracts ASC
		<cfelseif #sv# is 3>
		 order by value DESC
		<cfelseif #sv# is 30>
		 order by value ASC
		</cfif>
	</cfquery>

<cfif isdefined("export")>
 <cfinclude template="/exchange/include/export_to_excel.cfm">
</cfif>

<cfoutput>

</cfoutput>

<cfif agencies.recordcount is 0>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header" style="font-size: 30;">Subcontracts</td></tr>
	   <tr><td colspan=3><hr></td></tr>
	   <tr><td height=10></td></tr>
	</table>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <tr><td class="feed_sub_header">No subcontracts were found.</td></tr>
	</table>

<cfelse>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td class="feed_header" style="font-size: 30;">Subcontracts</td>
		   <td class="feed_sub_header" align=right><a href="/exchange/include/profile.cfm?l=11"><u>By Prime Contractor</u></a> &nbsp;|&nbsp;<a href="/exchange/include/profile.cfm?l=15">By Agency</a>&nbsp;&nbsp;
		   <a href="/exchange/include/profile.cfm?l=11&sv=#sv#&export=1"><img src="/images/icon_export_excel.png" border=0 width=20 alt="Export to Excel" title="Export to Excel"></a></td></tr>
	   <tr><td colspan=3><hr></td></tr>
	   <tr><td height=10></td></tr>
	</table>

	<cfoutput>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	<tr height=50>
	   <td class="feed_sub_header" width=75><a href="/exchange/include/profile.cfm?l=11&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>PRIME CONTRACTOR</b></a></td>
	   <td class="feed_sub_header" align=center><a href="/exchange/include/profile.cfm?l=11&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>## OF SUBCONTRACTS</b></a></td>
	   <td class="feed_sub_header" align=right><a href="/exchange/include/profile.cfm?l=11&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TOTAL VALUE</b></a></td>
	</tr>

	</cfoutput>

	<cfset #counter# = 0>
	<cfset #sub_total# = 0>

	<cfoutput query="agencies">

	 <cfif counter is 0>
	  <tr bgcolor="ffffff" height=30>
	 <cfelse>
	  <tr bgcolor="f0f0f0" height=30>
	 </cfif>

		 <td class="feed_sub_header"><a href="/exchange/include/company_subcontracts_awards.cfm?prime_awardee_duns=#prime_awardee_duns#" target="_blank" rel="noopener">#prime_awardee_name#</a></td>
		 <td class="feed_sub_header" style="font-weight: normal;" align=center>#numberformat(contracts,'999,999')#</td>
		 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(value,'$999,999,999,999')#</td>

	 </tr>


	 <cfif counter is 0>
	  <cfset counter = 1>
	 <cfelse>
	  <cfset counter = 0>
	 </cfif>
	 <cfset #sub_total# = #sub_total# + #value#>

	</cfoutput>

	<tr><td height=5></td></tr>
	<tr><td class="feed_sub_header">TOTAL</td>
		<td class="feed_sub_header" align=right colspan=7><cfoutput>#numberformat(sub_total,'$999,999,999,999')#</cfoutput></td></tr>

   </cfif>

   </table>