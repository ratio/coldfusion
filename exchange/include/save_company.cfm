<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif isdefined("comp_id")>
 <cfset session.company_profile_id = #comp_id#>
</cfif>

<cfif isdefined("id")>
 <cfset session.company_profile_id = #id#>
</cfif>

<cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing.sharing_group_id from sharing
 join sharing_group on sharing_group.sharing_group_id = sharing.sharing_group_id
 join sharing_group_usr on sharing_group_usr_group_id = sharing.sharing_group_id
 where sharing_hub_id = #session.hub# and
       sharing_access > 1 and
       sharing_group_usr_usr_id = #session.usr_id#
</cfquery>

<cfif groups.recordcount is 0>
 <cfset group_list = 0>
<cfelse>
 <cfset group_list = valuelist(groups.sharing_group_id)>
</cfif>

<cfquery name="port_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_portfolio_id from sharing
 where sharing_hub_id = #session.hub# and
       sharing_access > 1 and
       sharing_portfolio_id is not null and
       (sharing_to_usr_id = #session.usr_id# or sharing_group_id in (#group_list#))
 union
 select portfolio_id as sharing_portfolio_id from portfolio
 where portfolio_usr_id = #session.usr_id# and
       portfolio_hub_id = #session.hub#
</cfquery>

<cfif port_access.recordcount is 0>
 <cfset port_list = 0>
<cfelse>
 <cfset port_list = valuelist(port_access.sharing_portfolio_id)>
</cfif>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(portfolio_item_portfolio_id), usr_first_name, usr_last_name, portfolio_name from portfolio_item
 join portfolio on portfolio_id = portfolio_item_portfolio_id
 join usr on usr_id = portfolio_usr_id
 where (portfolio_item_portfolio_id in (#port_list#) or portfolio_usr_id = #session.usr_id#) and
       portfolio_hub_id = #session.hub# and
       portfolio_item_company_id = #session.company_profile_id#
 group by portfolio_item_portfolio_id, usr_first_name, usr_last_name, portfolio_name
</cfquery>

<cfif check.recordcount is 0>
 <cfset check_list = 0>
<cfelse>
 <cfset check_list = valuelist(check.portfolio_item_portfolio_id)>
</cfif>

<cfquery name="portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from portfolio
 where (portfolio_id in (#port_list#) and portfolio_id not in (#check_list#))
 order by portfolio_name
</cfquery>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">Add to Portfoio</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<cfoutput query="comp">
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td valign=top width=130>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=10></td></tr>
			   <tr><td>
					<a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif comp.company_logo is "">
					  <img src="//logo.clearbit.com/#comp.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
					  <img src="#media_virtual#/#comp.company_logo#" width=100 border=0>
					</cfif>
					</a>
			   </td></tr>
			   </table>
             </td><td valign=top>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr><td class="feed_title"><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">#company_name#</a></td>
			   <td align=right valign=top width=100></td></tr>
               <tr><td class="feed_option">
				<cfif #comp.company_city# is "" or #comp.company_state# is "">
				 <cfif #comp.company_city# is "">
				  #comp.company_state#
				 <cfelse>
				  #comp.company_state#
				 </cfif>
				<cfelse>
				 <b>#comp.company_city#, #comp.company_state#</b>
				</cfif>
			   </td></tr>

		       <tr><td colspan=3 class="feed_option" valign=top><cfif comp.company_long_desc is "">Company description not provided.<cfelse><cfif len(comp.company_long_desc GT 525)>#left(comp.company_long_desc,'525')#...<cfelse>#comp.company_long_desc#</cfif></cfif></td></tr>
		   </table>
		   </td></tr>

		   <tr><td colspan=2><hr></td></tr>
     </table>
</cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="save_portfolio.cfm" method="post">

 <cfif portfolio.recordcount is 0>

     <tr><td class="feed_sub_header">Create New Portfolio</td></tr>
     <tr><td><input type="text" name="portfolio_name" size=50 class="input_text" required placeholder="Please provide a name for this Portfolio."></td></tr>

     <cfif check.recordcount GT 0>
      <tr><td colspan=2 class="feed_option"><b>Company already exists in the following Portfolio(s):</b></td></tr>
      <cfoutput query="check">
       <tr><td colspan=2 class="feed_option">#portfolio_name# (#usr_first_name# #usr_last_name#)</td></tr>
      </cfoutput>
     <cfelse>
          <tr><td height=20></td></tr>

     </cfif>

     <tr><td><hr></td></tr>
     <tr><td class="feed_sub_header" valign=top><b>Comments or Notes</td></tr>
     <tr><td class="feed_option"><textarea name="portfolio_item_comments" rows=3 cols=120 class="input_textarea" placeholder="Please add any comments you'd like about this Company."></textarea></td></tr>
     <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Create and Save">

 <cfelse>

     <tr>
	     <td class="feed_sub_header">Select Existing Portfolio</td>
         <td class="feed_sub_header">Or, Create a New Portfolio</td>
     </tr>

     <tr>
         <td width=450>
			 <select name="portfolio_item_portfolio_id" class="input_select" style="width: 400px;">
			 <cfoutput query="portfolio">
			  <option value=#portfolio_id#>#portfolio_name#
			 </cfoutput>
			 <select></td>
		<td><input type="text" name="portfolio_name" style="width: 350px;" class="input_text" placeholder="Please provide a name for this Portfolio."></td></tr>

	 <tr><td height=10></td></tr>

     <cfif check.recordcount GT 0>
      <tr><td colspan=2 class="feed_option"><b>Company already exists in the following Portfolio(s):</b></td></tr>
      <cfoutput query="check">
       <tr><td colspan=2 class="feed_option">#portfolio_name# (#usr_first_name# #usr_last_name#)</td></tr>
      </cfoutput>
     </cfif>

     <tr><td colspan=2><hr></td></tr>
	 <tr><td colspan=2 class="feed_sub_header" valign=top><b>Comments or Notes</td></tr>
	 <tr><td colspan=2 class="feed_option"><textarea name="portfolio_item_comments" style="width: 800px; height: 100px;" placeholder="Please add any comments about this company." class="input_textarea"></textarea></td></tr>
	 <tr><td height=5></td></tr>
	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Add">

 </cfif>

<cfoutput>
	<input type="hidden" name="portfolio_item_company_id" value=#session.company_profile_id#>
	<input type="hidden" name="portfolio_item_duns" value="#comp.company_duns#">
	<input type="hidden" name="portfolio_item_type_id" value=1>
</cfoutput>

</form>

</table>

</center>

</body>
</html>

