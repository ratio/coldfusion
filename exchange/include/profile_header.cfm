 <!--- Record View --->

 <cfquery name="view" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  insert into cview
  (cview_by_usr_id, cview_company_id, cview_date)
  values
  (#session.usr_id#,#id#,#now()#)
 </cfquery>

<!--- Update Recent --->

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_company_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values(#id#,#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<!--- Check to see if this company is being followed or not --->

<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from follow
  where follow_company_id = #id# and
		follow_by_usr_id = #session.usr_id#
</cfquery>

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfoutput>

 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr><td class="feed_header" width=50>

                    <cfif company.company_logo is "">
					  <img src="//logo.clearbit.com/#company.company_website#" height=40 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company.company_logo#" height=40>
					</cfif>

	 </td>
	     <td class="feed_big_header">

	     <cfif id is 0>
	     &nbsp;&nbsp;No additional information on this company is available
	     <cfelse>
	     &nbsp;&nbsp;#company.company_name#
	     </cfif>
	     </td>

	  <cfif isdefined("u")>
	   <cfif u is 1>
		 <td class="feed_option" align=right><font color="green"><b>You are now following this company.</b></font></td>
	   <cfelseif u is 2>
		 <td class="feed_option" align=right><font color="red"><b>You have stopped following this company.</b></font></td>
	   <cfelseif u is 3>
		 <td class="feed_option" align=right><font color="red"><b>Invitation has been successfully sent.</b></font></td>
	   </cfif>
	  </cfif>

		 <td align=right>

         <cfif isdefined("session.hub")>

			  <cfif #admin.recordcount# is 1>
		       <input class="button_blue" type="button" value="Save as Featured" onclick="window.open('/exchange/include/feature.cfm?id=#company.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">&nbsp;&nbsp;
         	  </cfif>

         <cfelse>

			 <cfif #usr.usr_exchange_admin# is 1>
		       <input class="button_blue" type="button" value="Save as Featured" onclick="window.open('/exchange/include/feature.cfm?id=#company.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">&nbsp;&nbsp;
			 </cfif>

	     </cfif>

		 <input class="button_blue" type="button" value="Add to Portfolio" onclick="window.open('/exchange/include/save_company.cfm?id=#company.company_id#','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=200, top=50, scrollbars=yes,resizable=yes,width=900,height=475'); return false;">&nbsp;&nbsp;

         <cfif company.company_registered is "">
           <input class="button_blue" type="submit" name="button" value="Invite" onclick="location.href = 'invite.cfm?id=#id#'" />&nbsp;&nbsp;
         </cfif>

               <cfif #follow.recordcount# is 0>
                <input class="button_blue" type="submit" name="button" value="Follow" onclick="location.href = '/exchange/marketplace/follow.cfm?id=#id#&l=3'" />
               <cfelse>
                <input class="button_blue" type="submit" name="button" value="Unfollow" onclick="location.href = '/exchange/marketplace/unfollow.cfm?id=#id#&l=3'" />
               </cfif>
               &nbsp;&nbsp;
               <input class="button_blue" type="button" value="Close" onclick="windowClose();">

		 </td></tr>

</table>

</cfoutput>