<cfinclude template="/exchange/security/check.cfm">

<cfquery name="getco" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select company_duns from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sams
 where duns = '#getco.company_duns#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <tr><td class="feed_header">INVITE COMPANY</a></td>
				     <cfoutput>
				     <td align=right><a href="/exchange/include/profile.cfm"><img src="/images/delete.png" width=20 alt="Close" title="Close" border=0></a></td>
				     </cfoutput>

					 </tr>
				 <tr><td colspan=2><hr></td></tr>

                 <cfif isdefined("u")>
                  <cfif #u# is 1>
                   <tr><td class="feed_option"><font color="red"><b>You must include a name and email address if you choose the Other Email Address option.</b></font></td></tr>
                  </cfif>
				  <tr><td height=5></td></tr>
				 </cfif>
				 <tr><td class="feed_option">Displayed below are the email address(es) we already have for this company.  Please choose one or more of these or create your own.</td></tr>
				 <tr><td height=10></td></tr>
			   </table>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

                 <cfoutput>

                 <tr><td class="feed_sub_header" colspan=3>#ucase(company.legal_business_name)#</td></tr>
                 <tr><td height=10></td></tr>

				 <form action="invite_db.cfm" method="post">

                 <cfif #company.poc_email# is not "">
                  <tr>
                     <td class="feed_option" width=30><input type="checkbox" name="poc_email" value="#company.poc_email#" checked></td>
                     <td class="feed_option" width=200><b>Primary Point of Contact</b></td>
                     <td class="feed_option">#company.poc_fnme# #company.poc_lname# (<i>#company.poc_email#</i>)</td>
                  </tr>
                 </cfif>

                 <cfif #company.alt_poc_email# is not "">
                  <tr>
                     <td class="feed_option" width=30><input type="checkbox" name="alt_poc_email" value="#company.alt_poc_email#" checked></td>
                     <td class="feed_option" width=200><b>Alternate Point of Contact</b></td>
                     <td class="feed_option">#company.alt_poc_fname# #company.alt_poc_lname# (<i>#company.alt_poc_email#</i>)</td>
                  </tr>
                 </cfif>

                 <cfif #company.pp_poc_email# is not "">
                  <tr>
                     <td class="feed_option" width=30><input type="checkbox" name="pp_poc_email" value="#company.pp_poc_email#" checked></td>
                     <td class="feed_option" width=200><b>Past Performance POC</b></td>
                     <td class="feed_option">#company.pp_poc_fname# #company.pp_poc_lname# (<i>#company.pp_poc_email#</i>)</td>
                  </tr>
                 </cfif>

                 <cfif #company.elec_bus_poc_email# is not "">
                  <tr>
                     <td class="feed_option" width=30><input type="checkbox" name="elec_bus_poc_email" value="#company.elec_bus_poc_email#" checked></td>
                     <td class="feed_option" width=200><b>Electronic Business POC</b></td>
                     <td class="feed_option">#company.elec_bus_poc_fname# #company.elec_bus_poc_lnmae# (<i>#company.elec_bus_poc_email#</i>)</td>
                  </tr>
                 </cfif>

                  <tr>
                     <td class="feed_option" width=30><input type="checkbox" name="email_other"></td>
                     <td class="feed_option" width=200><b>Other Email Address</b></td>

                  </tr>

                  <tr><td height=5></td></tr>

                  <tr>
                  <td>&nbsp;</td>
                      <td class="text_xsmall"><b>Name</b></td>
                      <td class="text_xsmall"><b>Email Address</b></td>
                  </tr>

                  <tr><td>&nbsp;</td>
                      <td class="feed_option"><input class="input_text" type="text" name="email_name" size=20></td>
                      <td class="feed_option"><input class="input_text" type="text" name="email_address" size=20></td>
                  </tr>

                  <tr><td height=10></td></tr>

                  <tr><td class="feed_option" colspan=3><b>PERSONAL MESSAGAE</b></td></tr>
                  <tr><td class="feed_option" colspan=3><textarea class="input_textarea" name="email_message" cols=140 rows=10>
Hi [Name]

I hope you are doing well.   I saw your company on the Exchange and wanted to personally invite you to create an account.  It's free and a great way of discovering opportunities to help your company accelerate growth and broadcast your services and capabilities to the market.

Sincerely,

[ Your Name ]

                  </textarea></td></tr>

                 </cfoutput>

                 <tr><td height=5></td></tr>

                 <tr><td class="link_small_gray" colspan=3>By clicking <b>Send Invitation</b> we will email the below message to all the email addresses you have checked above with an invitation link.  The email will include your name and email address as the invitee.</td></tr>

                 <tr><td height=20></td></tr>
                 <tr><td colspan=3>
                     <input class="button_blue_large" type="submit" name="button" value="Send Invitation">
                 </td></tr>

                 <cfoutput>
                 <input type="hidden" name="duns" value="#company.duns#">
                 <input type="hidden" name="id" value=#session.company_profile_id#>
                 </cfoutput>

                 </form>


			   </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

