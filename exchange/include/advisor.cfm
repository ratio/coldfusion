<cfquery name="advisor_lookup" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select usr_advisor_id from usr
 where usr_id = #session.usr_id#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header">My Advisor</td></tr>
  <tr><td>

      <cfif #advisor_lookup.usr_advisor_id# is "" or #advisor_lookup.usr_advisor_id# is 0>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td height=10></td></tr>
       <tr><td valign=top width=50><img src="/images/headshot.png" width=40></td>
           <td valign=top class="small_text"><a href="connect.cfm">Connect with an Advisor</a> to help you capitalize on the Exchange.</td></tr>
      </table>

      <cfelse>

		<cfquery name="advisor_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from usr
		 where usr_id = #advisor_lookup.usr_advisor_id#
		</cfquery>

	   <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=10></td></tr>
		   <tr><td>

		   <cfif #advisor_info.usr_photo# is "">
		     <img style="border-radius: 8px;" src="/images/headshot.png" width=100>
		   <cfelse>
	   	     <img style="border-radius: 8px;" src="#media_virtual#/#advisor_info.usr_photo#" width=125>
	   	   </cfif>

	   	   </td></tr>
		   <tr><td height=5></td></tr>
           <tr><td class="feed_option"><b>#advisor_info.usr_first_name# #advisor_info.usr_last_name#</b></td></tr>
		   <cfif #advisor_info.usr_title# is not ""><tr><td class="small_text"><b>#advisor_info.usr_title#</b></td></tr></cfif>
		   <tr><td height=6></td></tr>
		   <cfif #advisor_info.usr_phone# is not ""><tr><td class="small_text">#advisor_info.usr_phone#</td></tr></cfif>
		   <tr><td height=3></td></tr>
		   <tr><td class="small_text"><a href="mailto:#advisor_info.usr_email#">#advisor_info.usr_email#</a></td></tr>
		   <tr><td height=6></td></tr>
		   <tr><td class="small_text"><a href="request.cfm">Request a Meeting</a></td></tr></tr>

		  </table>

       </cfoutput>

      </cfif>

  </td></tr>
</table>