<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Contract Vehicles</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<cfquery name="vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cv
 order by cv_name
</cfquery>

<cfquery name="duns" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_duns from company
 where company_id = #session.company_profile_id#
</cfquery>

<cfquery name="lake_vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(parent_award_id) from award_data
 where recipient_duns = '#duns.company_duns#' or
       recipient_parent_duns = '#duns.company_duns#'
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfset counter = 0>

<tr>
   <td class="feed_sub_header">ORGANIZATION</td>
   <td class="feed_sub_header">VEHICLE NAME</td>
   <td class="feed_sub_header">DESCRIPTION</td>
   <td class="feed_sub_header" align=center>AWARDS</td>
   <td class="feed_sub_header" align=right>VALUE</td>
</tr>

<cfset vehicle_count = 0>

<cfloop query="vehicles">

	<cfquery name="company_vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
      select count(id) as total, sum(federal_action_obligation) as obligated from award_data
      where (recipient_duns = '#duns.company_duns#' or recipient_parent_duns = '#duns.company_duns#') and
            parent_award_id like '#vehicles.cv_pattern#%'
	</cfquery>

	<cfoutput>

 	<cfif company_vehicles.total GT 0>

	 <cfif counter is 0>
	  <tr bgcolor="ffffff">
	 <cfelse>
	  <tr bgcolor="e0e0e0">
	 </cfif>

	    <td class="feed_sub_header" valign=top width=150><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicles.cv_pattern#&duns=#duns.company_duns#">#vehicles.cv_org_acronym# <cfif #vehicles.cv_type# is not "">&nbsp;(#vehicles.cv_type#)</cfif></a></td>
	    <td class="feed_sub_header" valign=top width=300><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicles.cv_pattern#&duns=#duns.company_duns#">#vehicles.cv_name#</a></td>
	    <td class="feed_sub_header" valign=top style="font-weight: normal;">#vehicles.cv_desc#</td>
	    <td class="feed_sub_header" valign=top align=center style="font-weight: normal;">#company_vehicles.total#</td>
	    <td class="feed_sub_header" valign=top width=125 style="font-weight: normal;" align=right>#numberformat(company_vehicles.obligated,'$999,999,999')#</td>
	 </tr>

	<cfif counter is 0>
	 <cfset counter = 1>
	<cfelse>
	 <cfset counter = 0>
	</cfif>

	<cfset vehicle_count = vehicle_count + 1>

	</cfif>

	</cfoutput>

</cfloop>

	<cfif vehicle_count is 0>
	 <tr><td colspan=3 class="feed_sub_header" style="font-weight: normal">No contract vehicles were found.</td></tr>
	</cfif>

</table>

   </td></tr>
</td></tr>
</table>