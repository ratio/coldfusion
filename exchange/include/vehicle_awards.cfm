<cfinclude template="/exchange/security/check.cfm">

<cfset #contract_number# = #replace(contract,"-","","all")#>

<cfquery name="name" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sams
 where duns = '#duns#'
</cfquery>

<cfquery name="v_total" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(id) as awards, sum(federal_action_obligation) as total from award_data
 where recipient_duns = '#duns#' and
       parent_award_id = '#contract_number#'
</cfquery>

<cfquery name="vehicle_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(source), contractno, email, phone, vlookup_agency, vlookup_vehicle_abbr, contract_end_date from vehicles
 left join vlookup on vlookup.vlookup_vehicle_abbr = vehicles.source
 where duns = '#duns#' and
       contractno = '#contract#'
</cfquery>

<cfquery name="v_awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_data award_data
 where recipient_duns = '#duns#' and
       parent_award_id = '#contract_number#'
 order by action_date DESC
</cfquery>

<html>
<head>
	<title><cfoutput>#name.legal_business_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" bgcolor="afafaf">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <cfoutput>
			  <tr><td class="feed_header">#name.legal_business_name#</td>
				  <td class="feed_option" align=right><a href="company_profile.cfm?id=0&duns=#duns#">Return</a></td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr><td class="feed_option"><b>Contract Vehicle: </b>&nbsp;#vehicle_info.vlookup_agency# #vehicle_info.vlookup_vehicle_abbr#</td>
			      <td class="feed_option" align=right><b>Total Awards: </b>&nbsp;#v_total.awards#</td></tr>
			  <tr><td class="feed_option"><b>Contract Number: </b>&nbsp;#contract#</td>
			      <td class="feed_option" align=right><b>Total Value: </b>&nbsp;#numberformat(v_total.total,'$999,999,999')#</td></tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr><td class="feed_option"><b>Contract Awards</b></td></tr>
			  <tr><td height=10></td></tr>
          </cfoutput>
         </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
              <td class="text_xsmall" width=75><b>Award Date</b></td>
              <td class="text_xsmall"><b>Contract Number</b></td>
              <td class="text_xsmall"><b>Department</b></td>
              <td class="text_xsmall"><b>Agency</b></td>
              <td class="text_xsmall"><b>Office</b></td>
              <td class="text_xsmall"><b>Product or Services</b></td>
              <td class="text_xsmall" align=center><b>NAICS</b></td>
              <td class="text_xsmall" align=center><b>Pricing</b></td>
              <td class="text_xsmall"><b>Start</b></td>
              <td class="text_xsmall"><b>End</b></td>
              <td class="text_xsmall" align=right width=75><b>Obligation</b></td>
           </tr>

			<cfset #total_value# = 0>

			<cfset #counter# = 0>

			<cfoutput query="v_awards">

			  <tr

			  <cfif #counter# is 0>
			   bgcolor="ffffff"
			  <cfelse>
			   bgcolor="e0e0e0"
			  </cfif>


			  >
				 <td class="text_xsmall">#dateformat(action_date,'mm/dd/yyyy')#</td>
				 <td class="text_xsmall"><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">#award_id_piid#</a></td>
				 <td class="text_xsmall">#awarding_agency_name#</td>
				 <td class="text_xsmall">#awarding_sub_agency_name#</td>
				 <td class="text_xsmall">#awarding_office_name#</td>
				 <td class="text_xsmall">#product_or_service_code_description#</td>
				 <td class="text_xsmall" align=center width=75>#naics_code#</td>
				 <td class="text_xsmall" width=75 align=center>
				 <cfif type_of_contract_pricing is "Firm Fixed Price">
				  FFP
				 <cfelseif type_of_contract_pricing is "Time and Materials">
				  T&M
				 <cfelseif type_of_contract_pricing is "Cost Plus Fixed Fee">
				  CPFF
				 <cfelseif type_of_contract_pricing is "Labor Hours">
				  LH
				 <cfelseif type_of_contract_pricing is "Fixed Price Incentive">
				  FPI
				 <cfelse>
				 #left(type_of_contract_pricing,21)#
				 </cfif>
				 </td>
				 <td class="text_xsmall" width=75>#dateformat(period_of_performance_start_date,'mm/dd/yyyy')#</td>
				 <td class="text_xsmall" width=75>#dateformat(period_of_performance_potential_end_date,'mm/dd/yyyy')#</td>
				 <td class="text_xsmall" align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
			  </tr>

			  <cfset #total_value# = #total_value# + #federal_action_obligation#>

			  <cfif #counter# is 0>
			   <cfset #counter# = 1>
			  <cfelse>
			   <cfset #counter# = 0>
			  </cfif>

			</cfoutput>

         </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

