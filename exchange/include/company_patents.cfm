<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Patents</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td class="feed_sub_header" style="font-weight: normal;">Patents are matched to Company Profiles based on EXACT name match with the United States Patent Office (USPTO).  Additional Patents may exist<br>and can be found in the <a href="/exchange/patents" target="_blank" rel="noopener">Patents Module</a>.</td></tr>
</table>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select patent.id, patent.patent_id, country, title, organization, patent.type, abstract as patent_desc, date from patent
 left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
 where patent.patent_id is not null and
       patent_rawassignee.organization = '#name.company_name#'
</cfquery>

  <cfparam name="URL.PageId" default="0">
  <cfset RecordsPerPage = 250>
  <cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
  <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
  <cfset EndRow = StartRow+RecordsPerPage-1>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr><td height=5></td></tr>

<cfif agencies.recordcount is 0>
  <tr><td class="feed_sub_header" colspan=16>No patents were found.</td></tr>
<cfelse>

  <tr>
	  <td class="feed_option">

		  <cfif agencies.recordcount GT #RecordsPerPage#>
			  <b>Page: </b>&nbsp;|
			  <cfloop index="Pages" from="0" to="#TotalPages#">
			   <cfoutput>
			   <cfset DisplayPgNo = Pages+1>
				  <cfif URL.PageId eq pages>
					 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
				  <cfelse>
					 <a href="?pageid=#pages#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
				  </cfif>
			   </cfoutput>
			  </cfloop>
		   </cfif>
	  </td>

	  <td class="feed_option" align=right></td></tr>

  </table>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <cfoutput>

	  <tr height=40>
		 <td class="feed_option" width=75><b>DATE GRANTED</b></td>
		 <td class="feed_option" width=75><b>PATENT ##</b></td>
		 <td class="feed_option"><b>TITLE</b></td>
		 <td class="feed_option"><b>ORGANIZATION</b></td>
		 <td class="feed_option"><b>DESCRIPTION</b></td>
		 <td class="feed_option" align=right><b>TYPE</b></td>
	  </tr>

  </cfoutput>

  <cfset counter = 0>

   <cfloop query="agencies">

	   <cfif CurrentRow gte StartRow >

	   <cfif counter is 0>
		<tr bgcolor="ffffff" height=30>
	   <cfelse>
		<tr bgcolor="e0e0e0" height=30>
	   </cfif>

	   <cfoutput>

		   <td class="feed_option" valign=top width=100 style="padding-top: 20px; padding-bottom: 20px;">#dateformat(agencies.date,'mm/dd/yyyy')#</td>
		   <td class="feed_option" valign=top width=75 style="padding-top: 20px; padding-bottom: 20px;"><a href="/exchange/include/patent_information.cfm?patent_id=#patent_id#" target="_blank" rel="noopener"><b>#agencies.patent_id#</b></a></td>
		   <td class="feed_option" valign=top width=300 style="padding-top: 20px; padding-bottom: 20px;">
		   <a href="/exchange/include/patent_information.cfm?patent_id=#patent_id#" target="_blank" rel="noopener"><b>#agencies.title#</b></a></td>
		   <td class="feed_option" valign=top width=200 style="padding-top: 20px; padding-bottom: 20px;">#ucase(agencies.organization)#</td>
		   <td class="feed_option" valign=top style="padding-top: 20px; padding-bottom: 20px;">#agencies.patent_desc#</td>
		   <td class="feed_option" valign=top width=100 align=right style="padding-top: 20px; padding-bottom: 20px;">#ucase(agencies.type)#</td>

		</cfoutput>

		</tr>

	  <cfif counter is 0>
	   <cfset counter = 1>
	  <cfelse>
	   <cfset counter = 0>
	  </cfif>

	  </cfif>

	  <cfif CurrentRow eq EndRow>
	   <cfbreak>
	  </cfif>

  </cfloop>

  </cfif>

  <tr><td>&nbsp;</td></tr>

  </table>


