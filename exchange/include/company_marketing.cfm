
<cfquery name="private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from doc
 where doc_company_id = #session.company_profile_id# and
       doc_public = 1
 order by doc_order
</cfquery>


<cfquery name="public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from doc
 where doc_company_id = #session.company_profile_id# and
       doc_public = 1
 order by doc_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Marketing Material</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Local Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been approved by Companies for use on the <cfoutput>#session.network_name#</cfoutput>.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <cfif private.recordcount is 0>
   <tr><td class="feed_sub_header">No information was found.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="private">

      <tr>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(private.doc_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(private.doc_description,"#chr(10)#","<br>","all")#</td></tr>
			     <cfif private.doc_attachment is not "">
			     	<tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><a href="#media_virtual#/#private.doc_attachment#" target="_blank" rel="noopener">Download Material</a></td></tr>
                 </cfif>

			   </table>

           </td>

           <td align=right>

            <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('private#private.doc_id#');">

           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="private#private.doc_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfif private.doc_embed is not "">
			  <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>#private.doc_embed#</td></tr>
		  <cfelse>
			  <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>No additional information available</td></tr>
		  </cfif>

         </table>
       </div>
       </td></tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

  <tr><td height=10></td></tr>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Public Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been sourced from the Ratio Exchange.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <tr><td height=5></td></tr>

  <cfif public.recordcount is 0>
   <tr><td class="feed_sub_header">No information has been sourced.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfoutput query="public">

        <tr>

             <td valign=top>

  			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
  			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(public.doc_name)#</td></tr>
  			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(public.doc_description,"#chr(10)#","<br>","all")#</td></tr>
  			     <cfif public.doc_attachment is not "">
  			     	<tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><a href="#media_virtual#/#public.doc_attachment#" target="_blank" rel="noopener">Download Material</a></td></tr>
                   </cfif>

  			   </table>

             </td>

             <td align=right>

              <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('public#public.doc_id#');">

             </td>

        </tr>

        <tr><td></td><td colspan=2>
          <div id="public#public.doc_id#" style="display:none;">
           <table cellspacing=0 cellpadding=0 border=0 width=100%>

  		  <cfif public.doc_embed is not "">
  			  <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>#public.doc_embed#</td></tr>
  		  <cfelse>
  			  <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top>No additional information available</td></tr>
  		  </cfif>

           </table>
         </div>
         </td></tr>

        <tr><td colspan=3><hr></td></tr>
     </cfoutput>


   </table>

   </td></tr>

  </cfif>

</table>
