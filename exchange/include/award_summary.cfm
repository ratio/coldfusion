<cfinclude template="/exchange/security/check.cfm">

<cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sams
 where duns = '#duns#'
</cfquery>

<html>
<head>
	<title><cfoutput>#sams.legal_business_name# - Company Dashboard</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<body class="body" bgcolor="afafaf">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">
           <cfoutput>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td class="feed_header" valign=top>Company Summary</a></td>
					<td class="feed_option" align=right><input class="button_blue" style="font-size: 11px; height: 20px; width: 60px;" type="button" value="Close" onclick="windowClose();"></td></tr>
				<tr><td height=5></td></tr>
			   </table>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td>
					   <table cellspacing=0 cellpadding=0 border=0 width=100%>
						<tr><td valign=top width=25%>
								<table cellspacing=0 cellpadding=0 border=0 width=100%>
									<tr><td class="feed_option"><b>Company</b></td></tr>
									<tr><td class="feed_option">#sams.legal_business_name#<br><a href="#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#sams.corp_url#</a></td></tr>
								</table>
						</td><td valign=top width=25%>
							<table cellspacing=0 cellpadding=0 border=0 width=100%>
								<tr><td class="feed_option"><b>Address</b></td></tr>
								<tr><td class="feed_option"><cfif #sams.phys_address_l1# is not "">#sams.phys_address_l1#<br></cfif>
															<cfif #sams.phys_address_line_2# is not "">#sams.phys_address_line_2#<br></cfif>
															#sams.city_1# #sams.state_1#, #sams.zip_1#<br></td></tr>
							</table>
						</td><td valign=top width=25%>
							<table cellspacing=0 cellpadding=0 border=0 width=100%>
								<tr><td class="feed_option"><b>Important Dates</b></td></tr>
								<tr><td class="feed_option">
															<b>Founded: </b>#mid(sams.biz_start_date,5,2)#/#right(sams.biz_start_date,2)#/#left(sams.biz_start_date,4)#<br>
															<b>Registration Date: </b>#mid(sams.init_reg_date,5,2)#/#right(sams.init_reg_date,2)#/#left(sams.init_reg_date,4)#<br>
															<b>Activation Date: </b>#mid(sams.activation_date,5,2)#/#right(sams.activation_date,2)#/#left(sams.activation_date,4)#<br>
															<b>Expiration Date: </b>#mid(sams.reg_exp_date,5,2)#/#right(sams.reg_exp_date,2)#/#left(sams.reg_exp_date,4)#<br>
															<b>Last Updated: </b>#mid(sams.last_update,5,2)#/#right(sams.last_update,2)#/#left(sams.last_update,4)#</td></tr>
							</table>
						</td><td valign=top width=25%>
							<table cellspacing=0 cellpadding=0 border=0 width=100%>
								<tr><td class="feed_option"><b>Corporate Information</b></td></tr>
								<tr><td class="feed_option">
															<b>DUNS: </b>#sams.duns#<cfif #sams.duns_4# is not "">-#sams.duns_4#</cfif><br>
															<b>State of Incorporation: </b>#sams.state_of_inc#<br>
															<b>Cage Code: </b>#sams.cage_code#</td></tr>
							</table>
						</td></tr>
					   </table>
				  </td></tr>
				  <tr><td colspan=4><hr></td></tr>
				</table>

        </cfoutput>

		  <cfquery name="award_summary_1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select count(id) as total, avg(CAST(annual_revenue AS numeric)) as revenue, avg(CAST(number_of_employees AS int)) as employees, datepart(yyyy, [action_date]) as [year], count(distinct(id)) as awards, count(distinct(award_id_piid)) as contracts, count(distinct(awarding_sub_agency_code)) as sub_agencies, count(distinct(awarding_agency_code)) as agencies, sum(base_and_all_options_value) as all_options, sum(base_and_exercised_options_value) as options, sum(federal_action_obligation) as federal_action_obligation from award_data
	       where recipient_duns = '#duns#'
           group by datepart(yyyy, [action_date])
           order by [year]
		  </cfquery>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <cfif award_summary_1.total GT 0>

        <cfset pcounter = 1>

         <tr>
              <td class="feed_option"><b>Year</b></td>
              <td class="feed_option" align=center><b>Departments</b></td>
              <td class="feed_option" align=center><b>Agencies</b></td>
              <td class="feed_option" align=center><b>Contracts</b></td>
              <td class="feed_option" align=center><b>Awards</b></td>
              <td class="feed_option" align=right><b>Obligated</b></td>
              <td class="feed_option" align=right><b>With Options</b></td>
              <td class="feed_option" align=center><b>Avg # of Employees</b></td>
              <td class="feed_option" align=right><b>Avg Revenue</b></td>
              <td class="feed_option" align=right><b>Fed Contract Growth %</b></td>
         </tr>

        <cfset counter = 0>
        <cfoutput query="award_summary_1">

         <cfif counter is 0>
          <tr bgcolor="ffffff">
         <cfelse>
          <tr bgcolor="e0e0e0">
         </cfif>

             <td class="feed_option">#year#</td>
             <td class="feed_option" align=center>#agencies#</td>
             <td class="feed_option" align=center>#sub_agencies#</td>
             <td class="feed_option" align=center>#contracts#</td>
             <td class="feed_option" align=center>#awards#</td>
             <td class="feed_option" align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>
             <td class="feed_option" align=right>#numberformat(options,'$999,999,999,999')#</td>
             <td class="feed_option" align=center>#employees#</td>
             <td class="feed_option" align=right>#numberformat(revenue,'$999,999,999')#</td>

             <cfif pcounter is 1>
              <td class="feed_option" align=right>N/A</td>
             <cfelse>

              <cfif last_year is 0 or last_year is "">

              <td class="feed_option" align=right>0%</td>

              <cfelse>

              <td class="feed_option" align=right>#round(evaluate((federal_action_obligation-last_year)/last_year*100))#%</td>

              </cfif>

             </cfif>

             <cfset pcounter = 2>
             <cfset last_year = federal_action_obligation>

             <cfif counter is 0>
              <cfset counter = 1>
             <cfelse>
              <cfset counter = 0>
             </cfif>


         </tr>
        </cfoutput>

       <cfelse>

         <tr><td class="feed_option">No awards were found.</td></tr>

       </cfif>


      </table>




	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

