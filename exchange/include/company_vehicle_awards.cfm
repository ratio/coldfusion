<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfif isdefined("session.company_profile_name")><cfoutput>Exchange - #session.company_profile_name#</cfoutput><cfelse>Exchange</cfif></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfquery name="vehicles" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from cv
 where cv_pattern = '#vehicle#'
</cfquery>

<cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select id, action_date, award_id_piid, modification_number, parent_award_id, recipient_name, recipient_duns, recipient_city_name, recipient_state_code, awarding_agency_name, awarding_sub_agency_name, awarding_office_name, funding_office_name, award_description, primary_place_of_performance_city_name, primary_place_of_performance_state_code, naics_code, product_or_service_code, product_or_service_code_description, type_of_contract_pricing, type_of_set_aside, period_of_performance_start_date, period_of_performance_current_end_date, federal_action_obligation from award_data
  where (recipient_duns = '#duns#' or recipient_parent_duns = '#duns#') and
		parent_award_id like '#vehicle#%'


	<cfif #sv# is 1>
	 order by action_date DESC
	<cfelseif #sv# is 10>
	 order by action_date ASC
	<cfelseif #sv# is 2>
	 order by award_id_piid ASC, modification_number ASC
	<cfelseif #sv# is 20>
	 order by award_id_piid DESC, modification_number ASC
	<cfelseif #sv# is 4>
	 order by recipient_name ASC
	<cfelseif #sv# is 40>
	 order by recipient_name DESC
	<cfelseif #sv# is 5>
	 order by primary_place_of_performance_city_name ASC
	<cfelseif #sv# is 50>
	 order by primary_place_of_performance_city_name DESC
	<cfelseif #sv# is 6>
	 order by recipient_state_code ASC
	<cfelseif #sv# is 60>
	 order by recipient_state_code DESC
	<cfelseif #sv# is 7>
	 order by awarding_office_name ASC
	<cfelseif #sv# is 70>
	 order by awarding_office_name DESC
	<cfelseif #sv# is 8>
	 order by naics_code ASC
	<cfelseif #sv# is 80>
	 order by naics_code DESC
	<cfelseif #sv# is 9>
	 order by product_or_service_code ASC
	<cfelseif #sv# is 90>
	 order by product_or_service_code DESC
	<cfelseif #sv# is 20>
	 order by period_of_performance_state_date DESC
	<cfelseif #sv# is 200>
	 order by period_of_performance_start_date ASC
	<cfelseif #sv# is 11>
	 order by period_of_performance_current_end_date DESC
	<cfelseif #sv# is 110>
	 order by period_of_performance_current_end_date ASC
	<cfelseif #sv# is 12>
	 order by federal_action_obligation DESC
	<cfelseif #sv# is 120>
	 order by federal_action_obligation ASC
	<cfelseif #sv# is 13>
	 order by type_of_set_aside ASC
	<cfelseif #sv# is 130>
	 order by type_of_set_aside DESC
	<cfelseif #sv# is 14>
	 order by type_of_contract_pricing ASC
	<cfelseif #sv# is 140>
	 order by type_of_contract_pricing DESC
	<cfelseif #sv# is 15>
	 order by awarding_sub_agency_name ASC
	<cfelseif #sv# is 150>
	 order by awarding_sub_agency_name DESC
	<cfelseif #sv# is 16>
	 order by awarding_agency_name ASC
	<cfelseif #sv# is 160>
	 order by awarding_agency_name DESC
	<cfelseif #sv# is 19>
	 order by recipient_name ASC
	<cfelseif #sv# is 190>
	 order by recipient_name DESC
	</cfif>

 </cfquery>

   <cfif isdefined("export")>
	 <cfinclude template="/exchange/include/export_to_excel.cfm">
	</cfif>

  <cfparam name="URL.PageId" default="0">
  <cfset RecordsPerPage = 100>
  <cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
  <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
  <cfset EndRow = StartRow+RecordsPerPage-1>

<cfinclude template = "/exchange/include/header.cfm">

           <div class="main_box">

           <cfinclude template="/exchange/include/company_header.cfm">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=20></td></tr>

		   <tr><td valign=top>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header" style="font-size: 30;">Contract Awards - <cfoutput>#vehicles.cv_name#</cfoutput></td>
				   <td class="feed_sub_header" align=right><a href="/exchange/include/profile.cfm?l=20">Return</a></td></tr>
			   <tr><td colspan=3><hr></td></tr>
			   <tr><td height=10></td></tr>
			</table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" colspan=16>No awards or mods were found with the information you entered.</td></tr>
          <cfelse>

          <tr>
              <td class="feed_option">

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&<cfif isdefined("sv")>vehicle=#vehicle#&duns=#duns#&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
              </td>
              <td class="feed_option" align=right>

              <cfoutput>
              <a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 border=0 alt="Export to Excel" title="Export to Excel"></a>
              </cfoutput>

              </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
				 <td class="text_xsmall" width=75><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award Date</b></a></td>
				 <td class="text_xsmall" width=100><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Contract ##</b></a></td>
				 <td class="text_xsmall" width=100><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Parent Award ##</b></a></td>

				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=19<cfelse><cfif #sv# is 19>sv=190<cfelse>sv=19</cfif></cfif>"><b>Company</b></a></td>

				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>City</b></a></td>
				 <td class="text_xsmall" align=center width=60><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>State</b></a></td>
			 	 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=16<cfelse><cfif #sv# is 16>sv=160<cfelse>sv=15</cfif></cfif>"><b>Department</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=15<cfelse><cfif #sv# is 15>sv=150<cfelse>sv=15</cfif></cfif>"><b>Agency</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Office</b></a></td>
				 <td class="text_xsmall"><b>Award Description</b></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>NAICS</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>PSC</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=13<cfelse><cfif #sv# is 13>sv=130<cfelse>sv=13</cfif></cfif>"><b>Set Aside</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=14<cfelse><cfif #sv# is 14>sv=140<cfelse>sv=14</cfif></cfif>"><b>Pricing</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=20<cfelse><cfif #sv# is 20>sv=200<cfelse>sv=20</cfif></cfif>"><b>PoP Start</b></a></td>
				 <td class="text_xsmall"><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>PoP End</b></a></td>
				 <td class="text_xsmall" align=right><a href="/exchange/include/company_vehicle_awards.cfm?vehicle=#vehicle#&duns=#duns#&<cfif not isdefined("sv")>sv=12<cfelse><cfif #sv# is 12>sv=120<cfelse>sv=12</cfif></cfif>"><b>Obligation</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>
          <cfset tot = 0>

           <cfloop query="agencies">

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

			   <cfoutput>

				   <td class="text_xsmall" valign=top width=120 style="padding-right: 20px;">#dateformat(agencies.action_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;"><a href="/exchange/include/award_information.cfm?id=#agencies.id#" target="_blank" rel="noopener"><b>#agencies.award_id_piid#</b></a><br>MOD - #agencies.modification_number#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;">#agencies.parent_award_id#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" width=200>#agencies.recipient_name#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;">#agencies.primary_place_of_performance_city_name#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" align=center>#agencies.primary_place_of_performance_state_code#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;">#agencies.awarding_agency_name#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;">#agencies.awarding_sub_agency_name#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;">#agencies.awarding_office_name#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" width=300>#agencies.award_description#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" width=75>#agencies.naics_code#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" width=50>#agencies.product_or_service_code#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" width=50>#agencies.type_of_set_aside#</td>
				   <td class="text_xsmall" valign=top style="padding-right: 20px;" width=100>#agencies.type_of_contract_pricing#</td>
				   <td class="text_xsmall" valign=top width=75 style="padding-right: 20px;">#dateformat(agencies.period_of_performance_start_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top width=75 style="padding-right: 20px;">#dateformat(agencies.period_of_performance_current_end_date,'mm/dd/yyyy')#</td>
				   <td class="text_xsmall" valign=top align=right width=75>#numberformat(agencies.federal_action_obligation,'$999,999,999')#</td>

				</cfoutput>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

		  <cfset tot = tot + #federal_action_obligation#>

 </cfif>

								  <cfif CurrentRow eq EndRow>
								   <cfbreak>
								  </cfif>

          </cfloop>

          <tr><td colspan=17><hr></td></tr>
          <tr>
             <td class="feed_option"><b>Total</b></td>
             <td class="feed_option" colspan=16 align=right><b><cfoutput>#numberformat(tot,'$999,999,999')#</cfoutput></b></td>
          </tr>

          </cfif>

		   </td></tr>
		</td></tr>
		</table>

           </td></tr>

           <tr><td height=20></td></tr>

           </table>

           </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>


