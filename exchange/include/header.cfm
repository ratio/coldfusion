<!-- MenuMaker Plugin -->

<script src="https://s3.amazonaws.com/menumaker/menumaker.min.js"></script>
<script src="/exchange/include/script_library.js"></script>

<!-- Icon Library -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="/include/menu.css" rel="stylesheet" type="text/css">
<script src="/exchange/include/script_library.js"></script>

<cfset page_time_start = gettickcount()>

<cfquery name="ass" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(usr_comp_id) as total from usr_comp
 where usr_comp_usr_id = #session.usr_id# and
       usr_comp_company_id <> #session.company_id#
</cfquery>

<cfif not isdefined("session.key")>
  <cfset session.key = generateSecretKey("AES") />
</cfif>

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_xref
 join usr on usr_id = hub_xref_usr_id
 where hub_xref_usr_id = #session.usr_id# and
       hub_xref_hub_id = #session.hub#
</cfquery>

<cfif usr.hub_xref_usr_role is 1>
 <cfset hub_admin = 1>
<cfelse>
 <cfset hub_admin = 0>
</cfif>

<cfif #findnocase("/tools/",cgi.script_name)# and #hub_admin# is not 1>
 <cflocation URL="/exchange/" addtoken="no">
</cfif>

<cfquery name="hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #session.hub#
</cfquery>

<cfif hub.hub_parent_hub_id is "" or hub.hub_parent_hub_id is 0>
 <cfset session.network = 0>
<cfelse>
 <cfset session.network = 1>
</cfif>

<!--- Check to see if there is a Parent hub --->

<cfif hub.hub_parent_hub_id is "">

 <cfset session.network_name = #hub.hub_name#>
 <cfset session.short_name = #hub.hub_name_short#>
 <cfset master_id = #hub.hub_id#>

<cfelse>

   <!--- Get Master Hub --->

    <cfquery name="master" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     select hub_name, hub_name_short, hub_id from hub
     where hub_id = #hub.hub_parent_hub_id#
    </cfquery>

    <cfset session.network_name = #master.hub_name#>
    <cfset session.sub_network_name = #hub.hub_name#>
    <cfset session.short_name = #master.hub_name_short#>
    <cfset master_id = #master.hub_id#>

</cfif>

<!--- Check to see if there are any networks --->

<cfquery name="netlist" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_id, hub_name from hub
 where hub_parent_hub_id = #master_id#
</cfquery>

<cfif netlist.recordcount is 0>
 <cfset show_networks = 0>
<cfelse>
 <cfset show_networks = 1>

 <cfset hlist = valuelist(netlist.hub_id)>

 <cfquery name="networks" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select hub_id, hub_name from hub
  join hub_xref on hub_xref_hub_id = hub_id and
       hub_xref_usr_id = #session.usr_id#
  where hub_id in (#hlist#)
 </cfquery>

</cfif>

<cfquery name="exchanges" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_xref
 join usr on usr_id = hub_xref_usr_id
 join hub on hub_id = hub_xref_hub_id
 where hub_xref_usr_id = #session.usr_id# and
       hub_xref_active = 1 and
       hub_parent_hub_id is null
 order by hub_name
</cfquery>

<cfif exchanges.recordcount is 0>
 <cfset show_exchanges = 0>
<cfelse>
 <cfset show_exchanges = 1>
</cfif>

<cfquery name="parent_items" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from menu
  left join app on app_id = menu_app_id
  where menu_role_id = #usr.hub_xref_role_id# and
        menu_hub_id = #session.hub# and
        menu_active = 1 and
        (menu_parent_id is null or menu_parent_id = 0)
  order by menu_order
</cfquery>

     <div class="head">

	 <cfoutput>
	   <table style="position: fixed;" cellspacing=0 cellpadding=0 border=0 width=100% height=75 bgcolor="<cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>">
	 </cfoutput>

 		  <tr>
					  <td width=20>&nbsp;</td>

				      <cfif show_networks is 1 or show_exchanges is 1>

                      <td width=40>

						<div class="dropdown" style="cursor: pointer; padding-left: 0px; padding-right: 0px;">

                        <cfif show_networks is 1>
							<img src="/images/icon_holder.png" width=20 style="padding-bottom: 0px;">
                        <cfelse>
							<img src="/images/icon_holder.png" width=20 style="padding-bottom: 0px;">
						</cfif>

						  <div class="dropdown-content" style="left: 0; top: 15; padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; width: 300px;">

							  <table cellspacing=0 cellpadding=0 border=0 width=100% style="padding-left: 0px; padding-right: 0px;">

							   <cfif show_networks is 1>

								   <cfif networks.recordcount GT 0>

									   <tr><td colspan=2 class="feed_sub_header" bgcolor="e0e0e0" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px; margin-top: 0px; margin-bottom: 0px;">&nbsp;<b><cfoutput>#session.short_name#</cfoutput> Networks</b></td></tr>
									   <tr><td height=10></td></tr>
									   <cfoutput query="networks">
										 <tr><td style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;"><a class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px; margin-top: 0px; margin-bottom: 0px; font-weight: normal;" href="/exchange/go.cfm?i=#encrypt(networks.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><span style="padding-left: 10px;">#networks.hub_name#</span></a></td></tr>
									   </cfoutput>
									   <tr><td height=10></td></tr>

								   </cfif>

							   </cfif>

							   <cfif show_exchanges is 1>

								   <tr><td colspan=2 class="feed_sub_header" bgcolor="e0e0e0" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px; margin-top: 0px; margin-bottom: 0px;">&nbsp;<b>My Exchanges</b></td></tr>
								   <tr><td height=10></td></tr>
								   <cfoutput query="exchanges">
									 <tr>
									     <td style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;"><a class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px; margin-top: 0px; margin-bottom: 0px; font-weight: normal;" href="/exchange/go.cfm?i=#encrypt(exchanges.hub_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" onclick="javascript:document.getElementById('page-loader').style.display='block';"><span style="padding-left: 10px;">#exchanges.hub_name#</span></a></td>
									 </tr>
								   </cfoutput>
								   <tr><td height=10></td></tr>

							   </cfif>


							  </table>

						  </div>
						</div>

					</td>

                      </cfif>

                      <td>

				  <cfoutput>

				     <cfif hub.hub_parent_hub_id is "">
                      <span class="hub_header"><a href="/exchange/"><font color="ffffff">#session.network_name#</font></a></span>
				     <cfelse>

                      <span class="hub_header"><a href="/exchange/"><font color="ffffff">#session.network_name#</font></a></span>
                      <br>
                      <span class="hub_header" style="font-size: 18px; color: ffffff; padding-left: 1px;"><a href="/exchange/"><font color="ffffff"><b>#session.sub_network_name#</b></font></a></td>


				     </cfif>

				  </cfoutput>

                  <td align=right>

				  <!--- Header Menu --->

					<cfoutput><div id="cssmenu" style="background: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"></cfoutput>

				  <ul>

					 <li><a href="/exchange/"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;Home</a></li>

                     <cfloop query="parent_items">
						 <cfoutput>
						 <li>

						 <cfif parent_items.menu_type_id is 1>
						    <a href="##" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true"></i></cfif> &nbsp;#parent_items.menu_name#</a>
					     <cfelseif parent_items.menu_type_id is 2>
							<a href="#parent_items.menu_url#" target="_blank" rel="noopener" rel="noreferrer" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse></cfif> &nbsp;#parent_items.menu_name#</a>
					     <cfelseif parent_items.menu_type_id is 3>
							<a href="#parent_items.app_path#" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse><cfif #parent_items.app_icon# is not "">#parent_items.app_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif></cfif> &nbsp;&nbsp;#parent_items.menu_name#</a>
					     <cfelseif parent_items.menu_type_id is 5>
							<a href="#parent_items.menu_custom_path#" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#parent_items.menu_name#</a>
					     <cfelseif parent_items.menu_type_id is 4>
							<a href="/exchange/display/index.cfm?i=#encrypt(parent_items.menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#parent_items.menu_name#</a>
					     <cfelseif parent_items.menu_type_id is 7>
							<a href="/exchange/display/page.cfm?i=#encrypt(parent_items.menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#parent_items.menu_name#</a>
					     <cfelseif parent_items.menu_type_id is 6>
							<a href="/exchange/display/content.cfm?i=#encrypt(parent_items.menu_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#" style="padding-right: 17px;"><cfif parent_items.menu_icon is not "">#parent_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#parent_items.menu_name#</a>
						 </cfif>
						 </cfoutput>

						<cfquery name="child_items" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
						  select * from menu
						  left join app on app_id = menu_app_id
						  where menu_role_id = #usr.hub_xref_role_id# and
								menu_hub_id = #session.hub# and
								menu_active = 1 and
								menu_parent_id = #parent_items.menu_id#
						  order by menu_order
						</cfquery>

						<cfif child_items.recordcount GT 0>

						   <cfoutput>
						   <ul style="background-color: <cfif #hub.hub_header_bgcolor# is "">000000<cfelse>#hub.hub_header_bgcolor#</cfif>;">
						   </cfoutput>

                           <cfloop query="child_items">

							<cfquery name="grandchild_items" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
							  select * from menu
							  left join app on app_id = menu_app_id
							  where menu_role_id = #usr.hub_xref_role_id# and
									menu_hub_id = #session.hub# and
									menu_active = 1 and
									menu_parent_id = #child_items.menu_id#
							  order by menu_order
							</cfquery>

                           <cfoutput>

							 <li>
							 <cfif child_items.menu_type_id is 1>
								<a href="##" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#child_items.menu_name#
							 <cfelseif child_items.menu_type_id is 2>
								<a href="#child_items.menu_url#" target="_blank" rel="noopener" rel="noreferrer" style=" padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#child_items.menu_name#
							 <cfelseif child_items.menu_type_id is 3>
								<a href="#child_items.app_path#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><cfif child_items.app_icon is not "">#child_items.app_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif></cfif> &nbsp;#child_items.menu_name#
						     <cfelseif child_items.menu_type_id is 5>
								<a href="#child_items.menu_custom_path#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#child_items.menu_name#
						     <cfelseif child_items.menu_type_id is 4>
								<a href="/exchange/display/index.cfm?i=#encrypt(child_items.menu_id,session.key, 'AES/CBC/PKCS5Padding', 'HEX')#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#child_items.menu_name#
						     <cfelseif child_items.menu_type_id is 7>
								<a href="/exchange/display/page.cfm?i=#encrypt(child_items.menu_id,session.key, 'AES/CBC/PKCS5Padding', 'HEX')#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#child_items.menu_name#
						     <cfelseif child_items.menu_type_id is 6>
								<a href="/exchange/display/content.cfm?i=#encrypt(child_items.menu_id,session.key, 'AES/CBC/PKCS5Padding', 'HEX')#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif child_items.menu_icon is not "">#child_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#child_items.menu_name#
							 </cfif>

                             <cfif grandchild_items.recordcount is 0>
                             </a></li>

                             <cfelse>
                             &nbsp;&nbsp;<b>>></b></a>

							  <ul style="background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>; width: auto;">

	                           <cfloop query="grandchild_items">

	                           <cfoutput>

									 <cfif grandchild_items.menu_type_id is 1>
										<li><a href="##" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#</cfif> &nbsp;#grandchild_items.menu_name#</a></li>
									 <cfelseif grandchild_items.menu_type_id is 2>
										<li><a href="#grandchild_items.menu_url#" target="_blank" rel="noopener" rel="noreferrer" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#grandchild_items.menu_name#</a></li>
									 <cfelseif grandchild_items.menu_type_id is 3>
										<li><a href="#grandchild_items.app_path#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#<cfelse><cfif #grandchild_items.app_icon# is not "">#grandchild_items.app_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif></cfif> &nbsp;#grandchild_items.menu_name#</a></li>
								     <cfelseif grandchild_items.menu_type_id is 5>
										<a href="#grandchild_items.menu_custom_path#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#grandchild_items.menu_name#</a>
								     <cfelseif grandchild_items.menu_type_id is 4>
										<a href="/exchange/display/index.cfm?i=#encrypt(grandchild_items.menu_id,session.key, 'AES/CBC/PKCS5Padding', 'HEX')#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#grandchild_items.menu_name#</a>
								     <cfelseif grandchild_items.menu_type_id is 7>
										<a href="/exchange/display/page.cfm?i=#encrypt(grandchild_items.menu_id,session.key, 'AES/CBC/PKCS5Padding', 'HEX')#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#grandchild_items.menu_name#</a>
								     <cfelseif grandchild_items.menu_type_id is 6>
										<a href="/exchange/display/content.cfm?i=#encrypt(grandchild_items.menu_id,session.key, 'AES/CBC/PKCS5Padding', 'HEX')#" style="padding-right: 17px; background-color: <cfif #hub.hub_header_bgcolor# is "">##000000<cfelse>#hub.hub_header_bgcolor#</cfif>;"><cfif grandchild_items.menu_icon is not "">#grandchild_items.menu_icon#<cfelse><i class="fa fa-square" aria-hidden="true" style="font-size: 10px;"></i></cfif> &nbsp;#grandchild_items.menu_name#</a>
									 </cfif>
							    </cfoutput>

							    </cfloop>

								  </ul>

							 </li>

                             </cfif>

                            </cfoutput>

                            </cfloop>

                            </ul>

						</cfif>

                     </li>

                     </cfloop>

					</div>

                </td>

                  <td width=20>&nbsp;</td>

                  <td align=right width=10>

				<div class="dropdown" style="cursor: pointer;">

				 <cfoutput>
				 <cfif #usr.usr_photo# is "">
				  <a href="/exchange/profile/edit.cfm"><img src="/images/headshot.png" style="border-radius: 180px;" width=35 height=35 border=0 title="Update Profile" alt="Update Profile"></a>
				 <cfelse>
				  <a href="/exchange/profile/edit.cfm"><img src="#media_virtual#/#usr.usr_photo#" width=35 height=35 border=0 title="Update Profile" alt="Update Profile" style="border-radius: 100px; border-style: solid; border-width: 2px; border-color: FFFFFF;"></a>
				 </cfif>
				 </cfoutput>

				  <div class="dropdown-content" style="top: 20; width: 225px; padding-left: 0px; margin-top: 2px;">
					  <a href="/exchange/profile/">&nbsp;<i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;My Account</a>
					  <cfif session.company_id is not 0>
					  	<a href="/exchange/company/">&nbsp;<i class="fa fa-building" aria-hidden="true"></i>&nbsp;&nbsp;My Company Profile</a>
					  </cfif>

					  <!---

				  	  <cfif ass.total GT 0>
				  	  	<a href="/exchange/company/profiles.cfm">&nbsp;<i class="fa fa-building" aria-hidden="true"></i>&nbsp;&nbsp;Company Associations</a>
				  	  </cfif> --->

					  <a href="/exchange/profile/notifications/index.cfm?l=3">&nbsp;<i class="fa fa-bell" aria-hidden="true"></i>&nbsp;&nbsp;Notifications</a>

					  <cfif hub.hub_library_id is 1>
					  <a href="https://exchangeanswers.zendesk.com/hc/en-us" target="_blank" rel="noopener">&nbsp;<i class="fa fa-question-circle"></i>&nbsp;&nbsp;Help & Support</a>
					  <cfelse>
					  <a href="/exchange/support/">&nbsp;<i class="fa fa-question-circle"></i>&nbsp;&nbsp;Help & Support</a>
					  </cfif>

					 <cfif hub.hub_show_app_library is 1>
					  <a href="/exchange/apps/">&nbsp;<i class="fa fa-th-large" aria-hidden="true"></i>&nbsp;&nbsp;App Library</a>
                     </cfif>

					 <cfif hub.hub_owner_id is #session.usr_id# or #hub_admin# is 1>
					 <hr>
						  <a href="/exchange/admin/"><i class="fa fa-fw fa-cog"></i>&nbsp;&nbsp;<cfif session.network is 0>Exchange<cfelse>Network</cfif> Settings</a>
					 </cfif>

				     <hr>

				     <a href="/exchange/logout.cfm"><i class="fa fa-fw fa-paper-plane" style="padding-right: 10px;"></i>&nbsp;&nbsp;Logout</a>

				  </div>
				</div>

				 </td>

     <td width=30>&nbsp;</td>

          </tr>

</table>

</div>

<style>
.space {
   margin-top: 60px;
   background-color: #d0d0d0;
}
</style>

<div class="space">&nbsp;</div>

