<cfquery name="university" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
select * from autm
where exchange_company_id = #session.company_profile_id#
</cfquery>

<cfquery name="labs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from lab_tech
 where lab_company_id = #session.company_profile_id#
</cfquery>

<cfset university_count = university.recordcount>
<cfset lab_count = labs.recordcount>
<cfset innovation_count = university_count + lab_count>

<cfoutput>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Innovations <cfif innovation_count GT 0>(#innovation_count#)</cfif></td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>
</cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfif #innovation_count# is 0>
 <tr><td class="feed_sub_header" style="font-weight: normal;">No Innovations were found.</td></tr>
<cfelse>

  <cfset counter = 0>

  <cfoutput query="university">

  <tr>
	 <td class="feed_sub_header"><a href="/exchange/research/universities/innovation.cfm?i=#inn_id#&u_name=no&r=no" target="_blank" rel="noopener" rel="noreferrer"><b>#projecttitle#</b></a></td>
  </tr>

  <tr>
	 <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#ab#</td>
  </tr>

  <tr>
	 <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#contactinformationname#<br>#contactinformationemail#<br>#contactinformationphone#</td>
  </tr>

  <tr><td><hr></td></tr>

  <cfif counter is 0>
   <cfset counter = 1>
  <cfelse>
   <cfset counter = 0>
  </cfif>

  </cfoutput>

  <cfoutput query="labs">

  <tr>
	 <td class="feed_sub_header"><a href="/exchange/include/labtech_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#availabletechnology#</b></a></td>
  </tr>

  <tr>
	 <td class="feed_sub_header" style="font-weight: normal; padding-top: 0px;">#shortdescription#</td>
  </tr>

 <cfif LaboratoryRepresentitive is not "">
 <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Point of Contact</b> - #LaboratoryRepresentitive#</td></tr>
 </cfif>


 <cfif RepresentativeTitle is not "">
 <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Title</b> - #RepresentativeTitle#</td></tr>
 </cfif>

 <cfif RepresentativeEmail is not "">
 <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Email</b> - #RepresentativeEmail#</td></tr>
 </cfif>

 <cfif phone is not "">
 <tr><td class="feed_sub_header" style="font-weight: normal; padding-bottom: 0px;"><b>Phone</b> - #phone#</td></tr>
 </cfif>

  <tr><td height=10></td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <cfif counter is 0>
   <cfset counter = 1>
  <cfelse>
   <cfset counter = 0>
  </cfif>

  </cfoutput>

</cfif>

</table>