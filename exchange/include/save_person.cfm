<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" style="background-color: FFFFFF">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<cfset session.person_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#>

<cfquery name="person" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from person
 where person_id = #session.person_id#
</cfquery>

<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing.sharing_group_id from sharing
 join sharing_group on sharing_group.sharing_group_id = sharing.sharing_group_id
 join sharing_group_usr on sharing_group_usr_group_id = sharing.sharing_group_id
 where sharing_hub_id = #session.hub# and
       sharing_access > 1 and
       sharing_group_usr_usr_id = #session.usr_id#
</cfquery>

<cfif groups.recordcount is 0>
 <cfset group_list = 0>
<cfelse>
 <cfset group_list = valuelist(groups.sharing_group_id)>
</cfif>

<cfquery name="port_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_person_portfolio_id from sharing
 where sharing_hub_id = #session.hub# and
       sharing_access > 1 and
       sharing_person_portfolio_id is not null and
       (sharing_to_usr_id = #session.usr_id# or sharing_group_id in (#group_list#))
 union
 select person_portfolio_id as sharing_person_portfolio_id from person_portfolio
 where person_portfolio_usr_id = #session.usr_id# and
       person_portfolio_hub_id = #session.hub#
</cfquery>

<cfif port_access.recordcount is 0>
 <cfset port_list = 0>
<cfelse>
 <cfset port_list = valuelist(port_access.sharing_person_portfolio_id)>
</cfif>

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select distinct(person_portfolio_item_portfolio_id), usr_first_name, usr_last_name, person_portfolio_name from person_portfolio_item
 join person_portfolio on person_portfolio_id = person_portfolio_item_portfolio_id
 join usr on usr_id = person_portfolio_usr_id
 where (person_portfolio_item_portfolio_id in (#port_list#) or person_portfolio_usr_id = #session.usr_id#) and
       person_portfolio_hub_id = #session.hub#
 group by person_portfolio_item_portfolio_id, usr_first_name, usr_last_name, person_portfolio_name
</cfquery>

<cfif check.recordcount is 0>
 <cfset check_list = 0>
<cfelse>
 <cfset check_list = valuelist(check.person_portfolio_item_portfolio_id)>
</cfif>

<cfquery name="person_portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from person_portfolio

 order by person_portfolio_name
</cfquery>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td height=20></td></tr>
 <tr><td class="feed_header">Add to Portfoio</td>
	 <td class="feed_header" align=right><img src="/images/delete.png" style="cursor: pointer;" alt="Close" title="Close" width=20 onclick="windowClose();"></td></tr>
 <tr><td colspan=2><hr></td></tr>
</table>

<cfoutput query="person">
	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td valign=top width=130>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			    <tr><td height=10></td></tr>
                <tr><td class="text_xsmall" align=center><img style="border-radius: 2px;" src="#person.person_photo#" width=100 height=100></td></tr>
 			   </table>

               </td><td valign=top>
			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
               <tr><td class="feed_header" style="padding-top: 10px;">#person_full_name#</a></td></tr>
               <tr><td class="feed_sub_header" style="font-size: 18px;">#person_company#</a></td></tr>
               <tr><td class="feed_sub_header">#person_title#</a></td></tr>

		   </table>
		   </td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=2><hr></td></tr>
     </table>
</cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <form action="save_person_portfolio.cfm" method="post">

     <tr>
	     <td class="feed_sub_header">Select Existing Portfolio</td>
         <td class="feed_sub_header">Or, Create a New Portfolio</td>
     </tr>

     <tr>
         <td width=450>
			 <select name="person_portfolio_item_portfolio_id" class="input_select" style="width: 400px;">
			 <cfoutput query="person_portfolio">
			  <option value=#person_portfolio_id#>#person_portfolio_name#
			 </cfoutput>
			 <select></td>
		<td><input type="text" name="person_portfolio_name" style="width: 350px;" class="input_text" placeholder="Please provide a name for this Portfolio."></td></tr>

	 <tr><td height=10></td></tr>

     <cfoutput>
     <input type="hidden" name="person_portfolio_item_person_id" value=#session.person_id#>
     </cfoutput>

     <tr><td colspan=2><hr></td></tr>

	 <tr><td colspan=2><input type="submit" name="button" class="button_blue_large" value="Add">

</form>

</table>

</center>

</body>
</html>

