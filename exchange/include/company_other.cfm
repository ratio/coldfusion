
<cfquery name="section_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from section
 where section_id = #session.section_selected#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<cfoutput>
<tr><td class="feed_header" style="font-size: 30;" valign=middle>#section_info.section_name#</td>
   <td align=right class="feed_sub_header"></td></tr>
<tr><td colspan=2><hr></td></tr>
</cfoutput>

</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfquery name="fields" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from section_field
			 where section_field_section_id = #session.section_selected#
			 order by section_field_order
		</cfquery>

		 <cfloop query="fields">

		 <cfquery name="getval" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from section_field_value
			 where section_field_value_section_id = #session.section_selected# and
				   section_field_value_field_id = #fields.section_field_id# and
				   section_field_value_company_id = #session.company_profile_id#
		 </cfquery>

		 <cfoutput>
		  <tr><td class="feed_sub_header">#fields.section_field_name#</td></tr>

		  <!--- Text Field --->
		  <cfif fields.section_field_type is 1>
			<cfif #getval.section_field_value_text# is not "">
			  <tr><td class="feed_sub_header" style="font-weight: normal;">#getval.section_field_value_text#</td></tr>
			<cfelse>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">No information has been entered.</td></tr>
			</cfif>
		  <!-- Memo Field --->
		  <cfelseif fields.section_field_type is 2>
			<cfif #getval.section_field_value_textarea# is not "">
			<tr><td class="feed_sub_header" style="font-weight: normal;">#replace(getval.section_field_value_textarea,"#chr(10)#","<br>","all")#</td></tr>
			<cfelse>
			  <tr><td class="feed_sub_header" style="font-weight: normal;">No information has been entered.</td></tr>
			</cfif>
		  </cfif>

		 </cfoutput>

		 <tr><td height=5></td></tr>
		 <tr><td><hr></td></tr>
		 <tr><td height=5></td></tr>

		 </cfloop>

	  </table>

