<cfinclude template="/exchange/security/check.cfm">

<cfquery name="history" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from award_data
 where award_id_piid = '#contractid#' and
 recipient_duns = '#duns#'
 order by action_date ASC, modification_number
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

	   <cfoutput>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_header">CONTRACT HISTORY</td>
				 <td class="feed_option" align=right><img src="/images/delete.png" width=20 style="cursor: pointer;" onclick="windowClose();"></td></tr>
			 <tr><td colspan=2><hr></td></tr>
		   </table>

       </cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr height=50>
             <td class="text_xsmall"><b>Award Date</b></td>
             <td class="text_xsmall"><b>Award ID</b></td>
             <td class="text_xsmall" align=center><b>Mod</b></td>
             <td class="text_xsmall"><b>Description</b></td>
             <td class="text_xsmall"><b>Action Type</b></td>
             <td class="text_xsmall" align=right><b>Obligated</b></td>
             <td class="text_xsmall" align=right><b>Base & Exercised Options</b></td>
             <td class="text_xsmall" align=right><b>Base & All Options</b></td>
         </tr>

       <cfset counter = 0>

       <cfset total_1 = 0>
       <cfset total_2 = 0>
       <cfset total_3 = 0>

       <cfoutput query="history">

       <cfif counter is 0>
        <tr bgcolor="ffffff" height=30>
       <cfelse>
        <tr bgcolor="e0e0e0" height=30>
       </cfif>
            <td class="text_xsmall" valign=middle width=75>#dateformat(action_date,'mm/dd/yyyy')#</td>
            <td class="text_xsmall" valign=middle width=125><a href="/exchange/include/award_information.cfm?id=#id#&duns=#duns#" target="_blank" rel="noopener"><b>#award_id_piid#</b></a></td>
            <td class="text_xsmall" valign=middle align=center width=75><a href="/exchange/include/award_information.cfm?id=#id#&duns=#duns#" target="_blank" rel="noopener"><b>#modification_number#</b></a></td>
            <td class="text_xsmall" valign=middle width=550>#award_description#</td>
            <td class="text_xsmall" valign=middle>#action_type#</td>
            <td class="text_xsmall" valign=middle align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
            <td class="text_xsmall" valign=middle align=right>#numberformat(base_and_exercised_options_value,'$999,999,999')#</td>
            <td class="text_xsmall" valign=middle align=right>#numberformat(base_and_all_options_value,'$999,999,999')#</td>
        </tr>

        <cfif counter is 0>
         <cfset counter = 1>
        <cfelse>
         <cfset counter = 0>
        </cfif>

        <cfif #federal_action_obligation# is not "">
          <cfset total_1 = total_1 + #federal_action_obligation#>
        </cfif>

        <cfif #base_and_exercised_options_value# is not "">
          <cfset total_2 = total_2 + #base_and_exercised_options_value#>
        </cfif>

        <cfif #base_and_all_options_value# is not "">
          <cfset total_3 = total_3 + #base_and_all_options_value#>
        </cfif>

       </cfoutput>

       <cfoutput>
       <tr><td class="text_xsmall" colspan=5 height=50><b>Total</b></td>
           <td class="text_xsmall" align=right><b>#numberformat(total_1,'$999,999,999')#</b></td>
           <td class="text_xsmall" align=right><b>#numberformat(total_2,'$999,999,999')#</b></td>
           <td class="text_xsmall" align=right><b>#numberformat(total_3,'$999,999,999')#</b></td>
       </tr>
       </cfoutput>

	  </div>

	  </td></tr>

  </table>

  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

