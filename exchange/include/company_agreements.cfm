<cfquery name="agreements" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from company_agreement
 join agreement on agreement_id = company_agreement_agreement_id
 where company_agreement_company_id = #session.company_profile_id# and
       company_agreement_hub_id = #session.hub#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=2 class="feed_header" style="font-size: 30;">Agreements</td>
       <td align=right class="feed_sub_header">

       </td></tr>

   <tr><td colspan=3><hr></td></tr>
   <tr><td height=20></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

<tr><td valign=top>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif #agreements.recordcount# is 0>
		 <tr><td class="feed_sub_header">No Agreements are available.</td></tr>
		<cfelse>

         <cfset counter = 0>

          <tr>
             <td class="feed_sub_header">Agreement</td>
             <td class="feed_sub_header">Context / Comments</td>
             <td class="feed_sub_header" align=center>Start Date</td>
             <td class="feed_sub_header" align=center>End Date</td>
             <td class="feed_sub_header" align=right>Last Updated</td>
          </tr>

          <cfoutput query="agreements">

          <cfif counter is 0>
           <tr bgcolor="ffffff">
          <cfelse>
           <tr bgcolor="e0e0e0">
          </cfif>

              <td class="feed_sub_header" style="font-weight: normal;"><b>#agreement_name#</b></td>
              <td class="feed_sub_header" style="font-weight: normal;">#company_agreement_comments#</b></td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#dateformat(company_agreement_start_date,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=center>#dateformat(company_agreement_end_date,'mm/dd/yyyy')#</td>
              <td class="feed_sub_header" style="font-weight: normal;" align=right>#dateformat(company_agreement_updated,'mm/dd/yyyy')#</td>

            </tr>

            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
          </cfoutput>

		</cfif>

     </table>

</td></tr>

</table>
