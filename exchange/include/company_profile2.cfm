<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("id")>

	<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select company_logo, company_website, company_id, company_registered, company_duns, company_name from company
	  where company_id = #id#
	</cfquery>

<cfelse>

	<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select company_logo, company_website, company_id, company_registered, company_duns, company_name from company
	  where company_duns = '#duns#'
	</cfquery>

	<cfset id = #company.company_id#>

</cfif>

<html>
<head>
	<title><cfoutput>#company.company_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=4" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

 <cfinclude template = "/exchange/include/header.cfm">

 <div class="main_box">
  <cfinclude template="/exchange/include/profile_header.cfm">
 </div>

 <div class="main_box">
  <cfinclude template="/exchange/include/exchange_profile.cfm">
 </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

