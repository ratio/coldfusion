<cfquery name="billing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select recipient_name,
       recipient_duns,
       award_id_piid,
	   awarding_agency_name,
	   awarding_sub_agency_name,
	   type_of_contract_pricing,
       period_of_performance_start_date as start_billing,
       period_of_performance_current_end_date as end_billing,
       datediff(m, period_of_performance_start_date, period_of_performance_current_end_date) as months_billing,
	   (federal_action_obligation / datediff(m, period_of_performance_start_date, period_of_performance_current_end_date)) as monthly_contract_revenue
	   from award_data
where recipient_duns = '#session.company_profile_duns#' and
      federal_action_obligation > 0 and
      datediff(m, period_of_performance_start_date, period_of_performance_current_end_date) > 0
order by period_of_performance_start_date
</cfquery>

<table cellspacing=0 cellpadding=0 border=1 width=100%>
<tr>
   <td>Contractor</td>
   <td>Contract</td>
   <td>Department</td>
   <td>Agency</td>
   <td>Price Type</td>
   <td>Bill Date</td>
   <td align=right>Monthly Revenue</td>
   <td align=right>Profit</td>
</tr>

<cfloop query="billing">

 <cfif billing.type_of_contract_pricing is "Firm Fixed Price">
  <cfset profit_percentage = .25>
 <cfelseif billing.type_of_contract_pricing is "Time and Materials">
  <cfset profit_percentage = .10>
 <cfelseif billing.type_of_contract_pricing is "Fixed Price">
  <cfset profit_percentage = .25>
 <cfelseif billing.type_of_contract_pricing is "Labor Hours">
  <cfset profit_percentage = .10>
 <cfelseif billing.type_of_contract_pricing is "Cost Plus">
  <cfset profit_percentage = .05>
 <cfelseif billing.type_of_contract_pricing is "Cost Plus Award Fee">
  <cfset profit_percentage = .07>
 <cfelseif billing.type_of_contract_pricing is "Cost Plus Fixed Fee">
  <cfset profit_percentage = .07>
 <cfelseif billing.type_of_contract_pricing is "Cost Plus Incentive">
  <cfset profit_percentage = .07>
 <cfelseif billing.type_of_contract_pricing is "Cost Plus Incentive Fee">
  <cfset profit_percentage = .07>
 <cfelse>
  <cfset profit_percentage = 0>
 </cfif>


 <cfoutput>
 <tr>
    <td>#billing.recipient_name#</td>
    <td>#award_id_piid#</td>
    <td>#awarding_agency_name#</td>
    <td>#awarding_sub_agency_name#</td>
    <td>#type_of_contract_pricing#</td>
    <td>#dateformat(billing.start_billing,'mm/dd/yyyy')#</td>
    <td align=right>#numberformat(billing.monthly_contract_revenue,'$999,999,999')#</td>
    <td align=right><cfif profit_percentage is 0>Unknown<cfelse>#numberformat(evaluate(billing.monthly_contract_revenue * profit_percentage),'$999,999')#</cfif></td>
 </tr>
 </cfoutput>

 <cfloop index="LoopCount" from="2" to = "#billing.months_billing#">

	 <cfoutput>

	 <tr>
		<td>#billing.recipient_name#</td>
	    <td>#award_id_piid#</td>
	    <td>#awarding_agency_name#</td>
	    <td>#awarding_sub_agency_name#</td>
	    <td>#type_of_contract_pricing#</td>
		<td>#dateformat(dateadd("m", loopcount, billing.start_billing),'mm/dd/yyyy')#</td>
		<td align=right>#numberformat(billing.monthly_contract_revenue,'$999,999,999')#</td>
	    <td align=right><cfif profit_percentage is 0>Unknown<cfelse>#numberformat(evaluate(billing.monthly_contract_revenue * profit_percentage),'$999,999')#</cfif></td>
	 </tr>

	 </cfoutput>

 </cfloop>

</cfloop>

</table>