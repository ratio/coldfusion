<cfinclude template="/exchange/security/check.cfm">

<cfquery name="sams" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from sams
 where duns = '#session.company_profile_duns#'
</cfquery>

<html>
<head>
	<title><cfoutput>#ucase(sams.legal_business_name)#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet"></head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

           <cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td class="feed_header" valign=top>EXTENDED AWARDS</a></td>
                <td class="feed_option" align=right><a href="/exchange/include/profile.cfm?l=10"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
            <tr><td colspan=2><hr></td></tr>
           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr><td>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				    <tr><td valign=top width=25%>

							<table cellspacing=0 cellpadding=0 border=0 width=100%>
							<tr><td class="feed_option"><b>Company</b></td></tr>
							<tr><td class="feed_option">#sams.legal_business_name#<br><a href="#sams.corp_url#" target="_blank" rel="noopener" rel="noreferrer">#sams.corp_url#</a></td></tr>
							</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>

						<tr><td class="feed_option"><b>Address</b></td></tr>
						<tr><td class="feed_option"><cfif #sams.phys_address_l1# is not "">#sams.phys_address_l1#<br></cfif>
													<cfif #sams.phys_address_line_2# is not "">#sams.phys_address_line_2#<br></cfif>
													#sams.city_1# #sams.state_1#, #sams.zip_1#<br>
													</td></tr>
						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>

						<tr><td class="feed_option"><b>Important Dates</b></td></tr>
						<tr><td class="feed_option">
                                                    <b>Founded: </b>#mid(sams.biz_start_date,5,2)#/#right(sams.biz_start_date,2)#/#left(sams.biz_start_date,4)#<br>
						                            <b>Registration Date: </b>#mid(sams.init_reg_date,5,2)#/#right(sams.init_reg_date,2)#/#left(sams.init_reg_date,4)#<br>
						                            <b>Activation Date: </b>#mid(sams.activation_date,5,2)#/#right(sams.activation_date,2)#/#left(sams.activation_date,4)#<br>
						                            <b>Expiration Date: </b>#mid(sams.reg_exp_date,5,2)#/#right(sams.reg_exp_date,2)#/#left(sams.reg_exp_date,4)#<br>
						                            <b>Last Updated: </b>#mid(sams.last_update,5,2)#/#right(sams.last_update,2)#/#left(sams.last_update,4)#

						                            </td></tr>

						</table>

                    </td><td valign=top width=25%>

						<table cellspacing=0 cellpadding=0 border=0 width=100%>

						<tr><td class="feed_option"><b>Corporate Information</b></td></tr>


						<tr><td class="feed_option">
						                            <b>DUNS: </b>#sams.duns#<cfif #sams.duns_4# is not "">-#sams.duns_4#</cfif><br>
                                                    <b>State of Incorporation: </b>#sams.state_of_inc#<br>
                                                    <b>Cage Code: </b>#sams.cage_code#

						                            </td></tr>


						</table>

                    </td></tr>

                   <tr><td colspan=4><hr></td></tr>

                   </table>

              </td></tr>

            </table>

        </cfoutput>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				 select id, modification_number, award_description, product_or_service_code_description, awarding_sub_agency_name, action_date,award_id_piid,awarding_agency_name,awarding_office_name,naics_code,type_of_contract_pricing,type_of_set_aside,period_of_performance_start_date,period_of_performance_potential_end_date,federal_action_obligation from award_data
				 where recipient_duns = '#session.company_profile_duns#'

                 <cfif isdefined("session.keywords")>
                  and award_description like '%#session.keywords#%'
                 </cfif>

				   <cfif isdefined("sv")>

					<cfif #sv# is 1>
					 order by action_date DESC
					<cfelseif #sv# is 10>
					 order by action_date ASC
					<cfelseif #sv# is 2>
					 order by award_id_piid, modification_number ASC
					<cfelseif #sv# is 20>
					 order by award_id_piid, modification_number DESC
					<cfelseif #sv# is 3>
					 order by product_or_service_code ASC
					<cfelseif #sv# is 30>
					 order by product_or_service_code DESC
					<cfelseif #sv# is 4>
					 order by awarding_sub_agency_name ASC
					<cfelseif #sv# is 40>
					 order by awarding_sub_agency_name DESC
					<cfelseif #sv# is 5>
					 order by naics_code ASC
					<cfelseif #sv# is 50>
					 order by naics_code DESC
					<cfelseif #sv# is 6>
					 order by type_of_contract_pricing ASC
					<cfelseif #sv# is 60>
					 order by type_of_contract_pricing DESC
					<cfelseif #sv# is 7>
					 order by type_of_set_aside ASC
					<cfelseif #sv# is 70>
					 order by type_of_set_aside DESC
					<cfelseif #sv# is 8>
					 order by period_of_performance_start_date ASC
					<cfelseif #sv# is 80>
					 order by period_of_performance_start_date DESC
					<cfelseif #sv# is 9>
					 order by period_of_performance_potential_end_date ASC
					<cfelseif #sv# is 90>
					 order by period_of_performance_potential_end_date DESC
					<cfelseif #sv# is 11>
					 order by federal_action_obligation ASC
					<cfelseif #sv# is 110>
					 order by federal_action_obligation DESC
					</cfif>

				   <cfelse>
					 order by action_date DESC
				   </cfif>

				</cfquery>

			    <cfparam name="URL.PageId" default="0">
			    <cfset RecordsPerPage = 250>
			    <cfset TotalPages = (awards.Recordcount/RecordsPerPage)>
			    <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
			    <cfset EndRow = StartRow+RecordsPerPage-1>

 		        <tr><td class="feed_sub_header" colspan=11>FEDERAL PRIME CONTRACT AWARDS</td>
 		            <td align=right>

                   <form action="refresh.cfm" method="post">
                    <span class="feed_sub_header">Search Awards: </span>&nbsp;<input class="input_text" name="keywords" type="text" required <cfif isdefined("session.keywords")>value="<cfoutput>#session.keywords#</cfoutput>"</cfif>>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Search">&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Show All">
                    <cfoutput>
                    <input type="hidden" name="id" value=#id#>
                    </cfoutput>
                   </form>

 		            </td></tr>
			    <tr><td height=5></td></tr>

			    </table>

			    <table cellspacing=0 cellpadding=0 border=0 width=100%>

			    <cfoutput>
                <tr><td class="feed_option" colspan=2><b>Total Awards - #numberformat(awards.recordcount,'999,999')#</b></td>
                    <td class="feed_option" align=right colspan=9>

				  <cfif awards.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&id=0<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>

                    </td>

                </tr>
                </cfoutput>
			    <tr><td>&nbsp;</td></tr>

				<tr><td>

                                <cfoutput>
									<tr height=30>
									   <td class="text_xsmall" width=75><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATA</b></a></td>
									   <td class="text_xsmall"><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CONTRACT ##</b></a></td>
									   <td class="text_xsmall"><b>MOD</b></td>
									   <td class="text_xsmall"><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AGENCY</b></a></td>
									   <td class="text_xsmall"><b>DESCRIPTION</b></td>
									   <td class="text_xsmall"><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PRODUCT OR SERVICE</b></a></td>
									   <td class="text_xsmall"><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>NAICS</b></a></td>
									   <td class="text_xsmall" align=center><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>PRICING</b></a></td>
									   <td class="text_xsmall"><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>POP START</b></a></td>
									   <td class="text_xsmall"><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>POP END</b></a></td>
									   <td class="text_xsmall" align=right><a href="extended_awards.cfm?id=#id#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>OBLIGATION</b></a></td>
									</tr>
								</cfoutput>

								<cfset #total_value# = 0>

								<cfset #counter# = 0>

								<cfloop query="awards">

						        <cfif CurrentRow gte StartRow >

								  <tr height=30

								  <cfif #counter# is 0>
								   bgcolor="ffffff"
								  <cfelse>
								   bgcolor="e0e0e0"
								  </cfif>


								  >

								  <cfoutput>

									 <td class="text_xsmall">#dateformat(awards.action_date,'mm/dd/yyyy')#</td>
									 <td class="text_xsmall"><a href="/exchange/include/award_information.cfm?id=#awards.id#" target="_blank" rel="noopener" rel="noreferrer"><b>#awards.award_id_piid#</b></a></td>
									 <td class="text_xsmall">#awards.modification_number#</td>
									 <td class="text_xsmall">#awards.awarding_sub_agency_name#</td>
									 <td class="text_xsmall" width=300>



									   <cfif isdefined("session.keywords")>
										#replaceNoCase(award_description,session.keywords,"<span style='background:yellow'>#ucase(session.keywords)#</span>","all")#
									   <cfelse>
										#award_description#
									   </cfif>



									 #awards.award_description#</td>
									 <td class="text_xsmall">#awards.product_or_service_code_description#</td>
									 <td class="text_xsmall">#awards.naics_code#</td>
									 <td class="text_xsmall" align=center>
									 <cfif awards.type_of_contract_pricing is "Firm Fixed Price">
									  FFP
									 <cfelseif awards.type_of_contract_pricing is "Cost Plus Fixed Fee">
									  CPFF
									 <cfelseif awards.type_of_contract_pricing is "Time and Materials">
									  T&M
									 <cfelseif awards.type_of_contract_pricing is "Cost Plus Award Fee">
									  CPAF
									 <cfelse>
									  #left(awards.type_of_contract_pricing,21)#
									 </cfif>
									 </td>
									 <td class="text_xsmall" width=75>#dateformat(awards.period_of_performance_start_date,'mm/dd/yy')#</td>
									 <td class="text_xsmall" width=75>#dateformat(awards.period_of_performance_potential_end_date,'mm/dd/yy')#</td>
									 <td class="text_xsmall" width=50 align=right>#numberformat(awards.federal_action_obligation,'$999,999,999')#</td>

								  </cfoutput>

								  </tr>

								  <cfset #total_value# = #total_value# + #awards.federal_action_obligation#>

								  <cfif #counter# is 0>
								   <cfset #counter# = 1>
								  <cfelse>
								   <cfset #counter# = 0>
								  </cfif>

								  </cfif>

								  <cfif CurrentRow eq EndRow>
								   <cfbreak>
								  </cfif>

								</cfloop>

								<tr><td class="feed_option" colspan=10><b>Total:</b></td>

								<cfoutput>
									<td class="feed_option" align=right><b>#numberformat(total_value,'$999,999,999')#</b></td>
								</cfoutput>

                 </tr>
            </table>
          </td></tr>
        </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

