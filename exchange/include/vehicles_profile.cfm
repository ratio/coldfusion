
		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr><td>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td height=8></td></tr>

			   <tr><td colspan=15><hr></td></tr>

			   <tr><td class="feed_header" colspan=5>Federal Contract Vehicles</td></tr>
			   <tr><td height=5></td></tr>

				<!--- Contract Vehicles --->

                <cfif company.company_duns is "">

					<cfquery name="vehicles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select distinct(source), contractno, email, phone, vlookup_agency,contract_end_date from vehicles
					 left join vlookup on vlookup.vlookup_vehicle_abbr = vehicles.source
					 where duns = '0'
					</cfquery>

                <cfelse>

					<cfquery name="vehicles" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
					 select distinct(source), contractno, email, phone, vlookup_agency,contract_end_date from vehicles
					 left join vlookup on vlookup.vlookup_vehicle_abbr = vehicles.source
					 where duns = '#company.company_duns#'
					</cfquery>

			    </cfif>

                <cfif subscription.premium_vehicle is 1 or (isdefined("session.hub") and hub_subscription.premium_vehicle is 1)>

							<cfif #vehicles.recordcount# is 0>

									<tr><td class="feed_option">No Federal contract vehicles were found.</td></tr>

							<cfelse>

			                        <tr><td class="feed_option" colspan=3><b>Prime Contract Vehicles</b><cfif #vehicles.recordcount# GT 0>&nbsp;-&nbsp;<cfoutput>#vehicles.recordcount#</cfoutput> Vehicles</cfif></td></tr>

									<tr><td height=10></td></tr>

									<cfoutput>
										<tr>
										   <td class="feed_option" width=75><b>Agency</b></a></td>
										   <td class="feed_option" width=75><b>Vehicle</b></a></td>
										   <td class="feed_option"><b>Vehicle Contract Number</b></a></td>
										   <td class="feed_option"><b>Contact Email</b></a></td>
										   <td class="feed_option"><b>Contact Number</b></a></td>
										   <td class="feed_option" align=right><b>Expiration Date</b></a></td>
										   <td class="feed_option" align=center><b>Awards</b></a></td>
										   <td class="feed_option" align=right><b>Award Value</b></a></td>
										</tr>
									</cfoutput>

									<cfset #counter# = 0>

									<cfloop query="vehicles">

                                        <cfset #contract_number# = #replace(vehicles.contractno,"-","","all")#>

										<cfquery name="vehicle_awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
										 select count(id) as awards, sum(federal_action_obligation) as total from award_data
										 where recipient_duns = '#company.company_duns#' and
										       parent_award_id = '#contract_number#'
										</cfquery>

									  <cfoutput>

									  <tr

									  <cfif #counter# is 0>
									   bgcolor="ffffff"
									  <cfelse>
									   bgcolor="e0e0e0"
									  </cfif>

									  >
										 <td class="feed_option">#vehicles.vlookup_agency#</td>
										 <td class="feed_option">#vehicles.source#</td>
										 <td class="feed_option"><a href="vehicle_awards.cfm?duns=#company.company_duns#&contract=#vehicles.contractno#">#vehicles.contractno#</a></td>
										 <td class="feed_option">#vehicles.email#</td>
										 <td class="feed_option">#vehicles.phone#</td>
										 <td class="feed_option" align=right>#dateformat(vehicles.contract_end_date,'mm/dd/yyyy')#</td>
										 <td class="feed_option" align=center width=100>#vehicle_awards.awards#</td>
										 <td class="feed_option" align=right width=100>#numberformat(vehicle_awards.total,'$999,999,999')#</td>

									  </tr>

									  <cfif #counter# is 0>
									   <cfset #counter# = 1>
									  <cfelse>
									   <cfset #counter# = 0>
									  </cfif>

									</cfoutput>

									</cfloop>

									  <tr><td height=10></td></tr>
									  <tr><td colspan=12 class="feed_option"><b>Note - </b> We are constantly adding new contract vehicles as they are discovered and reported.  This list may not be complete.  See <a href="/exchange/vehicles/"><b>Vehicles</b></a> for a current list of Contract Vehicles we are tracking.</td></tr>

								</cfif>

			        <cfelse>

			          <tr><td class="feed_option">To get a complete list of vendors contract vehices this requires a premium service.</td></tr>

			        </cfif>

				   </table>

			   </td></tr>

			</table>