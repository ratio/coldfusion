<cfinclude template="/exchange/security/check.cfm">

<cfquery name="insert_recent" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert recent(recent_opp_grant_id, recent_usr_id, recent_usr_company_id, recent_hub_id, recent_date)
 values('#id#',#session.usr_id#,#session.company_id#,<cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,#now()#)
</cfquery>

<cfquery name="grant" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from opp_grant
 where opp_grant_id = '#id#'
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header">Grant Information</td>
             <td align=right>
             <cfoutput>

			<div class="dropdown" style="cursor: pointer;">
			  <img src="/images/3dots2.png" style="cursor: pointer; padding-left: 10px;" height=10>
			  <div class="dropdown-content" style="top: 5; width: 250px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px; padding-top: 0px; margin-top: 2px;">
				<a href="##"  onclick="window.open('/exchange/include/save_opp.cfm?id=#grant.opp_grant_id#&t=grant','targetWindow','toolbar=no,location=no,status=no,menubar=no,left=400, top=50, scrollbars=yes,resizable=yes,width=600,height=575'); return false;"><i class="fa fa-thumb-tack" aria-hidden="true" style="padding-right: 10px;"></i>Pin to Opportunity Board</a>
				<a href="#cgi.http_referer#" alt="Close" title="Close" style="cursor: pointer;"><i class="fa fa-times" aria-hidden="true" style="padding-right: 10px;"></i>Close</a>
			  </div>
			</div>

             </cfoutput></td></tr>
         <tr><td height=10></td></tr>
         <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td valign=top>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
			 <tr><td class="feed_sub_header" width=250>DEPARTMENT</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#ucase(grant.department)#</td></tr>
			 <tr><td class="feed_sub_header">AGENCY</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#ucase(grant.agencyname)#</td></tr>
			 <tr><td class="feed_sub_header">OPPORTUNITY NUMBER</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#grant.opportunitynumber#</td></tr>
			 <tr><td class="feed_sub_header">CFDA NUMBER</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#grant.cfdanumber#</td></tr>
			 <tr><td class="feed_sub_header">FUNDING OPPORTUNITY ##</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#grant.fundingopportunitynumber#</td></tr>
			 <tr><td class="feed_sub_header">FUNDING TYPE</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#grant.fundinginstrumenttype#</td></tr>
			 <tr><td class="feed_sub_header">COST SHARING / MATCHING</td>
			     <td class="feed_sub_header" style="font-weight: normal;">#grant.costsharingormatchingrequirement#</td></tr>
         </table>

         </td><td width=50>&nbsp;</td><td valign=top width=30%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <tr><td class="feed_sub_header">POSTED DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(grant.posteddate,'mm/dd/yyyy')#</td></tr>
				 <tr><td class="feed_sub_header">CLOSE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(grant.closedate,'mm/dd/yyyy')#</td></tr>
				 <tr><td class="feed_sub_header">ARCHIVE DATE</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(grant.archivedate,'mm/dd/yyyy')#</td></tr>
				 <tr><td class="feed_sub_header">LAST UPDATED</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#dateformat(grant.lastupdate,'mm/dd/yyyy')#</td></tr>
				 <tr><td class="feed_sub_header">AWARD CEILING</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#numberformat(grant.awardceiling,'$999,999,999')#</td></tr>
				 <tr><td class="feed_sub_header">AWARD FLOOR</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#numberformat(grant.awardfloor,'$999,999,999')#</td></tr>
				 <tr><td class="feed_sub_header">EST ## OF AWARDS</td>
					 <td class="feed_sub_header" style="font-weight: normal;">#numberformat(grant.expectednumberofawards,'9,999')#</td></tr>


			 </table>

         </td></tr>

         <tr><td colspan=3><hr></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>


		 <tr><td class="feed_sub_header">VERSION</td></tr>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">#grant.version#</td></tr>

		 <tr><td class="feed_sub_header">TITLE</td></tr>
		 <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.grant_keyword")>
			  #replaceNoCase(grant.opportunitytitle,session.grant_keyword,"<span style='background:yellow'>#session.grant_keyword#</span>","all")#
			 <cfelse>
			 #grant.opportunitytitle#
			 </cfif>

		 </td></tr>
         <tr><td class="feed_sub_header">DESCRIPTION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

			 <cfif isdefined("session.grant_keyword")>
			  #replaceNoCase(grant.description,session.grant_keyword,"<span style='background:yellow'>#session.grant_keyword#</span>","all")#
			 <cfelse>
			 #grant.description#
			 </cfif>

         </td></tr>


         <tr><td><hr></td></tr>
         <tr><td class="feed_sub_header">ELIGIBLE PARTICIPANTS</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">#grant.eligibleapplicants#</td></tr>

         <tr><td class="feed_sub_header">ADDITIONAL ELIGIBILITY INFORMATION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">
         <cfif grant.additionalinformationeligibility is "">
          None Provided
         <cfelse>
         #grant.additionalinformationeligibility#
         </cfif>
         </td></tr>


         <tr><td><hr></td></tr>
         <tr><td class="feed_sub_header">CONTACT INFORMATION</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">

         <cfif grant.grantorcontactinformation is not "">
          <b>Name - </b>&nbsp;#grant.grantorcontactinformation#<br>
         </cfif>

         <cfif grant.grantorcontactinformationtitle is not "">
          <b>Title - </b>&nbsp;#grant.grantorcontactinformationtitle#<br>
         </cfif>

         <cfif grant.grantorcontactinformationphone is not "">
          <b>Phone - </b>&nbsp;#grant.grantorcontactinformationphone#<br>
         </cfif>

         </td></tr>

         <tr><td><hr></td></tr>

         <tr><td class="feed_sub_header">REFERENCING URL</td></tr>

         <cfif grant.linktoadditionalinformation is not "">
          <tr><td class="feed_sub_header" style="font-weight: normal;"><a href="#grant.linktoadditionalinformation#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><u>#grant.linktoadditionalinformation#</u></a></td></tr>
         <cfelse>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No reference was provided.</td></tr>
         </cfif>

       </table>

       </cfoutput>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

