<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Follow">

	<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert into follow
	  (follow_company_id, follow_by_usr_id, follow_date)
	  values
	  (#id#,#session.usr_id#,#now()#)
	</cfquery>

	<cflocation URL="/exchange/include/company_profile.cfm?id=#id#&u=1" addtoken="no">

<cfelseif #button# is "Unfollow">

	<cfquery name="unfollow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete follow
	  where follow_company_id = #id# and
	        follow_by_usr_id = #session.usr_id#
	</cfquery>

	<cflocation URL="/exchange/include/company_profile.cfm?id=#id#&u=0" addtoken="no">

</cfif>

