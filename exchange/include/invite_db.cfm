<cfinclude template="/exchange/security/check.cfm">

<cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sams
 where duns = '#duns#'
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #session.usr_id#
</cfquery>

<cfoutput>

<cfif isdefined("email_other")>

<cfif #email_name# is "" or #email_address# is "">
 <cflocation URL="invite.cfm?u=1&duns=#duns#">
</cfif>

<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into invite
 (invite_name, invite_email, invite_date, invite_duns)
 values
 ('#email_name#','#email_address#',#now()#,'#duns#')
</cfquery>

<cfmail from="EXCHANGE <noreply@ratio.exchange>"
		  to="#email_address#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="EXCHANGE - Invitation Request from #info.usr_first_name# #info.usr_last_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head><div class="center">
<body class="body">

<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
 <cfoutput>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(email_message,"#chr(10)#","<br>","all")#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><a href="https://www.ratio.exchange"><b>RATIO Exchange</b></a></td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">This message was sent to you by #info.usr_first_name# #info.usr_last_name#.</td></tr>
 <tr><td>&nbsp;</td></tr>
 </cfoutput>
</table>
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="223a60">
 <tr><td height=75 valign=middle><a href="https://www.ratio.exchange"><img src="http://go.ratio.exchange/images/exchange_logo.png" width=160 hspace=10 border=0></a></td></tr>
</table>
</body>
</html>

</cfmail>

</cfif>

<cfif isdefined("poc_email")>

<cfmail from="EXCHANGE <noreply@ratio.exchange>"
		  to="#company.poc_fnme# #company.poc_lname# <#company.poc_email#>"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="EXCHANGE - Invitation Request from #info.usr_first_name# #info.usr_last_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head><div class="center">
<body class="body">

<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
 <cfoutput>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(email_message,"#chr(10)#","<br>","all")#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><a href="https://www.ratio.exchange"><b>RATIO Exchange</b></a></td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">This message was sent to you by #info.usr_first_name# #info.usr_last_name#.</td></tr>
 <tr><td>&nbsp;</td></tr>
 </cfoutput>
</table>
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
 <tr><td height=75 valign=middle><a href="https://www.ratio.exchange"><img src="#image_virtual#/exchange_logo.png" width=160 hspace=10 border=0></a></td></tr>
</table>
</body>
</html>

</cfmail>

</cfif>

<cfif isdefined("alt_poc_email")>

<cfmail from="EXCHANGE <noreply@ratio.exchange>"
		  to="#company.alt_poc_fname# #company.alt_poc_lname# <#company.alt_poc_email#>"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="EXCHANGE - Invitation Request from #info.usr_first_name# #info.usr_last_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head><div class="center">
<body class="body">

<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
 <cfoutput>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(email_message,"#chr(10)#","<br>","all")#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><a href="https://www.ratio.exchange"><b>RATIO Exchange</b></a></td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">This message was sent to you by #info.usr_first_name# #info.usr_last_name#.</td></tr>
 <tr><td>&nbsp;</td></tr>
 </cfoutput>
</table>
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
 <tr><td height=75 valign=middle><a href="https://www.ratio.exchange"><img src="#image_virtual#/exchange_logo.png" width=160 hspace=10 border=0></a></td></tr>
</table>
</body>
</html>

</cfmail>

</cfif>

<cfif isdefined("pp_poc_email")>

<cfmail from="EXCHANGE <noreply@ratio.exchange>"
		  to="#company.pp_poc_fname# #company.pp_poc_lname# <#company.pp_poc_email#>"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="EXCHANGE - Invitation Request from #info.usr_first_name# #info.usr_last_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head><div class="center">
<body class="body">

<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
 <cfoutput>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(email_message,"#chr(10)#","<br>","all")#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><a href="https://www.ratio.exchange"><b>RATIO Exchange</b></a></td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">This message was sent to you by #info.usr_first_name# #info.usr_last_name#.</td></tr>
 <tr><td>&nbsp;</td></tr>
 </cfoutput>
</table>
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
 <tr><td height=75 valign=middle><a href="https://www.ratio.exchange"><img src="#image_virtual#/exchange_logo.png" width=160 hspace=10 border=0></a></td></tr>
</table>
</body>
</html>

</cfmail>

</cfif>

<cfif isdefined("elec_bus_poc_email")>

<cfmail from="EXCHANGE <noreply@ratio.exchange>"
		  to="#company.elec_bus_poc_fname# #company.elec_bus_poc_lnmae# <#company.elec_bus_poc_email#>"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="EXCHANGE - Invitation Request from #info.usr_first_name# #info.usr_last_name#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head><div class="center">
<body class="body">

<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
 <cfoutput>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(email_message,"#chr(10)#","<br>","all")#</td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><a href="https://www.ratio.exchange"><b>RATIO Exchange</b></a></td></tr>
 <tr><td>&nbsp;</td></tr>
 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">This message was sent to you by #info.usr_first_name# #info.usr_last_name#.</td></tr>
 <tr><td>&nbsp;</td></tr>
 </cfoutput>
</table>
<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
 <tr><td height=75 valign=middle><a href="https://www.ratio.exchange"><img src="#image_virtual#/exchange_logo.png" width=160 hspace=10 border=0></a></td></tr>
</table>

</body>
</html>

</cfmail>

</cfif>

<cflocation URL="/exchange/include/profile.cfm?id=#id#&u=3" addtoken="no">

</cfoutput>
