<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save">

 <cfif portfolio_name is not "">

 <cftransaction>

	<cfquery name="insert_port" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into portfolio
	 (portfolio_type_id, portfolio_name, portfolio_usr_id, portfolio_company_id, portfolio_hub_id, portfolio_updated)
	 values
	 (#portfolio_type_id#, '#portfolio_name#',#session.usr_id#,#session.company_id#,<cfif not isdefined("session.hub")>null<cfelse>#session.hub#</cfif>,#now()#)
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(portfolio_id) as id from portfolio
	</cfquery>

	<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      insert into portfolio_item
      (portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_value_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
      values
      (#portfolio_item_type_id#, #max.id#,#portfolio_item_award_id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
    </cfquery>

 </cftransaction>

 <cfelse>

 	<cfquery name="insert_item" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
       insert into portfolio_item
       (portfolio_item_type_id, portfolio_item_portfolio_id, portfolio_item_value_id, portfolio_item_usr_id, portfolio_item_updated, portfolio_item_comments)
       values
       (#portfolio_item_type_id#, #portfolio_item_portfolio_id#,#portfolio_item_award_id#,#session.usr_id#,#now()#,'#portfolio_item_comments#')
    </cfquery>

 </cfif>

</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body" bgcolor="ffffff">

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

<center>
<table cellspacing=0 cellpadding=0 border=0 width=95%>

         <tr><td height=20></td></tr>
         <tr><td class="feed_header">Award has been saved to your Portfolio</td></tr>
         <tr><td>&nbsp;</td></tr>
         <tr><td class="feed_header"><input class="button_blue_large" type="button" value="Close" onclick="windowClose();"></td></tr>
         <tr><td height=10></td></tr>

</table>

</body>
</html>


