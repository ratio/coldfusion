<cfset area_id = 1>

<cfquery name="brokers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from broker_callout
 join company on company_id = broker_callout_company_id
 left join broker on broker_company_id = company_id
 where broker_callout_area_id = #area_id#

 <cfif isdefined("session.hub")>
  and broker_callout_hub_id = #session.hub#
 <cfelse>
  and broker_callout_hub_id is null
 </cfif>
 order by broker_callout_order
</cfquery>

<cfif brokers.recordcount GT 0>

	<div class="left_box">

	  <center>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header" colspan=2>PREFERRED BROKERS</td></tr>
	  <tr><td height=5></td></tr>
	  <tr><td colspan=2><hr></td></tr>

	  <cfset count = 1>

	  <cfoutput query="brokers">
		  <tr><td width=50 valign=middle>
					<a href="/exchange/include/company_profile.cfm?id=#brokers.company_id#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif brokers.company_logo is "">
					  <img src="//logo.clearbit.com/#brokers.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
					  <img src="#media_virtual#/#brokers.company_logo#" width=40 border=0>
					</cfif>
					</a>
			  </td>
			  <td class="link_med_blue" valign=middle><a href="/exchange/include/company_profile.cfm?id=#brokers.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(company_name)#</a></td>
			  </tr>

			  <tr><td></td><td class="link_small_gray">#brokers.broker_tagline#</td></tr>

		  <cfif count is not brokers.recordcount>
		   <tr><td colspan=3><hr></td></tr>
		  </cfif>
		  <cfset count = count + 1>
	  </cfoutput>

	  </table>

	  </center>

	</div>

</cfif>
