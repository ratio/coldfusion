<cfquery name="private" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from product
 where product_company_id = #session.company_profile_id#
 and product_public = 1
 order by product_order
</cfquery>

<cfquery name="public" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from product
 where product_company_id = #session.company_profile_id#
 and product_public = 1
 order by product_order
</cfquery>

<cfset product_total = private.recordcount + public.recordcount>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
   <tr><td colspan=3 class="feed_header" style="font-size: 30;">Products & Services</td></tr>
   <tr><td colspan=3><hr></td></tr>
   <tr><td height=10></td></tr>
</table>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Local Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been approved by Companies for use on the <cfoutput>#session.network_name#</cfoutput>.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <cfif private.recordcount is 0>
   <tr><td class="feed_sub_header">No information was found.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="private">

      <tr><td valign=top width=120>
		   <cfif #private.product_attachment# is "">
		   <img src="/images/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#private.product_attachment#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(private.product_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(private.product_desc,"#chr(10)#","<br>","all")#</td></tr>
			     <tr><td class="link_small_gray" style="font-weight: normal;" valign=top>Tags - #private.product_keywords#</td></tr>

			   </table>

           </td>

           <td align=right>

            <input type="submit" name="button" value="More..." class="button_blue_large" onclick="toggle_visibility('private#private.product_id#');">

           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="private#private.product_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td><hr></td></tr>
		   <cfif #private.product_type# is "Product">
			   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Why Choose This Product</td></tr>
		   <cfelse>
			   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Why Use This Service</td></tr>
		   </cfif>
		   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #private.product_pitch# is "">Not provided<cfelse>#replace(private.product_pitch,"#chr(10)#","<br>","all")#</cfif></td></tr>

		   <cfif #private.product_diff# is not "">
			   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Key Differentiators</td></tr>
			   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #private.product_diff# is "">Not provided<cfelse>#replace(private.product_diff,"#chr(10)#","<br>","all")#</cfif></td></tr>
		   </cfif>

		   <cfif #private.product_patent_details# is not "">
			   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px;">Patent & Trademark Information</td></tr>
			   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #private.product_patent_details# is "">Not provided<cfelse>#replace(private.product_patent_details,"#chr(10)#","<br>","all")#</cfif></td></tr>
		   </cfif>

		   <cfif #private.product_url# is not "">
			   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px;">More Information</td></tr>
			   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><a href="#private.product_url#" target="_blank" rel="noopener" style="font-weight: normal;"><u>#private.product_url#</u></a></td></tr>
		   </cfif>

         </table>
       </div>
       </td></tr>

      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

  <tr><td height=10></td></tr>

  <tr><td class="feed_sub_header" style="font-size: 20px; padding-bottom: 0px;">Public Information</td></tr>
  <tr><td class="feed_sub_header" style="font-weight: normal;">Information that has been sourced from the Ratio Exchange.</td></tr>
  <tr><td><hr></td></tr>
  <tr><td height=5></td></tr>

  <tr><td height=5></td></tr>

  <cfif public.recordcount is 0>
   <tr><td class="feed_sub_header">No information has been sourced.</td></tr>
  <cfelse>

   <tr><td>

   <table cellspacing=0 cellpadding=0 border=0 width=100%>

     <cfoutput query="public">

      <tr><td valign=top width=120>
		   <cfif #public.product_attachment# is "">
		   <img src="#image_virtual#/icon_product_stock.png" width=90 vspace=5 border=0>
		   <cfelse>
		   <img src="#media_virtual#/#public.product_attachment#" width=90 vspace=5 border=0>
		   </cfif>
           </td>

           <td valign=top>

			   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			     <tr><td class="feed_sub_header" valign=top style="padding-top: 5px; padding-bottom: 0px;">#ucase(public.product_name)#</td></tr>
			     <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top>#replace(public.product_desc,"#chr(10)#","<br>","all")#</td></tr>
			     <cfif public.product_keywords is not "">
			     	<tr><td class="link_small_gray" style="font-weight: normal;" valign=top>Tags - #public.product_keywords#</td></tr>
			     </cfif>
			   </table>

           </td>

           <td align=right>

            <input type="submit" name="button" value="More..." onclick="toggle_visibility('public#public.product_id#');" class="button_blue_large">

           </td>

      </tr>

      <tr><td></td><td colspan=2>
        <div id="public#public.product_id#" style="display:none;">
         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td><hr></td></tr>
		   <cfif #public.product_type# is "Product">
			   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Why Choose This Product</td></tr>
		   <cfelse>
			   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Why Use This Service</td></tr>
		   </cfif>
		   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #public.product_pitch# is "">Not provided<cfelse>#replace(public.product_pitch,"#chr(10)#","<br>","all")#</cfif></td></tr>

		   <cfif #public.product_diff# is not "">
			   <tr><td class="feed_sub_header" valign=top style="padding-bottom: 0px;">Key Differentiators</td></tr>
			   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #public.product_diff# is "">Not provided<cfelse>#replace(public.product_diff,"#chr(10)#","<br>","all")#</cfif></td></tr>
		   </cfif>

		   <cfif #public.product_patent_details# is not "">
			   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px;">Patent & Trademark Information</td></tr>
			   <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;" valign=top><cfif #public.product_patent_details# is "">Not provided<cfelse>#replace(public.product_patent_details,"#chr(10)#","<br>","all")#</cfif></td></tr>
		   </cfif>

		   <cfif #public.product_url# is not "">
			   <tr><td colspan=2 class="feed_sub_header" style="padding-bottom: 0px;">More Information</td></tr>
			   <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><a href="#public.product_url#" target="_blank" rel="noopener" style="font-weight: normal;"><u>#public.product_url#</u></a></td></tr>
		   </cfif>

         </table>
       </div>
       </td></tr>


      <tr><td colspan=3><hr></td></tr>
     </cfoutput>

   </table>

   </td></tr>

  </cfif>

</table>

