<cfinclude template="/exchange/security/check.cfm">

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(cview_id) as total from cview
 where cview_company_id = #session.company_id#
</cfquery>

<cfquery name="stats1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(customer_id) as customer_total, sum(customer_deal) as customer_revenue from customer
  where customer_company_id = #session.company_id#
</cfquery>

<cfquery name="stats2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(product_id) as product_total from product
  where product_company_id = #session.company_id#
</cfquery>

<cfquery name="stats3" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(partner_id) as partner_total from partner
  where partner_company_id = #session.company_id#
</cfquery>

<cfquery name="duns" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select company_duns from company
  where company_id = #session.company_id#
</cfquery>

<cfquery name="fedawards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(recipient_duns) as total from award_data
  where recipient_duns = '#duns.company_duns#'
</cfquery>

<cfquery name="followers" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select count(follow_id) as total from follow
  where follow_company_id = #session.company_id#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

    <tr><td valign=top>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfoutput>
			<tr><td class="feed_header">Company Stats</td></tr>
			<tr><td height=5></td></tr>
			<tr><td class="text_xsmall"><img src="/images/icon_bluebox.png" width=5 hspace=3>&nbsp;&nbsp;&nbsp;<a href="/exchange/company/"><cfif #fedawards.total# is 0>No Federal Awards<cfelse>#fedawards.total# Federal Awards</cfif></a></td></tr>
			<tr><td class="text_xsmall"><img src="/images/icon_bluebox.png" width=5 hspace=3>&nbsp;&nbsp;&nbsp;<a href="/exchange/company/"><cfif #stats1.customer_total# is 0>No Customers<cfelse>#stats1.customer_total# Customer<cfif #stats1.customer_total# GT 1>s</cfif></a></cfif></td></tr>
			<tr><td class="text_xsmall"><img src="/images/icon_bluebox.png" width=5 hspace=3>&nbsp;&nbsp;&nbsp;<a href="/exchange/company/"><cfif #stats2.product_total# is 0>No Products<cfelse>#stats2.product_total# Product<cfif #stats2.product_total# GT 1>s</cfif></a></cfif></td></tr>
			<tr><td class="text_xsmall"><img src="/images/icon_bluebox.png" width=5 hspace=3>&nbsp;&nbsp;&nbsp;<a href="/exchange/company/"><cfif #stats3.partner_total# is 0>No Partners<cfelse>#stats3.partner_total# Partner<cfif #stats3.partner_total# GT 1>s</cfif></a></cfif></td></tr>
			<tr><td class="text_xsmall"><img src="/images/icon_bluebox.png" width=5 hspace=3>&nbsp;&nbsp;

			<cfif #views.total# is 0>
			No Views
			<cfelse>
			<a href="action.cfm">#views.total# View<cfif #views.total# GT 1>s</cfif></a>
			</cfif>

			</td></tr>

			<tr><td class="text_xsmall"><img src="/images/icon_bluebox.png" width=5 hspace=3>&nbsp;&nbsp;
			<cfif #followers.total# is 0>
			No Followers
			<cfelse>
			<a href="action.cfm">#followers.total# Follower<cfif #followers.total# GT 1>s</cfif></a>
			</cfif>
			</td></tr>

		  </cfoutput>

		</table>

    </td></tr>

</table>