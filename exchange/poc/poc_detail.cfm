<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

	  <cfquery name="offices" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select distinct(fbo_office) from fbo
	   where fbo_dept = '#dept#' and
	         fbo_agency = '#agency#'
	   order by fbo_office
	  </cfquery>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

      <div class="main_box">

      <cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header">#ucase(dept)# - #ucase(agency)#</td>
	         <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

      </cfoutput>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td class="feed_sub_header"><b>Office</b></td>
              <td class="feed_sub_header"><b>Contracting Point of Contacts</b></td>
          </tr>

          <tr><td height=10></td></tr>

          <cfloop query="offices">

		  <cfquery name="poc" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select distinct fbo_poc as name, fbo_poc_email as email, fbo_poc_phone as phone from fbo
		   where fbo_dept = '#dept#' and
				 fbo_agency = '#agency#' and
				 fbo_office = '#offices.fbo_office#' and
				 fbo_poc <> '' and
				 fbo_poc not like 'Questions%'
		   union
		   select distinct fbo_poc_secondary as name, fbo_poc_secondary_email as email, fbo_poc_secondary_phone as phone from fbo
		   where fbo_dept = '#dept#' and
				 fbo_agency = '#agency#' and
				 fbo_office = '#offices.fbo_office#' and
				 fbo_poc_secondary <> '' and
				 fbo_poc_secondary not like 'Questions%'
		   order by name ASC
		  </cfquery>

          <cfoutput>
            <tr>
               <td class="feed_sub_header" bgcolor="e0e0e0" colspan=4>#ucase(fbo_office)#</td>
            </tr>
            <tr><td height=5></td></tr>
          </cfoutput>

		  <cfif poc.recordcount is 0>
		    <tr><td></td><td class="feed_option">No contract POC's found.</td></tr>
		  <cfelse>

			  <cfloop query="poc">

			  <cfoutput>

			   <tr><td></td>
				   <td class="feed_sub_header" style="font-weight: normal;">#poc.name#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#poc.email#</td>
				   <td class="feed_sub_header" style="font-weight: normal;">#poc.phone#</td>
			   </tr>

			  </cfoutput>

			  </cfloop>

          </cfif>

          </cfloop>

		<tr><td height=10></td></tr>
        <tr><td colspan=4><hr></td></tr>
		<tr><td height=10></td></tr>
        <tr><td colspan=4 class="link_small_gray">Note - due to the format of the information received in the Exchange, there may be duplicative contacts.</td></tr>
        <tr><td height=10></td></tr>

        </table>

        </td></tr>

       </table>


	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>