<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

	  <cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   select distinct(fbo_dept), count(distinct(fbo_poc)) as pri_contacts, count(distinct(fbo_poc_secondary)) as sec_contacts from fbo
	   where (fbo_dept <> '')
	   group by fbo_dept
	   order by fbo_dept
	  </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
	     <tr><td class="feed_header"><img src="/images/icon_broker.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;Contracting Points of Contact</td>
	         <td class="feed_option" align=right></td></tr>
	     <tr><td colspan=2><hr></td></tr>
	     <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>The Exchange collects points of contacts from procurements that are issued from the Federal government.  Displayed below are contacts organized by the locations / offices that have issued contracts.</td></tr>
         <tr><td height=15></td></tr>
        </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfloop query="dept">

		  <cfquery name="agency" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select distinct(fbo_agency), count(distinct(fbo_poc)) as pri_contacts, count(distinct(fbo_poc_secondary)) as sec_contacts from fbo
		   where fbo_dept = '#dept.fbo_dept#'
		   and (fbo_poc <> '' or fbo_poc_secondary <> '')
		   group by fbo_agency
		  </cfquery>

         <cfoutput>

		 <tr>
		      <td class="feed_sub_header" bgcolor="e0e0e0">&nbsp;&nbsp;#ucase(dept.fbo_dept)#</td>
		      <td class="feed_sub_header" align=right bgcolor="e0e0e0">#numberformat(evaluate(dept.pri_contacts+dept.sec_contacts),'99,999')#</td>
		 </tr>
		 <tr><td height=5></td></tr>

		 <cfset counter = 0>

		 <cfloop query="agency">

			 <cfoutput>
			  <cfif counter is 0>
			   <tr bgcolor="ffffff">
			  <cfelse>
			   <tr bgcolor="f0f0f0">
			  </cfif>
				 <td class="feed_sub_header" style="padding-left: 30px;">-&nbsp;&nbsp;<a href="poc_detail.cfm?dept=#dept.fbo_dept#&agency=#agency.fbo_agency#" style="font-weight: normal;"><cfif #agency.fbo_agency# is "">Not Specified<cfelse>#agency.fbo_agency#</cfif></a></td>
				 <td class="feed_sub_header" style="font-weight: normal;" align=right>#numberformat(evaluate(agency.pri_contacts+agency.sec_contacts),'9,999')#</td>
			  </tr>
			 </cfoutput>
			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

		 </cfloop>
		 <tr><td height=5></td></tr>


		 </cfoutput>

		 </cfloop>

		 </table>

		 </td></tr>

	   </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>