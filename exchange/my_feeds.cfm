<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>
      <cfset location = "left">
	  <cfinclude template="column_left.cfm">

      </td><td valign=top width=100%>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/">Welcome</a></span>
          </div>

		  <cfquery name="hasnews" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select hub_app_id from hub_app
           join app on app_id = hub_app_app_id
           where hub_app_hub_id = #session.hub# and
                 app_security_id = 'NEWSWIRE01'
          </cfquery>

          <cfif hasnews.recordcount GT 0>

	          <div class="tab_not_active">
	           <span class="feed_sub_header"><img src="/images/icon_news_feed.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/news.cfm">Newswire</a></span>
	          </div>

          </cfif>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_feed2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/my_feeds.cfm">Posts</a></span>
          </div>

		  <div class="main_box_2">
		   <cfinclude template="/exchange/feed.cfm">
		  </div>

      </td><td valign=top width=185>

      <cfset location = "right">

      <cfinclude template="column_right.cfm">

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>