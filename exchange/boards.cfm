<div class="left_box">

<cfquery name="pipe_access" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_pipeline_id from sharing
 where sharing_hub_id = #session.hub# and
       sharing_pipeline_id is not null and
	   (sharing_to_usr_id = #session.usr_id#)
</cfquery>

<cfif pipe_access.recordcount is 0>
 <cfset pipe_list = 0>
<cfelse>
 <cfset pipe_list = valuelist(pipe_access.sharing_pipeline_id)>
</cfif>

<cfquery name="pipeline" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select pipeline_name, pipeline_image, pipeline_id, usr_first_name, usr_last_name, pipeline_updated from pipeline
  left join usr on usr_id = pipeline_usr_id
  where pipeline_hub_id = #session.hub# and
		(pipeline_id in (#pipe_list#) or pipeline_usr_id = #session.usr_id#)
  order by pipeline_updated DESC
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
 <tr><td class="feed_header" valign=bottom><a href="/exchange/pipeline/"><b>Opportunity Boards</b></a></td>
     <td align=right valign=top></td></tr>
 <tr><td><hr></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr><td height=10></td></tr>

<cfset count = 1>

<cfoutput query="pipeline">

<tr>

<cfif #pipeline.pipeline_image# is "">
 <td width=40><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="/images/stock_pipeline.png" width=30 border=0 alt="Open" title="Open"></a></td>
<cfelse>
 <td width=40><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><img src="#media_virtual#/#pipeline.pipeline_image#" width=30 border=0 alt="Open" title="Open"></a></td>
</cfif>

 <td class="link_med_blue"><a href="/exchange/pipeline/refresh.cfm?i=#encrypt(pipeline.pipeline_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#"><cfif len(pipeline.pipeline_name) GT 40>#left(pipeline.pipeline_name,'40')#...<cfelse>#pipeline.pipeline_name#</cfif></a></td>

</tr>

<cfif count LT pipeline.recordcount>
 <tr><td colspan=2><hr></td></tr>
</cfif>

<cfset count = count + 1>

</cfoutput>

</table>

</div>