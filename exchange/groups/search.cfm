<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="group" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group
 where sharing_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="existing" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select sharing_group_usr_usr_id from sharing_group_usr
 where sharing_group_usr_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfif existing.recordcount is 0>
 <cfset user_list = 0>
<cfelse>
 <cfset user_list = valuelist(existing.sharing_group_usr_usr_id)>
</cfif>

<cfquery name="search" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  join hub_xref on hub_xref_usr_id = usr_id
  where hub_xref_hub_id = #session.hub# and
  hub_xref_active = 1 and
  usr_id <> #session.usr_id# and
  usr_id not in (#user_list#) and
  contains((usr_full_name, usr_first_name, usr_last_name, usr_about, usr_background, usr_keywords, usr_experience),'"#trim(keyword)#"')
  order by usr_last_name, usr_first_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header"><cfoutput>#ucase(group.sharing_group_name)# - USERS</cfoutput></td>
            <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr>
        <form action="search.cfm?i=<cfoutput>#i#</cfoutput>" method="post">

        <td class="feed_sub_header" style="font-weight: normal;">

        <b>Search for User</b>&nbsp;&nbsp;
        <input type="text" name="keyword" style="width: 200px;" class="input_text" placeholder="enter name..." required>
        &nbsp;
        <input type="submit" name="button" class="button_blue" value="Go">

          </td></form></tr>

          <tr><td class="feed_sub_header" style="font-weight: normal;">Users that have already been added to this Group will not appear in search results.</td></tr>

         </table>


         <cfif search.recordcount is 0>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
          	<tr><td class="feed_sub_header" style="font-weight: normal;">No Users were found.</td></tr>
          </table>

         <cfelse>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
              <td class="feed_sub_header">ADD</td>
              <td></td>
              <td class="feed_sub_header">NAME</td>
              <td class="feed_sub_header">TITLE</td>
              <td class="feed_sub_header">EMAIL</td>
           </tr>

           <form action="save.cfm?i=<cfoutput>#i#</cfoutput>" method="post">

           <cfoutput query="search">

              <tr>
                  <td width=75>&nbsp;<input type="checkbox" class="input_text" name="usr_id" value=#encrypt(search.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;"></td>
                  <td width=80 class="feed_sub_header">

 				 <cfif #search.usr_photo# is "">
 				  <img src="/images/headshot.png" height=40 width=40 border=0>
 				 <cfelse>
 				  <img style="border-radius: 0px;" src="#media_virtual#/#search.usr_photo#" height=40 width=40 border=0>
 				 </cfif>

 				</td>

 			    <td width=200 class="feed_sub_header">#search.usr_first_name# #search.usr_last_name#</a></td>
 			    <td width=200 class="feed_sub_header" style="font-weight: normal;">#search.usr_title#</td>
 			    <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(search.usr_email))#</td>
 			 </tr>

 			 <tr><td colspan=7><hr></td></tr>

 			</cfoutput>

             <tr><td height=10></td></tr>
             <tr><td colspan=5><input type="submit" name="button" class="button_blue_large" value="Add to Group"></td></tr>

 			</form>

 			</table>

		 </cfif>

          </td></tr>
          </table>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>