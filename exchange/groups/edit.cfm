<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="edit" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group
 where sharing_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

 		   <cfinclude template="/exchange/components/my_profile/profile.cfm">
		   <cfinclude template="/exchange/portfolio/recent.cfm">

        </td><td valign=top>

       <div class="main_box">

       <form action="save.cfm" method="post" enctype="multipart/form-data" >

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">Edit Group</td>
            <td align=right class="feed_sub_header">

              <a href="/exchange/groups/">Return</a>

            </td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfoutput>

         <tr><td class="feed_sub_header" width=150><b>Group Name</b></td>
             <td class="feed_option"><input type="text" class="input_text" value="#edit.sharing_group_name#" style="width: 534px;" name="sharing_group_name" style="width:300px;" placeholder="Please give this Group a name." required></td>
             </td></tr>

         <tr><td class="feed_sub_header" valign=top><b>Description</b></td>
             <td class="feed_option"><textarea name="sharing_group_desc" class="input_textarea" cols=70 rows=5 placeholder="Please provide a short description of this Group.">#edit.sharing_group_desc#</textarea></td>
             </td></tr>

          <tr><td height=10></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td height=10></td></tr>
          <tr><td></td><td>

             <input type="submit" name="button" class="button_blue_large" value="Save">
             &nbsp;&nbsp;<input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Are you sure you want to delete this Group?');">

          <input type="hidden" name="i" value=#i#>

          </cfoutput>

          </td></tr>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>