<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="group" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group
 where sharing_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group_usr
 join usr on usr_id = sharing_group_usr_usr_id
 where sharing_group_usr_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header"><cfoutput>#group.sharing_group_name# - Users</cfoutput></td>
            <td align=right class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfif isdefined("u")>
           <cfif u is 1>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">User has been successfully added.</td></tr>
           <cfelseif u is 2>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">User has been successfully removed.</td></tr>
           <cfelseif u is 3>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">No user(s) were selected to remove.</td></tr>
           <cfelseif u is 4>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">No user(s) were selected to add.</td></tr>
           </cfif>
          </cfif>

          </table>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <tr>
            <form action="search.cfm?i=<cfoutput>#i#</cfoutput>" method="post">

        <td class="feed_sub_header" style="font-weight: normal;">

        <b>Search for User</b>&nbsp;&nbsp;
        <input type="text" name="keyword" style="width: 200px;" class="input_text" placeholder="enter name..." required>
        &nbsp;
        <input type="submit" name="button" class="button_blue" value="Go">

        <cfoutput>
         <input type="hidden" name="i" value="#i#">
        </cfoutput>

          </td></form></tr>

         </table>


         <cfif members.recordcount is 0>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
          	<tr><td class="feed_sub_header" style="font-weight: normal;">No Users have been added to this Group.</td></tr>
          </table>

         <cfelse>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
              <td class="feed_sub_header">REMOVE</td>
              <td></td>
              <td class="feed_sub_header">NAME</td>
              <td class="feed_sub_header">TITLE</td>
              <td class="feed_sub_header">EMAIL</td>
           </tr>

           <form action="remove.cfm?i=<cfoutput>#i#</cfoutput>" method="post">

           <cfoutput query="members">

              <tr>
                  <td width=100>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="input_text" name="share_id" value=#encrypt(members.usr_id,session.key, "AES/CBC/PKCS5Padding", "HEX")# style="width: 22px; height: 22px;"></td>
                  <td width=80 class="feed_sub_header">

 				 <cfif #members.usr_photo# is "">
 				  <img src="/images/headshot.png" height=40 width=40 border=0>
 				 <cfelse>
 				  <img style="border-radius: 0px;" src="#media_virtual#/#members.usr_photo#" height=40 width=40 border=0>
 				 </cfif>

 				</td>

 			    <td width=200 class="feed_sub_header">#members.usr_first_name# #members.usr_last_name#</a></td>
 			    <td width=200 class="feed_sub_header" style="font-weight: normal;">#members.usr_title#</td>
 			    <td class="feed_sub_header" style="font-weight: normal;">#tostring(tobinary(members.usr_email))#</td>
 			 </tr>

 			 <tr><td colspan=7><hr></td></tr>

 			</cfoutput>

             <tr><td height=10></td></tr>
             <tr><td colspan=5><input type="submit" name="button" class="button_blue_large" value="Remove" onclick="return confirm('Remove?\r\nAre you sure you want to remove this user from the Group?');"></td></tr>

 			</form>

 			</table>




		 </cfif>

          </td></tr>
          </table>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>