<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="groups" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from sharing_group
 where sharing_group_usr_id = #session.usr_id# and
       sharing_group_hub_id = #session.hub#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

	   <cfinclude template="/exchange/components/my_profile/profile.cfm">
	   <cfinclude template="/exchange/portfolio/recent.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">My Groups</td>
            <td align=right class="feed_sub_header"><img src="/images/plus3.png" hspace=10 width=15><a href="add.cfm">Create Group</a>

            <cfif isdefined("session.group_return_page")>
             <cfif session.group_return_page is 1>
              &nbsp;|&nbsp;<a href="/exchange/portfolio/portfolio_sharing.cfm">Return</a>
             <cfelseif session.group_return_page is 2>
              &nbsp;|&nbsp;<a href="/exchange/pipeline/sharing.cfm">Return</a>
             </cfif>
            </cfif>

            </td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

	      <table cellspacing=0 cellpadding=0 border=0 width=100%>


          <cfif isdefined("u")>
           <cfif u is 1>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">Group has been successfully created.</td></tr>
           <cfelseif u is 2>
            <tr><td colspan=4 class="feed_sub_header" style="color: green;">Group has been successfully updated.</td></tr>
           <cfelseif u is 3>
            <tr><td colspan=4 class="feed_sub_header" style="color: red;">Group has been successfully deleted.</td></tr>
           </cfif>
          </cfif>

          </table>

         <cfif groups.recordcount is 0>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>
          	<tr><td class="feed_sub_header" style="font-weight: normal;">No groups have been created.</td></tr>
          </table>

         <cfelse>

 	      <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
             <td class="feed_sub_header">Group Name</td>
             <td class="feed_sub_header">Description</td>
             <td class="feed_sub_header" align=center>Users</td>
          </tr>

          <cfset counter = 0>

          <cfloop query="groups">

			<cfquery name="members" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select count(sharing_group_usr_id) as total from sharing_group_usr
			 where sharing_group_usr_group_id = #groups.sharing_group_id#
			</cfquery>

             <cfif counter is 0>
              <tr bgcolor="ffffff">
             <cfelse>
              <tr bgcolor="e0e0e0">
             </cfif>

			    <cfoutput>
			    <td width=300 class="feed_sub_header"><a href="edit.cfm?i=#encrypt(sharing_group_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">#groups.sharing_group_name#</a></td>
			    <td valign=top class="feed_sub_header" style="font-weight: normal;">#groups.sharing_group_desc#</td>
			    <td valign=top class="feed_sub_header" style="font-weight: normal;" align=center>#members.total#</td>
			    <td align=right class="feed_sub_header"><a href="manage.cfm?i=#encrypt(sharing_group_id,session.key, "AES/CBC/PKCS5Padding", "HEX")#">Users</a></td>
			    </cfoutput>
			 </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

		  </cfloop>

			</table>

			</cfif>

          </td></tr>
          </table>

   	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>