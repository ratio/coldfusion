<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Create Group">

	<cfquery name="insert_port" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
      insert into sharing_group
      (sharing_group_name, sharing_group_desc, sharing_group_hub_id, sharing_group_usr_id)
      values
      ('#sharing_group_name#','#sharing_group_desc#',#session.hub#,#session.usr_id#)
	</cfquery>

    <cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Save">

	<cfquery name="update_port" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 update sharing_group
	 set sharing_group_name = '#sharing_group_name#',
	     sharing_group_desc = '#sharing_group_desc#'
	 where sharing_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Add to Group">

<cfif isdefined("usr_id")>

	<cftransaction>

	 <cfloop index="d" list="#usr_id#">

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into sharing_group_usr
		 (
		  sharing_group_usr_usr_id,
		  sharing_group_usr_group_id
		 )
		 values
		 (
		 #decrypt(d,session.key, "AES/CBC/PKCS5Padding", "HEX")#,
		 #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
		 )
		</cfquery>

	 </cfloop>

	</cftransaction>

	<cflocation URL="manage.cfm?i=#i#&u=1" addtoken="no">

<cfelse>

	<cflocation URL="manage.cfm?i=#i#&u=4" addtoken="no">

</cfif>

<cfelseif #button# is "Delete">

<cftransaction>

	<cfquery name="delete_port_2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete sharing_group
	 where sharing_group_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
	</cfquery>

</cftransaction>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>