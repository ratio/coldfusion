<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as amount from grants
  where ((action_date between '#session.grant_from#' and '#session.grant_to#')

         <cfif session.grant_state is not 0>
          and recipient_state_code = '#session.grant_state#'
         </cfif>

         <cfif session.grant_dept is not 0>
          and awarding_agency_code = '#session.grant_dept#'
         </cfif>

         )

	and contains((award_description, cfda_title, cfda_number, recipient_duns, recipient_name, sai_number),'"#session.grant_keyword#"')

 </cfquery>

 <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from grants
  where ((action_date between '#session.grant_from#' and '#session.grant_to#')

         <cfif session.grant_state is not 0>
          and recipient_state_code = '#session.grant_state#'
         </cfif>

         <cfif session.grant_dept is not 0>
          and awarding_agency_code = '#session.grant_dept#'
         </cfif>

         )

	and contains((award_description, cfda_title, cfda_number, recipient_duns, recipient_name, sai_number),'"#session.grant_keyword#"')

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by action_date ASC
 		    <cfelseif #sv# is 10>
 		     order by action_date DESC
 		    <cfelseif #sv# is 2>
 		     order by award_id_fain ASC
 		    <cfelseif #sv# is 20>
 		     order by award_id_fain DESC
 		    <cfelseif #sv# is 3>
 		     order by recipient_name ASC
 		    <cfelseif #sv# is 30>
 		     order by recipient_name DESC
 		    <cfelseif #sv# is 4>
 		     order by awarding_sub_agency_name ASC
 		    <cfelseif #sv# is 40>
 		     order by awarding_sub_agency_name DESC
 		    <cfelseif #sv# is 5>
 		     order by federal_action_obligation DESC
 		    <cfelseif #sv# is 50>
 		     order by federal_action_obligation ASC
 		    <cfelseif #sv# is 6>
 		     order by awarding_agency_name ASC
 		    <cfelseif #sv# is 60>
 		     order by awarding_agency_name DESC
 		    <cfelseif #sv# is 7>
 		     order by cfda_title ASC
 		    <cfelseif #sv# is 70>
 		     order by cfda_title DESC
 		    </cfif>

 		   <cfelse>
             order by federal_action_obligation DESC
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

	   <cfparam name="URL.PageId" default="0">
	   <cfset RecordsPerPage = 250>
	   <cfset TotalPages = (grants.Recordcount/RecordsPerPage)>
	   <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
	   <cfset EndRow = StartRow+RecordsPerPage-1>
	   <cfset counter = 0>
	   <cfset tot = 0>

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
			  <tr><td class="feed_header" valign=top>FEDERAL GRANTS</td>
				  <td class="feed_option" valign=top align=right><a href="/exchange/grants/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" valign=top><b>Search Results</b></td></tr>
			  <tr><td class="feed_sub_header" colspan=2><b>

				 <cfif #session.grant_state# is not 0>

					 <cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					  select state_name from state
					  where state_abbr = '#session.grant_state#'
					 </cfquery>

					 #ucase(state.state_name)#,

				 <cfelse>
				 All States,
				 </cfif>

				 <cfif #session.grant_dept# is not 0>

					 <cfquery name="dept_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
					  select top(1) awarding_agency_name from grants
					  where awarding_agency_code = '#session.grant_dept#'
					 </cfquery>

					 #ucase(dept_info.awarding_agency_name)#,

				 <cfelse>
				 All Departments,
				 </cfif>

				 #dateformat(session.grant_from,'mm/dd/yyyy')# - #dateformat(session.grant_to,'mm/dd/yyyy')#, Keyword: #session.grant_keyword#


			  </b></td></tr>
			  <tr><td height=5></td></tr>
			  <tr><td class="feed_option">
			     <b>Total Grants: #numberformat(grant_count.total,'999,999')#<br>
			        Total Value: #numberformat(grant_count.amount,'$999,999,999,999')#<br>
			        Total Awardees: #numberformat(grant_count.vendors,'999,999')#
			     </b></td>
		   </cfoutput>

		      <td class="feed_option" align=right valign=bottom width=75%>

                  <cfoutput>

					  <cfif grants.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

			  </td></tr>

         <tr><td height=5></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <cfoutput>
			 <tr height=40>
				<td class="text_xsmall" width=75><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Award Date</b></a></td>
				<td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Award ID</b></a></td>
				<td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Awardee</b></a></td>
				<td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>Department</b></a></td>
				<td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>Agency</b></a></td>
				<td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>Award Title</b></a></td>
				<td class="text_xsmall"><b>Description</b></td>
				<td class="text_xsmall" align=right width=100><a href="results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>Value</b></a></td>
			 </tr>
			 </cfoutput>

             <cfset counter = 0>

			 <cfloop query="grants">

             <cfif CurrentRow gte StartRow >

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

			 <cfoutput>

				  <td class="text_xsmall" valign=top>#dateformat(grants.action_date,'mm/dd/yyyy')#</td>
				  <td class="text_xsmall" valign=top width=150><a href="/exchange/include/grant_information.cfm?id=#id#" target="blank"><b>#grants.award_id_fain#</b></a></td>
				  <td class="text_xsmall" valign=top width=200><a href="/exchange/include/federal_profile.cfm?duns=#grants.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#grants.recipient_name#</b></a></td>
				  <td class="text_xsmall" valign=top>#grants.awarding_agency_name#</td>
				  <td class="text_xsmall" valign=top>#grants.awarding_sub_agency_name#</td>
				  <td class="text_xsmall" valign=top>#grants.cfda_title#</td>
				  <td class="text_xsmall" valign=top width=350>

                  #replaceNoCase(award_description,session.grant_keyword,"<span style='background:yellow'>#ucase(session.grant_keyword)#</span>","all")#

                  </td>
				  <td class="text_xsmall" valign=top align=right>#numberformat(grants.federal_action_obligation,'$999,999,999,999')#</td>

             </cfoutput>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

             </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>