<cfquery name="state" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from state
 order by state_name
</cfquery>

<cfquery name="dept" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(awarding_agency_code), awarding_agency_name from grants
 where recipient_country_code = 'USA'
 order by awarding_agency_name
</cfquery>

<cfif not isdefined("session.grant_from")>
	<cfset session.grant_from = dateadd('yyyy',-2,now())>
</cfif>

<cfif not isdefined("session.grant_to")>
	<cfset session.grant_to = now()>
</cfif>

 <cfoutput>
	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_header"><img src="/images/icon_grants3.png" width=18 align=absmiddle>&nbsp;&nbsp;&nbsp;SEARCH FEDERAL GRANTS</td>
		  <td align=right></td></tr>
	   <tr><td colspan=2><hr></td></tr>
	 </table>
 </cfoutput>

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

  <form action="/exchange/grants/set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">

   <tr>
	  <td class="feed_option"><b>Keyword</b></td>
	  <td class="feed_option"><b>State</b></td>
	  <td class="feed_option"><b>Department</b></td>
	  <td class="feed_option"><b>Award From</b></td>
	  <td class="feed_option"><b>Award To</b></td>
	  <td>&nbsp;</td>
   </tr>

   <tr>

	   <td class="feed_option">
	   <cfoutput>
		<input type="text" class="input_text" placeholder="i.e., machine learning" name="search_keyword" size=20 required <cfif isdefined("session.grant_keyword")>value=#session.grant_keyword#</cfif>>
	   </cfoutput>
		</td>


	   <td class="feed_option">
		<select name="search_state" class="input_select">
		<option value=0>All States
		<cfoutput query="state">
		 <option value="#state_abbr#" <cfif isdefined("session.grant_state") and #session.grant_state# is "#state.state_abbr#">selected</cfif>>#state_name#
		</cfoutput>
		</td>

	   <td class="feed_option">
		<select name="search_dept" class="input_select" style="width: 250px;">
		<option value=0>All Departments
		<cfoutput query="dept">
		 <option value="#awarding_agency_code#" <cfif isdefined("session.grant_dept") and #session.grant_dept# is "#awarding_agency_code#">selected</cfif>>#awarding_agency_name#
		</cfoutput>
		</td>

		<cfoutput>

	   <td class="feed_option">
		<input type="date" class="input_date" name="search_from" required value="#dateformat(session.grant_from,'yyyy-mm-dd')#">
		</td>

	   <td class="feed_option">
		<input type="date" class="input_date" name="search_to" required value="#dateformat(session.grant_to,'yyyy-mm-dd')#">
		</td>

		</cfoutput>

	   <td class="feed_option" align=right>
		<input class="button_blue" type="submit" name="button" value="Search">
		</td>

   </tr>

   <tr><td height=5></td></tr>
</form>

  </table>

