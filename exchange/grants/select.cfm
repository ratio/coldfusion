<cfinclude template="/exchange/security/check.cfm">
<cfset #session.grant_year# = #year#>
<cfif loc is 1>
 <cflocation URL="index.cfm" addtoken="no">
<cfelseif loc is 2>
 <cflocation URL="detail_dept.cfm?dept=#dept#" addtoken="no">
<cfelseif loc is 3>
 <cflocation URL="detail_state.cfm?state=#state#" addtoken="no">
<cfelseif loc is 4>
 <cflocation URL="awardee_grants.cfm?state=#state#&awardee=#awardee#" addtoken="no">
<cfelseif loc is 5>
 <cflocation URL="awardee_city.cfm?state=#state#&city=#city#" addtoken="no">
<cfelseif loc is 6>
 <cflocation URL="state_vendors.cfm?dept=#dept#&state=#state#" addtoken="no">
<cfelseif loc is 7>
 <cflocation URL="dept_state_vendor_grants.cfm?dept=#dept#&state=#state#&duns=#duns#" addtoken="no">
<cfelseif loc is 8>
 <cflocation URL="dept_vendor.cfm?dept=#dept#&duns=#duns#" addtoken="no">
<cfelseif loc is 9>
 <cflocation URL="detail_agency.cfm?dept=#dept#&agency=#agency#" addtoken="no">
<cfelseif loc is 10>
 <cflocation URL="agency_vendor_grants.cfm?dept=#dept#&agency=#agency#&duns=#duns#" addtoken="no">
<cfelseif loc is 11>
 <cflocation URL="detail_dept_vendors.cfm?dept=#dept#" addtoken="no">
</cfif>