<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfset start_year = #session.grant_year# - 1>
 <cfset start_date = '10/1/' & #start_year#>
 <cfset end_date = '9/30/' & #session.grant_year#>

 <cfquery name="dept_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_agency_name from grants
  where awarding_agency_code = '#dept#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total from grants
  where awarding_agency_code = '#dept#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select sum(federal_action_obligation) as total from grants
  where awarding_agency_code = '#dept#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
 </cfquery>

 <cfquery name="vend" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as value, recipient_duns, recipient_name, recipient_state_code from grants
  where awarding_agency_code = '#dept#'
  and ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
  group by recipient_duns, recipient_name, recipient_state_code
  order by value DESC
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		 <cfparam name="URL.PageId" default="0">
		 <cfset RecordsPerPage = 500>
		 <cfset TotalPages = (vend.Recordcount/RecordsPerPage)>
		 <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		 <cfset EndRow = StartRow+RecordsPerPage-1>
		 <cfset counter = 0>
		 <cfset tot = 0>

         <form action="select.cfm" method="post">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Grants</td>
              <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td height=10></td></tr>
          <tr><td class="feed_header" valign=top>#dept_info.awarding_agency_name#</b></td>
              <td class="feed_option" align=right><b>Change Year: &nbsp;</b>
             <select name="year" onchange="this.form.submit()">
              <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
              <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
              <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
              <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
              <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
              <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
             </select>

             &nbsp;


              </td></tr>
              <tr><td>&nbsp;</td></tr>
              <tr><td class="feed_option">

                   <cfoutput>

                   <b>Total Grants:</b> #numberformat(grant_count.total,'999,999,999')#<br>
                   <b>Total Value: </b> #numberformat(grant_total.total,'$99,999,999,999')#</td>

                   </cfoutput>

                  <td valign=bottom class="feed_option" align=right>

                  <cfoutput>

					  <cfif vend.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#&dept=#dept#">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

                  </td>
             </tr>

         <tr><td height=5></td></tr>

         </table>

         <cfoutput>

         <input type="hidden" name="dept" value="#dept#">
         <input type="hidden" name="loc" value=11>

         </cfoutput>

         </form>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

             <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr>
				<td class="feed_option"><b>Awardee</b></td>
				<td class="feed_option"><b>State</b></td>
				<td class="feed_option" width=100 align=right><b>Awards</b></td>
				<td class="feed_option" align=right width=150><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfloop query="vend">

             <cfif CurrentRow gte StartRow >

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>
			 <cfoutput>

				  <td class="feed_option"><a href="dept_vendor.cfm?dept=#dept#&duns=#vend.recipient_duns#">#recipient_name#</a></td>
				  <td class="feed_option">#recipient_state_code#</td>
				  <td class="feed_option" align=right>#numberformat(vend.total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(vend.value,'$999,999,999,999')#</td>

 </cfoutput>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 <tr><td>&nbsp;</td></tr>
			 <tr><td class="feed_option"><a href="detail_dept_vendors.cfm">All Awardees</a>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>