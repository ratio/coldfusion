<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="agency_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_sub_agency_name from grants
  where awarding_sub_agency_code = '#agency#'
 </cfquery>

 <cfquery name="dept_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_agency_name from grants
  where awarding_agency_code = '#dept#'
 </cfquery>

 <cfquery name="vendor_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) recipient_name from grants
  where recipient_duns = '#duns#'
 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as count, sum(federal_action_obligation) as total from grants
  where awarding_agency_code = '#dept#' and
        awarding_sub_agency_code = '#agency#' and
        recipient_duns = '#duns#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
 </cfquery>

 <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from grants
  where awarding_agency_code = '#dept#' and
        awarding_sub_agency_code = '#agency#' and
        recipient_duns = '#duns#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by action_date ASC
 		    <cfelseif #sv# is 10>
 		     order by action_date DESC
 		    <cfelseif #sv# is 2>
 		     order by award_id_fain ASC
 		    <cfelseif #sv# is 20>
 		     order by award_id_fain DESC
 		    <cfelseif #sv# is 3>
 		     order by cfda_title ASC
 		    <cfelseif #sv# is 30>
 		     order by cfda_title DESC
 		    <cfelseif #sv# is 4>
 		     order by federal_action_obligation DESC
 		    <cfelseif #sv# is 40>
 		     order by federal_action_obligation ASC
 		    </cfif>

 		   <cfelse>
             order by action_date DESC
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		 <cfparam name="URL.PageId" default="0">
		 <cfset RecordsPerPage = 250>
		 <cfset TotalPages = (grants.Recordcount/RecordsPerPage)>
		 <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		 <cfset EndRow = StartRow+RecordsPerPage-1>
		 <cfset counter = 0>
		 <cfset tot = 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
			  <tr><td class="feed_header">FEDERAL GRANTS</td>
				  <td class="feed_option" align=right><a href="/exchange/grants/detail_agency.cfm?dept=#dept#&agency=#agency#"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" valign=top><b>#dept_info.awarding_agency_name#</b></td></tr>
			  <tr><td height=5></td></tr>
              <tr><td class="feed_sub_header"><b>#ucase(agency_info.awarding_sub_agency_name)#</td>

                           <form action="/exchange/grants/set2.cfm" method="post">
			               <td class="feed_option" align=right><b>Select Year</b>&nbsp;&nbsp;

			                <select name="year" class="input_select">
			                 <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
			                 <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
			                 <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
			                 <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
			                 <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
			                 <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
			                </select>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Refresh">
			               </td>

			               <cfoutput>
			               <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">
			               </cfoutput>

			             </td>
          </form>

              </tr>
              <tr><td height=5></td></tr>
              <tr><td class="feed_sub_header"><b><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer"><u>#vendor_info.recipient_name#</u></a></td></tr>
              <tr><td height=5></td></tr>
              <tr><td class="feed_option">

				 <b>Total Grants: #numberformat(grant_total.count,'999,999,999')#<br>
				    Total Value: #numberformat(grant_total.total,'$99,999,999,999')#</b>
             </cfoutput>

             </td><td class="feed_option" align=right valign=bottom>

                  <cfoutput>

					  <cfif grants.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#&dept=#dept#&agency=#agency#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

                   <td></tr>

         <tr><td height=5></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfoutput>
			 <tr height=40>
				<td class="feed_option" width=100><a href="agency_vendor_grants.cfm?dept=#dept#&agency=#agency#&duns=#duns#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATE</b></a></td>
				<td class="feed_option"><a href="agency_vendor_grants.cfm?dept=#dept#&agency=#agency#&duns=#duns#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARD ID</b></a></td>
				<td class="feed_option"><a href="agency_vendor_grants.cfm?dept=#dept#&agency=#agency#&duns=#duns#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AWARD TITLE</b></a></td>
				<td class="feed_option"><b>AWARD DESCRIPTION</b></td>
				<td class="feed_option" align=right width=100><a href="agency_vendor_grants.cfm?dept=#dept#&agency=#agency#&duns=#duns#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AMOUNT</b></a></td>
			 </tr>
			 </cfoutput>

             <cfset ccounter = 0>

			 <cfloop query="grants">

             <cfif CurrentRow gte StartRow >

 			 <cfif ccounter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

			 <cfoutput>

				  <td class="text_xsmall" valign=middle>#dateformat(action_date,'mm/dd/yyyy')#</a></td>
				  <td class="text_xsmall" valign=middle width=150><a href="/exchange/include/grant_information.cfm?id=#id#" target="blank"><b>#award_id_fain#</b></a></td>
				  <td class="text_xsmall" valign=middle>#cfda_title#</td>
				  <td class="text_xsmall" valign=middle>#left(award_description,200)#...</td>
				  <td class="text_xsmall" valign=middle align=right>#numberformat(federal_action_obligation,'$999,999,999,999')#</td>

             </cfoutput>

			  </tr>

			 <cfif ccounter is 0>
			  <cfset ccounter = 1>
			 <cfelse>
			  <cfset ccounter = 0>
			 </cfif>

             </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>