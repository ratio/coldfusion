<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="state_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  where state_abbr = '#state#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as amount from grants
  where recipient_state_code = '#state#' and
        YEAR(action_date) = #session.grant_year# and
        recipient_country_code = 'USA'
 </cfquery>

 <cfquery name="vend" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as value, recipient_duns, recipient_name from grants
  where recipient_state_code = '#state#' and
        year(action_date) = #session.grant_year# and
        recipient_country_code = 'USA'
  group by recipient_duns, recipient_name

   		   <cfif isdefined("sv")>

   		    <cfif #sv# is 5>
   		     order by recipient_name ASC
   		    <cfelseif #sv# is 50>
   		     order by recipient_name DESC
   		    <cfelseif #sv# is 6>
   		     order by recipient_duns ASC
   		    <cfelseif #sv# is 60>
   		     order by recipient_duns DESC
   		    <cfelseif #sv# is 7>
   		     order by total DESC
   		    <cfelseif #sv# is 70>
   		     order by total ASC
   		    <cfelseif #sv# is 8>
   		     order by value DESC
   		    <cfelseif #sv# is 80>
   		     order by value ASC
            <cfelse>
             order by value DESC
   		    </cfif>

   		   <cfelse>
             order by value DESC
		   </cfif>

 </cfquery>

 <cfquery name="cities" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as value, count(distinct(recipient_duns)) as vendors, recipient_city_name from grants
  where recipient_state_code = '#state#' and
        year(action_date) = #session.grant_year# and
        recipient_country_code = 'USA'
  group by recipient_city_name

   		   <cfif isdefined("sv")>

   		    <cfif #sv# is 1>
   		     order by recipient_city_name ASC
   		    <cfelseif #sv# is 10>
   		     order by recipient_city_name DESC
   		    <cfelseif #sv# is 2>
   		     order by total DESC
   		    <cfelseif #sv# is 20>
   		     order by total ASC
   		    <cfelseif #sv# is 3>
   		     order by vendors DESC
   		    <cfelseif #sv# is 30>
   		     order by vendors ASC
   		    <cfelseif #sv# is 4>
   		     order by value DESC
   		    <cfelseif #sv# is 40>
   		     order by value ASC
   		    <cfelse>
   		     order by recipient_city_name ASC
   		    </cfif>

   		   <cfelse>
            order by recipient_city_name ASC
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

         <cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header" valign=top>FEDERAL GRANTS</td>
              <td class="feed_option" valign=top align=right><a href="/exchange/grants/"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td class="feed_sub_header" valign=top><b>#ucase(state_info.state_name)#</b></td>

             <form action="set2.cfm" method="post">
             <td align=right><span class="feed_sub_header" style="font-weight: normal;"><b>Select Year</b></span>&nbsp;&nbsp;

              <select name="year" class="input_select" onchange="form.submit();">
               <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
               <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
               <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
               <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
               <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
               <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
              </select>
             </td>

             <cfoutput>
             <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">
             </cfoutput>

           </td>
          </form>

          </tr>

          <tr><td class="feed_option" valign=top><b>Total Grants: #numberformat(grant_count.total,'999,999,999')#<br>
             Total Value: #numberformat(grant_count.amount,'$99,999,999,999')#<br>
             Total Awardess: #grant_count.vendors#</b></td></tr>
         <tr><td height=5></td></tr>

         </table>

         </cfoutput>

         <input type="hidden" name="loc" value=3>

         </form>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfoutput>

			 <tr height=40>
				<td class="feed_option" width=50><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>BY CITY</b></a></td>
				<td class="feed_option" align=center><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" align=center><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PARTNERS</b></a></td>
				<td class="feed_option" align=right><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>TOTAL VALUE</b></a></td>
			 </tr>

			 </cfoutput>

             <cfset counter = 0>

			 <cfoutput query="cities">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>

				  <td class="feed_option"><a href="awardee_city.cfm?state=#state#&city=#recipient_city_name#"><cfif #recipient_city_name# is "">Unknown<cfelse><b>#recipient_city_name#</b></cfif></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=center>#numberformat(vendors,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td valign=top width=2%>&nbsp;</td>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <cfoutput>
			 <tr height=40>
				<td class="feed_option"><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>PARTNER</b></a></td>
				<td class="feed_option" width=100><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DUNS</b></a></td>
				<td class="feed_option" width=100 align=center><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" align=right width=150><a href="detail_state.cfm?state=#state#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>TOTAL VALUE</b></a></td>
			 </tr>
			 </cfoutput>
             <cfset counter = 0>

			 <cfoutput query="vend">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>

				  <td class="feed_option"><a href="awardee_grants.cfm?state=#state#&awardee=#recipient_duns#"><b>#recipient_name#</b></a></td>
				  <td class="feed_option">#recipient_duns#</td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right width=100>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>