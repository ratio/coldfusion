<cfinclude template="/exchange/security/check.cfm">

<cfset session.grant_state = #search_state#>
<cfset session.grant_dept = #search_dept#>
<cfset session.grant_keyword = #search_keyword#>
<cfset session.grant_from = #search_from#>
<cfset session.grant_to = #search_to#>

 <cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  insert into keyword_search
  (
   keyword_search_usr_id,
   keyword_search_company_id,
   keyword_search_hub_id,
   keyword_search_keyword,
   keyword_search_date,
   keyword_search_area
   )
   values
   (
   #session.usr_id#,
   #session.company_id#,
   <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
  '#search_keyword#',
   #now()#,
   3
   )
 </cfquery>

<cflocation URL="results.cfm" addtoken="no">