<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="state_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  where state_abbr = '#state#'
 </cfquery>

 <cfquery name="awardee_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) recipient_name from grants
  where recipient_duns = '#awardee#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as amount from grants
  where recipient_state_code = '#state#' and
        recipient_duns = '#awardee#' and
        recipient_country_code = 'USA' AND
        YEAR(action_date) = #session.grant_year#
 </cfquery>

 <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from grants
  where recipient_state_code = '#state#' and
        recipient_duns = '#awardee#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by action_date ASC
 		    <cfelseif #sv# is 10>
 		     order by action_date DESC
 		    <cfelseif #sv# is 2>
 		     order by award_id_fain ASC
 		    <cfelseif #sv# is 20>
 		     order by award_id_fain DESC
 		    <cfelseif #sv# is 3>
 		     order by awarding_agency_name ASC
 		    <cfelseif #sv# is 30>
 		     order by awarding_agency_name DESC
 		    <cfelseif #sv# is 4>
 		     order by awarding_sub_agency_name ASC
 		    <cfelseif #sv# is 40>
 		     order by awarding_sub_agency_name DESC
 		    <cfelseif #sv# is 5>
 		     order by federal_action_obligation DESC
 		    <cfelseif #sv# is 50>
 		     order by federal_action_obligation ASC
 		    </cfif>

 		   <cfelse>
             order by action_date DESC
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		 <cfparam name="URL.PageId" default="0">
		 <cfset RecordsPerPage = 250>
		 <cfset TotalPages = (grants.Recordcount/RecordsPerPage)>
		 <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		 <cfset EndRow = StartRow+RecordsPerPage-1>
		 <cfset counter = 0>
		 <cfset tot = 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
			  <tr><td class="feed_header">FEDERAL GRANTS</td>
				   <td class="feed_option" align=right><a href="/exchange/grants/detail_state.cfm?state=#state#"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" valign=top><b>#ucase(state_info.state_name)#</b></td>

             <form action="/exchange/grants/set2.cfm" method="post">
             <td align=right><span class="feed_sub_header">Select Year</span>&nbsp;&nbsp;

              <select name="year" class="input_select">
               <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
               <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
               <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
               <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
               <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
               <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
              </select>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Refresh">
             </td>

             <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">

			 	</td>
			 	</form>


			  </tr>


             <tr><td class="feed_option">
				 <b><a href="/exchange/include/federal_profile.cfm?duns=#awardee#" target="_blank" rel="noopener" rel="noreferrer"><u>#ucase(awardee_info.recipient_name)#</u></a><br><br>
				 <b>Total Grants:</b> #numberformat(grant_count.total,'999,999,999')#<br>
				 <b>Total Value: </b> #numberformat(grant_count.amount,'$99,999,999,999')#
             </cfoutput>

             </td><td class="feed_option" align=right valign=bottom>

                  <cfoutput>

					  <cfif grants.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#&state=#state#&awardee=#awardee#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

                   <td></tr>

         <tr><td height=5></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=50>
			 <cfoutput>
				<td class="feed_option" width=100><a href="awardee_grants.cfm?state=#state#&awardee=#awardee#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD DATE</b></a></td>
				<td class="feed_option"><a href="awardee_grants.cfm?state=#state#&awardee=#awardee#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARD ID</b></a></td>
				<td class="feed_option"><a href="awardee_grants.cfm?state=#state#&awardee=#awardee#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>DEPARTMENT</b></a></td>
				<td class="feed_option"><a href="awardee_grants.cfm?state=#state#&awardee=#awardee#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AGENCY</b></a></td>
				<td class="feed_option"><b>AWARD TITLE</b></td>
				<td class="feed_option" align=right width=100><a href="awardee_grants.cfm?state=#state#&awardee=#awardee#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AMOUNT</b></a></td>
			 </cfoutput>
			 </tr>

             <cfset ccounter = 0>

			 <cfloop query="grants">

             <cfif CurrentRow gte StartRow >

			 <cfif ccounter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

			 <cfoutput>

				  <td class="text_xsmall" valign=middle>#dateformat(grants.action_date,'mm/dd/yyyy')#</td>
				  <td width=150 class="text_xsmall" valign=middle><a href="/exchange/include/grant_information.cfm?id=#id#" target="blank"><b>#grants.award_id_fain#</b></a></td>
				  <td class="text_xsmall" valign=middle>#grants.awarding_agency_name#</td>
				  <td class="text_xsmall" valign=middle>#grants.awarding_sub_agency_name#</td>
				  <td class="text_xsmall" valign=middle>#grants.cfda_title#</td>
				  <td class="text_xsmall" valign=middle align=right>#numberformat(grants.federal_action_obligation,'$999,999,999,999')#</td>

             </cfoutput>

			  </tr>

			 <cfif ccounter is 0>
			  <cfset ccounter = 1>
			 <cfelse>
			  <cfset ccounter = 0>
			 </cfif>

             </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 <tr><td height=10></td></tr>
			 <tr><td class="text_xsmall" colspan=5><b>Note - </b> Grants are associated with the organizations DUNS number.  As such, there may be additional awards listed on this page that did not use the organizations registered SAMS organization name.</td></tr>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>