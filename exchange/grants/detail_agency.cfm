<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="agency_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_sub_agency_name from grants
  where awarding_sub_agency_code = '#agency#'
 </cfquery>

 <cfquery name="dept_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_agency_name from grants
  where awarding_agency_code = '#dept#'
 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as count, sum(federal_action_obligation) as total, count(distinct(recipient_duns)) as vendors from grants
  where awarding_agency_code = '#dept#' and
        awarding_sub_agency_code = '#agency#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
 </cfquery>

 <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select recipient_name, recipient_duns, count(id) as count, sum(federal_action_obligation) as total from grants
  where awarding_agency_code = '#dept#' and
        awarding_sub_agency_code = '#agency#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
  group by recipient_name, recipient_duns

		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by recipient_name ASC
 		    <cfelseif #sv# is 10>
 		     order by recipient_name DESC
 		    <cfelseif #sv# is 2>
 		     order by count DESC
 		    <cfelseif #sv# is 20>
 		     order by count ASC
 		    <cfelseif #sv# is 3>
 		     order by total DESC
 		    <cfelseif #sv# is 30>
 		     order by total ASC
 		    </cfif>

 		   <cfelse>
             order by total DESC
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

		 <cfparam name="URL.PageId" default="0">
		 <cfset RecordsPerPage = 250>
		 <cfset TotalPages = (grants.Recordcount/RecordsPerPage)>
		 <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
		 <cfset EndRow = StartRow+RecordsPerPage-1>
		 <cfset counter = 0>
		 <cfset tot = 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
			  <tr><td class="feed_header" valign=top>FEDERAL GRANTS</td>
				  <td class="feed_option" valign=top align=right><a href="/exchange/grants/detail_dept.cfm?dept=#dept#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" valign=top><b>#dept_info.awarding_agency_name#</b></td></tr>
			  <tr><td height=5></td></tr>
              <tr><td class="feed_sub_header"><b>#ucase(agency_info.awarding_sub_agency_name)#</b></a></td>

                           <form action="/exchange/grants/set2.cfm" method="post">
			               <td class="feed_option" align=right><b>Select Year</b>&nbsp;&nbsp;

			                <select name="year" class="input_select">
			                 <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
			                 <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
			                 <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
			                 <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
			                 <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
			                 <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
			                </select>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Refresh">
			               </td>

			               <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">

			             </td>
          </form>

              </tr>
              <tr><td height=5></td></tr>
          </cfoutput>

          <cfoutput>

             <tr><td class="feed_option">
				 <b>Total Grants: #numberformat(grant_total.count,'999,999,999')#<br>
				    Total Value: #numberformat(grant_total.total,'$99,999,999,999')#<br>
				    Total Awardees: #numberformat(grant_total.vendors,'999,999')#
             </cfoutput>

             </td><td class="feed_option" align=right valign=bottom width=50%>

                  <cfoutput>

					  <cfif grants.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#&dept=#dept#&agency=#agency#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

                   <td></tr>

         <tr><td height=5></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfoutput>
			 <tr height=40>
				<td class="feed_option"><a href="detail_agency.cfm?dept=#dept#&agency=#agency#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARDEE</b></a></td>
				<td class="feed_option"><a href="detail_agency.cfm?dept=#dept#&agency=#agency#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" align=right width=100><a href="detail_agency.cfm?dept=#dept#&agency=#agency#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AMOUNT</b></a></td>
			 </tr>
			 </cfoutput>

             <cfset ccounter = 0>

			 <cfloop query="grants">

             <cfif CurrentRow gte StartRow >

			 <cfif ccounter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

			 <cfoutput>

				  <td class="feed_option" valign=middle><a href="/exchange/grants/agency_vendor_grants.cfm?dept=#dept#&agency=#agency#&duns=#recipient_duns#"><b>#recipient_name#</b></a></td>
				  <td class="feed_option" valign=middle>#grants.count#</td>
				  <td class="feed_option" valign=middle align=right>#numberformat(grants.total,'$999,999,999,999')#</td>

             </cfoutput>

			  </tr>

			 <cfif ccounter is 0>
			  <cfset ccounter = 1>
			 <cfelse>
			  <cfset ccounter = 0>
			 </cfif>

             </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 <tr><td height=10></td></tr>
			 <tr><td class="text_xsmall" colspan=5><b>Note - </b> Grants are associated with the organizations DUNS number.  As such, there may be additional awards listed on this page that did not use the organizations registered SAMS organization name.</td></tr>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>