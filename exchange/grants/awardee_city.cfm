<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="state_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  where state_abbr = '#state#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as amount from grants
  where recipient_state_code = '#state#' and
        year(action_date) = #session.grant_year# and

        <cfif #city# is "">
        recipient_city_name is null and
        <cfelse>
        recipient_city_name = '#city#' and
        </cfif>
        recipient_country_code = 'USA'

 </cfquery>

 <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from grants
  where recipient_state_code = '#state#' and
        year(action_date) = #session.grant_year# and
        <cfif #city# is "">
        recipient_city_name is null and
        <cfelse>
        recipient_city_name = '#city#' and
        </cfif>
        recipient_country_code = 'USA'

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by award_id_fain ASC
 		    <cfelseif #sv# is 10>
 		     order by award_id_fain DESC
 		    <cfelseif #sv# is 2>
 		     order by action_date ASC
 		    <cfelseif #sv# is 20>
 		     order by action_date DESC
 		    <cfelseif #sv# is 3>
 		     order by recipient_name ASC
 		    <cfelseif #sv# is 30>
 		     order by recipient_name DESC
 		    <cfelseif #sv# is 4>
 		     order by awarding_sub_agency_name ASC
 		    <cfelseif #sv# is 40>
 		     order by awarding_sub_agency_name DESC
 		    <cfelseif #sv# is 5>
 		     order by federal_action_obligation DESC
 		    <cfelseif #sv# is 50>
 		     order by federal_action_obligation ASC
 		    <cfelseif #sv# is 6>
 		     order by awarding_agency_name ASC
 		    <cfelseif #sv# is 60>
 		     order by awarding_agency_name DESC
 		    </cfif>

 		   <cfelse>
 		     order by action_date DESC
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

	   <cfparam name="URL.PageId" default="0">
	   <cfset RecordsPerPage = 250>
	   <cfset TotalPages = (grants.Recordcount/RecordsPerPage)>
	   <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
	   <cfset EndRow = StartRow+RecordsPerPage-1>
	   <cfset counter = 0>
	   <cfset tot = 0>

      <div class="main_box">

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
			  <tr><td class="feed_header" valign=top>FEDERAL GRANTS</td>
				  <td class="feed_option" valign=top align=right><a href="/exchange/grants/detail_state.cfm?state=#state#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
			  <tr><td colspan=2><hr></td></tr>
			  <tr><td class="feed_sub_header" valign=top><b>#ucase(state_info.state_name)#, <cfif #city# is "">Unknown<cfelse>#ucase(city)#</cfif></b></td>

			               <form action="set2.cfm" method="post">
			               <td class="feed_option" align=right><b>Select Year</b>&nbsp;&nbsp;

			                <select name="year" class="input_select">
			                 <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
			                 <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
			                 <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
			                 <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
			                 <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
			                 <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
			                </select>&nbsp;&nbsp;<input class="button_blue" type="submit" name="button" value="Refresh">
			               </td>

			               <cfoutput>
			               <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">
             </cfoutput>

             </td>

             </form>

			  </tr>
			  <tr><td height=5></td></tr>
			  <tr><td class="feed_option">
			     <b>Total Grants: #numberformat(grant_count.total,'999,999,999')#<br>
				    Total Value: #numberformat(grant_count.amount,'$99,999,999,999')#<br>
				    Total Awardees: #numberformat(grant_count.vendors,'9,999')#</b></td>
		   </cfoutput>

		      <td class="feed_option" align=right valign=bottom width=75%>

                  <cfoutput>

					  <cfif grants.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#&state=#state#&city=#city#<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

			  </td></tr>

         <tr><td height=5></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr >

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <cfoutput>
			 <tr height=50>
				<td class="feed_option" width=75><a href="awardee_city.cfm?state=#state#&city=#city#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARD DATE</b></a></td>
				<td class="feed_option"><a href="awardee_city.cfm?state=#state#&city=#city#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>AWARD ID</b></a></td>
				<td class="feed_option"><a href="awardee_city.cfm?state=#state#&city=#city#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AWARDEE</b></a></td>
				<td class="feed_option"><a href="awardee_city.cfm?state=#state#&city=#city#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>DEPARTMENT</b></a></td>
				<td class="feed_option"><a href="awardee_city.cfm?state=#state#&city=#city#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AGENCY</b></a></td>
				<td class="feed_option"><b>AWARD TITLE</b></td>
				<td class="feed_option" align=right width=100><a href="awardee_city.cfm?state=#state#&city=#city#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>AMOUNT</b></a></td>
			 </tr>
			 </cfoutput>

             <cfset counter = 0>

			 <cfloop query="grants">

             <cfif CurrentRow gte StartRow >

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

			 <cfoutput>

				  <td class="text_xsmall" valign=middle width=100>#dateformat(grants.action_date,'mm/dd/yyyy')#</td>
				  <td class="text_xsmall" valign=middle width=150><a href="/exchange/include/grant_information.cfm?id=#id#" target="blank"><b>#grants.award_id_fain#</b></a></td>
				  <td class="text_xsmall" valign=middle><a href="/exchange/include/federal_profile.cfm?duns=#grants.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer"><b>#grants.recipient_name#</b></a></td>
				  <td class="text_xsmall" valign=middle>#grants.awarding_agency_name#</td>
				  <td class="text_xsmall" valign=middle>#grants.awarding_sub_agency_name#</td>
				  <td class="text_xsmall" valign=middle>#grants.cfda_title#</td>
				  <td class="text_xsmall" valign=middle align=right>#numberformat(grants.federal_action_obligation,'$999,999,999,999')#</td>

             </cfoutput>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

             </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>