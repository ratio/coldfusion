<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfset start_year = #session.grant_year# - 1>
 <cfset start_date = '10/1/' & #start_year#>
 <cfset end_date = '9/30/' & #session.grant_year#>

 <cfquery name="state_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  where state_abbr = '#state#'
 </cfquery>

 <cfquery name="awardee_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) recipient_city_name from grants
  where recipient_city_name = '#city#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total from grants
  where recipient_state_code = '#state#' and
        recipient_city_name = '#city#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select sum(federal_action_obligation) as total from grants
  where recipient_state_code = '#state#' and
        recipient_city_name = '#city#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
 </cfquery>

 <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from grants
  where recipient_state_code = '#state#' and
        recipient_city_name = '#city#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
  order by action_date DESC
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

	   <cfparam name="URL.PageId" default="0">
	   <cfset RecordsPerPage = 250>
	   <cfset TotalPages = (grants.Recordcount/RecordsPerPage)>
	   <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
	   <cfset EndRow = StartRow+RecordsPerPage-1>
	   <cfset counter = 0>
	   <cfset tot = 0>

      <div class="main_box">

         <form action="select.cfm" method="post">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
			  <tr><td class="feed_header">Grants</td>
				  <td class="feed_option" align=right><a href="detail_state.cfm?state=#state#">Return</a></td></tr>
			  <tr><td height=10></td></tr>
			  <tr><td class="feed_option" valign=top><b>State - #state_info.state_name#</b></td>
			      <td valign=top class="feed_option" align=right>

			      <b>Change Year: </b>&nbsp;

				  <select name="year" onchange="this.form.submit()">
					  <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
					  <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
					  <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
					  <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
					  <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
					  <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
				  </select>&nbsp;

			      </td></tr>

				 <input type="hidden" name="state" value="#state#">
				 <input type="hidden" name="city" value="#city#">
          <tr><td valign=top class="feed_option"><b>City: </b>#awardee_info.recipient_city_name#</td></tr>
          <tr><td class="feed_option">
				 <b>Total Grants:</b> #numberformat(grant_count.total,'999,999,999')#<br>
				 <b>Total Value: </b> #numberformat(grant_total.total,'$99,999,999,999')#</td>
		   </cfoutput>
		      <td class="feed_option" align=right valign=bottom>

                  <cfoutput>

					  <cfif grants.recordcount GT #RecordsPerPage#>
						  <b>Page: </b>&nbsp;|
						  <cfloop index="Pages" from="0" to="#TotalPages#">
						   <cfoutput>
						   <cfset DisplayPgNo = Pages+1>
							  <cfif URL.PageId eq pages>
								 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
							  <cfelse>
								 <a href="?pageid=#pages#&state=#state#&city=#city#">#DisplayPgNo#</a>&nbsp;|&nbsp;
							  </cfif>
						   </cfoutput>
						  </cfloop>
					   </cfif>

				   </cfoutput>

			  </td></tr>

         <tr><td height=5></td></tr>

         </table>

         <input type="hidden" name="loc" value=5>

         </form>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr>
				<td class="text_xsmall" width=120><b>Award ID</b></td>
				<td class="text_xsmall" width=75><b>Award Date</b></td>
				<td class="text_xsmall"><b>Awardee</b></td>
				<td class="text_xsmall"><b>Agency</b></td>
				<td class="text_xsmall"><b>Award Title</b></td>
				<td class="text_xsmall" align=right width=100><b>Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfloop query="grants">

             <cfif CurrentRow gte StartRow >

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>

			 <cfoutput>

				  <td class="text_xsmall"><a href="detail.cfm">#grants.award_id_fain#</a></td>
				  <td class="text_xsmall">#dateformat(grants.action_date,'mm/dd/yyyy')#</td>
				  <td class="text_xsmall"><a href="/exchange/include/federal_profile.cfm?duns=#grants.recipient_duns#" target="_blank" rel="noopener" rel="noreferrer">#grants.recipient_name#</a></td>
				  <td class="text_xsmall">#grants.awarding_sub_agency_name#</td>
				  <td class="text_xsmall">#grants.cfda_title#</td>
				  <td class="text_xsmall" align=right>#numberformat(grants.federal_action_obligation,'$999,999,999,999')#</td>

             </cfoutput>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

             </cfif>

			 <cfif CurrentRow eq EndRow>
			  <cfbreak>
			 </cfif>

			 </cfloop>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>