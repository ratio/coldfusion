<cfinclude template="/exchange/security/check.cfm">

<cfif not isdefined("session.grant_year")>
 <cfset session.grant_year = 2018>
</cfif>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfquery name="dept_grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select sum(federal_action_obligation) as amount, awarding_agency_code, awarding_agency_name from grants
  where YEAR(action_date) = #session.grant_year#
  group by awarding_agency_code, awarding_agency_name
  order by awarding_agency_name
 </cfquery>

 <cfquery name="state_grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select sum(federal_action_obligation) as amount, recipient_state_code from grants
  where YEAR(action_date) = #session.grant_year# and
        recipient_state_code is not null
  group by recipient_state_code
  order by recipient_state_code
 </cfquery>

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top>

	  <div class="main_box">
	   <cfinclude template="/exchange/grants/grant_search.cfm">
	  </div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <form action="set2.cfm" method="post">

         <tr><td class="feed_header">SNAPSHOTS</td>
             <td align=right><span class="feed_sub_header">Select Year</span>&nbsp;&nbsp;
              <select name="year" class="input_select" onchange="form.submit();">
               <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
               <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
               <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
               <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
               <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
               <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
              </select>
             </td>

             </tr>
             <cfoutput>
             <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">
             </cfoutput>

         <tr><td colspan=2><hr></td></tr>
         </form>

        </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=40>
				<td class="feed_option" width=75><b>BY STATE</b></td>
				<td class="feed_option" align=right><b>TOTAL AWARDS</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="state_grants">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="/exchange/grants/detail_state.cfm?state=#recipient_state_code#"><b>#recipient_state_code#</b></a></td>
				  <td class="feed_option" align=right>#numberformat(amount,'$999,999,999')#</td>
			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td valign=top width=40>&nbsp;</td>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr height=40>
				<td class="feed_option"><b>BY DEPARTMENT</b></td>
				<td class="feed_option" align=right><b>TOTAL AWARDS</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="dept_grants">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="/exchange/grants/detail_dept.cfm?dept=#awarding_agency_code#"><b>#awarding_agency_name#</b></a></td>
				  <td class="feed_option" align=right>#numberformat(amount,'$999,999,999,999')#</td>
			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td>
         </tr>

        </table>

       </td></tr>

     </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>