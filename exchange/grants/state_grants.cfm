<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfset start_year = #session.grant_year# - 1>
 <cfset start_date = '10/1/' & #start_year#>
 <cfset end_date = '9/30/' & #session.grant_year#>

 <cfquery name="state_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  where state_abbr = '#state#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total from grants
  where recipient_state_code = '#state#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select sum(federal_action_obligation) as total from grants
  where recipient_state_code = '#state#' and
        ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
 </cfquery>

 <cfquery name="vend" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(100) count(id) as total, sum(federal_action_obligation) as value, recipient_duns, recipient_name from grants
  where recipient_state_code = '#state#'
  and ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
  group by recipient_duns, recipient_name
  order by value DESC
 </cfquery>

 <cfquery name="cities" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as value, count(distinct(recipient_duns)) as vendors, recipient_city_name from grants
  where recipient_state_code = '#state#'
  and ((action_date >= '#start_date#') and (action_date <= '#end_date#'))
  group by recipient_city_name
  order by recipient_city_name ASC
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

         <form action="select.cfm" method="post">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header">Grants</td>
              <td class="feed_option" align=right><a href="index.cfm">Return</a></td></tr>
          <tr><td height=10></td></tr>
           <tr><td class="feed_header" valign=top>
          <cfoutput>
             <input type="hidden" name="state" value="#state#">
             <b>State - #state_info.state_name#</b><br>
             <span class="feed_option"><b>Total Grants:</b> #numberformat(grant_count.total,'999,999,999')#<br>
             <b>Total Value: </b> #numberformat(grant_total.total,'$99,999,999,999')#</span></td>
          </cfoutput>
             <td valign=top align=right class="feed_option"><b>Change Year: </b>&nbsp;
             <select name="year" onchange="this.form.submit()">
              <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
              <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
              <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
              <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
              <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
              <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
             </select>

             &nbsp;&nbsp;</td>

             </td></tr>

         <tr><td height=5></td></tr>

         </table>

         <input type="hidden" name="loc" value=3>

         </form>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr>
				<td class="feed_option" width=50><b>By City</b></td>
				<td class="feed_option" align=right><b>Awards</b></td>
				<td class="feed_option" align=right><b>Awardees</b></td>
				<td class="feed_option" align=right><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="cities">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>

				  <td class="feed_option"><a href="grants_state.cfm">#recipient_city_name#</a></td>
				  <td class="feed_option" align=right>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(vendors,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td valign=top width=2%>&nbsp;</td>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <tr>
				<td class="feed_option"><b>Top 100 Awardees</b></td>
				<td class="feed_option" width=100 align=right><b>Awards</b></td>
				<td class="feed_option" align=right width=150><b>Total Value</b></td>
			 </tr>

             <cfset counter = 0>

			 <cfoutput query="vend">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff">
			 <cfelse>
			  <tr bgcolor="e0e0e0">
			 </cfif>

				  <td class="feed_option"><a href="detail_agency.cfm">#recipient_name#</a></td>
				  <td class="feed_option" align=right>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

             <tr><td height=10></td></tr>
			 <tr><td colspan=2 class="feed_option"><a href="all.cfm">ALL AWARDEES</a></td></tr>

			 </table>

         </td>

         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>