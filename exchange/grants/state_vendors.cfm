<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="dept_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_agency_name from grants
  where awarding_agency_code = '#dept#'
 </cfquery>

 <cfquery name="state_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select * from state
  where state_abbr = '#state#'
 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as count, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as total from grants
  where awarding_agency_code = '#dept#' and
        recipient_state_code = '#state#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#

 </cfquery>

 <cfquery name="vendors" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select recipient_name, recipient_city_name, recipient_duns, count(id) as count, sum(federal_action_obligation) as total from grants
  where awarding_agency_code = '#dept#' and
        recipient_state_code = '#state#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
  group by recipient_duns, recipient_name, recipient_city_name


 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by recipient_name ASC
 		    <cfelseif #sv# is 10>
 		     order by recipient_name DESC
 		    <cfelseif #sv# is 2>
 		     order by recipient_city_name ASC,total DESC
 		    <cfelseif #sv# is 20>
 		     order by recipient_city_name DESC, total DESC
 		    <cfelseif #sv# is 3>
 		     order by count DESC
 		    <cfelseif #sv# is 30>
 		     order by count ASC
 		    <cfelseif #sv# is 4>
 		     order by total DESC
 		    <cfelseif #sv# is 40>
 		     order by total ASC
 		    </cfif>

 		   <cfelse>
             order by total DESC
           </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <cfoutput>
          <tr><td class="feed_header">FEDERAL GRANTS</td>
              <td class="feed_option" align=right><a href="/exchange/grants/detail_dept.cfm?dept=#dept#"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
          <tr><td colspan=2><hr></td></tr>
           <tr><td class="feed_sub_header" valign=top>#dept_info.awarding_agency_name#</td></tr>
           <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header" valign=top><b>#ucase(state_info.state_name)#</b></td>

             <form action="/exchange/grants/set2.cfm" method="post">
             <td align=right><span class="feed_sub_header">Select Year</span>&nbsp;&nbsp;

              <select name="year" class="input_select" onchange="form.submit();">
               <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
               <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
               <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
               <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
               <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
               <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
              </select>
             </td>

             <cfoutput>
             <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">
             </cfoutput>

          </td>
          </form>

           </tr>
           <tr><td height=5></td></tr>
           <tr><td class="feed_option">

             <b>Total Grants: #numberformat(grant_total.count,'999,999,999')#<br>
                Total Value: #numberformat(grant_total.total,'$99,999,999,999')#<br>
                Total Partners: #numberformat(grant_total.vendors,'999,999')#<br>
                </b></td>
          </cfoutput>
             <td valign=top align=right class="feed_option">

             </td></tr>

         <tr><td height=5></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top width=25%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <cfoutput>
			 <tr height=40>
				<td class="feed_option"><a href="state_vendors.cfm?dept=#dept#&state=#state#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>PARTNER</b></a></td>
				<td class="feed_option"><a href="state_vendors.cfm?dept=#dept#&state=#state#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>CITY</b></a></td>
				<td class="feed_option" align=center><a href="state_vendors.cfm?dept=#dept#&state=#state#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" align=right><a href="state_vendors.cfm?dept=#dept#&state=#state#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>AMOUNT</b></a></td>
			 </tr>
			 </cfoutput>
             <cfset counter = 0>

			 <cfoutput query="vendors">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="dept_state_vendor_grants.cfm?dept=#dept#&state=#state#&duns=#recipient_duns#"><b>#recipient_name#</b></a></td>
				  <td class="feed_option">#recipient_city_name#</a></td>
				  <td class="feed_option" align=center>#numberformat(count,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(total,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

			 </td></tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>