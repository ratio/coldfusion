<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfif not isdefined("session.grant_year")>
  <cfset session.grant_year = 2018>
 </cfif>

 <cfquery name="dept_info" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select top(1) awarding_agency_name from grants
  where awarding_agency_code = '#dept#'
 </cfquery>

 <cfquery name="grant_count" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, count(distinct(recipient_duns)) as vendors from grants
  where year(action_date) = #session.grant_year# and
        awarding_agency_code = '#dept#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#

 </cfquery>

 <cfquery name="grant_total" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select sum(federal_action_obligation) as total from grants
  where year(action_date) = #session.grant_year# and
        awarding_agency_code = '#dept#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#

 </cfquery>

 <cfquery name="vend" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as value, recipient_duns, recipient_name from grants
  where year(action_date) = #session.grant_year# and
        awarding_agency_code = '#dept#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
  group by recipient_duns, recipient_name

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 5>
 		     order by recipient_name ASC
 		    <cfelseif #sv# is 50>
 		     order by recipient_name DESC
 		    <cfelseif #sv# is 6>
 		     order by total DESC
 		    <cfelseif #sv# is 60>
 		     order by total ASC
 		    <cfelseif #sv# is 7>
 		     order by value DESC
 		    <cfelseif #sv# is 70>
 		     order by value ASC
            <cfelse>
              order by value DESC
 		    </cfif>

 		   <cfelse>
             order by value DESC
		   </cfif>

 </cfquery>

 <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select count(id) as total, sum(federal_action_obligation) as value, count(distinct(recipient_duns)) as vendors, awarding_sub_agency_code, awarding_sub_agency_name from grants
  where year(action_date) = #session.grant_year# and
        awarding_agency_code = '#dept#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
  group by awarding_sub_agency_code, awarding_sub_agency_name

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 8>
 		     order by awarding_sub_agency_name ASC
 		    <cfelseif #sv# is 80>
 		     order by awarding_sub_agency_name DESC
 		    <cfelseif #sv# is 9>
 		     order by total DESC
 		    <cfelseif #sv# is 90>
 		     order by total ASC
 		    <cfelseif #sv# is 10>
 		     order by vendors DESC
 		    <cfelseif #sv# is 100>
 		     order by vendors ASC
 		    <cfelseif #sv# is 11>
 		     order by value DESC
 		    <cfelseif #sv# is 110>
 		     order by value ASC
            <cfelse>
              order by value DESC
 		    </cfif>

 		   <cfelse>
             order by value DESC
		   </cfif>

 </cfquery>

 <cfquery name="state_grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select recipient_state_code, count(id) as total, count(distinct(recipient_duns)) as vendors, sum(federal_action_obligation) as value from grants
  where awarding_agency_code = '#dept#' and
        recipient_country_code = 'USA' and
        year(action_date) = #session.grant_year#
  group by recipient_state_code

 		   <cfif isdefined("sv")>

 		    <cfif #sv# is 1>
 		     order by recipient_state_code DESC
 		    <cfelseif #sv# is 10>
 		     order by recipient_state_code ASC
 		    <cfelseif #sv# is 2>
 		     order by total DESC
 		    <cfelseif #sv# is 20>
 		     order by total ASC
 		    <cfelseif #sv# is 3>
 		     order by vendors DESC
 		    <cfelseif #sv# is 30>
 		     order by vendors ASC
 		    <cfelseif #sv# is 4>
 		     order by value DESC
 		    <cfelseif #sv# is 40>
 		     order by value ASC
            <cfelse>
             order by recipient_state_code
 		    </cfif>

 		   <cfelse>
             order by recipient_state_code
		   </cfif>

 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

         <form action="/exchange/grants/set2.cfm" method="post">

         <cfoutput>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_header" valign=top>FEDERAL GRANTS</td>
              <td class="feed_option" valign=top align=right><a href="/exchange/grants/"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
          <tr><td colspan=2><hr></td></tr>
          <tr><td class="feed_sub_header" valign=top><b>#dept_info.awarding_agency_name#</b></td>

             <td align=right><span class="feed_sub_header">Select Year</span>&nbsp;&nbsp;

              <select name="year" class="input_select" onchange="form.submit();">
               <option value=2018 <cfif session.grant_year is 2018>selected</cfif>>2018
               <option value=2017 <cfif session.grant_year is 2017>selected</cfif>>2017
               <option value=2016 <cfif session.grant_year is 2016>selected</cfif>>2016
               <option value=2015 <cfif session.grant_year is 2015>selected</cfif>>2015
               <option value=2014 <cfif session.grant_year is 2014>selected</cfif>>2014
               <option value=2013 <cfif session.grant_year is 2013>selected</cfif>>2013
              </select>
              <input type="hidden" name="location" value="#cgi.script_name#?#cgi.query_string#">

               </td></tr>
          <tr><td class="feed_option">
             <b>Total Grants: #numberformat(grant_count.total,'999,999,999')#</b><br>
             <b>Total Value: #numberformat(grant_total.total,'$99,999,999,999')#</b><br>
             <b>Total Awardees: #numberformat(grant_count.vendors,'999,999,999')#</b></td>
             </tr>

         <tr><td height=5></td></tr>

          </form>

         </table>

         </cfoutput>


         <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=5><hr></td></tr>
         <tr><td height=10></td></tr>

		 <tr>

		     <td valign=top width=25%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

			 <cfoutput>

			 <tr height=40>
				<td class="feed_option" width=75><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>BY STATE</b></a></td>
				<td class="feed_option" align=center><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" align=center><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>PARTNERS</b></a></td>
				<td class="feed_option" align=right><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>TOTAL</b></a></td>
			 </tr>

			 </cfoutput>

             <cfset counter = 0>

			 <cfoutput query="state_grants">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="state_vendors.cfm?dept=#dept#&state=#recipient_state_code#"><b>#recipient_state_code#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=center>#numberformat(vendors,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td><td valign=top width=2%>&nbsp;</td>

		     <td valign=top width=35%>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <cfoutput>
			 <tr height=40>
				<td class="feed_option"><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>BY PARTNER</b></a></td>
				<td class="feed_option" width=100 align=center><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=6<cfelse><cfif #sv# is 6>sv=60<cfelse>sv=6</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" align=right width=150><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=7<cfelse><cfif #sv# is 7>sv=70<cfelse>sv=7</cfif></cfif>"><b>TOTAL</b></a></td>
			 </tr>
			 </cfoutput>

             <cfset counter = 0>

			 <cfoutput query="vend">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="dept_vendor.cfm?dept=#dept#&duns=#recipient_duns#"><b>#recipient_name#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

         </td>

             <td valign=top width=2%>&nbsp;</td>

		     <td valign=top>

			 <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfoutput>
			 <tr height=40>
				<td class="feed_option"><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=8<cfelse><cfif #sv# is 8>sv=80<cfelse>sv=8</cfif></cfif>"><b>BY AGENCY</b></a></td>
				<td class="feed_option" width=100 align=center><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=9<cfelse><cfif #sv# is 9>sv=90<cfelse>sv=9</cfif></cfif>"><b>AWARDS</b></a></td>
				<td class="feed_option" width=100 align=center><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=10<cfelse><cfif #sv# is 10>sv=100<cfelse>sv=10</cfif></cfif>"><b>PARTNER</b></a></td>
				<td class="feed_option" align=right width=150><a href="detail_dept.cfm?dept=#dept#&<cfif not isdefined("sv")>sv=11<cfelse><cfif #sv# is 11>sv=110<cfelse>sv=11</cfif></cfif>"><b>TOTAL</b></a></td>
			 </tr>
			 </cfoutput>

             <cfset counter = 0>

			 <cfoutput query="agencies">

			 <cfif counter is 0>
			  <tr bgcolor="ffffff" height=30>
			 <cfelse>
			  <tr bgcolor="e0e0e0" height=30>
			 </cfif>

				  <td class="feed_option"><a href="detail_agency.cfm?dept=#dept#&agency=#awarding_sub_agency_code#"><b>#awarding_sub_agency_name#</b></a></td>
				  <td class="feed_option" align=center>#numberformat(total,'99,999')#</td>
				  <td class="feed_option" align=center>#numberformat(vendors,'99,999')#</td>
				  <td class="feed_option" align=right>#numberformat(value,'$999,999,999,999')#</td>

			  </tr>

			 <cfif counter is 0>
			  <cfset counter = 1>
			 <cfelse>
			  <cfset counter = 0>
			 </cfif>

			 </cfoutput>

			 </table>

             </td>
         </tr>

        </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>