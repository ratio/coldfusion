<cfinclude template="/exchange/security/check.cfm">

<cfquery name="opp_data" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<html>
<head>
	<title>Exchange</title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

	  <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfif usr.hub_xref_role_id is 1>
	      <cfinclude template="/exchange/portfolio/recent.cfm">
      <cfelse>
	      <cfinclude template="/exchange/profile_company.cfm">
      </cfif>

      </td><td valign=top>

	  <div class="main_box">

          <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
			   <tr><td class="feed_header">COMPANY SNAPSHOTS</td><td align=right class="feed_option"><a href="/exchange/"><img src="/images/delete.png" width=20 border=0></a></td></tr>
			   <tr><td colspan=2><hr></td></tr>
			   <tr><td class="feed_sub_header" style="font-weight: normal;">Please update the keywords below to display Company Snapshots on the homepage.</td></tr>
			   <tr><td height=10></td></tr>
			  </table>

		   <form action="/exchange/company_config_save.cfm" method="post">

 			 <table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header">TOPIC AREAS</td></tr>
			  <tr><td><input name="usr_dashboard_comp_keywords" type="text" class="input_text" size=100 maxlength="150" value="#opp_data.usr_dashboard_comp_keywords#"></td></tr>
			  <tr><td class="link_small_gray" colspan=2>Company keywords allow you to filter Companies based on matching name, description or capability keywords (i.e., Machine Learning, Clinical, etc.).</td></tr>

             <tr><td height=20></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr><td height=10></td></tr>
			 <tr><td><input type="submit" name="button" class="button_blue_large" value="Update"</td></tr>

             </table>


          </cfoutput>

          </form>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

