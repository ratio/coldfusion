
<cfquery name="list" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select distinct(venture), venture_description, impact_objectives, Alligning_SDG, venture_industry, seed_spot_focus_area, location, first_name, last_name, gender_identity, race from seedspot
 where venture is not null
 order by venture
</cfquery>

<table cellpadding=0 cellspacing=0 border=1 width=100%>

<cfloop query="list">

<cfquery name="match" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select top(1) company_id from company
 where company_name = '#list.venture#'
</cfquery>

<cfif match.recordcount is 1>
match<br>

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_comp_id from hub_comp
		  where hub_comp_hub_id = #session.hub# and
		        hub_comp_company_id = #match.company_id#
		</cfquery>

		<cfif check.recordcount GT 0>

		<cfelse>

			<cfquery name="align" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into hub_comp
			 (
			  hub_comp_hub_id,
			  hub_comp_company_id,
			  hub_comp_added
			  )
			  values
			  (
			  #session.hub#,
			  #match.company_id#,
			  #now()#
			  )
			</cfquery>

		</cfif>

<cfelseif match.recordcount is 0>
No match<br>

		<cfset company_name = #list.venture#>
		<cfset company_desc = #list.venture_description#>

		<cfif list.seed_spot_focus_area is not "">
			<cfset company_keywords = #list.seed_spot_focus_area#>
		<cfelse>
			<cfset company_keywords = #list.venture_industry#>
		</cfif>

		<cfset company_city = #trim(listfirst(list.location))#>
		<cfset company_state = #trim(listlast(list.location))#>
		<cfset company_poc_first_name = #list.first_name#>
		<cfset company_impact_objectives = #list.impact_objectives#>
		<cfset company_goals = #list.Alligning_SDG#>
		<cfset company_poc_last_name = #list.last_name#>
		<cfset company_poc_gender = #list.gender_identity#>
		<cfset company_poc_race = #list.race#>

		<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 insert into company
		 (
		  company_name,
		  company_about,
		  company_long_desc,
		  company_keywords,
		  company_city,
		  company_state,
		  company_poc_first_name,
		  company_poc_last_name,
		  company_poc_gender,
		  company_poc_race,
		  company_impact_objectives,
		  company_goals,
		  company_updated,
		  company_discoverable
		  )
		  values
		  (
		  '#company_name#',
		  '#company_desc#',
		  '#company_desc#',
		  '#company_keywords#',
		  '#company_city#',
		  '#company_state#',
		  '#company_poc_first_name#',
		  '#company_poc_last_name#',
		  '#company_poc_gender#',
		  '#company_poc_race#',
		  '#company_impact_objectives#',
		  '#company_goals#',
		   #now()#,
		   1
		  )
		</cfquery>

		<cfquery name="max" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 select max(company_id) as id from company
		</cfquery>

		<cfquery name="hubcop" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into hub_comp
		 (
		  hub_comp_hub_id,
		  hub_comp_company_id,
		  hub_comp_added
		  )
		  values
		  (
		  #session.hub#,
		  #max.id#,
		  #now()#
		  )
		</cfquery>

    </cfif>

</cfloop>

</table>