<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add">

<cfif #hub_sponsor_file# is not "">
 <cffile action = "upload"
   fileField = "hub_sponsor_file"
   destination = "#media_path#"
   nameConflict = "MakeUnique">
</cfif>

<cfquery name="sponsor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 insert into hub_sponsor
 (hub_sponsor_active, hub_sponsor_poc_name, hub_sponsor_poc_email, hub_sponsor_poc_phone, hub_sponsor_order, hub_sponsor_hub_id, hub_sponsor_file, hub_sponsor_name, hub_sponsor_desc, hub_sponsor_url, hub_sponsor_created, hub_sponsor_usr_id)
 values
 (#hub_sponsor_active#,'#hub_sponsor_poc_name#','#hub_sponsor_poc_email#','#hub_sponsor_poc_phone#',#hub_sponsor_order#, #session.hub#, <cfif #hub_sponsor_file# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,'#hub_sponsor_name#','#hub_sponsor_desc#','#hub_sponsor_url#',#now()#,#session.usr_id#)
</cfquery>

<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif #button# is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_sponsor_file from hub_sponsor
		  where hub_sponsor_id = #hub_sponsor_id# and
		        hub_sponsor_hub_id = #session.hub#
		</cfquery>

        <cfif fileexists("#media_path#\#remove.hub_sponsor_file#")>
			<cffile action = "delete" file = "#media_path#\#remove.hub_sponsor_file#">
        </cfif>

	</cfif>

	<cfif #hub_sponsor_file# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select hub_sponsor_file from hub_sponsor
		  where hub_sponsor_id = #hub_sponsor_id#
		</cfquery>

		<cfif #getfile.hub_sponsor_file# is not "">
	        <cfif fileexists("#media_path#\#getfile.hub_sponsor_file#")>
			 <cffile action = "delete" file = "#media_path#\#getfile.hub_sponsor_file#">
            </cfif>
		</cfif>

		<cffile action = "upload"
		 fileField = "hub_sponsor_file"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update hub_sponsor
	  set hub_sponsor_name = '#hub_sponsor_name#',

		  <cfif #hub_sponsor_file# is not "">
		   hub_sponsor_file = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   hub_sponsor_file = null,
		  </cfif>

         hub_sponsor_desc = '#hub_sponsor_desc#',
         hub_sponsor_active = #hub_sponsor_active#,
         hub_sponsor_poc_name = '#hub_sponsor_poc_name#',
         hub_sponsor_poc_email = '#hub_sponsor_poc_email#',
         hub_sponsor_poc_phone = '#hub_sponsor_poc_phone#',
         hub_sponsor_order = #hub_sponsor_order#,
         hub_sponsor_url = '#hub_sponsor_url#'
       where hub_sponsor_id = #hub_sponsor_id# and
             hub_sponsor_hub_id = #session.hub#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif #button# is "Delete">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select hub_sponsor_file from hub_sponsor
	  where (hub_sponsor_id = #hub_sponsor_id#) and
	        (hub_sponsor_hub_id = #session.hub#)
	</cfquery>

    <cfif remove.hub_sponsor_file is not "">
        <cfif fileexists("#media_path#\#remove.hub_sponsor_file#")>
		 <cffile action = "delete" file = "#media_path#\#remove.hub_sponsor_file#">
	    </cfif>
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete hub_sponsor
	  where (hub_sponsor_id = #hub_sponsor_id#) and
	        (hub_sponsor_hub_id = #session.hub#)
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>