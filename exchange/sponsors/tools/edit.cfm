<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="sponsor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_sponsor
 where hub_sponsor_id = #hub_sponsor_id# and
       hub_sponsor_hub_id = #session.hub#
</cfquery>

  <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=175>

       <cfinclude template="/exchange/admin/admin_menu.cfm">

      </td><td valign=top>

	  <div class="main_box">

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_header">Edit Sponsor</td>
	       <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
	   <tr><td colspan=2><hr></td></tr>

           <cfoutput>

           <form action="db.cfm" method="post" enctype="multipart/form-data">

                <tr><td class="feed_sub_header" width=150>Sponsor Name</td>
                    <td><input type="text" class="input_text" name="hub_sponsor_name" size=50 value="#sponsor.hub_sponsor_name#" required></td></tr>

                <tr><td class="feed_sub_header" valign=top>Description</td>
                    <td><textarea name="hub_sponsor_desc" class="input_textarea" style="width: 800px; height: 100px;">#sponsor.hub_sponsor_desc#</textarea></td></tr>

                <tr><td class="feed_sub_header" width=200>Point of Contact (POC)</td>
                    <td><input type="text" class="input_text" name="hub_sponsor_poc_name" value="#sponsor.hub_sponsor_poc_name#"></td></tr>

                <tr><td class="feed_sub_header">POC Email</td>
                    <td><input type="email" class="input_text" name="hub_sponsor_poc_email" value="#sponsor.hub_sponsor_poc_email#"></td></tr>

                <tr><td class="feed_sub_header">POC Phone</td>
                    <td><input type="text" class="input_text" name="hub_sponsor_poc_phone" value="#sponsor.hub_sponsor_poc_phone#"></td></tr>

				<tr><td class="feed_sub_header">Sponsor URL</td>
                    <td><input name="hub_sponsor_url" type="url" value="#sponsor.hub_sponsor_url#" class="input_text" size=50 maxlength="100"></td></tr>

				<tr><td class="feed_sub_header">Logo / Picture</td>

					<td class="feed_sub_header" style="font-weight: normal;">

					<cfif #sponsor.hub_sponsor_file# is "">
					  <input type="file" name="hub_sponsor_file">
					<cfelse>
					  View Current Logo: <a href="#media_virtual#/#sponsor.hub_sponsor_file#" target="_blank" rel="noopener" rel="noreferrer">#sponsor.hub_sponsor_file#</a><br><br>
					  <input type="file" name="hub_sponsor_file"><br><br>
					  <input type="checkbox" name="remove_attachment">&nbsp;or, check to remove logo
					 </cfif>


					 </td></tr>


				<tr><td class="feed_sub_header">Display</td>
			        <td>
			        <select name="hub_sponsor_active" class="input_select">
			         <option value=1 <cfif #sponsor.hub_sponsor_active# is 1>selected</cfif>>Yes
			         <option value=0 <cfif #sponsor.hub_sponsor_active# is 0>selected</cfif>>No
			        </select>

			        <span class="link_small_gray">Determines whether Sponsor will be displayed.</span>

			        </td></tr>

				<tr><td class="feed_sub_header">Order</td>
			        <td><input type="number" name="hub_sponsor_order" value=#sponsor.hub_sponsor_order# class="input_text" required style="width: 100px;">

			        <span class="link_small_gray">Determines the order the Sponsor will be displayed when viewing all Sponsors.</span>

			        </td></tr>

  			    <tr><td height=5></td></tr>
  			    <tr><td colspan=2><hr></td></tr>
  			    <tr><td height=5></td></tr>

   			    <tr><td></td><td><input class="button_blue_large" type="submit" name="button" value="Update" vspace=10>&nbsp;&nbsp;
              			         <input class="button_blue_large" type="submit" name="button" value="Delete" vspace=10 onclick="return confirm('Delete Sponsor.\r\nAre you sure you want to delete this sponsor?');">&nbsp;&nbsp;
   			            </td></tr>

			   <input type="hidden" name="hub_sponsor_id" value=#hub_sponsor_id#>

			   </form>

			   </cfoutput>

	  </table>

	  </div>

	  </td></tr>

  </table>

  </div>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>