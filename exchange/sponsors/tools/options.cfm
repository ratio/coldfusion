<cfquery name="sponsors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_sponsor
 where hub_sponsor_hub_id = #session.hub#
 order by hub_sponsor_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td class="feed_header">Sponsors Configuration</td>
      <td class="feed_sub_header" align=right><a href="/exchange/admin/apps/myapps.cfm">My Apps</a></td></tr>
  <tr><td colspan=2><hr></td></tr>
  <tr><td height=10></td></tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
<tr>

    <td class="feed_sub_header">

     <cfif isdefined("u")>
      <cfif u is 1>
       <span style="color: green;">Sponsor has been successfully added.</span>
      <cfelseif u is 2>
       <span style="color: green;">Sponsor has been successfully updated.</span>
      <cfelseif u is 3>
       <span style="color: red;">Sponsor has been successfully deleted.</span>
      </cfif>
     </cfif>

    </td>
    <td class="feed_sub_header" align=right><a href="add.cfm"><img src="/images/plus3.png" width=15 hspace=10></a><a href="add.cfm">Add Sponsor</a></td></tr>

</table>

<cfif sponsors.recordcount is 0>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
	 <tr><td class="feed_sub_header" style="font-weight: normal;">No Sponsors have been added.</td></tr>
	</table>

<cfelse>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>

	 <tr>
		 <td></td>
		 <td class="feed_sub_header">Name</td>
		 <td class="feed_sub_header">Description</td>
		 <td class="feed_sub_header" align=center>Order</td>
		 <td class="feed_sub_header" align=right>Display</td>
	 </tr>

    <cfset counter = 0>
	<cfloop query="sponsors">

		<cfoutput>

         <cfif counter is 0>
          <tr bgcolor="ffffff" height=60>
         <cfelse>
          <tr bgcolor="e0e0e0" height=60>
         </cfif>

			 <td class="feed_sub_header" width=70><a href="edit.cfm?hub_sponsor_id=#sponsors.hub_sponsor_id#"><img src="#media_virtual#/#sponsors.hub_sponsor_file#" width=40 height=40 hspace=5></td>
			 <td class="feed_sub_header" width=300><a href="edit.cfm?hub_sponsor_id=#sponsors.hub_sponsor_id#">#sponsors.hub_sponsor_name#</a></td>
			 <td class="feed_sub_header">#sponsors.hub_sponsor_desc#</td>
			 <td class="feed_sub_header" align=center>#sponsors.hub_sponsor_order#</td>
			 <td class="feed_sub_header" align=right width=75><cfif #sponsors.hub_sponsor_active# is 1>Yes<cfelse>No</cfif></td>
		 </tr>

		 <cfif counter is 0>
		  <cfset counter = 1>
		 <cfelse>
		  <cfset counter = 0>
		 </cfif>

		</cfoutput>

	</cfloop>

</cfif>

</table>

