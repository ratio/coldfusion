<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

<cfquery name="sponsors" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub_sponsor
 where hub_sponsor_hub_id = #session.hub#
 order by hub_sponsor_order
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">

      </td><td valign=top>

		<div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  	<tr><td class="feed_header">Sponsors</td></tr>
		  	<tr><td><hr></td></tr>
		  	<tr><td height=10></td></tr>
		  </table>

			<cfif sponsors.recordcount is 0>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>
				 <tr><td class="feed_sub_header" style="font-weight: normal;">No Sponsors have been added.</td></tr>
				</table>

			<cfelse>

				<table cellspacing=0 cellpadding=0 border=0 width=100%>

				<cfset counter = 1>
				<cfloop query="sponsors">

					<cfoutput>

					 <tr>
						 <td class="feed_sub_header" valign=top width=150>

							 <cfif #sponsors.hub_sponsor_url# is not "">
                               <cfif #sponsors.hub_sponsor_file# is not "">
                                 <a href="#sponsors.hub_sponsor_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#sponsors.hub_sponsor_file#" width=125></a>
							   <cfelse>
							     <a href="#sponsors.hub_sponsor_url#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/stock_sponsor.png" width=125></a>
							   </cfif>
							 <cfelse>
                               <cfif #sponsors.hub_sponsor_file# is not "">
                                 <img src="#media_virtual#/#sponsors.hub_sponsor_file#" width=125>
							   <cfelse>
							     <img src="/images/stock_sponsor.png" width=125>
							   </cfif>
							 </cfif>

						 </td>

						 </td><td valign=top>


							<table cellspacing=0 cellpadding=0 border=0 width=100%>

							 <tr><td height=10></td></tr>
							 <tr><td class="feed_header">

							 <cfif #sponsors.hub_sponsor_url# is not "">
                               <a href="#sponsors.hub_sponsor_url#" target="_blank" rel="noopener" rel="noreferrer">#sponsors.hub_sponsor_name#</a>
							 <cfelse>
                               #sponsors.hub_sponsor_name#
							 </cfif>

							 </td></tr>

							 </td></tr>
							 <tr><td class="feed_sub_header" style="font-weight: normal;">#sponsors.hub_sponsor_desc#</td></tr>
                             <tr><td height=40 class="feed_sub_header" align=right>

                             #sponsors.hub_sponsor_poc_name#  #sponsors.hub_sponsor_poc_phone#  #sponsors.hub_sponsor_poc_email#

                             </td></tr>
							</table>

						 </td></tr>

						 <cfif counter LT #sponsors.recordcount#>
                          <tr><td height=10></td></tr>
                          <tr><td colspan=2><hr></td></tr>
                          <tr><td height=10></td></tr>
                         </cfif>

                         <cfset counter = counter + 1>

					</cfoutput>

				</cfloop>

			</cfif>

		</table>

		</div>

      </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>