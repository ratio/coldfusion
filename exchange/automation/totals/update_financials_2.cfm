<!--- Update Financials --->

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select sum(federal_action_obligation) as fy16total, recipient_duns from award_data
 where action_data betweeen '01/01/2016' and '12/31/2016'
 group by recipient_duns
 order by recipient_duns
</cfquery>

<cfloop query = "companies">

	<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	  update company
	  set company_fy16_revenue = #companies.fy16total#
	  where company_duns = '#companies.company_duns#'
	</cfquery>

</cfloop>

Done