<!--- Update Financials --->

<cfset duns_number = 1>

<cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select company_duns from company
 where company_duns like '#duns_number#%'
 order by company_duns
</cfquery>

<cfloop query = "companies">

    <!--- Update FY16 --->

    <cftransaction>

		<cfquery name="fy16revenue" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select sum(federal_action_obligation) as total from award_data
		  where recipient_duns = #companies.company_duns# and
				action_date between '01/01/2016' and '12/31/2016'
		</cfquery>

		<cfquery name="fy17revenue" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select sum(federal_action_obligation) as total from award_data
		  where recipient_duns = '#companies.company_duns#' and
				action_date between '01/01/2017' and '12/31/2017'
		</cfquery>

		<cfquery name="fy18revenue" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  select sum(federal_action_obligation) as total from award_data
		  where recipient_duns = '#companies.company_duns#' and
				action_date between '01/01/2018' and '12/31/2018'
		</cfquery>


		<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		 update company
		 set company_fy16_revenue = <cfif #fy16revenue.total# is "">0<cfelse>#fy16revenue.total#</cfif>,
			 company_fy17_revenue = <cfif #fy17revenue.total# is "">0<cfelse>#fy17revenue.total#</cfif>,
			 company_fy18_revenue = <cfif #fy18revenue.total# is "">0<cfelse>#fy18revenue.total#</cfif>
		 where company_duns = '#companies.company_duns#'
		</cfquery>

	</cftransaction>

</cfloop>

Done