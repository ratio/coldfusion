<!--- FBO Count --->

<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(distinct(fbo_solicitation_number)) as total from fbo
</cfquery>

<!--- Future Needs --->

<cfquery name="needs" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(need_id) as total from need
 where need_public = 1
</cfquery>

<!--- Market Challengess --->

<cfquery name="challenges" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(challenge_id) as total from challenge
 where challenge_public = 1
</cfquery>

<!--- SBIR --->

<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(id) as total from opp_sbir
</cfquery>

<!--- Grants --->

<cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select count(opp_grant_id) as total from opp_grant
</cfquery>

<!--- Update Totals --->

<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 update total
 set total_fbo = #fbo.total#,
     total_needs = #needs.total#,
     total_challenges = #challenges.total#,
     total_sbirs = #sbir.total#,
     total_grants =#grants.total#
</cfquery>

Done