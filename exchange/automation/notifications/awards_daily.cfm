<cfset end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset start_date = #dateformat(dateadd("d",-0,end_date),'mm/dd/yyyy')#>
<cfset yesterday = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset emails_sent = 0>

<cfquery name="notifications" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_notification
 join hub on hub_id = usr_notification_hub_id
 join usr on usr_id = usr_notification_usr_id
 where usr_notification_type = 'New Awards' and
       usr_notification_frequency = 'Daily' and
       usr_notification_active = 1
</cfquery>

<cfloop query="notifications">

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #notifications.usr_notification_usr_id#
</cfquery>

<cfquery name="hinfo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #notifications.usr_notification_hub_id#
</cfquery>

<cfif notifications.usr_notification_frequency is "Daily">
 <cfset end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
 <cfset start_date = #dateformat(dateadd("d",-0,end_date),'mm/dd/yyyy')#>
<cfelseif notifications.usr_notification_frequency is "Weekly">
 <cfset end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
 <cfset start_date = #dateformat(dateadd("d",-7,end_date),'mm/dd/yyyy')#>
<cfelseif notifications.usr_notification_frequency is "Monthly">
 <cfset end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
 <cfset start_date = #dateformat(dateadd("d",-30,end_date),'mm/dd/yyyy')#>
</cfif>

<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from fbo
 left join class_code on class_code_code = fbo_class_code
 left join naics on naics_code = fbo_naics_code
 left join orgname on orgname_name = fbo_agency
 where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(notifications.usr_notification_keywords)#') )
 and (fbo_pub_date between '#start_date#' and '#end_date#')
 and (fbo_type like '%Award%' or fbo_type like '%Award%')

 <cfif notifications.usr_notification_duns is not "">

   <cfif listlen(notifications.usr_notification_duns) is 1>
    and fbo_contract_award_duns = '#notifications.usr_notification_duns#'
   <cfelse>
   and
   (
   fbo_contract_award_duns = '0'
   <cfloop index="d" list="#notifications.usr_notification_duns#">
    <cfoutput>
    or fbo_contract_award_duns = '#d#'
    </cfoutput>
   </cfloop>
   )
   </cfif>

 </cfif>

 <cfif notifications.usr_notification_naics is not "">

   <cfif listlen(notifications.usr_notification_naics) is 1>
    and fbo_naics_code = '#notifications.usr_notification_naics#'
   <cfelse>
   and
   (
   fbo_naics_code = '0'
   <cfloop index="n" list="#notifications.usr_notification_naics#">
    <cfoutput>
    or fbo_naics_code = '#n#'
    </cfoutput>
   </cfloop>
   )
   </cfif>

 </cfif>

</cfquery>

<cfloop index="email" list="#notifications.usr_notification_email_list#">

<cfset emails_sent = emails_sent + 1>

	<cfmail from="#hinfo.hub_name# <noreply@ratio.exchange>"
			  to="#trim(email)#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#hinfo.hub_name# New Awards">

	<html>
	<head>
	<title><cfoutput>#hinfo.hub_name#</cfoutput></title>
	</head>
	<body class="body">

	 <cfif fbo.recordcount is 0>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_sub_header">No new Awards were found.</td></tr>

	    <tr><td>&nbsp;</td></tr>
        <tr><td><b>#notifications.usr_notification_name#</b></td></tr>
          <cfif notifications.usr_notification_desc is not "">
		    <tr><td>#notifications.usr_notification_desc#</td></tr>
          </cfif>
  	    <tr><td>&nbsp;</td></tr>
	    <tr><td>Keywords used - <i>#trim(notifications.usr_notification_keywords)#</i></td></tr>
	    <tr><td>DUNS Number(s) used - <i><cfif trim(notifications.usr_notification_duns) is "">Not Specified<cfelse>#trim(notifications.usr_notification_duns)#</cfif></i></td></tr>
	    <tr><td>NAICS Code(s) used - <i><cfif trim(notifications.usr_notification_naics) is "">Not Specified<cfelse>#trim(notifications.usr_notification_naics)#</cfif></i></td></tr>
  	    <tr><td>&nbsp;</td></tr>
	  </table>

	 <cfelse>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>
	  <tr><td class="feed_sub_header"><b><cfoutput>Total New Awards - #fbo.recordcount#</cfoutput></b></td></tr>
	  <tr><td>&nbsp;</td></tr>
      <tr><td><b>#notifications.usr_notification_name#</b></td></tr>
        <cfif notifications.usr_notification_desc is not "">
		  <tr><td>#notifications.usr_notification_desc#</td></tr>
        </cfif>
	  <tr><td>&nbsp;</td></tr>
	  <tr><td>Keywords used - <i>#trim(notifications.usr_notification_keywords)#</i></td></tr>
	  <tr><td>DUNS Number(s) used - <i><cfif trim(notifications.usr_notification_duns) is "">Not Specified<cfelse>#trim(notifications.usr_notification_duns)#</cfif></i></td></tr>
	  <tr><td>NAICS Code(s) used - <i><cfif trim(notifications.usr_notification_naics) is "">Not Specified<cfelse>#trim(notifications.usr_notification_naics)#</cfif></i></td></tr>

	  <tr><td><hr></td></tr>
	  <tr><td>&nbsp;</td></tr>
	  <tr><td class="feed_sub_header"><b>Award Summaries</b></td></tr>
	  <tr><td>&nbsp;</td></tr>
	 </table>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfloop query="fbo">

	   <cfoutput>

	   <tr><td class="feed_sub_header">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td width=50>
				<cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0>
				<cfelse>
				  <img src="#image_virtual#/#fbo.orgname_logo#" valign=top align=top width=40 border=0>
				</cfif>
			   </td>
			   <td class="feed_sub_header">
			   <a href="#session.site_url#/opps/detail.cfm?i=#hinfo.hub_encryption_key#&t=contract&id=#fbo.fbo_id#"><b>#fbo.fbo_opp_name#</b></a>
			   </td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <tr><td colspan=2 class="feed_sub_header">
           <b>Opportunity Name</b> - #fbo.fbo_opp_name#
           <br>
           <b>Date</b> - #dateformat(fbo.fbo_pub_date,'mm/dd/yyyy')#<br>
           <b>Awardee</b> - #fbo.fbo_contract_award_name# <cfif #fbo.fbo_contract_award_duns# is not ""><b>DUNS</b> (#fbo.fbo_contract_award_duns#)</cfif><br>
           <b>Amount</b> - <cfif isnumeric(fbo.fbo_contract_award_amount)>#numberformat(fbo.fbo_contract_award_amount,'$999,999,999')#<cfelse>#fbo.fbo_contract_award_amount#</cfif><br>
           <b>Solicitation Number</b> - #fbo.fbo_solicitation_number#<br>
           <b>Awarding Organization</b><br>
           #fbo.fbo_dept#
           <cfif #fbo.fbo_agency# is not "">, #fbo_agency#</cfif>
           <cfif #fbo.fbo_office# is not "">, #fbo_office#</cfif>
           <br>
           <cfif #fbo.fbo_major_command# is not "">#fbo_major_command#<br></cfif>
           <cfif #fbo.fbo_sub_command# is not "">#fbo_sub_command#<br></cfif>

 		   </td></tr>

		   </table>

		   <tr><td>&nbsp;</td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td>&nbsp;</td></tr>

	   </cfoutput>

	   </cfloop>

	   </table>

	  </cfif>

	  <cfoutput>

	  <tr><td>&nbsp;</td></tr>
	  <tr><td><hr></td></tr>
	  <tr><td class="feed_sub_header">
	  <br><br>This email was sent to you on behalf of #usr.usr_first_name# #usr.usr_last_name# .  If you do not wish to receive future updates, please contact #usr.usr_first_name# #usr.usr_last_name# at #tostring(tobinary(usr.usr_email))# to get removed.
	  <br><br><br></td></tr>

	  </cfoutput>

	</table>

	</body>
	</html>

	</cfmail>

</cfloop>

</cfloop>

Done.  <cfoutput>#emails_sent#</cfoutput> were sent.