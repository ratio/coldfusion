<cfset end_date = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset start_date = #dateformat(dateadd("d",-0,end_date),'mm/dd/yyyy')#>
<cfset yesterday = #dateformat(dateadd("d",-1,now()),'mm/dd/yyyy')#>
<cfset emails_sent = 0>

<cfquery name="notifications" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr_notification
 join hub on hub_id = usr_notification_hub_id
 join usr on usr_id = usr_notification_usr_id
 where usr_notification_type = 'Open Opportunities' and
       usr_notification_frequency = 'Daily' and
       usr_notification_active = 1
</cfquery>

<cfloop query="notifications">

<cfquery name="usr" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from usr
 where usr_id = #notifications.usr_notification_usr_id#
</cfquery>

<cfquery name="find" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from hub
 where hub_id = #notifications.usr_notification_hub_id#
</cfquery>

<cfset fbo_count = 0>
<cfset need_count = 0>
<cfset sbir_count = 0>
<cfset award_count = 0>
<cfset grant_count = 0>
<cfset challenge_count_hub = 0>
<cfset challenge_count_lake = 0>

<cfif notifications.usr_notification_grants is 1>

	<cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select opp_grant_id, opportunitytitle, department, agencyname, opportunitynumber, fundinginstrumenttype, awardfloor, awardceiling, closedate, orgname_logo from opp_grant
	 left join orgname on orgname_name = department
	 where posteddate between '#start_date#' and '#end_date#' and
	       closedate >= #now()# and
	       contains((opportunitytitle, department, description, agencyname),'#trim(notifications.usr_notification_keywords)#')
     order by closedate
	</cfquery>
	<cfset grant_count = grants.recordcount>

</cfif>

<cfif notifications.usr_notification_challenges is 1>

	<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from challenge
	 where challenge_hub_id is null and
	       challenge_end >= #now()# and
	       challenge_created between '#start_date#' and '#end_date#' and
		   contains((challenge_name, challenge_keywords, challenge_organization),'#trim(notifications.usr_notification_keywords)#')
	</cfquery>

	<cfset challenge_count_lake = challenges.recordcount>

	<cfquery name="challenges_hub" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from challenge
	 join hub on hub_id = challenge_hub_id
	 where challenge_public <> 0 and
	       challenge_end >= #now()# and
	       challenge_created between '#start_date#' and '#end_date#' and
		   contains((challenge_name, challenge_keywords, challenge_organization),'#trim(notifications.usr_notification_keywords)#')
	</cfquery>

	<cfset challenge_count_hub = challenges_hub.recordcount>

</cfif>

<cfif notifications.usr_notification_contracts is 1>

	<cfquery name="fbo" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from fbo
	 left join class_code on class_code_code = fbo_class_code
	 left join naics on naics_code = fbo_naics_code
	 left join orgname on orgname_name = fbo_agency
	 where (contains((fbo_office, fbo_agency, fbo_dept, fbo_naics_code, fbo_major_command, fbo_sub_command, fbo_opp_name, fbo_desc),'#trim(notifications.usr_notification_keywords)#') )
	 and fbo_pub_date between '#start_date#' and '#end_date#'

     <cfif notifications.usr_notification_state is not 0>

      <cfif notifications.usr_notification_state is not "">
        and fbo_pop_state = '#notifications.usr_notification_state#'
      </cfif>

     </cfif>

	 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
	</cfquery>

	<cfset fbo_count = fbo.recordcount>

</cfif>

<cfif notifications.usr_notification_sbirs is 1>

	<cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 select * from opp_sbir
	 left join orgname on orgname_name = agency
	 where posteddate between '#start_date#' and '#end_date#' and
	       closedate >= #now()# and
	       contains((solicitationnumber,title,objective, description, phasei, phaseii, phaseiii, keywords ),'#trim(notifications.usr_notification_keywords)#')
	 order by closedate
	</cfquery>

	<cfset sbir_count = sbir.recordcount>

</cfif>

<cfset total_count = sbir_count + grant_count + award_count + challenge_count_lake + challenge_count_hub + need_count + fbo_count>

<cfloop index="email" list="#notifications.usr_notification_email_list#">

<cfset emails_sent = emails_sent + 1>

	<cfmail from="#find.hub_name# <noreply@ratio.exchange>"
			  to="#trim(email)#"
	  username="noreply@ratio.exchange"
	  password="Gofus107!"
		  port="25"
		useSSL="false"
		type="html"
		server="mail.ratio.exchange"
	   subject="#find.hub_name# Open Opportunities">

	<html>
	<head>
	<title><cfoutput>#find.hub_name#</cfoutput></title>
	</head>
	<body class="body">

	 <cfif total_count is 0>
	  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_sub_header">No opportunities were found.</td></tr>

	    <tr><td>&nbsp;</td></tr>

	    <tr><td><b>#notifications.usr_notification_name#</b></td></tr>
        <cfif notifications.usr_notification_desc is not "">
		  <tr><td>#notifications.usr_notification_desc#</td></tr>
        </cfif>
	    <tr><td>Keywords used - <i>#trim(notifications.usr_notification_keywords)#</i></td></tr>

	    <tr><td>&nbsp;</td></tr>

        <tr><td><b>Opportunities Searched</b></td></tr>
        <cfif notifications.usr_notification_contracts is 1><tr><td>- Contracts / New Procurements</td></tr></cfif>
        <cfif notifications.usr_notification_grants is 1><tr><td>- Grants</td></tr></cfif>
        <cfif notifications.usr_notification_sbirs is 1><tr><td>- SBIR/STTRs</td></tr></cfif>
        <cfif notifications.usr_notification_challenges is 1><tr><td>- Challenges</td></tr></cfif>

	    <tr><td>&nbsp;</td></tr>

	  </table>

	 <cfelse>

	 <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <cfif total_count is 1>

	  <tr><td class="feed_sub_header"><b>

	  <cfif notifications.usr_notification_frequency is "Daily">
		1 Opportunity was posted to the Exchange on #yesterday# that you may be interested in.
	  <cfelseif notifications.usr_notification_frequency is "Weekly">
		1 Opportunity was posted to the Exchange between #start_date# and #end_date# that you may be interested in.
	  <cfelseif notifications.usr_notification_frequency is "Monthly">
		1 Opportunity was posted to the Exchange between #start_date# and #end_date# that you may be interested in.
	  </cfif>

      <cfelse>

	  <tr><td class="feed_sub_header"><b><cfoutput>#total_count#</cfoutput>

	  <cfif notifications.usr_notification_frequency is "Daily">
		Opportunities were posted to the Exchange on #yesterday# that you may be interested in.
	  <cfelseif notifications.usr_notification_frequency is "Weekly">
		Opportunities were posted to the Exchange between #start_date# and #end_date# that you may be interested in.
	  <cfelseif notifications.usr_notification_frequency is "Monthly">
		Opportunities were posted to the Exchange between #start_date# and #end_date# that you may be interested in.
	  </cfif>

	  </cfif>

	  </b></td></tr>
	  <tr><td>&nbsp;</td></tr>

	  <tr><td><b>#notifications.usr_notification_name#</b></td></tr>
      <cfif notifications.usr_notification_desc is not "">
		  <tr><td>#notifications.usr_notification_desc#</td></tr>
      </cfif>
	  <tr><td>Keywords used - <i>#trim(notifications.usr_notification_keywords)#</i></td></tr>

	  <tr><td>&nbsp;</td></tr>

      <tr><td><b>Opportunities Searched</b></td></tr>
      <cfif notifications.usr_notification_contracts is 1><tr><td>- Contracts / New Procurements</td></tr></cfif>
      <cfif notifications.usr_notification_grants is 1><tr><td>- Grants</td></tr></cfif>
      <cfif notifications.usr_notification_sbirs is 1><tr><td>- SBIR/STTRs</td></tr></cfif>
      <cfif notifications.usr_notification_challenges is 1><tr><td>- Challenges</td></tr></cfif>

	  <tr><td>&nbsp;</td></tr>
	  <tr><td><hr></td></tr>
	  <tr><td>&nbsp;</td></tr>

	  <cfoutput>
		  <tr><td class="feed_sub_header"><b>Opportunity Snapshots</b></td></tr>
	  </cfoutput>

	  <tr><td>&nbsp;</td></tr>

	</table>

	<!--- Active Contracts --->

	  <cfif fbo_count GT 0>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" bgcolor="e0e0e0" height=50><b>Active Contracts</b></td></tr>
	   <tr><td>&nbsp;</td></tr>

	   <cfloop query="fbo">

	   <cfoutput>

	   <tr><td class="feed_sub_header">

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		   <tr><td width=50>
				<cfif #orgname_logo# is "">
				  <img src="#image_virtual#/icon_usa.png" valign=top align=top width=40 border=0>
				<cfelse>
				  <img src="#image_virtual#/#fbo.orgname_logo#" valign=top align=top width=40 border=0>
				</cfif>
			   </td>
			   <td class="feed_sub_header">
			   <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=contract&id=#fbo.fbo_id#"><b>#fbo.fbo_opp_name#</b></a>
			   </td></tr>
		   <tr><td>&nbsp;</td></tr>
		   <tr><td colspan=2 class="feed_sub_header">
				#fbo.fbo_dept# | #fbo.fbo_agency#<br>
				#fbo.fbo_office#<br>
				<cfif fbo.fbo_major_command is not "" and fbo.fbo_major_command is not fbo.fbo_office>#fbo.fbo_major_command#<br></cfif>
				<cfif fbo.fbo_sub_command is not "" and fbo.fbo_sub_command is not fbo.fbo_office>#fbo.fbo_sub_command#<br></cfif>
				Notice Type - #fbo.fbo_notice_type#<br>
				Small Business Setaside - <cfif fbo.fbo_setaside_original is "">Not specified<cfelse>#fbo.fbo_setaside_original#</cfif><br>
				Posted - #dateformat(fbo.fbo_pub_date,'mm/dd/yyyy')# | Due Date - #dateformat(fbo.fbo_response_date_original,'mm/dd/yyyy')#
				</td></tr>

		   </table>

		   <tr><td>&nbsp;</td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td>&nbsp;</td></tr>

	   </cfoutput>

	   </cfloop>

	   </table>

	  </cfif>

	<!--- Grants --->

	  <cfif grant_count GT 0>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" bgcolor="e0e0e0" height=50><b>Grants</b></td></tr>
	   <tr><td>&nbsp;</td></tr>

	   <cfloop query="grants">

	   <cfoutput>

	   <tr><td>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr>
			<td width=50 valign=top>

			<cfif #grants.orgname_logo# is "">
			 <img src="#image_virtual#/icon_usa.png" width=40 border=0>
			<cfelse>
			 <img src="#image_virtual#/#grants.orgname_logo#" width=40 border=0>
			</cfif>

			</td>

			<td>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td class="feed_sub_header"><a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=grant&id=#grants.opp_grant_id#"><b>#grants.opportunitytitle#</b></a></td></tr>
			  <tr><td class="feed_sub_header">#grants.agencyname#</td></tr>
			</table>

			</td></tr>
			<tr><td height=10></td></tr>

		   <tr><td colspan=2 class="feed_sub_header">
		   #grants.opportunitynumber#<br>
		   Funding Type - #grants.fundinginstrumenttype#<br>
		   Award Ceiling - #numberformat(grants.awardceiling,'$999,999,999')#<br>
		   Close Date - #grants.closedate#<br>

		   </td></tr>

		   <tr><td>&nbsp;</td></tr>
		   <tr><td colspan=2><hr></td></tr>
		   <tr><td>&nbsp;</td></tr>

	   </cfoutput>

	   </table>

	   </td></tr>

	   </cfloop>

	   </table>

	  </cfif>

	<!--- SBIR/STTRs --->

	  <cfif sbir_count GT 0>

	  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <tr><td class="feed_sub_header" colspan=2 bgcolor="e0e0e0" height=50><b>SBIR/STTRs</b></td></tr>
	   <tr><td>&nbsp;</td></tr>

	   <cfloop query="sbir">

	   <cfoutput>

		<cfif sbir.source is "HHS">

		<tr><td class="feed_sub_header" width=10%>

			<cfif sbir.orgname_logo is "">
			 <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=sbir&id=#sbir.id#"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
			<cfelse>
			 <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=sbir&id=#sbir.id#"><img src="#image_virtual#/#sbir.orgname_logo#" align=top width=40 border=0></a>
			</cfif>

		 </td><td class="feed_sub_header">

			#ucase(sbir.agency)#<br>
			#ucase(sbir.department)#

		 </td></tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td colspan=2><cfif len(sbir.objective GT 500)>#left(sbir.objective,500)#...<cfelse>#sbir.objective#</cfif></td></tr>
		<tr><td colspan=2>Close Date - #sbir.closedate#</td></tr>

	   <cfelseif sbir.source is "DOD">

		<tr>

		<td class="feed_sub_header" width=10%>

			 <cfif sbir.orgname_logo is "">
			  <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=sbir&id=#sbir.id#"><img src="#image_virtual#/icon_sbbr.png" valign=top align=top width=40 border=0></a>
			 <cfelse>
			  <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=sbir&id=#sbir.id#"><img src="#image_virtual#/#sbir.orgname_logo#" align=top width=40 border=0></a>
			 </cfif>

		</td><td class="feed_sub_header">

			  #ucase(sbir.agency)#<br>
			  #ucase(sbir.department)#

			</td></tr>

		<tr><td>&nbsp;</td></tr>
		<tr><td colspan=2><cfif len(sbir.objective GT 500)>#left(sbir.objective,500)#...<cfelse>#sbir.objective#</cfif></td></tr>
		<tr><td colspan=2>Keywords - #ucase(sbir.keywords)#</td></tr>
		<tr><td colspan=2>Close Date - #sbir.closedate#</td></tr>

		</cfif>

		<tr><td>&nbsp;</td></tr>
		<tr><td colspan=2><hr></td></tr>
		<tr><td>&nbsp;</td></tr>

	   </cfoutput>

	   </cfloop>

	   </table>

	  </cfif>

	<!--- National Challenges --->

	   <cfif challenge_count_lake GT 0>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_sub_header" colspan=2 bgcolor="e0e0e0" height=50><b>Challenges (Industry)</b></td></tr>
		<tr><td>&nbsp;</td></tr>

	   <cfloop query="challenges">

	   <cfoutput>

		<tr><td class="feed_sub_header">

		<cfif challenges.challenge_type_id is 1>

			<cfif challenges.challenge_image is "">
			  <a href="#challenge_url#" target="_blank" rel="noreferrer"><img src="#image_virtual#/stock_challenge.png" vspace=10 width=100 border=0></a>
			<cfelse>
			 <cfif left(challenges.challenge_image,4) is "http">
			  <a href="#challenge_url#" target="_blank" rel="noreferrer"><img src="#challenges.challenge_image#" vspace=10 width=100 border=0></a>
			 <cfelse>
			  <a href="#challenge_url#" target="_blank" rel="noreferrer"><img src="http://www.challenge.gov#challenges.challenge_image#" vspace=10 width=100 border=0></a>
			 </cfif>
			</cfif>

		<cfelse>

			<cfif challenges.challenge_image is "">
			  <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=challenge&id=#challenges.challenge_id#"><img src="#image_virtual#/stock_challenge.png" vspace=10 width=100 border=0></a>
			<cfelse>
			  <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=challenge&id=#challenges.challenge_id#"><img src="#media_virtual#/#challenges.challenge_image#" vspace=10 width=100 border=0></a>
			</cfif>

		</cfif>

		</td><td class="feed_sub_header">

		<cfif challenges.challenge_type_id is 1>
			<a href="#challenge_url#" target="_blank" rel="noreferrer"><b>#ucase(challenges.challenge_name)#</b></a>
		<cfelse>
			<a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=challenge&id=#challenges.challenge_id#"><b>#ucase(challenges.challenge_name)#</b></a>
		</cfif>

		<br>
		Sponsoring Organization -
		<cfif challenges.challenge_organization is not "">
		  <b><cfif #challenges.challenge_annoymous# is 1>Private<cfelse>#challenges.challenge_organization#</cfif></b>
		</cfif>

		<br>

			Prize:
			<cfif #challenges.challenge_total_cash# is not "">

				<cfif challenges.challenge_currency is "$">$
				<cfelseif challenges.challenge_currency is "I">INR
				<cfelseif challenges.challenge_currency is "�">�
				<cfelseif challenges.challenge_currency is "�">�
				<cfelse>
				</cfif>

			  #numberformat(challenges.challenge_total_cash,'999,999,999')#
			<cfelse>
			  Not Provided
			</cfif>

		</td></tr>

	   </cfoutput>
	   </cfloop>

	   </table>

	   </cfif>

	<!--- Local Challenges --->

	   <cfif challenge_count_hub GT 0>

		<table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr><td class="feed_sub_header" colspan=2 bgcolor="e0e0e0" height=50><b>Challenges (Exchange)</b></td></tr>
		<tr><td>&nbsp;</td></tr>

	   <cfloop query="challenges">

	   <cfoutput>

		<tr><td class="feed_sub_header">

		<cfif challenges.challenge_image is "">
		  <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=challenge&id=#challenges_hub.challenge_id#"><img src="#image_virtual#/stock_challenge.png" vspace=10 width=100 border=0></a>
		<cfelse>
		  <a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=challenge&id=#challenges_hub.challenge_id#"><img src="#media_virtual#/#challenges_hub.challenge_image#" vspace=10 width=100 border=0></a>
		</cfif>

		</td><td class="feed_sub_header">

		<a href="#session.site_url#/opps/detail.cfm?i=#find.hub_encryption_key#&t=challenge&id=#challenges_hub.challenge_id#"><b>#ucase(challenges_hub.challenge_name)#</b></a>

		<br>
		Sponsoring Organization -
		<cfif challenges_hub.challenge_organization is not "">
		  <b><cfif #challenges_hub.challenge_annoymous# is 1>Private<cfelse>#challenges_hub.challenge_organization#</cfif></b>
		</cfif>

		<br>

			Prize:
			<cfif #challenges_hub.challenge_total_cash# is not "">
			  #numberformat(challenges_hub.challenge_total_cash,'999,999,999')#
			<cfelse>
			  Not Provided
			</cfif>

		</td></tr>

	   </cfoutput>
	   </cfloop>

	   </table>

	   </cfif>

	  <cfoutput>

	  <tr><td class="feed_sub_header">
	  <br><br>This email was sent to you on behalf of #usr.usr_first_name# #usr.usr_last_name# .  If you do not wish to receive future updates, please contact #usr.usr_first_name# #usr.usr_last_name# at #tostring(tobinary(usr.usr_email))# to get removed.
	  <br><br><br></td></tr>

	 </cfoutput>

	</table>

	</body>
	</html>

	</cfif>

	</cfmail>

</cfloop>

</cfloop>

Done.  <cfoutput>#emails_sent#</cfoutput> were sent.