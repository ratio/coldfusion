<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\grants\grants.xlsx">

<cfset counter = 0>

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

	<cfloop query="data" startrow="2">

	<cfif #documenttype# is "" and lastupdate is "">
	<cfelse>

     <cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
      select count(opp_grant_id) as total from opp_grant
      where opportunitynumber = '#opportunitynumber#' and
            lastupdate = '#lastupdate#'
     </cfquery>

     <cfif check.total is 0>

			<cfquery name="grants_import" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			  insert into opp_grant
			  (
				opportunitynumber,
				opportunitytitle,
				opportunitystatus,
				posteddate,
				closedate,
				documenttype,
				opportunitycategory,
				opportunitycategoryexplanation,
				fundinginstrumenttype,
				categoryexplanation,
				expectednumberofawards,
				cfdanumber,
				costsharingormatchingrequirement,
				version,
				lastupdate,
				originalclosingdate,
				archivedate,
				awardceiling,
				awardfloor,
				eligibleapplicants,
				additionalinformationeligibility,
				description,
				linktoadditionalinformation,
				grantorcontactinformation,
				grantorcontactinformationtitle,
				agencyname,
				fundingopportunitynumber,
				grantorcontactinformationphone,
				pageload,
				department,
				grantoremailaddress,
				import_date
			   )
			   values
			   (
				'#opportunitynumber#',
				'#opportunitytitle#',
				'#opportunitystatus#',
				'#posteddate#',
				'#closedate#',
				'#documenttype#',
				'#OpportunityCatagory#',
				'#OpportunityCatagoryExplanation#',
				'#fundinginstrumenttype#',
				'#OpportunityCatagoryExplanation#',
				'#expectednumberofawards#',
				'#cfdanumber#',
				'#costsharingormatchingrequirement#',
				'#version#',
				'#lastupdate#',
				<cfif #isdate(originalclosingdate)#>'#originalclosingdate#'<cfelse>null</cfif>,
				'#archivedate#',
				 <cfif awardceiling is "">null<cfelse>#replace(awardceiling,",","","all")#</cfif>,
				 <cfif awardfloor is "">null<cfelse>#replace(awardfloor,",","","all")#</cfif>,
				'#eligibleapplicants#',
				'#additionalinformationeligibility#',
				'#description#',
				'#linktoadditionalinformation#',
				'#GrantorContactInformationName#',
				'#grantorcontactinformationtitle#',
				'#agencyname#',
				'#fundingopportunitynumber#',
				'#grantorcontactinformationphone#',
				'#pageload#',
				'#department#',
				'#grantoremailaddress#',
				 #now()#
				)
	 		</cfquery>

           <cfset counter = counter + 1>

           </cfif>

	 	 </cfif>

	</cfloop>

<cfoutput>Grants Imported - #counter#</cfoutput>

<cfelse>

 File not found.

</cfif>