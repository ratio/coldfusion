<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\challengesites\ideaconnection\ideaconnection.xlsx">

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

    <cftransaction>

	<cfloop query="data" startrow="2">

            <cfset insert_price = #replace(prize,",","","all")#>
            <cfset insert_price = #replace(insert_price,"$","","all")#>
            <cfset insert_price = #replace(insert_price,"�","","all")#>
            <cfset insert_price = #replace(insert_price,"INR","","all")#>
            <cfset insert_price = #replace(insert_price,"�","","all")#>

            <cfif left(prize,1) is "$">
             <cfset challenge_currency = '$'>
            <cfelseif left(prize,1) is "I">
             <cfset challenge_currency = 'I'>
            <cfelseif left(prize,1) is "�">
             <cfset challenge_currency = '�'>
            <cfelseif left(prize,1) is "�">
             <cfset challenge_currency = '�'>
            <cfelse>
             <cfset challenge_currency = '?'>
            </cfif>

			<cfquery name="exists" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select challenge_id from challenge
			 where challenge_url = '#url#'
			</cfquery>

			<cfif exists.recordcount is 1>

				<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  update challenge
				  set challenge_desc = '#description#',
				      challenge_currency = '#challenge_currency#',
				      challenge_source = 'IdeaConnection',
				      challenge_updated = #now()#,
				      challenge_public = 1,
				      challenge_image = '#image#',
				      challenge_name = '#challenge#',
				      challenge_total_cash = <cfif isnumeric(insert_price)>#insert_price#<cfelse>null</cfif>,
				      challenge_end = <cfif #submissionend# is "">null<cfelse><cfif isdate(submissionend)>'#submissionend#'<cfelse>null</cfif></cfif>,
				      challenge_url = '#url#'
				      where challenge_id = #exists.challenge_id#
				</cfquery>

			<cfelse>

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into challenge
				  (
				   challenge_name,
				   challenge_currency,
				   challenge_total_cash,
				   challenge_image,
				   challenge_public,
				   challenge_source,
				   challenge_updated,
				   challenge_type_id,
				   challenge_desc,
				   challenge_end,
				   challenge_url
				   )
				  values
				  (
				  '#challenge#',
				  '#challenge_currency#',
				   <cfif isnumeric(insert_price)>#insert_price#<cfelse>null</cfif>,
				  '#image#',
				  1,
				  'IdeaConnection',
				   #now()#,
				  1,
				  '#description#',
				  <cfif #submissionend# is "">null<cfelse><cfif isdate(submissionend)>'#submissionend#'<cfelse>null</cfif></cfif>,
				  '#url#'
				  )
				</cfquery>

			</cfif>

	</cfloop>

   </cftransaction>

 Done

<cfelse>

 File not found.

</cfif>