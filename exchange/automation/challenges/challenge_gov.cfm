<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\challenge.gov\challenge.xlsx">

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

    <cftransaction>

	<cfloop query="data" startrow="2">

		<cfquery name="exists" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select challenge_id from challenge
		 where challenge_type_id = 1 and
			   challenge_organization = '#agency#' and
			   challenge_name = '#challenge#'
		</cfquery>

		<cfset challenge_image = 'https://www.challenge.gov' & #image#>

		<cfif exists.recordcount is 1>

			<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  update challenge
			  set challenge_desc = '#opportunity#',
			      challenge_image = '#challenge_image#',
				  challenge_updated = #now()#,

			  <cfif #status# is "Closed On">
			   challenge_status = 2,
			  <cfelseif #status# is "Open Until">
			   challenge_status = 1,
			  </cfif>
			  challenge_start = <cfif #submissionstart# is "">null<cfelse>'#submissionstart#'</cfif>,
			  challenge_end = <cfif #submissionend# is "">null<cfelse>'#submissionend#'</cfif>,
			  challenge_url = '#url#',
			  challenge_keywords = '#typeofchallenge#',
			  challenge_source = 'Challenge.gov',
			  challenge_total_cash =
			  <cfif #totalcashprizesoffered# is "">null<cfelse>
			  #replace(totalcashprizesoffered,",","","all")#
			  </cfif>

			  where challenge_id = #exists.challenge_id#
			</cfquery>

		<cfelse>

		<cftransaction>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  insert into challenge
			  (
			   challenge_name,
			   challenge_image,
			   challenge_public,
			   challenge_organization,
			   challenge_updated,
			   challenge_type_id,
			   challenge_desc,
			   challenge_status,
			   challenge_start,
			   challenge_end,
			   challenge_url,
			   challenge_keywords,
			   challenge_total_cash,
			   challenge_source
			  )
			  values
			  (
			  '#challenge#',
			  '#challenge_image#',
			  1,
			  '#agency#',
			   #now()#,
			  1,
			  '#opportunity#',
			  <cfif #status# is "Closed On">2<cfelseif #status# is "Open Until">1</cfif>,
			  <cfif #submissionstart# is "">null<cfelse>'#submissionstart#'</cfif>,
			  <cfif #submissionend# is "">null<cfelse>'#submissionend#'</cfif>,
			  '#url#',
			  '#typeofchallenge#',
			  <cfif #totalcashprizesoffered# is "">null<cfelse>#replace(totalcashprizesoffered,",","","all")#</cfif>,
			 'Challenge.gov'

			  )
			</cfquery>

		</cfif>

	</cfloop>

	</cftransaction>

 Done

<cfelse>

 File not found.

</cfif>