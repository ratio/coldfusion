<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\challengesites\innocentive\innocentive.xlsx">

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

    <cftransaction>

	<cfloop query="data" startrow="2">

			<cfquery name="exists" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select challenge_id from challenge
			 where challenge_url = '#url#'
			</cfquery>

			<cfif exists.recordcount is 1>

				<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  update challenge
				  set challenge_desc = '#opportunity#',
				      challenge_updated = #now()#,
				      challenge_public = 1,
				      challenge_image = '#image#',
				      challenge_source = 'Innocentive',
				      challenge_name = '#challenge#',
                      challenge_start = <cfif #submissionstart# is "">null<cfelse>'#submissionstart#'</cfif>,
				      challenge_end = <cfif #submissionend# is "">null<cfelse><cfif isdate(submissionend)>'#submissionend#'<cfelse>null</cfif></cfif>,
				      challenge_url = '#url#',
				      challenge_keywords = '#keywords#',
				      challenge_total_cash =

				      <cfif #prize# is "">null<cfelse>
                      #replace(prize,",","","all")#
				      </cfif>

				      where challenge_id = #exists.challenge_id#
				</cfquery>

			<cfelse>

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into challenge
				  (
				   challenge_name,
				   challenge_image,
				   challenge_public,
				   challenge_source,
				   challenge_updated,
				   challenge_type_id,
				   challenge_desc,
				   challenge_end,
				   challenge_start,
				   challenge_url,
				   challenge_keywords,
				   challenge_total_cash
				  )
				  values
				  (
				  '#challenge#',
				  '#image#',
				  1,
				  'Innocentive',
				   #now()#,
				  1,
				  '#opportunity#',
				  <cfif #submissionstart# is "">null<cfelse>'#submissionstart#'</cfif>,
				  <cfif #submissionend# is "">null<cfelse><cfif isdate(submissionend)>'#submissionend#'<cfelse>null</cfif></cfif>,
				  '#url#',
				  '#keywords#',
				  <cfif #prize# is "">null<cfelse>
                  #replace(prize,",","","all")#
				  </cfif>
				  )
				</cfquery>

  			</cfif>

	</cfloop>

	</cftransaction>

Done

<cfelse>

 File not found.

</cfif>