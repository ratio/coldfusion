<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\challengesites\kaggle\kaggle.xlsx">

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

    <cftransaction>

	<cfloop query="data" startrow="2">

			<cfquery name="exists" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select challenge_id from challenge
			 where challenge_url = '#url#'
			</cfquery>

			<cfif exists.recordcount is 1>

				<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  update challenge
				  set challenge_desc = '#description#',
				      challenge_organization = 'Kaggle',
				      challenge_source = 'Kaggle',
				      challenge_updated = #now()#,
				      challenge_public = 1,
				      challenge_image = '#image#',
				      challenge_name = '#challenge#',
				      challenge_url = '#url#',
				      challenge_total_cash = <cfif isnumeric(prize)>#prize#<cfelse>null</cfif>
				      where challenge_id = #exists.challenge_id#
				</cfquery>

			<cfelse>

				<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				  insert into challenge
				  (
				   challenge_name,
				   challenge_source,
				   challenge_image,
				   challenge_public,
				   challenge_organization,
				   challenge_updated,
				   challenge_type_id,
				   challenge_desc,
				   challenge_url,
				   challenge_total_cash
				  )
				  values
				  (
				  '#challenge#',
				  'Kaggle',
				  '#image#',
				  1,
				  'Kaggle',
				   #now()#,
				  1,
				  '#description#',
				  '#url#',
				  <cfif isnumeric(prize)>#prize#<cfelse>null</cfif>
				  )
				</cfquery>

			</cfif>

	</cfloop>

	</cftransaction>

  Done

<cfelse>

 File not found.

</cfif>