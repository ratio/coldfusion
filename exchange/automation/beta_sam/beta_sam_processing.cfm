<!--- Start - Import records into the FBO table --->

<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\beta.sam.gov\beta_sam_gov.xlsx">

<cfset counter = 0>

<cftry>

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

	<cfloop query="data" startrow="2">

		<cfquery name="fbo_import" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		  insert into fbo
		  (
          fbo_item_id,
          fbo_ui,
          fbo_url,
          fbo_type,
          fbo_pub_date,
          fbo_updated,
          fbo_office,
          fbo_agency,
          fbo_dept,
          fbo_solicitation_number,
          fbo_opp_name,
          fbo_contract_award_date,
          fbo_contract_award_number,
          fbo_notice_type,
          fbo_task_order_number,
          fbo_mod_number,
          fbo_timezone,
          fbo_pub_date_updated,
          fbo_pub_date_original,
          fbo_inactive_policy,
          fbo_inactive_date_updated,
          fbo_inactive_date_original,
          fbo_authority,
          fbo_additional_reporting,
          fbo_class_code,
          fbo_naics_code,
          fbo_poc,
          fbo_poc_email,
          fbo_poc_phone,
          fbo_poc_secondary,
          fbo_poc_secondary_email,
          fbo_poc_secondary_phone,
          fbo_history,
          fbo_page,
          fbo_desc,
          fbo_major_command,
          fbo_sub_command,
          fbo_response_date_updated,
          fbo_response_date_original,
          fbo_initiative,
          fbo_pop_city,
          fbo_pop_state,
          fbo_pop_zip,
          fbo_pop_country,
          fbo_contracting_office,
          fbo_contract_award_duns,
          fbo_contract_award_name,
          fbo_contract_award_address,
          fbo_contract_award_city,
          fbo_contract_award_state,
          fbo_contract_award_zip,
          fbo_contract_award_country,
          fbo_contract_award_amount,
          fbo_setaside_original,
          fbo_contract_lin,
          fbo_related_notice,
          fbo_due_date_updated,
          fbo_due_date_original,
          fbo_import_date
		  )
		  values
		  (
           #ItemID#,
          '#UI#',
          '#URL#',
          '#Type#',
          <cfif isdate(publisheddate)><cfif #publisheddate# is "">null<cfelse>'#PublishedDate#'</cfif><cfelse>null</cfif>,
          <cfif isdate(updateddate)><cfif #updateddate# is "">null><cfelse>'#UpdatedDate#'</cfif><cfelse>null</cfif>,
          '#Office#',
          '#Agency#',
          '#Organization#',
          '#NoticeID#',
          '#Opportunity#',
          <cfif isdate(contractawarddate)><cfif #contractawarddate# is "">null<cfelse>'#ContractAwardDate#'</cfif><cfelse>null</cfif>,
          '#ContractAwardNumber#',
          '#ContractOpportunityType#',
          '#TaskDeliveryOrderNumber#',
          '#ModificationNumber#',
          '#TimeZone#',
          <cfif isdate(updatedpublishdate)><cfif #UpdatedPublishDate# is "">null<cfelse>'#UpdatedPublishDate#'</cfif><cfelse>null</cfif>,
          <cfif isdate(originalpublisheddate)><cfif #OriginalPublishedDate# is "">null<cfelse>'#OriginalPublishedDate#'</cfif><cfelse>null</cfif>,
          '#InactivePolicy#',
          <cfif isdate(updatedinactivedate)><cfif #updatedinactivedate# is "">null<cfelse>'#UpdatedInactiveDate#'</cfif><cfelse>null</cfif>,
          <cfif isdate(originalinactivedate)><cfif #originalinactivedate# is "">null<cfelse>'#OriginalInactiveDate#'</cfif><cfelse>null</cfif>,
          '#Authority#',
          '#AdditionalReporting#',
          '#ProductServiceCode#',
          '#NAICSCode#',
          '#PrimaryPointofContactName#',
          '#PrimaryPointofContacrEmail#',
          '#PrimaryPointofContactPhone#',
          '#SecondaryPointofContact#',
          '#SecondaryPointofContactEmail#',
          '#SecondaryPointofContactPhone#',
          '#History#',
          '#PAGEHTML#',
          '#Description#',
          '#MajorCommand#',
          '#SubCommand#',
          <cfif isdate(updatedresponsedate)><cfif #updatedresponsedate# is "">null<cfelse>'#UpdatedResponseDate#'</cfif><cfelse>null</cfif>,
          <cfif isdate(originalresponsedate)><cfif #originalresponsedate# is "">null<cfelse>'#OriginalResponseDate#'</cfif><cfelse>null</cfif>,
          '#Initiative#',
          '#PlaceofPerformanceCity#',
          '#PlaceofPerformanceState#',
          '#PlaceofPerformanceZipCode#',
          '#PlaceofPerformanceCountry#',
          '#ContractingOffice#',
          '#ContractorAwardedUniqueEntityIDDUNS#',
          '#ContractorAwardedNameandAddress#',
          '#ContractorAwardedAddress#',
          '#ContractorAwardedCity#',
          '#ContractorAwardedState#',
          '#ContractorAwardedZipCode#',
          '#ContractorAwardedCountry#',
           <cfif #TotalContractValue# is "">null<cfelse><cfif isnumeric(totalcontractvalue)>#TotalContractValue#<cfelse>null</cfif></cfif>,
          '#OriginalSetAside#',
          '#ContractLineItemNumber#',
          '#RelatedNotice#',
          <cfif isdate(updateddateoffersdue)><cfif #updateddateoffersdue# is "">null<cfelse>'#UpdatedDateOffersDue#'</cfif><cfelse>null</cfif>,
          <cfif isdate(originaldateoffersdue)><cfif #originaldateoffersdue# is "">null<cfelse>'#OriginalDateOffersDue#'</cfif><cfelse>null</cfif>,
           #now()#
		  )
		</cfquery>

	   <cfset counter = counter + 1>

	</cfloop>

		<cfquery name="log" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into event_log
		 (
		  event_log_date,
		  event_log_ip,
		  event_log_event,
		  event_log_message,
		  event_log_result
		 )
		 values
		 (
		 #now()#,
		 '#cgi.remote_addr#',
		 'BETA.SAM.GOV Integration',
		 'Import Successful.  #counter# records were imported.',
		 'Success'
		 )
		</cfquery>

<cfelse>

		<cfquery name="log" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into event_log
		 (
		  event_log_date,
		  event_log_ip,
		  event_log_event,
		  event_log_message,
		  event_log_result
		 )
		 values
		 (
		 #now()#,
		 '#cgi.remote_addr#',
		 'BETA.SAM.GOV Integration',
		 'Import Failed.  File Not Found',
		 'Failed'
		 )
		</cfquery>

</cfif>

<cfcatch type="any">

	<cfquery name="log" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 insert into event_log
	 (
	  event_log_date,
	  event_log_ip,
	  event_log_event,
	  event_log_message,
	  event_log_result,
	  event_error_details
	 )
	 values
	 (
	 #now()#,
	 '#cgi.remote_addr#',
	 'BETA.SAM.GOV Integration',
	 'Import Failed.  An error occured during the import process',
	 'Failed',
	 '#cfcatch.message#'
	 )
	</cfquery>

	<cfset status = "Failed">

</cfcatch>

</cftry>

<cfif fileexists(theFile)>
  <cffile action = "delete" file = "#theFile#">
<cfelse>
</cfif>


