<cfset rssUrl = "https://www.gsa.gov/_rssfeed/newsReleases.xml">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'General Services Administration',
	  'GSA',
	  'https://www.gsa.gov',
	   null,
	  'icon_news_gsa.png'
	  )
	</cfquery>

</cfif>

</cfloop>