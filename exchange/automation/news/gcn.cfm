<!--- Cloud & Infrastructure --->

<cfset rssUrl = "https://gcn.com/rss-feeds/cloud-infrastructure.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'Cloud, Infrastructure',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>

<!--- AI & Automation --->

<cfset rssUrl = "https://gcn.com/rss-feeds/automation-ai.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'AI, Automation',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>

<!--- Cybersecurity --->

<cfset rssUrl = "https://gcn.com/rss-feeds/cybersecurity.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'Cybersecurity',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>

<!--- Data & Analytics --->

<cfset rssUrl = "https://gcn.com/rss-feeds/data-analytics.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'Data, Analytics',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>

<!--- Emerging Tech --->

<cfset rssUrl = "https://gcn.com/rss-feeds/emerging-tech.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'Emerging Tech',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>

<!--- Smart Cities, IoT --->

<cfset rssUrl = "https://gcn.com/rss-feeds/smart-city-iot.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>


	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'Smart Cities, IoT',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>

<!--- State & Local --->

<cfset rssUrl = "https://gcn.com/rss-feeds/state-local.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'State & Local',
	  'icon_news_gcn.png'
	  )
	</cfquery>

 </cfif>

</cfloop>

<!--- Insights --->

<cfset rssUrl = "https://gcn.com/rss-feeds/all.aspx">
<cffeed action="read" source="#rssUrl#" query="entries" properties="info">
<cfloop query="entries">

<cfquery name="check" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select newsfeed_id from newsfeed
 where newsfeed_title = '#entries.title#' and
       newsfeed_date = '#datetimeformat(entries.publisheddate)#'
</cfquery>

<cfif check.recordcount is 0>

	<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	 insert into newsfeed
	 (
	  newsfeed_title,
	  newsfeed_content,
	  newsfeed_url,
	  newsfeed_date,
	  newsfeed_import_date,
	  newsfeed_source,
	  newsfeed_source_acronym,
	  newsfeed_source_url,
	  newsfeed_category,
	  newsfeed_source_logo
	  )
	  values
	  (
	  '#entries.title#',
	  '#entries.content#',
	  '#entries.rsslink#',
	  '#datetimeformat(entries.publisheddate)#',
	   #now()#,
      'Government Computing News',
	  'GCN',
	  'https://www.gcn.com',
	  'News, Explainers, Insights',
	  'icon_news_gcn.png'
	  )
	</cfquery>

</cfif>

</cfloop>