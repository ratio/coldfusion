<cfquery name="hubs" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select hub_id from hub
</cfquery>

<cfloop query="hubs">

<cfquery name="feeds" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from rss
 where rss_hub_id = #hubs.hub_id#
</cfquery>

<cfif feeds.recordcount GT 0>

<cfloop query="feeds">

	<cfset rssUrl = "#feeds.rss_url#">
	<cffeed action="read" source="#rssUrl#" query="entries" properties="info">

	<cfloop query="entries">

		<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select newsfeed_id from newsfeed
		 where newsfeed_title = '#entries.title#' and
			   newsfeed_date = '#datetimeformat(entries.publisheddate)#'
		</cfquery>

		<cfif check.recordcount is 0>

			<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 insert into newsfeed
			 (
			  newsfeed_title,
			  newsfeed_content,
			  newsfeed_url,
			  newsfeed_date,
			  newsfeed_import_date,
			  newsfeed_source,
			  newsfeed_source_url,
			  newsfeed_category,
			  newsfeed_source_logo,
			  newsfeed_hub_id,
			  newsfeed_source_id
			  )
			  values
			  (
			  '#entries.title#',
			  '#entries.content#',
			  '#entries.rsslink#',
			  '#datetimeformat(entries.publisheddate)#',
			   #now()#,
			  '#feeds.rss_name#',
			  '#feeds.rss_name_url#',
			  '#feeds.rss_category#',
			  '#feeds.rss_image#',
			   #hubs.hub_id#,
			   #feeds.rss_id#
			  )
			</cfquery>

		</cfif>

	</cfloop>

 </cfloop>

</cfif>

</cfloop>

Done
