 <cfsetting RequestTimeout = "900000">

 <!--- Set the first number of the DUNS to process --->

 <cfset duns_number = 1>

 <cfquery name="sams_update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select duns from sam_update
  where duns like '#duns_number#%'
  order by duns
 </cfquery>

 <cfset counter = 1>

 <cfloop query="sams_update">

  <cftransaction>

      <!--- Delete Old SAM Record --->

	  <cfquery name="delete" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   delete sams
	   where duns = '#sams_update.duns#'
	  </cfquery>

	  <!--- Insert New SAM Records --->

	  <cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
	   set ANSI_WARNINGS OFF
	   insert into sams
	   select * from sam_update
	   where duns = '#sams_update.duns#'
	   set ANSI_WARNINGS ON
	  </cfquery>

  </cftransaction>

  <cfset counter = counter + 1>

 </cfloop>

<cfoutput>
#duns_number# is now done.  #counter# records updated.
</cfoutput>