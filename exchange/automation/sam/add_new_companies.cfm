 <cfsetting RequestTimeout = "900000">

 <!--- Add New SAMS Companies --->

 <cfquery name="companies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
  select legal_business_name, corp_url, phys_address_l1, phys_address_line_2, city_1,
         state_1, zip_1, duns, poc_fnme, poc_lname, poc_title, poc_us_phone, poc_email from sams
  left join company on company_duns = duns
  where company_id is null
  order by company_id
 </cfquery>

 <cfset counter = 1>

 <cfloop query="companies">

        	<cfquery name="insert_comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		     set ANSI_WARNINGS OFF
			 insert company
			 (company_name,
			  company_website,
			  company_address_l1,
			  company_address_l2,
			  company_city,
			  company_state,
			  company_zip,
			  company_duns,
			  company_poc_first_name,
			  company_poc_last_name,
			  company_poc_title,
			  company_poc_phone,
			  company_poc_email,
			  company_updated,
			  company_source_id)
			 values
			 ('#companies.legal_business_name#',
			  '#companies.corp_url#',
			  '#companies.phys_address_l1#',
			  '#companies.phys_address_line_2#',
			  '#companies.city_1#',
			  '#companies.state_1#',
			  '#companies.zip_1#',
			  '#companies.duns#',
			  '#companies.poc_fnme#',
			  '#companies.poc_lname#',
			  '#companies.poc_title#',
			  '#companies.poc_us_phone#',
			  '#companies.poc_email#',
			   #now()#,
			   199
			 )
	        set ANSI_WARNINGS ON
	        </cfquery>

  <cfset counter = counter + 1>
</cfloop>

<cfoutput>Done.  Inserted #counter# new companies.</cfoutput>