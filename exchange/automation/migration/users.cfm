<cfinclude template="/exchange/security/check.cfm">

<!--- Get Old Users --->

<cfquery name="old" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
 select * from usr
 order by usr_id
</cfquery>

<table cellspacing=0 cellpadding=0 border=1 width=100%>

<cfoutput>
<tr><td colspan=2><b>Users - #old.recordcount#</b></td></tr>
</cfoutput>

<tr>
   <td><b>User</b></td>
   <td><b>Email</b></td>
   <td><b>Old Company ID</b></td>
   <td><b>New Company ID</b></td>
   <td><b>Match</b></td>

</tr>

<cftransaction>

<cfloop query="old">

    <cfset email = #tobase64(old.usr_email)#>
    <cfset password = #tobase64(old.usr_password)#>

    <!--- Check to see if the user exists in Shared --->

	<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select * from usr
	 where usr_email = '#email#'
	</cfquery>

    <cfif check.recordcount is 0>

		<cfset usr_full = #old.usr_first_name# & " " & #old.usr_last_name#>

		<!--- Insert new user --->

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert into usr
		 (
		  usr_processed,
		  usr_first_name,
		  usr_last_name,
		  usr_address_line_1,
		  usr_address_line_2,
		  usr_city,
		  usr_state,
		  usr_zip,
		  usr_country,
		  usr_created,
		  usr_updated,
		  usr_photo,
		  usr_title,
		  usr_phone,
		  usr_experience,
		  usr_hobbies,
		  usr_company_id,
		  usr_last_login,
		  usr_active,
		  usr_company_name,
		  usr_profile_display,
		  usr_background,
		  usr_about,
		  usr_cell_phone,
		  usr_education,
		  usr_keywords,
		  usr_facebook,
		  usr_twitter,
		  usr_password_reset,
		  usr_full_name,
		  usr_email,
		  usr_password
		 )
		 values
		 (
		  1,
		  '#old.usr_first_name#',
		  '#old.usr_last_name#',
		  '#old.usr_address_line_1#',
		  '#old.usr_address_line_2#',
		  '#old.usr_city#',
		  '#old.usr_state#',
		  '#old.usr_zip#',
		  '#old.usr_country#',
		  <cfif old.usr_created is "">null<cfelse>'#old.usr_created#'</cfif>,
		  <cfif old.usr_updated is "">null<cfelse>'#old.usr_updated#'</cfif>,
		  '#old.usr_photo#',
		  '#old.usr_title#',
		  '#old.usr_phone#',
		  '#old.usr_experience#',
		  '#old.usr_hobbies#',
		   <cfif old.usr_company_id is "">null<cfelse>#old.usr_company_id#</cfif>,
		   <cfif old.usr_last_login is "">null<cfelse>'#old.usr_last_login#'</cfif>,
		   <cfif old.usr_active is "">0<cfelse>#old.usr_active#</cfif>,
		  '#old.usr_company_name#',
		   <cfif old.usr_profile_display is "">null<cfelse>#old.usr_profile_display#</cfif>,
		  '#old.usr_background#',
		  '#old.usr_about#',
		  '#old.usr_cell_phone#',
		  '#old.usr_education#',
		  '#old.usr_keywords#',
		  '#old.usr_facebook#',
		  '#old.usr_twitter#',
		   <cfif old.usr_password_reset is "">null<cfelse>#old.usr_password_reset#</cfif>,
		  '#usr_full#',
		  '#email#',
		  '#password#'
		 )

		</cfquery>

    </cfif>

</cfloop>

</cftransaction>

Done

</table>