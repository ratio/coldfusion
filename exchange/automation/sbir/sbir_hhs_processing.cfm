<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\sbir\hhs\sbirhhs.xlsx">

<cfset insert_counter = 0>
<cfset update_counter = 0>

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

	<cfloop query="data" startrow="2">

			<cfquery name="exists" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select id from opp_sbir
			 where SolicitationNumber = '#SolicitationNumber#' and
			       title = '#availablefundingtopics1#'
			</cfquery>

			<cfif exists.recordcount is 0>

				<cfquery name="sbir_import_add" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  insert into opp_sbir
				  (
					ItemID,
					Source,
					ProgramPhaseYear,
					Objective,
					Program,
					CloseDate,
					ReleaseDate,
					OpenDate,
					ApplicationDueDate,
					SolicitationNumber,
					Department,
					Agency,
					Description,
					SBIRreferences,
					PostedDate,
					URL,
					Updated
				   )
					values
					(
					'#ItemID#',
					'HHS',
					'#ProgramPhaseYear#',
					'#Solicitation#',
					'#Program#',
					'#CloseDate#',
					'#ReleaseDate#',
					'#OpenDate#',
					'#ApplicationDueDate#',
					'#SolicitationNumber#',
					'#Department#',
					'#Agency#',
					'#Objective#',
					'#OtherinformationLink#',
					 #now()#,
					'#url#',
					 #now()#
					 )
				</cfquery>

            <cfelse>

				<cfquery name="sbir_import_update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  update opp_sbir
				  set ItemID = '#ItemID#',
					  ProgramPhaseYear = '#ProgramPhaseYear#',
					  Title = '#availablefundingtopics1#',
					  Program = '#Program#',
					  CloseDate = '#CloseDate#',
					  ReleaseDate = '#ReleaseDate#',
					  OpenDate = '#OpenDate#',
					  ApplicationDueDate = '#ApplicationDueDate#',
					  SolicitationNumber = '#SolicitationNumber#',
					  Department = '#Department#',
					  Agency = '#Agency#',
					  URL = '#officiallink#',
					  Objective = '#Objective#',
					  SBIRreferences = '#OtherinformationLink#',
					  PostedDate = #now()#,
					  Updated = #now()#
				  where id = #exists.id#
				</cfquery>

            <cfset update_counter = update_counter + 1>

			</cfif>

	</cfloop>

<cfoutput>#insert_counter# SBIR's inserted<br>
          #update_counter# SBIR's updated</cfoutput>

<cfelse>

 File not found.

</cfif>