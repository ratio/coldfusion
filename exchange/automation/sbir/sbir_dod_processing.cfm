<cfsetting RequestTimeout = "90000">

<cfset theFile = "#data_path#\sbir\dod\SBIRDoD.xlsx">

<cfset counter = 0>

<cfif fileexists(theFile)>

	<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">

	<cfloop query="data" startrow="2">

			<cfquery name="exists" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			 select id from opp_sbir
			 where sbir_unique_id = '#topicnumber#'
			</cfquery>

			<cfif exists.recordcount is 1>

				<cfquery name="update" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  update opp_sbir
				  set ItemID = '#ItemID#',
					  Agency = '#Agency#',
					  SolicitationNumber = '#topicnumber#',
					  ReleaseDate = '#ReleaseDate#',
					  OpenDate = '#OpenDate#',
					  CloseDate = '#CloseDate#',
					  Objective = '#Objective#',
					  Description = '#Description#',
					  PhaseI = '#phasei#',
					  PhaseII = '#phaseii#',
					  PhaseIII = '#phaseiii#',
					  Keywords = '#keywords#',
					  TechnologyArea = '#technologyarea#',
					  Department = '#department#',
					  Title = '#title#',
					  Program = '#program#',
					  URL = '#url#',
					  SBIRReferences = '#sbirreferences#',
					  QuestionsandAnswers = '#questionsandanswers#',
					  AcquisitionProgram = '#acquisitionprogram#',
					  ITAR = '#itar#',
					  ICON = '#icon#',
					  Name1 = '#name1#',
					  Phone1 = '#phone1#',
					  Email1 = '#email1#',
					  Name2 = '#name2#',
					  Phone2 = '#phone2#',
					  Email2 = '#email2#',
					  SBIR_Unique_ID = '#topicnumber#',
					  updated = #now()#
				   where SolicitationNumber = '#topicnumber#'
				</cfquery>

			<cfelse>

				<cfquery name="insert" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
				  insert into opp_sbir
				  (
                   keywords,
                   title,
                   solicitationNumber,
                   source,
                   sbir_unique_id,
                   technologyarea,
                   objective,
                   description,
                   sbirreferences,
                   agency,
                   department,
                   phasei,
                   phaseii,
                   phaseiii,
                   updated
					)
					values
					(
					'#keywords#',
					'#title#',
					'#topicnumber#',
				    'DoD',
				    '#baa#',
				    '#technologyareas#',
				    '#objective#',
				    '#description#',
				    '#AvailableDocuments#',
				    '#agency#',
				    'Department of Defense',
				    '#phase1#',
				    '#phase2#',
				    '#phase3#',
				     #now()#
				    )
				</cfquery>

			</cfif>

       <cfset counter = counter + 1>

	</cfloop>

<cfoutput>SBIRs Imported / Upated - #counter#</cfoutput>

<cfelse>

 File not found.

</cfif>