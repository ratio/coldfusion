<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="updates" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from up
 left join usr on usr_id = up_usr_id
 where up_challenge_id = #session.challenge_id#
 order by up_date DESC
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>CHALLENGE UPDATES</td>
            <td valign=top align=right class="feed_sub_header">
            <a href="/exchange/sourcing/challenges/update_add.cfm"><img src="/images/plus3.png" alt="Add Update" title="Add Update" width=15 border=0 hspace=10>Add Update</a>&nbsp;|&nbsp;
            <a href="/exchange/sourcing/challenges/open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Updates allow you to add additional information to the Challenge.</td></tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif updates.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No Updates have been recorded.</td></tr>
	    <cfelse>

	   <cfloop query="updates">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #up_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="update_edit.cfm?update_id=#up_id#"><b>#up_name#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#up_name#</td>
			</cfif>
			<td class="feed_sub_header" valign=top" align=right width=75>
			<cfif up_post is 0>
			 Not Posted
			<cfelse>
			 Posted
			</cfif>
			</td>

		 </tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfif #up_desc# is "">No update provided<cfelse>#up_desc#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #up_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#up_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #up_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#up_url#" target="_blank" rel="noopener" rel="noreferrer">#up_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#usr_first_name# #usr_last_name# | #dateformat(up_date,'mm/dd/yyyy')# at #timeformat(up_date)#</td>
		   </tr>

		   <tr><td colspan=2><hr></td></tr>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>











	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>