<style>

/* Style The Dropdown Button */
.dropbtn {
  background-color: #000000;
  color: white;
  padding: 10px;
  font-size: 14px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  right: 0;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: blue;
  padding-right: 20px;
  padding-left: 10px;
  font-weight: normal;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #e0e0e0}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #000000;
}
</style>

<div class="dropdown" style="width: 300px; height: 30px;">
  <button class="dropbtn">Options</button>
  <div class="dropdown-content">
	<a href="/exchange/sourcing/challenges/" style="font-size: 18px; text-align: left;"><img src="/images/plus3.png" width=10 hspace=10 border=0>All Challenges</a>
	<a href="/exchange/sourcing/challenges/open.cfm" style="font-size: 18px; text-align: left;"><img src="/images/plus3.png" width=10 hspace=10 border=0>Challenge Summary</a>
	<a href="/exchange/sourcing/challenges/edit.cfm" style="font-size: 18px; text-align: left;"><img src="/images/plus3.png" width=10 hspace=10 border=0>Edit Challenge</a>
	<a href="/exchange/sourcing/challenges/sourcing/" style="font-size: 18px; text-align: left;"><img src="/images/plus3.png" width=10 hspace=10 border=0>Source Companies</a>
	<a href="/exchange/sourcing/challenges/announce/" style="font-size: 18px; text-align: left;"><img src="/images/plus3.png" width=10 hspace=10 border=0>Create Announcement</a>
	<a href="/exchange/sourcing/challenges/launch/" style="font-size: 18px; text-align: left;"><img src="/images/plus3.png" width=10 hspace=10 border=0>Launch & Monitor</a>
  </div>
</div>