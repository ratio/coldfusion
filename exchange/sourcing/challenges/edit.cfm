<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=3" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 220px;
  font-size: 12;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding-left: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 10px;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
  top: -5px;
  left: 105%;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>EDIT CHALLENGE</td>
            <td valign=top align=right><a href="/exchange/sourcing/challenges/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header">Challenge Name / Title</td>
				 <td><input class="input_text" type="text" name="challenge_name" style="width: 600px;" required maxlength="500" value="#info.challenge_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Organization</td>
				 <td><input class="input_text" type="text" name="challenge_organization" value="#info.challenge_organization#" style="width: 600px;" maxlength="500" placeholder="Name of Organization that is conducting the Challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="challenge_desc" style="width: 900px; height: 75px;" placeholder="Please provide a short description of what this Challenge is intented to solve.">#info.challenge_desc#</textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Today's<br>Challenges</td>
				 <td><textarea class="input_textarea" name="challenge_challenges" style="width: 900px; height: 75px;" placeholder="Please describes the challenges associated with today's solution or process.">#info.challenge_challenges#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
				 <td><textarea class="input_textarea" name="challenge_use_case" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how the new solution will be used in the future.">#info.challenge_use_case#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Future State<br>Solution</td>
				 <td><textarea class="input_textarea" name="challenge_future_state" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state solution and any constraints.">#info.challenge_future_state#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="challenge_keywords" maxlength="500" style="width: 900px;" value="#info.challenge_keywords#" required placeholder="Enter up to 5 keywords (seperated by commas) that are related to this Challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="challenge_url" maxlength="1000" style="width: 900px;" value="#info.challenge_url#" placeholder="Please provide the link the external URL / announcement."></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Challenge Reward</td>
				 <td><textarea class="input_textarea" name="challenge_reward" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the what Companies will receive by winning this Challenge.">#info.challenge_reward#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Participation<br>Instructions</td>
				 <td><textarea class="input_textarea" name="challenge_instructions" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide and / all instructions for Companies who participate in this Challenge.">#info.challenge_instructions#</textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" style="width: 165px;" value="#dateformat(info.challenge_start,'yyyy-mm-dd')#"></td></tr>

		 <tr><td class="feed_sub_header">End Date</td>
		     <td><input type="date" name="challenge_end" class="input_date" style="width: 165px;" value="#dateformat(info.challenge_end,'yyyy-mm-dd')#"></td></tr>

		 <tr><td class="feed_sub_header">Anonymous</td>
		     <td>
		     <select name="challenge_annoymous" class="input_select" style="width: 165px;">
		      <option value=0 <cfif info.challenge_annoymous is 0>selected</cfif>>No
		      <option value=1 <cfif info.challenge_annoymous is 1>selected</cfif>>Yes
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will hide your name and your organization when launched on the Exchange.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Discoverable</td>
		     <td>
		     <select name="challenge_public" class="input_select" style="width: 250px;">
		      <option value=0 <cfif info.challenge_public is 0>selected</cfif>>No, keep private
		      <option value=1 <cfif info.challenge_public is 1>selected</cfif>>Yes, allow others to view
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will display your Challenge on the Exchange after you choose to launch it.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Open</td>
		     <td>
		     <select name="challenge_open" class="input_select" style="width: 250px;">
		      <option value=0 <cfif info.challenge_open is 0>selected</cfif>>No, only to people I send it to
		      <option value=1 <cfif info.challenge_open is 1>selected</cfif>>Yes, allow others to respond
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will allow anyone to accept and interact with the Challenge.</span>
			 </div>

		     </td></tr>


                <tr><td class="feed_sub_header" valign=top>Challenge Image</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #info.challenge_image# is "">
					  <input type="file" name="challenge_image">
					<cfelse>
					  <img src="#media_virtual#/#info.challenge_image#" width=150><br><br>
					  <input type="file" name="challenge_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove Image
					 </cfif>

					 </td></tr>

			  <tr>
				 <td class="feed_sub_header">Total Prizes</td>
				 <td><input type="number" class="input_text" name="challenge_total_cash" value=#info.challenge_total_cash# style="width: 150px;"></td>
		      </tr>

		 <tr><td class="feed_sub_header">Status</td>
		     <td>
		      <select name="challenge_status" class="input_select" style="width: 165px;">
		       <option value=0 <cfif info.challenge_status is 0>selected</cfif>>Not Launched
		       <option value=1 <cfif info.challenge_status is 1>selected</cfif>>Launched
		       <option value=2 <cfif info.challenge_status is 2>selected</cfif>>Closed
		      </td></tr>

              <tr><td colspan=4><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header">Contact Name</td>
				 <td><input class="input_text" type="text" name="challenge_poc_name" maxlength="200" style="width: 300px;" value="#info.challenge_poc_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Title</td>
				 <td><input class="input_text" type="text" name="challenge_poc_title" maxlength="200" style="width: 300px;" value="#info.challenge_poc_title#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Email</td>
				 <td><input class="input_text" type="text" name="challenge_poc_email" maxlength="200" style="width: 300px;" value="#info.challenge_poc_email#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Phone</td>
				 <td><input class="input_text" type="text" name="challenge_poc_phone" maxlength="100" style="width: 300px;" value="#info.challenge_poc_phone#"></td>
		      </tr>

		      </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
                         <input type="submit" name="button" class="button_blue_large" value="Update">&nbsp;&nbsp;
			             <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Challenge?\r\nAre you sure you want to delete this Challenge?');">

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>