<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 220px;
  font-size: 12;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding-left: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 10px;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
  top: -5px;
  left: 105%;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #need_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>CREATE CHALLENGE</td>
            <td valign=top align=right><a href="/exchange/sourcing/challenges/"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>

              <tr><td class="feed_sub_header" style="color: green;" colspan=2>Need information has been successfully imported into the fields below.</td></tr>
              <tr><td height=5></td></tr>

			  <tr>
				 <td class="feed_sub_header">Challenge Name</td>
				 <td><input class="input_text" type="text" name="challenge_name" style="width: 600px;" required maxlength="500" value="#info.need_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Organization</td>
				 <td><input class="input_text" type="text" name="challenge_organization" value="#info.need_organization#" style="width: 600px;" maxlength="500" placeholder="Name of Organization that is conducting the Challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="challenge_desc" style="width: 900px; height: 75px;" placeholder="Please provide a short description of what this Challenge is intented to solve.">#info.need_desc#</textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Today's<br>Challenges</td>
				 <td><textarea class="input_textarea" name="challenge_challenges" style="width: 900px; height: 75px;" placeholder="Please describes the challenges associated with today's solution or process.">#info.need_challenges#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
				 <td><textarea class="input_textarea" name="challenge_use_case" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how the new solution will be used in the future.">#info.need_use_case#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Future State<br>Solution</td>
				 <td><textarea class="input_textarea" name="challenge_future_state" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state solution and any constraints.">#info.need_future_state#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="challenge_keywords" maxlength="1500" style="width: 900px;" value="#info.need_keywords#" required placeholder="Enter up to 5 keywords (seperated by commas) that are related to this Challenge."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="challenge_url" maxlength="1000" style="width: 900px;" value="#info.need_url#" placeholder="Please provide the link the external URL / announcement."></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Challenge Reward</td>
				 <td><textarea class="input_textarea" name="challenge_reward" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the what Companies will receive by winning this Challenge."></textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Participation<br>Instructions</td>
				 <td><textarea class="input_textarea" name="challenge_instructions" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide and / all instructions for Companies who participate in this Challenge."></textarea></td>
		      </tr>

              <tr><td height=10></td></tr>
              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">End Date</td>
		     <td><input type="date" name="challenge_end" class="input_date" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">Anonymous</td>
		     <td>
		     <select name="challenge_annoymous" class="input_select" style="width: 165px;">
		      <option value=0>No
		      <option value=1>Yes
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will hide your name and your organization when launched on the Exchange.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Discoverable</td>
		     <td>
		     <select name="challenge_public" class="input_select" style="width: 250px;">
		      <option value=0>No, keep private
		      <option value=1>Yes, allow others to view
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will display your Challenge on the Exchange after you choose to launch it.</span>
			 </div>

		     </td></tr>

		 <tr><td class="feed_sub_header">Open</td>
		     <td>
		     <select name="challenge_open" class="input_select" style="width: 250px;">
		      <option value=0>No, only to people I send it to
		      <option value=1>Yes, allow others to respond
		     </select>

			 <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			  <span class="tooltiptext">Selecting "Yes" will allow anyone to accept and interact with the Challenge.</span>
			 </div>

		     </td></tr>

			  <tr>
				 <td class="feed_sub_header">Total Prizes</td>
				 <td><input type="number" class="input_text" name="challenge_total_cash" style="width: 150px;"></td>
		      </tr>


              <tr><td class="feed_sub_header" valign=top>Challenge Image</td>
                  <td colspan=3 class="feed_sub_header" style="font-weight: normal;"><input type="file" name="challenge_image"></td></tr>

		 <tr><td class="feed_sub_header">Status</td>
		     <td class="feed_sub_header">Not Launched</td></tr>

              <tr><td colspan=4><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header">Contact Name</td>
				 <td><input class="input_text" type="text" name="challenge_poc_name" maxlength="200" style="width: 300px;" value="#info.need_poc_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Title</td>
				 <td><input class="input_text" type="text" name="challenge_poc_title" maxlength="200" style="width: 300px;" value="#info.need_poc_title#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Email</td>
				 <td><input class="input_text" type="text" name="challenge_poc_email" maxlength="200" style="width: 300px;" value="#info.need_poc_email#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Phone</td>
				 <td><input class="input_text" type="text" name="challenge_poc_phone" maxlength="100" style="width: 300px;" value="#info.need_poc_phone#"></td>
		      </tr>

		      <input type="hidden" name="challenge_need_id" value=#need_id#>

		      </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
              <input type="submit" class="button_blue_large" name="button" value="Create Challenge">&nbsp;&nbsp;

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>