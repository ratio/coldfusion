<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Create Challenge">

	<cfif #challenge_attachment# is not "">
		<cffile action = "upload"
		 fileField = "challenge_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 insert into challenge
	 (
	  challenge_name,
	  challenge_open,
	  challenge_public,
	  challenge_annoymous,
	  challenge_poc_name,
	  challenge_poc_title,
	  challenge_poc_phone,
	  challenge_poc_email,
	  challenge_attachment,
	  challenge_desc,
	  challenge_instructions,
	  challenge_reward,
	  challenge_start,
	  challenge_end,
	  challenge_phase_id,
	  challenge_created,
	  challenge_updated,
	  challenge_created_by_id,
	  challenge_hub_id,
	  challenge_company_id,
	  challenge_need_id,
	  challenge_url
	 )
	 values (
	 '#challenge_name#',
	  #challenge_open#,
	  #challenge_public#,
	  #challenge_annoymous#,
	 '#challenge_poc_name#',
	 '#challenge_poc_title#',
	 '#challenge_poc_phone#',
	 '#challenge_poc_email#',
      <cfif #challenge_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	 '#challenge_desc#',
	 '#challenge_instructions#',
	 '#challenge_reward#',
	 <cfif #challenge_start# is "">null<cfelse>'#challenge_start#'</cfif>,
	 <cfif #challenge_end# is "">null<cfelse>'#challenge_end#'</cfif>,
	  #challenge_phase_id#,
	  #now()#,
	  #now()#,
	  #session.usr_id#,
	  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	  #session.company_id#,
	  #session.need_id#,
	 '#challenge_url#'
	  )
	</cfquery>

	<cflocation URL="index.cfm?u=1" addtoken="no">

<cfelseif button is "Save Changes">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_attachment from challenge
		  where challenge_need_id = #session.need_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.challenge_attachment#">

	</cfif>

	<cfif #challenge_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_attachment from challenge
		  where challenge_need_id = #session.need_id#
		</cfquery>

		<cfif #getfile.challenge_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.challenge_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "challenge_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 update challenge
	 set challenge_name = '#challenge_name#',

		  <cfif #challenge_attachment# is not "">
		   challenge_attachment = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   challenge_attachment = null,
		  </cfif>

	     challenge_desc = '#challenge_desc#',
	     challenge_annoymous = #challenge_annoymous#,
	     challenge_poc_name = '#challenge_poc_name#',
	     challenge_poc_title = '#challenge_poc_title#',
	     challenge_poc_phone = '#challenge_poc_phone#',
	     challenge_open = #challenge_open#,
	     challenge_public = #challenge_public#,
	     challenge_poc_email = '#challenge_poc_email#',
	     challenge_instructions = '#challenge_instructions#',
	     challenge_reward = '#challenge_reward#',
	     challenge_start = <cfif #challenge_start# is "">null<cfelse>'#challenge_start#'</cfif>,
	     challenge_end = <cfif challenge_end is "">null<cfelse>'#challenge_end#'</cfif>,
	     challenge_phase_id = #challenge_phase_id#,
	     challenge_updated = #now()#,
	     challenge_url = '#challenge_url#'
	 where challenge_need_id = #session.need_id#
	</cfquery>

    <cflocation URL="index.cfm?u=2" addtoken="no">

<cfelseif button is "Delete Challenge">

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  select challenge_attachment from challenge
	  where (challenge_need_id = #session.need_id#)
	</cfquery>

	<cfif remove.challenge_attachment is not "">
	 <cffile action = "delete" file = "#media_path#\#remove.challenge_attachment#">
	</cfif>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 delete challenge
	 where challenge_need_id = #session.need_id#
	</cfquery>

    <cflocation URL="index.cfm?u=3" addtoken="no">

</cfif>
