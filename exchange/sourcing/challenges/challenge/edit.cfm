<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_created_by
 where need_id = #session.need_id#
</cfquery>

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="phase" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_phase
 order by challenge_phase_order
</cfquery>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_need_id = #session.need_id# and
 challenge_created_by_id = #session.usr_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>


       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">EDIT CHALLENGE</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>

	   </table>

	   <cfoutput>

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_sub_header">Challenge Name / Title</td>
		     <td><input type="text" name="challenge_name" class="input_text" size=70 value="#info.challenge_name#"></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Description</td>
		     <td><textarea name="challenge_desc" class="input_textarea" style="width: 900px; height: 200px;">#info.challenge_desc#</textarea></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Instructions<br>to Participate</td>
		     <td><textarea name="challenge_instructions" class="input_textarea" style="width: 900px; height: 100px;">#info.challenge_instructions#</textarea></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Winners Reward</td>
		     <td><textarea name="challenge_reward" class="input_textarea" style="width: 900px; height: 100px;">#info.challenge_reward#</textarea></td></tr>

		 <tr><td class="feed_sub_header">Information URL</td>
		     <td><input type="text" name="challenge_url" class="input_text" size=120 maxlength="1000" value="#info.challenge_url#"></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Attachment</td>
			 <td class="feed_sub_header" style="font-weight: normal;">

			<cfif #info.challenge_attachment# is "">
			  <input type="file" name="challenge_attachment">
			<cfelse>
			  <a href="#media_virtual#/#info.challenge_attachment#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#info.challenge_attachment#</a><br><br>
			  <input type="file" name="challenge_attachment"><br><br>
			  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove attachment
			 </cfif>

			 </td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" value="#info.challenge_start#" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">End Date</td>
		     <td><input type="date" name="challenge_end" class="input_date" value="#info.challenge_end#" style="width: 165px;"></td></tr>

         </cfoutput>

		 <tr><td class="feed_sub_header">Phase</td>
		     <td>
		     <select name="challenge_phase_id" class="input_select" style="width: 165px;">
		     <cfoutput query="phase">
		      <option value=#challenge_phase_id# <cfif #challenge_phase_id# is #info.challenge_phase_id#>selected</cfif>>#challenge_phase_name#
		     </cfoutput>
		     </select>
		     </td></tr>

		 <tr><td class="feed_sub_header">Anonymous</td>
		     <td>
		     <select name="challenge_annoymous" class="input_select" style="width: 165px;">
		      <option value=0 <cfif #info.challenge_annoymous# is 0>selected</cfif>>No
		      <option value=1 <cfif #info.challenge_annoymous# is 1>selected</cfif>>Yes
		     </select>

		     </td></tr>

		 <tr><td class="feed_sub_header">Discoverable</td>
		     <td>
		     <select name="challenge_public" class="input_select" style="width: 250px;">
		      <option value=0 <cfif #info.challenge_public# is 0>selected</cfif>>No, keep private
		      <option value=1 <cfif #info.challenge_public# is 1>selected</cfif>>Yes, allow others to see
		     </select>
		     </td></tr>

		 <tr><td class="feed_sub_header">Open</td>
		     <td>
		     <select name="challenge_open" class="input_select" style="width: 250px;">
		      <option value=0 <cfif #info.challenge_open# is 0>selected</cfif>>No, only to people I send it to
		      <option value=1 <cfif #info.challenge_open# is 1>selected</cfif>>Yes, allow others to respond
		     </select>
		     </td></tr>

         <tr><td colspan=2><hr></td></tr>

		 <cfoutput>

		 <tr><td class="feed_sub_header">Challenge POC Name</td>
		     <td><input type="text" name="challenge_poc_name" class="input_text" style="width: 300px;" value="#info.challenge_poc_name#"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Title</td>
		     <td><input type="text" name="challenge_poc_title" class="input_text" style="width: 300px;" value="#info.challenge_poc_title#"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Phone</td>
		     <td><input type="text" name="challenge_poc_phone" class="input_text" style="width: 300px;" value="#info.challenge_poc_phone#"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Email</td>
		     <td><input type="email" name="challenge_poc_email" class="input_text" style="width: 300px;" value="#info.challenge_poc_email#"></td></tr>


		 <tr><td colspan=2><hr></td></tr>
		 <tr><td></td>
		     <td>

             <input type="submit" class="button_blue_large" name="button" value="Save Changes">&nbsp;&nbsp;
	         <input class="button_blue_large" type="submit" name="button" value="Delete Challenge" vspace=10 onclick="return confirm('Delete Challenge?\r\nAre you sure you want to delete this Challenge?');">

		     </td></tr>

         </form>

         </cfoutput>

        </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>