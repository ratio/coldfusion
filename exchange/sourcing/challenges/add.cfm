<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=3" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<body style="text-align:center;">

<!--- Get Data --->

<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_created_by = #session.usr_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">CREATE CHALLENGE</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>

	   </table>

       <form action="choice.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td height=5></td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">There are multiple ways to create a Challenge.  Please select one of the options below.</td></tr>
         <tr><td height=5></td></tr>

       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td><input type="radio" style="height: 20px; width: 20px;" name="choice" value=1 checked></td><td class="feed_sub_header">Option 1.  Start from Scratch</td></tr>
		 <tr><td></td><td class="feed_sub_header" style="font-weight: normal;">I want to create to create a new Challenge and will enter all the information myself.</td></tr>

		 <tr><td><input type="radio" style="height: 20px; width: 20px;" name="choice" value=2></td><td class="feed_sub_header">Option 2.  Import information from a Future Need I already created.</td></tr>
		 <tr><td></td><td class="feed_sub_header" style="font-weight: normal;">Select Future Need&nbsp;&nbsp;
		 <select name="need_id" class="input_select">
		   <cfif need.recordcount is 0>
		    <option value=0>No Needs have been created
		   <cfelse>
		    <cfoutput query="need">
		     <option value=#need_id#>#need_name#
		    </cfoutput>
		   </cfif>

		  </td></tr>

		 <tr><td height=10></td></tr>
		 <tr><td colspan=2><hr></td></tr>
		 <tr><td height=10></td></tr>
		 <tr><td></td>
		     <td><input type="submit" name="button" value="Next >>" class="button_blue_large"></td></tr>

         </form>


        </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>