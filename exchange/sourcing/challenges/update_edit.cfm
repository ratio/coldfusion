<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from up
 where up_id = #update_id# and
 up_challenge_id = #session.challenge_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">EDIT UPDATE</td>
            <td align=right><a href="/exchange/sourcing/challenges/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

            <form action="update_db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header">Title</td>
				 <td><input class="input_text" type="text" name="up_name" style="width: 600px;" required maxlength="500" value="#info.up_name#" placeholder="Please provide a title / purpose for this update."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Update</td>
				 <td><textarea class="input_textarea" name="up_desc" style="width: 900px; height: 200px;" placeholder="Please add the full details of the update.">#info.up_desc#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="up_url" maxlength="1000" value="#info.up_url#" style="width: 900px;" placeholder="If required - http://www..."></td>
		      </tr>

                <tr><td class="feed_sub_header" valign=top>Attachment</td>
                    <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #info.up_attachment# is "">
					  <input type="file" name="up_attachment">
					<cfelse>
					  <a href="#media_virtual#/#info.up_attachment#" width=150>#info.up_attachment#</a><br><br>
					  <input type="file" name="up_attachment"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove Attachment
					 </cfif>

					 </td></tr>

                <tr><td class="feed_sub_header" valign=top>Post Online</td>
                    <td class="feed_sub_header" style="font-weight: normal;">
                    <select name="up_post" class="input_select">
                     <option value=0 <cfif #info.up_post# is 0>selected</cfif>>No, keep hidden
                     <option value=1 <cfif #info.up_post# is 1>selected</cfif>>Yes, allow others to see
                    <seect>

					 </td></tr>

			  <input type="hidden" name="update_id" value=#update_id#>
			  </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=2>
                         <input type="submit" name="button" class="button_blue_large" value="Save Changes">&nbsp;&nbsp;
			             <input type="submit" name="button" class="button_blue_large" value="Delete Update" onclick="return confirm('Delete Update?\r\nAre you sure you want to delete this update?');">

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>