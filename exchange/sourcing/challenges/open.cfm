<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/challenges/challenge_activity.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/">DEMAND PORTFOLIO</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_risk.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/risks/">RISKS</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/">NEEDS</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/challenges/">CHALLENGES</a></span>
          </div>

       <div class="main_box_2">

       <cfoutput>

       <cfinclude template="/exchange/sourcing/challenges/challenge_header.cfm">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
	       <tr><td height=10></td></tr>
	       <tr><td class="feed_header">CHALLENGE SUMMARY</td>
	           <td class="feed_sub_header" align=right><img src="/images/icon_analyze.png" hspace=10 align=absmiddle width=20><a href="public.cfm" target="_blank" rel="noopener" rel="noreferrer">View Public Challenge Page</a></td></tr>
           <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif isdefined("u")>
	    <cfif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge has been successfully updated.</td></tr>
        <cfelseif u is 10>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Attachment has been successfully added.</td></tr>
        <cfelseif u is 20>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Attachment has been successfully updated.</td></tr>
        <cfelseif u is 30>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Attachment has been successfully deleted.</td></tr>
        <cfelseif u is 41>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Update has been successfully added.</td></tr>
        <cfelseif u is 42>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Update has been successfully saved.</td></tr>
        <cfelseif u is 43>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Update has been successfully deleted.</td></tr>
        <cfelseif u is 100>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Challenge has been successfully created.</td></tr>
        </cfif>
	   </cfif>

             <tr>
                 <td class="feed_sub_header">DESCRIPTION</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">TODAY'S CHALLANGES</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_desc# is "">Not Provided<cfelse>#challenge_info.challenge_desc#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_challenges# is "">Not Provided<cfelse>#challenge_info.challenge_challenges#</cfif></td>
             </tr>

             <tr><td class="feed_sub_header">USE CASE SCENARIO</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">FUTURE STATE SOLUTION</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_use_case# is "">Not Provided<cfelse>#challenge_info.challenge_use_case#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_future_state# is "">Not Provided<cfelse>#challenge_info.challenge_future_state#</cfif></td>
             </tr>

             <tr>
                 <td class="feed_sub_header">KEYWORDS</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">REFERENCE URL</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_keywords# is "">Not Provided<cfelse>#challenge_info.challenge_keywords#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_url# is "">Not Provided<cfelse><a href="#challenge_info.challenge_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#challenge_info.challenge_url#</a></cfif></td>
             </tr>

             <tr><td height=10></td></tr>
             <tr><td colspan=3><hr></td></tr>
             <tr><td height=10></td></tr>

             <tr><td class="feed_sub_header">REWARD</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">INSTRUCTIONS</td>
             </tr>


             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_reward# is "">Not Provided<cfelse>#challenge_info.challenge_reward#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #challenge_info.challenge_instructions# is "">Not Provided<cfelse>#challenge_info.challenge_instructions#</cfif></td>
             </tr>



           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>

             <tr>
                 <td class="feed_sub_header" width=200>POINT OF CONTACT</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #challenge_info.challenge_poc_name# is "">Not Provided<cfelse>#challenge_info.challenge_poc_name#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>TITLE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #challenge_info.challenge_poc_email# is "">Not Provided<cfelse>#challenge_info.challenge_poc_title#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>EMAIL</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #challenge_info.challenge_poc_email# is "">Not Provided<cfelse>#challenge_info.challenge_poc_email#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>PHONE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #challenge_info.challenge_poc_phone# is "">Not Provided<cfelse>#challenge_info.challenge_poc_phone#</cfif></td></tr>
             </tr>

           </table>

			<cfquery name="attachments" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 select * from attachment
			 join usr on usr_id = attachment_created_by
			 where attachment_challenge_id = #session.challenge_id#
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">ATTACHMENTS</td>
                 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="attachment_add.cfm">Add Attachment</a></td></tr>
           </table>

       </cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif attachments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No attachments have been added.</td></tr>
             <cfelse>

              <tr>
                  <td class="feed_sub_header">NAME</td>
                  <td class="feed_sub_header">DESCRIPTION</td>
                  <td class="feed_sub_header">FILE</td>
                  <td class="feed_sub_header" align=center>ADDED</td>
                  <td class="feed_sub_header" align=right>BY</td>
              </tr>

              <cfset counter = 0>

              <cfoutput query="attachments">

              <cfif counter is 0>
               <tr bgcolor="FFFFFF">
              <cfelse>
               <tr bgcolor="e0e0e0">
              </cfif>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top width=200><b><a href="attachment_edit.cfm?attachment_id=#attachment_id#">#attachment_name#</a></b></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_desc# is "">Not Provided<cfelse>#attachment_desc#</cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_file# is "">No attachment<cfelse><a href="#media_virtual#/#attachment_file#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#attachment_file#</a></cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=center width=120>#dateformat(attachment_updated,'mm/dd/yyyy')#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=right width=150>#usr_first_name# #usr_last_name#</td>

                  </tr>

                  <cfif counter is 0>
                   <cfset counter = 1>
                  <cfelse>
                   <cfset counter = 0>
                  </cfif>

              </cfoutput>

             </cfif>

			<cfquery name="updates" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from up
			 left join usr on usr_id = up_usr_id
			 where up_challenge_id = #session.challenge_id#
			 order by up_date DESC
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">CHALLENGE UPDATES</td>
                 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="update_add.cfm">Add Update</a></td></tr>
           </table>



	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif updates.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No Updates have been recorded.</td></tr>
	    <cfelse>

	   <cfloop query="updates">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #up_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="update_edit.cfm?update_id=#up_id#"><b>#ucase(up_name)#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#ucase(up_name)#</td>
			</cfif>
			<td class="feed_sub_header" valign=top align=right width=75>
			<cfif up_post is 0>
			 NOT POSTED
			<cfelse>
			 POSTED
			</cfif>
			</td>

		 </tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfif #up_desc# is "">No update provided<cfelse>#up_desc#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #up_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#up_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #up_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#up_url#" target="_blank" rel="noopener" rel="noreferrer">#up_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#usr_first_name# #usr_last_name# | #dateformat(up_date,'mm/dd/yyyy')# at #timeformat(up_date)#</td>
		   </tr>

		   <tr><td colspan=2><hr></td></tr>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>














           </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>