<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfif isdefined("selected_company")>

		<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 delete need_comp
		 where need_comp_challenge_id = #session.challenge_id# and
			   need_comp_selected_company_id in (#selected_company#)
		</cfquery>

	</cfif>

</cftransaction>

<cflocation URL="/exchange/sourcing/challenges/sourcing/index.cfm?u=2" addtoken="no">