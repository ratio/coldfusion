<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>FIND COMPANIES</td>
             <td align=right valign=absmiddle class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=2><hr></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<form action="set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
		 <tr><td class="feed_sub_header" width=200>Keyword Matching</td>
		     <td colspan=3><input class="input_text" type="text" style="width: 275px;" placeholder = "keyword" name="auto_keyword" <cfif isdefined("session.auto_keywords")>value="<cfoutput>#session.auto_keywords#</cfoutput>"</cfif> required onfocus="clearThis(this)">
         <tr><td height=10></td></tr>
		 <tr>
		    <td class="feed_sub_header">Sources</td>
            <td width=50><input type="checkbox" name="non" <cfif isdefined("session.non") and session.non is 1>checked</cfif> style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Non Federal Companies</td></tr>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="awards" <cfif isdefined("session.awards") and session.awards is 1>checked</cfif> style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Won Federal Contracts</td>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="sbirs" <cfif isdefined("session.sbirs") and session.sbirs is 1>checked</cfif> style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Won SBIR/STTRs</td>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="grants" <cfif isdefined("session.grants") and session.grants is 1>checked</cfif> style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Won Grants</td>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="patents" <cfif isdefined("session.patents") and session.patents is 1>checked</cfif> style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Received Patents</td>

		 </tr>

		 <tr><td colspan=3><hr></td></tr>
		 <tr><td height=10></td></tr>

         <tr><td></td><td colspan=3><input type="submit" name="button" value="Find Companies" class="button_blue_large"></td></tr>

          </form>

          </table>


       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>