<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="selected" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need_comp
 left join company on company_id = need_comp_selected_company_id
 where need_comp_challenge_id = #session.challenge_id#
 order by company_name
</cfquery>

<cfquery name="sel" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
select * from need_comp
where need_comp_selected_company_id = #id# and
need_comp_challenge_id = #session.challenge_id# and
need_comp_contender = 1
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>COMPANY INFORMATION</td>
             <td align=right valign=absmiddle>

					<form action="go.cfm" method="post">
						   <span class="feed_sub_header">Selected&nbsp;&nbsp;</span>
						   <select name="selected_company_id" class="input_select" style="width: 250px;">
						   <cfoutput query="selected">
							 <option value=#selected.company_id# <cfif #selected.company_id# is #id#>selected</cfif>>#selected.company_name#
						   </cfoutput>
						</select>

						&nbsp;<input class="button_blue" type="submit" name="button" value="Go">

				   <span class="feed_sub_header">&nbsp;&nbsp;&nbsp;<a href="index.cfm">Return</a></span>

					 </form>

             </td>

		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
	    <cfif u is 1>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully added.</td></tr>
	    <cfelseif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully updated.</td></tr>
	    <cfelseif u is 3>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully deleted.</td></tr>
	    </cfif>
	   </cfif>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=150>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option">

                    <a href="open.cfm?id=#info.company_id#">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=100 border=0>
					</cfif>
					</a>

					</td>
			    </tr>

                <!--- Social Icons --->

                <tr><td height=10></td></tr>

                <tr><td>

                <cfif info.company_twitter_url is not "">
                 <a href="#info.company_twitter_url#" target="blank"><img src="/images/icon_twitter.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif info.company_linkedin_url is not "">
                 <a href="#info.company_linkedin_url#" target="blank"><img src="/images/icon_linkedin.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif info.company_facebook_url is not "">
                 <a href="#info.company_facebook_url#" target="blank"><img src="/images/icon_facebook.png" width=20 border=0 hspace=5></a>
                </cfif>

                </td></tr>

			    <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header">

                <cfif sel.recordcount is 1>
	                <a href="select.cfm?id=#id#&s=0"><img src="/images/icon_checked.png" width=20 hspace=10 align=absmiddle alt="Unselect" title="Unselect" border=0></a><a href="select.cfm?id=#id#&s=0" alt="Unselect" title="Unselect">Selected</a>
                <cfelse>
	                <a href="select.cfm?id=#id#&s=1"><img src="/images/icon_unchecked.png" width=20 hspace=10 align=absmiddle alt="Select" title="Select" border=0></a><a href="select.cfm?id=#id#&s=1" alt="Select" title="Select">Select</a>
                </cfif>

                </td></tr>

			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(info.company_name)#</td>
	              <td class="feed_sub_header" align=right width=600>

	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/resource_home.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Full Company Profile</a>

	              &nbsp;|&nbsp;
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_dashboard.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Award Dashboard</a>

	              </td></tr>

                 <tr><td class="feed_option" colspan=2><b>OVERVIEW</b></td></tr>
                 <tr>

					<td class="feed_option" colspan=2>
					<cfif #info.company_about# is "">
					No overview found.
					<cfelse>
					#info.company_about#
					</cfif>

					</td>

			    </tr>

			    <cfif info.company_long_desc is not "">

                 <tr><td class="feed_option" colspan=2><b>FULL DESCRIPTION</b></td></tr>

                 <tr>

					<td class="feed_option" style="font-weight: normal;" colspan=2>
					<cfif #info.company_long_desc# is "">
					No full description found.
					<cfelse>
					#info.company_long_desc#
					</cfif>

					</td>
			    </tr>

			    </cfif>


                 <tr><td class="feed_option"><b>KEYWORDS / TAGS</b></td>
                     <td class="feed_option" align=right><b>WEBSITE</b></td></tr>

                 <tr>

					<td class="feed_option">
					<cfif #info.company_keywords# is "">
					No keywords found.
					<cfelse>
					#info.company_keywords#
					</cfif>

					</td>

					<td align=right class="feed_option">

					<u>
					<a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(info.company_website) GT 40>
					#ucase(left(info.company_website,40))#...
					<cfelse>
					#ucase(info.company_website)#
					</cfif>
					</u>
					</a>

					</td>
			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

       <!--- POC Information --->

	   <cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select * from sams
		 where duns = '#info.company_duns#'
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=10></td></tr>
		   <tr><td class="feed_header">POINTS OF CONTACT</td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
            <td class="feed_sub_header">NAME</td>
            <td class="feed_sub_header">TITLE / ROLE</td>
            <td class="feed_sub_header">EMAIL</td>
            <td class="feed_sub_header" align=right>PHONE</td>
        </tr>

        <cfoutput>

        <cfset poc = 0>

        <cfif info.company_poc_phone is not "" and info.company_poc_email is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif info.company_poc_first_name is "" and info.company_poc_last_name is "">Unknown<cfelse>#info.company_poc_first_name# #info.company_poc_last_name#</cfif></td>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif #info.company_poc_title# is "">Unknown<cfelse>#info.company_poc_title#</cfif></td>
			   <td class="feed_sub_header" style="font-weight: normal;">#info.company_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#info.company_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.poc_fnme is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_fnme# #sams.poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_us_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.pp_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_fname# #sams.pp_poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.pp_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.alt_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_fname# #sams.alt_poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.akt_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.alt_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.elec_bus_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_us_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.elec_bus_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>


        <cfif poc is 0>
         <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No contact information could be found.</td></tr>
        </cfif>

        </cfoutput>

        <tr><td colspan=4><hr></td></tr>

       </table>

       <!--- Company Feedback --->

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">COMPANY COMMENTS & INTELLIGENCE</td>
	       <td class="feed_sub_header" align=right>

	       <cfoutput>
	       <img src="/images/plus3.png" width=15 hspace=10><a href="comment_add.cfm?id=#id#" align=absmiddle>Add Comments</a>
	       </cfoutput>

	       </td></tr>
       </table>

		<cfquery name="notes" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select * from company_intel
		 left join usr on usr_id = company_intel_created_by_usr_id
		 left join comments_rating on company_intel_rating = comments_rating_value
		 where  company_intel_company_id = #id# and
		       (company_intel_created_by_usr_id = #session.usr_id# or company_intel_created_by_company_id = #session.company_id#)
		 order by company_intel_created_date DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif notes.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No comments or notes have been recorded.</td></tr>
	    <cfelse>

	   <cfloop query="notes">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #company_intel_created_by_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><a href="comment_edit.cfm?id=#id#&company_intel_id=#company_intel_id#"><b>#company_intel_context#</b></a></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#company_intel_context#</td>
			</cfif>
			<td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=75>
			<img src="/images/#comments_rating_image#" height=16>
			</td>

		 </tr>

		 <tr><td colspan=4 class="feed_sub_header" style="font-weight: normal;"><cfif #company_intel_comments# is "">No comments provided<cfelse>#company_intel_comments#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #company_intel_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#company_intel_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #company_intel_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#company_intel_url#" target="_blank" rel="noopener" rel="noreferrer">#company_intel_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#usr_first_name# #usr_last_name# | #dateformat(company_intel_created_date,'mm/dd/yyyy')# at #timeformat(company_intel_created_date)#</td>
		   </tr>

		   <tr><td colspan=2><hr></td></tr>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>