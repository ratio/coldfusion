<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="selected_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select need_comp_selected_company_id from need_comp
 where need_comp_challenge_id = #session.challenge_id#
</cfquery>

<cfif selected_list.recordcount is 0>
 <cfset comp_list = 0>
<cfelse>
 <cfset comp_list = valuelist(selected_list.need_comp_selected_company_id)>
</cfif>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">

 select distinct(company_id), company_logo, company_keywords, company_meta_keywords, company.company_website, company_name, company_city, company_state from company
 where company_id = -1

 <!--- Non --->

 <cfif session.non is 1>
  union
  select distinct(company_id), company_logo, company_keywords, company_meta_keywords, company.company_website, company_name, company_city, company_state from company

	<cfif isdefined("session.add_filter")>
	 where contains((company_name, company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(session.auto_keywords)#"')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((company_name, company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((company_name, company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#trim(session.auto_keywords)#"')
	</cfif>

 </cfif>

 <!--- Awards --->

 <cfif session.awards is 1>
  union
  select distinct(company_id), company_logo, company_keywords, company_meta_keywords, company.company_website, company_name, company_city, company_state from company
  join award_data on recipient_duns = company_duns

	<cfif isdefined("session.add_filter")>
	 where contains((recipient_name, award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(session.auto_keywords)#"')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((recipient_name, award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((recipient_name, award_description, awarding_agency_name, awarding_sub_agency_name),'"#trim(session.auto_keywords)#"')
	</cfif>

 </cfif>

 <!--- SBIRs --->

 <cfif session.sbirs is 1>
  union
  select distinct(company_id), company_logo, company_keywords, company_meta_keywords, company.company_website, company_name, company_city, company_state from company
  join sbir on duns = company_duns
	<cfif isdefined("session.add_filter")>
	 where contains((company, award_title,abstract, research_keywords),'"#trim(session.auto_keywords)#"')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((company, award_title,abstract, research_keywords),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((company, award_title,abstract, research_keywords),'"#trim(session.auto_keywords)#"')
	</cfif>

  and company_duns <> ''
 </cfif>

 <!--- Grants --->

 <cfif session.grants is 1>
  union
  select distinct(company_id), company_logo, company_keywords, company_meta_keywords, company.company_website, company_name, company_city, company_state from company
  join grants on recipient_duns = company_duns

	<cfif isdefined("session.add_filter")>
	 where contains((recipient_name, award_description),'"#trim(session.auto_keywords)#"')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((recipient_name, award_description),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((recipient_name, award_description),'"#trim(session.auto_keywords)#"')
	</cfif>


 </cfif>

 <!--- Patents --->

 <cfif session.patents is 1>
  union
  select distinct(company_id), company_logo, company_keywords, company_meta_keywords, company.company_website, company_name, company_city, company_state from patent
  left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
  left join company on company_name = patent_rawassignee.organization

	<cfif isdefined("session.add_filter")>
	 where contains((patent.patent_id, title, abstract),'"#trim(session.auto_keywords)#"')
	 <cfloop index="i" list="#session.add_filter#">
	   <cfoutput>
	   and contains((patent.patent_id, 	title, abstract),'"#trim(i)#"')
	   </cfoutput>
	 </cfloop>
	<cfelse>
	 where contains((patent.patent_id, title, abstract),'"#trim(session.auto_keywords)#"')
	</cfif>
  and patent.patent_id is not null
 </cfif>

 order by company_name

</cfquery>

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfset perpage = 100>

<cfparam name="url.start" default="1">
<cfif not isNumeric(url.start) or url.start lt 1 or url.start gt info.recordCount or round(url.start) neq url.start>
	<cfset url.start = 1>
</cfif>

<cfset totalPages = ceiling(info.recordCount / perpage)>
<cfset thisPage = ceiling(url.start / perpage)>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="filter_box.cfm">

       </td><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <cfoutput>
		 <tr><td class="feed_header" valign=absmiddle>FIND COMPANIES</td>
		 </cfoutput>
             <td align=right valign=absmiddle class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
	    <cfif u is 1>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully added.</td></tr>
	    <cfelseif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully removed.</td></tr>
	    </cfif>
	   </cfif>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfoutput>
				 <td class="feed_sub_header">SEARCH RESULTS</td>
				 <td class="feed_sub_header" align=right>

					<cfif info.recordcount GT #perpage#>
						<b>#thisPage# of #totalPages#</b>&nbsp;&nbsp;

						<cfif url.start gt 1>
							<cfset link = cgi.script_name & "?start=" & (url.start - perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_previous.png" alt="Previous Page" title="Previous Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>

						<cfif (url.start + perpage - 1) lt info.recordCount>
							<cfset link = cgi.script_name & "?start=" & (url.start + perpage) & "&sv=#sv#">
							<a href="#link#"><img src="/images/icon_next.png" alt="Next Page" title="Next Page" border=0 height=25 valign=top></a>
						<cfelse>
						</cfif>
					</cfif>

				 </td></tr>
			 <tr><td height=10></td></tr>
         </cfoutput>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif info.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies were found.</td>
              <td align=right></td></tr>

         <cfelse>

			<tr>
				<td class="feed_sub_header">Select</td>
				<td></td>
				<td class="feed_sub_header">Company Name</td>
				<td class="feed_sub_header">Keywords / Tags</td>
				<td class="feed_sub_header">Website</td>
				<td class="feed_sub_header">City</td>
				<td class="feed_sub_header" align=center>State</td>
			</tr>

			<tr><td height=10></td></tr>

          <cfset row_counter = 0>

          <form action="remove_selected.cfm" method="post">

          <cfoutput query="info" startrow="#url.start#" maxrows="#perpage#">

                <tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>

					<td width=1%>&nbsp;&nbsp;

                    <cfif listfind(comp_list,info.company_id)>
                    <a href="check.cfm?u=0&company_id=#info.company_id#"><img src="/images/icon_checked.png" width=20 border=0 alt="Unselect" title="Unselect"></a>
                    <cfelse>
                    <a href="check.cfm?u=1&company_id=#info.company_id#"><img src="/images/icon_unchecked.png" width=20 border=0 alt="Select" title="Select"></a>
                    </cfif>

					</td>

					<td width=100 class="feed_option" align=center>

                    <a href="open.cfm?id=#info.company_id#&d=1">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=40 border=0>
					</cfif>
					</a>


					</td>
					<td class="feed_sub_header" width=250><a href="profile.cfm?id=#info.company_id#&d=1">#ucase(info.company_name)#</a></td>
					<td class="feed_option">

                    <cfif info.company_keywords is "" and info.company_meta_keywords is "">
                    NO KEYWORDS FOUND
                    <cfelse>
						<cfif info.company_keywords is "">
						#ucase(wrap(info.company_meta_keywords,30,0))#
						<cfelse>
						#ucase(wrap(info.company_keywords,30,0))#
						</cfif>
					</cfif>

					</td>

					<td class="feed_option" width=250>
					<cfif #info.company_website# is "">
						 NO WEBSITE FOUND
					<cfelse>
						<a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
						<cfif len(info.company_website) GT 40>
						<u>#ucase(left(info.company_website,40))#...</u>
						<cfelse>
						<u>#ucase(info.company_website)#</u>
						</cfif>
						</a>
					</cfif>
					</td>

					<td class="feed_option">#ucase(info.company_city)#</a></td>
					<td class="feed_option" width=75 align=center>#ucase(info.company_state)#</a></td>

				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				 <tr><td colspan=10><hr></td></tr>

			</cfoutput>

          </form>

          </table>

         </cfif>

       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>