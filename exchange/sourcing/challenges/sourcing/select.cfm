<cfif isdefined("p")>
	<cfif s is 1>

	        <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 insert need_comp
			 (need_comp_contender, need_comp_challenge_id, need_comp_selected_company_id)
			 values
			 (1, #session.challenge_id#,#id#)
			</cfquery>

	<cfelse>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 delete need_comp
			 where need_comp_contender = 1 and
			       need_comp_challenge_id = #session.challenge_id# and
				   need_comp_selected_company_id = #id#
		</cfquery>
	</cfif>

	<cflocation URL="/exchange/sourcing/challenges/sourcing/profile.cfm?id=#id#" addtoken="no">
</cfif>

<cfif s is 1>

		<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 update need_comp
		 set need_comp_contender = 1
		 where need_comp_challenge_id = #session.challenge_id# and
		       need_comp_selected_company_id = #id#
		</cfquery>

<cfelse>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 update need_comp
		 set need_comp_contender = null
		 where need_comp_challenge_id = #session.challenge_id# and
		       need_comp_selected_company_id = #id#
	</cfquery>

</cfif>

<cfif isdefined("l")>
	<cflocation URL="/exchange/sourcing/challenges/sourcing/" addtoken="no">
<cfelse>
	<cflocation URL="/exchange/sourcing/challenges/sourcing/open.cfm?id=#id#" addtoken="no">
</cfif>