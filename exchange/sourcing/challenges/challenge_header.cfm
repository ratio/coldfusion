<style>

/* Style The Dropdown Button */
.dropbtn {
  background-color: #283E4A;
  color: white;
  padding: 10px;
  font-size: 13px;
  border: none;
  cursor: pointer;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  right: 0;
  box-shadow: 0px 8px 16px 2px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: blue;
  padding-right: 20px;
  padding-left: 10px;
  font-weight: normal;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #e0e0e0}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #000000;
}
</style>

<cfquery name="challenge_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

<cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header">CHALLENGE - #ucase(challenge_info.challenge_name)#</td>
           <td align=right class="feed_sub_header" align=right></td>
           <td align=right class="feed_sub_header">


			<div class="dropdown" style="width: 300px;">
			  <button class="dropbtn">Options</button>
			  <div class="dropdown-content">
				<a href="/exchange/sourcing/challenges/" style="font-size: 18px; text-align: left; padding-left: 20px;" valign=absmiddle>All Challenges</a>
				<a href="/exchange/sourcing/challenges/open.cfm" style="font-size: 18px; text-align: left; padding-left: 20px;">Challenge Summary</a>
				<a href="/exchange/sourcing/challenges/edit.cfm" style="font-size: 18px; text-align: left; padding-left: 20px;">Edit Challenge</a>
				<a href="/exchange/sourcing/challenges/sourcing/" style="font-size: 18px; text-align: left; padding-left: 20px;">Source Companies</a>
				<a href="/exchange/sourcing/challenges/announce/" style="font-size: 18px; text-align: left; padding-left: 20px;">Launch Challenge</a>
				<a href="/exchange/sourcing/challenges/manage/" style="font-size: 18px; text-align: left; padding-left: 20px;">Manage Challenge</a>
			  </div>
			</div>

           </td></tr>

       <tr><td height=10></td></tr>

       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td colspan=10><hr></td></tr>

       <tr><td valign=middle width=125>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif challenge_info.challenge_image is "">
				  <img src="/images/stock_challenge.png" width=100>
				<cfelse>
				  <img src="#media_virtual#/#challenge_info.challenge_image#" width=100>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                <td class="feed_sub_header">UNIQUE ID</td>
                <td class="feed_sub_header">ORGANIZATION</td>
                <td class="feed_sub_header" align=center>STATUS</td>
                <td class="feed_sub_header" align=center>START DATE</td>
                <td class="feed_sub_header" align=center>END DATE</td>
                <td class="feed_sub_header" align=right>UPDATED</td>
             </tr>

             <tr>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" width=150>#challenge_info.challenge_code#</td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #challenge_info.challenge_organization# is "">Not Identified<cfelse>#challenge_info.challenge_organization#</cfif></td>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120>

				  <cfif challenge_info.challenge_status is 0>
				   Not Launched
				  <cfelseif challenge_info.challenge_status is 1>
				   Launched
				  <cfelse>
				   Closed
				  </cfif>

			    </td>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120><cfif challenge_info.challenge_start is "">TBD<cfelse>#dateformat(challenge_info.challenge_start,'mm/dd/yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120><cfif challenge_info.challenge_end is "">TBD<cfelse>#dateformat(challenge_info.challenge_end,'mm/dd/yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=120>#dateformat(challenge_info.challenge_updated,'mm/dd/yyyy')#</td>
             </tr>

          </table>

         </td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=10><hr></td></tr>

       </table>

</cfoutput>