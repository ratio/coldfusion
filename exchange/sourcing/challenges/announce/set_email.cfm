<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Continue">
 <cflocation URL="step_4.cfm" addtoken="no">
</cfif>

<cftransaction>

<cfif listlen(email) GT 0>

   <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    delete challenge_email
    where challenge_email_challenge_id = #session.challenge_id#
   </cfquery>

 <cfloop index="e" list="#email#">

   <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
    insert into challenge_email
    (challenge_email_challenge_id, challenge_email_email_address)
    values
    (#session.challenge_id#,'#e#')
   </cfquery>

 </cfloop>

</cfif>

<cftransaction>

<cflocation URL="step_4.cfm" addtoken="no">