<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/challenges/challenge_activity.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/">DEMAND PORTFOLIO</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_risk.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/risks/">RISKS</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_config.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/">NEEDS</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/challenges/">CHALLENGES</a></span>
          </div>

       <div class="main_box_2">

       <cfinclude template="/exchange/sourcing/challenges/challenge_header.cfm">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><a href="index.cfm">LAUNCH CHALLENGE:  STEP 2 - APPROVE ANNOUNCEMENT</td>
			 <td class="feed_sub_header" align=right><img src="/images/icon_return2.png" width=20 hspace=10 align=absmiddle><a href="index.cfm">Return to Process</a></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3>This step requires you to approve the announcement which is displayed below.  After completing this step you will choose what Companies will receive the Challenge.</td></tr>
		 <tr><td colspan=10><hr></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">Announcement information has been successfully saved.</td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

	   </table>

	   <form action="db.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td class="feed_sub_header" style="font-weight: normal;">&nbsp;<b>EXCHANGE POST</b> - Snapshot of the Challenge that users will see in their Feed and email.</td>
             <td class="feed_sub_header" align=right><input type="checkbox" style="width: 20px; height: 20px;" name="post" required></td>
             <td class="feed_sub_header" align=right width=110>I APPROVE&nbsp;&nbsp;&nbsp;</td></tr>
         <tr><td colspan=3>

         <!--- Show Preview --->

	     <div class="feed_box">

			  <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td width=120 valign=middle>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<cfif info.challenge_image is "">
						  <img src="/images/app_innovation.png" width=100 border=0>
						<cfelse>
						  <img src="#media_virtual#/#info.challenge_image#" width=100 border=0>
						</cfif>

					 </table>

					 </td><td valign=top>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr>
						   <td class="feed_title" valign=top>CHALLENGE - #ucase(info.challenge_name)#</td>
						   <td align=right width=100 valign=top>

						   </td></tr>
					   <tr><td class="feed_option">#info.challenge_desc#</td></tr>
					   <tr><td class="feed_option"><b>Challenge Timeline: </b> #dateformat(info.challenge_start,'mm/dd/yyyy')# - #dateformat(info.challenge_end,'mm/dd/yyyy')#</td></tr>
					 </table>

					 </td></tr>

			</table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=10></td></tr>
				 <tr><td class="feed_sub_header" colspan=2>#ucase(info.challenge_organization)#</td></tr>
				 <tr><td height=10></td></tr>
				 <tr><td colspan=2 class="feed_option"><b>FUTURE STATE SOLUTION</b></td></tr>
				 <tr><td colspan=2 class="feed_option">#info.challenge_future_state#</td></tr>

				 <cfif #info.challenge_url# is not "">

				  <tr><td height=10></td></tr>
				  <tr><td class="link_small_gray"><a href="#info.challenge_url#" target="_blank" rel="noopener" rel="noreferrer">#info.challenge_url#</a></td>
					  <td align=right class="link_small_gray" align=right><b>1 Minute Ago</b></td></tr>

				 <tr><td height=5></td></tr>
				 </cfif>

			  </table>

          </div>

	     </cfoutput>

         </td></tr>

         <tr><td height=15></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">&nbsp;<b>MORE INFORMATION</B> - When clicking on the above image or the Challenge title, users will be taken to the below screen with more information.</td>
             <td class="feed_sub_header" align=right><input type="checkbox" style="width: 22px; height: 22px;" name="more" required></td>
             <td class="feed_sub_header" align=right width=110>I APPROVE&nbsp;&nbsp;&nbsp;</td></tr>
         </tr>
         </table>


         <div class="feed_box">

         <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td width=120 valign=middle>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<cfif info.challenge_image is "">
						  <img src="/images/app_innovation.png" width=100 border=0>
						<cfelse>
						  <img src="#media_virtual#/#info.challenge_image#" width=100 border=0>
						</cfif>

					 </table>

					 </td><td valign=top>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr>
						   <td class="feed_title" valign=top>CHALLENGE - #ucase(info.challenge_name)#</td>
						   <td align=right width=100 valign=top>

						   </td></tr>
					   <tr><td class="feed_option">#info.challenge_future_state#</td></tr>
					   <tr><td class="feed_option"><b>Challenge Timeline: </b> #dateformat(info.challenge_start,'mm/dd/yyyy')# - #dateformat(info.challenge_end,'mm/dd/yyyy')#</td></tr>
					 </table>

					 </td></tr>

			</table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		    <tr><td class="feed_sub_header" colspan=2>#ucase(info.challenge_organization)#</td></tr>
		    <tr><td height=10></td></tr>

		    <tr><td class="feed_option"><b>TODAYS CHALLENGES</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_challenges# is "">Not provided<cfelse>#info.challenge_challenges#</cfif></td></tr>

		    <tr><td class="feed_option"><b>USE CASE SCENARIO</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_use_case# is "">Not provided<cfelse>#info.challenge_use_case#</cfif></td></tr>

		    <tr><td class="feed_option"><b>FUTURE STATE SOLUTION</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_future_state# is "">Not provided<cfelse>#info.challenge_future_state#</cfif></td></tr>

			<tr><td colspan=2><hr></td></tr>

		    <tr><td class="feed_option"><b>KEYWORDS</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_keywords# is "">Not provided<cfelse>#info.challenge_keywords#</cfif></td></tr>

		    <tr><td class="feed_option"><b>INSTRUCTIONS TO PARTICIPATE</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_instructions# is "">Not provided<cfelse>#info.challenge_instructions#</cfif></td></tr>

		    <tr><td class="feed_option"><b>WINNERS REWARD</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_reward# is "">Not provided<cfelse>#info.challenge_reward#</cfif></td></tr>

		    <tr><td class="feed_option"><b>POINT OF CONTACT</b></td></tr>
		    <tr><td class="feed_option">
		    <cfif #info.challenge_poc_name# is not "">#info.challenge_poc_name#</cfif>
		    <cfif #info.challenge_poc_title# is not ""><br>#info.challenge_poc_title#</cfif>
		    <cfif #info.challenge_poc_phone# is not ""><br>#info.challenge_poc_phone#</cfif>
		    <cfif #info.challenge_poc_email# is not ""><br>#info.challenge_poc_email#</cfif>

		    </td></tr>

           </table>

         </cfoutput>

         </div>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=20></td></tr>
         <tr><td>
              &nbsp;<input type="submit" name="button" value="Approve & Select Recipients" class="button_blue_large">
              </td></tr>

        </table>

       </td></tr>

       </form>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>