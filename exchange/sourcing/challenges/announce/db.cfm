<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save Information" or #button# is "Save & Approve">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_image from challenge
		  where challenge_id = #session.challenge_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.challenge_attachment#">

	</cfif>

	<cfif #challenge_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_image from challenge
		  where challenge_id = #session.challenge_id#
		</cfquery>

		<cfif #getfile.challenge_image# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.challenge_image#">
		</cfif>

		<cffile action = "upload"
		 fileField = "challenge_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cftransaction>

		<cfquery name="update_challenge" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 update challenge
		 set challenge_name = '#challenge_name#',

			  <cfif #challenge_image# is not "">
			   challenge_image = '#cffile.serverfile#',
			  </cfif>
			  <cfif isdefined("remove_attachment")>
			   challenge_image = null,
			  </cfif>

			 challenge_desc = '#challenge_desc#',
			 challenge_instructions = '#challenge_instructions#',
			 challenge_reward = '#challenge_reward#',
			 challenge_keywords = '#challenge_keywords#',
			 challenge_start = <cfif #challenge_start# is "">null<cfelse>'#challenge_start#'</cfif>,
			 challenge_end = <cfif challenge_end is "">null<cfelse>'#challenge_end#'</cfif>,
			 challenge_updated = #now()#,
			 challenge_poc_name = '#challenge_poc_name#',
			 challenge_poc_title = '#challenge_poc_title#',
			 challenge_poc_phone = '#challenge_poc_phone#',
			 challenge_poc_email = '#challenge_poc_email#',
			 challenge_url = '#challenge_url#'
		 where challenge_id = #session.challenge_id#
		</cfquery>

	</cftransaction>

	<cfif button is "Save Information">
    	<cflocation URL="step_1.cfm?u=1" addtoken="no">
    <cfelse>
    	<cflocation URL="step_2.cfm?u=1" addtoken="no">
    </cfif>

<cfelseif button is "Save Announcement">

  <cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update challenge
   set challenge_subject = '#challenge_subject#',
       challenge_email = '#challenge_email#',
       challenge_message = '#challenge_message#',
       challenge_from = '#challenge_from#'
   where challenge_id = #session.challenge_id#
  </cfquery>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 delete challenge_email
	 where challenge_email_challenge_id = #session.challenge_id#
	</cfquery>

	<cfif listlen(challenge_email) GT 0>

	 <cfloop index="e" list="#challenge_email#">

	   <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		insert into challenge_email
		(challenge_email_challenge_id, challenge_email_email_address)
		values
		(#session.challenge_id#,'#trim(e)#')
	   </cfquery>

	 </cfloop>

	</cfif>

  <cflocation URL="step_4.cfm?u=1" addtoken="no">

<cfelseif button is "Save & Launch Challenge">

  <cftransaction>

	  <cfquery name="challenge_id" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   select challenge_code, challenge_url from challenge
	   where challenge_id = #session.challenge_id#
	  </cfquery>

	  <cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   update challenge
	   set challenge_subject = '#challenge_subject#',
		   challenge_email = '#challenge_email#',
		   challenge_status = 1,
		   challenge_issued = #now()#,
		   challenge_message = '#challenge_message#',
		   challenge_from = '#challenge_from#'
	   where challenge_id = #session.challenge_id#
	  </cfquery>

	  <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   delete challenge_email
	   where challenge_email_challenge_id = #session.challenge_id#
	  </cfquery>

	  <cfif listlen(challenge_email) GT 0>

	  <cfloop index="e" list="#challenge_email#">

	   <cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		insert into challenge_email
		(challenge_email_challenge_id, challenge_email_email_address)
		values
		(#session.challenge_id#,'#trim(e)#')
	   </cfquery>

	 </cfloop>

	</cfif>

  </cftransaction>

      <!--- Email Recipients - Start --->

      <cfif listlen(challenge_email) GT 0>

        <cfloop index="e" list="#challenge_email#">

			<cfmail from="EXCHANGE CHALLENGE <noreply@ratio.exchange>"
					  to="#e#"
			  username="noreply@ratio.exchange"
			  password="Gofus107!"
				  port="25"
				useSSL="false"
				type="html"
				server="mail.ratio.exchange"
			   subject="#challenge_subject#">
			<html>
			<head>
			<title><cfoutput>#session.network_name#</cfoutput></title>
			</head><div class="center">
			<body class="body">

			<cfoutput>

				<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="ffffff">
		         <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>From: </b> #challenge_from#</td></tr>
		         <tr><td>&nbsp;</td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">#replace(challenge_message,"#chr(10)#","<br>","all")#</td></tr>
				 <tr><td><hr></td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Challenge Information</b></td></tr>
                 <tr><td>&nbsp;</td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>Challenge ID:</b> #challenge_id.challenge_code#</td></tr>
                 <tr><td>&nbsp;</td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">To view more information about this Challenge, please <a href="https://go.ratio.exchange/signin/index.cfm?challenge_code=#challenge_id.challenge_code#"><b><u>login to the Exchange</u></b></a> where you will be directly routed to information about this Challenge.</b></td></tr>
                 <tr><td>&nbsp;</td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">Or, please paste the below URL into your brower.</td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><a href="https://go.ratio.exchange/signin/index.cfm?challenge_code=#challenge_id.challenge_code#"><b><u>https://go.ratio.exchange/signin/index.cfm?challenge_code=#challenge_id.challenge_code#</u></b></a>.</td></tr>
                 <tr><td>&nbsp;</td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;"><b>About the Exchange</b></td></tr>
				 <tr><td style="font-family; color: 000000; calibri, arial; font-size: 14px;">The Exchange is the worlds most intelligent government marketplace focused on accelerating revenue and growth for companies with innovative products and services.</td></tr>
				 <tr><td>&nbsp;</td></tr>
				</table>

				<table cellspacing=0 cellpadding=0 border=0 width=100% bgcolor="000000">
				 <tr><td height=75 valign=middle><a href="https://www.ratio.exchange"><img src="http://go.ratio.exchange/images/exchange_logo.png" width=160 hspace=10 border=0></a></td></tr>
				</table>

			</cfoutput>

			</body>
			</html>

			</cfmail>

		</cfloop>

      </cfif>

      <!--- Email Recipients - Start --->

  <cflocation URL="/exchange/sourcing/challenges/manage/" addtoken="no">

<cfelseif button is "Approve & Select Recipients">

		<cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update challenge
		  set challenge_approved = 1,
		      challenge_approved_date = #now()#,
		      challenge_approved_by = #session.usr_id#
		  where challenge_id = #session.challenge_id#
		</cfquery>

		<cflocation URL="step_3.cfm?u=1" addtoken="no">

</cfif>
