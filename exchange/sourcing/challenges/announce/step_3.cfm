<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<cfquery name="challenge" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/challenges/challenge_activity.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/">DEMAND PORTFOLIO</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_risk.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/risks/">RISKS</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_config.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/">NEEDS</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/challenges/">CHALLENGES</a></span>
          </div>

       <div class="main_box_2">

       <cfinclude template="/exchange/sourcing/challenges/challenge_header.cfm">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><a href="index.cfm">LAUNCH CHALLENGE:  STEP 3 - SELECT RECIPIENTS</td>
			 <td class="feed_sub_header" align=right><img src="/images/icon_return2.png" width=20 border=0 hspace=10 align=absmiddle><a href="index.cfm">Return to Process</a></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>This step allows you select or add the recipients that will receive this Challenge via email.</td></tr>
         <tr><td colspan=2><hr></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select company_duns, company_poc_phone, company_poc_email, company_poc_first_name, company_poc_last_name, company_poc_title, company_id, need_comp_contender, need_comp_id, company_id, company_logo, company_name, company_website, company_keywords, company_city, company_state, need_comp_added, company_meta_keywords,
		 (select ROUND(AVG(CAST(company_comments_rating AS FLOAT)), 2) as total from company_comments where company_comments_company_id = company.company_id) as rating from need_comp
		 left join company on company_id = need_comp_selected_company_id
		 where need_comp_challenge_id = #session.challenge_id# and
			   need_comp_contender = 1
		 order by company_name
		</cfquery>

         <tr><td height=10></td></tr>
         <tr><td class="feed_header" colspan=2>COMPANIES SELECTED</td></tr>

         <form action="set_email.cfm" method="post">

         <cfif info.recordcount is 0>
         	<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=8>You have not selected / sourced any Companies.  Please <a href="/exchange/sourcing/challenges/sourcing">click here</a> to find and select Companies for this Challenge.  Or, click <b>Continue</b> to enter email addresses.</td></tr>


            <tr><td height=10></td></tr>
            <tr><td>
            <input type="submit" name="button" value="Continue" class="button_blue_large">
            </td></tr>


         <cfelse>
         	<tr><td class="feed_sub_header" style="font-weight: normal;" colspan=8>You have selected the following companies to issue this Challenge to.  Please choose what email address you'd like to send this to.  You will have the opportunity to add / edit email addresses when you email users.</td></tr>

         <tr><td height=10></td></tr>

		 <tr>
			<td class="feed_sub_header" align=center>Company</td>
			<td></td>
			<td class="feed_sub_header">Points of Contact</td>
		 </tr>

		 <tr><td height=10></td></tr>

         <tr><td>

          <cfset row_counter = 1>

          <cfloop query="info">

                <cfoutput>
                <tr>

					<td width=225 class="feed_sub_header" align=center>

	                <table cellspacing=0 cellpadding=0 border=0 width=100%>

	                <tr><td align=center>

						<cfif info.company_logo is "">
						  <a href="/exchange/include/company_profile.cfm?id=#info.company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="//logo.clearbit.com/#info.company_website#" width=80 border=0 onerror="this.src='/images/no_logo.png'"></a>
						<cfelse>
						  <a href="/exchange/include/company_profile.cfm?id=#info.company_id#" target="_blank" rel="noopener" rel="noreferrer"><img src="#media_virtual#/#info.company_logo#" width=80 border=0></a>
						</cfif>

						</td></tr>

				    <tr><td height=10></td></tr>
				    <tr><td class="feed_sub_header" align=center><a href="/exchange/include/company_profile.cfm?id=#info.company_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(info.company_name)#</b></a></td></tr>
				    <tr><td height=10></td></tr>
				    <tr><td align=center>

                    <cfif #info.rating# is "">
                     <cfset #rate# = 0>
                    <cfelse>
                     <cfset #rate# = #round(info.rating)#>
                    </cfif>

		            <cfquery name="comment_rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		             select comments_rating_name from comments_rating
		             where comments_rating_value = #rate#
		            </cfquery>

					<cfif info.rating is 0 or info.rating is "">
					 <img src="/images/star_0.png" height=16 alt="Not Rated" title="Not Rated">
					<cfelse>
					 <img src="/images/star_#round(info.rating)#.png" height=16 alt="#comment_rating.comments_rating_name#" title="#comment_rating.comments_rating_name#">
					</cfif>

					</td></tr>

					</table>

           </cfoutput>

		   </td><td width=50>&nbsp;</td><td valign=top>

		   <cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 select * from sams
			 where duns = '#info.company_duns#'
		   </cfquery>

		   <table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr><td height=10></td></tr>
			<tr>
				<td class="feed_sub_header"></td>
				<td class="feed_option"><b>NAME</b></td>
				<td class="feed_option"><b>TITLE / ROLE</b></td>
				<td class="feed_option"><b>PHONE</b></td>
				<td class="feed_option"><b>EMAIL</b></td>
				<td></td>
			</tr>

			<cfoutput>

			<cfset poc = 0>

			<cfif info.company_poc_email is not "">
				<tr height=35>
				   <td class="feed_option" width=40><input type="checkbox" name="email" style="width: 18px; height: 18px;" value="#info.company_poc_email#" <cfif listfind(challenge.challenge_email_list_1,info.company_poc_email)>checked</cfif>></td>
				   <td class="feed_option" width=200 style="font-weight: normal;"><cfif info.company_poc_first_name is "" and info.company_poc_last_name is "">Unknown<cfelse>#info.company_poc_first_name# #info.company_poc_last_name#</cfif></td>
				   <td class="feed_option" width=200 style="font-weight: normal;"><cfif #info.company_poc_title# is "">Unknown<cfelse>#info.company_poc_title#</cfif></td>
				   <td class="feed_option" width=200 style="font-weight: normal;"><cfif #info.company_poc_phone# is "">Unknown<cfelse>#info.company_poc_phone#</cfif></td>
				   <td class="feed_option" style="font-weight: normal;">#info.company_poc_email#</td>
				   <td width=200>&nbsp;</td>
				</tr>
				<cfset poc = 1>
			</cfif>

			<cfif sams.poc_fnme is not "">
				<tr height=35>
				   <td class="feed_option" width=40><input type="checkbox" name="email" style="width: 18px; height: 18px;" value="#sams.poc_email#" <cfif listfind(challenge.challenge_email_list_1,sams.poc_email)>checked</cfif>></td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.poc_fnme# #sams.poc_lname#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.poc_title#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.poc_us_phone#</td>
				   <td class="feed_option" style="font-weight: normal;">#sams.poc_email#</td>
				   <td width=200>&nbsp;</td>
				</tr>
				<cfset poc = 1>
			</cfif>

			<cfif sams.pp_poc_fname is not "">
				<tr height=35>
				   <td class="feed_option" width=40><input type="checkbox" name="email" style="width: 18px; height: 18px;" value="#sams.pp_poc_email#" <cfif listfind(challenge.challenge_email_list_1,sams.pp_poc_email)>checked</cfif>></td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.pp_poc_fname# #sams.pp_poc_lname#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.pp_poc_title#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.pp_poc_phone#</td>
				   <td class="feed_option" style="font-weight: normal;">#sams.pp_poc_email#</td>
				   <td width=200>&nbsp;</td>
				</tr>
				<cfset poc = 1>
			</cfif>

			<cfif sams.alt_poc_fname is not "">
				<tr height=35>
				   <td class="feed_option" width=40><input type="checkbox" name="email" style="width: 18px; height: 18px;" value="#sams.alt_poc_email#" <cfif listfind(challenge.challenge_email_list_1,sams.alt_poc_email)>checked</cfif>></td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.alt_poc_fname# #sams.alt_poc_lname#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.alt_poc_title#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.akt_poc_phone#</td>
				   <td class="feed_option" style="font-weight: normal;">#sams.alt_poc_email#</td>
				   <td width=200>&nbsp;</td>
				</tr>
				<cfset poc = 1>
			</cfif>

			<cfif sams.elec_bus_poc_fname is not "">
				<tr height=35>
				   <td class="feed_option" width=40><input type="checkbox" name="email" style="width: 18px; height: 18px;" value="#sams.elec_bus_poc_email#" <cfif listfind(challenge.challenge_email_list_1,sams.elec_bus_poc_email)>checked</cfif>></td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.elec_bus_poc_title#</td>
				   <td class="feed_option" width=200 style="font-weight: normal;">#sams.elec_bus_poc_us_phone#</td>
				   <td class="feed_option" style="font-weight: normal;">#sams.elec_bus_poc_email#</td>
				   <td width=200>&nbsp;</td>
				</tr>
				<cfset poc = 1>
			</cfif>

			<cfif poc is 0>
			 <tr><td colspan=2 class="feed_option" style="font-weight: normal;">No point of contact information could be found.</td></tr>
			</cfif>

            </cfoutput>

		   </table>

		   </td></tr>

        <tr><td height=10></td></tr>
        <tr><td colspan=3><hr></td></tr>
        <tr><td height=10></td></tr>

		</cfloop>

            <tr><td height=10></td></tr>
            <tr><td>
            <input type="submit" name="button" value="Save & Continue" class="button_blue_large">
            </td></tr>

		</cfif>

        </form>

	  </table>

       </td></tr>

       </table>

       </td></tr>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>