<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_created_by
 where need_id = #session.need_id#
</cfquery>

<cfquery name="phase" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_phase
 order by challenge_phase_order
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header">ADD CHALLENGE</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>

	   </table>

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_sub_header">Challenge Name / Title</td>
		     <td><input type="text" name="challenge_name" class="input_text" size=70></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Description</td>
		     <td><textarea name="challenge_desc" class="input_textarea" style="width: 900px; height: 200px;"></textarea></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Instructions<br>to Participate</td>
		     <td><textarea name="challenge_instructions" class="input_textarea" style="width: 900px; height: 100px;"></textarea></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Winners Reward</td>
		     <td><textarea name="challenge_reward" class="input_textarea" style="width: 900px; height: 100px;"></textarea></td></tr>

		 <tr><td class="feed_sub_header">Information URL</td>
		     <td><input type="text" name="challenge_url" class="input_text" size=120 maxlength="1000"></td></tr>

         <tr><td class="feed_sub_header" valign=top>Attachment</td>
             <td class="feed_sub_header" style="font-weight: normal;"><input type="file" name="challenge_attachment"></td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">End Date</td>
		     <td><input type="date" name="challenge_end" class="input_date" style="width: 165px;"></td></tr>

		 <tr><td class="feed_sub_header">Phase</td>
		     <td>
		     <select name="challenge_phase_id" class="input_select" style="width: 165px;">
		     <cfoutput query="phase">
		      <option value=#challenge_phase_id#>#challenge_phase_name#
		     </cfoutput>
		     </select>
		     </td></tr>

		 <tr><td class="feed_sub_header">Anonymous</td>
		     <td>
		     <select name="challenge_annoymous" class="input_select" style="width: 165px;">
		      <option value=0>No
		      <option value=1>Yes
		     </select>

		     </td></tr>

		 <tr><td class="feed_sub_header">Discoverable</td>
		     <td>
		     <select name="challenge_public" class="input_select" style="width: 250px;">
		      <option value=0>No, keep private
		      <option value=1>Yes, allow others to see
		     </select>
		     </td></tr>

		 <tr><td class="feed_sub_header">Open</td>
		     <td>
		     <select name="challenge_open" class="input_select" style="width: 250px;">
		      <option value=0>No, only to people I send it to
		      <option value=1>Yes, allow others to respond
		     </select>
		     </td></tr>

         <tr><td colspan=2><hr></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Name</td>
		     <td><input type="text" name="challenge_poc_name" class="input_text" style="width: 300px;"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Title</td>
		     <td><input type="text" name="challenge_poc_title" class="input_text" style="width: 300px;"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Phone</td>
		     <td><input type="text" name="challenge_poc_phone" class="input_text" style="width: 300px;"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Email</td>
		     <td><input type="email" name="challenge_poc_email" class="input_text" style="width: 300px;"></td></tr>

		 <tr><td colspan=2><hr></td></tr>
		 <tr><td></td>
		     <td><input type="submit" name="button" value="Create Challenge" class="button_blue_large"></td></tr>

         </form>


        </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>