<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="who" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select count(follow_id) as total from follow
 where follow_challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="follow" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select follow_id from follow
 where follow_challenge_id = #session.challenge_id# and
       follow_by_usr_id = #session.usr_id#
</cfquery>

<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       <div class="main_box">

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">#ucase(info.challenge_name)#</td>
            <td class="feed_sub_header" align=right><img src="/images/delete.png" width=20 alt="Close" title="Close" style="cursor: pointer;" onclick="windowClose();"></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td height=10></td></tr>

       <tr><td valign=middle width=125>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif info.challenge_image is "">
				  <img src="/images/stock_challenge.png" width=100>
				<cfelse>
				  <img src="#media_virtual#/#info.challenge_image#" width=100>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                <td class="feed_sub_header">UNIQUE ID</td>
                <td class="feed_sub_header">ORGANIZATION</td>
                <td class="feed_sub_header" align=center>FOLLOWERS</td>
                <td class="feed_sub_header" align=center>STATUS</td>
                <td class="feed_sub_header" align=right>START DATE</td>
                <td class="feed_sub_header" align=right>END DATE</td>
             </tr>

             <tr>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" width=150>#info.challenge_code#</td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #info.challenge_organization# is "">Not Identified<cfelse>#info.challenge_organization#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=100>#who.total#</td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=120>

				  <cfif info.challenge_status is 0>
				   Not Launched
				  <cfelseif info.challenge_status is 1>
				   Launched
				  <cfelse>
				   Closed
				  </cfif>

			    </td>

                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=120><cfif info.challenge_start is "">TBD<cfelse>#dateformat(info.challenge_start,'mm/dd/yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=120><cfif info.challenge_end is "">TBD<cfelse>#dateformat(info.challenge_end,'mm/dd/yyyy')#</cfif></td>
             </tr>

          </table>

         </td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=10><hr></td></tr>


       </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_sub_header">DESCRIPTION</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">TODAY'S CHALLANGES</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_desc# is "">Not Provided<cfelse>#info.challenge_desc#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_challenges# is "">Not Provided<cfelse>#info.challenge_challenges#</cfif></td>
             </tr>

             <tr><td class="feed_sub_header">USE CASE SCENARIO</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">FUTURE STATE SOLUTION</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_use_case# is "">Not Provided<cfelse>#info.challenge_use_case#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_future_state# is "">Not Provided<cfelse>#info.challenge_future_state#</cfif></td>
             </tr>

             <tr>
                 <td class="feed_sub_header">KEYWORDS</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">REFERENCE URL</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_keywords# is "">Not Provided<cfelse>#info.challenge_keywords#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_url# is "">Not Provided<cfelse><a href="#info.challenge_url#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#info.challenge_url#</a></cfif></td>
             </tr>

             <tr><td height=10></td></tr>
             <tr><td colspan=3><hr></td></tr>
             <tr><td height=10></td></tr>

             <tr><td class="feed_sub_header">REWARD</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">INSTRUCTIONS</td>
             </tr>


             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_reward# is "">Not Provided<cfelse>#info.challenge_reward#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.challenge_instructions# is "">Not Provided<cfelse>#info.challenge_instructions#</cfif></td>
             </tr>

           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>

             <tr>
                 <td class="feed_sub_header" width=200>POINT OF CONTACT</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.challenge_poc_name# is "">Not Provided<cfelse>#info.challenge_poc_name#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>TITLE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.challenge_poc_email# is "">Not Provided<cfelse>#info.challenge_poc_title#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>EMAIL</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.challenge_poc_email# is "">Not Provided<cfelse>#info.challenge_poc_email#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>PHONE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.challenge_poc_phone# is "">Not Provided<cfelse>#info.challenge_poc_phone#</cfif></td></tr>
             </tr>

           </table>

			<cfquery name="attachments" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 select * from attachment
			 join usr on usr_id = attachment_created_by
			 where attachment_challenge_id = #session.challenge_id#
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">ATTACHMENTS</td>
                 <td class="feed_sub_header" align=right></td></tr>
           </table>

       </cfoutput>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <cfif attachments.recordcount is 0>
              <tr><td class="feed_sub_header" style="font-weight: normal;">No attachments have been added.</td></tr>
             <cfelse>

              <tr>
                  <td class="feed_sub_header">NAME</td>
                  <td class="feed_sub_header">DESCRIPTION</td>
                  <td class="feed_sub_header">FILE</td>
                  <td class="feed_sub_header" align=center>ADDED</td>
                  <td class="feed_sub_header" align=right>BY</td>
              </tr>

              <cfoutput query="attachments">

               <tr>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top width=200><b>#attachment_name#</b></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_desc# is "">Not Provided<cfelse>#attachment_desc#</cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #attachment_file# is "">No attachment<cfelse><a href="#media_virtual#/#attachment_file#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#attachment_file#</a></cfif></td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=center width=120>#dateformat(attachment_updated,'mm/dd/yyyy')#</td>
                  <td class="feed_sub_header" style="font-weight: normal;" valign=top align=right width=100>#usr_first_name# #usr_last_name#</td>
               </tr>

              </cfoutput>

             </cfif>

			<cfquery name="updates" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select * from up
			 left join usr on usr_id = up_usr_id
			 where up_challenge_id = #session.challenge_id# and
			 up_post = 1
			 order by up_date DESC
			</cfquery>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>
             <tr>
                 <td class="feed_sub_header">CHALLENGE UPDATES</td>
                 <td class="feed_sub_header" align=right></td></tr>
           </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif updates.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No updates have been recorded.</td></tr>
	    <cfelse>

	   <cfloop query="updates">

	   <cfoutput>

		 <tr bgcolor="FFFFFF" height=35>

			<cfif #up_usr_id# is #session.usr_id#>
			 <td class="feed_sub_header" valign=top width=300><b>#ucase(up_name)#</b></td>
			<cfelse>
			 <td class="feed_sub_header" valign=top width=300 style="font-weight: normal;">#ucase(up_name)#</td>
			</cfif>
			<td class="feed_sub_header" valign=top align=right width=75>
			<cfif up_post is 0>
			 NOT POSTED
			<cfelse>
			 POSTED
			</cfif>
			</td>

		 </tr>

		 <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;"><cfif #up_desc# is "">No update provided<cfelse>#up_desc#</cfif></td></tr>

		   <tr><td class="link_small_gray">
			   <cfif #up_attachment# is "">
				No Attachment Provided&nbsp;|&nbsp;
			   <cfelse>
				<a href="#media_virtual#/#up_attachment#" target="_blank" rel="noopener" rel="noreferrer">Download Attachment</a>&nbsp;|&nbsp;

			   </cfif>

			   <cfif #up_url# is "">
				No Reference URL Posted
			   <cfelse>
				URL: <a href="#up_url#" target="_blank" rel="noopener" rel="noreferrer">#up_url#</a>
			   </cfif>

			   </td>
			   <td align=right class="link_small_gray">#usr_first_name# #usr_last_name# | #dateformat(up_date,'mm/dd/yyyy')# at #timeformat(up_date)#</td>
		   </tr>

           </cfoutput>

         </cfloop>

	    </cfif>
	   </table>

		<cfquery name="questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 select * from question
		 where question_public = 1 and
		 question_challenge_id = #session.challenge_id#
         order by question_date_submitted DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td colspan=2><hr></td></tr>
		 <tr>
			 <td class="feed_sub_header">QUESTIONS & ANSWERS</td>
			 <td class="feed_sub_header" align=right></td></tr>
	   </table>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <cfif questions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been submitted or posted for this Challenge.</td></tr>
         <cfelse>

         <cfoutput query="questions">

          <tr><td class="feed_sub_header" style="font-weight: normal;" valign=top><b>Q.  #ucase(questions.question_subject)#</b><br>#questions.question_text#</td>
              <td class="feed_sub_header" valign=top align=right>#dateformat(questions.question_updated,'mm/dd/yyyy')#, #timeformat(questions.question_updated)#</td></tr>
          <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2><b>A.  </b>#questions.question_answer#</td></tr></tr>
          <tr><td colspan=2><hr></td></tr>

         </cfoutput>

         </cfif>

           </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>