<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from challenge
 where challenge_created_by_id = #session.usr_id#

 <cfif isdefined("session.hub")>
  and challenge_hub_id = #session.hub#
 <cfelse>
  and challenge_hub_id is null
 </cfif>

</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

       <cfinclude template="/exchange/sourcing/menu.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/">DEMAND PORTFOLIO</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_risk.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/risks/">RISKS</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/">NEEDS</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/challenges/">CHALLENGES</a></span>
          </div>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>

			<td class="feed_header">CHALLENGES</td>
			<td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 width=15 hspace=10 align=absmiddle><a href="/exchange/sourcing/challenges/add.cfm">Create Challenge</a></td>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Challenges allow you to engage the Market (i.e., companies, organizations, etc.) to help you solve your problem or issue.</td></tr>
	    </tr>

	    <tr><td colspan=2><hr></td></tr>

        <cfif isdefined("u")>
         <cfif u is 3>
          <tr><td class="feed_sub_header" style="color: green;">Challenge has been successfully deleted.</td></tr>
         </cfif>
        </cfif>

       </table>

        <cfif challenges.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Challenges have been created.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfloop query="challenges">

			   <cfoutput>

			   <tr><td valign=top width=125>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr>
						<td align=center>

						<cfif challenges.challenge_image is "">
						  <a href="/exchange/sourcing/challenges/set.cfm?challenge_id=#challenge_id#"><img src="/images/stock_challenge.png" vspace=10 width=100 border=0></a>
						<cfelse>
						  <a href="/exchange/sourcing/challenges/set.cfm?challenge_id=#challenge_id#"><img src="#media_virtual#/#challenges.challenge_image#" vspace=10 width=100 border=0></a>
						</cfif>

					   </td>

					</tr>

					<tr><td class="feed_sub_header" align=center>

					  <cfif challenge_status is 0>
					   Not Launched
					  <cfelseif challenge_status is 1>
					   Launched
					  <cfelse>
					   Closed
				      </cfif>
				  </td></tr>

			    </table>

			   </td><td valign=top><td width=30>&nbsp;</td><td valign=top>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" valign=middle><a href="/exchange/sourcing/challenges/set.cfm?challenge_id=#challenge_id#"><b>#ucase(challenges.challenge_name)#</b></a></td>
					   <td align=right></td></tr>
					<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=10>#challenges.challenge_desc#</td></tr>
					<tr><td class="link_small_gray"><b>KEYWORDS</b>&nbsp;:&nbsp;#ucase(challenges.challenge_keywords)#</td>
						<td class="link_small_gray" align=right><b>LAST UPDATED</b>&nbsp;:&nbsp;#dateformat(challenges.challenge_updated,'mm/dd/yyyy')#</td></tr>
				   </table>

               </td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=4><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>