<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 where challenge_id = #session.challenge_id#
</cfquery>

<cfquery name="email" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge_email
 where challenge_email_challenge_id = #session.challenge_id#
</cfquery>

<cfif email.recordcount is 0>
 <cfset email_list = "">
<cfelse>
 <cfset email_list = valuelist(email.challenge_email_email_address)>
</cfif>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/challenges/challenge_activity.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/">DEMAND PORTFOLIO</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_risk.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/risks/">RISKS</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_config.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/">FUTURE NEEDS</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_challenge.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/challenges/">MARKET CHALLENGES</a></span>
          </div>

       <div class="main_box_2">

       <cfinclude template="/exchange/sourcing/challenges/challenge_header.cfm">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_header"><a href="index.cfm">MANAGE CHALLENGE</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return</a>
			 </td></tr>
         <tr><td colspan=2><hr></td></tr>
	   </table>

       <form action="email_db.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif info.recordcount is 0>

	    <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created a Challenge.</td></tr>

	   <cfelse>

         <cfif isdefined("u")>
          <tr><td class="feed_sub_header" style="color: green;">Announcement has been successfully saved.</td></tr>
         </cfif>

         <cfoutput>
         <tr><td class="feed_sub_header">From:</td></tr>
         <tr><td><input type="text" name="challenge_from" class="input_text" style="width: 900px;"></tr>

         <tr><td class="feed_sub_header">To (Challenge Recipients):

       	 	 <div class="tooltip" style="align: top;"><img src="/images/icon_help.png" width=18 hspace=5 align=absmiddle>
		 	  <span class="tooltiptext">Email addresses need to be seperated by comma's with no spaces.  If you would like to include additional people in the email message please feel free to add them here.</span>
			 </div>

         </td></tr>

       	 <tr><td><textarea name="challenge_email" class="input_textarea" style="width: 900px; height: 125px;">#email_list#</textarea>
         <tr><td class="feed_sub_header">Subject:</td></tr>
         <tr><td><input type="text" name="challenge_subject" class="input_text" style="width: 900px;"></tr>

         <tr><td class="feed_sub_header">Message:</td></tr>
         <tr><td><textarea name="challenge_message" class="input_textarea" style="width: 900px; height: 200px;"></textarea></td></tr>

          </cfoutput>

         <tr><td height=10></td></tr>
         <tr><td class="link_small_gray"><b>Note: </b> emails will be sent seperately and will not include all email addresses in the "To" line.</td></tr>
         <tr><td height=10></td></tr>
         <tr><td><hr></td></tr>
         <tr><td height=10></td></tr>

         <tr><tr><td><input type="submit" name="button" value="Send Email" class="button_blue_large"></td></tr>

	  </table>

       </td></tr>

       </cfif>

       </table>

       </td></tr>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>