<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=middle>FEDERAL GRANTS</td><td class="feed_option" align=right><a href="source_grants.cfm" valign=middle><img src="/images/delete.png" width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <cfquery name="vendor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select top(1) recipient_name from grants
	       where contains((cfda_title, award_description),'"#session.source_keywords#"')
	       and recipient_duns = '#duns#'
		  </cfquery>

		  <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select awarding_agency_name, awarding_sub_agency_name, cfda_title, award_description, action_date, federal_action_obligation from grants
	       where contains((cfda_title, award_description),'"#session.source_keywords#"')
	       and recipient_duns = '#duns#'
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_sub_header"><cfoutput><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer">#ucase(vendor.recipient_name)#</a></cfoutput></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr height=50>
               <td class="feed_option"><b>Award Date</b></td>
               <td class="feed_option"><b>Agency</b></td>
               <td class="feed_option"><b>Award Title & Description</b></td>
               <td class="feed_option" align=right><b>Award Value</b></td>
            </tr>

           <cfset counter = 0>
           <cfoutput query="agencies">
            <cfif counter is 0>
             <tr bgcolor="ffffff" height=30>
            <cfelse>
             <tr bgcolor="e0e0e0" height=30>
            </cfif>
               <td class="text_xsmall" valign=top width=75>#dateformat(action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=250>#awarding_agency_name#<br>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>
               #ucase(replaceNoCase(agencies.cfda_title,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all"))#
               <br>
               #ucase(replaceNoCase(agencies.award_description,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all"))#
               </td>
               <td class="text_xsmall" valign=top width=100 align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
            </tr>
            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>
          </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

