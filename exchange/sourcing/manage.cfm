<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfset fb_date = #dateformat(now(),'mm/dd/yyyy')#>

<!--- Check Access --->

<cfquery name="check" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select req_id, req_keywords from req
 where req_created_by = #session.usr_id# and
       req_id = #session.req_id#
</cfquery>

<cfif check.recordcount is 0>
 <cflocation URL="/exchange/sourcing/needs/" addtoken="no">
</cfif>

<!--- Get Data --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from req
 left join req_type on req_type.req_type_id = req.req_type_id
 left join req_status on req_status.req_status_id = req.req_status_id
 left join req_stage on req_stage.req_stage_id = req.req_stage_id
 left join usr on usr_id = req_created_by
 where req_id = #session.req_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/needs/filter.cfm">

       </td><td valign=top>

       <div class="main_box">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr>
			<td class="feed_header">NEED SUMMARY</td>
			<td class="feed_sub_header" align=right><a href="/exchange/sourcing/">Return</a></td>
		 </tr>
		 <tr><td colspan=2><hr></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <cfoutput>

			   <tr><td valign=middle width=150>


              <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
			        <td>

			        <cfif info.req_image is "">
			          <a href="/exchange/sourcing/needs/set.cfm?req_id=#info.req_id#"><img src="/images/app_innovation.png" width=125 border=0></a>
			        <cfelse>
			          <a href="/exchange/sourcing/needs/set.cfm?req_id=#info.req_id#"><img src="#media_virtual#/#info.req_image#" width=125 border=0></a>
			        </cfif>

			       </td></tr>
			    </table>

			    </td><td valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr><td class="feed_sub_header" valign=middle><cfif info.req_number is not "">#info.req_number# - </cfif>#ucase(info.req_name)#</td>
                    <td align=right class="feed_sub_header"><a href="/exchange/sourcing/needs/edit.cfm"><img src="/images/icon_edit.png" width=20 border=0 hspace=10 alt="Update" title="Update" align=absmiddle></a><a href="/exchange/sourcing/needs/edit.cfm">Edit Profile</a>
                   </td></tr>

				<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=10>#info.req_desc#</td></tr>
		        <tr><td class="link_small_gray">Keywords - #info.req_keywords#</td>
		            <td align=right>



					<cfif #info.req_stage_id# is "" or #info.req_stage_id# is 0>
					<span class="feed_option"><b>Not Started</b>&nbsp;&nbsp;</span><a href="/exchange/sourcing/needs/set.cfm?req_id=#info.req_id#"><img src="/images/req_stage_0.png" height=20 width=100 alt="Not Selected" title="Not Selected" border=0 align=absmiddle></a>

					<cfelse>
					<span class="feed_option"><b>#info.req_stage_name#</b>&nbsp;&nbsp;</span><a href="/exchange/sourcing/needs/set.cfm?req_id=#info.req_id#"><img src="/images/req_stage_#info.req_stage_order#.png" alt="#info.req_stage_name#" title="#info.req_stage_name#" height=20 width=100 align=absmiddle border=0></a>
					</cfif>


		            </td></tr>

               </table>

               </td><td valign=middle>

				</cfoutput>

		   <tr><td height=10></td></tr>
		   <tr><td colspan=3><hr></td></tr>

         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=10></td></tr>
          <form action="filter.cfm" method="post">
          <tr><td class="feed_header"><b>THE MARKETPLACE</b></td>

               <td align=right>

               <select name="filter_operator" class="input_select" style="width: 75px;">
                <option value="and">and
                <option value="or">or
               </select>

               <input type="text" name="filter_keyword" class="input_text" style="width: 125px;" placeholder="keyword">

               <input type="submit" name="button" class="button_blue" value="Go">

               </td>


              </tr>

              </form>
          <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">The numbers below represent the number of unique companies and active opportunities that match each keyword listed in the requirement profile.</td></tr>
          <tr><td height=10></td></tr>
         </table>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td></td>
              <td class="feed_sub_header" align=center>COMPANY PROFILES</td>
              <td class="feed_sub_header" align=center>WON FED CONTRACTS</td>
              <td class="feed_sub_header" align=center>WON SBIR/STTRs</td>
              <td class="feed_sub_header" align=center>WON GRANTS</td>
              <td class="feed_sub_header" align=center>PATENT HOLDERS</td>
              <td class="feed_sub_header" align=center>ACTIVE OPPS</td>
              <td class="feed_sub_header" align=center>FILTER</td>
          </tr>

<!---
'"machine learning" and ("clinical" and "health" or ("Defense" and "Navy"))'
--->

          <cfloop index="i" list="#check.req_keywords#">

          <cfset filter_string = "machine learning">
          <cfset filter_string2 = "defense">

			<!--- Awards --->

			<cfquery name="awards" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select count(distinct(recipient_duns)) as total from award_data
			 where id > 0 and
			 (
			 contains((awarding_agency_name, awarding_sub_agency_name, award_description),'"#i#" and "#filter_string2#"')
			 )
			</cfquery>

			<!--- SBIRs --->

			<cfquery name="sbir" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select count(distinct(duns)) as total from sbir
			 where id > 0 and
			 (
			 contains((department, agency, award_title, abstract, research_keywords),'"#i#"')
			 )
			</cfquery>

			<!--- Grants --->

			<cfquery name="grants" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select count(distinct(recipient_duns)) as total from grants
			 where id > 0 and
			 (
			 contains((cfda_title, awarding_agency_name, awarding_sub_agency_name, award_description),'"#i#"')
			 )
			</cfquery>

			<!--- Patents --->

			<cfquery name="patents" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select count(patent_id) as total from patent
			 where patent_id is not null and
			 (
			 contains((title, abstract),'"#i#"')
			 )
			</cfquery>

			<!--- Companies --->

			<cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			select count(company_id) as total from company
			 where company_id > 0 and
			 (
			 contains((company_meta_keywords, company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#i#"')
			 )
			</cfquery>

			<cfquery name="fbo" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			 select count(distinct(fbo_solicitation_number)) as total from fbo
			 where
			 (
			  contains((fbo_opp_name, fbo_solicitation_number, fbo_synopsis, fbo_office, fbo_agency, fbo_naics, fbo_contract_award_contractor),'"#i#"')
			 )
			 and fbo_archive_date > '#fb_date#'
			 and (fbo_type not like 'Award%' or fbo_type not like '%cancel%')
			</cfquery>


          <cfoutput>

          <tr>
              <td class="feed_sub_header" width=10%>#ucase(i)#</td>
			  <td class="big_number" align=center style="background-color: AEDDAF;"><a href="open.cfm">#numberformat(companies.total,'99,999')#</td>
			  <td class="big_number" align=center style="background-color: B2C2EF;"><a href="open.cfm">#numberformat(awards.total,'99,999')#</td>
			  <td class="big_number" align=center style="background-color: AEDDAF;"><a href="open.cfm">#numberformat(sbir.total,'99,999')#</td>
			  <td class="big_number" align=center style="background-color: B2C2EF;"><a href="open.cfm">#numberformat(grants.total,'99,999')#</td>
			  <td class="big_number" align=center style="background-color: AEDDAF;"><a href="open.cfm">#numberformat(patents.total,'99,999')#</td>
			  <td class="big_number" align=center style="background-color: AEDDAF;"><a href="open.cfm">#numberformat(fbo.total,'99,999')#</td>
              <td align=center width=2%><a href="/exchange/sourcing/needs/filter.cfm?i=#i#"><img src="/images/icon_filter.png" width=30 align=absmiddle border=0 alt="Filter Results" title="Filter Results"></a></td>
          </tr>
          </cfoutput>

          </cfloop>

         </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>