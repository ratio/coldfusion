<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from req
 where req_id = #session.req_id#
</cfquery>

<cfquery name="types" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from req_type
 order by req_type_order
</cfquery>

<cfquery name="stage" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from req_stage
 order by req_stage_order
</cfquery>

<cfquery name="status" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from req_status
 order by req_status_order
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=bottom>UPDATE NEED</td>
            <td align=right><a href="/exchange/sourcing/needs/manage.cfm"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header" valign=top>Stage</td>
				 <td width=225>

                 <select name="req_stage_id" class="input_select" style="width: 175px;">
                 <option value=0>Select Stage
                 <cfoutput query="stage">
                  <option value=#req_stage_id# <cfif #info.req_stage_id# is #req_stage_id#>selected</cfif>>#req_stage_name#
                 </cfoutput>
                 </select>

				 </td></tr>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header">ID / Number</td>
				 <td><input class="input_text" type="text" name="req_number" style="width: 175px;" value="#info.req_number#" required maxlength="50"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Name</td>
				 <td><input class="input_text" type="text" name="req_name" style="width: 600px;" value="#info.req_name#" required maxlength="500"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description/<br>Problem Statement</td>
				 <td><textarea class="input_textarea" name="req_desc" style="width: 900px; height: 100px;" placeholder="Please provide a short description of the problems or challenges with today's capability.">#info.req_desc#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
				 <td><textarea class="input_textarea" name="req_use" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how this new capability will be used in the future.">#info.req_use#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Future State<br>Needs</td>
				 <td><textarea class="input_textarea" name="req_future" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state needs and capabilites.">#info.req_future#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Constraints/<br>Unique Requirements</td>
				 <td><textarea class="input_textarea" name="req_unique" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide any constraints or unique requirements.">#info.req_unique#</textarea></td>
		      </tr>

              <tr><td colspan=2><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="req_keywords" value="#info.req_keywords#" maxlength="500" style="width: 900px;" required placeholder="Enter up to 5 keywords (seperated by commas) that are related to this requirement."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="req_url" value="#info.req_url#" maxlength="1000" style="width: 900px;" placeholder="Please provide the link the external URL / announcement."></td>
		      </tr>

              </cfoutput>

		      </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Type</td>
				 <td width=225>

                 <select name="req_type_id" class="input_select" style="width: 175px;">
                 <option value=0>Select Type
                 <cfoutput query="types">
                  <option value=#req_type_id# <cfif #info.req_type_id# is #req_type_id#>selected</cfif>>#req_type_name#
                 </cfoutput>
                 </select>

				 </td>

				 <td class="feed_sub_header" valign=top width=150>Status</td>
				 <td>

                 <select name="req_status_id" class="input_select" style="width: 175px;">
                 <option value=0>Select Type
                 <cfoutput query="status">
                  <option value=#req_status_id# <cfif #info.req_status_id# is #req_status_id#>selected</cfif>>#req_status_name#
                 </cfoutput>
                 </select>

				 </td>
		      </tr>

		      <cfoutput>

			  <tr>
				 <td class="feed_sub_header">Start Sourcing</td>
				 <td><input class="input_date" type="date" name="req_start_date" value="#info.req_start_date#" style="width: 175px;"></td>

				 <td class="feed_sub_header">End Sourcing</td>
				 <td><input class="input_date" type="date" name="req_end_date" value="#info.req_end_date#" style="width: 175px;"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Selection Date</td>
				 <td><input class="input_date" type="date" name="req_selection_date" value="#info.req_selection_date#" style="width: 175px;"></td>

				 <td class="feed_sub_header">Arrival Date</td>
				 <td><input class="input_date" type="date" name="req_arrival_date" value="#info.req_arrival_date#"  style="width: 175px;"></td>

		      </tr>

              <tr><td class="feed_sub_header" valign=top>Display Image</td>
                  <td colspan=3 class="feed_sub_header" style="font-weight: normal;">

					<cfif #info.req_image# is "">
					  <input type="file" name="req_image">
					<cfelse>
					  <img src="#media_virtual#/#info.req_image#" width=100><br><br>
					  <input type="file" name="req_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					</cfif>

                  </td></tr>

		      </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
              <input type="submit" class="button_blue_large" name="button" value="Update Requirement">&nbsp;&nbsp;
		      <input class="button_blue_large" type="submit" name="button" value="Delete Requirement" vspace=10 onclick="return confirm('Delete Requirement?\r\nAre you sure you want to delete this Requirement?');">


              </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>