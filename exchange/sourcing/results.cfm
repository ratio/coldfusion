<cfinclude template="/exchange/security/check.cfm">

<cfset StructDelete(Session,"award_search_name")>

<cfquery name="views" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from award_view
 where award_view_usr_id = #session.usr_id#
 order by award_view_name
</cfquery>

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

      </td><td valign=top>

		<div class="main_box">
		<cfinclude template="/exchange/source/source_search.cfm">
		</div>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" valign=middle>SEARCH RESULTS - "<i><cfoutput>#session.source_keywords#</cfoutput></i>"</td>
             <td align=right><a href="index.cfm" valign=middle><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></td></tr>
         <tr><td colspan=2><hr></td></tr>
        </table>

		  <cfquery name="awards" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from award_data
	       where contains((award_description),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="graph" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], sum(federal_action_obligation) as awards from award_data
		   where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year] ASC
         </cfquery>

		  <cfquery name="award_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from award_data
		   where contains((award_description, awarding_agency_name, awarding_sub_agency_name),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year] DESC
		  </cfquery>

		  <cfquery name="grants" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from grants
	       where contains((award_description),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="grant_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
           select count(id) as total, datepart(yyyy, [action_date]) as [year], count(distinct(recipient_duns)) as vendors, count(distinct(awarding_agency_code)) as agencies, sum(federal_action_obligation) as amount from grants
	       where contains((award_description),'"#session.source_keywords#"')
           group by datepart(yyyy, [action_date])
           order by [year] DESC
		  </cfquery>

		  <cfquery name="sbir" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(id) as total, count(distinct(duns)) as vendors, count(distinct(department)) as agencies, sum(award_amount) as amount from sbir
		   where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
		  </cfquery>

		  <cfquery name="sbir_detail" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			select count(id) as total, award_year, count(distinct(duns)) as vendors, count(distinct(department)) as agencies, sum(award_amount) as amount from sbir
		    where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"')
			group by award_year
			order by award_year DESC
		  </cfquery>

		  <cfset total_value = #evaluate(sbir.vendors + grants.vendors + awards.vendors)#>

		  <cfquery name="company" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
		   select count(company_id) as total from company
 	       where contains((company_about, company_long_desc, company_history, company_keywords, company_homepage_text),'"#session.source_keywords#"')
		  </cfquery>

		  <cfoutput>
			  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		      <tr><td class="feed_sub_header">#total_value# companies were found who were awarded Federal contracts, grants, and SBIR/STTRs.
		      <a href="source_exchange.cfm"><u><cfoutput>#numberformat(company.total,'99,999')#</cfoutput> additional companies</u></a> were found that appear not to have Federal contracts.

		      <td></tr>
              <tr><td><hr></td></tr>
              </table>
          </cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr><td class="feed_sub_header" align=center width=48%>Money Spent</td>
                  <td width=50></td>
                  <td class="feed_sub_header" align=center width=48%>Awards</td></tr>

			  <tr><td>

			 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			 <script type="text/javascript">


				  google.charts.load('current', {'packages':['line']});
				  google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Year', 'Awards', 'Grants', 'SBIRs'],
						   ['2014',12000,1333,11],
						   ['2014',0,1523,0],
						   ['2014',0,0,454],
						]);

			  var options = {
					legend: { position: 'none' }
				  };

				  var chart = new google.charts.Line(document.getElementById('line_top_x'));
				  chart.draw(data, google.charts.Line.convertOptions(options));
				}
			  </script>

			<div id="line_top_x"></div>

            </td><td></td><td valign=top>

			 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">
				  google.charts.load('current', {'packages':['line']});
				  google.charts.setOnLoadCallback(drawChart);

				  function drawChart() {

						var data = google.visualization.arrayToDataTable([
						  ['Year', 'Awards', 'Grants'],
						  <cfoutput query="award_detail">
						   ['#year#',#round(amount)#,100000],
						  </cfoutput>

						]);

			  var options = {
					legend: { position: 'none' }
				  };

				  var chart = new google.charts.Line(document.getElementById('line_top_2'));
				  chart.draw(data, google.charts.Line.convertOptions(options));
				}
			  </script>

			<div id="line_top_2"></div>








			</td></tr>
			</table>

            <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

			   <tr height=50><td class="feed_sub_header" width=100>

               Federal Awards

			   </td>
			       <td class="feed_sub_header" width=100 align=center>## of Awards</td>
			       <td class="feed_sub_header" width=100 align=center>Agencies</td>
			       <td class="feed_sub_header" width=100 align=center>Companies</td>
			       <td class="feed_sub_header" width=150 align=right>Amount Awarded</td>
			       </tr>

               <tr><td colspan=5><hr></td></tr>

			   <tr height=30>
                   <td class="feed_sub_header"><b><a href="source_awards.cfm"><u>Contracts</u></a></b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(awards.total,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(awards.agencies,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(awards.vendors,'999,999')#</b></td>
                   <td class="feed_sub_header" align=right><b>#numberformat(awards.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfset counter = 0>

           <cfoutput query="award_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_option">#award_detail.year#</td>
                   <td class="feed_option" align=center>#numberformat(award_detail.total,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(award_detail.agencies,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(award_detail.vendors,'999,999')#</td>
                   <td class="feed_option" align=right>#numberformat(award_detail.amount,'$999,999,999')#</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>

           <tr><td colspan=5><hr></td></tr>

           <cfoutput>

               <tr><td class="feed_sub_header" height=30><a href="source_grants.cfm"><u>Grants</u></a></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(grants.total,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(grants.agencies,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(grants.vendors,'999,999')#</b></td>
                   <td class="feed_sub_header" align=right><b>#numberformat(grants.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfset counter = 0>

           <cfoutput query="grant_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_option" height=30>#grant_detail.year#</td>
                   <td class="feed_option" align=center>#numberformat(grant_detail.total,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(grant_detail.agencies,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(grant_detail.vendors,'999,999')#</td>
                   <td class="feed_option" align=right>#numberformat(grant_detail.amount,'$999,999,999')#</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>

           <tr><td colspan=5><hr></td></tr>

           <cfoutput>

               <tr><td class="feed_sub_header" height=30><a href="source_sbir.cfm"><u>SBIR/STTR's</u></a></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(sbir.total,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(sbir.agencies,'999,999')#</b></td>
                   <td class="feed_sub_header" align=center><b>#numberformat(sbir.vendors,'999,999')#</b></td>
                   <td class="feed_sub_header" align=right><b>#numberformat(sbir.amount,'$999,999,999')#</b></td>
                   </tr>

           </cfoutput>

           <cfset counter = 0>

           <cfoutput query="sbir_detail">

			   <cfif counter is 0>
			   <tr height=30 bgcolor="ffffff">
			   <cfelse>
			   <tr height=30 bgcolor="e0e0e0">
			   </cfif>

                   <td class="feed_option" height=30>#sbir_detail.award_year#</td>
                   <td class="feed_option" align=center>#numberformat(sbir_detail.total,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(sbir_detail.agencies,'999,999')#</td>
                   <td class="feed_option" align=center>#numberformat(sbir_detail.vendors,'999,999')#</td>
                   <td class="feed_option" align=right>#numberformat(sbir_detail.amount,'$999,999,999')#</td>
                   </tr>

               <cfif counter is 0>
                <cfset counter = 1>
               <cfelse>
                <cfset counter = 0>
               </cfif>

           </cfoutput>

			  </table>

	  </div>

    </td></tr>
  </table>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>