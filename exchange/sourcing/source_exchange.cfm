<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <cfquery name="company" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select * from company
 	       where contains((company_about, company_history, company_long_desc, company_keywords, company_homepage_text),'"#session.source_keywords#"')
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <cfoutput>
           <tr><td class="feed_header" valign=middle>#company.recordcount# companies were found matching "<i>#session.source_keywords#</i>"</td><td class="feed_option" valign=middle align=right><a href="/exchange/source/results.cfm"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </cfoutput>
         </table>

          <!--- Get Results --->

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

              <cfif company.recordcount is 0>
               <tr><td class="feed_option">No companies were found.</td></tr>
              <cfelse>

              <tr>
                  <td class="feed_sub_header"><b>Company</b></td>
                  <td class="feed_sub_header"><b>Description</b></td>
                  <td width=30>&nbsp;</td>
                  <td class="feed_sub_header"><b>City</b></td>
                  <td class="feed_sub_header" align=right><b>State</b></td>
              </tr>

              <tr><td height=15></td></tr>

              <cfoutput query="company">
              <tr>
                  <td class="feed_option" valign=top width=100 valign=top><b><a href="/exchange/include/company_profile.cfm?id=#company_id#" target="_blank" rel="noopener" rel="noreferrer">#company_name#</a></b><br>
                  <span class="text_xsmall">

                    <cfif company_logo is "">
					  <img src="//logo.clearbit.com/#company_website#" width=75 vspace=15 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#company_logo#" width=75 vspace=15>
					</cfif>

                  <br>


                  <cfif left(company_website,4) is not "http">
                  <cfset comp_url = "http://#company_website#">
                  <a href="#comp_url#" target="_blank" rel="noopener" rel="noreferrer">#comp_url#</a></span>
                  <cfelse>
                  <a href="#company_website#" target="_blank" rel="noopener" rel="noreferrer">#company_website#</a></span>
                  </cfif>

                  </td>
                  <td class="feed_option" valign=top width=800>

                  <cfif company_about is not "">

                  <cfset #desc# = #replace(company_about,"\n\n"," ","all")#>
                   #replaceNoCase(desc,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all")#

                  </cfif>

                  <cfif #company_about# is not "" and #company_keywords# is not "">
                   <br>
                   <br>
                  </cfif>

				  <cfif #company_keywords# is not "">
					<i>#replaceNoCase(company_keywords,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all")#</i>
				  </cfif>

                  </td>

                  <td width=30>&nbsp;</td>
                  <td class="feed_option" valign=top width=100>#company_city#</td>
                  <td class="feed_option" valign=top align=right width=50>#company_state#</td>

               </tr>
               <tr><td colspan=5><hr></td></tr>
              </cfoutput>

              <tr><td height=5></td></tr>

              <tr><td colspan=4 class="feed_option"><i>Commercial company names, descriptions and tags provided by Crunchbase.  <a href="http://www.clearbit.com" target="_blank" rel="noopener" rel="noreferrer">Logos provided by ClearBit.</a></td></tr>


              </cfif>

              </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

