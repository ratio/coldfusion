<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="need_questions" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from question
 left join usr on usr_id = question_usr_id
 left join company on company_id = usr_company_id
 where question_need_id = #session.need_id#
 order by question_date_submitted DESC
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/sourcing/needs/need_activity.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>QUESTIONS & ANSWERS</td>
            <td valign=top align=right class="feed_sub_header">

            <img src="/images/plus3.png" alt="Add Question" title="Add Question" width=15 border=0 hspace=10><a href="/exchange/sourcing/needs/question_add.cfm">Add Question</a>&nbsp;|&nbsp;


            <a href="/exchange/sourcing/needs/open.cfm">Return</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
        <tr><td height=10></td></tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

        <cfif need_questions.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No questions have been asked.</td><tr>
        <cfelse>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">Question has been successfully added.</td></tr>
          <cfelseif u is 2>
           <tr><td class="feed_sub_header" style="color: green;">Question has been successfully updated.</td></tr>
          <cfelseif u is 3>
           <tr><td class="feed_sub_header" style="color: green;">Question has been successfully deleted.</td></tr>
          </cfif>
        </cfif>


        <cfoutput query="need_questions">

         <tr><td class="feed_sub_header"><b>QUESTION</b></td>
             <td class="feed_sub_header" align=right><img src="/images/icon_edit.png" width=20 hspace=10><a href="question_update.cfm?question_id=#need_questions.question_id#">Update</a></b>
             </tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;"><b>#ucase(need_questions.question_subject)#</b><br>#need_questions.question_text#</td></tr>

         <tr><td class="feed_sub_header" colspan=2><b>ANSWER</b></td></tr>
         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;">

         <cfif need_questions.question_answer is "">
          Not Answered.
         <cfelse>
          #need_questions.question_answer#
         </cfif>

         </td></tr>

         <tr><td class="feed_option"><b>Submitted on: </b>#dateformat(need_questions.question_date_submitted,'mm/dd/yyyy')# at #timeformat(need_questions.question_date_submitted)#&nbsp;|&nbsp;<b>By: </b>#need_questions.usr_first_name# #need_questions.usr_last_name#&nbsp;|&nbsp;<b>Company: </b> <cfif need_questions.company_name is "">Unknown<cfelse>#need_questions.company_name#</cfif></td>
             <td class="feed_option" align=right>

             <B><cfif need_questions.question_public is "" or need_questions.question_public is 0>PRIVATE<cfelse>DISPLAYED PUBLICLY</cfif></B>

             </td></tr>
         <tr><td colspan=2><hr></td></tr>
        </cfoutput>
        </cfif>

  	   </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>