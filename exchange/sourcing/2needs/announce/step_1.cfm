<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 left join challenge_phase on challenge_phase.challenge_phase_id = challenge.challenge_phase_id
 where challenge_need_id = #session.need_id#
</cfquery>

<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #session.need_id#
</cfquery>

 <cfquery name="req" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from req
  where req_need_id = #session.need_id#
  order by req_priority DESC
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>

       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><a href="index.cfm">ANNOUNCEMENT</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return to Process</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">Announcement information has been successfully saved.</td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

         <tr><td class="feed_header">STEP 1 - DRAFT ANNOUNCEMENT</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>This step allows you finalize the information you've already entered about your Need and package it so it can be posted on the Exchange and sent to Company participants.</td></tr>

	   </table>

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif info.recordcount is 0>

	    <tr><td class="feed_sub_header" style="font-weight: normal;">You must first create a Challenge before you can issue an annoucement.</td></tr>

	   <cfelse>

	   <cfoutput>

         <tr><td colspan=2><hr></td></tr>

         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;"><b>EXCHANGE POST</b> - Snapshot of the Challenge that users will see in their Feed and email.</td></tr>
         <tr><td height=10></td></tr>

		 <tr>
			<td class="feed_sub_header">Organization</td>
			<td><input class="input_text" type="text" name="need_organization" value="#need.need_organization#" style="width: 600px;" maxlength="500" placeholder="Name of organization that is requesting the need."></td>
	     </tr>

		 <tr><td class="feed_sub_header" width=200>Challenge Name / Title</td>
		     <td><input type="text" name="challenge_name" class="input_text" style="width: 600px;" value="#info.challenge_name#"></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Challenge Description</td>
		     <td><textarea name="challenge_desc" class="input_textarea" style="width: 900px; height: 200px;">#info.challenge_desc#</textarea></td></tr>

		 <tr><td class="feed_sub_header">Start Date</td>
		     <td><input type="date" name="challenge_start" class="input_date" value="#info.challenge_start#">
		     <span class="feed_sub_header">End Date</span>&nbsp;&nbsp;<input type="date" name="challenge_end" class="input_date" value="#info.challenge_end#"></td></tr>

		 <tr><td class="feed_sub_header">Information URL</td>
		     <td><input type="text" name="challenge_url" class="input_text" size=120 maxlength="1000" value="#info.challenge_url#"></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Attachment</td>
			 <td class="feed_sub_header" style="font-weight: normal;">

			<cfif #info.challenge_attachment# is "">
			  <input type="file" name="challenge_attachment">
			<cfelse>
			  <a href="#media_virtual#/#info.challenge_attachment#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#info.challenge_attachment#</a><br><br>
			  <input type="file" name="challenge_attachment"><br><br>
			  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove attachment
			 </cfif>

			 </td></tr>
         <tr><td colspan=2><hr></td></tr>

         <tr><td class="feed_sub_header" colspan=2 style="font-weight: normal;"><b>MORE INFORMATION</b> - What users will see when the clicking on the Challenge from the Feed or opening the Challenge in the Exchange.</td></tr>
         <tr><td height=10></td></tr>

		 <tr>
			 <td class="feed_sub_header" valign=top>Need Statement</td>
			 <td><textarea class="input_textarea" name="need_desc" style="width: 900px; height: 75px;" placeholder="Please provide a short description of what this need is intented to solve.">#need.need_desc#</textarea></td>
		 </tr>

		 <tr>
		     </td><td class="feed_sub_header" valign=top>Todays<br>Challenges</td>
		 	 <td><textarea class="input_textarea" name="need_challenges" style="width: 900px; height: 75px;" placeholder="Please describes the challanges associated with today's solution or process.">#need.need_challenges#</textarea></td>
		 </tr>

		 <tr>
		    <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
		    <td><textarea class="input_textarea" name="need_use_case" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how the new solution will be used in the future.">#need.need_use_case#</textarea></td>
		 </tr>

		 <tr>
		     <td class="feed_sub_header" valign=top width=175>Future State<br>Solution</td>
		 	 <td><textarea class="input_textarea" name="need_future_state" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state solution and any constraints.">#need.need_future_state#</textarea></td>
		 </tr>

		 <tr>
			 <td class="feed_sub_header">Keywords</td>
			 <td><input class="input_text" type="text" name="need_keywords" maxlength="500" style="width: 900px;" value="#need.need_keywords#" required placeholder="Enter up to 5 keywords (seperated by commas) that are related to this requirement."></td>
		 </tr>

		 <tr><td class="feed_sub_header" valign=top>Instructions to<br>Participate</td>
		     <td><textarea name="challenge_instructions" class="input_textarea" style="width: 900px; height: 100px;">#info.challenge_instructions#</textarea></td></tr>

		 <tr><td class="feed_sub_header" valign=top>Winners Reward</td>
		     <td><textarea name="challenge_reward" class="input_textarea" style="width: 900px; height: 100px;">#info.challenge_reward#</textarea></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Name</td>
		     <td><input type="text" name="challenge_poc_name" class="input_text" style="width: 300px;" value="#info.challenge_poc_name#"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Title</td>
		     <td><input type="text" name="challenge_poc_title" class="input_text" style="width: 300px;" value="#info.challenge_poc_title#"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Phone</td>
		     <td><input type="text" name="challenge_poc_phone" class="input_text" style="width: 300px;" value="#info.challenge_poc_phone#"></td></tr>

		 <tr><td class="feed_sub_header">Challenge POC Email</td>
		     <td><input type="email" name="challenge_poc_email" class="input_text" style="width: 300px;" value="#info.challenge_poc_email#"></td></tr>




         <tr><td colspan=3><hr></td></tr>

       </cfoutput>

       <tr><td class="feed_sub_header" valign=top>Requirements</td>

       <td valign=top>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif req.recordcount is 0>
			   <tr><td class="feed_sub_header" colspan=4 style="font-weight: normal;">No Requirements have been added for this Need.</td></tr>
		    <cfelse>

			<cfloop query="req">
			<cfoutput>
			<tr><td class="feed_sub_header">#ucase(req.req_name)#</td>
				<td class="feed_sub_header" align=right>
					<cfif #req.req_priority# is 1>
					Priority: Low
					<cfelseif #req.req_priority# is 2>
					Priority: Medium
					<cfelseif #req.req_priority# is 3>
					Priority: High
					<cfelse>
					Priority: Critical
					</cfif></td></tr>
			<tr><td colspan=2 class="feed_option" style="font-weight: normal;">#req.req_desc#</td></tr>

			<cfif req.req_attachment is not "">
			<tr><td colspan=2 class="feed_option"><b>Attachment</b> - <a href="#media_virtual#/#req.req_attachment#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">Download Attachment</a></td></tr>
			</cfif>
			<cfif req.req_link is not "">
			<tr><td colspan=2 class="feed_option"><b>Reference URL</b> - <a href="#req.req_link#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#req.req_link#</a></td><tr>
			</cfif>
			<tr><td colspan=2><hr></td></tr>
			</cfoutput>

			</cfloop>

		    </cfif>

       </td></tr>

       <tr><td height=10></td></tr>
       <tr><td>

              <input type="submit" name="button" value="Save Information" class="button_blue_large">
              <input type="submit" name="button" value="Save & Approve" class="button_blue_large">

       </td></tr>

       </table>

       </td></tr>

       </table>

       </form>

       </cfif>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>