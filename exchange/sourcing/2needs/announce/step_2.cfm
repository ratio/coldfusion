<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 left join challenge_phase on challenge_phase.challenge_phase_id = challenge.challenge_phase_id
 where challenge_need_id = #session.need_id#
</cfquery>

<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #session.need_id#
</cfquery>

<cfquery name="req" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from req
 where req_need_id = #session.need_id#
 order by req_priority DESC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>

       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><a href="index.cfm">ANNOUNCEMENT</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return to Process</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>

         <cfif isdefined("u")>
          <cfif u is 1>
           <tr><td class="feed_sub_header" style="color: green;">Announcement information has been successfully saved.</td></tr>
          </cfif>
          <tr><td height=10></td></tr>
         </cfif>

         <tr><td class="feed_header">STEP 2 - APPROVE ANNOUNCEMENT</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=3>This step requires you to approve the announcement which is displayed below.  After completing this step you will choose what Companies will receive the Challenge.</td></tr>

	   </table>

	   <form action="db.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <tr><td colspan=3><hr></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">&nbsp;<b>EXCHANGE POST</b> - Snapshot of the Challenge that users will see in their Feed and email.</td>
             <td class="feed_sub_header" align=right><input type="checkbox" style="width: 20px; height: 20px;" name="post" required></td>
             <td class="feed_sub_header" align=right width=110>I APPROVE&nbsp;&nbsp;&nbsp;</td></tr>
         <tr><td colspan=3>

         <!--- Show Preview --->

	     <div class="feed_box">

			  <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td width=120 valign=middle>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<cfif need.need_image is "">
						  <img src="/images/app_innovation.png" width=100 border=0>
						<cfelse>
						  <img src="#media_virtual#/#need.need_image#" width=100 border=0>
						</cfif>

					 </table>

					 </td><td valign=top>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr>
						   <td class="feed_title" valign=top>CHALLENGE - #ucase(info.challenge_name)#</td>
						   <td align=right width=100 valign=top>

						   </td></tr>
					   <tr><td class="feed_option">#info.challenge_desc#</td></tr>
					   <tr><td class="feed_option"><b>Challenge Timeline: </b> #dateformat(info.challenge_start,'mm/dd/yyyy')# - #dateformat(info.challenge_end,'mm/dd/yyyy')#</td></tr>
					 </table>

					 </td></tr>

			</table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td height=10></td></tr>
				 <tr><td class="feed_sub_header" colspan=2>#ucase(need.need_organization)#</td></tr>
				 <tr><td height=10></td></tr>
				 <tr><td colspan=2 class="feed_option"><b>NEED STATEMENT</b></td></tr>
				 <tr><td colspan=2 class="feed_option">#need.need_desc#</td></tr>

				 <cfif #info.challenge_url# is not "">

				  <tr><td height=10></td></tr>
				  <tr><td class="link_small_gray"><a href="#info.challenge_url#" target="_blank" rel="noopener" rel="noreferrer">#info.challenge_url#</a></td>
					  <td align=right class="link_small_gray" align=right><b>1 Minute Ago</b></td></tr>

				 <tr><td height=5></td></tr>
				 </cfif>

			  </table>

          </div>

	     </cfoutput>

         </td></tr>

         <tr><td height=15></td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;">&nbsp;<b>MORE INFORMATION</B> - When clicking on the above image or the Challenge title, users will be taken to the below screen with more information.</td>
             <td class="feed_sub_header" align=right><input type="checkbox" style="width: 22px; height: 22px;" name="more" required></td>
             <td class="feed_sub_header" align=right width=110>I APPROVE&nbsp;&nbsp;&nbsp;</td></tr>
         </tr>
         </table>


         <div class="feed_box">

         <cfoutput>

			  <table cellspacing=0 cellpadding=0 border=0 width=100%>

				 <tr><td width=120 valign=middle>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>

						<cfif need.need_image is "">
						  <img src="/images/app_innovation.png" width=100 border=0>
						<cfelse>
						  <img src="#media_virtual#/#need.need_image#" width=100 border=0>
						</cfif>

					 </table>

					 </td><td valign=top>

					 <table cellspacing=0 cellpadding=0 border=0 width=100%>
					   <tr>
						   <td class="feed_title" valign=top>CHALLENGE - #ucase(info.challenge_name)#</td>
						   <td align=right width=100 valign=top>

						   </td></tr>
					   <tr><td class="feed_option">#info.challenge_desc#</td></tr>
					   <tr><td class="feed_option"><b>Challenge Timeline: </b> #dateformat(info.challenge_start,'mm/dd/yyyy')# - #dateformat(info.challenge_end,'mm/dd/yyyy')#</td></tr>
					 </table>

					 </td></tr>

			</table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		    <tr><td class="feed_sub_header" colspan=2>#ucase(need.need_organization)#</td></tr>
		    <tr><td height=10></td></tr>

		    <tr><td class="feed_option"><b>NEED STATEMENT</b></td></tr>
		    <tr><td class="feed_option">#info.challenge_desc#</td></tr>

		    <tr><td class="feed_option"><b>TODAYS CHALLENGES</b></td></tr>
		    <tr><td class="feed_option"><cfif #need.need_challenges# is "">Not provided<cfelse>#need.need_challenges#</cfif></td></tr>

		    <tr><td class="feed_option"><b>USE CASE SCENARIO</b></td></tr>
		    <tr><td class="feed_option"><cfif #need.need_use_case# is "">Not provided<cfelse>#need.need_use_case#</cfif></td></tr>

		    <tr><td class="feed_option"><b>FUTURE STATE SOLUTION</b></td></tr>
		    <tr><td class="feed_option"><cfif #need.need_future_state# is "">Not provided<cfelse>#need.need_future_state#</cfif></td></tr>

			<tr><td colspan=2><hr></td></tr>

		    <tr><td class="feed_option"><b>REQUIREMENTS</b></td></tr>
		    <tr><td class="feed_option">

		   		<table cellspacing=0 cellpadding=0 border=0 width=100%>
                 <cfif req.recordcount is 0>
                  <tr><td class="feed_option">No requirements have been developed.</td></tr>
                 <cfelse>

						<cfloop query="req">
						<cfoutput>
						<tr><td class="feed_option"><b>#ucase(req.req_name)#</b></td>
							<td class="feed_option" align=right>
							    <b>
								<cfif #req.req_priority# is 1>
								Priority: Low
								<cfelseif #req.req_priority# is 2>
								Priority: Medium
								<cfelseif #req.req_priority# is 3>
								Priority: High
								<cfelse>
								Priority: Critical
								</cfif>
								</b></td></tr>
						<tr><td colspan=2 class="feed_option" style="font-weight: normal;">#req.req_desc#</td></tr>

						<cfif req.req_attachment is not "">
						<tr><td colspan=2 class="feed_option"><b>Attachment</b> - <a href="#media_virtual#/#req.req_attachment#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">Download Attachment</a></td></tr>
						</cfif>
						<cfif req.req_link is not "">
						<tr><td colspan=2 class="feed_option"><b>Reference URL</b> - <a href="#req.req_link#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#req.req_link#</a></td><tr>
						</cfif>
						</cfoutput>

						</cfloop>

                 </cfif>

			<tr><td colspan=2><hr></td></tr>


		   		</table>

		    </td></tr>

		    <tr><td class="feed_option"><b>KEYWORDS</b></td></tr>
		    <tr><td class="feed_option"><cfif #need.need_keywords# is "">Not provided<cfelse>#need.need_keywords#</cfif></td></tr>

		    <tr><td class="feed_option"><b>INSTRUCTIONS TO PARTICIPATE</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_instructions# is "">Not provided<cfelse>#info.challenge_instructions#</cfif></td></tr>

		    <tr><td class="feed_option"><b>WINNERS REWARD</b></td></tr>
		    <tr><td class="feed_option"><cfif #info.challenge_reward# is "">Not provided<cfelse>#info.challenge_reward#</cfif></td></tr>

		    <tr><td class="feed_option"><b>POINT OF CONTACT</b></td></tr>
		    <tr><td class="feed_option">
		    <cfif #info.challenge_poc_name# is not "">#info.challenge_poc_name#</cfif><br>
		    <cfif #info.challenge_poc_title# is not "">#info.challenge_poc_title#</cfif><br>
		    <cfif #info.challenge_poc_phone# is not "">#info.challenge_poc_phone#</cfif><br>
		    <cfif #info.challenge_poc_email# is not "">#info.challenge_poc_email#</cfif><br>

		    </td></tr>

           </table>

         </cfoutput>

         </div>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=20></td></tr>
         <tr><td>
              &nbsp;<input type="submit" name="button" value="Approve & Select Recipients" class="button_blue_large">
              </td></tr>

        </table>

       </td></tr>

       </form>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>