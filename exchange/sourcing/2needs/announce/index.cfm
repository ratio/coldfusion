<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 left join challenge_phase on challenge_phase.challenge_phase_id = challenge.challenge_phase_id
 where challenge_need_id = #session.need_id#
</cfquery>

<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #session.need_id#
</cfquery>

 <cfquery name="req" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from req
  where req_need_id = #session.need_id#
 </cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>


       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header"><a href="index.cfm">ANNOUNCEMENT PROCESS</td>
			 <td class="feed_sub_header" align=right></td></tr>
		 <tr><td colspan=10><hr></td></tr>

	   </table>

	   <form action="model_db.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif info.recordcount is 0>
	    <tr><td class="feed_sub_header" style="font-weight: normal;">You must first create a Challenge before you can start the Announcement process.</td></tr>
	   <cfelse>

	     <tr><td height=20></td></tr>

         <tr><td class="feed_header" align=center><a href="step_1.cfm"><img src="/images/announce_1.png" width=150 border=0></a></td>
             <td width=50 valign=middle><img src="/images/gray_arrow.png" height=75></td>
             <td class="feed_header" align=center><a href="step_2.cfm"><img src="/images/announce_2.png" width=150 border=0></a></td>
             <td width=50 valign=middle><img src="/images/gray_arrow.png" height=75></td>
             <td class="feed_header" align=center><a href="step_3.cfm"><img src="/images/announce_3.png" height=150 border=0></a></td>
             <td width=50 valign=middle><img src="/images/gray_arrow.png" height=75></td>
             <td class="feed_header" align=center><a href="step_4.cfm"><img src="/images/announce_4.png" width=150 border=0></a></td></tr>

	     <tr><td height=20></td></tr>

         <tr><td class="feed_header" align=center><a href="step_1.cfm">Draft Announcement</a></td>
             <td width=50>&nbsp;</td>
             <td class="feed_header" align=center><a href="step_2.cfm">Approve Announcement</a></td>
             <td width=50>&nbsp;</td>
             <td class="feed_header" align=center><a href="step_3.cfm">Select Company Contacts</a></td>
             <td width=50>&nbsp;</td>
             <td class="feed_header" align=center><a href="step_4.cfm">Issue Challenge</a></td></tr>

         <tr><td class="feed_header" align=center><hr></td>
             <td width=50>&nbsp;</td>
             <td class="feed_header" align=center><hr></td>
             <td width=50>&nbsp;</td>
             <td class="feed_header" align=center><hr></td>
             <td width=50>&nbsp;</td>
             <td class="feed_header" align=center><hr><td></tr>

	     <tr><td height=20></td></tr>

         <tr><td width=23% class="feed_sub_header" style="font-weight: normal;" align=center>Finalize the information you've already entered about this Need and create a Challenge.</td>
             <td width=50>&nbsp;</td>
             <td width=23% class="feed_sub_header" style="font-weight: normal;" align=center>Write and preview the information in the Challenge and how it will be issued and viewed on the Exchange.</td>
             <td width=50>&nbsp;</td>
             <td width=23% class="feed_sub_header" style="font-weight: normal;" align=center>Select or add the points of contacts / email recipients who will receive this Challenge.</td>
             <td width=50>&nbsp;</td>
             <td width=23% class="feed_sub_header" style="font-weight: normal;" align=center>Send the Challenge to recipients, post the Challenge on the Exchange so users can learn more about it.<td></tr>


       </cfif>
       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>