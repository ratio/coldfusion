<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from challenge
 left join challenge_phase on challenge_phase.challenge_phase_id = challenge.challenge_phase_id
 where challenge_need_id = #session.need_id#
</cfquery>

<cfquery name="need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #session.need_id#
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>

       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_header"><a href="index.cfm">ANNOUNCEMENT</td>
			 <td class="feed_sub_header" align=right><a href="index.cfm">Return to Process</a></td></tr>
		 <tr><td colspan=2><hr></td></tr>

         <tr><td class="feed_header">STEP 4 - ISSUE CHALLENGE</td></tr>
         <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=2>Congratulations!  You're ready to send the Challenge to the email receipients you identified. Please confirm or modify the below information and Save the Announcement or when ready click Launch Challenge.  Launching the Challenge will send the email to the recipients are mark this Challenge as started.</td></tr>
         <tr><td colspan=2><hr></td></tr>

	   </table>

       <form action="db.cfm" method="post" enctype="multipart/form-data" >

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif info.recordcount is 0>

	    <tr><td class="feed_sub_header" style="font-weight: normal;">You have not created a Challenge.</td></tr>

	   <cfelse>

         <cfif isdefined("u")>
          <tr><td class="feed_sub_header" style="color: green;">Announcement has been successfully saved.</td></tr>
          <tr><td height=10></td></tr>
         </cfif>

         <cfoutput>
         <tr><td class="feed_sub_header">From:</td></tr>

         <cfif info.challenge_from is "">
	         <tr><td><input type="text" name="challenge_from" class="input_text" style="width: 900px;" value="#info.challenge_poc_email#"></tr>
         <cfelse>
	         <tr><td><input type="text" name="challenge_from" class="input_text" style="width: 900px;" value="#info.challenge_from#"></tr>
         </cfif>

         <tr><td class="feed_sub_header">To (recipients):</td></tr>

         <cfif info.challenge_email is "">
         	<tr><td><input type="text" name="challenge_email" class="input_text" style="width: 900px;" value="<cfif #info.challenge_email_list_1# is not "">#info.challenge_email_list_1#<cfif #info.challenge_email_list_2# is not "">, #trim(info.challenge_email_list_2)#</cfif><cfelse>#trim(info.challenge_email_list_2)#</cfif>"></td></tr>
         <cfelse>
         	<tr><td><input type="text" name="challenge_email" class="input_text" style="width: 900px;" value="#info.challenge_email#"></td></tr>
         </cfif>


         <tr><td class="feed_sub_header">Subject:</td></tr>

         <cfif info.challenge_subject is "">
         	<tr><td><input type="text" name="challenge_subject" class="input_text" style="width: 900px;" value="CHALLENGE - #info.challenge_name#"></tr>
         <cfelse>
         	<tr><td><input type="text" name="challenge_subject" class="input_text" style="width: 900px;" value="#info.challenge_subject#"></tr>
         </cfif>

         <tr><td class="feed_sub_header">Message:</td></tr>

         <cfif len(info.challenge_message) GT 0>

         <tr><td><textarea name="challenge_message" class="input_textarea" style="width: 900px; height: 400px;">#info.challenge_message#</textarea></td></tr>

         <cfelse>

         <tr><td><textarea name="challenge_message" class="input_textarea" style="width: 900px; height: 400px;">
Congratulations!

You have been selected to participate in a Challenge for [ #need.need_organization# ].  Your company has been identified as a
potential candidate with the requisite products, technologies or services that may assist in solving our challenges.

Challenge Overview

#info.challenge_desc#

Optimal Future-State Solution

#need.need_future_state#

Challenge Timeframe

Start Date - #dateformat(info.challenge_start,'mmmm dd, yyyy')#
End Date - #dateformat(info.challenge_end,'mmmm dd, yyyy')#

If you are interested in participating in this Challenge and want to learn more about it, please visit the Exchange to review
the full details, requirements, and use cases.

Thank you and we look forward to hearing from you.

Point of Contact

Name - #info.challenge_poc_name#
Title - #info.challenge_poc_title#
Phone Number - #info.challenge_poc_phone#

         </textarea></td></tr>
</cfif>
         </cfoutput>

         <tr><td height=10></td></tr>
         <tr><tr><td>

         <input type="submit" name="button" value="Save Announcement" class="button_blue_large">&nbsp;&nbsp;
         <input class="button_blue_large" type="submit" name="button" value="Save & Launch Challenge" vspace=10 onclick="return confirm('Launch Challenge?\r\nLaunching the Challenge will send the Announcements to all of your receipients and mark this Challenge as Started.  Are you sure you want to continue?');">

         </td></tr>

	  </table>

       </td></tr>

       </cfif>

       </table>

       </td></tr>

       </table>

       </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>