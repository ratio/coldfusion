<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Save Information" or #button# is "Save & Approve">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_attachment from challenge
		  where challenge_need_id = #session.need_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.challenge_attachment#">

	</cfif>

	<cfif #challenge_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select challenge_attachment from challenge
		  where challenge_need_id = #session.need_id#
		</cfquery>

		<cfif #getfile.challenge_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.challenge_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "challenge_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cftransaction>

		<cfquery name="update_challenge" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 update challenge
		 set challenge_name = '#challenge_name#',

			  <cfif #challenge_attachment# is not "">
			   challenge_attachment = '#cffile.serverfile#',
			  </cfif>
			  <cfif isdefined("remove_attachment")>
			   challenge_attachment = null,
			  </cfif>

			 challenge_desc = '#challenge_desc#',
			 challenge_instructions = '#challenge_instructions#',
			 challenge_reward = '#challenge_reward#',
			 challenge_start = <cfif #challenge_start# is "">null<cfelse>'#challenge_start#'</cfif>,
			 challenge_end = <cfif challenge_end is "">null<cfelse>'#challenge_end#'</cfif>,
			 challenge_updated = #now()#,
			 challenge_poc_name = '#challenge_poc_name#',
			 challenge_poc_title = '#challenge_poc_title#',
			 challenge_poc_phone = '#challenge_poc_phone#',
			 challenge_poc_email = '#challenge_poc_email#',
			 challenge_url = '#challenge_url#'
		 where challenge_need_id = #session.need_id#
		</cfquery>

		<cfquery name="update_need" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update need
		  set need_desc = '#need_desc#',
			  need_organization = '#need_organization#',
			  need_future_state = '#need_future_state#',
			  need_challenges = '#need_challenges#',
			  need_keywords = '#need_keywords#'
		  where need_id = #session.need_id#
		</cfquery>

	</cftransaction>

	<cfif button is "Save Information">
    	<cflocation URL="step_1.cfm?u=1" addtoken="no">
    <cfelse>
    	<cflocation URL="step_2.cfm?u=1" addtoken="no">
    </cfif>

<cfelseif button is "Save Announcement">

  <cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update challenge
   set challenge_subject = '#challenge_subject#',
       challenge_email = '#challenge_email#',
       challenge_message = '#challenge_message#',
       challenge_from = '#challenge_from#'
   where challenge_need_id = #session.need_id#
  </cfquery>

  <cflocation URL="step_4.cfm?u=1" addtoken="no">

<cfelseif button is "Save & Launch Challenge">

  <cftransaction>

	  <cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   update challenge
	   set challenge_subject = '#challenge_subject#',
		   challenge_email = '#challenge_email#',
		   challenge_message = '#challenge_message#',
		   challenge_from = '#challenge_from#'
	   where challenge_need_id = #session.need_id#
	  </cfquery>

	  <cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   update challenge
	   set challenge_issued = #now()#
	   where challenge_need_id = #session.need_id#
	  </cfquery>

  </cftransaction>

  <cflocation URL="step_4.cfm" addtoken="no">


<cfelseif button is "Approve & Select Recipients">

		<cfquery name="approve" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  update challenge
		  set challenge_approved = 1,
		      challenge_approved_date = #now()#,
		      challenge_approved_by = #session.usr_id#
		  where challenge_need_id = #session.need_id#
		</cfquery>

		<cflocation URL="step_3.cfm?u=1" addtoken="no">

</cfif>
