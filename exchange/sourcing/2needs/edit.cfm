<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css?v=3" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="stage" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need_stage
 order by need_stage_order
</cfquery>

<cfquery name="need_info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from need
 where need_id = #session.need_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

<td width=185 valign=top>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>EDIT NEED</td>
            <td valign=top align=right><a href="/exchange/sourcing/needs/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

            <form action="db.cfm" method="post" enctype="multipart/form-data" >

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

            <cfoutput>

			  <tr>
				 <td class="feed_sub_header">Name</td>
				 <td><input class="input_text" type="text" name="need_name" style="width: 600px;" required maxlength="500" value="#need_info.need_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Organization</td>
				 <td><input class="input_text" type="text" name="need_organization" value="#need_info.need_organization#" style="width: 600px;" maxlength="500" placeholder="Name of organization that is requesting the need."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="need_desc" style="width: 900px; height: 75px;" placeholder="Please provide a short description of what this need is intented to solve." required>#need_info.need_desc#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Today's<br>Challenges</td>
				 <td><textarea class="input_textarea" name="need_challenges" style="width: 900px; height: 75px;" placeholder="Please describes the challanges associated with today's solution or process.">#need_info.need_challenges#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Use Case<br>Scenario</td>
				 <td><textarea class="input_textarea" name="need_use_case" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short use case on how the new solution will be used in the future.">#need_info.need_use_case#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top width=175>Future State<br>Solution</td>
				 <td><textarea class="input_textarea" name="need_future_state" style="width: 900px; height: 75px;" rows=5 placeholder="Please provide a short description of the future-state solution and any constraints.">#need_info.need_future_state#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="need_keywords" maxlength="500" style="width: 900px;" value="#need_info.need_keywords#" required placeholder="Enter up to 5 keywords (seperated by commas) that are related to this requirement."></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Reference URL</td>
				 <td><input class="input_text" type="url" name="need_url" maxlength="1000" style="width: 900px;" value="#need_info.need_url#" placeholder="Please provide the link the external URL / announcement."></td>
		      </tr>

		      </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>

              <tr><td colspan=4><hr></td></tr>


			  <tr>
				 <td class="feed_sub_header">Solve By</td>
				 <td><input class="input_date" type="date" name="need_solve_by" style="width: 200px;" value="#need_info.need_solve_by#"></td>
		      </tr>

		   </cfoutput>

			  <tr>
				 <td class="feed_sub_header" valign=top>Allow Questions?</td>
				 <td>

                 <select name="need_commenting" class="input_select" style="width: 200px;">
                  <option value=0 <cfif need_info.need_commenting is 0>selected</cfif>>No
                  <option value=1 <cfif need_info.need_commenting is 1>selected</cfif>>Yes
                 </select>

			     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			      <span class="tooltiptext">Selecting "Yes" will allow Members to submit questions on your Need.</span>
			     </div>

				 </td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Allow Following?</td>
				 <td>

                 <select name="need_subscribe" class="input_select" style="width: 200px;">
                  <option value=0 <cfif #need_info.need_subscribe# is 0>selected</cfif>>No
                  <option value=1 <cfif #need_info.need_subscribe# is 1>selected</cfif>>Yes
                 </select>

			     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			      <span class="tooltiptext">Selecting "YES" will allow Members to Follow your Need.</span>
			     </div>

				 </td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Stage</td>
				 <td>

                 <select name="need_stage_id" class="input_select" style="width: 200px;">
                 <cfoutput query="stage">
                  <option value=#need_stage_id# <cfif need_stage_id is need_info.need_stage_id>selected</cfif>>#need_stage_name#
                 </cfoutput>
                 </select>

				 </td></tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Visibility</td>
				 <td>

                 <select name="need_public" class="input_select" style="width: 200px;">
                  <option value=0 <cfif need_info.need_public is 0>selected</cfif>>Private
                  <option value=1 <cfif need_info.need_public is 1>selected</cfif>>Public
                 </select>

			     <div class="tooltip"><img src="/images/icon_help.png" width=20 hspace=5>
			      <span class="tooltiptext">Selecting "Public" will allow Members to see your Need.</span>
			     </div>

				 </td></tr>

		      <cfoutput>

              <tr><td class="feed_sub_header" valign=top>Display Image</td>
                  <td colspan=3 class="feed_sub_header" style="font-weight: normal;">

					<cfif #need_info.need_image# is "">
					  <input type="file" name="need_image">
					<cfelse>
					  <img src="#media_virtual#/#need_info.need_image#" width=100><br><br>
					  <input type="file" name="need_image"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove logo / picture
					</cfif>

                  </td></tr>

              <tr><td colspan=4><hr></td></tr>

			  <tr>
				 <td class="feed_sub_header">Contact Name</td>
				 <td><input class="input_text" type="text" name="need_poc_name" maxlength="200" style="width: 300px;" value="#need_info.need_poc_name#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Email</td>
				 <td><input class="input_text" type="text" name="need_poc_email" maxlength="200" style="width: 300px;" value="#need_info.need_poc_email#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Contact Phone</td>
				 <td><input class="input_text" type="text" name="need_poc_phone" maxlength="100" style="width: 300px;" value="#need_info.need_poc_phone#"></td>
		      </tr>

		      </cfoutput>

              <tr><td colspan=4><hr></td></tr>
              <tr><td height=10></td></tr>
              <tr><td width=175>&nbsp;</td><td colspan=3>
              <input type="submit" class="button_blue_large" name="button" value="Update Need">&nbsp;&nbsp;
		      <input class="button_blue_large" type="submit" name="button" value="Delete Need" vspace=10 onclick="return confirm('Delete Need?\r\nDeleting this Need will remove it and any requirements created.  Are you sure you want to continue?');">

		      </td></tr>

             </table>

             </form>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>