<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
	height: auto;
	z-index: 100;
	padding-top: 10px;
	padding-left: 20px;
	padding-bottom: 10px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-left: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-bottom: 0px;
}
.tab_not_active {
	height: auto;
	z-index: 100;
	padding-top: 7px;
	padding-left: 20px;
	padding-bottom: 7px;
	padding-right: 20px;
	display: inline-block;
	margin-left: 0px;
	width: auto;
	margin-right: -4px;
	margin-top: 20px;
	margin-bottom: 0px;
	vertical-align: bottom;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #e0e0e0;
	border-bottom: 0px;
}
.main_box_2 {
	width: auto;
	height: auto;
	z-index: 100;
	box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
	padding-top: 20px;
	padding-left: 20px;
	padding-bottom: 20px;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 0px;
	margin-bottom: 0px;
	padding-right: 20px;
	border-radius: 2px;
	border-color: #b0b0b0;
	border-width: thin;
	border-style: solid;
	border-radius: 2px;
	background-color: #ffffff;
	border-top: 0px;
}
</style>

<!--- Get Data --->

<cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_created_by
 where need_id > 0

 <cfif isdefined("session.hub")>
  and need_hub_id = #session.hub#
 <cfelse>
  and need_hub_id is null
 </cfif>

 order by need_number
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

       <cfinclude template="/exchange/components/my_profile/profile.cfm">

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>

			<td class="feed_header">PARTNER NEEDS</td>
			<td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 align=absmiddle hspace=10><a href="/exchange/sourcing/needs/add.cfm">Add Partner Need</a></td>

        <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">Needs allow you to engage the Partner Network to find Partners that may be able to help you solve a problem or challenge.</td></tr>

	    </tr>

	    <tr><td colspan=2><hr></td></tr>

       </table>

        <cfif agencies.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No needs have been created.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfloop query="agencies">

			   <cfoutput>

			   <tr><td width=100 valign=top>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr>
						<td align=center valign=top>

						<cfif agencies.need_image is "">
						  <a href="/exchange/sourcing/needs/set.cfm?need_id=#agencies.need_id#"><img src="/images/stock_need.png" width=75 border=0 vspace=15></a>
						<cfelse>
						  <a href="/exchange/sourcing/needs/set.cfm?need_id=#agencies.need_id#"><img src="#media_virtual#/#agencies.need_image#" width=75 border=0 vspace=15></a>
						</cfif>

					   </td>

					</tr>

			    </table>

			   </td><td width=30>&nbsp;</td><td valign=top>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" valign=middle><a href="/exchange/sourcing/needs/set.cfm?need_id=#agencies.need_id#"><b>#ucase(agencies.need_name)#</b></a></td>
					   <td align=right><a href="/exchange/sourcing/needs/set.cfm?need_id=#agencies.need_id#"><img src="/images/#agencies.need_stage_image#" alt="#agencies.need_stage_name#" title="#agencies.need_stage_name#" height=20 width=100 align=absmiddle border=0></a></td></tr>
					<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=10>#agencies.need_desc#</td></tr>
					<tr><td class="link_small_gray"><b>Owner</b> : #agencies.usr_first_name# #agencies.usr_last_name#&nbsp;|&nbsp;<b>Keywords</b> : #agencies.need_keywords#</td>
						<td class="link_small_gray" align=right><b>Updated On</b> : #dateformat(agencies.need_updated,'mm/dd/yyyy')#</td></tr>
				   </table>

               </td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>