<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>


<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="comment" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company_comments
 where company_comments_id = #company_comment_id#
</cfquery>

<cfquery name="rating" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from comments_rating
 order by comments_rating_value DESC
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">CHALLENGE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/analyze/">ANALYZE RESULTS</a></span>
          </div>


       <div class="main_box_2">

       <cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>EDIT COMMENTS</td>
             <td align=right valign=absmiddle><a href="open.cfm?id=#id#"><img src="/images/delete.png" width=20 border=0 alt="Close" title="Close"></a></td>
		 <tr><td colspan=10><hr></td></tr>

	   </table>
	   </cfoutput>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=250>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option" align=center>

                    <a href="open.cfm?id=#info.company_id#">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=100 border=0>
					</cfif>
					</a>

					</td>
			    </tr>

                 <tr>

					<td class="feed_option" width=200 align=center>
					<a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(info.company_website) GT 40>
					<u>#ucase(left(info.company_website,40))#...</u>
					<cfelse>
					<u>#ucase(info.company_website)#</u>
					</cfif>
					</a>
					</td>

			    </tr>

			    <tr><td class="feed_option" align=center>#ucase(info.company_city)#<cfif info.company_state is not "">, #ucase(info.company_state)#</cfif></td></tr>


			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(info.company_name)#</td>
	              <td class="feed_sub_header" align=right></td></tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #info.company_about# is "">
					No Description Provided
					<cfelse>
					#info.company_about#
					</cfif>

					</td>

			    </tr>

                 <tr>

					<td class="feed_sub_header" style="font-weight: normal;" colspan=2>
					<cfif #info.company_keywords# is "">
					No Keywords Provided
					<cfelse>
					#info.company_keywords#
					</cfif>

					</td>
			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

       <!--- Company Feedback --->

       <cfoutput>

       <form action="comments_db.cfm" method="post">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_sub_header">CONTEXT</td></tr>
   		<tr><td colspan=2><input type="text" name="company_comments_context" class="input_text" size=100 maxlength="1000" value="#comment.company_comments_context#"></td></tr>
        <tr><td class="feed_sub_header">COMMENTS</td></tr>
   		<tr><td colspan=2><textarea name="company_comments_text" class="input_textarea" style="width: 1000px; height: 200px;">#comment.company_comments_text#</textarea></td></tr>

        <tr><td class="feed_sub_header" width=200>RATING</td>
            <td class="feed_sub_header">PUBLIC</td></tr>

        <tr>

        </cfoutput>

        <td><select name="company_comments_rating" class="input_select">
        <cfoutput query="rating">
         <option value=#comments_rating_value# <cfif #comments_rating_value# is #comment.company_comments_rating#>selected</cfif>>#comments_rating_name#
        </cfoutput>
        </select>

        <cfoutput>

        </td>

        <td><select name="company_comments_public" class="input_select">
         <option value=1 <cfif #comment.company_comments_public# is 1>selected</cfif>>Yes, anyone can see
         <option value=0 <cfif #comment.company_comments_public# is 0>selected</cfif>>No, display only to me and my company
        </select>

        </td></tr>


		<tr><td height=20></td></tr>

		<tr><td colspan=2>
              <input type="submit" class="button_blue_large" name="button" value="Update Comment">&nbsp;&nbsp;
		      <input class="button_blue_large" type="submit" name="button" value="Delete Comment" vspace=10 onclick="return confirm('Delete Comment?\r\nAre you sure you want to delete this comment?');">
		</td></tr>

	   </table>

	    <input type="hidden" name="id" value=#id#>
	    <input type="hidden" name="company_comments_id" value="#company_comment_id#">
	   </cfoutput>

	   </form>


        </table>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>