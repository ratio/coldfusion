<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/needs/sourcing/import.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>

       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>AUTOSOURCE - FIND COMPANIES</td>
             <td align=right valign=absmiddle class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=2><hr></td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<form action="set.cfm" method="post" onsubmit="javascript:document.getElementById('page-loader').style.display='block';">
		 <tr><td class="feed_sub_header" width=200>Keyword Matching</td>
		     <td colspan=3><input class="input_text" type="text" style="width: 275px;" placeholder = "keyword" name="auto_keyword" required onfocus="clearThis(this)">
         <tr><td height=10></td></tr>
		 <tr>
		    <td class="feed_sub_header">Sources</td>
            <td width=50><input type="checkbox" name="auto_1" checked style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Non Federal Companies</td></tr>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="auto_2" checked style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Won Federal Contracts</td>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="auto_3" checked style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Won SBIR/STTRs</td>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="auto_4" checked style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Won Grants</td>
         <tr>
		    <td></td>
            <td><input type="checkbox" name="auto_5" checked style="width: 22px; height: 22px;"</td>
		    <td class="feed_sub_header">Received Patents</td>

		 </tr>

		 <tr><td colspan=3><hr></td></tr>
		 <tr><td height=10></td></tr>

         <tr><td></td><td colspan=3><input type="submit" name="button" value="Find Companies" class="button_blue_large"></td></tr>

          </form>

          </table>


       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>