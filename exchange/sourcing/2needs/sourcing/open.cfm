<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from company
 where company_id = #id#
</cfquery>

<cfquery name="selected" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need_comp
 left join company on company_id = need_comp_selected_company_id
 where need_comp_need_id = #session.need_id# and
       need_comp_selected_company_id = #id#
 order by company_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/marketplace/portfolios.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">CHALLENGE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/analyze/">ANALYZE RESULTS</a></span>
          </div>


       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle>COMPANY INFORMATION</td>
             <td align=right valign=absmiddle>

				<form action="go.cfm" method="post">
					   <span class="feed_sub_header">Selected&nbsp;&nbsp;</span>
					   <select name="selected_company_id" class="input_select" style="width: 250px;">
					   <cfoutput query="selected">
						 <option value=#selected.company_id# <cfif #selected.company_id# is #id#>selected</cfif>>#selected.company_name#
					   </cfoutput>
  					</select>
						&nbsp;<input class="button_blue" type="submit" name="button" value="Go">

					   <span class="feed_sub_header">&nbsp;&nbsp;&nbsp;<a href="index.cfm">Return</a></span>

				</form>


             </td>


		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
	    <cfif u is 1>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully added.</td></tr>
	    <cfelseif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully updated.</td></tr>
	    <cfelseif u is 3>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Comment has been successfully deleted.</td></tr>
	    </cfif>
	   </cfif>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>

	   <cfoutput>

       <tr>

           <td valign=top width=250>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

               <tr>
					<td class="feed_option" align=center>

                    <a href="open.cfm?id=#info.company_id#">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=100 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=100 border=0>
					</cfif>
					</a>

					</td>
			    </tr>

                 <tr>

					<td class="feed_option" width=200 align=center><b>
					<a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(info.company_website) GT 40>
					#ucase(left(info.company_website,40))#...
					<cfelse>
					#ucase(info.company_website)#
					</cfif>
					</a>
					</b>
					</td>

			    </tr>


                <!--- Social Icons --->

                <tr><td height=10></td></tr>

                <tr><td align=center>

                <cfif info.company_twitter_url is not "">
                 <a href="#info.company_twitter_url#" target="blank"><img src="/images/icon_twitter.png" width=20 border=0 hspace=5></a>
                </cfif>

                <cfif info.company_linkedin_url is not "">
                 <a href="#info.company_linkedin_url#" target="blank"><img src="/images/icon_linkedin.png" width=20 border=0 hspace=5></a>
                </cfif>


                <cfif info.company_facebook_url is not "">
                 <a href="#info.company_facebook_url#" target="blank"><img src="/images/icon_facebook.png" width=20 border=0 hspace=5></a>
                </cfif>





                    </td>
                </tr>


			    <tr><td height=10></td></tr>

                <tr><td class="feed_sub_header" align=center>

                <cfif selected.need_comp_contender is 1>
	                <a href="select.cfm?id=#id#&s=0"><img src="/images/icon_checked.png" width=20 hspace=10 align=absmiddle alt="Unselect" title="Unselect" border=0></a><a href="select.cfm?id=#id#&s=0" alt="Unselect" title="Unselect">Challenge</a>
                <cfelse>
	                <a href="select.cfm?id=#id#&s=1"><img src="/images/icon_unchecked.png" width=20 hspace=10 align=absmiddle alt="Select" title="Select" border=0></a><a href="select.cfm?id=#id#&s=1" alt="Select" title="Select">Do Not Challenge</a>
                </cfif>

                </td></tr>


			    </table>

			<td valign=top>

	          <table cellspacing=0 cellpadding=0 border=0 width=100%>

	          <tr><td class="feed_header">#ucase(info.company_name)#</td>
	              <td class="feed_sub_header" align=right>

	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/resource_home.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/company_profile.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Full Company Profile</a>
	              &nbsp;|&nbsp;
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><img src="/images/icon_dashboard.png" width=20 hspace=10 align=absmiddle></a>
	              <a href="/exchange/include/award_dashboard.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer">Award Dashboard</a>

	              </td></tr>

                 <tr><td class="feed_option"><b>OVERVIEW</b></td></tr>
                 <tr>

					<td class="feed_option" style="font-weight: normal;" colspan=2>
					<cfif #info.company_about# is "">
					No overview found.
					<cfelse>
					#info.company_about#
					</cfif>

					</td>

			    </tr>

			    <cfif info.company_long_desc is not "">

                 <tr><td class="feed_option"><b>FULL DESCRIPTION</b></td></tr>

                 <tr>

					<td class="feed_option" style="font-weight: normal;" colspan=2>
					<cfif #info.company_long_desc# is "">
					No full description found.
					<cfelse>
					#info.company_long_desc#
					</cfif>

					</td>
			    </tr>

			    </cfif>


                 <tr><td class="feed_option"><b>KEYWORDS / TAGS</b></td></tr>

                 <tr>

					<td class="feed_option" style="font-weight: normal;" colspan=2>
					<cfif #info.company_keywords# is "">
					No keywords found.
					<cfelse>
					#info.company_keywords#
					</cfif>

					</td>
			    </tr>

				</table>

				</td></tr>

		    <tr><td colspan=2><hr><td></tr>

			</table>

		  </cfoutput>

       <!--- POC Information --->

	   <cfquery name="sams" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select * from sams
		 where duns = '#info.company_duns#'
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=10></td></tr>
		   <tr><td class="feed_header">POINTS OF CONTACT</td></tr>
	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr>
            <td class="feed_sub_header">NAME</td>
            <td class="feed_sub_header">TITLE / ROLE</td>
            <td class="feed_sub_header">EMAIL</td>
            <td class="feed_sub_header" align=right>PHONE</td>
        </tr>

        <cfoutput>

        <cfset poc = 0>

        <cfif info.company_poc_phone is not "" and info.company_poc_email is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif info.company_poc_first_name is "" and info.company_poc_last_name is "">Unknown<cfelse>#info.company_poc_first_name# #info.company_poc_last_name#</cfif></td>
			   <td class="feed_sub_header" style="font-weight: normal;"><cfif #info.company_poc_title# is "">Unknown<cfelse>#info.company_poc_title#</cfif></td>
			   <td class="feed_sub_header" style="font-weight: normal;">#info.company_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#info.company_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.poc_fnme is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_fnme# #sams.poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.poc_us_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.pp_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_fname# #sams.pp_poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.pp_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.pp_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.alt_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_fname# #sams.alt_poc_lname#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.alt_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.akt_poc_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.alt_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>

        <cfif sams.elec_bus_poc_fname is not "">
			<tr>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_fname# #sams.elec_bus_poc_lnmae#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_title#</td>
			   <td class="feed_sub_header" style="font-weight: normal;">#sams.elec_bus_poc_us_phone#</td>
			   <td class="feed_sub_header" style="font-weight: normal;" align=right>#sams.elec_bus_poc_email#</td>
			</tr>
			<cfset poc = 1>
        </cfif>


        <cfif poc is 0>
         <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No contact information could be found.</td></tr>
        </cfif>

        </cfoutput>

        <tr><td colspan=4><hr></td></tr>

       </table>

       <!--- Company Feedback --->

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	   <tr><td height=10></td></tr>
	   <tr><td class="feed_header">COMMENTS & NOTES</td>
	       <td class="feed_sub_header" align=right>

	       <img src="/images/icon_history2.png" width=25 hspace=10><a href="history.cfm" align=absmiddle>Historic Comments</a>

	       &nbsp;|&nbsp;

	       <cfoutput>
	       <img src="/images/plus3.png" width=15 hspace=10><a href="comment_add.cfm?id=#id#" align=absmiddle>Add Comments</a>
	       </cfoutput>

	       </td></tr>
       </table>

		<cfquery name="notes" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 select * from company_comments
		 left join usr on usr_id = company_comments_created_by
		 left join comments_rating on company_comments_rating = comments_rating_value
		 where company_comments_need_id = #session.need_id# and
		       company_comments_company_id = #id# and
		       (company_comments_created_by = #session.usr_id# or company_comments_created_by_company_id = #session.company_id#)
		 order by company_comments_created DESC
		</cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

		<cfif notes.recordcount is 0>
		  <tr><td class="feed_sub_header" style="font-weight: normal;">No comments or notes have been recorded.</td></tr>
	    <cfelse>

				<tr>
				 <td class="feed_sub_header">CONTEXT</td>
				 <td class="feed_sub_header">COMMENTS</td>
				 <td class="feed_sub_header">BY</td>
				 <td class="feed_sub_header" align=center>DATE</td>
				 <td class="feed_sub_header" align=right>RATING</td>
				</tr>

				<cfset counter = 0>

                       <cfloop query="notes">

                       <cfoutput>

						 <cfif counter is 0>
						 <tr bgcolor="FFFFFF">
						 <cfelse>
						 <tr bgcolor="E0E0E0">
						 </cfif>

 						    <cfif #company_comments_created_by# is #session.usr_id#>
						     <td class="feed_option" valign=top width=300><a href="comment_edit.cfm?id=#id#&company_comment_id=#company_comments_id#"><b>#company_comments_context#</b></a></td>
						    <cfelse>
						     <td class="feed_option" valign=top width=300 style="font-weight: normal;">#company_comments_context#</td>
						    </cfif>
							<td class="feed_option" valign=top style="font-weight: normal;" width=600>#company_comments_text#</td>
							<td class="feed_option" valign=top style="font-weight: normal;">#usr_first_name# #usr_last_name#</td>
							<td class="feed_option" valign=top style="font-weight: normal;" align=center>#dateformat(company_comments_updated,'mm/dd/yyyy')#</td>
							<td class="feed_option" valign=top style="font-weight: normal;" align=right width=50>
							<img src="/images/#comments_rating_image#" height=16>
							</td>

						 </tr>

						 <cfif counter is 0>
						  <cfset counter = 1>
						 <cfelse>
						  <cfset counter = 0>
						 </cfif>

                       </cfoutput>

                       </cfloop>


	    </cfif>
	   </table>




        </table>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>