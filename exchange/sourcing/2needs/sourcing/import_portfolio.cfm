<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="selected_list" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select need_comp_selected_company_id from need_comp
 where need_comp_need_id = #session.need_id#
</cfquery>

<cfif selected_list.recordcount is 0>
 <cfset comp_list = 0>
<cfelse>
 <cfset comp_list = valuelist(selected_list.need_comp_selected_company_id)>
</cfif>

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/needs/sourcing/import.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>


       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <tr><td class="feed_header" valign=absmiddle><a href="index.cfm">MY PORTFOLIOS</a></td>
             <td align=right valign=absmiddle class="feed_sub_header"><a href="index.cfm">Return</a></td></tr>
		 <tr><td colspan=10><hr></td></tr>
	   </table>

       <cfquery name="myportfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		select portfolio_usr_id, portfolio_image, portfolio_type_id, portfolio_access_id, portfolio_id, portfolio_name, usr_first_name, usr_last_name, portfolio_updated from portfolio
		left join usr on usr_id = portfolio_usr_id
		where (portfolio_usr_id = #session.usr_id# or (portfolio_company_id = #session.company_id# and portfolio_access_id = 2)) and
		       portfolio_type_id = 1
               order by portfolio_name
       </cfquery>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif myportfolio.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No have not created any Portfolios.</td>
              <td align=right></td></tr>

         <cfelse>

         <form action="import_portfolio_db.cfm" method="post">

				<tr>
                    <td class="feed_sub_header">Select</td>
                    <td></td>
                    <td class="feed_sub_header">Company Name</td>
                    <td class="feed_sub_header">Tags</td>
                    <td class="feed_sub_header">Website</td>
                    <td class="feed_sub_header">City</td>
                    <td class="feed_sub_header" align=center>State</td>
			    </tr>

			    <tr><td height=10></td></tr>

          <cfloop query="myportfolio">

           <cfoutput>
            <tr><td colspan=7 class="feed_sub_header" bgcolor="e0e0e0">&nbsp;#ucase(myportfolio.portfolio_name)#</td></tr>
           </cfoutput>

           <cfquery name="companies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  select company_name, company_logo, company_keywords, company_meta_keywords, company_id, company_duns, company_city, company_state, company_website, portfolio_item_portfolio_id, portfolio_item_id from portfolio_item
			  join company on company_id = portfolio_item_company_id
			  where portfolio_item_portfolio_id = #myportfolio.portfolio_id# and
					portfolio_item_type_id = 1
			  order by company_name
           </cfquery>

			<cfif #companies.recordcount# is 0>

			 <tr><td class="feed_sub_header">No companies exist.</td></tr>

		    <cfelse>

			<cfoutput>

				<tr><td>&nbsp;</td></tr>

            </cfoutput>

                <cfset row_counter = 0>
                <cfset count = 1>

				<cfoutput query="companies">

				<tr

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>
					<td width=25>&nbsp;&nbsp;<input type="checkbox" name="selected_company" <cfif listfind(comp_list,companies.company_id)>checked</cfif> style="width: 22px; height: 22px;" value=#companies.company_id#></td>
					<td width=100 class="feed_option" align=center>

                    <a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif companies.company_logo is "">
					  <img src="//logo.clearbit.com/#companies.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#companies.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header" width=300><a href="/exchange/include/company_profile.cfm?id=#companies.company_id#" target="_blank" rel="noopener" rel="noreferrer">#ucase(companies.company_name)#</a></td>
					<td class="feed_option">

					<cfif companies.company_keywords is "">
					#ucase(wrap(companies.company_meta_keywords,30,0))#
					<cfelse>
					#ucase(wrap(companies.company_keywords,30,0))#
					</cfif>

					</td>

					<td class="feed_option">
					<a href="#companies.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(companies.company_website) GT 40>
					#ucase(left(companies.company_website,40))#...
					<cfelse>
					#ucase(companies.company_website)#
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(companies.company_city)#</a></td>
					<td class="feed_option" align=center>#ucase(companies.company_state)#</a></td>
				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				<cfif count is not companies.recordcount>
				 <tr><td colspan=9><hr></td></tr>
				</cfif>

				<cfset count = count + 1>

				</cfoutput>

             </cfif>

          </cfloop>

          <tr><td colspan=3><input type="submit" name="button" value="Select" class="button_blue_large"></td></tr>

          </form>

         </cfif>

       </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>