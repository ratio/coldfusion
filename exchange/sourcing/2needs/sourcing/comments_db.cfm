<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Save Comment">

  <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   insert into company_comments
   (company_comments_context,
    company_comments_public,
    company_comments_need_id,
    company_comments_company_id,
    company_comments_text,
    company_comments_rating,
    company_comments_created_by,
    company_comments_hub_id,
    company_comments_created_by_company_id,
    company_comments_created,
    company_comments_updated)
   values
   (
   '#company_comments_context#',
    #company_comments_public#,
    #session.need_id#,
    #id#,
   '#company_comments_text#',
    #company_comments_rating#,
    #session.usr_id#,
    <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
    #session.company_id#,
    #now()#,
    #now()#
    )
  </cfquery>

  <cflocation URL="open.cfm?id=#id#&u=1" addtoken="no">

<cfelseif button is "Update Comment">

  <cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   update company_comments
   set company_comments_context = '#company_comments_context#',
       company_comments_public = #company_comments_public#,
	   company_comments_text = '#company_comments_text#',
	   company_comments_rating = #company_comments_rating#,
	   company_comments_updated = #now()#
   where company_comments_id = #company_comments_id#
  </cfquery>

  <cflocation URL="open.cfm?id=#id#&u=2" addtoken="no">

<cfelseif button is "Delete Comment">

  <cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
   delete company_comments
   where company_comments_id = #company_comments_id# and
  </cfquery>

  <cflocation URL="open.cfm?id=#id#&u=3" addtoken="no">

</cfif>