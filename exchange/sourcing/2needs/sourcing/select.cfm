<cfif s is 1>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   update need_comp
	   set need_comp_contender = 1
	   where need_comp_selected_company_id = #id# and
		 need_comp_need_id = #session.need_id#
	</cfquery>

<cfelse>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	   update need_comp
	   set need_comp_contender = null
	   where need_comp_selected_company_id = #id# and
		 need_comp_need_id = #session.need_id#
	</cfquery>

</cfif>

<cfif isdefined("l")>
	<cflocation URL="/exchange/sourcing/needs/sourcing/" addtoken="no">
<cfelse>
	<cflocation URL="/exchange/sourcing/needs/sourcing/" addtoken="no">
</cfif>