<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="delete" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
	 delete need_comp
	 where need_comp_need_id = #session.need_id#
	</cfquery>

<cfif isdefined("selected_company")>

    <cfset selected_list = listremoveduplicates(#selected_company#,',')>

	<cfloop index="i" list="#selected_list#">

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
		 insert into need_comp
		 (need_comp_company_id, need_comp_need_id, need_comp_added, need_comp_added_by, need_comp_hub_id, need_comp_selected_company_id)
		 values
		 (
		  <cfif isdefined("session.company_id")>#session.company_id#<cfelse>null</cfif>,
		  #session.need_id#,
		  #now()#,
		  #session.usr_id#,
		  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
		  #i#
		  )
		</cfquery>

	</cfloop>

</cfif>

</cftransaction>

<cflocation URL="/exchange/sourcing/needs/sourcing/index.cfm?u=1" addtoken="no">