<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

		<style>
		.tab_active {
			height: auto;
			z-index: 100;
			padding-top: 10px;
			padding-left: 20px;
			padding-bottom: 10px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-left: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-bottom: 0px;
		}
		.tab_not_active {
			height: auto;
			z-index: 100;
			padding-top: 7px;
			padding-left: 20px;
			padding-bottom: 7px;
			padding-right: 20px;
			display: inline-block;
			margin-left: 0px;
			width: auto;
			margin-right: -4px;
			margin-top: 20px;
			margin-bottom: 0px;
			vertical-align: bottom;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #e0e0e0;
			border-bottom: 0px;
		}
		.main_box_2 {
			width: auto;
			height: auto;
			z-index: 100;
			box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
			padding-top: 20px;
			padding-left: 20px;
			padding-bottom: 20px;
			margin-left: 20px;
			margin-right: 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-right: 20px;
			border-radius: 2px;
			border-color: #b0b0b0;
			border-width: thin;
			border-style: solid;
			border-radius: 2px;
			background-color: #ffffff;
			border-top: 0px;
		}
		</style>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select company_id, need_comp_contender, need_comp_id, company_id, company_logo, company_name, company_website, company_keywords, company_city, company_state, need_comp_added, company_meta_keywords,
 (select ROUND(AVG(CAST(company_comments_rating AS FLOAT)), 2) as total from company_comments where company_comments_company_id = company.company_id) as rating from need_comp
 left join company on company_id = need_comp_selected_company_id
 where need_comp_need_id = #session.need_id#
 order by company_name
</cfquery>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/needs/sourcing/import.cfm">

       </td><td valign=top>

          <div class="tab_not_active" style="margin-left: 20px;">
           <span class="feed_sub_header"><img src="/images/icon_home2.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
          </div>

          <div class="tab_active">
           <span class="feed_header"><img src="/images/icon_info.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
          </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>

       <div class="main_box_2">

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
		 <cfoutput>
		 <tr><td class="feed_header" valign=absmiddle>SOURCE COMPANIES<cfif info.recordcount GT 0>&nbsp;(#info.recordcount# Selected)</cfif></td>
		 </cfoutput>
             <td align=right valign=absmiddle>


             </td>

		 <tr><td colspan=10><hr></td></tr>

	   <cfif isdefined("u")>
	    <cfif u is 1>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully added.</td></tr>
	    <cfelseif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Selected Companies have been successfully removed.</td></tr>
	    </cfif>
	   </cfif>

	   </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_sub_header" valign=absmiddle>CANDIDATE COMPANIES</td>
			 <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="company_invite.cfm">Invite Company</a></td></tr>
         <tr><td height=10></td></tr>
       </table>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

         <cfif info.recordcount is 0>
          <tr><td class="feed_sub_header" style="font-weight: normal;">No Companies have been sourced for his need.</td>
              <td align=right></td></tr>

         <cfelse>

			<tr>
				<td class="feed_sub_header">Select</td>
				<td></td>
				<td class="feed_sub_header">Company Name</td>
				<td class="feed_sub_header">Keywords / Tags</td>
				<td class="feed_sub_header">Website</td>
				<td class="feed_sub_header">City</td>
				<td class="feed_sub_header" align=center>State</td>
				<td class="feed_sub_header" align=center>Added</td>
				<td class="feed_sub_header" align=center>Rating</td>
				<td class="feed_sub_header" align=center>Challenge</td>
			</tr>

			<tr><td height=10></td></tr>

          <cfset row_counter = 0>

          <form action="remove_selected.cfm" method="post">

          <cfloop query="info">

                <tr

                <cfoutput>

				<cfif #row_counter# is 0>
				 bgcolor="ffffff"
				<cfelse>
				 bgcolor="ffffff"
				</cfif>
				>

					<td width=25>&nbsp;&nbsp;<input type="checkbox" name="selected_company" style="width: 22px; height: 22px;" value=#info.company_id#></td>

					<td width=100 class="feed_option" align=center>

                    <a href="open.cfm?id=#info.company_id#" target="_blank" rel="noopener" rel="noreferrer">
                    <cfif info.company_logo is "">
					  <img src="//logo.clearbit.com/#info.company_website#" width=40 border=0 onerror="this.src='/images/no_logo.png'">
					<cfelse>
                      <img src="#media_virtual#/#info.company_logo#" width=40 border=0>
					</cfif>
					</a>

					</td>
					<td class="feed_sub_header" width=250><a href="open.cfm?id=#info.company_id#">#ucase(info.company_name)#</a></td>
					<td class="feed_option">

                    <cfif info.company_keywords is "" and info.company_meta_keywords is "">
                    NO KEYWORDS FOUND
                    <cfelse>
						<cfif info.company_keywords is "">
						#ucase(wrap(info.company_meta_keywords,30,0))#
						<cfelse>
						#ucase(wrap(info.company_keywords,30,0))#
						</cfif>
					</cfif>

					</td>

					<td class="feed_option" width=250>
					<a href="#info.company_website#" target="_blank" rel="noopener" rel="noreferrer">
					<cfif len(info.company_website) GT 40>
					<u>#ucase(left(info.company_website,40))#...</u>
					<cfelse>
					<u>#ucase(info.company_website)#</u>
					</cfif>
					</a>
					</td>

					<td class="feed_option">#ucase(info.company_city)#</a></td>
					<td class="feed_option" width=75 align=center>#ucase(info.company_state)#</a></td>
					<td class="feed_option" align=center width=100>#dateformat(info.need_comp_added,'mm/dd/yyyy')#</td>
					<td class="feed_option" align=center width=100>

					<cfif info.rating is 0 or info.rating is "">
					 <img src="/images/star_0.png" height=16>
					<cfelse>
					 <img src="/images/star_#round(info.rating)#.png" height=16>
					</cfif>

					</td>

					<td align=center width=75>

					<cfif info.need_comp_contender is 1>
						<a href="/exchange/sourcing/needs/sourcing/select.cfm?id=#info.company_id#&s=0&l=1"><img src="/images/icon_checked.png" width=16 align=absmiddle alt="Unselect" title="Unselect" border=0></a>
					<cfelse>
						<a href="/exchange/sourcing/needs/sourcing/select.cfm?id=#info.company_id#&s=1&l=1"><img src="/images/icon_unchecked.png" width=16 align=absmiddle alt="Select" title="Select" border=0></a>
					</cfif>

					 </td>

				</tr>

				<cfif #row_counter# is 0>
				 <cfset #row_counter# = 1>
				<cfelse>
				 <cfset #row_counter# = 0>
				</cfif>

				 <tr><td colspan=10><hr></td></tr>

				</cfoutput>

			</cfloop>

		  <tr><td height=10></td></tr>
          <tr><td colspan=3><input type="submit" name="button" value="Remove Selected" class="button_blue_large"  onclick="return confirm('Remove Selected?\r\nAre you sure you want to remove these companies from Sourcing?');"></td></tr>

          </form>

          </table>

         </cfif>

       </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>