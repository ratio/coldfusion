<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add">

	<cfif #req_attachment# is not "">
		<cffile action = "upload"
		 fileField = "req_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert req
	  (req_need_id,
	   req_start_date,
	   req_end_date,
	   req_company_id,
	   req_hub_id,
	   req_name,
	   req_created,
	   req_created_by,
	   req_desc,
	   req_attachment,
	   req_link,
	   req_priority,
	   req_est_cost,
	   req_keywords,
	   req_updated,
	   req_number
	  )
	  values
	  (
	  #session.need_id#,
	  <cfif #req_start_date# is "">null<cfelse>'#req_start_date#'</cfif>,
	  <cfif #req_end_date# is "">null<cfelse>'#req_end_date#'</cfif>,
	  #session.company_id#,
	  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
	 '#req_name#',
	  #now()#,
	  #session.usr_id#,
	 '#req_desc#',
	  <cfif #req_attachment# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
	 '#req_link#',
	  #req_priority#,
	  <cfif #req_est_cost# is "">null<cfelse>#req_est_cost#</cfif>,
	 '#req_keywords#',
	  #now()#,
	 '#req_number#'
	   )
	</cfquery>

	<cflocation URL="/exchange/sourcing/needs/open.cfm?u=10" addtoken="no">

<cfelseif button is "Update">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select req_attachment from req
		  where req_id = #req_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.req_attachment#">

	</cfif>

	<cfif #req_attachment# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select req_attachment from req
		  where req_id = #req_id#
		</cfquery>

		<cfif #getfile.req_attachment# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.req_attachment#">
		</cfif>

		<cffile action = "upload"
		 fileField = "req_attachment"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update req
       set req_name = '#req_name#',
           req_updated = #now()#,
           req_start_date = <cfif req_start_date is "">null<cfelse>'#req_start_date#'</cfif>,
           req_end_date = <cfif req_end_date is "">null<cfelse>'#req_end_date#'</cfif>,
           req_desc = '#req_desc#',
			  <cfif #req_attachment# is not "">
			   req_attachment = '#cffile.serverfile#',
			  </cfif>
			  <cfif isdefined("remove_attachment")>
			   req_attachment = null,
			  </cfif>
            req_link = '#req_link#',
            req_priority = #req_priority#,
            req_est_cost = <cfif #req_est_cost# is "">null<cfelse>#req_est_cost#</cfif>,
            req_keywords = '#req_keywords#',
            req_number = '#req_number#'
	   where req_id = #req_id#
	</cfquery>

	<cflocation URL="/exchange/sourcing/needs/open.cfm?u=20" addtoken="no">

<cfelseif button is "Delete">

    <cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select req_attachment from req
		  where req_id = #req_id#
		</cfquery>

		<cfif remove.req_attachment is not "">
		 <cffile action = "delete" file = "#media_path#\#remove.req_attachment#">
		</cfif>

		<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  delete req
		  where req_id = #req_id#
		</cfquery>

	</cftransaction>

<cflocation URL="/exchange/sourcing/needs/open.cfm?u=30" addtoken="no">

</cfif>