<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from req
 where req_id = #req_id#
</cfquery>

<cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top>

       </td><td valign=top>

       <div class="main_box">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header" valign=top>EDIT REQUIREMENT</td>
            <td valign=top align=right><a href="/exchange/sourcing/needs/open.cfm"><img src="/images/delete.png" alt="Close" title="Close" width=20 border=0></a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

        <cfoutput>

        <form action="db.cfm" method="post" enctype="multipart/form-data" >

		<table cellspacing=0 cellpadding=0 border=0 width=95%>

              <tr><td height=10></td></tr>

			  <tr>
				 <td class="feed_sub_header" width=100>Number</td>
				 <td><input class="input_text" type="text" name="req_number" style="width: 140px;" maxlength="20" value="#info.req_number#" placeholder="Requirement ID"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Name</td>
				 <td><input class="input_text" type="text" name="req_name" style="width: 800px;" value="#info.req_name#" placeholder="Requirement Name" required></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header" valign=top>Description</td>
				 <td><textarea class="input_textarea" name="req_desc" style="width: 1000px; height: 200px;" placeholder="Please provide a short description of this requirement.">#info.req_desc#</textarea></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Keywords</td>
				 <td><input class="input_text" type="text" name="req_keywords" style="width: 600px;" value="#info.req_keywords#" placeholder="i.e., Machine Learning, Energy, etc. (seperated by commas)"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Link / URL</td>
				 <td><input class="input_text" type="url" name="req_link" value="#info.req_link#" style="width: 600px;"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Start Date</td>
				 <td><input class="input_date" type="date" name="req_start_date" style="width: 175px;" value="#info.req_start_date#"></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">End Date</td>
				 <td><input class="input_date" type="date" name="req_end_date" style="width: 175px;" value="#info.req_end_date#"></td>
		      </tr>

              <tr><td class="feed_sub_header" valign=top>Attachment</td>
                  <td class="feed_sub_header" style="font-weight: normal;">

					<cfif #info.req_attachment# is "">
					  <input type="file" name="req_attachment">
					<cfelse>
					  <b>Current Attachment: </b>&nbsp;&nbsp;<a href="#media_virtual#/#info.req_attachment#" style="font-weight: normal;" target="_blank" rel="noopener" rel="noreferrer"><u>#info.req_attachment#</u></a><br><br>
					  <input type="file" name="req_attachment"><br><br>
					  <input type="checkbox" name="remove_attachment" style="width: 20px; height: 20px;">&nbsp;or, check to remove attachment
					 </cfif>

					 </td></tr>

			  <tr>
				 <td class="feed_sub_header">Estimated Cost</td>
				 <td><input class="input_text" type="number" name="req_est_cost" style="width: 150px;" value=#info.req_est_cost#></td>
		      </tr>

			  <tr>
				 <td class="feed_sub_header">Priority</td>
				 <td>

				 <select name="req_priority" class="input_select" style="width: 150px;">
				   <option value=1 <cfif info.req_priority is 1>selected</cfif>>Low
				   <option value=2 <cfif info.req_priority is 2>selected</cfif>>Medium
				   <option value=3 <cfif info.req_priority is 3>selected</cfif>>High
				   <option value=4 <cfif info.req_priority is 4>selected</cfif>>Critical
				 </select>

				 </td>
		      </tr>

              <tr><td height=10></td></tr>

              <tr><td colspan=3><hr></td></tr>

              <tr><td></td><td>

           <input type="submit" name="button" class="button_blue_large" value="Update">&nbsp;&nbsp;
           <input type="submit" name="button" class="button_blue_large" value="Delete" onclick="return confirm('Delete Requirement?\r\nAre you sure you want to delete this Requirement?');">

           </td></tr>

		</table>

		<input type="hidden" name="req_id" value=#req_id#>

		</form>

		</cfoutput>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>