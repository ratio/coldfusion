<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 20px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    display: inline-block;
    margin-left: -4px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    padding-right: 20px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}

.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<!--- Get Data --->

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td width=185 valign=top>

       <cfinclude template="/exchange/sourcing/menu.cfm">

       </td><td valign=top>

	   <div class="tab_active">
	    <span class="feed_header"><img src="/images/icon_home2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/">DEMAND PORTFOLIO</a></span>
	   </div>

	   <div class="tab_not_active">
	    <span class="feed_sub_header"><img src="/images/icon_risk.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/risks/">RISKS</a></span>
	   </div>


	   <div class="tab_not_active">
	    <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/">NEEDS</a></span>
	   </div>

	   <div class="tab_not_active">
	    <span class="feed_sub_header"><img src="/images/icon_challenge.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/challenges/">CHALLENGES</a></span>
	   </div>

       <div class="main_box_2">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr><td valign=top width=48%>

            <!--- Future Needs --->

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td valign=top class="feed_header"><a href="/exchange/sourcing/needs/">NEEDS</a></td></tr>
			  <tr><td><hr></td></tr>
			  <tr><td height=10></td></tr>
			</table>

			<cfquery name="needs" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 select * from need
			 left join need_stage on need_stage.need_stage_id = need.need_stage_id
			 left join usr on usr_id = need_created_by
			 <cfif #session.company_id# is 0>
			  where (need_created_by = #session.usr_id#)
			 <cfelse>
			  where (need_created_by = #session.usr_id# or need_company_id = #session.company_id#)
			 </cfif>
			 order by need_number
			</cfquery>

        <cfif needs.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Needs have been created.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfloop query="needs">

			   <cfoutput>

			   <tr><td valign=top width=140>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr>
						<td align=center>

						<cfif needs.need_image is "">
						  <a href="/exchange/sourcing/needs/set.cfm?need_id=#needs.need_id#"><img src="/images/stock_need.png" width=100 border=0 vspace=10></a>
						<cfelse>
						  <a href="/exchange/sourcing/needs/set.cfm?need_id=#needs.need_id#"><img src="#media_virtual#/#needs.need_image#" width=100 border=0 vspace=10></a>
						</cfif>

					   </td>

					</tr>

					<tr><td height=10></td></tr>
					<tr><td align=center><a href="/exchange/sourcing/needs/set.cfm?need_id=#needs.need_id#"><img src="/images/#needs.need_stage_image#" alt="#needs.need_stage_name#" title="#needs.need_stage_name#" height=20 width=100 align=absmiddle border=0></a></td></tr>
					<tr><td height=10></td></tr>
					<tr><td align=center class="link_small_gray"><b>#ucase(needs.need_stage_name)#</b></td></tr>

			    </table>

			   </td><td valign=top>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" valign=middle><a href="/exchange/sourcing/needs/set.cfm?need_id=#needs.need_id#"><b>#ucase(needs.need_name)#</b></a></td>
					   <td align=right></td></tr>
					<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=10>#needs.need_desc#</td></tr>
					<tr><td class="link_small_gray"><b>KEYWORDS</b>&nbsp;:&nbsp;#ucase(needs.need_keywords)#</td></tr>
				   </table>

               </td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>

          </td><td width=100>&nbsp;</td><td valign=top width=48%>

			<cfquery name="challenges" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
			 select * from challenge
			 where challenge_created_by_id = #session.usr_id#
			</cfquery>

			<!--- Market Challenges --->

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
			  <tr><td valign=top class="feed_header"><a href="/exchange/sourcing/challenges/">CHALLENGES</a></td></tr>
			  <tr><td><hr></td></tr>
			  <tr><td height=10></td></tr>
			</table>

        <cfif challenges.recordcount is 0>

         <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td height=10></td></tr>
           <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">No Market Challenges have been created.</td></tr>
         </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <cfloop query="challenges">

			   <cfoutput>

			   <tr><td valign=top width=125>

               <table cellspacing=0 cellpadding=0 border=0 width=100%>

				   <tr>
						<td>

						<cfif challenges.challenge_image is "">
						  <a href="/exchange/sourcing/challenges/set.cfm?challenge_id=#challenge_id#"><img src="/images/stock_challenge.png" vspace=10 width=100 border=0></a>
						<cfelse>
						  <a href="/exchange/sourcing/challenges/set.cfm?challenge_id=#challenge_id#"><img src="#media_virtual#/#challenges.challenge_image#" vspace=10 width=100 border=0></a>
						</cfif>

					   </td>

					</tr>

					<tr><td class="link_small_gray"><b>

					  <cfif challenge_status is 0>
					   NOT LAUNCHED
					  <cfelseif challenge_status is 1>
					   &nbsp;&nbsp;&nbsp;&nbsp;LAUNCHED
					  <cfelse>
					   CLOSED
				      </cfif></b>
				  </td></tr>

			    </table>

			   </td><td valign=top>

				   <table cellspacing=0 cellpadding=0 border=0 width=100%>
					<tr><td class="feed_sub_header" valign=middle><a href="/exchange/sourcing/challenges/set.cfm?challenge_id=#challenge_id#"><b>#ucase(challenges.challenge_name)#</b></a></td>
					   <td align=right></td></tr>
					<tr><td colspan=2 class="feed_sub_header" style="font-weight: normal; padding-top: 0px;" colspan=10>#challenges.challenge_desc#</td></tr>
					<tr><td class="link_small_gray"><b>KEYWORDS</b>&nbsp;:&nbsp;#ucase(challenges.challenge_keywords)#</td></tr>
				   </table>

               </td></tr>

               <tr><td height=10></td></tr>
               <tr><td colspan=3><hr></td></tr>
               <tr><td height=10></td></tr>

			 </cfoutput>

          </cfloop>

         </table>

        </cfif>


          </td></tr>


         </table>

	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>