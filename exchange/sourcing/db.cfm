<cfinclude template="/exchange/security/check.cfm">

<cfif #button# is "Add Need">

	<cfif #need_image# is not "">
		<cffile action = "upload"
		 fileField = "need_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">
	</cfif>

    <cftransaction>

		<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  insert into need
		  (
		  need_name,
		  need_poc_name,
		  need_poc_email,
		  need_poc_phone,
		  need_stage_id,
		  need_url,
		  need_desc,
		  need_challenges,
		  need_image,
		  need_number,
		  need_use_case,
		  need_future_state,
		  need_keywords,
		  need_solve_by,
		  need_created_by,
		  need_company_id,
		  need_created,
		  need_updated,
		  need_hub_id,
		  need_public,
		  need_subscribe,
		  need_type_id
		  )
		 values
		 (
		 '#need_name#',
		 '#need_poc_name#',
		 '#need_poc_email#',
		 '#need_poc_phone#',
		  #need_stage_id#,
		 '#need_url#',
		 '#need_desc#',
		 '#need_challenges#',
		  <cfif #need_image# is not "">'#cffile.serverfile#'<cfelse>null</cfif>,
		 '#need_number#',
		 '#need_use_case#',
		 '#need_future_state#',
		 '#need_keywords#',
		  <cfif need_solve_by is "">null<cfelse>'#need_solve_by#'</cfif>,
		  #session.usr_id#,
		  #session.company_id#,
		  #now()#,
		  #now()#,
		  <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>,
		  #need_public#,
		  #need_subscribe#,
		  1
		  )
		</cfquery>

		<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select max(need_id) as id from need
		</cfquery>

    </cftransaction>

    <cfset session.need_id = #max.id#>

	<cflocation URL="/exchange/sourcing/needs/open.cfm" addtoken="no">

<cfelseif #button# is "Delete Need">

	<cftransaction>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select need_image from need
		  where (need_id = #session.need_id#)
		</cfquery>

		<cfif remove.need_image is not "">
		 <cffile action = "delete" file = "#media_path#\#remove.need_image#">
		</cfif>

		<cftransaction>
			<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  delete req
			  where req_need_id = #session.need_id#
			</cfquery>
			<cfquery name="delete2" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
			  delete need
			  where need_id = #session.need_id#
			</cfquery>
		</cftransaction>

	<cflocation URL="/exchange/sourcing/" addtoken="no">

<cfelseif #button# is "Update Need">

	<cfif isdefined("remove_attachment")>

		<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select need_image from need
		  where need_id = #session.need_id#
		</cfquery>

		<cffile action = "delete" file = "#media_path#\#remove.need_image#">

	</cfif>

	<cfif #need_image# is not "">

		<cfquery name="getfile" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		  select need_image from need
		  where need_id = #session.need_id#
		</cfquery>

		<cfif #getfile.need_image# is not "">
		 <cffile action = "delete" file = "#media_path#\#getfile.need_image#">
		</cfif>

		<cffile action = "upload"
		 fileField = "need_image"
		 destination = "#media_path#"
		 nameConflict = "MakeUnique">

	</cfif>

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update need
	  set need_name = '#need_name#',

		  <cfif #need_image# is not "">
		   need_image = '#cffile.serverfile#',
		  </cfif>
		  <cfif isdefined("remove_attachment")>
		   need_image = null,
		  </cfif>

          need_desc = '#need_desc#',
          need_poc_name = '#need_poc_name#',
          need_poc_email = '#need_poc_email#',
          need_poc_phone = '#need_poc_phone#',
          need_organization = '#need_organization#',
          need_stage_id = #need_stage_id#,
          need_url = '#need_url#',
          need_number = '#need_number#',
          need_use_case = '#need_use_case#',
          need_future_state = '#need_future_state#',
          need_challenges = '#need_challenges#',
          need_keywords = '#need_keywords#',
          need_solve_by = <cfif need_solve_by is "">null<cfelse>'#need_solve_by#'</cfif>,
          need_updated = #now()#,
          need_subscribe = #need_subscribe#,
          need_public = #need_public#
      where need_id = #session.need_id#
	</cfquery>

</cfif>

<cflocation URL="/exchange/sourcing/needs/open.cfm?u=2" addtoken="no">