<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=top>FEDERAL CONTRACTS</td><td class="feed_option" align=right><a href="source_awards.cfm" valign=top><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>

           <tr><td colspan=2><hr></td></tr>

          </table>

		  <cfquery name="vendor" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select top(1) recipient_name from award_data
	       where contains((award_description),'"#session.source_keywords#"')
	       and recipient_duns = '#duns#'
		  </cfquery>

		  <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
           select action_date, award_description, federal_action_obligation, award_id_piid, id, awarding_agency_name, awarding_sub_agency_name from award_data
	       where contains((award_description),'"#session.source_keywords#"')
	       and recipient_duns = '#duns#'
	       and federal_action_obligation > 0
	       order by action_date DESC
		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td height=10></td></tr>
           <tr><td class="feed_sub_header"><cfoutput><a href="/exchange/include/federal_profile.cfm?duns=#duns#" target="_blank" rel="noopener" rel="noreferrer">#ucase(vendor.recipient_name)#</a></cfoutput></td></tr>
		   <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr height=50>
               <td class="feed_option"><b>Award Date</b></td>
               <td class="feed_option"><b>Contract #</b></td>
               <td class="feed_option"><b>Department</b></td>
               <td class="feed_option"><b>Agency</b></td>
               <td class="feed_option"><b>Description</b></td>
               <td class="feed_option" align=right><b>Award Value</b></td>
            </tr>

           <cfset counter = 0>
           <cfoutput query="agencies">
            <cfif counter is 0>
             <tr bgcolor="ffffff" height=30>
            <cfelse>
             <tr bgcolor="e0e0e0" height=30>
            </cfif>
               <td class="text_xsmall" valign=top width=75>#dateformat(action_date,'mm/dd/yyyy')#</td>
               <td class="text_xsmall" valign=top width=120><a href="/exchange/include/award_information.cfm?id=#id#" target="_blank" rel="noopener" rel="noreferrer"><b>#award_id_piid#</b></a></td>
               <td class="text_xsmall" valign=top width=200>#awarding_agency_name#</td>
               <td class="text_xsmall" valign=top width=200>#awarding_sub_agency_name#</td>
               <td class="text_xsmall" valign=top>#ucase(replaceNoCase(award_description,session.source_keywords,"<span style='background:yellow'>#session.source_keywords#</span>","all"))#</td>
               <td class="text_xsmall" valign=top width=100 align=right>#numberformat(federal_action_obligation,'$999,999,999')#</td>
            </tr>
            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>
          </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

