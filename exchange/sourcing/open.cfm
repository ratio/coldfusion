<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Get Data --->

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="100">
 select * from need
 left join need_stage on need_stage.need_stage_id = need.need_stage_id
 left join usr on usr_id = need_created_by
 where need_id = #session.need_id#
</cfquery>

<style>
.tab_active {
    height: auto;
    z-index: 100;
    padding-top: 10px;
    padding-left: 20px;
    padding-bottom: 10px;
    display: inline-block;
    margin-left: 20px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    padding-right: 20px;
    align: bottom;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-bottom: 0px;
}
.tab_not_active {
    height: auto;
    z-index: 100;
    padding-top: 7px;
    padding-left: 20px;
    padding-bottom: 7px;
    display: inline-block;
    margin-left: -4px;
    width: auto;
    margin-right: 0px;
    margin-top: 20px;
    margin-bottom: 0px;
    vertical-align: bottom;
    padding-right: 20px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #e0e0e0;
    border-bottom: 0px;
}

.main_box_2 {
    width: auto;
    height: auto;
    z-index: 100;
    box-shadow: 0 1px 2px 0 #a0a0a0, 0 1px 2px 0 #a0a0a0;
    padding-top: 20px;
    padding-left: 20px;
    padding-bottom: 20px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 0px;
    margin-bottom: 0px;
    padding-right: 20px;
    border-radius: 2px;
    border-color: #b0b0b0;
    border-width: thin;
    border-style: solid;
    border-radius: 2px;
    background-color: #ffffff;
    border-top: 0px;
}
</style>

<script>
function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
</script>

  <cfinclude template="/exchange/include/header.cfm">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

      <tr><td valign=top width=185>

       <cfinclude template="/exchange/sourcing/menu.cfm">
       <cfinclude template="/exchange/sourcing/needs/need_snapshot.cfm">

       </td><td valign=top>

	   <div class="tab_active">
	    <span class="feed_header"><img src="/images/icon_home2.png" width=20 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/open.cfm">NEED SUMMARY</a></span>
	   </div>

	   <div class="tab_not_active">
	    <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 align=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/sourcing/">SOURCE COMPANIES</a></span>
	   </div>

	   <div class="tab_not_active">
	    <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/challenge/">MODEL CHALLENGE</a></span>
	   </div>

	   <div class="tab_not_active">
	    <span class="feed_sub_header"><img src="/images/icon_grants3.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/announce/">ANNOUNCEMENT</a></span>
	   </div>

          <div class="tab_not_active">
           <span class="feed_sub_header"><img src="/images/icon_info.png" width=15 valign=absmiddle>&nbsp;&nbsp;<a href="/exchange/sourcing/needs/launch/">LAUNCH & MONITOR</a></span>
          </div>

       <div class="main_box_2">

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td class="feed_header">NEED SUMMARY</td>
            <td class="feed_sub_header" align=right><img src="/images/icon_edit.png" width=20 hspace=10 align=absmiddle><a href="/exchange/sourcing/needs/edit.cfm">Edit Need</a></td></tr>
        <tr><td colspan=2><hr></td></tr>
       </table>

       <cfoutput>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif isdefined("u")>
	    <cfif u is 2>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Need has been successfully updated.</td></tr>
        <cfelseif u is 10>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Requirement has been successfully added.</td></tr>
        <cfelseif u is 20>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Requirement has been successfully updated.</td></tr>
        <cfelseif u is 30>
	     <tr><td class="feed_sub_header" style="color: green;" colspan=2>Requirement has been successfully deleted.</td></tr>
        </cfif>
	   </cfif>

       <tr><td height=10></td></tr>

       <tr><td valign=middle width=125>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td>
				<cfif info.need_image is "">
				  <img src="/images/app_innovation.png" width=100>
				<cfelse>
				  <img src="#media_virtual#/#info.need_image#" width=100>
				</cfif>

			    </td></tr>
            </table>

         </td><td valign=top>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                <td class="feed_sub_header">NAME</td>
                <td class="feed_sub_header">ORGANIZATION</td>
                <td class="feed_sub_header" align=center>STAGE</td>
                <td class="feed_sub_header" align=center>VISIBILITY</td>
                <td class="feed_sub_header" align=center>SUBSCRIBE</td>
                <td class="feed_sub_header" align=center>SOLVE BY</td>
                <td class="feed_sub_header" align=right>UPDATED</td>
             </tr>

             <tr>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" width=400><b>#ucase(info.need_name)#</b></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;"><cfif #info.need_organization# is "">Not Identified<cfelse>#info.need_organization#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=100><img src="/images/#info.need_stage_image#" alt="#info.need_stage_name#" title="#info.need_stage_name#" height=20 width=100 align=absmiddle border=0></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=75><cfif #info.need_public# is 0>No<cfelse>Yes</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center width=100><cfif #info.need_subscribe# is 0>No<cfelse>Yes</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" width=100 align=center><cfif #info.need_solve_by# is "">TBD<cfelse>#dateformat(info.need_solve_by,'mm/dd/yyyy')#</cfif></td>
                <td class="feed_sub_header" valign=top style="font-weight: normal;" align=right width=120>#dateformat(info.need_updated,'mm/dd/yyyy')#</td>
             </tr>

          </table>

         </td></tr>

         <tr><td height=10></td></tr>
         <tr><td colspan=10><hr></td></tr>


       </table>

       <!---

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=10><hr></td></tr>
		     <tr><td class="feed_sub_header"><a href="##" onclick="toggle_visibility('foo');">MORE INFORMATION</a>&nbsp;&nbsp;&nbsp;<a href="##" onclick="toggle_visibility('foo');"><img src="/images/plus3.png" height=15 align=absmiddle border=0></a></td></tr>
             <tr><td height=10></td></tr>
           </table> --->

          <!--- <div id="foo" style="display:none;"> --->

           <table cellspacing=0 cellpadding=0 border=0 width=100%>

             <tr>
                 <td class="feed_sub_header">DESCRIPTION</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">TODAY'S CHALLANGES</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.need_desc# is "">Not Provided<cfelse>#info.need_desc#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.need_challenges# is "">Not Provided<cfelse>#info.need_challenges#</cfif></td>
             </tr>

             <tr><td class="feed_sub_header">USE CASE SCENARIO</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">FUTURE STATE SOLUTION</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.need_use_case# is "">Not Provided<cfelse>#info.need_use_case#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.need_future_state# is "">Not Provided<cfelse>#info.need_future_state#</cfif></td>
             </tr>

             <tr>
                 <td class="feed_sub_header">KEYWORDS</td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header">REFERENCE URL</td>
             </tr>

             <tr>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.need_keywords# is "">Not Provided<cfelse>#info.need_keywords#</cfif></td>
                 <td width=30>&nbsp;</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top width=48%><cfif #info.need_url# is "">Not Provided<cfelse><a href="#info.need_url#" target="_blank" rel="noopener" rel="noreferrer">#info.need_url#</a></cfif></td>
             </tr>

           </table>

           <table cellspacing=0 cellpadding=0 border=0 width=100%>
             <tr><td height=10></td></tr>
             <tr><td colspan=2><hr></td></tr>

             <tr>
                 <td class="feed_sub_header" width=200>POINT OF CONTACT</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.need_poc_name# is "">Not Provided<cfelse>#info.need_poc_name#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>EMAIL</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.need_poc_email# is "">Not Provided<cfelse>#info.need_poc_email#</cfif></td></tr>
             </tr>

             <tr>
                 <td class="feed_sub_header" width=200>PHONE</td>
                 <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.need_poc_phone# is "">Not Provided<cfelse>#info.need_poc_phone#</cfif></td></tr>
             </tr>

           </table>

       </cfoutput>

 	   <cfquery name="req" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	     select * from req
	     where req_need_id = #session.need_id#
	   </cfquery>

       <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td colspan=2><hr></td></tr>
         <tr><td class="feed_sub_header">REQUIREMENTS</td>
             <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 hspace=10><a href="/exchange/sourcing/needs/requirements/add.cfm">Add Requirement</a></td></tr>
       </table>


       <table cellspacing=0 cellpadding=0 border=0 width=100%>

			<cfif req.recordcount is 0>
			   <tr><td class="feed_sub_header" colspan=4 style="font-weight: normal;">No Requirements have been added for this Need.</td></tr>
		    <cfelse>

				<tr>
				 <td class="feed_sub_header">ID / #</td>
				 <td class="feed_sub_header">NAME</td>
				 <td class="feed_sub_header">DESCRIPTION</td>
				 <td class="feed_sub_header">REFERENCES</td>
				 <td class="feed_sub_header">PRIORITY</td>
				 <td class="feed_sub_header" align=right>UPDATED ON</td>
				</tr>

				<cfset counter = 0>

                       <cfloop query="req">

                       <cfoutput>

						 <cfif counter is 0>
						 <tr bgcolor="FFFFFF">
						 <cfelse>
						 <tr bgcolor="E0E0E0">
						 </cfif>

							<td class="feed_option" valign=top width=75><cfif #req.req_number# is "">TBD<cfelse>#req.req_number#</cfif></td>
							<td class="feed_option" valign=top width=200><a href="/exchange/sourcing/needs/requirements/edit.cfm?req_id=#req.req_id#"><b><cfif req.req_name is "">NOT PROVIDED<cfelse>#ucase(req.req_name)#</cfif></b></a></td>
							<td class="feed_option" valign=top width=500><cfif req.req_desc is "">Not Provided<cfelse>#replace(req.req_desc,"#chr(10)#","<br>","all")#</cfif></td>
                            <td class="feed_option" valign=top>

                            <cfif req.req_attachment is "">
                             No Attachment
                            <cfelse>
                             <a href="#media_virtual#/#req.req_attachment#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">Download Attachment</a>
                            </cfif>
                            <br>

                            <cfif req.req_link is "">
                             No Link / URL
                            <cfelse>
                             <a href="#req.req_link#" target="_blank" rel="noopener" rel="noreferrer" style="font-weight: normal;">#req.req_link#</a>
                            </cfif>

                            </td>

							<td class="feed_option" valign=top width=100>

							<cfif #req.req_priority# is 1>
							 Low
							<cfelseif #req.req_priority# is 2>
							 Medium
							<cfelseif #req.req_priority# is 3>
							 High
							<cfelse>
							 Critical
							</cfif>

							</td>

							<td class="feed_option" valign=top align=right width=100>#dateformat(req.req_updated,'mm/dd/yyyy')#</td>

						 </tr>

						 <cfif counter is 0>
						  <cfset counter = 1>
						 <cfelse>
						  <cfset counter = 0>
						 </cfif>

                       </cfoutput>

                       </cfloop>

		    </cfif>

		    </table>


	   </div>

	  </td></tr>

	  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>