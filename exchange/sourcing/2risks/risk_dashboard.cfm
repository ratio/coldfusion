<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<!--- Lookups --->

<cfquery name="stakeholders" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from stakeholder
  order by stakeholder_name
</cfquery>

<cfquery name="area" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from impact_area
  order by impact_area_name
</cfquery>

<cfquery name="division" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from division
  order by division_name
</cfquery>

<cfquery name="status" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_status
  order by risk_status_order
</cfquery>

<cfquery name="type" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_type
  order by risk_type_order
</cfquery>

<cfquery name="portfolio" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_portfolio
  order by risk_portfolio_name
</cfquery>

<cfquery name="schedule" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from assess_schedule
  order by assess_schedule_order
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>
		  <tr><td class="feed_header" valign=top height=24>RISK DASHBOARD</td>
		      <td align=right><a href="/exchange/sourcing/risks/"><img src="/images/close.png" width=20 border=0 alt="Close" title="Close"></a></td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td height=20></td></tr>
		 <tr><td height=30 class="feed_header" align=center>SUMMARY RISK</td></tr>
		 <tr><td height=20></td></tr>

		 <tr><td height=250 valign=top align=center>

		   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		   <script type="text/javascript">
			  google.charts.load('current', {'packages':['gauge']});
			  google.charts.setOnLoadCallback(drawChart);

			  function drawChart() {

				var data = google.visualization.arrayToDataTable([

				  ['Label', 'Value'],
				  <cfoutput query="area">
				  ['#impact_area_name#', #randrange(1,25)#],
				  </cfoutput>
				]);

				var options = {
				  width: 1200, height: 320,
				  redFrom: 70, redTo: 100,
				  yellowFrom:30, yellowTo: 70,
				  greenFrom:0, greenTo: 30,
				  minorTicks: 5
				};

				var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

				chart.draw(data, options);

				setInterval(function() {
				  data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
				  chart.draw(data, options);
				}, 13000);
				setInterval(function() {
				  data.setValue(1, 1, 40 + Math.round(60 * Math.random()));
				  chart.draw(data, options);
				}, 5000);
				setInterval(function() {
				  data.setValue(2, 1, 60 + Math.round(20 * Math.random()));
				  chart.draw(data, options);
				}, 26000);
			  }
			</script>
			<div id="chart_div" style="width: 100%; height: 120px;"></div>

		 </td></tr>

		 <tr><td height=30>&nbsp;</td></tr>

		 <tr><td height=20></td></tr>
		 <tr><td height=30 class="feed_header" align=center>BUSINESS LINE</td></tr>
		 <tr><td height=20></td></tr>

		 <tr><td valign=top height=250 align=center>

		   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		   <script type="text/javascript">
			  google.charts.load('current', {'packages':['gauge']});
			  google.charts.setOnLoadCallback(drawChart);

			  function drawChart() {

				var data = google.visualization.arrayToDataTable([

				  ['Label', 'Value'],
				  <cfoutput query="division">
				  ['#division_name#', #randrange(1,25)#],
				  </cfoutput>
				]);

				var options = {
				  width: 1200, height: 320,
				  redFrom: 70, redTo: 100,
				  yellowFrom:30, yellowTo: 70,
				  greenFrom:0, greenTo: 30,
				  minorTicks: 5
				};

				var chart = new google.visualization.Gauge(document.getElementById('chart_div2'));

				chart.draw(data, options);

				setInterval(function() {
				  data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
				  chart.draw(data, options);
				}, 13000);
				setInterval(function() {
				  data.setValue(1, 1, 40 + Math.round(60 * Math.random()));
				  chart.draw(data, options);
				}, 5000);
				setInterval(function() {
				  data.setValue(2, 1, 60 + Math.round(20 * Math.random()));
				  chart.draw(data, options);
				}, 26000);
			  }
			</script>
			<div id="chart_div2" style="width: 100%; height: 120px;"></div>

		 </td></tr>











         </table>

      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>