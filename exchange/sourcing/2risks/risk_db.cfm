<cfinclude template="/exchange/security/check.cfm">

<cfif button is "Add Risk">

<cftransaction>

	<cfquery name="insert" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  insert risk
	  (risk_company_id, risk_hub_id, risk_impact_id, risk_like_id, risk_observe, risk_timing_id, risk_status_close, risk_level_id, risk_stakeholders, risk_indicators, risk_arrival_date, risk_mag_id, risk_current, risk_strategy_id, risk_name, risk_desc, risk_status_id, risk_background, risk_start_date, risk_mitigation_date, risk_close_date, risk_impact, risk_comments, risk_created, risk_updated, risk_created_by)
	  values
	  (#session.company_id#, <cfif isdefined("session.hub")>#session.hub#<cfelse>null</cfif>, #risk_impact_id#,#risk_like_id#,#risk_observe#, #risk_timing_id#, <cfif #risk_status_close# is "">null<cfelse>#risk_status_close#</cfif>, #risk_level_id#, <cfif isdefined("risk_stakeholders")>'#risk_stakeholders#'<cfelse>null</cfif>,'#risk_indicators#',<cfif #risk_arrival_date# is "">null<cfelse>'#risk_arrival_date#'</cfif>,#risk_mag_id#, #risk_current#, #risk_strategy_id#,'#risk_name#','#risk_desc#', #risk_status_id#, '#risk_background#',<cfif risk_start_date is "">null<cfelse>'#risk_start_date#'</cfif>,<cfif #risk_mitigation_date# is "">null<cfelse>'#risk_mitigation_date#'</cfif>,<cfif risk_close_date is "">null<cfelse>'#risk_close_date#'</cfif>,'#risk_impact#','#risk_comments#',#now()#,#now()#,#session.usr_id#)
	</cfquery>

	<cfquery name="max" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	 select max(risk_id) as id from risk
	</cfquery>

    <cfset session.risk_id = #max.id#>

</cftransaction>

	<cfif isdefined("l")>

	 <cfif l is 1>

        <!--- Outcome Risk --->

		<cfquery name="associate" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		 insert risk_ass
		 (risk_ass_risk_id, risk_ass_outcome_id, risk_ass_associated)
		 values(#max.id#, #session.outcome_id#, #now()#)
		</cfquery>

		<cflocation URL="/exchange/outcomes/outcome_open.cfm?u=3" addtoken="no">

	 </cfif>

    </cfif>

	<cflocation URL="/exchange/sourcing/risks/risk_open.cfm?u=r1" addtoken="no">

<cfelseif button is "Update Risk">

	<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  update risk
	  set risk_name = '#risk_name#',
	      risk_status_close = <cfif #risk_status_close# is "">null<cfelse>#risk_status_close#</cfif>,
	      risk_observe = #risk_observe#,
	      risk_level_id = #risk_level_id#,
	      risk_indicators = '#risk_indicators#',
	      risk_arrival_date = <cfif #risk_arrival_date# is "">null<cfelse>'#risk_arrival_date#'</cfif>,
	      risk_mag_id = #risk_mag_id#,
	      risk_current = #risk_current#,
	      risk_strategy_id = #risk_strategy_id#,
	      risk_stakeholders = <cfif isdefined("risk_stakeholders")>'#risk_stakeholders#'<cfelse>null</cfif>,
		  risk_desc = '#risk_desc#',
		  risk_like_id = #risk_like_id#,
		  risk_impact_id = #risk_impact_id#,
		  risk_timing_id = #risk_timing_id#,
		  risk_status_id = #risk_status_id#,
		  risk_background = '#risk_background#',
		  risk_start_date = <cfif risk_start_date is "">null<cfelse>'#risk_start_date#'</cfif>,
		  risk_mitigation_date = <cfif #risk_mitigation_date# is "">null<cfelse>'#risk_mitigation_date#'</cfif>,
		  risk_close_date = <cfif risk_close_date is "">null<cfelse>'#risk_close_date#'</cfif>,
		  risk_impact = '#risk_impact#',
		  risk_comments = '#risk_comments#',
		  risk_updated = #now()#
	  where risk_id = #session.risk_id#
	</cfquery>

    <cflocation URL="/exchange/sourcing/risks/risk_open.cfm?u=r2" addtoken="no">

<cfelseif button is "Delete Risk">

    <cftransaction>

	<cfquery name="delete1" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
	  delete risk
	  where risk_id = #session.risk_id#
	</cfquery>

	</cftransaction>

<cflocation URL="/exchange/sourcing/risks/index.cfm?u=r3" addtoken="no">

</cfif>