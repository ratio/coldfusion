<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/miso_logo.png">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">

<body class="body">

<cfquery name="info" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk
  left join risk_status on risk_status.risk_status_id = risk.risk_status_id
  left join usr on usr_id = risk_created_by
  left join risk_strategy on risk_strategy.risk_strategy_id = risk.risk_strategy_id
  left join risk_timing on risk_timing.risk_timing_id = risk.risk_timing_id
  left join risk_level on risk_level.risk_level_id = risk.risk_level_id
  left join risk_mag on risk_mag.risk_mag_id = risk.risk_mag_id
  left join risk_type on risk_type.risk_type_id = risk.risk_type_id
  where risk_id = #session.risk_id#
</cfquery>

<cfquery name="mitigate" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from risk_mitigate
  left join risk_mitigate_cat on risk_mitigate_cat.risk_mitigate_cat_id = risk_mitigate.risk_mitigate_cat_id
  left join risk_level on risk_level_id = risk_mitigate_level
  where risk_mitigate_risk_id = #session.risk_id#
  order by risk_mitigate_selected DESC
</cfquery>


<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>
      </td><td valign=top>

      <div class="main_box">

      <cfoutput>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <tr><td class="feed_header">#ucase(info.risk_name)#</td>
		      <td align=right class="feed_sub_header">

		      <img src="/images/icon_edit.png" width=20 alt="Edit" title="Edit" hspace=10><a href="/exchange/sourcing/risks/risk_edit.cfm">Edit Risk</a>&nbsp;|&nbsp;
		      <a href="/exchange/sourcing/risks/">Return</a></a>

		      </td></tr>
		  <tr><td colspan=2><hr></td></tr>
		 </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

		  <cfif isdefined("u")>
		   <cfif u is "r1">
		    <tr><td class="feed_sub_header" style="color: green;">New Risk has been successfully added.</td></tr>
		   </cfif>
		  </cfif>

         <tr><td colspan=2 class="feed_sub_header" style="font-weight: normal;">#info.risk_desc#</td></tr>

		 </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <tr>
              <td class="feed_sub_header">CREATED</td>
              <td class="feed_sub_header">LAST UPDATED</td>
              <td class="feed_sub_header" colspan=2>STATUS</td>
              <td class="feed_sub_header" align=center>MAGNITUDE</td>
              <td class="feed_sub_header" align=right>CURRENT LEVEL</td>
           </tr>

          <tr>
               <td class="feed_sub_header" style="font-weight: normal;"><cfif #info.risk_created# is "">TBD<cfelse>#dateformat(info.risk_created,'mm/dd/yyyy')#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;"><cfif #info.risk_updated# is "">TBD<cfelse>#dateformat(info.risk_updated,'mm/dd/yyyy')#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;" width=100 colspan=2><cfif #info.risk_status_name# is "">TBD<cfelse>#info.risk_status_name#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif #info.risk_mag_name# is "">TBD<cfelse>#info.risk_mag_name#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;" valign=absmiddle align=right width=100>

               #info.risk_level_name#&nbsp;&nbsp;

               <cfif info.risk_level_value is 0 or info.risk_level_value is "">
                <img src="/images/risk_1.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 1>
                <img src="/images/risk_1.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 2>
                <img src="/images/risk_2.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 3>
                <img src="/images/risk_3.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 4>
                <img src="/images/risk_4.png" height=20 width=40 valign=absmiddle>
               <cfelseif info.risk_level_value is 5>
                <img src="/images/risk_5.png" height=20 width=40 valign=absmiddle>
               </cfif>

               </td>
           </tr>

           <tr><td colspan=7><hr></td></tr>

           <tr>
              <td class="feed_sub_header">STRATEGY</td>
              <td class="feed_sub_header">RISK TIMING</td>
              <td class="feed_sub_header">RISK IDENTIFIED</td>
              <td class="feed_sub_header">EST ARRIVAL DATE</td>
              <td class="feed_sub_header" align=center>MIGITATE BY</td>
              <td class="feed_sub_header" colspan=2 align=right>CLOSE BY</td>
           </tr>

          <tr>
               <td class="feed_sub_header" style="font-weight: normal;"><cfif #info.risk_strategy_name# is "">TBD<cfelse>#info.risk_strategy_name#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;"><cfif #info.risk_timing_name# is "">TBD<cfelse>#info.risk_timing_name#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;"><cfif info.risk_start_date is "">TBD<cfelse>#dateformat(info.risk_start_date,'mm/dd/yyyy')#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;"><cfif info.risk_arrival_date is "">TBD<cfelse>#dateformat(info.risk_arrival_date,'mm/dd/yyyy')#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;" align=center><cfif info.risk_mitigation_date is "">TBD<cfelse>#dateformat(info.risk_mitigation_date,'mm/dd/yyyy')#</cfif></td>
               <td class="feed_sub_header" style="font-weight: normal;" align=right colspan=2><cfif info.risk_close_date is "">TBD<cfelse>#dateformat(info.risk_close_date,'mm/dd/yyyy')#</cfif></td>
           </tr>

          <tr><td colspan=7><hr></td></tr>

          </table>

		 <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <tr>
              <td class="feed_sub_header" width=25%>RISK HISTORY</td>
              <td width=2%>&nbsp;</td>
              <td class="feed_sub_header" width=20%>STAKEHOLDERS</td>
              <td width=2%>&nbsp;</td>
              <td class="feed_sub_header" width=35%>RISK IMPACT</td>
              <td width=2%>&nbsp;</td>
              <td class="feed_sub_header" width=20%>INDICATORS</td>
          </tr>


          <tr>
              <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.risk_background# is "">No history provided.<cfelse>#info.risk_background#</cfif></td>
              <td>&nbsp;</td>

        </cfoutput>

              <td class="feed_sub_header" style="font-weight: normal;" valign=top>

				<cfif info.risk_stakeholders is "">
				 No stakeholders identified.
				<cfelse>
				<cfquery name="stakeholders" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
				 select stakeholder_name from stakeholder
				 where stakeholder_id in (#info.risk_stakeholders#)
				 order by stakeholder_name
				</cfquery>
				<cfoutput query="stakeholders">
				 <li>#stakeholder_name#
				</cfoutput>

				</cfif>

              </td>

              <td></td>

          <cfoutput>

          <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.risk_impact# is "">No impact provided.<cfelse>#info.risk_impact#</cfif></td>
          <td></td>
          <td class="feed_sub_header" style="font-weight: normal;" valign=top><cfif #info.risk_indicators# is "">No indicators identified.<cfelse>#info.risk_indicators#</cfif></td>

          </tr>

          <tr><td colspan=7><hr></td></tr>


      </table>

      </cfoutput>


 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
	      <tr><td class="feed_sub_header">RISK MITIGATION OPTIONS</td>
	          <td class="feed_sub_header" align=right><img src="/images/plus3.png" width=15 valign=middle>&nbsp;&nbsp;<a href="risk_mitigate_add.cfm?l=1">Add Mitigation Strategy</a>&nbsp;|&nbsp;<a href="risk_mitigate.cfm">View All Strategies</a></td></tr>
	      <tr><td height=10></td></tr>
	   </table>

 	   <table cellspacing=0 cellpadding=0 border=0 width=100%>

	   <cfif #mitigate.recordcount# is 0>
	      <tr><td class="feed_sub_header" style="font-weight: normal;">No risk mitigation strategies have been created or selected.</td></tr>
	   <cfelse>

	      <tr>
	          <td class="feed_sub_header">SELECTED</td>
	          <td class="feed_sub_header">NAME</td>
	          <td class="feed_sub_header">MITIGATION STRATEGY</td>
	          <td class="feed_sub_header" align=center>THROUGH</td>
	          <td class="feed_sub_header" align=center>LEVEL OF RISK</td>
	          <td class="feed_sub_header" align=right>EST COST</td>
	          <td class="feed_sub_header" align=center>START DATE</td>
	          <td class="feed_sub_header" align=right>END DATE</td>
	      </tr>

	   <cfset counter=0>

	   <cfoutput query="mitigate">

	    <cfif counter is 0>
		    <tr bgcolor="FFFFFF">
	    <cfelse>
		    <tr bgcolor="E0E0E0">
	    </cfif>

	       <td width=100 valign=top>

	       <cfif #risk_mitigate_selected# is 1>
	        &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/checkbox_checked.png" height=25 vspace=8>
	       <cfelse>
	        &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/checkbox_unchecked.png" height=22 vspace=8>
	       </cfif>

	       </td>
	       <td class="data_row" valign=top width=300><a href="risk_mitigate_edit.cfm?risk_mitigate_id=#risk_mitigate_id#&l=1"><b>#ucase(risk_mitigate_name)#</b></a></td>
           <td class="data_row" valign=top>#risk_mitigate_desc#</td>
           <td class="data_row" valign=top align=center>#risk_mitigate_cat_name#</td>

           <td class="data_row" valign=top align=center width=140>

               <cfif risk_level_value is 0 or risk_level_value is "">
                <img src="/images/risk_1.png" height=20 width=40 valign=absmiddle>
               <cfelseif risk_level_value is 1>
                <img src="/images/risk_1.png" height=20 width=40 valign=absmiddle>
               <cfelseif risk_level_value is 2>
                <img src="/images/risk_2.png" height=20 width=40 valign=absmiddle>
               <cfelseif risk_level_value is 3>
                <img src="/images/risk_3.png" height=20 width=40 valign=absmiddle>
               <cfelseif risk_level_value is 4>
                <img src="/images/risk_4.png" height=20 width=40 valign=absmiddle>
               <cfelseif risk_level_value is 5>
                <img src="/images/risk_5.png" height=20 width=40 valign=absmiddle>
               </cfif>

           </td>

           <td class="data_row" align=right width=100 valign=top><cfif #risk_mitigate_cost# is "">TBD<cfelse>#numberformat(risk_mitigate_cost,'$999,999,999')#</cfif></td>

           <td class="data_row" align=center width=120 valign=top>

                  <cfif risk_mitigate_start_date is "">
                   TBD
                  <cfelse>
                   #dateformat(risk_mitigate_start_date,'mm/dd/yyyy')#
                  </cfif>
           </td>

           <td class="data_row" align=right width=100 valign=top>
                  <cfif risk_mitigate_end_date is "">
                   TBD
                  <cfelse>
                   #dateformat(risk_mitigate_end_date,'mm/dd/yyyy')#
                  </cfif>

           </td>

	    </tr>

	    <cfif counter is 0>
	     <cfset counter = 1>
	    <cfelse>
	     <cfset counter = 0>
	    </cfif>

	   </cfoutput>

	   </cfif>

	   </table>


      </div>

      </td></tr>

   </table>

   <cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>