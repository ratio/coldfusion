<cfinclude template="/exchange/security/check.cfm">

<cftransaction>

	<cfquery name="remove" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     update risk_mitigate
     set risk_mitigate_selected = 0
	 where risk_mitigate_risk_id = #session.risk_id#
	</cfquery>

	<cfquery name="associate" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
     update risk_mitigate
     set risk_mitigate_selected = 1
	 where risk_mitigate_id = #risk_mitigate_id#
	</cfquery>

</cftransaction>

<cflocation URL="/exchange/sourcing/risks/risk_open.cfm?u=4" addtoken="no">
