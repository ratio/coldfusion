<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

 <cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top>

	  <div class="main_box">

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
           <tr><td class="feed_header" valign=middle>Companies awarded SBIR/STTRs for <i>"<cfoutput>#session.source_keywords#</cfoutput></i>"</td><td class="feed_option" align=right valign=middle><a href="/exchange/sourcing/results.cfm"><img src="/images/delete.png" border=0 width=20 alt="Close" title="Close"></a></td></tr>
           <tr><td colspan=2><hr></td></tr>
          </table>

		  <cfquery name="agencies" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
		   select count(id) as total, sum(award_amount) as amount, award_title, company, duns, city, state from sbir
	       where contains((award_title, abstract, research_keywords),'"#session.source_keywords#"') and
	       duns <> ''
	       group by duns, company, award_title, city, state

		   <cfif isdefined("sv")>

		    <cfif #sv# is 1>
		     order by company ASC
		    <cfelseif #sv# is 10>
		     order by company DESC
		    <cfelseif #sv# is 2>
		     order by total DESC
		    <cfelseif #sv# is 20>
		     order by total ASC
		    <cfelseif #sv# is 3>
		     order by amount DESC
		    <cfelseif #sv# is 30>
		     order by amount ASC
		    <cfelseif #sv# is 4>
		     order by city ASC
		    <cfelseif #sv# is 40>
		     order by city DESC
		    <cfelseif #sv# is 5>
		     order by state ASC
		    <cfelseif #sv# is 50>
		     order by state DESC

		    </cfif>

		   <cfelse>
		     order by amount DESC
		   </cfif>

		  </cfquery>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

            <tr height=50>
               <td class="feed_option"><a href="source_sbir.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>Vendor</b></a></td>
               <td class="feed_option"><a href="source_sbir.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>City</b></a></td>
               <td class="feed_option"><a href="source_sbir.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>State</b></a></td>
               <td class="feed_option" align=center><a href="source_sbir.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>Awards</b></a></td>
               <td class="feed_option" align=right><a href="source_sbir.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>Amount</b></a></td>
            </tr>

           <cfset counter = 0>
           <cfoutput query="agencies">
            <cfif counter is 0>
             <tr bgcolor="ffffff" height=30>
            <cfelse>
             <tr bgcolor="e0e0e0" height=30>
            </cfif>
               <td class="feed_option"><a href="source_sbir_detail.cfm?duns=#duns#"><b>#ucase(company)#</b></a></td>
               <td class="feed_option">#ucase(city)#</td>
               <td class="feed_option">#state#</td>
               <td class="feed_option" align=center>#numberformat(total,'999,999')#</td>
               <td class="feed_option" width=100 align=right>#numberformat(amount,'$999,999,999')#</td>
            </tr>
            <cfif counter is 0>
             <cfset counter = 1>
            <cfelse>
             <cfset counter = 0>
            </cfif>
           </cfoutput>
          </table>

	  </div>

	  </td></tr>

  </table>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>

