<style>
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  width: 300px;
  padding-top: 10px;
  padding-bottom: 10px;
  text-align: left;
  box-shadow: 1px 8px 16px 1px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<cfquery name="options" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=5></td></tr>
  <tr><form action="search_set.cfm" method="post">


	  <td class="feed_header" width=225 colspan=3>DefenseWerx Exchange
	  </td>
	  <cfoutput>
	  <td>

	  </td>
	  </cfoutput>

     <td class="feed_sub_header" align=right class="feed_sub_header">
     </td>

     </form>
     </tr>
  <tr><td height=10></td></tr>
  <tr><td colspan=5><hr></td></tr>
  <tr><td height=10></td></tr>
  </tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td height=10></td></tr>
 <tr><td width=25% valign=top>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/sourcing/challenges/">Challenge Portfolio</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/sourcing/challenges/"><img src="/images/growth.png" width=200 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

   </td><td valign=top width=25%>


	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/marketplace/partners/network_in.cfm">Companies In Network</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/marketplace/partners/network_in.cfm"><img src="/images/network_in.png" width=200 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

   </td><td valign=top width=25%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/marketplace/partners/network_out.cfm">Company Marketplace</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/marketplace/partners/network_out.cfm"><img src="/images/network_out.png" width=200 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>


   </td><td valign=top width=25%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/awards/">Research Company Awards</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/awards/"><img src="/images/research.png" width=200 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>


 </td></tr>

 <tr>
         <td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Navigate our Innovation portfolio to discover companies that are solving our clients challenges.</td>
         <td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Find Companies within DefenseWerx' network and discover their products, services, and clients.</td>
         <td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Discover thousands of Companies that are not in DefenseWerx' network.  Review their profile.</td>
         <td align=center class="feed_sub_header" style="font-weight: normal; padding-left: 30px; padding-right: 30px;">Search and find Company awards, analyze client buying patterns, trends and insights.</td>
 </tr>

</table>
