<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">
      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td></tr>
       <tr><td><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

      <cfinclude template="menu.cfm">

      </td><td width=30>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">We're Here to Help</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">
		   Booz Allen Hamilton Teams are here to help you in your process of
		   working with Partners and understanding existing relationships and agreements with have with
		   Partners. To get the process started, please fill out the general request form below or choose from
		   one of the options to the left.</td></tr>

		   <tr><td><hr></td></tr>

		   <tr><td height=10></td></tr>
		   <tr><td class="feed_header">Common Requests</td></tr>
		   <tr><td height=20></td></tr>

		  </table>

			<table cellspacing=0 cellpadding=0 border=0 width=100%>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="https://www.bah.com" target="_blank" rel="noopener" rel="noreferrer">View Risk Matrix</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="https://www.bah.com" target="_blank" rel="noopener" rel="noreferrer">View OTs</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=1">Request Tech Scouting</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=2">Request a Teaming Agreement</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=3">Request an Alliance</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=4">Request a Firmwide Agreement</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=5">Request a Market Agreement</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=6">Request a Sponsorship or Community Agreement</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=7">Request a Non-Disclosure Agreement (NDA)</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=8">Request a Reseller Agreement</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=9">Request an International Agreement</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=10">Request Colocation</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=11">Request Cobranding</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=12">Request a Joint Venture (JV)</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=13">Request Logo Usage</td></tr>
				<tr><td width=30><img src="/images/icon_bluebox.png" width=10 align=absmiddle hspace=5></td><td class="feed_sub_header" style="padding-top: 5px; padding-bottom: 5px;"><a href="request.cfm?r=14">Request a Press Release</td></tr>
			</table>


      </td></tr>

      </table>

      </div>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>