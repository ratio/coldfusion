<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">
      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td></tr>
       <tr><td><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

          <cfinclude template="menu.cfm">

      </td><td width=30>&nbsp;</td><td valign=top>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
		   <tr><td class="feed_header">Request Agreements</td></tr>
		   <tr><td class="feed_sub_header" style="font-weight: normal;">
			In addition to an NDA, you can request additional agreements from Booz Allen�s contracting team including Teaming Agreements or other acquisition strategy or proposal support agreements needed for client delivery.
		   </td></tr>

		   <tr><td><hr></td></tr>

		   <tr><td height=10></td></tr>

		   <form action="submit.cfm" method="post">

            <cfif isdefined("u")>
             <tr><td class="feed_sub_header" style="color: green;">Your request has been successfully sent.</td></tr>
            </cfif>

		   	<tr><td class="feed_sub_header">Name</td></tr>
		   	<tr><td><input type="text" name="support_name" class="input_text" style="width: 300px;" required></td></tr>

		   	<tr><td class="feed_sub_header">Email Address</td></tr>
		   	<tr><td><input type="email" name="support_email" class="input_text" style="width: 300px;" required></td></tr>

		   	<tr><td class="feed_sub_header">Partner Name (if known)</td></tr>
		   	<tr><td><input type="text" name="support_partner" class="input_text" style="width: 700px;"></td></tr>

		   	<tr><td class="feed_sub_header">Client or Account</td></tr>
		   	<tr><td><input type="text" name="support_client" class="input_text" style="width: 700px;"></td></tr>

		   	<tr><td class="feed_sub_header">More Information</td></tr>
		   	<tr><td><textarea name="support_message" class="input_textarea" style="width: 700px; height: 100px;"></textarea></td></tr>

            <tr><td height=10></td></tr>
            <tr><td><input type="submit" name="button" class="button_blue_large" value="Submit"></td></tr>

            <input type="hidden" name="support_area" value=5>

		   </form>

		  </table>

      </td></tr>

      </table>

      </div>

      </td><td valign=top>

	  <cfinclude template="/exchange/network.cfm">
      <cfinclude template="/exchange/components/my_communities/index.cfm">
      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>