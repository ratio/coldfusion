<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">
      </td><td valign=top>

      </td><td width=100% valign=top>

      <div class="main_box">

      <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr><td class="feed_header"><a href="index.cfm">SUPPORT SERVICES</a></td>
           <td class="feed_sub_header" align=right><a href="index.cfm">More Support Services</a></td></tr>
       <tr><td colspan=2><hr></td></tr>
       <tr><td height=10></td></tr>
      </table>

      <table cellspacing=0 cellpadding=0 border=0 width=100%>

       <tr><td valign=top width=200>

          <cfinclude template="menu.cfm">

      </td><td width=30>&nbsp;</td><td valign=top>

      <cfif r is 1>
       <cfinclude template="request_scouting.cfm">
      <cfelseif r is 2>
       <cfinclude template="request_teaming.cfm">
      <cfelseif r is 3>
       <cfinclude template="request_alliance.cfm">
      <cfelseif r is 4>
       <cfinclude template="request_firmwide.cfm">
      <cfelseif r is 5>
       <cfinclude template="request_market.cfm">
      <cfelseif r is 6>
       <cfinclude template="request_sponsor.cfm">
      <cfelseif r is 7>
       <cfinclude template="request_nda.cfm">
      <cfelseif r is 8>
       <cfinclude template="request_resale.cfm">
      <cfelseif r is 9>
       <cfinclude template="request_international.cfm">
      <cfelseif r is 10>
       <cfinclude template="request_colocation.cfm">
      <cfelseif r is 11>
       <cfinclude template="request_cobranding.cfm">
      <cfelseif r is 12>
       <cfinclude template="request_jv.cfm">
      <cfelseif r is 13>
       <cfinclude template="request_logo.cfm">
      <cfelseif r is 14>
       <cfinclude template="request_press.cfm">
      </cfif>

      </td></tr>

      </table>

      </div>

      </td>

	  </div>

	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>