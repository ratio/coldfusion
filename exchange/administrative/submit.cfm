<cfinclude template="/exchange/security/check.cfm">

<cfif isdefined("test_email")>
 <cfset to = #test_email#>
<cfelse>
 <cfset to = #administrative_email#>
</cfif>

<cfif support_area is 1>
 <cfset #support_subject# = "General Support Request">
<cfelseif support_area is 2>
 <cfset #support_subject# = "Request NDA">
<cfelseif support_area is 3>
 <cfset #support_subject# = "Request Partner Risk Matrix">
<cfelseif support_area is 4>
 <cfset #support_subject# = "Request a Resale">
<cfelseif support_area is 5>
 <cfset #support_subject# = "Request Agreement">
<cfelseif support_area is 6>
 <cfset #support_subject# = "Tech Scouting Request">
</cfif>

<cfmail from="Exchange <noreply@ratio.exchange>"
		  to="#to#"
  username="noreply@ratio.exchange"
  password="Gofus107!"
	  port="25"
	useSSL="false"
	type="html"
	server="mail.ratio.exchange"
   subject="#support_subject#">
<html>
<head>
<title><cfoutput>#session.network_name#</cfoutput></title>
</head>
<body class="body">

<cfoutput>

<table cellspacing=0 cellpadding=0 border=0 width=675 bgcolor="ffffff">
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 20px;"><b>An administrative support request was received through the Booz Allen Partner Exchange.</b></td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>User: </b>#support_name#</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>Email:</b>#support_email#</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>Support Need:</b>#support_subject#</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>Partner (if known):</b>#support_partner#</td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>Client or Account:</b>#support_client#</td></tr>
 <tr><td height=20></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;"><b>More Information</b></td></tr>
 <tr><td style="feed_sub_header" style="font-weight: normal; font-size: 16px;">#support_message#</td></tr>
 <tr><td>&nbsp;</td></tr>
</table>
</cfoutput>

</body>
</html>

</cfmail>

<cfif support_area is 1>
	<cflocation URL="index.cfm?u=1" addtoken="no">
<cfelseif support_area is 2>
	<cflocation URL="request_nda.cfm?u=1" addtoken="no">
<cfelseif support_area is 3>
	<cflocation URL="request_matrix.cfm?u=1" addtoken="no">
<cfelseif support_area is 4>
	<cflocation URL="request_resale.cfm?u=1" addtoken="no">
<cfelseif support_area is 5>
	<cflocation URL="request_agreements.cfm?u=1" addtoken="no">
<cfelseif support_area is 6>
	<cflocation URL="request_scouting.cfm?u=1" addtoken="no">
</cfif>
