<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<cfquery name="display" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
 select * from menu
 where menu_id = #decrypt(i,session.key, "AES/CBC/PKCS5Padding", "HEX")#
</cfquery>

<cfinclude template = "/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr>
	  <td valign=top>
	      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      </td>

	  <td valign=top width=100%>

	      <div class="main_box">

	      <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
	          <tr><td class="feed_header">#display.menu_name#</td></tr>
	          <tr><td><hr></td></tr>
	          <tr><td height=10></td></tr>
          </table>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tr><td>

             #display.menu_custom_code#

            </td></tr>
          </table>

          </cfoutput>

          </div>

   	  </td></tr>

  </table>

  <cfinclude template="/exchange/include/footer.cfm">

</body>
</html>