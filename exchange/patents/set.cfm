<cfinclude template="/exchange/security/check.cfm">

<cfset search_string = #replace(search_keyword,chr(34),'',"all")#>
<cfset search_string = #replace(search_string,'(','',"all")#>
<cfset search_string = #replace(search_string,')','',"all")#>
<cfset search_string = #replace(search_string,'''','',"all")#>
<cfset search_string = #replace(search_string,',','',"all")#>
<cfset search_string = #replace(search_string,':','',"all")#>
<cfset search_string = '"' & #search_string#>
<cfset search_string = #search_string# & '"'>
<cfset search_string = #replace(search_string,' or ','" or "',"all")#>
<cfset search_string = #replace(search_string,' and ','" and "',"all")#>
<cfset search_string = #replace(search_string,' and "not ',' and not "',"all")#>
<cfset search_string = #replace(search_string,'"(','("',"all")#>
<cfset search_string = #replace(search_string,')"','")',"all")#>

<cfset session.patent_keyword = #search_string#>
<cfset session.search_type = #search_type#>

<cflocation URL="/exchange/patents/results.cfm" addtoken="no">