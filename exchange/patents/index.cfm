<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

  <cfinclude template="/exchange/include/header.cfm">

  <table cellspacing=0 cellpadding=0 border=0 width=100%>

	  <tr><td valign=top width=185>

      <cfinclude template="/exchange/components/my_profile/profile.cfm">
      <cfinclude template="/exchange/portfolio/recent.cfm">

      </td><td valign=top width=100%>

		<div class="main_box">
			<cfinclude template="/exchange/patents/search_patents.cfm">
		</div>

	   <!--- Get Last 50 viewed --->

       <cfset counter = 1>

	   <cfquery name="list" datasource="#client_datasource#" username="#client_username#" password="#client_password#" maxrows="25">
		select recent_patent_id, max(recent_date) from recent
		where recent_usr_id = #session.usr_id# and
		      recent_patent_id is not null
		group by recent_patent_id
		order by max(recent_date) DESC
	   </cfquery>

	   <cfif list.recordcount is 0>
	    <cfset recent_list = 0>
	   <cfelse>
	    <cfset recent_list = valuelist(list.recent_patent_id)>
	   </cfif>

	   <cfquery name="recent" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#" maxrows="15">
		select patent.patent_id, date, title, patent.type as patent_type, organization from patent
		join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id

             <cfif listlen(recent_list) GT 0>
             order by case patent.patent_id
             <cfloop index="i" list="#recent_list#">
              when <cfoutput>'#i#' then #counter#</cfoutput>
              <cfset counter = counter + 1>
             </cfloop>
             END
             </cfif>

	   </cfquery>

      <div class="main_box">

        <table cellspacing=0 cellpadding=0 border=0 width=100%>

		 <tr><td class="feed_header" valign=middle>Recently Viewed Patents (Last 15)</td>
		 <tr><td colspan=2><hr></td></tr>

        </table>

        <cfif recent.recordcount is 0>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td class="feed_sub_header" style="font-weight: normal;">You have not viewed any Patents.</td></tr>
        </table>

        <cfelse>

        <table cellspacing=0 cellpadding=0 border=0 width=100%>
        <tr><td height=10></td></tr>

          <cfoutput>

			  <tr>
				 <td class="feed_sub_header"><b>Patent ##</b></td>
				 <td class="feed_sub_header"><b>Title</b></td>
			 	 <td class="feed_sub_header"><b>Organization</b></td>
			 	 <td class="feed_sub_header" align=center><b>Granted</b></td>
			 	 <td class="feed_sub_header" align=center><b>Type</b></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>

           <cfloop query="recent">

			   <cfif counter is 0>
				<cfset bgc = "ffffff">
			   <cfelse>
			    <cfset bgc = "e0e0e0">
			   </cfif>

               <cfoutput>

			   <tr bgcolor="#bgc#" height=50>

				       <td class="feed_sub_header" valign=top width=100><a href="/exchange/include/patent_information.cfm?patent_id=#recent.patent_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#recent.patent_id#</b></a></td>
				       <td class="feed_sub_header" valign=top><a href="/exchange/include/patent_information.cfm?patent_id=#recent.patent_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#recent.title#</b></a></td>
				       <td class="feed_sub_header" valign=top style="font-weight: normal;">#recent.organization#</td>
				       <td class="feed_sub_header" valign=top style="font-weight: normal;" align=center>#dateformat(recent.date,'mm/dd/yyyy')#</td>
				       <td class="feed_sub_header" valign=top style="font-weight: normal;" width=100 align=center>#ucase(recent.patent_type)#</td>
				</tr>

				</cfoutput>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

         </cfloop>

	  </table>

	  </cfif>

       </td></tr>

       </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

 </body>
</html>