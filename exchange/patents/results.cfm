<cfinclude template="/exchange/security/check.cfm">

<html>
<head>
	<title><cfoutput>#session.network_name#</cfoutput></title>
    <link rel="shortcut icon" type="image/png" href="/images/exchange.png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="/include/exchange_style.css" rel="stylesheet" type="text/css">
</head><div class="center">
<body class="body">

<!--- Build Query --->

<cfif not isdefined("sv")>
 <cfset sv = 1>
</cfif>

<cfif session.search_type is 1>

  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select patent.patent_id, country, title, organization, patent.type, abstract as patent_desc, date from patent
   left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
   where patent.patent_id is not null
   and contains((patent.patent_id, title, abstract),'#trim(session.patent_keyword)#')

   <cfif sv is 1>
    order by date DESC
   <cfelseif sv is 10>
    order by date ASC
   <cfelseif sv is 2>
    order by patent_id ASC
   <cfelseif sv is 20>
    order by patent_id DESC
   <cfelseif sv is 3>
    order by title ASC
   <cfelseif sv is 30>
    order by title DESC
   <cfelseif sv is 4>
    order by organization ASC
   <cfelseif sv is 40>
    order by organization DESC
   <cfelseif sv is 5>
    order by type ASC
   <cfelseif sv is 50>
    order by type ASC
   </cfif>

  </cfquery>

<cfelse>

  <cfquery name="agencies" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
   select patent.patent_id, country, title, organization, patent.type, abstract as patent_desc, date from patent
   left join patent_rawassignee on patent_rawassignee.patent_id = patent.patent_id
   where patent.patent_id is not null
   and contains((organization),'#trim(session.patent_keyword)#')

   <cfif sv is 1>
    order by date DESC
   <cfelseif sv is 10>
    order by date ASC
   <cfelseif sv is 2>
    order by patent_id ASC
   <cfelseif sv is 20>
    order by patent_id DESC
   <cfelseif sv is 3>
    order by title ASC
   <cfelseif sv is 30>
    order by title DESC
   <cfelseif sv is 4>
    order by organization ASC
   <cfelseif sv is 40>
    order by organization DESC
   <cfelseif sv is 5>
    order by type ASC
   <cfelseif sv is 50>
    order by type ASC
   </cfif>

  </cfquery>

</cfif>

  <cfif isdefined("export")>
   <cfinclude template="/exchange/include/export_to_excel.cfm">
  </cfif>

  <cfparam name="URL.PageId" default="0">
  <cfset RecordsPerPage = 100>
  <cfset TotalPages = (agencies.Recordcount/RecordsPerPage)>
  <cfset StartRow = (URL.PageId*RecordsPerPage)+1>
  <cfset EndRow = StartRow+RecordsPerPage-1>

  <cfinclude template="/exchange/include/header.cfm">

      <div class="main_box">

          <cfoutput>

		  <table cellspacing=0 cellpadding=0 border=0 width=100%>

           <form action="/exchange/awards/save_search.cfm" method="post">

           <tr><td class="feed_header">Search Results (#trim(numberformat(agencies.recordcount,'999,999'))# Matching Records)</td>
               <td class="feed_sub_header" align=right><a href="index.cfm">Return</a></td></tr>
           <tr><td colspan=2><hr></td></tr>

           <tr><td class="feed_sub_header"><b>You searched for "<i>#replace(session.patent_keyword,'"','','all')#</i>"</b></td></tr>

           </cfoutput>

           </form>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>
          <tr><td height=5></td></tr>

          <cfif agencies.recordcount is 0>
           <tr><td class="feed_sub_header" style="font-weight: normal;" colspan=16>No patents were found with the information you entered.</td></tr>
          <cfelse>

          <tr>
              <td class="feed_option">

				  <cfif agencies.recordcount GT #RecordsPerPage#>
					  <b>Page: </b>&nbsp;|
					  <cfloop index="Pages" from="0" to="#TotalPages#">
					   <cfoutput>
					   <cfset DisplayPgNo = Pages+1>
						  <cfif URL.PageId eq pages>
							 <b>#DisplayPgNo#</b>&nbsp;|&nbsp;
						  <cfelse>
							 <a href="?pageid=#pages#&<cfif isdefined("sv")>&sv=#sv#</cfif>">#DisplayPgNo#</a>&nbsp;|&nbsp;
						  </cfif>
					   </cfoutput>
					  </cfloop>
				   </cfif>
              </td>
              <td class="feed_sub_header" align=right>

              <cfoutput>
              <a href="results.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>"><img src="/images/icon_export_excel.png" width=20 hspace=10 border=0 alt="Export to Excel" title="Export to Excel"></a>
              <a href="results.cfm?export=1<cfif isdefined("sv")>&sv=#sv#</cfif>">Export to Excel</a>
              </cfoutput>

              </td></tr>

          </table>

          <table cellspacing=0 cellpadding=0 border=0 width=100%>

          <cfoutput>

			  <tr height=40>
				 <td class="text_xsmall" width=75><a href="results.cfm?<cfif not isdefined("sv")>sv=1<cfelse><cfif #sv# is 1>sv=10<cfelse>sv=1</cfif></cfif>"><b>DATE GRANTED</b></a></td>
				 <td class="text_xsmall" width=75><a href="results.cfm?<cfif not isdefined("sv")>sv=2<cfelse><cfif #sv# is 2>sv=20<cfelse>sv=2</cfif></cfif>"><b>PATENT ##</b></a></td>
				 <td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=3<cfelse><cfif #sv# is 3>sv=30<cfelse>sv=3</cfif></cfif>"><b>TITLE</b></a></td>
				 <td class="text_xsmall"><a href="results.cfm?<cfif not isdefined("sv")>sv=4<cfelse><cfif #sv# is 4>sv=40<cfelse>sv=4</cfif></cfif>"><b>ORGANIZATION</b></a></td>
				 <td class="text_xsmall"><b>DESCRIPTION</b></td>
				 <td class="text_xsmall" align=right><a href="results.cfm?<cfif not isdefined("sv")>sv=5<cfelse><cfif #sv# is 5>sv=50<cfelse>sv=5</cfif></cfif>"><b>TYPE</b></a></td>
			  </tr>

          </cfoutput>

          <cfset counter = 0>

           <cfloop query="agencies">

			  <cfquery name="comp" datasource="#lake_datasource#" username="#lake_username#" password="#lake_password#">
			   select top(1) company_id from company
			   where company_name = '#agencies.organization#'
			  </cfquery>

           <cfoutput>

		       <cfif CurrentRow gte StartRow >

			   <cfif counter is 0>
				<tr bgcolor="ffffff" height=30>
			   <cfelse>
				<tr bgcolor="e0e0e0" height=30>
			   </cfif>

				   <td class="text_xsmall" valign=top width=100 style="padding-top: 20px; padding-bottom: 20px;">#dateformat(agencies.date,'mm/dd/yyyy')#</td>
			       <td class="text_xsmall" valign=top width=75 style="padding-top: 20px; padding-bottom: 20px;"><a href="/exchange/include/patent_information.cfm?patent_id=#agencies.patent_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#agencies.patent_id#</b></a></td>
				   <td class="text_xsmall" valign=top width=300 style="padding-top: 20px; padding-bottom: 20px;">
				   <a href="/exchange/include/patent_information.cfm?patent_id=#agencies.patent_id#" target="_blank" rel="noopener" rel="noreferrer"><b>
                    #replaceNoCase(agencies.title,session.patent_keyword,"<span style='background:yellow'>#ucase(session.patent_keyword)#</span>","all")#
				   </b></a></td>
				   <td class="text_xsmall" valign=top width=200 style="padding-top: 20px; padding-bottom: 20px;">

				   <cfif agencies.organization is "">
						Personal Patent
					   <cfelse>
					   <cfif #comp.recordcount# is 0>
						#ucase(agencies.organization)#
					   <cfelse>
						<a href="/exchange/include/company_profile.cfm?id=#comp.company_id#" target="_blank" rel="noopener" rel="noreferrer"><b>#ucase(agencies.organization)#</b></a>
					   </cfif>
					   </cfif>

				   </td>
                   <td class="text_xsmall" valign=top style="padding-top: 20px; padding-bottom: 20px;">
                    #replaceNoCase(agencies.patent_desc,session.patent_keyword,"<span style='background:yellow'>#ucase(session.patent_keyword)#</span>","all")#
				   </td>
				   <td class="text_xsmall" valign=top width=100 align=right style="padding-top: 20px; padding-bottom: 20px;">#ucase(agencies.type)#</td>

				</tr>

			  <cfif counter is 0>
			   <cfset counter = 1>
			  <cfelse>
			   <cfset counter = 0>
			  </cfif>

              </cfif>

				  <cfif CurrentRow eq EndRow>
				   <cfbreak>
				  </cfif>

          </cfoutput>

          </cfloop>

          </cfif>

          <tr><td>&nbsp;</td></tr>

		  </table>

	  </div>

<cfinclude template="/exchange/include/footer.cfm">

</body>
</html>