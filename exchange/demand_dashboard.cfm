<style>
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  width: 300px;
  padding-top: 10px;
  padding-bottom: 10px;
  text-align: left;
  box-shadow: 1px 8px 16px 1px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<cfquery name="options" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  select * from usr
  where usr_id = #session.usr_id#
</cfquery>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
  <tr><td height=5></td></tr>
  <tr>
	  <td class="feed_header" width=225 colspan=3>

      <cfoutput>#session.network_name#</cfoutput>

		 <!---
		 <div class="tooltip" style="text-decoration-line: underline; text-decoration-style: dashed;"><a href="/exchange/profile/" style="text-decoration-line: underline; text-decoration-style: dashed;">Personal Profile Strength</a>
		  <span class="tooltiptext">Increase your profile strength to help Partners.</span>
		 </div> --->
	  </td>
	  <cfoutput>
	  <td>
	  <!---
	  <a href="/exchange/company/strength.cfm"><progress value="#score#" max="#score_total#" style="width: 100px; height: 28px;" alt="#score_percent#%" title="#score_percent#%" align=absmiddle></progress></a>
	  --->

	  </td>
	  </cfoutput>

     <td class="feed_sub_header" align=right class="feed_sub_header"><a href="#" onclick="HelpHero.startTour('X3WALkUAQov')"><img src="/images/icon_tour.png" width=20 hspace=5 border=0></a><a href="#" onclick="HelpHero.startTour('X3WALkUAQov')">Take a Tour</a></td>

     </tr>
  <tr><td height=10></td></tr>
  <tr><td colspan=5><hr></td></tr>
  <tr><td height=10></td></tr>
  </tr>
</table>

<table cellspacing=0 cellpadding=0 border=0 width=100%>

 <tr><td height=10></td></tr>
 <tr>

   <td width=20% valign=top>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/portfolio/">Manage<br>Portfolios</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/portfolio/"><img src="/images/research.png" height=110 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

   </td><td valign=top width=20%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/marketplace/people/">Entrepreneurs &<br>Startups</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/marketplace/people/"><img src="/images/network_in.png" height=110 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

   </td><td valign=top width=20%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/marketplace/partners/network_out.cfm">Explore the<br>Marketplace</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/marketplace/partners/network_out.cfm"><img src="/images/network_out.png" height=110 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

   </td><td valign=top width=20%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/opps/">Find Funding<br>Opportunities</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/opps/"><img src="/images/funding.png" height=110 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>


   </td><td valign=top width=20%>

	   <table cellspacing=0 cellpadding=0 border=0 width=100%>
         <tr><td class="feed_header" align=center><a href="/exchange/awards/">Research the<br>Market</a></td></tr>
         <tr><td height=20></td></tr>
         <tr><td align=center><a href="/exchange/awards/"><img src="/images/research_market.png" height=110 border=0></a></td></tr>
		 <tr><td height=10></td></tr>
	   </table>

 </td></tr>
         <td align=center valign=top class="feed_sub_header" style="font-weight: normal; padding-left: 20px; padding-right: 20px;">Create and manage program or company portfolios, track company performance, growth and impact.</td>
         <td align=center valign=top class="feed_sub_header" style="font-weight: normal; padding-left: 20px; padding-right: 20px;">Connect with entrepreneurs, alumni, and people within and across the SEED SPOT ecosystem.</td>
         <td align=center valign=top  class="feed_sub_header" style="font-weight: normal; padding-left: 20px; padding-right: 20px;">Search for companies and organizations across the market, create market views and reports.</td>
         <td align=center valign=top  class="feed_sub_header" style="font-weight: normal; padding-left: 20px; padding-right: 20px;">Find new opportunities for Federal Funding, search new procurements, grants, and innovation needs.</td>
         <td align=center valign=top  class="feed_sub_header" style="font-weight: normal; padding-left: 20px; padding-right: 20px;">Research buying trends and patters, find partners, assess competitors, and find new innovations.</td>
 </tr>

 <tr><td height=10></td></tr>

</table>

