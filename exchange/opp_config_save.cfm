<cfinclude template="/exchange/security/check.cfm">

<cfquery name="update" datasource="#client_datasource#" username="#client_username#" password="#client_password#">
  update usr
  set usr_keywords = '#trim(keywords)#',
      usr_dashboard_opp = <cfif isdefined("usr_dashboard_opp")>1<cfelse>null</cfif>,
      usr_dashboard_comp = <cfif isdefined("usr_dashboard_comp")>1<cfelse>null</cfif>,
      usr_dashboard_comp_keywords = <cfif isdefined("usr_dashboard_comp_keywords")>'#trim(usr_dashboard_comp_keywords)#'<cfelse>null</cfif>
  where usr_id = #session.usr_id#
</cfquery>

<cflocation URL="/exchange/index.cfm?u=11" addtoken="no">